.class public Lnet/minidev/json/JSONStyle;
.super Ljava/lang/Object;
.source "JSONStyle.java"


# static fields
.field public static final FLAG_AGRESSIVE:I = 0x8

.field public static final FLAG_IGNORE_NULL:I = 0x10

.field public static final FLAG_PROTECT_4WEB:I = 0x2

.field public static final FLAG_PROTECT_KEYS:I = 0x1

.field public static final FLAG_PROTECT_VALUES:I = 0x4

.field public static final LT_COMPRESS:Lnet/minidev/json/JSONStyle;

.field public static final MAX_COMPRESS:Lnet/minidev/json/JSONStyle;

.field public static final NO_COMPRESS:Lnet/minidev/json/JSONStyle;


# instance fields
.field private _ignore_null:Z

.field private _protect4Web:Z

.field private _protectKeys:Z

.field private _protectValues:Z

.field private esc:Lnet/minidev/json/JStylerObj$StringProtector;

.field private mpKey:Lnet/minidev/json/JStylerObj$MustProtect;

.field private mpValue:Lnet/minidev/json/JStylerObj$MustProtect;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 49
    new-instance v0, Lnet/minidev/json/JSONStyle;

    invoke-direct {v0}, Lnet/minidev/json/JSONStyle;-><init>()V

    sput-object v0, Lnet/minidev/json/JSONStyle;->NO_COMPRESS:Lnet/minidev/json/JSONStyle;

    .line 50
    new-instance v0, Lnet/minidev/json/JSONStyle;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Lnet/minidev/json/JSONStyle;-><init>(I)V

    sput-object v0, Lnet/minidev/json/JSONStyle;->MAX_COMPRESS:Lnet/minidev/json/JSONStyle;

    .line 54
    new-instance v0, Lnet/minidev/json/JSONStyle;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Lnet/minidev/json/JSONStyle;-><init>(I)V

    sput-object v0, Lnet/minidev/json/JSONStyle;->LT_COMPRESS:Lnet/minidev/json/JSONStyle;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lnet/minidev/json/JSONStyle;-><init>(I)V

    .line 95
    return-void
.end method

.method public constructor <init>(I)V
    .locals 4
    .param p1, "FLAG"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    and-int/lit8 v1, p1, 0x1

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    iput-boolean v1, p0, Lnet/minidev/json/JSONStyle;->_protectKeys:Z

    .line 68
    and-int/lit8 v1, p1, 0x4

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    iput-boolean v1, p0, Lnet/minidev/json/JSONStyle;->_protectValues:Z

    .line 69
    and-int/lit8 v1, p1, 0x2

    if-nez v1, :cond_2

    move v1, v2

    :goto_2
    iput-boolean v1, p0, Lnet/minidev/json/JSONStyle;->_protect4Web:Z

    .line 70
    and-int/lit8 v1, p1, 0x10

    if-lez v1, :cond_3

    :goto_3
    iput-boolean v2, p0, Lnet/minidev/json/JSONStyle;->_ignore_null:Z

    .line 72
    and-int/lit8 v1, p1, 0x8

    if-lez v1, :cond_4

    .line 73
    sget-object v0, Lnet/minidev/json/JStylerObj;->MP_AGGRESIVE:Lnet/minidev/json/JStylerObj$MPAgressive;

    .line 77
    .local v0, "mp":Lnet/minidev/json/JStylerObj$MustProtect;
    :goto_4
    iget-boolean v1, p0, Lnet/minidev/json/JSONStyle;->_protectValues:Z

    if-eqz v1, :cond_5

    .line 78
    sget-object v1, Lnet/minidev/json/JStylerObj;->MP_TRUE:Lnet/minidev/json/JStylerObj$MPTrue;

    iput-object v1, p0, Lnet/minidev/json/JSONStyle;->mpValue:Lnet/minidev/json/JStylerObj$MustProtect;

    .line 82
    :goto_5
    iget-boolean v1, p0, Lnet/minidev/json/JSONStyle;->_protectKeys:Z

    if-eqz v1, :cond_6

    .line 83
    sget-object v1, Lnet/minidev/json/JStylerObj;->MP_TRUE:Lnet/minidev/json/JStylerObj$MPTrue;

    iput-object v1, p0, Lnet/minidev/json/JSONStyle;->mpKey:Lnet/minidev/json/JStylerObj$MustProtect;

    .line 87
    :goto_6
    iget-boolean v1, p0, Lnet/minidev/json/JSONStyle;->_protect4Web:Z

    if-eqz v1, :cond_7

    .line 88
    sget-object v1, Lnet/minidev/json/JStylerObj;->ESCAPE4Web:Lnet/minidev/json/JStylerObj$Escape4Web;

    iput-object v1, p0, Lnet/minidev/json/JSONStyle;->esc:Lnet/minidev/json/JStylerObj$StringProtector;

    .line 91
    :goto_7
    return-void

    .end local v0    # "mp":Lnet/minidev/json/JStylerObj$MustProtect;
    :cond_0
    move v1, v3

    .line 67
    goto :goto_0

    :cond_1
    move v1, v3

    .line 68
    goto :goto_1

    :cond_2
    move v1, v3

    .line 69
    goto :goto_2

    :cond_3
    move v2, v3

    .line 70
    goto :goto_3

    .line 75
    :cond_4
    sget-object v0, Lnet/minidev/json/JStylerObj;->MP_SIMPLE:Lnet/minidev/json/JStylerObj$MPSimple;

    .restart local v0    # "mp":Lnet/minidev/json/JStylerObj$MustProtect;
    goto :goto_4

    .line 80
    :cond_5
    iput-object v0, p0, Lnet/minidev/json/JSONStyle;->mpValue:Lnet/minidev/json/JStylerObj$MustProtect;

    goto :goto_5

    .line 85
    :cond_6
    iput-object v0, p0, Lnet/minidev/json/JSONStyle;->mpKey:Lnet/minidev/json/JStylerObj$MustProtect;

    goto :goto_6

    .line 90
    :cond_7
    sget-object v1, Lnet/minidev/json/JStylerObj;->ESCAPE_LT:Lnet/minidev/json/JStylerObj$EscapeLT;

    iput-object v1, p0, Lnet/minidev/json/JSONStyle;->esc:Lnet/minidev/json/JStylerObj$StringProtector;

    goto :goto_7
.end method


# virtual methods
.method public arrayNextElm(Ljava/lang/Appendable;)V
    .locals 1
    .param p1, "out"    # Ljava/lang/Appendable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 203
    const/16 v0, 0x2c

    invoke-interface {p1, v0}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 204
    return-void
.end method

.method public arrayObjectEnd(Ljava/lang/Appendable;)V
    .locals 0
    .param p1, "out"    # Ljava/lang/Appendable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 210
    return-void
.end method

.method public arrayStart(Ljava/lang/Appendable;)V
    .locals 1
    .param p1, "out"    # Ljava/lang/Appendable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 183
    const/16 v0, 0x5b

    invoke-interface {p1, v0}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 184
    return-void
.end method

.method public arrayStop(Ljava/lang/Appendable;)V
    .locals 1
    .param p1, "out"    # Ljava/lang/Appendable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 190
    const/16 v0, 0x5d

    invoke-interface {p1, v0}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 191
    return-void
.end method

.method public arrayfirstObject(Ljava/lang/Appendable;)V
    .locals 0
    .param p1, "out"    # Ljava/lang/Appendable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 197
    return-void
.end method

.method public escape(Ljava/lang/String;Ljava/lang/Appendable;)V
    .locals 1
    .param p1, "s"    # Ljava/lang/String;
    .param p2, "out"    # Ljava/lang/Appendable;

    .prologue
    .line 136
    iget-object v0, p0, Lnet/minidev/json/JSONStyle;->esc:Lnet/minidev/json/JStylerObj$StringProtector;

    invoke-interface {v0, p1, p2}, Lnet/minidev/json/JStylerObj$StringProtector;->escape(Ljava/lang/String;Ljava/lang/Appendable;)V

    .line 137
    return-void
.end method

.method public ignoreNull()Z
    .locals 1

    .prologue
    .line 110
    iget-boolean v0, p0, Lnet/minidev/json/JSONStyle;->_ignore_null:Z

    return v0
.end method

.method public indent()Z
    .locals 1

    .prologue
    .line 114
    const/4 v0, 0x0

    return v0
.end method

.method public mustProtectKey(Ljava/lang/String;)Z
    .locals 1
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 118
    iget-object v0, p0, Lnet/minidev/json/JSONStyle;->mpKey:Lnet/minidev/json/JStylerObj$MustProtect;

    invoke-interface {v0, p1}, Lnet/minidev/json/JStylerObj$MustProtect;->mustBeProtect(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public mustProtectValue(Ljava/lang/String;)Z
    .locals 1
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 122
    iget-object v0, p0, Lnet/minidev/json/JSONStyle;->mpValue:Lnet/minidev/json/JStylerObj$MustProtect;

    invoke-interface {v0, p1}, Lnet/minidev/json/JStylerObj$MustProtect;->mustBeProtect(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public objectElmStop(Ljava/lang/Appendable;)V
    .locals 0
    .param p1, "out"    # Ljava/lang/Appendable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 170
    return-void
.end method

.method public objectEndOfKey(Ljava/lang/Appendable;)V
    .locals 1
    .param p1, "out"    # Ljava/lang/Appendable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 176
    const/16 v0, 0x3a

    invoke-interface {p1, v0}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 177
    return-void
.end method

.method public objectFirstStart(Ljava/lang/Appendable;)V
    .locals 0
    .param p1, "out"    # Ljava/lang/Appendable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 157
    return-void
.end method

.method public objectNext(Ljava/lang/Appendable;)V
    .locals 1
    .param p1, "out"    # Ljava/lang/Appendable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 163
    const/16 v0, 0x2c

    invoke-interface {p1, v0}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 164
    return-void
.end method

.method public objectStart(Ljava/lang/Appendable;)V
    .locals 1
    .param p1, "out"    # Ljava/lang/Appendable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 143
    const/16 v0, 0x7b

    invoke-interface {p1, v0}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 144
    return-void
.end method

.method public objectStop(Ljava/lang/Appendable;)V
    .locals 1
    .param p1, "out"    # Ljava/lang/Appendable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 150
    const/16 v0, 0x7d

    invoke-interface {p1, v0}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 151
    return-void
.end method

.method public protect4Web()Z
    .locals 1

    .prologue
    .line 106
    iget-boolean v0, p0, Lnet/minidev/json/JSONStyle;->_protect4Web:Z

    return v0
.end method

.method public protectKeys()Z
    .locals 1

    .prologue
    .line 98
    iget-boolean v0, p0, Lnet/minidev/json/JSONStyle;->_protectKeys:Z

    return v0
.end method

.method public protectValues()Z
    .locals 1

    .prologue
    .line 102
    iget-boolean v0, p0, Lnet/minidev/json/JSONStyle;->_protectValues:Z

    return v0
.end method

.method public writeString(Ljava/lang/Appendable;Ljava/lang/String;)V
    .locals 2
    .param p1, "out"    # Ljava/lang/Appendable;
    .param p2, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v1, 0x22

    .line 126
    invoke-virtual {p0, p2}, Lnet/minidev/json/JSONStyle;->mustProtectValue(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 127
    invoke-interface {p1, p2}, Ljava/lang/Appendable;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    .line 133
    :goto_0
    return-void

    .line 129
    :cond_0
    invoke-interface {p1, v1}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 130
    invoke-static {p2, p1, p0}, Lnet/minidev/json/JSONValue;->escape(Ljava/lang/String;Ljava/lang/Appendable;Lnet/minidev/json/JSONStyle;)V

    .line 131
    invoke-interface {p1, v1}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    goto :goto_0
.end method

.class public Lorg/droidparts/concurrent/thread/BackgroundThreadExecutor;
.super Ljava/util/concurrent/ThreadPoolExecutor;
.source "BackgroundThreadExecutor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/droidparts/concurrent/thread/BackgroundThreadExecutor$BackgroundThreadFactory;
    }
.end annotation


# direct methods
.method public constructor <init>(ILjava/lang/String;)V
    .locals 9
    .param p1, "nThreads"    # I
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 26
    const-wide/16 v4, 0x0

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v7, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v7}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    new-instance v8, Lorg/droidparts/concurrent/thread/BackgroundThreadExecutor$BackgroundThreadFactory;

    invoke-direct {v8, p2}, Lorg/droidparts/concurrent/thread/BackgroundThreadExecutor$BackgroundThreadFactory;-><init>(Ljava/lang/String;)V

    move-object v1, p0

    move v2, p1

    move v3, p1

    invoke-direct/range {v1 .. v8}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    .line 29
    return-void
.end method

.class public Lorg/droidparts/net/http/worker/HttpClientWorker;
.super Lorg/droidparts/net/http/worker/HTTPWorker;
.source "HttpClientWorker.java"


# instance fields
.field private final httpClient:Lorg/apache/http/impl/client/DefaultHttpClient;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 3
    .param p1, "userAgent"    # Ljava/lang/String;

    .prologue
    const v2, 0xea60

    .line 50
    invoke-direct {p0}, Lorg/droidparts/net/http/worker/HTTPWorker;-><init>()V

    .line 51
    new-instance v1, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v1}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    iput-object v1, p0, Lorg/droidparts/net/http/worker/HttpClientWorker;->httpClient:Lorg/apache/http/impl/client/DefaultHttpClient;

    .line 52
    iget-object v1, p0, Lorg/droidparts/net/http/worker/HttpClientWorker;->httpClient:Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-virtual {v1}, Lorg/apache/http/impl/client/DefaultHttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v0

    .line 53
    .local v0, "params":Lorg/apache/http/params/HttpParams;
    if-eqz p1, :cond_0

    .line 54
    invoke-static {v0, p1}, Lorg/apache/http/params/HttpProtocolParams;->setUserAgent(Lorg/apache/http/params/HttpParams;Ljava/lang/String;)V

    .line 56
    :cond_0
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lorg/apache/http/params/HttpConnectionParams;->setStaleCheckingEnabled(Lorg/apache/http/params/HttpParams;Z)V

    .line 57
    iget-boolean v1, p0, Lorg/droidparts/net/http/worker/HttpClientWorker;->followRedirects:Z

    invoke-virtual {p0, v1}, Lorg/droidparts/net/http/worker/HttpClientWorker;->setFollowRedirects(Z)V

    .line 58
    invoke-static {v0, v2}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 60
    invoke-static {v0, v2}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 61
    const/16 v1, 0x2000

    invoke-static {v0, v1}, Lorg/apache/http/params/HttpConnectionParams;->setSocketBufferSize(Lorg/apache/http/params/HttpParams;I)V

    .line 62
    const-string v1, "compatibility"

    invoke-static {v0, v1}, Lorg/apache/http/client/params/HttpClientParams;->setCookiePolicy(Lorg/apache/http/params/HttpParams;Ljava/lang/String;)V

    .line 63
    return-void
.end method

.method public static buildMultipartEntity(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Lorg/apache/http/HttpEntity;
    .locals 3
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "contentType"    # Ljava/lang/String;
    .param p2, "file"    # Ljava/io/File;

    .prologue
    .line 94
    :try_start_0
    invoke-static {p0, p1, p2}, Lorg/droidparts/net/http/worker/wrapper/HttpMimeWrapper;->buildMultipartEntity(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Lorg/apache/http/HttpEntity;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 96
    :catch_0
    move-exception v0

    .line 97
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "You have to add Apache HttpMime dependency in order to use multipart entities."

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static buildStringEntity(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/http/entity/StringEntity;
    .locals 3
    .param p0, "contentType"    # Ljava/lang/String;
    .param p1, "data"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/droidparts/net/http/HTTPException;
        }
    .end annotation

    .prologue
    .line 83
    :try_start_0
    new-instance v1, Lorg/apache/http/entity/StringEntity;

    const-string v2, "utf-8"

    invoke-direct {v1, p1, v2}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    .local v1, "entity":Lorg/apache/http/entity/StringEntity;
    invoke-virtual {v1, p0}, Lorg/apache/http/entity/StringEntity;->setContentType(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 85
    return-object v1

    .line 86
    .end local v1    # "entity":Lorg/apache/http/entity/StringEntity;
    :catch_0
    move-exception v0

    .line 87
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    new-instance v2, Lorg/droidparts/net/http/HTTPException;

    invoke-direct {v2, v0}, Lorg/droidparts/net/http/HTTPException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method private static getHeaders(Lorg/apache/http/HttpResponse;)Ljava/util/Map;
    .locals 8
    .param p0, "resp"    # Lorg/apache/http/HttpResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/http/HttpResponse;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 143
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 144
    .local v2, "headers":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    invoke-interface {p0}, Lorg/apache/http/HttpResponse;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v0

    .local v0, "arr$":[Lorg/apache/http/Header;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v1, v0, v3

    .line 145
    .local v1, "header":Lorg/apache/http/Header;
    invoke-interface {v1}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v5

    .line 146
    .local v5, "name":Ljava/lang/String;
    invoke-virtual {v2, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 147
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v2, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    :cond_0
    invoke-virtual {v2, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/List;

    invoke-interface {v1}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 144
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 151
    .end local v1    # "header":Lorg/apache/http/Header;
    .end local v5    # "name":Ljava/lang/String;
    :cond_1
    return-object v2
.end method

.method private getHttpResponse(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    .locals 5
    .param p1, "req"    # Lorg/apache/http/client/methods/HttpUriRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/droidparts/net/http/HTTPException;
        }
    .end annotation

    .prologue
    .line 120
    iget-object v3, p0, Lorg/droidparts/net/http/worker/HttpClientWorker;->headers:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 121
    .local v2, "key":Ljava/lang/String;
    iget-object v3, p0, Lorg/droidparts/net/http/worker/HttpClientWorker;->headers:Ljava/util/HashMap;

    invoke-virtual {v3, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {p1, v2, v3}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 123
    .end local v2    # "key":Ljava/lang/String;
    :cond_0
    const-string v3, "Accept-Encoding"

    const-string v4, "gzip,deflate"

    invoke-interface {p1, v3, v4}, Lorg/apache/http/client/methods/HttpUriRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    :try_start_0
    iget-object v3, p0, Lorg/droidparts/net/http/worker/HttpClientWorker;->httpClient:Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-virtual {v3, p1}, Lorg/apache/http/impl/client/DefaultHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    return-object v3

    .line 126
    :catch_0
    move-exception v0

    .line 127
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {v0}, Lorg/droidparts/net/http/worker/HttpClientWorker;->throwIfNetworkOnMainThreadException(Ljava/lang/Exception;)V

    .line 128
    new-instance v3, Lorg/droidparts/net/http/HTTPException;

    invoke-direct {v3, v0}, Lorg/droidparts/net/http/HTTPException;-><init>(Ljava/lang/Throwable;)V

    throw v3
.end method

.method private static getResponseCodeOrThrow(Lorg/apache/http/HttpResponse;)I
    .locals 3
    .param p0, "resp"    # Lorg/apache/http/HttpResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/droidparts/net/http/HTTPException;
        }
    .end annotation

    .prologue
    .line 134
    invoke-interface {p0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v1

    .line 135
    .local v1, "respCode":I
    invoke-static {v1}, Lorg/droidparts/net/http/worker/HttpClientWorker;->isErrorResponseCode(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 136
    invoke-static {p0}, Lorg/droidparts/net/http/worker/HTTPInputStream;->getInstance(Lorg/apache/http/HttpResponse;)Lorg/droidparts/net/http/worker/HTTPInputStream;

    move-result-object v2

    invoke-virtual {v2}, Lorg/droidparts/net/http/worker/HTTPInputStream;->readAndClose()Ljava/lang/String;

    move-result-object v0

    .line 137
    .local v0, "respBody":Ljava/lang/String;
    new-instance v2, Lorg/droidparts/net/http/HTTPException;

    invoke-direct {v2, v1, v0}, Lorg/droidparts/net/http/HTTPException;-><init>(ILjava/lang/String;)V

    throw v2

    .line 139
    .end local v0    # "respBody":Ljava/lang/String;
    :cond_0
    return v1
.end method


# virtual methods
.method public final getHttpClient()Lorg/apache/http/impl/client/DefaultHttpClient;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lorg/droidparts/net/http/worker/HttpClientWorker;->httpClient:Lorg/apache/http/impl/client/DefaultHttpClient;

    return-object v0
.end method

.method public getResponse(Lorg/apache/http/client/methods/HttpUriRequest;Z)Lorg/droidparts/net/http/HTTPResponse;
    .locals 4
    .param p1, "req"    # Lorg/apache/http/client/methods/HttpUriRequest;
    .param p2, "body"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/droidparts/net/http/HTTPException;
        }
    .end annotation

    .prologue
    .line 105
    new-instance v2, Lorg/droidparts/net/http/HTTPResponse;

    invoke-direct {v2}, Lorg/droidparts/net/http/HTTPResponse;-><init>()V

    .line 106
    .local v2, "response":Lorg/droidparts/net/http/HTTPResponse;
    invoke-direct {p0, p1}, Lorg/droidparts/net/http/worker/HttpClientWorker;->getHttpResponse(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v1

    .line 107
    .local v1, "resp":Lorg/apache/http/HttpResponse;
    invoke-static {v1}, Lorg/droidparts/net/http/worker/HttpClientWorker;->getResponseCodeOrThrow(Lorg/apache/http/HttpResponse;)I

    move-result v3

    iput v3, v2, Lorg/droidparts/net/http/HTTPResponse;->code:I

    .line 108
    invoke-static {v1}, Lorg/droidparts/net/http/worker/HttpClientWorker;->getHeaders(Lorg/apache/http/HttpResponse;)Ljava/util/Map;

    move-result-object v3

    iput-object v3, v2, Lorg/droidparts/net/http/HTTPResponse;->headers:Ljava/util/Map;

    .line 109
    invoke-static {v1}, Lorg/droidparts/net/http/worker/HTTPInputStream;->getInstance(Lorg/apache/http/HttpResponse;)Lorg/droidparts/net/http/worker/HTTPInputStream;

    move-result-object v0

    .line 110
    .local v0, "is":Lorg/droidparts/net/http/worker/HTTPInputStream;
    if-eqz p2, :cond_0

    .line 111
    invoke-virtual {v0}, Lorg/droidparts/net/http/worker/HTTPInputStream;->readAndClose()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lorg/droidparts/net/http/HTTPResponse;->body:Ljava/lang/String;

    .line 115
    :goto_0
    return-object v2

    .line 113
    :cond_0
    iput-object v0, v2, Lorg/droidparts/net/http/HTTPResponse;->inputStream:Lorg/droidparts/net/http/worker/HTTPInputStream;

    goto :goto_0
.end method

.method public setCookieJar(Lorg/droidparts/net/http/CookieJar;)V
    .locals 1
    .param p1, "cookieJar"    # Lorg/droidparts/net/http/CookieJar;

    .prologue
    .line 73
    iget-object v0, p0, Lorg/droidparts/net/http/worker/HttpClientWorker;->httpClient:Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-virtual {v0, p1}, Lorg/apache/http/impl/client/DefaultHttpClient;->setCookieStore(Lorg/apache/http/client/CookieStore;)V

    .line 74
    return-void
.end method

.method public setFollowRedirects(Z)V
    .locals 2
    .param p1, "follow"    # Z

    .prologue
    .line 67
    iget-object v0, p0, Lorg/droidparts/net/http/worker/HttpClientWorker;->httpClient:Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-virtual {v0}, Lorg/apache/http/impl/client/DefaultHttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v0

    iget-boolean v1, p0, Lorg/droidparts/net/http/worker/HttpClientWorker;->followRedirects:Z

    invoke-static {v0, v1}, Lorg/apache/http/client/params/HttpClientParams;->setRedirecting(Lorg/apache/http/params/HttpParams;Z)V

    .line 69
    return-void
.end method

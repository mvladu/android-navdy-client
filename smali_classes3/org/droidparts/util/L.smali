.class public Lorg/droidparts/util/L;
.super Ljava/lang/Object;
.source "L.java"


# static fields
.field public static final ASSERT:I = 0x7

.field public static final DEBUG:I = 0x3

.field private static final DISABLE:I = 0x400

.field public static final ERROR:I = 0x6

.field public static final INFO:I = 0x4

.field private static final TAG:Ljava/lang/String; = "DroidParts"

.field public static final VERBOSE:I = 0x2

.field public static final WARN:I = 0x5

.field private static _debug:I

.field private static _logLevel:I

.field private static _tag:Ljava/lang/String;


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 223
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 224
    return-void
.end method

.method public static d(Ljava/lang/Object;)V
    .locals 2
    .param p0, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x3

    .line 46
    invoke-static {v1}, Lorg/droidparts/util/L;->isLoggable(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 47
    invoke-static {v1, p0}, Lorg/droidparts/util/L;->log(ILjava/lang/Object;)V

    .line 49
    :cond_0
    return-void
.end method

.method public static varargs d(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2
    .param p0, "format"    # Ljava/lang/String;
    .param p1, "args"    # [Ljava/lang/Object;

    .prologue
    const/4 v1, 0x3

    .line 52
    invoke-static {v1}, Lorg/droidparts/util/L;->isLoggable(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 53
    invoke-static {v1, p0, p1}, Lorg/droidparts/util/L;->log(ILjava/lang/String;[Ljava/lang/Object;)V

    .line 55
    :cond_0
    return-void
.end method

.method public static e(Ljava/lang/Object;)V
    .locals 2
    .param p0, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x6

    .line 82
    invoke-static {v1}, Lorg/droidparts/util/L;->isLoggable(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83
    invoke-static {v1, p0}, Lorg/droidparts/util/L;->log(ILjava/lang/Object;)V

    .line 85
    :cond_0
    return-void
.end method

.method public static varargs e(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2
    .param p0, "format"    # Ljava/lang/String;
    .param p1, "args"    # [Ljava/lang/Object;

    .prologue
    const/4 v1, 0x6

    .line 88
    invoke-static {v1}, Lorg/droidparts/util/L;->isLoggable(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    invoke-static {v1, p0, p1}, Lorg/droidparts/util/L;->log(ILjava/lang/String;[Ljava/lang/Object;)V

    .line 91
    :cond_0
    return-void
.end method

.method private static getLogLevel()I
    .locals 5

    .prologue
    const/4 v2, 0x2

    .line 160
    sget v3, Lorg/droidparts/util/L;->_logLevel:I

    if-nez v3, :cond_0

    .line 161
    invoke-static {}, Lorg/droidparts/Injector;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 162
    .local v0, "ctx":Landroid/content/Context;
    if-eqz v0, :cond_0

    .line 163
    const/4 v1, 0x0

    .line 165
    .local v1, "logLevelStr":Ljava/lang/String;
    :try_start_0
    const-string v3, "droidparts_log_level"

    invoke-static {v0, v3}, Lorg/droidparts/inner/ManifestMetaData;->get(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 170
    :goto_0
    const-string v3, "verbose"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 171
    sput v2, Lorg/droidparts/util/L;->_logLevel:I

    .line 193
    .end local v1    # "logLevelStr":Ljava/lang/String;
    :cond_0
    :goto_1
    sget v3, Lorg/droidparts/util/L;->_logLevel:I

    if-eqz v3, :cond_1

    sget v2, Lorg/droidparts/util/L;->_logLevel:I

    :cond_1
    return v2

    .line 172
    .restart local v1    # "logLevelStr":Ljava/lang/String;
    :cond_2
    const-string v3, "debug"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 173
    const/4 v3, 0x3

    sput v3, Lorg/droidparts/util/L;->_logLevel:I

    goto :goto_1

    .line 174
    :cond_3
    const-string v3, "info"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 175
    const/4 v3, 0x4

    sput v3, Lorg/droidparts/util/L;->_logLevel:I

    goto :goto_1

    .line 176
    :cond_4
    const-string v3, "warn"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 177
    const/4 v3, 0x5

    sput v3, Lorg/droidparts/util/L;->_logLevel:I

    goto :goto_1

    .line 178
    :cond_5
    const-string v3, "error"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 179
    const/4 v3, 0x6

    sput v3, Lorg/droidparts/util/L;->_logLevel:I

    goto :goto_1

    .line 180
    :cond_6
    const-string v3, "assert"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 181
    const/4 v3, 0x7

    sput v3, Lorg/droidparts/util/L;->_logLevel:I

    goto :goto_1

    .line 182
    :cond_7
    const-string v3, "disable"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 183
    const/16 v3, 0x400

    sput v3, Lorg/droidparts/util/L;->_logLevel:I

    goto :goto_1

    .line 185
    :cond_8
    sput v2, Lorg/droidparts/util/L;->_logLevel:I

    .line 186
    const-string v3, "DroidParts"

    const-string v4, "No valid <meta-data android:name=\"droidparts_log_level\" android:value=\"...\"/> in AndroidManifest.xml."

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 167
    :catch_0
    move-exception v3

    goto :goto_0
.end method

.method private static getTag(Z)Ljava/lang/String;
    .locals 8
    .param p0, "debug"    # Z

    .prologue
    const/4 v7, 0x5

    .line 197
    if-eqz p0, :cond_0

    .line 198
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v5

    aget-object v1, v5, v7

    .line 199
    .local v1, "caller":Ljava/lang/StackTraceElement;
    invoke-virtual {v1}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v0

    .line 200
    .local v0, "c":Ljava/lang/String;
    const-string v5, "."

    invoke-virtual {v0, v5}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v0, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 201
    .local v2, "className":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 202
    .local v4, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 203
    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 204
    invoke-virtual {v1}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 205
    const-string v5, "():"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 206
    invoke-virtual {v1}, Ljava/lang/StackTraceElement;->getLineNumber()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 207
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 215
    .end local v0    # "c":Ljava/lang/String;
    .end local v1    # "caller":Ljava/lang/StackTraceElement;
    .end local v2    # "className":Ljava/lang/String;
    .end local v4    # "sb":Ljava/lang/StringBuilder;
    :goto_0
    return-object v5

    .line 209
    :cond_0
    sget-object v5, Lorg/droidparts/util/L;->_tag:Ljava/lang/String;

    if-nez v5, :cond_1

    .line 210
    invoke-static {}, Lorg/droidparts/Injector;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    .line 211
    .local v3, "ctx":Landroid/content/Context;
    if-eqz v3, :cond_1

    .line 212
    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    sput-object v5, Lorg/droidparts/util/L;->_tag:Ljava/lang/String;

    .line 215
    .end local v3    # "ctx":Landroid/content/Context;
    :cond_1
    sget-object v5, Lorg/droidparts/util/L;->_tag:Ljava/lang/String;

    if-eqz v5, :cond_2

    sget-object v5, Lorg/droidparts/util/L;->_tag:Ljava/lang/String;

    goto :goto_0

    :cond_2
    const-string v5, "DroidParts"

    goto :goto_0
.end method

.method public static i(Ljava/lang/Object;)V
    .locals 2
    .param p0, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x4

    .line 58
    invoke-static {v1}, Lorg/droidparts/util/L;->isLoggable(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 59
    invoke-static {v1, p0}, Lorg/droidparts/util/L;->log(ILjava/lang/Object;)V

    .line 61
    :cond_0
    return-void
.end method

.method public static varargs i(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2
    .param p0, "format"    # Ljava/lang/String;
    .param p1, "args"    # [Ljava/lang/Object;

    .prologue
    const/4 v1, 0x4

    .line 64
    invoke-static {v1}, Lorg/droidparts/util/L;->isLoggable(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    invoke-static {v1, p0, p1}, Lorg/droidparts/util/L;->log(ILjava/lang/String;[Ljava/lang/Object;)V

    .line 67
    :cond_0
    return-void
.end method

.method private static isDebug()Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 150
    sget v1, Lorg/droidparts/util/L;->_debug:I

    if-nez v1, :cond_0

    .line 151
    invoke-static {}, Lorg/droidparts/Injector;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 152
    .local v0, "ctx":Landroid/content/Context;
    if-eqz v0, :cond_0

    .line 153
    invoke-static {v0}, Lorg/droidparts/util/AppUtils;->isDebuggable(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    move v1, v2

    :goto_0
    sput v1, Lorg/droidparts/util/L;->_debug:I

    .line 156
    :cond_0
    sget v1, Lorg/droidparts/util/L;->_debug:I

    if-ne v1, v2, :cond_2

    :goto_1
    return v2

    .line 153
    :cond_1
    const/4 v1, -0x1

    goto :goto_0

    .line 156
    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public static isLoggable(I)Z
    .locals 1
    .param p0, "level"    # I

    .prologue
    .line 112
    invoke-static {}, Lorg/droidparts/util/L;->getLogLevel()I

    move-result v0

    if-lt p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static log(ILjava/lang/Object;)V
    .locals 3
    .param p0, "priority"    # I
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 127
    instance-of v2, p1, Ljava/lang/Throwable;

    if-eqz v2, :cond_1

    .line 128
    new-instance v1, Ljava/io/StringWriter;

    invoke-direct {v1}, Ljava/io/StringWriter;-><init>()V

    .line 129
    .local v1, "sw":Ljava/io/StringWriter;
    check-cast p1, Ljava/lang/Throwable;

    .end local p1    # "obj":Ljava/lang/Object;
    new-instance v2, Ljava/io/PrintWriter;

    invoke-direct {v2, v1}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    invoke-virtual {p1, v2}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintWriter;)V

    .line 130
    invoke-virtual {v1}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v0

    .line 137
    .end local v1    # "sw":Ljava/io/StringWriter;
    .local v0, "msg":Ljava/lang/String;
    :cond_0
    :goto_0
    invoke-static {}, Lorg/droidparts/util/L;->isDebug()Z

    move-result v2

    invoke-static {v2}, Lorg/droidparts/util/L;->getTag(Z)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2, v0}, Landroid/util/Log;->println(ILjava/lang/String;Ljava/lang/String;)I

    .line 138
    return-void

    .line 132
    .end local v0    # "msg":Ljava/lang/String;
    .restart local p1    # "obj":Ljava/lang/Object;
    :cond_1
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 133
    .restart local v0    # "msg":Ljava/lang/String;
    invoke-static {v0}, Lorg/droidparts/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 134
    const-string v0, "\"\""

    goto :goto_0
.end method

.method private static varargs log(ILjava/lang/String;[Ljava/lang/Object;)V
    .locals 3
    .param p0, "priority"    # I
    .param p1, "format"    # Ljava/lang/String;
    .param p2, "args"    # [Ljava/lang/Object;

    .prologue
    .line 142
    :try_start_0
    invoke-static {p1, p2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 143
    .local v1, "msg":Ljava/lang/String;
    invoke-static {}, Lorg/droidparts/util/L;->isDebug()Z

    move-result v2

    invoke-static {v2}, Lorg/droidparts/util/L;->getTag(Z)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2, v1}, Landroid/util/Log;->println(ILjava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 147
    .end local v1    # "msg":Ljava/lang/String;
    :goto_0
    return-void

    .line 144
    :catch_0
    move-exception v0

    .line 145
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {v0}, Lorg/droidparts/util/L;->e(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static v(Ljava/lang/Object;)V
    .locals 2
    .param p0, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x2

    .line 34
    invoke-static {v1}, Lorg/droidparts/util/L;->isLoggable(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 35
    invoke-static {v1, p0}, Lorg/droidparts/util/L;->log(ILjava/lang/Object;)V

    .line 37
    :cond_0
    return-void
.end method

.method public static varargs v(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2
    .param p0, "format"    # Ljava/lang/String;
    .param p1, "args"    # [Ljava/lang/Object;

    .prologue
    const/4 v1, 0x2

    .line 40
    invoke-static {v1}, Lorg/droidparts/util/L;->isLoggable(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 41
    invoke-static {v1, p0, p1}, Lorg/droidparts/util/L;->log(ILjava/lang/String;[Ljava/lang/Object;)V

    .line 43
    :cond_0
    return-void
.end method

.method public static w(Ljava/lang/Object;)V
    .locals 2
    .param p0, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x5

    .line 70
    invoke-static {v1}, Lorg/droidparts/util/L;->isLoggable(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 71
    invoke-static {v1, p0}, Lorg/droidparts/util/L;->log(ILjava/lang/Object;)V

    .line 73
    :cond_0
    return-void
.end method

.method public static varargs w(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2
    .param p0, "format"    # Ljava/lang/String;
    .param p1, "args"    # [Ljava/lang/Object;

    .prologue
    const/4 v1, 0x5

    .line 76
    invoke-static {v1}, Lorg/droidparts/util/L;->isLoggable(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    invoke-static {v1, p0, p1}, Lorg/droidparts/util/L;->log(ILjava/lang/String;[Ljava/lang/Object;)V

    .line 79
    :cond_0
    return-void
.end method

.method public static wtf()V
    .locals 2

    .prologue
    const/4 v1, 0x7

    .line 106
    invoke-static {v1}, Lorg/droidparts/util/L;->isLoggable(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 107
    const-string v0, "WTF"

    invoke-static {v1, v0}, Lorg/droidparts/util/L;->log(ILjava/lang/Object;)V

    .line 109
    :cond_0
    return-void
.end method

.method public static wtf(Ljava/lang/Object;)V
    .locals 2
    .param p0, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x7

    .line 94
    invoke-static {v1}, Lorg/droidparts/util/L;->isLoggable(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 95
    invoke-static {v1, p0}, Lorg/droidparts/util/L;->log(ILjava/lang/Object;)V

    .line 97
    :cond_0
    return-void
.end method

.method public static varargs wtf(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2
    .param p0, "format"    # Ljava/lang/String;
    .param p1, "args"    # [Ljava/lang/Object;

    .prologue
    const/4 v1, 0x7

    .line 100
    invoke-static {v1}, Lorg/droidparts/util/L;->isLoggable(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 101
    invoke-static {v1, p0, p1}, Lorg/droidparts/util/L;->log(ILjava/lang/String;[Ljava/lang/Object;)V

    .line 103
    :cond_0
    return-void
.end method

.class public final Lorg/droidparts/inner/ann/serialize/JSONAnn;
.super Lorg/droidparts/inner/ann/Ann;
.source "JSONAnn.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/droidparts/inner/ann/Ann",
        "<",
        "Lorg/droidparts/annotation/serialize/JSON;",
        ">;"
    }
.end annotation


# instance fields
.field public key:Ljava/lang/String;

.field public final optional:Z


# direct methods
.method public constructor <init>(Lorg/droidparts/annotation/serialize/JSON;)V
    .locals 1
    .param p1, "annotation"    # Lorg/droidparts/annotation/serialize/JSON;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lorg/droidparts/inner/ann/Ann;-><init>(Ljava/lang/annotation/Annotation;)V

    .line 28
    invoke-virtual {p0}, Lorg/droidparts/inner/ann/serialize/JSONAnn;->hackSuccess()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 29
    const-string v0, "key"

    invoke-virtual {p0, v0}, Lorg/droidparts/inner/ann/serialize/JSONAnn;->getElement(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lorg/droidparts/inner/ann/serialize/JSONAnn;->key:Ljava/lang/String;

    .line 30
    const-string v0, "optional"

    invoke-virtual {p0, v0}, Lorg/droidparts/inner/ann/serialize/JSONAnn;->getElement(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lorg/droidparts/inner/ann/serialize/JSONAnn;->optional:Z

    .line 31
    invoke-virtual {p0}, Lorg/droidparts/inner/ann/serialize/JSONAnn;->cleanup()V

    .line 36
    :goto_0
    return-void

    .line 33
    :cond_0
    invoke-interface {p1}, Lorg/droidparts/annotation/serialize/JSON;->key()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/droidparts/inner/ann/serialize/JSONAnn;->key:Ljava/lang/String;

    .line 34
    invoke-interface {p1}, Lorg/droidparts/annotation/serialize/JSON;->optional()Z

    move-result v0

    iput-boolean v0, p0, Lorg/droidparts/inner/ann/serialize/JSONAnn;->optional:Z

    goto :goto_0
.end method

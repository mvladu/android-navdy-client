.class public Lorg/droidparts/inner/reader/LegacyReader;
.super Ljava/lang/Object;
.source "LegacyReader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/droidparts/inner/reader/LegacyReader$ISupportFragmentsReader;
    }
.end annotation


# static fields
.field private static supportFragmentsReader:Lorg/droidparts/inner/reader/LegacyReader$ISupportFragmentsReader;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 31
    :try_start_0
    const-string v1, "org.droidparts.inner.reader.SupportFragmentsReader"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/droidparts/inner/reader/LegacyReader$ISupportFragmentsReader;

    sput-object v1, Lorg/droidparts/inner/reader/LegacyReader;->supportFragmentsReader:Lorg/droidparts/inner/reader/LegacyReader$ISupportFragmentsReader;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 37
    .local v0, "e":Ljava/lang/Exception;
    :goto_0
    return-void

    .line 34
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_0
    move-exception v0

    .line 35
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string v1, "Legacy package not available."

    invoke-static {v1}, Lorg/droidparts/util/L;->i(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    return-void
.end method

.method static getFragment(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;
    .locals 1
    .param p0, "fragmentActivityObj"    # Ljava/lang/Object;
    .param p1, "fragmentId"    # I
    .param p2, "valName"    # Ljava/lang/String;

    .prologue
    .line 53
    sget-object v0, Lorg/droidparts/inner/reader/LegacyReader;->supportFragmentsReader:Lorg/droidparts/inner/reader/LegacyReader$ISupportFragmentsReader;

    invoke-interface {v0, p0, p1, p2}, Lorg/droidparts/inner/reader/LegacyReader$ISupportFragmentsReader;->getFragment(Ljava/lang/Object;ILjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static getFragmentArguments(Ljava/lang/Object;)Landroid/os/Bundle;
    .locals 1
    .param p0, "fragmentObj"    # Ljava/lang/Object;

    .prologue
    .line 58
    sget-object v0, Lorg/droidparts/inner/reader/LegacyReader;->supportFragmentsReader:Lorg/droidparts/inner/reader/LegacyReader$ISupportFragmentsReader;

    invoke-interface {v0, p0}, Lorg/droidparts/inner/reader/LegacyReader$ISupportFragmentsReader;->getFragmentArguments(Ljava/lang/Object;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method static getParentActivity(Ljava/lang/Object;)Landroid/app/Activity;
    .locals 1
    .param p0, "fragmentObj"    # Ljava/lang/Object;

    .prologue
    .line 48
    sget-object v0, Lorg/droidparts/inner/reader/LegacyReader;->supportFragmentsReader:Lorg/droidparts/inner/reader/LegacyReader$ISupportFragmentsReader;

    invoke-interface {v0, p0}, Lorg/droidparts/inner/reader/LegacyReader$ISupportFragmentsReader;->getParentActivity(Ljava/lang/Object;)Landroid/app/Activity;

    move-result-object v0

    return-object v0
.end method

.method public static isSupportAvaliable()Z
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lorg/droidparts/inner/reader/LegacyReader;->supportFragmentsReader:Lorg/droidparts/inner/reader/LegacyReader$ISupportFragmentsReader;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static isSupportObject(Ljava/lang/Object;)Z
    .locals 1
    .param p0, "obj"    # Ljava/lang/Object;

    .prologue
    .line 44
    sget-object v0, Lorg/droidparts/inner/reader/LegacyReader;->supportFragmentsReader:Lorg/droidparts/inner/reader/LegacyReader$ISupportFragmentsReader;

    invoke-interface {v0, p0}, Lorg/droidparts/inner/reader/LegacyReader$ISupportFragmentsReader;->isSupportObject(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

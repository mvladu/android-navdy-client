.class public Lorg/droidparts/persist/serializer/JSONSerializer;
.super Lorg/droidparts/persist/serializer/AbstractSerializer;
.source "JSONSerializer.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelType:",
        "Lorg/droidparts/model/Model;",
        ">",
        "Lorg/droidparts/persist/serializer/AbstractSerializer",
        "<TModelType;",
        "Lorg/json/JSONObject;",
        "Lorg/json/JSONArray;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/Class;Landroid/content/Context;)V
    .locals 0
    .param p2, "ctx"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<TModelType;>;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    .line 45
    .local p0, "this":Lorg/droidparts/persist/serializer/JSONSerializer;, "Lorg/droidparts/persist/serializer/JSONSerializer<TModelType;>;"
    .local p1, "cls":Ljava/lang/Class;, "Ljava/lang/Class<TModelType;>;"
    invoke-direct {p0, p1, p2}, Lorg/droidparts/persist/serializer/AbstractSerializer;-><init>(Ljava/lang/Class;Landroid/content/Context;)V

    .line 46
    return-void
.end method

.method private readFromJSONAndSetFieldVal(Lorg/droidparts/model/Model;Lorg/droidparts/inner/ann/FieldSpec;Lorg/json/JSONObject;Ljava/lang/String;)V
    .locals 11
    .param p3, "obj"    # Lorg/json/JSONObject;
    .param p4, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TModelType;",
            "Lorg/droidparts/inner/ann/FieldSpec",
            "<",
            "Lorg/droidparts/inner/ann/serialize/JSONAnn;",
            ">;",
            "Lorg/json/JSONObject;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/droidparts/persist/serializer/JSONSerializer;, "Lorg/droidparts/persist/serializer/JSONSerializer<TModelType;>;"
    .local p1, "model":Lorg/droidparts/model/Model;, "TModelType;"
    .local p2, "spec":Lorg/droidparts/inner/ann/FieldSpec;, "Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/serialize/JSONAnn;>;"
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 126
    invoke-static {p4}, Lorg/droidparts/persist/serializer/JSONSerializer;->getNestedKeyParts(Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v1

    .line 127
    .local v1, "keyParts":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz v1, :cond_1

    .line 128
    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    .line 129
    .local v2, "subKey":Ljava/lang/String;
    invoke-virtual {p0, p3, v2}, Lorg/droidparts/persist/serializer/JSONSerializer;->hasNonNull(Lorg/json/JSONObject;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 130
    invoke-virtual {p3, v2}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    .line 131
    .local v3, "subObj":Lorg/json/JSONObject;
    iget-object v5, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v5, Ljava/lang/String;

    invoke-direct {p0, p1, p2, v3, v5}, Lorg/droidparts/persist/serializer/JSONSerializer;->readFromJSONAndSetFieldVal(Lorg/droidparts/model/Model;Lorg/droidparts/inner/ann/FieldSpec;Lorg/json/JSONObject;Ljava/lang/String;)V

    .line 155
    .end local v2    # "subKey":Ljava/lang/String;
    .end local v3    # "subObj":Lorg/json/JSONObject;
    :goto_0
    return-void

    .line 133
    .restart local v2    # "subKey":Ljava/lang/String;
    :cond_0
    invoke-static {p2}, Lorg/droidparts/persist/serializer/JSONSerializer;->throwIfNotOptional(Lorg/droidparts/inner/ann/FieldSpec;)V

    goto :goto_0

    .line 135
    .end local v2    # "subKey":Ljava/lang/String;
    :cond_1
    invoke-virtual {p3, p4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 137
    :try_start_0
    iget-object v5, p2, Lorg/droidparts/inner/ann/FieldSpec;->field:Ljava/lang/reflect/Field;

    invoke-virtual {v5}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v5

    iget-object v6, p2, Lorg/droidparts/inner/ann/FieldSpec;->componentType:Ljava/lang/Class;

    invoke-virtual {p0, v5, v6, p3, p4}, Lorg/droidparts/persist/serializer/JSONSerializer;->readFromJSON(Ljava/lang/Class;Ljava/lang/Class;Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    .line 139
    .local v4, "val":Ljava/lang/Object;
    sget-object v5, Lorg/json/JSONObject;->NULL:Ljava/lang/Object;

    invoke-virtual {v5, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 140
    iget-object v5, p2, Lorg/droidparts/inner/ann/FieldSpec;->field:Ljava/lang/reflect/Field;

    invoke-static {p1, v5, v4}, Lorg/droidparts/inner/ReflectionUtils;->setFieldVal(Ljava/lang/Object;Ljava/lang/reflect/Field;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 144
    .end local v4    # "val":Ljava/lang/Object;
    :catch_0
    move-exception v0

    .line 145
    .local v0, "e":Ljava/lang/Exception;
    iget-object v5, p2, Lorg/droidparts/inner/ann/FieldSpec;->ann:Lorg/droidparts/inner/ann/Ann;

    check-cast v5, Lorg/droidparts/inner/ann/serialize/JSONAnn;

    iget-boolean v5, v5, Lorg/droidparts/inner/ann/serialize/JSONAnn;->optional:Z

    if-eqz v5, :cond_3

    .line 146
    const-string v6, "Failed to deserialize \'%s\': %s."

    const/4 v5, 0x2

    new-array v7, v5, [Ljava/lang/Object;

    iget-object v5, p2, Lorg/droidparts/inner/ann/FieldSpec;->ann:Lorg/droidparts/inner/ann/Ann;

    check-cast v5, Lorg/droidparts/inner/ann/serialize/JSONAnn;

    iget-object v5, v5, Lorg/droidparts/inner/ann/serialize/JSONAnn;->key:Ljava/lang/String;

    aput-object v5, v7, v9

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v7, v10

    invoke-static {v6, v7}, Lorg/droidparts/util/L;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 142
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v4    # "val":Ljava/lang/Object;
    :cond_2
    :try_start_1
    const-string v6, "Received NULL \'%s\', skipping."

    const/4 v5, 0x1

    new-array v7, v5, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget-object v5, p2, Lorg/droidparts/inner/ann/FieldSpec;->ann:Lorg/droidparts/inner/ann/Ann;

    check-cast v5, Lorg/droidparts/inner/ann/serialize/JSONAnn;

    iget-object v5, v5, Lorg/droidparts/inner/ann/serialize/JSONAnn;->key:Ljava/lang/String;

    aput-object v5, v7, v8

    invoke-static {v6, v7}, Lorg/droidparts/util/L;->i(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 149
    .end local v4    # "val":Ljava/lang/Object;
    .restart local v0    # "e":Ljava/lang/Exception;
    :cond_3
    throw v0

    .line 153
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_4
    invoke-static {p2}, Lorg/droidparts/persist/serializer/JSONSerializer;->throwIfNotOptional(Lorg/droidparts/inner/ann/FieldSpec;)V

    goto :goto_0
.end method

.method private readFromModelAndPutToJSON(Lorg/droidparts/model/Model;Lorg/droidparts/inner/ann/FieldSpec;Lorg/json/JSONObject;Ljava/lang/String;)V
    .locals 10
    .param p3, "obj"    # Lorg/json/JSONObject;
    .param p4, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TModelType;",
            "Lorg/droidparts/inner/ann/FieldSpec",
            "<",
            "Lorg/droidparts/inner/ann/serialize/JSONAnn;",
            ">;",
            "Lorg/json/JSONObject;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 96
    .local p0, "this":Lorg/droidparts/persist/serializer/JSONSerializer;, "Lorg/droidparts/persist/serializer/JSONSerializer<TModelType;>;"
    .local p1, "item":Lorg/droidparts/model/Model;, "TModelType;"
    .local p2, "spec":Lorg/droidparts/inner/ann/FieldSpec;, "Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/serialize/JSONAnn;>;"
    invoke-static {p4}, Lorg/droidparts/persist/serializer/JSONSerializer;->getNestedKeyParts(Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v7

    .line 97
    .local v7, "keyParts":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz v7, :cond_1

    .line 98
    iget-object v8, v7, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v8, Ljava/lang/String;

    .line 100
    .local v8, "subKey":Ljava/lang/String;
    invoke-virtual {p0, p3, v8}, Lorg/droidparts/persist/serializer/JSONSerializer;->hasNonNull(Lorg/json/JSONObject;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 101
    invoke-virtual {p3, v8}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v9

    .line 106
    .local v9, "subObj":Lorg/json/JSONObject;
    :goto_0
    iget-object v0, v7, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, p1, p2, v9, v0}, Lorg/droidparts/persist/serializer/JSONSerializer;->readFromModelAndPutToJSON(Lorg/droidparts/model/Model;Lorg/droidparts/inner/ann/FieldSpec;Lorg/json/JSONObject;Ljava/lang/String;)V

    .line 121
    .end local v8    # "subKey":Ljava/lang/String;
    .end local v9    # "subObj":Lorg/json/JSONObject;
    :goto_1
    return-void

    .line 103
    .restart local v8    # "subKey":Ljava/lang/String;
    :cond_0
    new-instance v9, Lorg/json/JSONObject;

    invoke-direct {v9}, Lorg/json/JSONObject;-><init>()V

    .line 104
    .restart local v9    # "subObj":Lorg/json/JSONObject;
    invoke-virtual {p3, v8, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_0

    .line 108
    .end local v8    # "subKey":Ljava/lang/String;
    .end local v9    # "subObj":Lorg/json/JSONObject;
    :cond_1
    iget-object v0, p2, Lorg/droidparts/inner/ann/FieldSpec;->field:Ljava/lang/reflect/Field;

    invoke-static {p1, v0}, Lorg/droidparts/inner/ReflectionUtils;->getFieldVal(Ljava/lang/Object;Ljava/lang/reflect/Field;)Ljava/lang/Object;

    move-result-object v5

    .line 110
    .local v5, "columnVal":Ljava/lang/Object;
    :try_start_0
    iget-object v0, p2, Lorg/droidparts/inner/ann/FieldSpec;->field:Ljava/lang/reflect/Field;

    invoke-virtual {v0}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v3

    iget-object v4, p2, Lorg/droidparts/inner/ann/FieldSpec;->componentType:Ljava/lang/Class;

    move-object v0, p0

    move-object v1, p3

    move-object v2, p4

    invoke-virtual/range {v0 .. v5}, Lorg/droidparts/persist/serializer/JSONSerializer;->putToJSON(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 112
    :catch_0
    move-exception v6

    .line 113
    .local v6, "e":Ljava/lang/Exception;
    iget-object v0, p2, Lorg/droidparts/inner/ann/FieldSpec;->ann:Lorg/droidparts/inner/ann/Ann;

    check-cast v0, Lorg/droidparts/inner/ann/serialize/JSONAnn;

    iget-boolean v0, v0, Lorg/droidparts/inner/ann/serialize/JSONAnn;->optional:Z

    if-eqz v0, :cond_2

    .line 114
    const-string v0, "Failded to serialize %s.%s: %s."

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lorg/droidparts/persist/serializer/JSONSerializer;->cls:Ljava/lang/Class;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p2, Lorg/droidparts/inner/ann/FieldSpec;->field:Ljava/lang/reflect/Field;

    invoke-virtual {v3}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-virtual {v6}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lorg/droidparts/util/L;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 117
    :cond_2
    throw v6
.end method

.method private static throwIfNotOptional(Lorg/droidparts/inner/ann/FieldSpec;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/droidparts/inner/ann/FieldSpec",
            "<",
            "Lorg/droidparts/inner/ann/serialize/JSONAnn;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 182
    .local p0, "spec":Lorg/droidparts/inner/ann/FieldSpec;, "Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/serialize/JSONAnn;>;"
    iget-object v0, p0, Lorg/droidparts/inner/ann/FieldSpec;->ann:Lorg/droidparts/inner/ann/Ann;

    check-cast v0, Lorg/droidparts/inner/ann/serialize/JSONAnn;

    iget-boolean v0, v0, Lorg/droidparts/inner/ann/serialize/JSONAnn;->optional:Z

    if-nez v0, :cond_0

    .line 183
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Required key \'%s\' not present."

    const/4 v0, 0x1

    new-array v3, v0, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v0, p0, Lorg/droidparts/inner/ann/FieldSpec;->ann:Lorg/droidparts/inner/ann/Ann;

    check-cast v0, Lorg/droidparts/inner/ann/serialize/JSONAnn;

    iget-object v0, v0, Lorg/droidparts/inner/ann/serialize/JSONAnn;->key:Ljava/lang/String;

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 186
    :cond_0
    return-void
.end method


# virtual methods
.method public bridge synthetic deserialize(Ljava/lang/Object;)Lorg/droidparts/model/Model;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 41
    .local p0, "this":Lorg/droidparts/persist/serializer/JSONSerializer;, "Lorg/droidparts/persist/serializer/JSONSerializer<TModelType;>;"
    check-cast p1, Lorg/json/JSONObject;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/droidparts/persist/serializer/JSONSerializer;->deserialize(Lorg/json/JSONObject;)Lorg/droidparts/model/Model;

    move-result-object v0

    return-object v0
.end method

.method public deserialize(Lorg/json/JSONObject;)Lorg/droidparts/model/Model;
    .locals 7
    .param p1, "obj"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            ")TModelType;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 59
    .local p0, "this":Lorg/droidparts/persist/serializer/JSONSerializer;, "Lorg/droidparts/persist/serializer/JSONSerializer<TModelType;>;"
    iget-object v6, p0, Lorg/droidparts/persist/serializer/JSONSerializer;->cls:Ljava/lang/Class;

    invoke-static {v6}, Lorg/droidparts/inner/ReflectionUtils;->newInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/droidparts/model/Model;

    .line 60
    .local v4, "model":Lorg/droidparts/model/Model;, "TModelType;"
    iget-object v6, p0, Lorg/droidparts/persist/serializer/JSONSerializer;->cls:Ljava/lang/Class;

    invoke-static {v6}, Lorg/droidparts/inner/ClassSpecRegistry;->getJSONSpecs(Ljava/lang/Class;)[Lorg/droidparts/inner/ann/FieldSpec;

    move-result-object v2

    .line 61
    .local v2, "jsonSpecs":[Lorg/droidparts/inner/ann/FieldSpec;, "[Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/serialize/JSONAnn;>;"
    move-object v0, v2

    .local v0, "arr$":[Lorg/droidparts/inner/ann/FieldSpec;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v5, v0, v1

    .line 62
    .local v5, "spec":Lorg/droidparts/inner/ann/FieldSpec;, "Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/serialize/JSONAnn;>;"
    iget-object v6, v5, Lorg/droidparts/inner/ann/FieldSpec;->ann:Lorg/droidparts/inner/ann/Ann;

    check-cast v6, Lorg/droidparts/inner/ann/serialize/JSONAnn;

    iget-object v6, v6, Lorg/droidparts/inner/ann/serialize/JSONAnn;->key:Ljava/lang/String;

    invoke-direct {p0, v4, v5, p1, v6}, Lorg/droidparts/persist/serializer/JSONSerializer;->readFromJSONAndSetFieldVal(Lorg/droidparts/model/Model;Lorg/droidparts/inner/ann/FieldSpec;Lorg/json/JSONObject;Ljava/lang/String;)V

    .line 61
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 64
    .end local v5    # "spec":Lorg/droidparts/inner/ann/FieldSpec;, "Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/serialize/JSONAnn;>;"
    :cond_0
    return-object v4
.end method

.method public bridge synthetic deserializeAll(Ljava/lang/Object;)Ljava/util/ArrayList;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 41
    .local p0, "this":Lorg/droidparts/persist/serializer/JSONSerializer;, "Lorg/droidparts/persist/serializer/JSONSerializer<TModelType;>;"
    check-cast p1, Lorg/json/JSONArray;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/droidparts/persist/serializer/JSONSerializer;->deserializeAll(Lorg/json/JSONArray;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public deserializeAll(Lorg/json/JSONArray;)Ljava/util/ArrayList;
    .locals 3
    .param p1, "arr"    # Lorg/json/JSONArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONArray;",
            ")",
            "Ljava/util/ArrayList",
            "<TModelType;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 77
    .local p0, "this":Lorg/droidparts/persist/serializer/JSONSerializer;, "Lorg/droidparts/persist/serializer/JSONSerializer<TModelType;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 78
    .local v1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TModelType;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 79
    invoke-virtual {p1, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {p0, v2}, Lorg/droidparts/persist/serializer/JSONSerializer;->deserialize(Lorg/json/JSONObject;)Lorg/droidparts/model/Model;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 78
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 81
    :cond_0
    return-object v1
.end method

.method protected final hasNonNull(Lorg/json/JSONObject;Ljava/lang/String;)Z
    .locals 1
    .param p1, "obj"    # Lorg/json/JSONObject;
    .param p2, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 88
    .local p0, "this":Lorg/droidparts/persist/serializer/JSONSerializer;, "Lorg/droidparts/persist/serializer/JSONSerializer<TModelType;>;"
    invoke-static {p1, p2}, Lorg/droidparts/inner/PersistUtils;->hasNonNull(Lorg/json/JSONObject;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method protected putToJSON(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/Object;)V
    .locals 6
    .param p1, "obj"    # Lorg/json/JSONObject;
    .param p2, "key"    # Ljava/lang/String;
    .param p5, "val"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/json/JSONObject;",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 172
    .local p0, "this":Lorg/droidparts/persist/serializer/JSONSerializer;, "Lorg/droidparts/persist/serializer/JSONSerializer<TModelType;>;"
    .local p3, "valType":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    .local p4, "componentType":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    if-nez p5, :cond_0

    .line 173
    sget-object v1, Lorg/json/JSONObject;->NULL:Ljava/lang/Object;

    invoke-virtual {p1, p2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 178
    :goto_0
    return-void

    .line 175
    :cond_0
    invoke-static {p3}, Lorg/droidparts/inner/ConverterRegistry;->getConverter(Ljava/lang/Class;)Lorg/droidparts/inner/converter/Converter;

    move-result-object v0

    .local v0, "converter":Lorg/droidparts/inner/converter/Converter;, "Lorg/droidparts/inner/converter/Converter<TT;>;"
    move-object v1, p3

    move-object v2, p4

    move-object v3, p1

    move-object v4, p2

    move-object v5, p5

    .line 176
    invoke-virtual/range {v0 .. v5}, Lorg/droidparts/inner/converter/Converter;->putToJSON(Ljava/lang/Class;Ljava/lang/Class;Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected readFromJSON(Ljava/lang/Class;Ljava/lang/Class;Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/Object;
    .locals 3
    .param p3, "obj"    # Lorg/json/JSONObject;
    .param p4, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/Class",
            "<TV;>;",
            "Lorg/json/JSONObject;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/Object;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 160
    .local p0, "this":Lorg/droidparts/persist/serializer/JSONSerializer;, "Lorg/droidparts/persist/serializer/JSONSerializer<TModelType;>;"
    .local p1, "valType":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    .local p2, "componentType":Ljava/lang/Class;, "Ljava/lang/Class<TV;>;"
    invoke-virtual {p3, p4}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 161
    .local v1, "jsonVal":Ljava/lang/Object;
    sget-object v2, Lorg/json/JSONObject;->NULL:Ljava/lang/Object;

    invoke-virtual {v2, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 165
    .end local v1    # "jsonVal":Ljava/lang/Object;
    :goto_0
    return-object v1

    .line 164
    .restart local v1    # "jsonVal":Ljava/lang/Object;
    :cond_0
    invoke-static {p1}, Lorg/droidparts/inner/ConverterRegistry;->getConverter(Ljava/lang/Class;)Lorg/droidparts/inner/converter/Converter;

    move-result-object v0

    .line 165
    .local v0, "converter":Lorg/droidparts/inner/converter/Converter;, "Lorg/droidparts/inner/converter/Converter<TT;>;"
    invoke-virtual {v0, p1, p2, p3, p4}, Lorg/droidparts/inner/converter/Converter;->readFromJSON(Ljava/lang/Class;Ljava/lang/Class;Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    goto :goto_0
.end method

.method public serialize(Lorg/droidparts/model/Model;)Lorg/json/JSONObject;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TModelType;)",
            "Lorg/json/JSONObject;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 49
    .local p0, "this":Lorg/droidparts/persist/serializer/JSONSerializer;, "Lorg/droidparts/persist/serializer/JSONSerializer<TModelType;>;"
    .local p1, "item":Lorg/droidparts/model/Model;, "TModelType;"
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    .line 50
    .local v4, "obj":Lorg/json/JSONObject;
    iget-object v6, p0, Lorg/droidparts/persist/serializer/JSONSerializer;->cls:Ljava/lang/Class;

    invoke-static {v6}, Lorg/droidparts/inner/ClassSpecRegistry;->getJSONSpecs(Ljava/lang/Class;)[Lorg/droidparts/inner/ann/FieldSpec;

    move-result-object v2

    .line 51
    .local v2, "jsonSpecs":[Lorg/droidparts/inner/ann/FieldSpec;, "[Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/serialize/JSONAnn;>;"
    move-object v0, v2

    .local v0, "arr$":[Lorg/droidparts/inner/ann/FieldSpec;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v5, v0, v1

    .line 52
    .local v5, "spec":Lorg/droidparts/inner/ann/FieldSpec;, "Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/serialize/JSONAnn;>;"
    iget-object v6, v5, Lorg/droidparts/inner/ann/FieldSpec;->ann:Lorg/droidparts/inner/ann/Ann;

    check-cast v6, Lorg/droidparts/inner/ann/serialize/JSONAnn;

    iget-object v6, v6, Lorg/droidparts/inner/ann/serialize/JSONAnn;->key:Ljava/lang/String;

    invoke-direct {p0, p1, v5, v4, v6}, Lorg/droidparts/persist/serializer/JSONSerializer;->readFromModelAndPutToJSON(Lorg/droidparts/model/Model;Lorg/droidparts/inner/ann/FieldSpec;Lorg/json/JSONObject;Ljava/lang/String;)V

    .line 51
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 54
    .end local v5    # "spec":Lorg/droidparts/inner/ann/FieldSpec;, "Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/serialize/JSONAnn;>;"
    :cond_0
    return-object v4
.end method

.method public serializeAll(Ljava/util/Collection;)Lorg/json/JSONArray;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<TModelType;>;)",
            "Lorg/json/JSONArray;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 68
    .local p0, "this":Lorg/droidparts/persist/serializer/JSONSerializer;, "Lorg/droidparts/persist/serializer/JSONSerializer<TModelType;>;"
    .local p1, "items":Ljava/util/Collection;, "Ljava/util/Collection<TModelType;>;"
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    .line 69
    .local v0, "arr":Lorg/json/JSONArray;
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/droidparts/model/Model;

    .line 70
    .local v2, "item":Lorg/droidparts/model/Model;, "TModelType;"
    invoke-virtual {p0, v2}, Lorg/droidparts/persist/serializer/JSONSerializer;->serialize(Lorg/droidparts/model/Model;)Lorg/json/JSONObject;

    move-result-object v3

    invoke-virtual {v0, v3}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_0

    .line 72
    .end local v2    # "item":Lorg/droidparts/model/Model;, "TModelType;"
    :cond_0
    return-object v0
.end method

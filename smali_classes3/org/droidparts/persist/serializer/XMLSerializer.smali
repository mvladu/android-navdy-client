.class public Lorg/droidparts/persist/serializer/XMLSerializer;
.super Lorg/droidparts/persist/serializer/AbstractSerializer;
.source "XMLSerializer.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelType:",
        "Lorg/droidparts/model/Model;",
        ">",
        "Lorg/droidparts/persist/serializer/AbstractSerializer",
        "<TModelType;",
        "Lorg/w3c/dom/Node;",
        "Lorg/w3c/dom/NodeList;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/Class;Landroid/content/Context;)V
    .locals 0
    .param p2, "ctx"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<TModelType;>;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    .line 56
    .local p0, "this":Lorg/droidparts/persist/serializer/XMLSerializer;, "Lorg/droidparts/persist/serializer/XMLSerializer<TModelType;>;"
    .local p1, "cls":Ljava/lang/Class;, "Ljava/lang/Class<TModelType;>;"
    invoke-direct {p0, p1, p2}, Lorg/droidparts/persist/serializer/AbstractSerializer;-><init>(Ljava/lang/Class;Landroid/content/Context;)V

    .line 57
    return-void
.end method

.method private static getChildNode(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;
    .locals 4
    .param p0, "tagNode"    # Lorg/w3c/dom/Node;
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 156
    invoke-interface {p0}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v1

    .line 157
    .local v1, "childTags":Lorg/w3c/dom/NodeList;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-interface {v1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 158
    invoke-interface {v1, v2}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 159
    .local v0, "childTag":Lorg/w3c/dom/Node;
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 163
    .end local v0    # "childTag":Lorg/w3c/dom/Node;
    :goto_1
    return-object v0

    .line 157
    .restart local v0    # "childTag":Lorg/w3c/dom/Node;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 163
    .end local v0    # "childTag":Lorg/w3c/dom/Node;
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static gotAttributeNode(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;
    .locals 4
    .param p0, "tagNode"    # Lorg/w3c/dom/Node;
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 143
    invoke-interface {p0}, Lorg/w3c/dom/Node;->getAttributes()Lorg/w3c/dom/NamedNodeMap;

    move-result-object v1

    .line 144
    .local v1, "attrs":Lorg/w3c/dom/NamedNodeMap;
    if-eqz v1, :cond_1

    .line 145
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-interface {v1}, Lorg/w3c/dom/NamedNodeMap;->getLength()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 146
    invoke-interface {v1, v2}, Lorg/w3c/dom/NamedNodeMap;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 147
    .local v0, "attr":Lorg/w3c/dom/Node;
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 152
    .end local v0    # "attr":Lorg/w3c/dom/Node;
    .end local v2    # "i":I
    :goto_1
    return-object v0

    .line 145
    .restart local v0    # "attr":Lorg/w3c/dom/Node;
    .restart local v2    # "i":I
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 152
    .end local v0    # "attr":Lorg/w3c/dom/Node;
    .end local v2    # "i":I
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static parseDocument(Ljava/lang/String;)Lorg/w3c/dom/Document;
    .locals 3
    .param p0, "xml"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljavax/xml/parsers/ParserConfigurationException;,
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 51
    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v0

    invoke-virtual {v0}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v0

    new-instance v1, Lorg/xml/sax/InputSource;

    new-instance v2, Ljava/io/StringReader;

    invoke-direct {v2, p0}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/Reader;)V

    invoke-virtual {v0, v1}, Ljavax/xml/parsers/DocumentBuilder;->parse(Lorg/xml/sax/InputSource;)Lorg/w3c/dom/Document;

    move-result-object v0

    return-object v0
.end method

.method private readFromXMLAndSetFieldVal(Ljava/lang/Object;Lorg/droidparts/inner/ann/FieldSpec;Lorg/w3c/dom/Node;Ljava/lang/String;Ljava/lang/String;)V
    .locals 17
    .param p1, "obj"    # Ljava/lang/Object;
    .param p3, "node"    # Lorg/w3c/dom/Node;
    .param p4, "tag"    # Ljava/lang/String;
    .param p5, "attribute"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Lorg/droidparts/inner/ann/FieldSpec",
            "<",
            "Lorg/droidparts/inner/ann/serialize/XMLAnn;",
            ">;",
            "Lorg/w3c/dom/Node;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 82
    .local p0, "this":Lorg/droidparts/persist/serializer/XMLSerializer;, "Lorg/droidparts/persist/serializer/XMLSerializer<TModelType;>;"
    .local p2, "spec":Lorg/droidparts/inner/ann/FieldSpec;, "Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/serialize/XMLAnn;>;"
    invoke-static/range {p4 .. p4}, Lorg/droidparts/persist/serializer/XMLSerializer;->getNestedKeyParts(Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v13

    .line 83
    .local v13, "keyParts":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz v13, :cond_2

    .line 84
    iget-object v14, v13, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v14, Ljava/lang/String;

    .line 85
    .local v14, "subKey":Ljava/lang/String;
    move-object/from16 v0, p3

    invoke-static {v0, v14}, Lorg/droidparts/persist/serializer/XMLSerializer;->getChildNode(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v5

    .line 86
    .local v5, "childTag":Lorg/w3c/dom/Node;
    if-eqz v5, :cond_1

    .line 87
    iget-object v6, v13, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v6, Ljava/lang/String;

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v7, p5

    invoke-direct/range {v2 .. v7}, Lorg/droidparts/persist/serializer/XMLSerializer;->readFromXMLAndSetFieldVal(Ljava/lang/Object;Lorg/droidparts/inner/ann/FieldSpec;Lorg/w3c/dom/Node;Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    .end local v5    # "childTag":Lorg/w3c/dom/Node;
    .end local v14    # "subKey":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 90
    .restart local v5    # "childTag":Lorg/w3c/dom/Node;
    .restart local v14    # "subKey":Ljava/lang/String;
    :cond_1
    invoke-static/range {p2 .. p2}, Lorg/droidparts/persist/serializer/XMLSerializer;->throwIfNotOptional(Lorg/droidparts/inner/ann/FieldSpec;)V

    goto :goto_0

    .line 93
    .end local v5    # "childTag":Lorg/w3c/dom/Node;
    .end local v14    # "subKey":Ljava/lang/String;
    :cond_2
    move-object/from16 v0, p2

    iget-object v2, v0, Lorg/droidparts/inner/ann/FieldSpec;->field:Ljava/lang/reflect/Field;

    invoke-virtual {v2}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p4

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-interface/range {p3 .. p3}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p4

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    :cond_3
    const/4 v11, 0x1

    .line 95
    .local v11, "defaultOrSameTag":Z
    :goto_1
    move-object/from16 v0, p2

    iget-object v2, v0, Lorg/droidparts/inner/ann/FieldSpec;->componentType:Ljava/lang/Class;

    if-nez v2, :cond_4

    invoke-static/range {p5 .. p5}, Lorg/droidparts/util/Strings;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 96
    invoke-interface/range {p3 .. p3}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p4

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 97
    invoke-static/range {p3 .. p4}, Lorg/droidparts/persist/serializer/XMLSerializer;->getChildNode(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v10

    .line 98
    .local v10, "child":Lorg/w3c/dom/Node;
    if-eqz v10, :cond_8

    .line 99
    move-object/from16 p3, v10

    .line 105
    .end local v10    # "child":Lorg/w3c/dom/Node;
    :cond_4
    :goto_2
    invoke-static/range {p5 .. p5}, Lorg/droidparts/util/Strings;->isNotEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_9

    move-object/from16 v2, p5

    :goto_3
    move-object/from16 v0, p3

    invoke-static {v0, v2}, Lorg/droidparts/persist/serializer/XMLSerializer;->gotAttributeNode(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v8

    .line 107
    .local v8, "attrNode":Lorg/w3c/dom/Node;
    invoke-static/range {p3 .. p4}, Lorg/droidparts/persist/serializer/XMLSerializer;->getChildNode(Lorg/w3c/dom/Node;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v15

    .line 108
    .local v15, "tagNode":Lorg/w3c/dom/Node;
    if-nez v15, :cond_5

    if-eqz v11, :cond_5

    .line 109
    move-object/from16 v15, p3

    .line 111
    :cond_5
    if-nez v8, :cond_6

    if-eqz v15, :cond_b

    .line 113
    :cond_6
    if-eqz v8, :cond_a

    .line 114
    :try_start_0
    move-object/from16 v0, p2

    iget-object v2, v0, Lorg/droidparts/inner/ann/FieldSpec;->field:Ljava/lang/reflect/Field;

    invoke-virtual {v2}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v2

    move-object/from16 v0, p2

    iget-object v3, v0, Lorg/droidparts/inner/ann/FieldSpec;->componentType:Ljava/lang/Class;

    move-object/from16 v0, p0

    move-object/from16 v1, p5

    invoke-virtual {v0, v2, v3, v8, v1}, Lorg/droidparts/persist/serializer/XMLSerializer;->getNodeVal(Ljava/lang/Class;Ljava/lang/Class;Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    .line 116
    .local v9, "attrVal":Ljava/lang/Object;
    move-object/from16 v0, p2

    iget-object v2, v0, Lorg/droidparts/inner/ann/FieldSpec;->field:Ljava/lang/reflect/Field;

    move-object/from16 v0, p1

    invoke-static {v0, v2, v9}, Lorg/droidparts/inner/ReflectionUtils;->setFieldVal(Ljava/lang/Object;Ljava/lang/reflect/Field;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 122
    .end local v9    # "attrVal":Ljava/lang/Object;
    :catch_0
    move-exception v12

    .line 124
    .local v12, "e":Ljava/lang/Exception;
    const-string v3, "Failed to deserialize \'%s\': %s."

    const/4 v2, 0x2

    new-array v4, v2, [Ljava/lang/Object;

    const/4 v6, 0x0

    move-object/from16 v0, p2

    iget-object v2, v0, Lorg/droidparts/inner/ann/FieldSpec;->ann:Lorg/droidparts/inner/ann/Ann;

    check-cast v2, Lorg/droidparts/inner/ann/serialize/XMLAnn;

    iget-object v2, v2, Lorg/droidparts/inner/ann/serialize/XMLAnn;->tag:Ljava/lang/String;

    aput-object v2, v4, v6

    const/4 v2, 0x1

    invoke-virtual {v12}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v2

    invoke-static {v3, v4}, Lorg/droidparts/util/L;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 126
    invoke-static/range {p2 .. p2}, Lorg/droidparts/persist/serializer/XMLSerializer;->throwIfNotOptional(Lorg/droidparts/inner/ann/FieldSpec;)V

    goto/16 :goto_0

    .line 93
    .end local v8    # "attrNode":Lorg/w3c/dom/Node;
    .end local v11    # "defaultOrSameTag":Z
    .end local v12    # "e":Ljava/lang/Exception;
    .end local v15    # "tagNode":Lorg/w3c/dom/Node;
    :cond_7
    const/4 v11, 0x0

    goto :goto_1

    .line 100
    .restart local v10    # "child":Lorg/w3c/dom/Node;
    .restart local v11    # "defaultOrSameTag":Z
    :cond_8
    if-nez v11, :cond_4

    .line 101
    invoke-static/range {p2 .. p2}, Lorg/droidparts/persist/serializer/XMLSerializer;->throwIfNotOptional(Lorg/droidparts/inner/ann/FieldSpec;)V

    goto :goto_2

    .end local v10    # "child":Lorg/w3c/dom/Node;
    :cond_9
    move-object/from16 v2, p4

    .line 105
    goto :goto_3

    .line 117
    .restart local v8    # "attrNode":Lorg/w3c/dom/Node;
    .restart local v15    # "tagNode":Lorg/w3c/dom/Node;
    :cond_a
    if-eqz v15, :cond_0

    .line 118
    :try_start_1
    move-object/from16 v0, p2

    iget-object v2, v0, Lorg/droidparts/inner/ann/FieldSpec;->field:Ljava/lang/reflect/Field;

    invoke-virtual {v2}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v2

    move-object/from16 v0, p2

    iget-object v3, v0, Lorg/droidparts/inner/ann/FieldSpec;->componentType:Ljava/lang/Class;

    move-object/from16 v0, p0

    move-object/from16 v1, p5

    invoke-virtual {v0, v2, v3, v15, v1}, Lorg/droidparts/persist/serializer/XMLSerializer;->getNodeVal(Ljava/lang/Class;Ljava/lang/Class;Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v16

    .line 120
    .local v16, "tagVal":Ljava/lang/Object;
    move-object/from16 v0, p2

    iget-object v2, v0, Lorg/droidparts/inner/ann/FieldSpec;->field:Ljava/lang/reflect/Field;

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v2, v1}, Lorg/droidparts/inner/ReflectionUtils;->setFieldVal(Ljava/lang/Object;Ljava/lang/reflect/Field;Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 129
    .end local v16    # "tagVal":Ljava/lang/Object;
    :cond_b
    invoke-static/range {p2 .. p2}, Lorg/droidparts/persist/serializer/XMLSerializer;->throwIfNotOptional(Lorg/droidparts/inner/ann/FieldSpec;)V

    goto/16 :goto_0
.end method

.method private static throwIfNotOptional(Lorg/droidparts/inner/ann/FieldSpec;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/droidparts/inner/ann/FieldSpec",
            "<",
            "Lorg/droidparts/inner/ann/serialize/XMLAnn;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 168
    .local p0, "spec":Lorg/droidparts/inner/ann/FieldSpec;, "Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/serialize/XMLAnn;>;"
    iget-object v0, p0, Lorg/droidparts/inner/ann/FieldSpec;->ann:Lorg/droidparts/inner/ann/Ann;

    check-cast v0, Lorg/droidparts/inner/ann/serialize/XMLAnn;

    iget-boolean v0, v0, Lorg/droidparts/inner/ann/serialize/XMLAnn;->optional:Z

    if-nez v0, :cond_0

    .line 169
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Required tag \'%s\' or attribute \'%s\' not present."

    const/4 v0, 0x2

    new-array v3, v0, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v0, p0, Lorg/droidparts/inner/ann/FieldSpec;->ann:Lorg/droidparts/inner/ann/Ann;

    check-cast v0, Lorg/droidparts/inner/ann/serialize/XMLAnn;

    iget-object v0, v0, Lorg/droidparts/inner/ann/serialize/XMLAnn;->tag:Ljava/lang/String;

    aput-object v0, v3, v4

    const/4 v4, 0x1

    iget-object v0, p0, Lorg/droidparts/inner/ann/FieldSpec;->ann:Lorg/droidparts/inner/ann/Ann;

    check-cast v0, Lorg/droidparts/inner/ann/serialize/XMLAnn;

    iget-object v0, v0, Lorg/droidparts/inner/ann/serialize/XMLAnn;->attribute:Ljava/lang/String;

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 173
    :cond_0
    return-void
.end method


# virtual methods
.method public bridge synthetic deserialize(Ljava/lang/Object;)Lorg/droidparts/model/Model;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 46
    .local p0, "this":Lorg/droidparts/persist/serializer/XMLSerializer;, "Lorg/droidparts/persist/serializer/XMLSerializer<TModelType;>;"
    check-cast p1, Lorg/w3c/dom/Node;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/droidparts/persist/serializer/XMLSerializer;->deserialize(Lorg/w3c/dom/Node;)Lorg/droidparts/model/Model;

    move-result-object v0

    return-object v0
.end method

.method public deserialize(Lorg/w3c/dom/Node;)Lorg/droidparts/model/Model;
    .locals 10
    .param p1, "node"    # Lorg/w3c/dom/Node;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/w3c/dom/Node;",
            ")TModelType;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 61
    .local p0, "this":Lorg/droidparts/persist/serializer/XMLSerializer;, "Lorg/droidparts/persist/serializer/XMLSerializer<TModelType;>;"
    iget-object v0, p0, Lorg/droidparts/persist/serializer/XMLSerializer;->cls:Ljava/lang/Class;

    invoke-static {v0}, Lorg/droidparts/inner/ReflectionUtils;->newInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/droidparts/model/Model;

    .line 62
    .local v1, "model":Lorg/droidparts/model/Model;, "TModelType;"
    iget-object v0, p0, Lorg/droidparts/persist/serializer/XMLSerializer;->cls:Ljava/lang/Class;

    invoke-static {v0}, Lorg/droidparts/inner/ClassSpecRegistry;->getXMLSpecs(Ljava/lang/Class;)[Lorg/droidparts/inner/ann/FieldSpec;

    move-result-object v9

    .line 63
    .local v9, "xmlSpecs":[Lorg/droidparts/inner/ann/FieldSpec;, "[Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/serialize/XMLAnn;>;"
    move-object v6, v9

    .local v6, "arr$":[Lorg/droidparts/inner/ann/FieldSpec;
    array-length v8, v6

    .local v8, "len$":I
    const/4 v7, 0x0

    .local v7, "i$":I
    :goto_0
    if-ge v7, v8, :cond_0

    aget-object v2, v6, v7

    .line 64
    .local v2, "spec":Lorg/droidparts/inner/ann/FieldSpec;, "Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/serialize/XMLAnn;>;"
    iget-object v0, v2, Lorg/droidparts/inner/ann/FieldSpec;->ann:Lorg/droidparts/inner/ann/Ann;

    check-cast v0, Lorg/droidparts/inner/ann/serialize/XMLAnn;

    iget-object v4, v0, Lorg/droidparts/inner/ann/serialize/XMLAnn;->tag:Ljava/lang/String;

    iget-object v0, v2, Lorg/droidparts/inner/ann/FieldSpec;->ann:Lorg/droidparts/inner/ann/Ann;

    check-cast v0, Lorg/droidparts/inner/ann/serialize/XMLAnn;

    iget-object v5, v0, Lorg/droidparts/inner/ann/serialize/XMLAnn;->attribute:Ljava/lang/String;

    move-object v0, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lorg/droidparts/persist/serializer/XMLSerializer;->readFromXMLAndSetFieldVal(Ljava/lang/Object;Lorg/droidparts/inner/ann/FieldSpec;Lorg/w3c/dom/Node;Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 67
    .end local v2    # "spec":Lorg/droidparts/inner/ann/FieldSpec;, "Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/serialize/XMLAnn;>;"
    :cond_0
    return-object v1
.end method

.method public bridge synthetic deserializeAll(Ljava/lang/Object;)Ljava/util/ArrayList;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 46
    .local p0, "this":Lorg/droidparts/persist/serializer/XMLSerializer;, "Lorg/droidparts/persist/serializer/XMLSerializer<TModelType;>;"
    check-cast p1, Lorg/w3c/dom/NodeList;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/droidparts/persist/serializer/XMLSerializer;->deserializeAll(Lorg/w3c/dom/NodeList;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public deserializeAll(Lorg/w3c/dom/NodeList;)Ljava/util/ArrayList;
    .locals 3
    .param p1, "nodeList"    # Lorg/w3c/dom/NodeList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/w3c/dom/NodeList;",
            ")",
            "Ljava/util/ArrayList",
            "<TModelType;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 73
    .local p0, "this":Lorg/droidparts/persist/serializer/XMLSerializer;, "Lorg/droidparts/persist/serializer/XMLSerializer<TModelType;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 74
    .local v1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TModelType;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {p1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 75
    invoke-interface {p1, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v2

    invoke-virtual {p0, v2}, Lorg/droidparts/persist/serializer/XMLSerializer;->deserialize(Lorg/w3c/dom/Node;)Lorg/droidparts/model/Model;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 74
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 77
    :cond_0
    return-object v1
.end method

.method protected getNodeVal(Ljava/lang/Class;Ljava/lang/Class;Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/Object;
    .locals 2
    .param p3, "node"    # Lorg/w3c/dom/Node;
    .param p4, "attribute"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/Class",
            "<TV;>;",
            "Lorg/w3c/dom/Node;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/Object;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 138
    .local p0, "this":Lorg/droidparts/persist/serializer/XMLSerializer;, "Lorg/droidparts/persist/serializer/XMLSerializer<TModelType;>;"
    .local p1, "valType":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    .local p2, "componentType":Ljava/lang/Class;, "Ljava/lang/Class<TV;>;"
    invoke-static {p1}, Lorg/droidparts/inner/ConverterRegistry;->getConverter(Ljava/lang/Class;)Lorg/droidparts/inner/converter/Converter;

    move-result-object v0

    .line 139
    .local v0, "converter":Lorg/droidparts/inner/converter/Converter;, "Lorg/droidparts/inner/converter/Converter<TT;>;"
    invoke-virtual {v0, p1, p2, p3, p4}, Lorg/droidparts/inner/converter/Converter;->readFromXML(Ljava/lang/Class;Ljava/lang/Class;Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    return-object v1
.end method

.class public abstract Lorg/droidparts/AbstractApplication;
.super Landroid/app/Application;
.source "AbstractApplication.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate()V
    .locals 1

    .prologue
    .line 26
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 27
    invoke-static {p0}, Lorg/droidparts/Injector;->setUp(Landroid/content/Context;)V

    .line 28
    invoke-static {p0, p0}, Lorg/droidparts/Injector;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 30
    const-string v0, "android.os.AsyncTask"

    invoke-static {v0}, Lorg/droidparts/inner/ReflectionUtils;->classForName(Ljava/lang/String;)Ljava/lang/Class;

    .line 31
    return-void
.end method

.method public onTerminate()V
    .locals 0

    .prologue
    .line 36
    invoke-static {}, Lorg/droidparts/Injector;->tearDown()V

    .line 37
    return-void
.end method

.method public final varargs registerConverters([Lorg/droidparts/inner/converter/Converter;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lorg/droidparts/inner/converter/Converter",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 40
    .local p1, "converters":[Lorg/droidparts/inner/converter/Converter;, "[Lorg/droidparts/inner/converter/Converter<*>;"
    move-object v0, p1

    .local v0, "arr$":[Lorg/droidparts/inner/converter/Converter;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 41
    .local v1, "converter":Lorg/droidparts/inner/converter/Converter;, "Lorg/droidparts/inner/converter/Converter<*>;"
    invoke-static {v1}, Lorg/droidparts/inner/ConverterRegistry;->registerConverter(Lorg/droidparts/inner/converter/Converter;)V

    .line 40
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 43
    .end local v1    # "converter":Lorg/droidparts/inner/converter/Converter;, "Lorg/droidparts/inner/converter/Converter<*>;"
    :cond_0
    return-void
.end method

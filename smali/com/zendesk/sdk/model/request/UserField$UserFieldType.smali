.class final enum Lcom/zendesk/sdk/model/request/UserField$UserFieldType;
.super Ljava/lang/Enum;
.source "UserField.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/sdk/model/request/UserField;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "UserFieldType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/zendesk/sdk/model/request/UserField$UserFieldType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/zendesk/sdk/model/request/UserField$UserFieldType;

.field public static final enum Checkbox:Lcom/zendesk/sdk/model/request/UserField$UserFieldType;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "checkbox"
    .end annotation
.end field

.field public static final enum Date:Lcom/zendesk/sdk/model/request/UserField$UserFieldType;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "date"
    .end annotation
.end field

.field public static final enum Decimal:Lcom/zendesk/sdk/model/request/UserField$UserFieldType;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "decimal"
    .end annotation
.end field

.field public static final enum Dropdown:Lcom/zendesk/sdk/model/request/UserField$UserFieldType;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "dropdown"
    .end annotation
.end field

.field public static final enum Integer:Lcom/zendesk/sdk/model/request/UserField$UserFieldType;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "integer"
    .end annotation
.end field

.field public static final enum Regexp:Lcom/zendesk/sdk/model/request/UserField$UserFieldType;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "regexp"
    .end annotation
.end field

.field public static final enum Text:Lcom/zendesk/sdk/model/request/UserField$UserFieldType;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "text"
    .end annotation
.end field

.field public static final enum Textarea:Lcom/zendesk/sdk/model/request/UserField$UserFieldType;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "textarea"
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 204
    new-instance v0, Lcom/zendesk/sdk/model/request/UserField$UserFieldType;

    const-string v1, "Integer"

    invoke-direct {v0, v1, v3}, Lcom/zendesk/sdk/model/request/UserField$UserFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/zendesk/sdk/model/request/UserField$UserFieldType;->Integer:Lcom/zendesk/sdk/model/request/UserField$UserFieldType;

    .line 207
    new-instance v0, Lcom/zendesk/sdk/model/request/UserField$UserFieldType;

    const-string v1, "Decimal"

    invoke-direct {v0, v1, v4}, Lcom/zendesk/sdk/model/request/UserField$UserFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/zendesk/sdk/model/request/UserField$UserFieldType;->Decimal:Lcom/zendesk/sdk/model/request/UserField$UserFieldType;

    .line 210
    new-instance v0, Lcom/zendesk/sdk/model/request/UserField$UserFieldType;

    const-string v1, "Checkbox"

    invoke-direct {v0, v1, v5}, Lcom/zendesk/sdk/model/request/UserField$UserFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/zendesk/sdk/model/request/UserField$UserFieldType;->Checkbox:Lcom/zendesk/sdk/model/request/UserField$UserFieldType;

    .line 213
    new-instance v0, Lcom/zendesk/sdk/model/request/UserField$UserFieldType;

    const-string v1, "Date"

    invoke-direct {v0, v1, v6}, Lcom/zendesk/sdk/model/request/UserField$UserFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/zendesk/sdk/model/request/UserField$UserFieldType;->Date:Lcom/zendesk/sdk/model/request/UserField$UserFieldType;

    .line 216
    new-instance v0, Lcom/zendesk/sdk/model/request/UserField$UserFieldType;

    const-string v1, "Text"

    invoke-direct {v0, v1, v7}, Lcom/zendesk/sdk/model/request/UserField$UserFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/zendesk/sdk/model/request/UserField$UserFieldType;->Text:Lcom/zendesk/sdk/model/request/UserField$UserFieldType;

    .line 219
    new-instance v0, Lcom/zendesk/sdk/model/request/UserField$UserFieldType;

    const-string v1, "Textarea"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/zendesk/sdk/model/request/UserField$UserFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/zendesk/sdk/model/request/UserField$UserFieldType;->Textarea:Lcom/zendesk/sdk/model/request/UserField$UserFieldType;

    .line 222
    new-instance v0, Lcom/zendesk/sdk/model/request/UserField$UserFieldType;

    const-string v1, "Dropdown"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/zendesk/sdk/model/request/UserField$UserFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/zendesk/sdk/model/request/UserField$UserFieldType;->Dropdown:Lcom/zendesk/sdk/model/request/UserField$UserFieldType;

    .line 225
    new-instance v0, Lcom/zendesk/sdk/model/request/UserField$UserFieldType;

    const-string v1, "Regexp"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/zendesk/sdk/model/request/UserField$UserFieldType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/zendesk/sdk/model/request/UserField$UserFieldType;->Regexp:Lcom/zendesk/sdk/model/request/UserField$UserFieldType;

    .line 202
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/zendesk/sdk/model/request/UserField$UserFieldType;

    sget-object v1, Lcom/zendesk/sdk/model/request/UserField$UserFieldType;->Integer:Lcom/zendesk/sdk/model/request/UserField$UserFieldType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/zendesk/sdk/model/request/UserField$UserFieldType;->Decimal:Lcom/zendesk/sdk/model/request/UserField$UserFieldType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/zendesk/sdk/model/request/UserField$UserFieldType;->Checkbox:Lcom/zendesk/sdk/model/request/UserField$UserFieldType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/zendesk/sdk/model/request/UserField$UserFieldType;->Date:Lcom/zendesk/sdk/model/request/UserField$UserFieldType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/zendesk/sdk/model/request/UserField$UserFieldType;->Text:Lcom/zendesk/sdk/model/request/UserField$UserFieldType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/zendesk/sdk/model/request/UserField$UserFieldType;->Textarea:Lcom/zendesk/sdk/model/request/UserField$UserFieldType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/zendesk/sdk/model/request/UserField$UserFieldType;->Dropdown:Lcom/zendesk/sdk/model/request/UserField$UserFieldType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/zendesk/sdk/model/request/UserField$UserFieldType;->Regexp:Lcom/zendesk/sdk/model/request/UserField$UserFieldType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/zendesk/sdk/model/request/UserField$UserFieldType;->$VALUES:[Lcom/zendesk/sdk/model/request/UserField$UserFieldType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 202
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/zendesk/sdk/model/request/UserField$UserFieldType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 202
    const-class v0, Lcom/zendesk/sdk/model/request/UserField$UserFieldType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/zendesk/sdk/model/request/UserField$UserFieldType;

    return-object v0
.end method

.method public static values()[Lcom/zendesk/sdk/model/request/UserField$UserFieldType;
    .locals 1

    .prologue
    .line 202
    sget-object v0, Lcom/zendesk/sdk/model/request/UserField$UserFieldType;->$VALUES:[Lcom/zendesk/sdk/model/request/UserField$UserFieldType;

    invoke-virtual {v0}, [Lcom/zendesk/sdk/model/request/UserField$UserFieldType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/zendesk/sdk/model/request/UserField$UserFieldType;

    return-object v0
.end method

.class public final Lcom/nimbusds/jose/jwk/RSAKey;
.super Lcom/nimbusds/jose/jwk/JWK;
.source "RSAKey.java"

# interfaces
.implements Lcom/nimbusds/jose/jwk/AssymetricJWK;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/nimbusds/jose/jwk/RSAKey$Builder;,
        Lcom/nimbusds/jose/jwk/RSAKey$OtherPrimesInfo;
    }
.end annotation

.annotation runtime Lnet/jcip/annotations/Immutable;
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final d:Lcom/nimbusds/jose/util/Base64URL;

.field private final dp:Lcom/nimbusds/jose/util/Base64URL;

.field private final dq:Lcom/nimbusds/jose/util/Base64URL;

.field private final e:Lcom/nimbusds/jose/util/Base64URL;

.field private final n:Lcom/nimbusds/jose/util/Base64URL;

.field private final oth:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/nimbusds/jose/jwk/RSAKey$OtherPrimesInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final p:Lcom/nimbusds/jose/util/Base64URL;

.field private final q:Lcom/nimbusds/jose/util/Base64URL;

.field private final qi:Lcom/nimbusds/jose/util/Base64URL;


# direct methods
.method public constructor <init>(Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/jwk/KeyUse;Ljava/util/Set;Lcom/nimbusds/jose/Algorithm;Ljava/lang/String;Ljava/net/URI;Lcom/nimbusds/jose/util/Base64URL;Ljava/util/List;)V
    .locals 17
    .param p1, "n"    # Lcom/nimbusds/jose/util/Base64URL;
    .param p2, "e"    # Lcom/nimbusds/jose/util/Base64URL;
    .param p3, "use"    # Lcom/nimbusds/jose/jwk/KeyUse;
    .param p5, "alg"    # Lcom/nimbusds/jose/Algorithm;
    .param p6, "kid"    # Ljava/lang/String;
    .param p7, "x5u"    # Ljava/net/URI;
    .param p8, "x5t"    # Lcom/nimbusds/jose/util/Base64URL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nimbusds/jose/util/Base64URL;",
            "Lcom/nimbusds/jose/util/Base64URL;",
            "Lcom/nimbusds/jose/jwk/KeyUse;",
            "Ljava/util/Set",
            "<",
            "Lcom/nimbusds/jose/jwk/KeyOperation;",
            ">;",
            "Lcom/nimbusds/jose/Algorithm;",
            "Ljava/lang/String;",
            "Ljava/net/URI;",
            "Lcom/nimbusds/jose/util/Base64URL;",
            "Ljava/util/List",
            "<",
            "Lcom/nimbusds/jose/util/Base64;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 916
    .local p4, "ops":Ljava/util/Set;, "Ljava/util/Set<Lcom/nimbusds/jose/jwk/KeyOperation;>;"
    .local p9, "x5c":Ljava/util/List;, "Ljava/util/List<Lcom/nimbusds/jose/util/Base64;>;"
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v10, p3

    move-object/from16 v11, p4

    move-object/from16 v12, p5

    move-object/from16 v13, p6

    move-object/from16 v14, p7

    move-object/from16 v15, p8

    move-object/from16 v16, p9

    .line 917
    invoke-direct/range {v0 .. v16}, Lcom/nimbusds/jose/jwk/RSAKey;-><init>(Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Ljava/util/List;Lcom/nimbusds/jose/jwk/KeyUse;Ljava/util/Set;Lcom/nimbusds/jose/Algorithm;Ljava/lang/String;Ljava/net/URI;Lcom/nimbusds/jose/util/Base64URL;Ljava/util/List;)V

    .line 918
    return-void
.end method

.method public constructor <init>(Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/jwk/KeyUse;Ljava/util/Set;Lcom/nimbusds/jose/Algorithm;Ljava/lang/String;Ljava/net/URI;Lcom/nimbusds/jose/util/Base64URL;Ljava/util/List;)V
    .locals 17
    .param p1, "n"    # Lcom/nimbusds/jose/util/Base64URL;
    .param p2, "e"    # Lcom/nimbusds/jose/util/Base64URL;
    .param p3, "d"    # Lcom/nimbusds/jose/util/Base64URL;
    .param p4, "use"    # Lcom/nimbusds/jose/jwk/KeyUse;
    .param p6, "alg"    # Lcom/nimbusds/jose/Algorithm;
    .param p7, "kid"    # Ljava/lang/String;
    .param p8, "x5u"    # Ljava/net/URI;
    .param p9, "x5t"    # Lcom/nimbusds/jose/util/Base64URL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nimbusds/jose/util/Base64URL;",
            "Lcom/nimbusds/jose/util/Base64URL;",
            "Lcom/nimbusds/jose/util/Base64URL;",
            "Lcom/nimbusds/jose/jwk/KeyUse;",
            "Ljava/util/Set",
            "<",
            "Lcom/nimbusds/jose/jwk/KeyOperation;",
            ">;",
            "Lcom/nimbusds/jose/Algorithm;",
            "Ljava/lang/String;",
            "Ljava/net/URI;",
            "Lcom/nimbusds/jose/util/Base64URL;",
            "Ljava/util/List",
            "<",
            "Lcom/nimbusds/jose/util/Base64;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 953
    .local p5, "ops":Ljava/util/Set;, "Ljava/util/Set<Lcom/nimbusds/jose/jwk/KeyOperation;>;"
    .local p10, "x5c":Ljava/util/List;, "Ljava/util/List<Lcom/nimbusds/jose/util/Base64;>;"
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v10, p4

    move-object/from16 v11, p5

    move-object/from16 v12, p6

    move-object/from16 v13, p7

    move-object/from16 v14, p8

    move-object/from16 v15, p9

    move-object/from16 v16, p10

    .line 954
    invoke-direct/range {v0 .. v16}, Lcom/nimbusds/jose/jwk/RSAKey;-><init>(Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Ljava/util/List;Lcom/nimbusds/jose/jwk/KeyUse;Ljava/util/Set;Lcom/nimbusds/jose/Algorithm;Ljava/lang/String;Ljava/net/URI;Lcom/nimbusds/jose/util/Base64URL;Ljava/util/List;)V

    .line 956
    if-nez p3, :cond_0

    .line 957
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The private exponent must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 959
    :cond_0
    return-void
.end method

.method public constructor <init>(Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Ljava/util/List;Lcom/nimbusds/jose/jwk/KeyUse;Ljava/util/Set;Lcom/nimbusds/jose/Algorithm;Ljava/lang/String;Ljava/net/URI;Lcom/nimbusds/jose/util/Base64URL;Ljava/util/List;)V
    .locals 10
    .param p1, "n"    # Lcom/nimbusds/jose/util/Base64URL;
    .param p2, "e"    # Lcom/nimbusds/jose/util/Base64URL;
    .param p3, "d"    # Lcom/nimbusds/jose/util/Base64URL;
    .param p4, "p"    # Lcom/nimbusds/jose/util/Base64URL;
    .param p5, "q"    # Lcom/nimbusds/jose/util/Base64URL;
    .param p6, "dp"    # Lcom/nimbusds/jose/util/Base64URL;
    .param p7, "dq"    # Lcom/nimbusds/jose/util/Base64URL;
    .param p8, "qi"    # Lcom/nimbusds/jose/util/Base64URL;
    .param p10, "use"    # Lcom/nimbusds/jose/jwk/KeyUse;
    .param p12, "alg"    # Lcom/nimbusds/jose/Algorithm;
    .param p13, "kid"    # Ljava/lang/String;
    .param p14, "x5u"    # Ljava/net/URI;
    .param p15, "x5t"    # Lcom/nimbusds/jose/util/Base64URL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nimbusds/jose/util/Base64URL;",
            "Lcom/nimbusds/jose/util/Base64URL;",
            "Lcom/nimbusds/jose/util/Base64URL;",
            "Lcom/nimbusds/jose/util/Base64URL;",
            "Lcom/nimbusds/jose/util/Base64URL;",
            "Lcom/nimbusds/jose/util/Base64URL;",
            "Lcom/nimbusds/jose/util/Base64URL;",
            "Lcom/nimbusds/jose/util/Base64URL;",
            "Ljava/util/List",
            "<",
            "Lcom/nimbusds/jose/jwk/RSAKey$OtherPrimesInfo;",
            ">;",
            "Lcom/nimbusds/jose/jwk/KeyUse;",
            "Ljava/util/Set",
            "<",
            "Lcom/nimbusds/jose/jwk/KeyOperation;",
            ">;",
            "Lcom/nimbusds/jose/Algorithm;",
            "Ljava/lang/String;",
            "Ljava/net/URI;",
            "Lcom/nimbusds/jose/util/Base64URL;",
            "Ljava/util/List",
            "<",
            "Lcom/nimbusds/jose/util/Base64;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1095
    .local p9, "oth":Ljava/util/List;, "Ljava/util/List<Lcom/nimbusds/jose/jwk/RSAKey$OtherPrimesInfo;>;"
    .local p11, "ops":Ljava/util/Set;, "Ljava/util/Set<Lcom/nimbusds/jose/jwk/KeyOperation;>;"
    .local p16, "x5c":Ljava/util/List;, "Ljava/util/List<Lcom/nimbusds/jose/util/Base64;>;"
    sget-object v2, Lcom/nimbusds/jose/jwk/KeyType;->RSA:Lcom/nimbusds/jose/jwk/KeyType;

    move-object v1, p0

    move-object/from16 v3, p10

    move-object/from16 v4, p11

    move-object/from16 v5, p12

    move-object/from16 v6, p13

    move-object/from16 v7, p14

    move-object/from16 v8, p15

    move-object/from16 v9, p16

    invoke-direct/range {v1 .. v9}, Lcom/nimbusds/jose/jwk/JWK;-><init>(Lcom/nimbusds/jose/jwk/KeyType;Lcom/nimbusds/jose/jwk/KeyUse;Ljava/util/Set;Lcom/nimbusds/jose/Algorithm;Ljava/lang/String;Ljava/net/URI;Lcom/nimbusds/jose/util/Base64URL;Ljava/util/List;)V

    .line 1100
    if-nez p1, :cond_0

    .line 1101
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "The modulus value must not be null"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1104
    :cond_0
    iput-object p1, p0, Lcom/nimbusds/jose/jwk/RSAKey;->n:Lcom/nimbusds/jose/util/Base64URL;

    .line 1107
    if-nez p2, :cond_1

    .line 1108
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "The public exponent value must not be null"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1111
    :cond_1
    iput-object p2, p0, Lcom/nimbusds/jose/jwk/RSAKey;->e:Lcom/nimbusds/jose/util/Base64URL;

    .line 1116
    iput-object p3, p0, Lcom/nimbusds/jose/jwk/RSAKey;->d:Lcom/nimbusds/jose/util/Base64URL;

    .line 1121
    if-eqz p4, :cond_3

    if-eqz p5, :cond_3

    if-eqz p6, :cond_3

    if-eqz p7, :cond_3

    if-eqz p8, :cond_3

    .line 1124
    iput-object p4, p0, Lcom/nimbusds/jose/jwk/RSAKey;->p:Lcom/nimbusds/jose/util/Base64URL;

    .line 1125
    iput-object p5, p0, Lcom/nimbusds/jose/jwk/RSAKey;->q:Lcom/nimbusds/jose/util/Base64URL;

    .line 1126
    move-object/from16 v0, p6

    iput-object v0, p0, Lcom/nimbusds/jose/jwk/RSAKey;->dp:Lcom/nimbusds/jose/util/Base64URL;

    .line 1127
    move-object/from16 v0, p7

    iput-object v0, p0, Lcom/nimbusds/jose/jwk/RSAKey;->dq:Lcom/nimbusds/jose/util/Base64URL;

    .line 1128
    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/nimbusds/jose/jwk/RSAKey;->qi:Lcom/nimbusds/jose/util/Base64URL;

    .line 1131
    if-eqz p9, :cond_2

    .line 1132
    invoke-static/range {p9 .. p9}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/nimbusds/jose/jwk/RSAKey;->oth:Ljava/util/List;

    .line 1162
    :goto_0
    return-void

    .line 1134
    :cond_2
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/nimbusds/jose/jwk/RSAKey;->oth:Ljava/util/List;

    goto :goto_0

    .line 1137
    :cond_3
    if-nez p4, :cond_4

    if-nez p5, :cond_4

    if-nez p6, :cond_4

    if-nez p7, :cond_4

    if-nez p8, :cond_4

    if-nez p9, :cond_4

    .line 1140
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/nimbusds/jose/jwk/RSAKey;->p:Lcom/nimbusds/jose/util/Base64URL;

    .line 1141
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/nimbusds/jose/jwk/RSAKey;->q:Lcom/nimbusds/jose/util/Base64URL;

    .line 1142
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/nimbusds/jose/jwk/RSAKey;->dp:Lcom/nimbusds/jose/util/Base64URL;

    .line 1143
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/nimbusds/jose/jwk/RSAKey;->dq:Lcom/nimbusds/jose/util/Base64URL;

    .line 1144
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/nimbusds/jose/jwk/RSAKey;->qi:Lcom/nimbusds/jose/util/Base64URL;

    .line 1146
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/nimbusds/jose/jwk/RSAKey;->oth:Ljava/util/List;

    goto :goto_0

    .line 1150
    :cond_4
    if-nez p4, :cond_5

    .line 1151
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Incomplete second private (CRT) representation: The first prime factor must not be null"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1152
    :cond_5
    if-nez p5, :cond_6

    .line 1153
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Incomplete second private (CRT) representation: The second prime factor must not be null"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1154
    :cond_6
    if-nez p6, :cond_7

    .line 1155
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Incomplete second private (CRT) representation: The first factor CRT exponent must not be null"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1156
    :cond_7
    if-nez p7, :cond_8

    .line 1157
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Incomplete second private (CRT) representation: The second factor CRT exponent must not be null"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1159
    :cond_8
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Incomplete second private (CRT) representation: The first CRT coefficient must not be null"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public constructor <init>(Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Ljava/util/List;Lcom/nimbusds/jose/jwk/KeyUse;Ljava/util/Set;Lcom/nimbusds/jose/Algorithm;Ljava/lang/String;Ljava/net/URI;Lcom/nimbusds/jose/util/Base64URL;Ljava/util/List;)V
    .locals 17
    .param p1, "n"    # Lcom/nimbusds/jose/util/Base64URL;
    .param p2, "e"    # Lcom/nimbusds/jose/util/Base64URL;
    .param p3, "p"    # Lcom/nimbusds/jose/util/Base64URL;
    .param p4, "q"    # Lcom/nimbusds/jose/util/Base64URL;
    .param p5, "dp"    # Lcom/nimbusds/jose/util/Base64URL;
    .param p6, "dq"    # Lcom/nimbusds/jose/util/Base64URL;
    .param p7, "qi"    # Lcom/nimbusds/jose/util/Base64URL;
    .param p9, "use"    # Lcom/nimbusds/jose/jwk/KeyUse;
    .param p11, "alg"    # Lcom/nimbusds/jose/Algorithm;
    .param p12, "kid"    # Ljava/lang/String;
    .param p13, "x5u"    # Ljava/net/URI;
    .param p14, "x5t"    # Lcom/nimbusds/jose/util/Base64URL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/nimbusds/jose/util/Base64URL;",
            "Lcom/nimbusds/jose/util/Base64URL;",
            "Lcom/nimbusds/jose/util/Base64URL;",
            "Lcom/nimbusds/jose/util/Base64URL;",
            "Lcom/nimbusds/jose/util/Base64URL;",
            "Lcom/nimbusds/jose/util/Base64URL;",
            "Lcom/nimbusds/jose/util/Base64URL;",
            "Ljava/util/List",
            "<",
            "Lcom/nimbusds/jose/jwk/RSAKey$OtherPrimesInfo;",
            ">;",
            "Lcom/nimbusds/jose/jwk/KeyUse;",
            "Ljava/util/Set",
            "<",
            "Lcom/nimbusds/jose/jwk/KeyOperation;",
            ">;",
            "Lcom/nimbusds/jose/Algorithm;",
            "Ljava/lang/String;",
            "Ljava/net/URI;",
            "Lcom/nimbusds/jose/util/Base64URL;",
            "Ljava/util/List",
            "<",
            "Lcom/nimbusds/jose/util/Base64;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1011
    .local p8, "oth":Ljava/util/List;, "Ljava/util/List<Lcom/nimbusds/jose/jwk/RSAKey$OtherPrimesInfo;>;"
    .local p10, "ops":Ljava/util/Set;, "Ljava/util/Set<Lcom/nimbusds/jose/jwk/KeyOperation;>;"
    .local p15, "x5c":Ljava/util/List;, "Ljava/util/List<Lcom/nimbusds/jose/util/Base64;>;"
    const/4 v3, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    .line 1012
    invoke-direct/range {v0 .. v16}, Lcom/nimbusds/jose/jwk/RSAKey;-><init>(Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Ljava/util/List;Lcom/nimbusds/jose/jwk/KeyUse;Ljava/util/Set;Lcom/nimbusds/jose/Algorithm;Ljava/lang/String;Ljava/net/URI;Lcom/nimbusds/jose/util/Base64URL;Ljava/util/List;)V

    .line 1014
    if-nez p3, :cond_0

    .line 1015
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The first prime factor must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1018
    :cond_0
    if-nez p4, :cond_1

    .line 1019
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The second prime factor must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1022
    :cond_1
    if-nez p5, :cond_2

    .line 1023
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The first factor CRT exponent must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1026
    :cond_2
    if-nez p6, :cond_3

    .line 1027
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The second factor CRT exponent must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1030
    :cond_3
    if-nez p7, :cond_4

    .line 1031
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The first CRT coefficient must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1033
    :cond_4
    return-void
.end method

.method public constructor <init>(Ljava/security/interfaces/RSAPublicKey;Lcom/nimbusds/jose/jwk/KeyUse;Ljava/util/Set;Lcom/nimbusds/jose/Algorithm;Ljava/lang/String;Ljava/net/URI;Lcom/nimbusds/jose/util/Base64URL;Ljava/util/List;)V
    .locals 10
    .param p1, "pub"    # Ljava/security/interfaces/RSAPublicKey;
    .param p2, "use"    # Lcom/nimbusds/jose/jwk/KeyUse;
    .param p4, "alg"    # Lcom/nimbusds/jose/Algorithm;
    .param p5, "kid"    # Ljava/lang/String;
    .param p6, "x5u"    # Ljava/net/URI;
    .param p7, "x5t"    # Lcom/nimbusds/jose/util/Base64URL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/security/interfaces/RSAPublicKey;",
            "Lcom/nimbusds/jose/jwk/KeyUse;",
            "Ljava/util/Set",
            "<",
            "Lcom/nimbusds/jose/jwk/KeyOperation;",
            ">;",
            "Lcom/nimbusds/jose/Algorithm;",
            "Ljava/lang/String;",
            "Ljava/net/URI;",
            "Lcom/nimbusds/jose/util/Base64URL;",
            "Ljava/util/List",
            "<",
            "Lcom/nimbusds/jose/util/Base64;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1187
    .local p3, "ops":Ljava/util/Set;, "Ljava/util/Set<Lcom/nimbusds/jose/jwk/KeyOperation;>;"
    .local p8, "x5c":Ljava/util/List;, "Ljava/util/List<Lcom/nimbusds/jose/util/Base64;>;"
    invoke-interface {p1}, Ljava/security/interfaces/RSAPublicKey;->getModulus()Ljava/math/BigInteger;

    move-result-object v0

    invoke-static {v0}, Lcom/nimbusds/jose/util/Base64URL;->encode(Ljava/math/BigInteger;)Lcom/nimbusds/jose/util/Base64URL;

    move-result-object v1

    .line 1188
    invoke-interface {p1}, Ljava/security/interfaces/RSAPublicKey;->getPublicExponent()Ljava/math/BigInteger;

    move-result-object v0

    invoke-static {v0}, Lcom/nimbusds/jose/util/Base64URL;->encode(Ljava/math/BigInteger;)Lcom/nimbusds/jose/util/Base64URL;

    move-result-object v2

    move-object v0, p0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    .line 1190
    invoke-direct/range {v0 .. v9}, Lcom/nimbusds/jose/jwk/RSAKey;-><init>(Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/jwk/KeyUse;Ljava/util/Set;Lcom/nimbusds/jose/Algorithm;Ljava/lang/String;Ljava/net/URI;Lcom/nimbusds/jose/util/Base64URL;Ljava/util/List;)V

    .line 1191
    return-void
.end method

.method public constructor <init>(Ljava/security/interfaces/RSAPublicKey;Ljava/security/interfaces/RSAMultiPrimePrivateCrtKey;Lcom/nimbusds/jose/jwk/KeyUse;Ljava/util/Set;Lcom/nimbusds/jose/Algorithm;Ljava/lang/String;Ljava/net/URI;Lcom/nimbusds/jose/util/Base64URL;Ljava/util/List;)V
    .locals 17
    .param p1, "pub"    # Ljava/security/interfaces/RSAPublicKey;
    .param p2, "priv"    # Ljava/security/interfaces/RSAMultiPrimePrivateCrtKey;
    .param p3, "use"    # Lcom/nimbusds/jose/jwk/KeyUse;
    .param p5, "alg"    # Lcom/nimbusds/jose/Algorithm;
    .param p6, "kid"    # Ljava/lang/String;
    .param p7, "x5u"    # Ljava/net/URI;
    .param p8, "x5t"    # Lcom/nimbusds/jose/util/Base64URL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/security/interfaces/RSAPublicKey;",
            "Ljava/security/interfaces/RSAMultiPrimePrivateCrtKey;",
            "Lcom/nimbusds/jose/jwk/KeyUse;",
            "Ljava/util/Set",
            "<",
            "Lcom/nimbusds/jose/jwk/KeyOperation;",
            ">;",
            "Lcom/nimbusds/jose/Algorithm;",
            "Ljava/lang/String;",
            "Ljava/net/URI;",
            "Lcom/nimbusds/jose/util/Base64URL;",
            "Ljava/util/List",
            "<",
            "Lcom/nimbusds/jose/util/Base64;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1295
    .local p4, "ops":Ljava/util/Set;, "Ljava/util/Set<Lcom/nimbusds/jose/jwk/KeyOperation;>;"
    .local p9, "x5c":Ljava/util/List;, "Ljava/util/List<Lcom/nimbusds/jose/util/Base64;>;"
    invoke-interface/range {p1 .. p1}, Ljava/security/interfaces/RSAPublicKey;->getModulus()Ljava/math/BigInteger;

    move-result-object v0

    invoke-static {v0}, Lcom/nimbusds/jose/util/Base64URL;->encode(Ljava/math/BigInteger;)Lcom/nimbusds/jose/util/Base64URL;

    move-result-object v1

    .line 1296
    invoke-interface/range {p1 .. p1}, Ljava/security/interfaces/RSAPublicKey;->getPublicExponent()Ljava/math/BigInteger;

    move-result-object v0

    invoke-static {v0}, Lcom/nimbusds/jose/util/Base64URL;->encode(Ljava/math/BigInteger;)Lcom/nimbusds/jose/util/Base64URL;

    move-result-object v2

    .line 1297
    invoke-interface/range {p2 .. p2}, Ljava/security/interfaces/RSAMultiPrimePrivateCrtKey;->getPrivateExponent()Ljava/math/BigInteger;

    move-result-object v0

    invoke-static {v0}, Lcom/nimbusds/jose/util/Base64URL;->encode(Ljava/math/BigInteger;)Lcom/nimbusds/jose/util/Base64URL;

    move-result-object v3

    .line 1298
    invoke-interface/range {p2 .. p2}, Ljava/security/interfaces/RSAMultiPrimePrivateCrtKey;->getPrimeP()Ljava/math/BigInteger;

    move-result-object v0

    invoke-static {v0}, Lcom/nimbusds/jose/util/Base64URL;->encode(Ljava/math/BigInteger;)Lcom/nimbusds/jose/util/Base64URL;

    move-result-object v4

    .line 1299
    invoke-interface/range {p2 .. p2}, Ljava/security/interfaces/RSAMultiPrimePrivateCrtKey;->getPrimeQ()Ljava/math/BigInteger;

    move-result-object v0

    invoke-static {v0}, Lcom/nimbusds/jose/util/Base64URL;->encode(Ljava/math/BigInteger;)Lcom/nimbusds/jose/util/Base64URL;

    move-result-object v5

    .line 1300
    invoke-interface/range {p2 .. p2}, Ljava/security/interfaces/RSAMultiPrimePrivateCrtKey;->getPrimeExponentP()Ljava/math/BigInteger;

    move-result-object v0

    invoke-static {v0}, Lcom/nimbusds/jose/util/Base64URL;->encode(Ljava/math/BigInteger;)Lcom/nimbusds/jose/util/Base64URL;

    move-result-object v6

    .line 1301
    invoke-interface/range {p2 .. p2}, Ljava/security/interfaces/RSAMultiPrimePrivateCrtKey;->getPrimeExponentQ()Ljava/math/BigInteger;

    move-result-object v0

    invoke-static {v0}, Lcom/nimbusds/jose/util/Base64URL;->encode(Ljava/math/BigInteger;)Lcom/nimbusds/jose/util/Base64URL;

    move-result-object v7

    .line 1302
    invoke-interface/range {p2 .. p2}, Ljava/security/interfaces/RSAMultiPrimePrivateCrtKey;->getCrtCoefficient()Ljava/math/BigInteger;

    move-result-object v0

    invoke-static {v0}, Lcom/nimbusds/jose/util/Base64URL;->encode(Ljava/math/BigInteger;)Lcom/nimbusds/jose/util/Base64URL;

    move-result-object v8

    .line 1303
    invoke-interface/range {p2 .. p2}, Ljava/security/interfaces/RSAMultiPrimePrivateCrtKey;->getOtherPrimeInfo()[Ljava/security/spec/RSAOtherPrimeInfo;

    move-result-object v0

    invoke-static {v0}, Lcom/nimbusds/jose/jwk/RSAKey$OtherPrimesInfo;->toList([Ljava/security/spec/RSAOtherPrimeInfo;)Ljava/util/List;

    move-result-object v9

    move-object/from16 v0, p0

    move-object/from16 v10, p3

    move-object/from16 v11, p4

    move-object/from16 v12, p5

    move-object/from16 v13, p6

    move-object/from16 v14, p7

    move-object/from16 v15, p8

    move-object/from16 v16, p9

    .line 1305
    invoke-direct/range {v0 .. v16}, Lcom/nimbusds/jose/jwk/RSAKey;-><init>(Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Ljava/util/List;Lcom/nimbusds/jose/jwk/KeyUse;Ljava/util/Set;Lcom/nimbusds/jose/Algorithm;Ljava/lang/String;Ljava/net/URI;Lcom/nimbusds/jose/util/Base64URL;Ljava/util/List;)V

    .line 1306
    return-void
.end method

.method public constructor <init>(Ljava/security/interfaces/RSAPublicKey;Ljava/security/interfaces/RSAPrivateCrtKey;Lcom/nimbusds/jose/jwk/KeyUse;Ljava/util/Set;Lcom/nimbusds/jose/Algorithm;Ljava/lang/String;Ljava/net/URI;Lcom/nimbusds/jose/util/Base64URL;Ljava/util/List;)V
    .locals 17
    .param p1, "pub"    # Ljava/security/interfaces/RSAPublicKey;
    .param p2, "priv"    # Ljava/security/interfaces/RSAPrivateCrtKey;
    .param p3, "use"    # Lcom/nimbusds/jose/jwk/KeyUse;
    .param p5, "alg"    # Lcom/nimbusds/jose/Algorithm;
    .param p6, "kid"    # Ljava/lang/String;
    .param p7, "x5u"    # Ljava/net/URI;
    .param p8, "x5t"    # Lcom/nimbusds/jose/util/Base64URL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/security/interfaces/RSAPublicKey;",
            "Ljava/security/interfaces/RSAPrivateCrtKey;",
            "Lcom/nimbusds/jose/jwk/KeyUse;",
            "Ljava/util/Set",
            "<",
            "Lcom/nimbusds/jose/jwk/KeyOperation;",
            ">;",
            "Lcom/nimbusds/jose/Algorithm;",
            "Ljava/lang/String;",
            "Ljava/net/URI;",
            "Lcom/nimbusds/jose/util/Base64URL;",
            "Ljava/util/List",
            "<",
            "Lcom/nimbusds/jose/util/Base64;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1254
    .local p4, "ops":Ljava/util/Set;, "Ljava/util/Set<Lcom/nimbusds/jose/jwk/KeyOperation;>;"
    .local p9, "x5c":Ljava/util/List;, "Ljava/util/List<Lcom/nimbusds/jose/util/Base64;>;"
    invoke-interface/range {p1 .. p1}, Ljava/security/interfaces/RSAPublicKey;->getModulus()Ljava/math/BigInteger;

    move-result-object v0

    invoke-static {v0}, Lcom/nimbusds/jose/util/Base64URL;->encode(Ljava/math/BigInteger;)Lcom/nimbusds/jose/util/Base64URL;

    move-result-object v1

    .line 1255
    invoke-interface/range {p1 .. p1}, Ljava/security/interfaces/RSAPublicKey;->getPublicExponent()Ljava/math/BigInteger;

    move-result-object v0

    invoke-static {v0}, Lcom/nimbusds/jose/util/Base64URL;->encode(Ljava/math/BigInteger;)Lcom/nimbusds/jose/util/Base64URL;

    move-result-object v2

    .line 1256
    invoke-interface/range {p2 .. p2}, Ljava/security/interfaces/RSAPrivateCrtKey;->getPrivateExponent()Ljava/math/BigInteger;

    move-result-object v0

    invoke-static {v0}, Lcom/nimbusds/jose/util/Base64URL;->encode(Ljava/math/BigInteger;)Lcom/nimbusds/jose/util/Base64URL;

    move-result-object v3

    .line 1257
    invoke-interface/range {p2 .. p2}, Ljava/security/interfaces/RSAPrivateCrtKey;->getPrimeP()Ljava/math/BigInteger;

    move-result-object v0

    invoke-static {v0}, Lcom/nimbusds/jose/util/Base64URL;->encode(Ljava/math/BigInteger;)Lcom/nimbusds/jose/util/Base64URL;

    move-result-object v4

    .line 1258
    invoke-interface/range {p2 .. p2}, Ljava/security/interfaces/RSAPrivateCrtKey;->getPrimeQ()Ljava/math/BigInteger;

    move-result-object v0

    invoke-static {v0}, Lcom/nimbusds/jose/util/Base64URL;->encode(Ljava/math/BigInteger;)Lcom/nimbusds/jose/util/Base64URL;

    move-result-object v5

    .line 1259
    invoke-interface/range {p2 .. p2}, Ljava/security/interfaces/RSAPrivateCrtKey;->getPrimeExponentP()Ljava/math/BigInteger;

    move-result-object v0

    invoke-static {v0}, Lcom/nimbusds/jose/util/Base64URL;->encode(Ljava/math/BigInteger;)Lcom/nimbusds/jose/util/Base64URL;

    move-result-object v6

    .line 1260
    invoke-interface/range {p2 .. p2}, Ljava/security/interfaces/RSAPrivateCrtKey;->getPrimeExponentQ()Ljava/math/BigInteger;

    move-result-object v0

    invoke-static {v0}, Lcom/nimbusds/jose/util/Base64URL;->encode(Ljava/math/BigInteger;)Lcom/nimbusds/jose/util/Base64URL;

    move-result-object v7

    .line 1261
    invoke-interface/range {p2 .. p2}, Ljava/security/interfaces/RSAPrivateCrtKey;->getCrtCoefficient()Ljava/math/BigInteger;

    move-result-object v0

    invoke-static {v0}, Lcom/nimbusds/jose/util/Base64URL;->encode(Ljava/math/BigInteger;)Lcom/nimbusds/jose/util/Base64URL;

    move-result-object v8

    .line 1262
    const/4 v9, 0x0

    move-object/from16 v0, p0

    move-object/from16 v10, p3

    move-object/from16 v11, p4

    move-object/from16 v12, p5

    move-object/from16 v13, p6

    move-object/from16 v14, p7

    move-object/from16 v15, p8

    move-object/from16 v16, p9

    .line 1264
    invoke-direct/range {v0 .. v16}, Lcom/nimbusds/jose/jwk/RSAKey;-><init>(Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Ljava/util/List;Lcom/nimbusds/jose/jwk/KeyUse;Ljava/util/Set;Lcom/nimbusds/jose/Algorithm;Ljava/lang/String;Ljava/net/URI;Lcom/nimbusds/jose/util/Base64URL;Ljava/util/List;)V

    .line 1265
    return-void
.end method

.method public constructor <init>(Ljava/security/interfaces/RSAPublicKey;Ljava/security/interfaces/RSAPrivateKey;Lcom/nimbusds/jose/jwk/KeyUse;Ljava/util/Set;Lcom/nimbusds/jose/Algorithm;Ljava/lang/String;Ljava/net/URI;Lcom/nimbusds/jose/util/Base64URL;Ljava/util/List;)V
    .locals 11
    .param p1, "pub"    # Ljava/security/interfaces/RSAPublicKey;
    .param p2, "priv"    # Ljava/security/interfaces/RSAPrivateKey;
    .param p3, "use"    # Lcom/nimbusds/jose/jwk/KeyUse;
    .param p5, "alg"    # Lcom/nimbusds/jose/Algorithm;
    .param p6, "kid"    # Ljava/lang/String;
    .param p7, "x5u"    # Ljava/net/URI;
    .param p8, "x5t"    # Lcom/nimbusds/jose/util/Base64URL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/security/interfaces/RSAPublicKey;",
            "Ljava/security/interfaces/RSAPrivateKey;",
            "Lcom/nimbusds/jose/jwk/KeyUse;",
            "Ljava/util/Set",
            "<",
            "Lcom/nimbusds/jose/jwk/KeyOperation;",
            ">;",
            "Lcom/nimbusds/jose/Algorithm;",
            "Ljava/lang/String;",
            "Ljava/net/URI;",
            "Lcom/nimbusds/jose/util/Base64URL;",
            "Ljava/util/List",
            "<",
            "Lcom/nimbusds/jose/util/Base64;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1220
    .local p4, "ops":Ljava/util/Set;, "Ljava/util/Set<Lcom/nimbusds/jose/jwk/KeyOperation;>;"
    .local p9, "x5c":Ljava/util/List;, "Ljava/util/List<Lcom/nimbusds/jose/util/Base64;>;"
    invoke-interface {p1}, Ljava/security/interfaces/RSAPublicKey;->getModulus()Ljava/math/BigInteger;

    move-result-object v0

    invoke-static {v0}, Lcom/nimbusds/jose/util/Base64URL;->encode(Ljava/math/BigInteger;)Lcom/nimbusds/jose/util/Base64URL;

    move-result-object v1

    .line 1221
    invoke-interface {p1}, Ljava/security/interfaces/RSAPublicKey;->getPublicExponent()Ljava/math/BigInteger;

    move-result-object v0

    invoke-static {v0}, Lcom/nimbusds/jose/util/Base64URL;->encode(Ljava/math/BigInteger;)Lcom/nimbusds/jose/util/Base64URL;

    move-result-object v2

    .line 1222
    invoke-interface {p2}, Ljava/security/interfaces/RSAPrivateKey;->getPrivateExponent()Ljava/math/BigInteger;

    move-result-object v0

    invoke-static {v0}, Lcom/nimbusds/jose/util/Base64URL;->encode(Ljava/math/BigInteger;)Lcom/nimbusds/jose/util/Base64URL;

    move-result-object v3

    move-object v0, p0

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    .line 1224
    invoke-direct/range {v0 .. v10}, Lcom/nimbusds/jose/jwk/RSAKey;-><init>(Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/jwk/KeyUse;Ljava/util/Set;Lcom/nimbusds/jose/Algorithm;Ljava/lang/String;Ljava/net/URI;Lcom/nimbusds/jose/util/Base64URL;Ljava/util/List;)V

    .line 1225
    return-void
.end method

.method public static bridge synthetic parse(Ljava/lang/String;)Lcom/nimbusds/jose/jwk/JWK;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-static {p0}, Lcom/nimbusds/jose/jwk/RSAKey;->parse(Ljava/lang/String;)Lcom/nimbusds/jose/jwk/RSAKey;

    move-result-object v0

    return-object v0
.end method

.method public static bridge synthetic parse(Lnet/minidev/json/JSONObject;)Lcom/nimbusds/jose/jwk/JWK;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-static {p0}, Lcom/nimbusds/jose/jwk/RSAKey;->parse(Lnet/minidev/json/JSONObject;)Lcom/nimbusds/jose/jwk/RSAKey;

    move-result-object v0

    return-object v0
.end method

.method public static parse(Ljava/lang/String;)Lcom/nimbusds/jose/jwk/RSAKey;
    .locals 1
    .param p0, "s"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 1682
    invoke-static {p0}, Lcom/nimbusds/jose/util/JSONObjectUtils;->parse(Ljava/lang/String;)Lnet/minidev/json/JSONObject;

    move-result-object v0

    invoke-static {v0}, Lcom/nimbusds/jose/jwk/RSAKey;->parse(Lnet/minidev/json/JSONObject;)Lcom/nimbusds/jose/jwk/RSAKey;

    move-result-object v0

    return-object v0
.end method

.method public static parse(Lnet/minidev/json/JSONObject;)Lcom/nimbusds/jose/jwk/RSAKey;
    .locals 30
    .param p0, "jsonObject"    # Lnet/minidev/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 1702
    new-instance v5, Lcom/nimbusds/jose/util/Base64URL;

    const-string v4, "n"

    move-object/from16 v0, p0

    invoke-static {v0, v4}, Lcom/nimbusds/jose/util/JSONObjectUtils;->getString(Lnet/minidev/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v4}, Lcom/nimbusds/jose/util/Base64URL;-><init>(Ljava/lang/String;)V

    .line 1703
    .local v5, "n":Lcom/nimbusds/jose/util/Base64URL;
    new-instance v6, Lcom/nimbusds/jose/util/Base64URL;

    const-string v4, "e"

    move-object/from16 v0, p0

    invoke-static {v0, v4}, Lcom/nimbusds/jose/util/JSONObjectUtils;->getString(Lnet/minidev/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v6, v4}, Lcom/nimbusds/jose/util/Base64URL;-><init>(Ljava/lang/String;)V

    .line 1706
    .local v6, "e":Lcom/nimbusds/jose/util/Base64URL;
    const-string v4, "kty"

    move-object/from16 v0, p0

    invoke-static {v0, v4}, Lcom/nimbusds/jose/util/JSONObjectUtils;->getString(Lnet/minidev/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/nimbusds/jose/jwk/KeyType;->parse(Ljava/lang/String;)Lcom/nimbusds/jose/jwk/KeyType;

    move-result-object v23

    .line 1707
    .local v23, "kty":Lcom/nimbusds/jose/jwk/KeyType;
    sget-object v4, Lcom/nimbusds/jose/jwk/KeyType;->RSA:Lcom/nimbusds/jose/jwk/KeyType;

    move-object/from16 v0, v23

    if-eq v0, v4, :cond_0

    .line 1708
    new-instance v4, Ljava/text/ParseException;

    const-string v14, "The key type \"kty\" must be RSA"

    const/4 v15, 0x0

    invoke-direct {v4, v14, v15}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v4

    .line 1714
    :cond_0
    const/4 v7, 0x0

    .line 1715
    .local v7, "d":Lcom/nimbusds/jose/util/Base64URL;
    const-string v4, "d"

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/minidev/json/JSONObject;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1716
    new-instance v7, Lcom/nimbusds/jose/util/Base64URL;

    .end local v7    # "d":Lcom/nimbusds/jose/util/Base64URL;
    const-string v4, "d"

    move-object/from16 v0, p0

    invoke-static {v0, v4}, Lcom/nimbusds/jose/util/JSONObjectUtils;->getString(Lnet/minidev/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v7, v4}, Lcom/nimbusds/jose/util/Base64URL;-><init>(Ljava/lang/String;)V

    .line 1720
    .restart local v7    # "d":Lcom/nimbusds/jose/util/Base64URL;
    :cond_1
    const/4 v8, 0x0

    .line 1721
    .local v8, "p":Lcom/nimbusds/jose/util/Base64URL;
    const-string v4, "p"

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/minidev/json/JSONObject;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1722
    new-instance v8, Lcom/nimbusds/jose/util/Base64URL;

    .end local v8    # "p":Lcom/nimbusds/jose/util/Base64URL;
    const-string v4, "p"

    move-object/from16 v0, p0

    invoke-static {v0, v4}, Lcom/nimbusds/jose/util/JSONObjectUtils;->getString(Lnet/minidev/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v8, v4}, Lcom/nimbusds/jose/util/Base64URL;-><init>(Ljava/lang/String;)V

    .line 1724
    .restart local v8    # "p":Lcom/nimbusds/jose/util/Base64URL;
    :cond_2
    const/4 v9, 0x0

    .line 1725
    .local v9, "q":Lcom/nimbusds/jose/util/Base64URL;
    const-string v4, "q"

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/minidev/json/JSONObject;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1726
    new-instance v9, Lcom/nimbusds/jose/util/Base64URL;

    .end local v9    # "q":Lcom/nimbusds/jose/util/Base64URL;
    const-string v4, "q"

    move-object/from16 v0, p0

    invoke-static {v0, v4}, Lcom/nimbusds/jose/util/JSONObjectUtils;->getString(Lnet/minidev/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v9, v4}, Lcom/nimbusds/jose/util/Base64URL;-><init>(Ljava/lang/String;)V

    .line 1728
    .restart local v9    # "q":Lcom/nimbusds/jose/util/Base64URL;
    :cond_3
    const/4 v10, 0x0

    .line 1729
    .local v10, "dp":Lcom/nimbusds/jose/util/Base64URL;
    const-string v4, "dp"

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/minidev/json/JSONObject;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1730
    new-instance v10, Lcom/nimbusds/jose/util/Base64URL;

    .end local v10    # "dp":Lcom/nimbusds/jose/util/Base64URL;
    const-string v4, "dp"

    move-object/from16 v0, p0

    invoke-static {v0, v4}, Lcom/nimbusds/jose/util/JSONObjectUtils;->getString(Lnet/minidev/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v10, v4}, Lcom/nimbusds/jose/util/Base64URL;-><init>(Ljava/lang/String;)V

    .line 1732
    .restart local v10    # "dp":Lcom/nimbusds/jose/util/Base64URL;
    :cond_4
    const/4 v11, 0x0

    .line 1733
    .local v11, "dq":Lcom/nimbusds/jose/util/Base64URL;
    const-string v4, "dq"

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/minidev/json/JSONObject;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1734
    new-instance v11, Lcom/nimbusds/jose/util/Base64URL;

    .end local v11    # "dq":Lcom/nimbusds/jose/util/Base64URL;
    const-string v4, "dq"

    move-object/from16 v0, p0

    invoke-static {v0, v4}, Lcom/nimbusds/jose/util/JSONObjectUtils;->getString(Lnet/minidev/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v11, v4}, Lcom/nimbusds/jose/util/Base64URL;-><init>(Ljava/lang/String;)V

    .line 1736
    .restart local v11    # "dq":Lcom/nimbusds/jose/util/Base64URL;
    :cond_5
    const/4 v12, 0x0

    .line 1737
    .local v12, "qi":Lcom/nimbusds/jose/util/Base64URL;
    const-string v4, "qi"

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/minidev/json/JSONObject;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1738
    new-instance v12, Lcom/nimbusds/jose/util/Base64URL;

    .end local v12    # "qi":Lcom/nimbusds/jose/util/Base64URL;
    const-string v4, "qi"

    move-object/from16 v0, p0

    invoke-static {v0, v4}, Lcom/nimbusds/jose/util/JSONObjectUtils;->getString(Lnet/minidev/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v12, v4}, Lcom/nimbusds/jose/util/Base64URL;-><init>(Ljava/lang/String;)V

    .line 1741
    .restart local v12    # "qi":Lcom/nimbusds/jose/util/Base64URL;
    :cond_6
    const/4 v13, 0x0

    .line 1742
    .local v13, "oth":Ljava/util/List;, "Ljava/util/List<Lcom/nimbusds/jose/jwk/RSAKey$OtherPrimesInfo;>;"
    const-string v4, "oth"

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lnet/minidev/json/JSONObject;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 1744
    const-string v4, "oth"

    move-object/from16 v0, p0

    invoke-static {v0, v4}, Lcom/nimbusds/jose/util/JSONObjectUtils;->getJSONArray(Lnet/minidev/json/JSONObject;Ljava/lang/String;)Lnet/minidev/json/JSONArray;

    move-result-object v21

    .line 1745
    .local v21, "arr":Lnet/minidev/json/JSONArray;
    new-instance v13, Ljava/util/ArrayList;

    .end local v13    # "oth":Ljava/util/List;, "Ljava/util/List<Lcom/nimbusds/jose/jwk/RSAKey$OtherPrimesInfo;>;"
    invoke-virtual/range {v21 .. v21}, Lnet/minidev/json/JSONArray;->size()I

    move-result v4

    invoke-direct {v13, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 1747
    .restart local v13    # "oth":Ljava/util/List;, "Ljava/util/List<Lcom/nimbusds/jose/jwk/RSAKey$OtherPrimesInfo;>;"
    invoke-virtual/range {v21 .. v21}, Lnet/minidev/json/JSONArray;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_7
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-nez v14, :cond_9

    .line 1763
    .end local v21    # "arr":Lnet/minidev/json/JSONArray;
    :cond_8
    :try_start_0
    new-instance v4, Lcom/nimbusds/jose/jwk/RSAKey;

    .line 1764
    invoke-static/range {p0 .. p0}, Lcom/nimbusds/jose/jwk/JWKMetadata;->parseKeyUse(Lnet/minidev/json/JSONObject;)Lcom/nimbusds/jose/jwk/KeyUse;

    move-result-object v14

    .line 1765
    invoke-static/range {p0 .. p0}, Lcom/nimbusds/jose/jwk/JWKMetadata;->parseKeyOperations(Lnet/minidev/json/JSONObject;)Ljava/util/Set;

    move-result-object v15

    .line 1766
    invoke-static/range {p0 .. p0}, Lcom/nimbusds/jose/jwk/JWKMetadata;->parseAlgorithm(Lnet/minidev/json/JSONObject;)Lcom/nimbusds/jose/Algorithm;

    move-result-object v16

    .line 1767
    invoke-static/range {p0 .. p0}, Lcom/nimbusds/jose/jwk/JWKMetadata;->parseKeyID(Lnet/minidev/json/JSONObject;)Ljava/lang/String;

    move-result-object v17

    .line 1768
    invoke-static/range {p0 .. p0}, Lcom/nimbusds/jose/jwk/JWKMetadata;->parseX509CertURL(Lnet/minidev/json/JSONObject;)Ljava/net/URI;

    move-result-object v18

    .line 1769
    invoke-static/range {p0 .. p0}, Lcom/nimbusds/jose/jwk/JWKMetadata;->parseX509CertThumbprint(Lnet/minidev/json/JSONObject;)Lcom/nimbusds/jose/util/Base64URL;

    move-result-object v19

    .line 1770
    invoke-static/range {p0 .. p0}, Lcom/nimbusds/jose/jwk/JWKMetadata;->parseX509CertChain(Lnet/minidev/json/JSONObject;)Ljava/util/List;

    move-result-object v20

    .line 1763
    invoke-direct/range {v4 .. v20}, Lcom/nimbusds/jose/jwk/RSAKey;-><init>(Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Ljava/util/List;Lcom/nimbusds/jose/jwk/KeyUse;Ljava/util/Set;Lcom/nimbusds/jose/Algorithm;Ljava/lang/String;Ljava/net/URI;Lcom/nimbusds/jose/util/Base64URL;Ljava/util/List;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v4

    .line 1747
    .restart local v21    # "arr":Lnet/minidev/json/JSONArray;
    :cond_9
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v24

    .line 1749
    .local v24, "o":Ljava/lang/Object;
    move-object/from16 v0, v24

    instance-of v14, v0, Lnet/minidev/json/JSONObject;

    if-eqz v14, :cond_7

    move-object/from16 v26, v24

    .line 1750
    check-cast v26, Lnet/minidev/json/JSONObject;

    .line 1752
    .local v26, "otherJson":Lnet/minidev/json/JSONObject;
    new-instance v28, Lcom/nimbusds/jose/util/Base64URL;

    const-string v14, "r"

    move-object/from16 v0, v26

    invoke-static {v0, v14}, Lcom/nimbusds/jose/util/JSONObjectUtils;->getString(Lnet/minidev/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, v28

    invoke-direct {v0, v14}, Lcom/nimbusds/jose/util/Base64URL;-><init>(Ljava/lang/String;)V

    .line 1753
    .local v28, "r":Lcom/nimbusds/jose/util/Base64URL;
    new-instance v25, Lcom/nimbusds/jose/util/Base64URL;

    const-string v14, "dq"

    move-object/from16 v0, v26

    invoke-static {v0, v14}, Lcom/nimbusds/jose/util/JSONObjectUtils;->getString(Lnet/minidev/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, v25

    invoke-direct {v0, v14}, Lcom/nimbusds/jose/util/Base64URL;-><init>(Ljava/lang/String;)V

    .line 1754
    .local v25, "odq":Lcom/nimbusds/jose/util/Base64URL;
    new-instance v29, Lcom/nimbusds/jose/util/Base64URL;

    const-string v14, "t"

    move-object/from16 v0, v26

    invoke-static {v0, v14}, Lcom/nimbusds/jose/util/JSONObjectUtils;->getString(Lnet/minidev/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, v29

    invoke-direct {v0, v14}, Lcom/nimbusds/jose/util/Base64URL;-><init>(Ljava/lang/String;)V

    .line 1756
    .local v29, "t":Lcom/nimbusds/jose/util/Base64URL;
    new-instance v27, Lcom/nimbusds/jose/jwk/RSAKey$OtherPrimesInfo;

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    move-object/from16 v2, v25

    move-object/from16 v3, v29

    invoke-direct {v0, v1, v2, v3}, Lcom/nimbusds/jose/jwk/RSAKey$OtherPrimesInfo;-><init>(Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;)V

    .line 1757
    .local v27, "prime":Lcom/nimbusds/jose/jwk/RSAKey$OtherPrimesInfo;
    move-object/from16 v0, v27

    invoke-interface {v13, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1772
    .end local v21    # "arr":Lnet/minidev/json/JSONArray;
    .end local v24    # "o":Ljava/lang/Object;
    .end local v25    # "odq":Lcom/nimbusds/jose/util/Base64URL;
    .end local v26    # "otherJson":Lnet/minidev/json/JSONObject;
    .end local v27    # "prime":Lcom/nimbusds/jose/jwk/RSAKey$OtherPrimesInfo;
    .end local v28    # "r":Lcom/nimbusds/jose/util/Base64URL;
    .end local v29    # "t":Lcom/nimbusds/jose/util/Base64URL;
    :catch_0
    move-exception v22

    .line 1775
    .local v22, "ex":Ljava/lang/IllegalArgumentException;
    new-instance v4, Ljava/text/ParseException;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x0

    invoke-direct {v4, v14, v15}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v4
.end method


# virtual methods
.method public getFirstCRTCoefficient()Lcom/nimbusds/jose/util/Base64URL;
    .locals 1

    .prologue
    .line 1416
    iget-object v0, p0, Lcom/nimbusds/jose/jwk/RSAKey;->qi:Lcom/nimbusds/jose/util/Base64URL;

    return-object v0
.end method

.method public getFirstFactorCRTExponent()Lcom/nimbusds/jose/util/Base64URL;
    .locals 1

    .prologue
    .line 1386
    iget-object v0, p0, Lcom/nimbusds/jose/jwk/RSAKey;->dp:Lcom/nimbusds/jose/util/Base64URL;

    return-object v0
.end method

.method public getFirstPrimeFactor()Lcom/nimbusds/jose/util/Base64URL;
    .locals 1

    .prologue
    .line 1357
    iget-object v0, p0, Lcom/nimbusds/jose/jwk/RSAKey;->p:Lcom/nimbusds/jose/util/Base64URL;

    return-object v0
.end method

.method public getModulus()Lcom/nimbusds/jose/util/Base64URL;
    .locals 1

    .prologue
    .line 1317
    iget-object v0, p0, Lcom/nimbusds/jose/jwk/RSAKey;->n:Lcom/nimbusds/jose/util/Base64URL;

    return-object v0
.end method

.method public getOtherPrimes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/nimbusds/jose/jwk/RSAKey$OtherPrimesInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1429
    iget-object v0, p0, Lcom/nimbusds/jose/jwk/RSAKey;->oth:Ljava/util/List;

    return-object v0
.end method

.method public getPrivateExponent()Lcom/nimbusds/jose/util/Base64URL;
    .locals 1

    .prologue
    .line 1343
    iget-object v0, p0, Lcom/nimbusds/jose/jwk/RSAKey;->d:Lcom/nimbusds/jose/util/Base64URL;

    return-object v0
.end method

.method public getPublicExponent()Lcom/nimbusds/jose/util/Base64URL;
    .locals 1

    .prologue
    .line 1329
    iget-object v0, p0, Lcom/nimbusds/jose/jwk/RSAKey;->e:Lcom/nimbusds/jose/util/Base64URL;

    return-object v0
.end method

.method public getRequiredParams()Ljava/util/LinkedHashMap;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "*>;"
        }
    .end annotation

    .prologue
    .line 1591
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 1592
    .local v0, "requiredParams":Ljava/util/LinkedHashMap;, "Ljava/util/LinkedHashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v1, "e"

    iget-object v2, p0, Lcom/nimbusds/jose/jwk/RSAKey;->e:Lcom/nimbusds/jose/util/Base64URL;

    invoke-virtual {v2}, Lcom/nimbusds/jose/util/Base64URL;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1593
    const-string v1, "kty"

    invoke-virtual {p0}, Lcom/nimbusds/jose/jwk/RSAKey;->getKeyType()Lcom/nimbusds/jose/jwk/KeyType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/nimbusds/jose/jwk/KeyType;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1594
    const-string v1, "n"

    iget-object v2, p0, Lcom/nimbusds/jose/jwk/RSAKey;->n:Lcom/nimbusds/jose/util/Base64URL;

    invoke-virtual {v2}, Lcom/nimbusds/jose/util/Base64URL;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1595
    return-object v0
.end method

.method public getSecondFactorCRTExponent()Lcom/nimbusds/jose/util/Base64URL;
    .locals 1

    .prologue
    .line 1401
    iget-object v0, p0, Lcom/nimbusds/jose/jwk/RSAKey;->dq:Lcom/nimbusds/jose/util/Base64URL;

    return-object v0
.end method

.method public getSecondPrimeFactor()Lcom/nimbusds/jose/util/Base64URL;
    .locals 1

    .prologue
    .line 1371
    iget-object v0, p0, Lcom/nimbusds/jose/jwk/RSAKey;->q:Lcom/nimbusds/jose/util/Base64URL;

    return-object v0
.end method

.method public isPrivate()Z
    .locals 1

    .prologue
    .line 1603
    iget-object v0, p0, Lcom/nimbusds/jose/jwk/RSAKey;->d:Lcom/nimbusds/jose/util/Base64URL;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/nimbusds/jose/jwk/RSAKey;->p:Lcom/nimbusds/jose/util/Base64URL;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public toJSONObject()Lnet/minidev/json/JSONObject;
    .locals 7

    .prologue
    .line 1624
    invoke-super {p0}, Lcom/nimbusds/jose/jwk/JWK;->toJSONObject()Lnet/minidev/json/JSONObject;

    move-result-object v1

    .line 1627
    .local v1, "o":Lnet/minidev/json/JSONObject;
    const-string v4, "n"

    iget-object v5, p0, Lcom/nimbusds/jose/jwk/RSAKey;->n:Lcom/nimbusds/jose/util/Base64URL;

    invoke-virtual {v5}, Lcom/nimbusds/jose/util/Base64URL;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Lnet/minidev/json/JSONObject;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1628
    const-string v4, "e"

    iget-object v5, p0, Lcom/nimbusds/jose/jwk/RSAKey;->e:Lcom/nimbusds/jose/util/Base64URL;

    invoke-virtual {v5}, Lcom/nimbusds/jose/util/Base64URL;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Lnet/minidev/json/JSONObject;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1629
    iget-object v4, p0, Lcom/nimbusds/jose/jwk/RSAKey;->d:Lcom/nimbusds/jose/util/Base64URL;

    if-eqz v4, :cond_0

    .line 1630
    const-string v4, "d"

    iget-object v5, p0, Lcom/nimbusds/jose/jwk/RSAKey;->d:Lcom/nimbusds/jose/util/Base64URL;

    invoke-virtual {v5}, Lcom/nimbusds/jose/util/Base64URL;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Lnet/minidev/json/JSONObject;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1632
    :cond_0
    iget-object v4, p0, Lcom/nimbusds/jose/jwk/RSAKey;->p:Lcom/nimbusds/jose/util/Base64URL;

    if-eqz v4, :cond_1

    .line 1633
    const-string v4, "p"

    iget-object v5, p0, Lcom/nimbusds/jose/jwk/RSAKey;->p:Lcom/nimbusds/jose/util/Base64URL;

    invoke-virtual {v5}, Lcom/nimbusds/jose/util/Base64URL;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Lnet/minidev/json/JSONObject;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1635
    :cond_1
    iget-object v4, p0, Lcom/nimbusds/jose/jwk/RSAKey;->q:Lcom/nimbusds/jose/util/Base64URL;

    if-eqz v4, :cond_2

    .line 1636
    const-string v4, "q"

    iget-object v5, p0, Lcom/nimbusds/jose/jwk/RSAKey;->q:Lcom/nimbusds/jose/util/Base64URL;

    invoke-virtual {v5}, Lcom/nimbusds/jose/util/Base64URL;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Lnet/minidev/json/JSONObject;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1638
    :cond_2
    iget-object v4, p0, Lcom/nimbusds/jose/jwk/RSAKey;->dp:Lcom/nimbusds/jose/util/Base64URL;

    if-eqz v4, :cond_3

    .line 1639
    const-string v4, "dp"

    iget-object v5, p0, Lcom/nimbusds/jose/jwk/RSAKey;->dp:Lcom/nimbusds/jose/util/Base64URL;

    invoke-virtual {v5}, Lcom/nimbusds/jose/util/Base64URL;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Lnet/minidev/json/JSONObject;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1641
    :cond_3
    iget-object v4, p0, Lcom/nimbusds/jose/jwk/RSAKey;->dq:Lcom/nimbusds/jose/util/Base64URL;

    if-eqz v4, :cond_4

    .line 1642
    const-string v4, "dq"

    iget-object v5, p0, Lcom/nimbusds/jose/jwk/RSAKey;->dq:Lcom/nimbusds/jose/util/Base64URL;

    invoke-virtual {v5}, Lcom/nimbusds/jose/util/Base64URL;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Lnet/minidev/json/JSONObject;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1644
    :cond_4
    iget-object v4, p0, Lcom/nimbusds/jose/jwk/RSAKey;->qi:Lcom/nimbusds/jose/util/Base64URL;

    if-eqz v4, :cond_5

    .line 1645
    const-string v4, "qi"

    iget-object v5, p0, Lcom/nimbusds/jose/jwk/RSAKey;->qi:Lcom/nimbusds/jose/util/Base64URL;

    invoke-virtual {v5}, Lcom/nimbusds/jose/util/Base64URL;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Lnet/minidev/json/JSONObject;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1647
    :cond_5
    iget-object v4, p0, Lcom/nimbusds/jose/jwk/RSAKey;->oth:Ljava/util/List;

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/nimbusds/jose/jwk/RSAKey;->oth:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_6

    .line 1649
    new-instance v0, Lnet/minidev/json/JSONArray;

    invoke-direct {v0}, Lnet/minidev/json/JSONArray;-><init>()V

    .line 1651
    .local v0, "a":Lnet/minidev/json/JSONArray;
    iget-object v4, p0, Lcom/nimbusds/jose/jwk/RSAKey;->oth:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_7

    .line 1661
    const-string v4, "oth"

    invoke-virtual {v1, v4, v0}, Lnet/minidev/json/JSONObject;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1664
    .end local v0    # "a":Lnet/minidev/json/JSONArray;
    :cond_6
    return-object v1

    .line 1651
    .restart local v0    # "a":Lnet/minidev/json/JSONArray;
    :cond_7
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/nimbusds/jose/jwk/RSAKey$OtherPrimesInfo;

    .line 1653
    .local v3, "other":Lcom/nimbusds/jose/jwk/RSAKey$OtherPrimesInfo;
    new-instance v2, Lnet/minidev/json/JSONObject;

    invoke-direct {v2}, Lnet/minidev/json/JSONObject;-><init>()V

    .line 1654
    .local v2, "oo":Lnet/minidev/json/JSONObject;
    const-string v5, "r"

    invoke-static {v3}, Lcom/nimbusds/jose/jwk/RSAKey$OtherPrimesInfo;->access$0(Lcom/nimbusds/jose/jwk/RSAKey$OtherPrimesInfo;)Lcom/nimbusds/jose/util/Base64URL;

    move-result-object v6

    invoke-virtual {v6}, Lcom/nimbusds/jose/util/Base64URL;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Lnet/minidev/json/JSONObject;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1655
    const-string v5, "d"

    invoke-static {v3}, Lcom/nimbusds/jose/jwk/RSAKey$OtherPrimesInfo;->access$1(Lcom/nimbusds/jose/jwk/RSAKey$OtherPrimesInfo;)Lcom/nimbusds/jose/util/Base64URL;

    move-result-object v6

    invoke-virtual {v6}, Lcom/nimbusds/jose/util/Base64URL;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Lnet/minidev/json/JSONObject;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1656
    const-string v5, "t"

    invoke-static {v3}, Lcom/nimbusds/jose/jwk/RSAKey$OtherPrimesInfo;->access$2(Lcom/nimbusds/jose/jwk/RSAKey$OtherPrimesInfo;)Lcom/nimbusds/jose/util/Base64URL;

    move-result-object v6

    invoke-virtual {v6}, Lcom/nimbusds/jose/util/Base64URL;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Lnet/minidev/json/JSONObject;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1658
    invoke-virtual {v0, v2}, Lnet/minidev/json/JSONArray;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public toKeyPair()Ljava/security/KeyPair;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nimbusds/jose/JOSEException;
        }
    .end annotation

    .prologue
    .line 1583
    new-instance v0, Ljava/security/KeyPair;

    invoke-virtual {p0}, Lcom/nimbusds/jose/jwk/RSAKey;->toRSAPublicKey()Ljava/security/interfaces/RSAPublicKey;

    move-result-object v1

    invoke-virtual {p0}, Lcom/nimbusds/jose/jwk/RSAKey;->toRSAPrivateKey()Ljava/security/interfaces/RSAPrivateKey;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/security/KeyPair;-><init>(Ljava/security/PublicKey;Ljava/security/PrivateKey;)V

    return-object v0
.end method

.method public toPrivateKey()Ljava/security/PrivateKey;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nimbusds/jose/JOSEException;
        }
    .end annotation

    .prologue
    .line 1563
    invoke-virtual {p0}, Lcom/nimbusds/jose/jwk/RSAKey;->toRSAPrivateKey()Ljava/security/interfaces/RSAPrivateKey;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toPublicJWK()Lcom/nimbusds/jose/jwk/JWK;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lcom/nimbusds/jose/jwk/RSAKey;->toPublicJWK()Lcom/nimbusds/jose/jwk/RSAKey;

    move-result-object v0

    return-object v0
.end method

.method public toPublicJWK()Lcom/nimbusds/jose/jwk/RSAKey;
    .locals 10

    .prologue
    .line 1615
    new-instance v0, Lcom/nimbusds/jose/jwk/RSAKey;

    invoke-virtual {p0}, Lcom/nimbusds/jose/jwk/RSAKey;->getModulus()Lcom/nimbusds/jose/util/Base64URL;

    move-result-object v1

    invoke-virtual {p0}, Lcom/nimbusds/jose/jwk/RSAKey;->getPublicExponent()Lcom/nimbusds/jose/util/Base64URL;

    move-result-object v2

    .line 1616
    invoke-virtual {p0}, Lcom/nimbusds/jose/jwk/RSAKey;->getKeyUse()Lcom/nimbusds/jose/jwk/KeyUse;

    move-result-object v3

    invoke-virtual {p0}, Lcom/nimbusds/jose/jwk/RSAKey;->getKeyOperations()Ljava/util/Set;

    move-result-object v4

    invoke-virtual {p0}, Lcom/nimbusds/jose/jwk/RSAKey;->getAlgorithm()Lcom/nimbusds/jose/Algorithm;

    move-result-object v5

    invoke-virtual {p0}, Lcom/nimbusds/jose/jwk/RSAKey;->getKeyID()Ljava/lang/String;

    move-result-object v6

    .line 1617
    invoke-virtual {p0}, Lcom/nimbusds/jose/jwk/RSAKey;->getX509CertURL()Ljava/net/URI;

    move-result-object v7

    invoke-virtual {p0}, Lcom/nimbusds/jose/jwk/RSAKey;->getX509CertThumbprint()Lcom/nimbusds/jose/util/Base64URL;

    move-result-object v8

    invoke-virtual {p0}, Lcom/nimbusds/jose/jwk/RSAKey;->getX509CertChain()Ljava/util/List;

    move-result-object v9

    .line 1615
    invoke-direct/range {v0 .. v9}, Lcom/nimbusds/jose/jwk/RSAKey;-><init>(Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/jwk/KeyUse;Ljava/util/Set;Lcom/nimbusds/jose/Algorithm;Ljava/lang/String;Ljava/net/URI;Lcom/nimbusds/jose/util/Base64URL;Ljava/util/List;)V

    return-object v0
.end method

.method public toPublicKey()Ljava/security/PublicKey;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nimbusds/jose/JOSEException;
        }
    .end annotation

    .prologue
    .line 1555
    invoke-virtual {p0}, Lcom/nimbusds/jose/jwk/RSAKey;->toRSAPublicKey()Ljava/security/interfaces/RSAPublicKey;

    move-result-object v0

    return-object v0
.end method

.method public toRSAPrivateKey()Ljava/security/interfaces/RSAPrivateKey;
    .locals 23
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nimbusds/jose/JOSEException;
        }
    .end annotation

    .prologue
    .line 1477
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/nimbusds/jose/jwk/RSAKey;->d:Lcom/nimbusds/jose/util/Base64URL;

    move-object/from16 v21, v0

    if-nez v21, :cond_0

    .line 1479
    const/16 v21, 0x0

    .line 1542
    :goto_0
    return-object v21

    .line 1482
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/nimbusds/jose/jwk/RSAKey;->n:Lcom/nimbusds/jose/util/Base64URL;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/nimbusds/jose/util/Base64URL;->decodeToBigInteger()Ljava/math/BigInteger;

    move-result-object v5

    .line 1483
    .local v5, "modulus":Ljava/math/BigInteger;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/nimbusds/jose/jwk/RSAKey;->d:Lcom/nimbusds/jose/util/Base64URL;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/nimbusds/jose/util/Base64URL;->decodeToBigInteger()Ljava/math/BigInteger;

    move-result-object v7

    .line 1487
    .local v7, "privateExponent":Ljava/math/BigInteger;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/nimbusds/jose/jwk/RSAKey;->p:Lcom/nimbusds/jose/util/Base64URL;

    move-object/from16 v21, v0

    if-nez v21, :cond_1

    .line 1489
    new-instance v4, Ljava/security/spec/RSAPrivateKeySpec;

    invoke-direct {v4, v5, v7}, Ljava/security/spec/RSAPrivateKeySpec;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    .line 1540
    .local v4, "spec":Ljava/security/spec/RSAPrivateKeySpec;
    :goto_1
    :try_start_0
    const-string v21, "RSA"

    invoke-static/range {v21 .. v21}, Ljava/security/KeyFactory;->getInstance(Ljava/lang/String;)Ljava/security/KeyFactory;

    move-result-object v15

    .line 1542
    .local v15, "factory":Ljava/security/KeyFactory;
    invoke-virtual {v15, v4}, Ljava/security/KeyFactory;->generatePrivate(Ljava/security/spec/KeySpec;)Ljava/security/PrivateKey;

    move-result-object v21

    check-cast v21, Ljava/security/interfaces/RSAPrivateKey;
    :try_end_0
    .catch Ljava/security/spec/InvalidKeySpecException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1493
    .end local v4    # "spec":Ljava/security/spec/RSAPrivateKeySpec;
    .end local v15    # "factory":Ljava/security/KeyFactory;
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/nimbusds/jose/jwk/RSAKey;->e:Lcom/nimbusds/jose/util/Base64URL;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/nimbusds/jose/util/Base64URL;->decodeToBigInteger()Ljava/math/BigInteger;

    move-result-object v6

    .line 1494
    .local v6, "publicExponent":Ljava/math/BigInteger;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/nimbusds/jose/jwk/RSAKey;->p:Lcom/nimbusds/jose/util/Base64URL;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/nimbusds/jose/util/Base64URL;->decodeToBigInteger()Ljava/math/BigInteger;

    move-result-object v8

    .line 1495
    .local v8, "primeP":Ljava/math/BigInteger;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/nimbusds/jose/jwk/RSAKey;->q:Lcom/nimbusds/jose/util/Base64URL;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/nimbusds/jose/util/Base64URL;->decodeToBigInteger()Ljava/math/BigInteger;

    move-result-object v9

    .line 1496
    .local v9, "primeQ":Ljava/math/BigInteger;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/nimbusds/jose/jwk/RSAKey;->dp:Lcom/nimbusds/jose/util/Base64URL;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/nimbusds/jose/util/Base64URL;->decodeToBigInteger()Ljava/math/BigInteger;

    move-result-object v10

    .line 1497
    .local v10, "primeExponentP":Ljava/math/BigInteger;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/nimbusds/jose/jwk/RSAKey;->dq:Lcom/nimbusds/jose/util/Base64URL;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/nimbusds/jose/util/Base64URL;->decodeToBigInteger()Ljava/math/BigInteger;

    move-result-object v11

    .line 1498
    .local v11, "primeExponentQ":Ljava/math/BigInteger;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/nimbusds/jose/jwk/RSAKey;->qi:Lcom/nimbusds/jose/util/Base64URL;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/nimbusds/jose/util/Base64URL;->decodeToBigInteger()Ljava/math/BigInteger;

    move-result-object v12

    .line 1500
    .local v12, "crtCoefficient":Ljava/math/BigInteger;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/nimbusds/jose/jwk/RSAKey;->oth:Ljava/util/List;

    move-object/from16 v21, v0

    if-eqz v21, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/nimbusds/jose/jwk/RSAKey;->oth:Ljava/util/List;

    move-object/from16 v21, v0

    invoke-interface/range {v21 .. v21}, Ljava/util/List;->isEmpty()Z

    move-result v21

    if-nez v21, :cond_3

    .line 1502
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/nimbusds/jose/jwk/RSAKey;->oth:Ljava/util/List;

    move-object/from16 v21, v0

    invoke-interface/range {v21 .. v21}, Ljava/util/List;->size()I

    move-result v21

    move/from16 v0, v21

    new-array v13, v0, [Ljava/security/spec/RSAOtherPrimeInfo;

    .line 1504
    .local v13, "otherInfo":[Ljava/security/spec/RSAOtherPrimeInfo;
    const/16 v16, 0x0

    .local v16, "i":I
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/nimbusds/jose/jwk/RSAKey;->oth:Ljava/util/List;

    move-object/from16 v21, v0

    invoke-interface/range {v21 .. v21}, Ljava/util/List;->size()I

    move-result v21

    move/from16 v0, v16

    move/from16 v1, v21

    if-lt v0, v1, :cond_2

    .line 1517
    new-instance v4, Ljava/security/spec/RSAMultiPrimePrivateCrtKeySpec;

    invoke-direct/range {v4 .. v13}, Ljava/security/spec/RSAMultiPrimePrivateCrtKeySpec;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;[Ljava/security/spec/RSAOtherPrimeInfo;)V

    .line 1526
    .restart local v4    # "spec":Ljava/security/spec/RSAPrivateKeySpec;
    goto/16 :goto_1

    .line 1506
    .end local v4    # "spec":Ljava/security/spec/RSAPrivateKeySpec;
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/nimbusds/jose/jwk/RSAKey;->oth:Ljava/util/List;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/nimbusds/jose/jwk/RSAKey$OtherPrimesInfo;

    .line 1508
    .local v17, "opi":Lcom/nimbusds/jose/jwk/RSAKey$OtherPrimesInfo;
    invoke-virtual/range {v17 .. v17}, Lcom/nimbusds/jose/jwk/RSAKey$OtherPrimesInfo;->getPrimeFactor()Lcom/nimbusds/jose/util/Base64URL;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/nimbusds/jose/util/Base64URL;->decodeToBigInteger()Ljava/math/BigInteger;

    move-result-object v19

    .line 1509
    .local v19, "otherPrime":Ljava/math/BigInteger;
    invoke-virtual/range {v17 .. v17}, Lcom/nimbusds/jose/jwk/RSAKey$OtherPrimesInfo;->getFactorCRTExponent()Lcom/nimbusds/jose/util/Base64URL;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/nimbusds/jose/util/Base64URL;->decodeToBigInteger()Ljava/math/BigInteger;

    move-result-object v20

    .line 1510
    .local v20, "otherPrimeExponent":Ljava/math/BigInteger;
    invoke-virtual/range {v17 .. v17}, Lcom/nimbusds/jose/jwk/RSAKey$OtherPrimesInfo;->getFactorCRTCoefficient()Lcom/nimbusds/jose/util/Base64URL;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/nimbusds/jose/util/Base64URL;->decodeToBigInteger()Ljava/math/BigInteger;

    move-result-object v18

    .line 1512
    .local v18, "otherCrtCoefficient":Ljava/math/BigInteger;
    new-instance v21, Ljava/security/spec/RSAOtherPrimeInfo;

    .line 1514
    move-object/from16 v0, v21

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    move-object/from16 v3, v18

    invoke-direct {v0, v1, v2, v3}, Ljava/security/spec/RSAOtherPrimeInfo;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    .line 1512
    aput-object v21, v13, v16

    .line 1504
    add-int/lit8 v16, v16, 0x1

    goto :goto_2

    .line 1528
    .end local v13    # "otherInfo":[Ljava/security/spec/RSAOtherPrimeInfo;
    .end local v16    # "i":I
    .end local v17    # "opi":Lcom/nimbusds/jose/jwk/RSAKey$OtherPrimesInfo;
    .end local v18    # "otherCrtCoefficient":Ljava/math/BigInteger;
    .end local v19    # "otherPrime":Ljava/math/BigInteger;
    .end local v20    # "otherPrimeExponent":Ljava/math/BigInteger;
    :cond_3
    new-instance v4, Ljava/security/spec/RSAPrivateCrtKeySpec;

    invoke-direct/range {v4 .. v12}, Ljava/security/spec/RSAPrivateCrtKeySpec;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    .restart local v4    # "spec":Ljava/security/spec/RSAPrivateKeySpec;
    goto/16 :goto_1

    .line 1544
    .end local v6    # "publicExponent":Ljava/math/BigInteger;
    .end local v8    # "primeP":Ljava/math/BigInteger;
    .end local v9    # "primeQ":Ljava/math/BigInteger;
    .end local v10    # "primeExponentP":Ljava/math/BigInteger;
    .end local v11    # "primeExponentQ":Ljava/math/BigInteger;
    .end local v12    # "crtCoefficient":Ljava/math/BigInteger;
    :catch_0
    move-exception v14

    .line 1546
    .local v14, "e":Ljava/security/GeneralSecurityException;
    :goto_3
    new-instance v21, Lcom/nimbusds/jose/JOSEException;

    invoke-virtual {v14}, Ljava/security/GeneralSecurityException;->getMessage()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-direct {v0, v1, v14}, Lcom/nimbusds/jose/JOSEException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v21

    .line 1544
    .end local v14    # "e":Ljava/security/GeneralSecurityException;
    :catch_1
    move-exception v14

    goto :goto_3
.end method

.method public toRSAPublicKey()Ljava/security/interfaces/RSAPublicKey;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nimbusds/jose/JOSEException;
        }
    .end annotation

    .prologue
    .line 1446
    iget-object v5, p0, Lcom/nimbusds/jose/jwk/RSAKey;->n:Lcom/nimbusds/jose/util/Base64URL;

    invoke-virtual {v5}, Lcom/nimbusds/jose/util/Base64URL;->decodeToBigInteger()Ljava/math/BigInteger;

    move-result-object v3

    .line 1447
    .local v3, "modulus":Ljava/math/BigInteger;
    iget-object v5, p0, Lcom/nimbusds/jose/jwk/RSAKey;->e:Lcom/nimbusds/jose/util/Base64URL;

    invoke-virtual {v5}, Lcom/nimbusds/jose/util/Base64URL;->decodeToBigInteger()Ljava/math/BigInteger;

    move-result-object v1

    .line 1449
    .local v1, "exponent":Ljava/math/BigInteger;
    new-instance v4, Ljava/security/spec/RSAPublicKeySpec;

    invoke-direct {v4, v3, v1}, Ljava/security/spec/RSAPublicKeySpec;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    .line 1452
    .local v4, "spec":Ljava/security/spec/RSAPublicKeySpec;
    :try_start_0
    const-string v5, "RSA"

    invoke-static {v5}, Ljava/security/KeyFactory;->getInstance(Ljava/lang/String;)Ljava/security/KeyFactory;

    move-result-object v2

    .line 1454
    .local v2, "factory":Ljava/security/KeyFactory;
    invoke-virtual {v2, v4}, Ljava/security/KeyFactory;->generatePublic(Ljava/security/spec/KeySpec;)Ljava/security/PublicKey;

    move-result-object v5

    check-cast v5, Ljava/security/interfaces/RSAPublicKey;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/spec/InvalidKeySpecException; {:try_start_0 .. :try_end_0} :catch_1

    return-object v5

    .line 1456
    .end local v2    # "factory":Ljava/security/KeyFactory;
    :catch_0
    move-exception v0

    .line 1458
    .local v0, "e":Ljava/security/GeneralSecurityException;
    :goto_0
    new-instance v5, Lcom/nimbusds/jose/JOSEException;

    invoke-virtual {v0}, Ljava/security/GeneralSecurityException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6, v0}, Lcom/nimbusds/jose/JOSEException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5

    .line 1456
    .end local v0    # "e":Ljava/security/GeneralSecurityException;
    :catch_1
    move-exception v0

    goto :goto_0
.end method

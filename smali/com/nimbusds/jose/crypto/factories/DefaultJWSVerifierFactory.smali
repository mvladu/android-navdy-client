.class public Lcom/nimbusds/jose/crypto/factories/DefaultJWSVerifierFactory;
.super Ljava/lang/Object;
.source "DefaultJWSVerifierFactory.java"

# interfaces
.implements Lcom/nimbusds/jose/proc/JWSVerifierFactory;


# annotations
.annotation runtime Lnet/jcip/annotations/ThreadSafe;
.end annotation


# static fields
.field public static final SUPPORTED_ALGORITHMS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/nimbusds/jose/JWSAlgorithm;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final jcaContext:Lcom/nimbusds/jose/jca/JCAContext;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 41
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    .line 42
    .local v0, "algs":Ljava/util/Set;, "Ljava/util/Set<Lcom/nimbusds/jose/JWSAlgorithm;>;"
    sget-object v1, Lcom/nimbusds/jose/crypto/MACVerifier;->SUPPORTED_ALGORITHMS:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 43
    sget-object v1, Lcom/nimbusds/jose/crypto/RSASSAVerifier;->SUPPORTED_ALGORITHMS:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 44
    sget-object v1, Lcom/nimbusds/jose/crypto/ECDSAVerifier;->SUPPORTED_ALGORITHMS:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 45
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    sput-object v1, Lcom/nimbusds/jose/crypto/factories/DefaultJWSVerifierFactory;->SUPPORTED_ALGORITHMS:Ljava/util/Set;

    .line 46
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    new-instance v0, Lcom/nimbusds/jose/jca/JCAContext;

    invoke-direct {v0}, Lcom/nimbusds/jose/jca/JCAContext;-><init>()V

    iput-object v0, p0, Lcom/nimbusds/jose/crypto/factories/DefaultJWSVerifierFactory;->jcaContext:Lcom/nimbusds/jose/jca/JCAContext;

    .line 31
    return-void
.end method


# virtual methods
.method public createJWSVerifier(Lcom/nimbusds/jose/JWSHeader;Ljava/security/Key;)Lcom/nimbusds/jose/JWSVerifier;
    .locals 7
    .param p1, "header"    # Lcom/nimbusds/jose/JWSHeader;
    .param p2, "key"    # Ljava/security/Key;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nimbusds/jose/JOSEException;
        }
    .end annotation

    .prologue
    .line 75
    sget-object v4, Lcom/nimbusds/jose/crypto/MACVerifier;->SUPPORTED_ALGORITHMS:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/nimbusds/jose/JWSHeader;->getAlgorithm()Lcom/nimbusds/jose/JWSAlgorithm;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 77
    instance-of v4, p2, Ljavax/crypto/SecretKey;

    if-nez v4, :cond_0

    .line 78
    new-instance v4, Lcom/nimbusds/jose/KeyTypeException;

    const-class v5, Ljavax/crypto/SecretKey;

    invoke-direct {v4, v5}, Lcom/nimbusds/jose/KeyTypeException;-><init>(Ljava/lang/Class;)V

    throw v4

    :cond_0
    move-object v1, p2

    .line 81
    check-cast v1, Ljavax/crypto/SecretKey;

    .line 83
    .local v1, "macKey":Ljavax/crypto/SecretKey;
    new-instance v3, Lcom/nimbusds/jose/crypto/MACVerifier;

    invoke-direct {v3, v1}, Lcom/nimbusds/jose/crypto/MACVerifier;-><init>(Ljavax/crypto/SecretKey;)V

    .line 111
    .end local v1    # "macKey":Ljavax/crypto/SecretKey;
    .local v3, "verifier":Lcom/nimbusds/jose/JWSVerifier;
    :goto_0
    invoke-interface {v3}, Lcom/nimbusds/jose/JWSVerifier;->getJCAContext()Lcom/nimbusds/jose/jca/JCAContext;

    move-result-object v4

    iget-object v5, p0, Lcom/nimbusds/jose/crypto/factories/DefaultJWSVerifierFactory;->jcaContext:Lcom/nimbusds/jose/jca/JCAContext;

    invoke-virtual {v5}, Lcom/nimbusds/jose/jca/JCAContext;->getSecureRandom()Ljava/security/SecureRandom;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/nimbusds/jose/jca/JCAContext;->setSecureRandom(Ljava/security/SecureRandom;)V

    .line 112
    invoke-interface {v3}, Lcom/nimbusds/jose/JWSVerifier;->getJCAContext()Lcom/nimbusds/jose/jca/JCAContext;

    move-result-object v4

    iget-object v5, p0, Lcom/nimbusds/jose/crypto/factories/DefaultJWSVerifierFactory;->jcaContext:Lcom/nimbusds/jose/jca/JCAContext;

    invoke-virtual {v5}, Lcom/nimbusds/jose/jca/JCAContext;->getProvider()Ljava/security/Provider;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/nimbusds/jose/jca/JCAContext;->setProvider(Ljava/security/Provider;)V

    .line 114
    return-object v3

    .line 85
    .end local v3    # "verifier":Lcom/nimbusds/jose/JWSVerifier;
    :cond_1
    sget-object v4, Lcom/nimbusds/jose/crypto/RSASSAVerifier;->SUPPORTED_ALGORITHMS:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/nimbusds/jose/JWSHeader;->getAlgorithm()Lcom/nimbusds/jose/JWSAlgorithm;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 87
    instance-of v4, p2, Ljava/security/interfaces/RSAPublicKey;

    if-nez v4, :cond_2

    .line 88
    new-instance v4, Lcom/nimbusds/jose/KeyTypeException;

    const-class v5, Ljava/security/interfaces/RSAPublicKey;

    invoke-direct {v4, v5}, Lcom/nimbusds/jose/KeyTypeException;-><init>(Ljava/lang/Class;)V

    throw v4

    :cond_2
    move-object v2, p2

    .line 91
    check-cast v2, Ljava/security/interfaces/RSAPublicKey;

    .line 93
    .local v2, "rsaPublicKey":Ljava/security/interfaces/RSAPublicKey;
    new-instance v3, Lcom/nimbusds/jose/crypto/RSASSAVerifier;

    invoke-direct {v3, v2}, Lcom/nimbusds/jose/crypto/RSASSAVerifier;-><init>(Ljava/security/interfaces/RSAPublicKey;)V

    .line 95
    .restart local v3    # "verifier":Lcom/nimbusds/jose/JWSVerifier;
    goto :goto_0

    .end local v2    # "rsaPublicKey":Ljava/security/interfaces/RSAPublicKey;
    .end local v3    # "verifier":Lcom/nimbusds/jose/JWSVerifier;
    :cond_3
    sget-object v4, Lcom/nimbusds/jose/crypto/ECDSAVerifier;->SUPPORTED_ALGORITHMS:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/nimbusds/jose/JWSHeader;->getAlgorithm()Lcom/nimbusds/jose/JWSAlgorithm;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 97
    instance-of v4, p2, Ljava/security/interfaces/ECPublicKey;

    if-nez v4, :cond_4

    .line 98
    new-instance v4, Lcom/nimbusds/jose/KeyTypeException;

    const-class v5, Ljava/security/interfaces/ECPublicKey;

    invoke-direct {v4, v5}, Lcom/nimbusds/jose/KeyTypeException;-><init>(Ljava/lang/Class;)V

    throw v4

    :cond_4
    move-object v0, p2

    .line 101
    check-cast v0, Ljava/security/interfaces/ECPublicKey;

    .line 103
    .local v0, "ecPublicKey":Ljava/security/interfaces/ECPublicKey;
    new-instance v3, Lcom/nimbusds/jose/crypto/ECDSAVerifier;

    invoke-direct {v3, v0}, Lcom/nimbusds/jose/crypto/ECDSAVerifier;-><init>(Ljava/security/interfaces/ECPublicKey;)V

    .line 105
    .restart local v3    # "verifier":Lcom/nimbusds/jose/JWSVerifier;
    goto :goto_0

    .line 107
    .end local v0    # "ecPublicKey":Ljava/security/interfaces/ECPublicKey;
    .end local v3    # "verifier":Lcom/nimbusds/jose/JWSVerifier;
    :cond_5
    new-instance v4, Lcom/nimbusds/jose/JOSEException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Unsupported JWS algorithm: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/nimbusds/jose/JWSHeader;->getAlgorithm()Lcom/nimbusds/jose/JWSAlgorithm;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/nimbusds/jose/JOSEException;-><init>(Ljava/lang/String;)V

    throw v4
.end method

.method public getJCAContext()Lcom/nimbusds/jose/jca/JCAContext;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/nimbusds/jose/crypto/factories/DefaultJWSVerifierFactory;->jcaContext:Lcom/nimbusds/jose/jca/JCAContext;

    return-object v0
.end method

.method public supportedJWSAlgorithms()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/nimbusds/jose/JWSAlgorithm;",
            ">;"
        }
    .end annotation

    .prologue
    .line 58
    sget-object v0, Lcom/nimbusds/jose/crypto/factories/DefaultJWSVerifierFactory;->SUPPORTED_ALGORITHMS:Ljava/util/Set;

    return-object v0
.end method

.class public final Lcom/navdy/service/library/events/connection/ConnectionStateChange;
.super Lcom/squareup/wire/Message;
.source "ConnectionStateChange.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;,
        Lcom/navdy/service/library/events/connection/ConnectionStateChange$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_REMOTEDEVICEID:Ljava/lang/String; = ""

.field public static final DEFAULT_STATE:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

.field private static final serialVersionUID:J


# instance fields
.field public final remoteDeviceId:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final state:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->CONNECTION_CONNECTED:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    sput-object v0, Lcom/navdy/service/library/events/connection/ConnectionStateChange;->DEFAULT_STATE:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/connection/ConnectionStateChange$Builder;)V
    .locals 2
    .param p1, "builder"    # Lcom/navdy/service/library/events/connection/ConnectionStateChange$Builder;

    .prologue
    .line 35
    iget-object v0, p1, Lcom/navdy/service/library/events/connection/ConnectionStateChange$Builder;->remoteDeviceId:Ljava/lang/String;

    iget-object v1, p1, Lcom/navdy/service/library/events/connection/ConnectionStateChange$Builder;->state:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    invoke-direct {p0, v0, v1}, Lcom/navdy/service/library/events/connection/ConnectionStateChange;-><init>(Ljava/lang/String;Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;)V

    .line 36
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/connection/ConnectionStateChange;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 37
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/connection/ConnectionStateChange$Builder;Lcom/navdy/service/library/events/connection/ConnectionStateChange$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/connection/ConnectionStateChange$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/connection/ConnectionStateChange$1;

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/connection/ConnectionStateChange;-><init>(Lcom/navdy/service/library/events/connection/ConnectionStateChange$Builder;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;)V
    .locals 0
    .param p1, "remoteDeviceId"    # Ljava/lang/String;
    .param p2, "state"    # Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/navdy/service/library/events/connection/ConnectionStateChange;->remoteDeviceId:Ljava/lang/String;

    .line 31
    iput-object p2, p0, Lcom/navdy/service/library/events/connection/ConnectionStateChange;->state:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    .line 32
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 41
    if-ne p1, p0, :cond_1

    .line 45
    :cond_0
    :goto_0
    return v1

    .line 42
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/connection/ConnectionStateChange;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 43
    check-cast v0, Lcom/navdy/service/library/events/connection/ConnectionStateChange;

    .line 44
    .local v0, "o":Lcom/navdy/service/library/events/connection/ConnectionStateChange;
    iget-object v3, p0, Lcom/navdy/service/library/events/connection/ConnectionStateChange;->remoteDeviceId:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/connection/ConnectionStateChange;->remoteDeviceId:Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/connection/ConnectionStateChange;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/connection/ConnectionStateChange;->state:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    iget-object v4, v0, Lcom/navdy/service/library/events/connection/ConnectionStateChange;->state:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    .line 45
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/connection/ConnectionStateChange;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 50
    iget v0, p0, Lcom/navdy/service/library/events/connection/ConnectionStateChange;->hashCode:I

    .line 51
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 52
    iget-object v2, p0, Lcom/navdy/service/library/events/connection/ConnectionStateChange;->remoteDeviceId:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/connection/ConnectionStateChange;->remoteDeviceId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 53
    :goto_0
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/connection/ConnectionStateChange;->state:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/connection/ConnectionStateChange;->state:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 54
    iput v0, p0, Lcom/navdy/service/library/events/connection/ConnectionStateChange;->hashCode:I

    .line 56
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 52
    goto :goto_0
.end method

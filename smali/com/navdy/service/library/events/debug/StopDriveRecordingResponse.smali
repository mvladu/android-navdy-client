.class public final Lcom/navdy/service/library/events/debug/StopDriveRecordingResponse;
.super Lcom/squareup/wire/Message;
.source "StopDriveRecordingResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/debug/StopDriveRecordingResponse$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_STATUS:Lcom/navdy/service/library/events/RequestStatus;

.field private static final serialVersionUID:J


# instance fields
.field public final status:Lcom/navdy/service/library/events/RequestStatus;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    sput-object v0, Lcom/navdy/service/library/events/debug/StopDriveRecordingResponse;->DEFAULT_STATUS:Lcom/navdy/service/library/events/RequestStatus;

    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/RequestStatus;)V
    .locals 0
    .param p1, "status"    # Lcom/navdy/service/library/events/RequestStatus;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/navdy/service/library/events/debug/StopDriveRecordingResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    .line 29
    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/debug/StopDriveRecordingResponse$Builder;)V
    .locals 1
    .param p1, "builder"    # Lcom/navdy/service/library/events/debug/StopDriveRecordingResponse$Builder;

    .prologue
    .line 32
    iget-object v0, p1, Lcom/navdy/service/library/events/debug/StopDriveRecordingResponse$Builder;->status:Lcom/navdy/service/library/events/RequestStatus;

    invoke-direct {p0, v0}, Lcom/navdy/service/library/events/debug/StopDriveRecordingResponse;-><init>(Lcom/navdy/service/library/events/RequestStatus;)V

    .line 33
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/debug/StopDriveRecordingResponse;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 34
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/debug/StopDriveRecordingResponse$Builder;Lcom/navdy/service/library/events/debug/StopDriveRecordingResponse$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/debug/StopDriveRecordingResponse$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/debug/StopDriveRecordingResponse$1;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/debug/StopDriveRecordingResponse;-><init>(Lcom/navdy/service/library/events/debug/StopDriveRecordingResponse$Builder;)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    .line 38
    if-ne p1, p0, :cond_0

    const/4 v0, 0x1

    .line 40
    .end local p1    # "other":Ljava/lang/Object;
    :goto_0
    return v0

    .line 39
    .restart local p1    # "other":Ljava/lang/Object;
    :cond_0
    instance-of v0, p1, Lcom/navdy/service/library/events/debug/StopDriveRecordingResponse;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 40
    :cond_1
    iget-object v0, p0, Lcom/navdy/service/library/events/debug/StopDriveRecordingResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    check-cast p1, Lcom/navdy/service/library/events/debug/StopDriveRecordingResponse;

    .end local p1    # "other":Ljava/lang/Object;
    iget-object v1, p1, Lcom/navdy/service/library/events/debug/StopDriveRecordingResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    invoke-virtual {p0, v0, v1}, Lcom/navdy/service/library/events/debug/StopDriveRecordingResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 45
    iget v0, p0, Lcom/navdy/service/library/events/debug/StopDriveRecordingResponse;->hashCode:I

    .line 46
    .local v0, "result":I
    if-eqz v0, :cond_0

    .end local v0    # "result":I
    :goto_0
    return v0

    .restart local v0    # "result":I
    :cond_0
    iget-object v1, p0, Lcom/navdy/service/library/events/debug/StopDriveRecordingResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/navdy/service/library/events/debug/StopDriveRecordingResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/RequestStatus;->hashCode()I

    move-result v1

    :goto_1
    iput v1, p0, Lcom/navdy/service/library/events/debug/StopDriveRecordingResponse;->hashCode:I

    move v0, v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

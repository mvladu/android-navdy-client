.class public final Lcom/navdy/service/library/events/dial/DialBondRequest;
.super Lcom/squareup/wire/Message;
.source "DialBondRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;,
        Lcom/navdy/service/library/events/dial/DialBondRequest$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_ACTION:Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;

.field private static final serialVersionUID:J


# instance fields
.field public final action:Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;->DIAL_BOND:Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;

    sput-object v0, Lcom/navdy/service/library/events/dial/DialBondRequest;->DEFAULT_ACTION:Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/dial/DialBondRequest$Builder;)V
    .locals 1
    .param p1, "builder"    # Lcom/navdy/service/library/events/dial/DialBondRequest$Builder;

    .prologue
    .line 30
    iget-object v0, p1, Lcom/navdy/service/library/events/dial/DialBondRequest$Builder;->action:Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;

    invoke-direct {p0, v0}, Lcom/navdy/service/library/events/dial/DialBondRequest;-><init>(Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;)V

    .line 31
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/dial/DialBondRequest;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 32
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/dial/DialBondRequest$Builder;Lcom/navdy/service/library/events/dial/DialBondRequest$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/dial/DialBondRequest$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/dial/DialBondRequest$1;

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/dial/DialBondRequest;-><init>(Lcom/navdy/service/library/events/dial/DialBondRequest$Builder;)V

    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;)V
    .locals 0
    .param p1, "action"    # Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/navdy/service/library/events/dial/DialBondRequest;->action:Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;

    .line 27
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    .line 36
    if-ne p1, p0, :cond_0

    const/4 v0, 0x1

    .line 38
    .end local p1    # "other":Ljava/lang/Object;
    :goto_0
    return v0

    .line 37
    .restart local p1    # "other":Ljava/lang/Object;
    :cond_0
    instance-of v0, p1, Lcom/navdy/service/library/events/dial/DialBondRequest;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 38
    :cond_1
    iget-object v0, p0, Lcom/navdy/service/library/events/dial/DialBondRequest;->action:Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;

    check-cast p1, Lcom/navdy/service/library/events/dial/DialBondRequest;

    .end local p1    # "other":Ljava/lang/Object;
    iget-object v1, p1, Lcom/navdy/service/library/events/dial/DialBondRequest;->action:Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;

    invoke-virtual {p0, v0, v1}, Lcom/navdy/service/library/events/dial/DialBondRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 43
    iget v0, p0, Lcom/navdy/service/library/events/dial/DialBondRequest;->hashCode:I

    .line 44
    .local v0, "result":I
    if-eqz v0, :cond_0

    .end local v0    # "result":I
    :goto_0
    return v0

    .restart local v0    # "result":I
    :cond_0
    iget-object v1, p0, Lcom/navdy/service/library/events/dial/DialBondRequest;->action:Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/navdy/service/library/events/dial/DialBondRequest;->action:Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;->hashCode()I

    move-result v1

    :goto_1
    iput v1, p0, Lcom/navdy/service/library/events/dial/DialBondRequest;->hashCode:I

    move v0, v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

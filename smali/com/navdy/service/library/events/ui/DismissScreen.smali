.class public final Lcom/navdy/service/library/events/ui/DismissScreen;
.super Lcom/squareup/wire/Message;
.source "DismissScreen.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/ui/DismissScreen$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_SCREEN:Lcom/navdy/service/library/events/ui/Screen;

.field private static final serialVersionUID:J


# instance fields
.field public final screen:Lcom/navdy/service/library/events/ui/Screen;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_DASHBOARD:Lcom/navdy/service/library/events/ui/Screen;

    sput-object v0, Lcom/navdy/service/library/events/ui/DismissScreen;->DEFAULT_SCREEN:Lcom/navdy/service/library/events/ui/Screen;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/ui/DismissScreen$Builder;)V
    .locals 1
    .param p1, "builder"    # Lcom/navdy/service/library/events/ui/DismissScreen$Builder;

    .prologue
    .line 24
    iget-object v0, p1, Lcom/navdy/service/library/events/ui/DismissScreen$Builder;->screen:Lcom/navdy/service/library/events/ui/Screen;

    invoke-direct {p0, v0}, Lcom/navdy/service/library/events/ui/DismissScreen;-><init>(Lcom/navdy/service/library/events/ui/Screen;)V

    .line 25
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/ui/DismissScreen;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 26
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/ui/DismissScreen$Builder;Lcom/navdy/service/library/events/ui/DismissScreen$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/ui/DismissScreen$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/ui/DismissScreen$1;

    .prologue
    .line 11
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/ui/DismissScreen;-><init>(Lcom/navdy/service/library/events/ui/DismissScreen$Builder;)V

    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/ui/Screen;)V
    .locals 0
    .param p1, "screen"    # Lcom/navdy/service/library/events/ui/Screen;

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/navdy/service/library/events/ui/DismissScreen;->screen:Lcom/navdy/service/library/events/ui/Screen;

    .line 21
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    .line 30
    if-ne p1, p0, :cond_0

    const/4 v0, 0x1

    .line 32
    .end local p1    # "other":Ljava/lang/Object;
    :goto_0
    return v0

    .line 31
    .restart local p1    # "other":Ljava/lang/Object;
    :cond_0
    instance-of v0, p1, Lcom/navdy/service/library/events/ui/DismissScreen;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 32
    :cond_1
    iget-object v0, p0, Lcom/navdy/service/library/events/ui/DismissScreen;->screen:Lcom/navdy/service/library/events/ui/Screen;

    check-cast p1, Lcom/navdy/service/library/events/ui/DismissScreen;

    .end local p1    # "other":Ljava/lang/Object;
    iget-object v1, p1, Lcom/navdy/service/library/events/ui/DismissScreen;->screen:Lcom/navdy/service/library/events/ui/Screen;

    invoke-virtual {p0, v0, v1}, Lcom/navdy/service/library/events/ui/DismissScreen;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 37
    iget v0, p0, Lcom/navdy/service/library/events/ui/DismissScreen;->hashCode:I

    .line 38
    .local v0, "result":I
    if-eqz v0, :cond_0

    .end local v0    # "result":I
    :goto_0
    return v0

    .restart local v0    # "result":I
    :cond_0
    iget-object v1, p0, Lcom/navdy/service/library/events/ui/DismissScreen;->screen:Lcom/navdy/service/library/events/ui/Screen;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/navdy/service/library/events/ui/DismissScreen;->screen:Lcom/navdy/service/library/events/ui/Screen;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/ui/Screen;->hashCode()I

    move-result v1

    :goto_1
    iput v1, p0, Lcom/navdy/service/library/events/ui/DismissScreen;->hashCode:I

    move v0, v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.class public final Lcom/navdy/service/library/events/photo/PhotoResponse;
.super Lcom/squareup/wire/Message;
.source "PhotoResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/photo/PhotoResponse$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_IDENTIFIER:Ljava/lang/String; = ""

.field public static final DEFAULT_PHOTO:Lokio/ByteString;

.field public static final DEFAULT_PHOTOCHECKSUM:Ljava/lang/String; = ""

.field public static final DEFAULT_PHOTOTYPE:Lcom/navdy/service/library/events/photo/PhotoType;

.field public static final DEFAULT_STATUS:Lcom/navdy/service/library/events/RequestStatus;

.field public static final DEFAULT_STATUSDETAIL:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final identifier:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x4
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final photo:Lokio/ByteString;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->BYTES:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final photoChecksum:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x5
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final photoType:Lcom/navdy/service/library/events/photo/PhotoType;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x6
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final status:Lcom/navdy/service/library/events/RequestStatus;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final statusDetail:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x3
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    sput-object v0, Lcom/navdy/service/library/events/photo/PhotoResponse;->DEFAULT_PHOTO:Lokio/ByteString;

    .line 19
    sget-object v0, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    sput-object v0, Lcom/navdy/service/library/events/photo/PhotoResponse;->DEFAULT_STATUS:Lcom/navdy/service/library/events/RequestStatus;

    .line 23
    sget-object v0, Lcom/navdy/service/library/events/photo/PhotoType;->PHOTO_CONTACT:Lcom/navdy/service/library/events/photo/PhotoType;

    sput-object v0, Lcom/navdy/service/library/events/photo/PhotoResponse;->DEFAULT_PHOTOTYPE:Lcom/navdy/service/library/events/photo/PhotoType;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/photo/PhotoResponse$Builder;)V
    .locals 7
    .param p1, "builder"    # Lcom/navdy/service/library/events/photo/PhotoResponse$Builder;

    .prologue
    .line 68
    iget-object v1, p1, Lcom/navdy/service/library/events/photo/PhotoResponse$Builder;->photo:Lokio/ByteString;

    iget-object v2, p1, Lcom/navdy/service/library/events/photo/PhotoResponse$Builder;->status:Lcom/navdy/service/library/events/RequestStatus;

    iget-object v3, p1, Lcom/navdy/service/library/events/photo/PhotoResponse$Builder;->statusDetail:Ljava/lang/String;

    iget-object v4, p1, Lcom/navdy/service/library/events/photo/PhotoResponse$Builder;->identifier:Ljava/lang/String;

    iget-object v5, p1, Lcom/navdy/service/library/events/photo/PhotoResponse$Builder;->photoChecksum:Ljava/lang/String;

    iget-object v6, p1, Lcom/navdy/service/library/events/photo/PhotoResponse$Builder;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/navdy/service/library/events/photo/PhotoResponse;-><init>(Lokio/ByteString;Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/events/photo/PhotoType;)V

    .line 69
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/photo/PhotoResponse;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 70
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/photo/PhotoResponse$Builder;Lcom/navdy/service/library/events/photo/PhotoResponse$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/photo/PhotoResponse$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/photo/PhotoResponse$1;

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/photo/PhotoResponse;-><init>(Lcom/navdy/service/library/events/photo/PhotoResponse$Builder;)V

    return-void
.end method

.method public constructor <init>(Lokio/ByteString;Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/events/photo/PhotoType;)V
    .locals 0
    .param p1, "photo"    # Lokio/ByteString;
    .param p2, "status"    # Lcom/navdy/service/library/events/RequestStatus;
    .param p3, "statusDetail"    # Ljava/lang/String;
    .param p4, "identifier"    # Ljava/lang/String;
    .param p5, "photoChecksum"    # Ljava/lang/String;
    .param p6, "photoType"    # Lcom/navdy/service/library/events/photo/PhotoType;

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 59
    iput-object p1, p0, Lcom/navdy/service/library/events/photo/PhotoResponse;->photo:Lokio/ByteString;

    .line 60
    iput-object p2, p0, Lcom/navdy/service/library/events/photo/PhotoResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    .line 61
    iput-object p3, p0, Lcom/navdy/service/library/events/photo/PhotoResponse;->statusDetail:Ljava/lang/String;

    .line 62
    iput-object p4, p0, Lcom/navdy/service/library/events/photo/PhotoResponse;->identifier:Ljava/lang/String;

    .line 63
    iput-object p5, p0, Lcom/navdy/service/library/events/photo/PhotoResponse;->photoChecksum:Ljava/lang/String;

    .line 64
    iput-object p6, p0, Lcom/navdy/service/library/events/photo/PhotoResponse;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    .line 65
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 74
    if-ne p1, p0, :cond_1

    .line 82
    :cond_0
    :goto_0
    return v1

    .line 75
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/photo/PhotoResponse;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 76
    check-cast v0, Lcom/navdy/service/library/events/photo/PhotoResponse;

    .line 77
    .local v0, "o":Lcom/navdy/service/library/events/photo/PhotoResponse;
    iget-object v3, p0, Lcom/navdy/service/library/events/photo/PhotoResponse;->photo:Lokio/ByteString;

    iget-object v4, v0, Lcom/navdy/service/library/events/photo/PhotoResponse;->photo:Lokio/ByteString;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/photo/PhotoResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/photo/PhotoResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    iget-object v4, v0, Lcom/navdy/service/library/events/photo/PhotoResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    .line 78
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/photo/PhotoResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/photo/PhotoResponse;->statusDetail:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/photo/PhotoResponse;->statusDetail:Ljava/lang/String;

    .line 79
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/photo/PhotoResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/photo/PhotoResponse;->identifier:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/photo/PhotoResponse;->identifier:Ljava/lang/String;

    .line 80
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/photo/PhotoResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/photo/PhotoResponse;->photoChecksum:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/photo/PhotoResponse;->photoChecksum:Ljava/lang/String;

    .line 81
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/photo/PhotoResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/photo/PhotoResponse;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    iget-object v4, v0, Lcom/navdy/service/library/events/photo/PhotoResponse;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    .line 82
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/photo/PhotoResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 87
    iget v0, p0, Lcom/navdy/service/library/events/photo/PhotoResponse;->hashCode:I

    .line 88
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 89
    iget-object v2, p0, Lcom/navdy/service/library/events/photo/PhotoResponse;->photo:Lokio/ByteString;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/photo/PhotoResponse;->photo:Lokio/ByteString;

    invoke-virtual {v2}, Lokio/ByteString;->hashCode()I

    move-result v0

    .line 90
    :goto_0
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/photo/PhotoResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/photo/PhotoResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/RequestStatus;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 91
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/photo/PhotoResponse;->statusDetail:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/navdy/service/library/events/photo/PhotoResponse;->statusDetail:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_2
    add-int v0, v3, v2

    .line 92
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/photo/PhotoResponse;->identifier:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/navdy/service/library/events/photo/PhotoResponse;->identifier:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_3
    add-int v0, v3, v2

    .line 93
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/photo/PhotoResponse;->photoChecksum:Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/navdy/service/library/events/photo/PhotoResponse;->photoChecksum:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_4
    add-int v0, v3, v2

    .line 94
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/photo/PhotoResponse;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/photo/PhotoResponse;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/photo/PhotoType;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 95
    iput v0, p0, Lcom/navdy/service/library/events/photo/PhotoResponse;->hashCode:I

    .line 97
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 89
    goto :goto_0

    :cond_3
    move v2, v1

    .line 90
    goto :goto_1

    :cond_4
    move v2, v1

    .line 91
    goto :goto_2

    :cond_5
    move v2, v1

    .line 92
    goto :goto_3

    :cond_6
    move v2, v1

    .line 93
    goto :goto_4
.end method

.class public final Lcom/navdy/service/library/events/settings/ReadSettingsResponse;
.super Lcom/squareup/wire/Message;
.source "ReadSettingsResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/settings/ReadSettingsResponse$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_SETTINGS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/settings/Setting;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final screenConfiguration:Lcom/navdy/service/library/events/settings/ScreenConfiguration;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x1
    .end annotation
.end field

.field public final settings:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REPEATED:Lcom/squareup/wire/Message$Label;
        messageType = Lcom/navdy/service/library/events/settings/Setting;
        tag = 0x2
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/settings/Setting;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/settings/ReadSettingsResponse;->DEFAULT_SETTINGS:Ljava/util/List;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/settings/ReadSettingsResponse$Builder;)V
    .locals 2
    .param p1, "builder"    # Lcom/navdy/service/library/events/settings/ReadSettingsResponse$Builder;

    .prologue
    .line 35
    iget-object v0, p1, Lcom/navdy/service/library/events/settings/ReadSettingsResponse$Builder;->screenConfiguration:Lcom/navdy/service/library/events/settings/ScreenConfiguration;

    iget-object v1, p1, Lcom/navdy/service/library/events/settings/ReadSettingsResponse$Builder;->settings:Ljava/util/List;

    invoke-direct {p0, v0, v1}, Lcom/navdy/service/library/events/settings/ReadSettingsResponse;-><init>(Lcom/navdy/service/library/events/settings/ScreenConfiguration;Ljava/util/List;)V

    .line 36
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/settings/ReadSettingsResponse;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 37
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/settings/ReadSettingsResponse$Builder;Lcom/navdy/service/library/events/settings/ReadSettingsResponse$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/settings/ReadSettingsResponse$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/settings/ReadSettingsResponse$1;

    .prologue
    .line 13
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/settings/ReadSettingsResponse;-><init>(Lcom/navdy/service/library/events/settings/ReadSettingsResponse$Builder;)V

    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/settings/ScreenConfiguration;Ljava/util/List;)V
    .locals 1
    .param p1, "screenConfiguration"    # Lcom/navdy/service/library/events/settings/ScreenConfiguration;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/service/library/events/settings/ScreenConfiguration;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/settings/Setting;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 29
    .local p2, "settings":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/settings/Setting;>;"
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/navdy/service/library/events/settings/ReadSettingsResponse;->screenConfiguration:Lcom/navdy/service/library/events/settings/ScreenConfiguration;

    .line 31
    invoke-static {p2}, Lcom/navdy/service/library/events/settings/ReadSettingsResponse;->immutableCopyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/settings/ReadSettingsResponse;->settings:Ljava/util/List;

    .line 32
    return-void
.end method

.method static synthetic access$000(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Ljava/util/List;

    .prologue
    .line 13
    invoke-static {p0}, Lcom/navdy/service/library/events/settings/ReadSettingsResponse;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 41
    if-ne p1, p0, :cond_1

    .line 45
    :cond_0
    :goto_0
    return v1

    .line 42
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/settings/ReadSettingsResponse;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 43
    check-cast v0, Lcom/navdy/service/library/events/settings/ReadSettingsResponse;

    .line 44
    .local v0, "o":Lcom/navdy/service/library/events/settings/ReadSettingsResponse;
    iget-object v3, p0, Lcom/navdy/service/library/events/settings/ReadSettingsResponse;->screenConfiguration:Lcom/navdy/service/library/events/settings/ScreenConfiguration;

    iget-object v4, v0, Lcom/navdy/service/library/events/settings/ReadSettingsResponse;->screenConfiguration:Lcom/navdy/service/library/events/settings/ScreenConfiguration;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/settings/ReadSettingsResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/settings/ReadSettingsResponse;->settings:Ljava/util/List;

    iget-object v4, v0, Lcom/navdy/service/library/events/settings/ReadSettingsResponse;->settings:Ljava/util/List;

    .line 45
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/settings/ReadSettingsResponse;->equals(Ljava/util/List;Ljava/util/List;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 50
    iget v0, p0, Lcom/navdy/service/library/events/settings/ReadSettingsResponse;->hashCode:I

    .line 51
    .local v0, "result":I
    if-nez v0, :cond_0

    .line 52
    iget-object v1, p0, Lcom/navdy/service/library/events/settings/ReadSettingsResponse;->screenConfiguration:Lcom/navdy/service/library/events/settings/ScreenConfiguration;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/navdy/service/library/events/settings/ReadSettingsResponse;->screenConfiguration:Lcom/navdy/service/library/events/settings/ScreenConfiguration;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/settings/ScreenConfiguration;->hashCode()I

    move-result v0

    .line 53
    :goto_0
    mul-int/lit8 v2, v0, 0x25

    iget-object v1, p0, Lcom/navdy/service/library/events/settings/ReadSettingsResponse;->settings:Ljava/util/List;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/navdy/service/library/events/settings/ReadSettingsResponse;->settings:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    :goto_1
    add-int v0, v2, v1

    .line 54
    iput v0, p0, Lcom/navdy/service/library/events/settings/ReadSettingsResponse;->hashCode:I

    .line 56
    :cond_0
    return v0

    .line 52
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 53
    :cond_2
    const/4 v1, 0x1

    goto :goto_1
.end method

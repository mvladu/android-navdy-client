.class public final Lcom/navdy/service/library/events/file/FileTransferResponse;
.super Lcom/squareup/wire/Message;
.source "FileTransferResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_CHECKSUM:Ljava/lang/String; = ""

.field public static final DEFAULT_DESTINATIONFILENAME:Ljava/lang/String; = ""

.field public static final DEFAULT_ERROR:Lcom/navdy/service/library/events/file/FileTransferError;

.field public static final DEFAULT_FILETYPE:Lcom/navdy/service/library/events/file/FileType;

.field public static final DEFAULT_MAXCHUNKSIZE:Ljava/lang/Integer;

.field public static final DEFAULT_OFFSET:Ljava/lang/Long;

.field public static final DEFAULT_SUCCESS:Ljava/lang/Boolean;

.field public static final DEFAULT_SUPPORTSACKS:Ljava/lang/Boolean;

.field public static final DEFAULT_TRANSFERID:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final checksum:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x3
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final destinationFileName:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x8
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final error:Lcom/navdy/service/library/events/file/FileTransferError;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x6
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final fileType:Lcom/navdy/service/library/events/file/FileType;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x7
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final maxChunkSize:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x5
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT32:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final offset:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT64:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final success:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x4
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final supportsAcks:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x9
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final transferId:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT32:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 18
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/file/FileTransferResponse;->DEFAULT_TRANSFERID:Ljava/lang/Integer;

    .line 19
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/file/FileTransferResponse;->DEFAULT_OFFSET:Ljava/lang/Long;

    .line 21
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/file/FileTransferResponse;->DEFAULT_SUCCESS:Ljava/lang/Boolean;

    .line 22
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/file/FileTransferResponse;->DEFAULT_MAXCHUNKSIZE:Ljava/lang/Integer;

    .line 23
    sget-object v0, Lcom/navdy/service/library/events/file/FileTransferError;->FILE_TRANSFER_NO_ERROR:Lcom/navdy/service/library/events/file/FileTransferError;

    sput-object v0, Lcom/navdy/service/library/events/file/FileTransferResponse;->DEFAULT_ERROR:Lcom/navdy/service/library/events/file/FileTransferError;

    .line 24
    sget-object v0, Lcom/navdy/service/library/events/file/FileType;->FILE_TYPE_OTA:Lcom/navdy/service/library/events/file/FileType;

    sput-object v0, Lcom/navdy/service/library/events/file/FileTransferResponse;->DEFAULT_FILETYPE:Lcom/navdy/service/library/events/file/FileType;

    .line 26
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/file/FileTransferResponse;->DEFAULT_SUPPORTSACKS:Ljava/lang/Boolean;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;)V
    .locals 10
    .param p1, "builder"    # Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    .prologue
    .line 99
    iget-object v1, p1, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->transferId:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->offset:Ljava/lang/Long;

    iget-object v3, p1, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->checksum:Ljava/lang/String;

    iget-object v4, p1, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->success:Ljava/lang/Boolean;

    iget-object v5, p1, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->maxChunkSize:Ljava/lang/Integer;

    iget-object v6, p1, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->error:Lcom/navdy/service/library/events/file/FileTransferError;

    iget-object v7, p1, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->fileType:Lcom/navdy/service/library/events/file/FileType;

    iget-object v8, p1, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->destinationFileName:Ljava/lang/String;

    iget-object v9, p1, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->supportsAcks:Ljava/lang/Boolean;

    move-object v0, p0

    invoke-direct/range {v0 .. v9}, Lcom/navdy/service/library/events/file/FileTransferResponse;-><init>(Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Integer;Lcom/navdy/service/library/events/file/FileTransferError;Lcom/navdy/service/library/events/file/FileType;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 100
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/file/FileTransferResponse;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 101
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;Lcom/navdy/service/library/events/file/FileTransferResponse$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/file/FileTransferResponse$1;

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/file/FileTransferResponse;-><init>(Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Integer;Lcom/navdy/service/library/events/file/FileTransferError;Lcom/navdy/service/library/events/file/FileType;Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 0
    .param p1, "transferId"    # Ljava/lang/Integer;
    .param p2, "offset"    # Ljava/lang/Long;
    .param p3, "checksum"    # Ljava/lang/String;
    .param p4, "success"    # Ljava/lang/Boolean;
    .param p5, "maxChunkSize"    # Ljava/lang/Integer;
    .param p6, "error"    # Lcom/navdy/service/library/events/file/FileTransferError;
    .param p7, "fileType"    # Lcom/navdy/service/library/events/file/FileType;
    .param p8, "destinationFileName"    # Ljava/lang/String;
    .param p9, "supportsAcks"    # Ljava/lang/Boolean;

    .prologue
    .line 86
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 87
    iput-object p1, p0, Lcom/navdy/service/library/events/file/FileTransferResponse;->transferId:Ljava/lang/Integer;

    .line 88
    iput-object p2, p0, Lcom/navdy/service/library/events/file/FileTransferResponse;->offset:Ljava/lang/Long;

    .line 89
    iput-object p3, p0, Lcom/navdy/service/library/events/file/FileTransferResponse;->checksum:Ljava/lang/String;

    .line 90
    iput-object p4, p0, Lcom/navdy/service/library/events/file/FileTransferResponse;->success:Ljava/lang/Boolean;

    .line 91
    iput-object p5, p0, Lcom/navdy/service/library/events/file/FileTransferResponse;->maxChunkSize:Ljava/lang/Integer;

    .line 92
    iput-object p6, p0, Lcom/navdy/service/library/events/file/FileTransferResponse;->error:Lcom/navdy/service/library/events/file/FileTransferError;

    .line 93
    iput-object p7, p0, Lcom/navdy/service/library/events/file/FileTransferResponse;->fileType:Lcom/navdy/service/library/events/file/FileType;

    .line 94
    iput-object p8, p0, Lcom/navdy/service/library/events/file/FileTransferResponse;->destinationFileName:Ljava/lang/String;

    .line 95
    iput-object p9, p0, Lcom/navdy/service/library/events/file/FileTransferResponse;->supportsAcks:Ljava/lang/Boolean;

    .line 96
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 105
    if-ne p1, p0, :cond_1

    .line 116
    :cond_0
    :goto_0
    return v1

    .line 106
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/file/FileTransferResponse;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 107
    check-cast v0, Lcom/navdy/service/library/events/file/FileTransferResponse;

    .line 108
    .local v0, "o":Lcom/navdy/service/library/events/file/FileTransferResponse;
    iget-object v3, p0, Lcom/navdy/service/library/events/file/FileTransferResponse;->transferId:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/navdy/service/library/events/file/FileTransferResponse;->transferId:Ljava/lang/Integer;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/file/FileTransferResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/file/FileTransferResponse;->offset:Ljava/lang/Long;

    iget-object v4, v0, Lcom/navdy/service/library/events/file/FileTransferResponse;->offset:Ljava/lang/Long;

    .line 109
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/file/FileTransferResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/file/FileTransferResponse;->checksum:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/file/FileTransferResponse;->checksum:Ljava/lang/String;

    .line 110
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/file/FileTransferResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/file/FileTransferResponse;->success:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/file/FileTransferResponse;->success:Ljava/lang/Boolean;

    .line 111
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/file/FileTransferResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/file/FileTransferResponse;->maxChunkSize:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/navdy/service/library/events/file/FileTransferResponse;->maxChunkSize:Ljava/lang/Integer;

    .line 112
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/file/FileTransferResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/file/FileTransferResponse;->error:Lcom/navdy/service/library/events/file/FileTransferError;

    iget-object v4, v0, Lcom/navdy/service/library/events/file/FileTransferResponse;->error:Lcom/navdy/service/library/events/file/FileTransferError;

    .line 113
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/file/FileTransferResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/file/FileTransferResponse;->fileType:Lcom/navdy/service/library/events/file/FileType;

    iget-object v4, v0, Lcom/navdy/service/library/events/file/FileTransferResponse;->fileType:Lcom/navdy/service/library/events/file/FileType;

    .line 114
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/file/FileTransferResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/file/FileTransferResponse;->destinationFileName:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/file/FileTransferResponse;->destinationFileName:Ljava/lang/String;

    .line 115
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/file/FileTransferResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/file/FileTransferResponse;->supportsAcks:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/file/FileTransferResponse;->supportsAcks:Ljava/lang/Boolean;

    .line 116
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/file/FileTransferResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 121
    iget v0, p0, Lcom/navdy/service/library/events/file/FileTransferResponse;->hashCode:I

    .line 122
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 123
    iget-object v2, p0, Lcom/navdy/service/library/events/file/FileTransferResponse;->transferId:Ljava/lang/Integer;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/file/FileTransferResponse;->transferId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    .line 124
    :goto_0
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/file/FileTransferResponse;->offset:Ljava/lang/Long;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/file/FileTransferResponse;->offset:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 125
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/file/FileTransferResponse;->checksum:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/navdy/service/library/events/file/FileTransferResponse;->checksum:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_2
    add-int v0, v3, v2

    .line 126
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/file/FileTransferResponse;->success:Ljava/lang/Boolean;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/navdy/service/library/events/file/FileTransferResponse;->success:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :goto_3
    add-int v0, v3, v2

    .line 127
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/file/FileTransferResponse;->maxChunkSize:Ljava/lang/Integer;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/navdy/service/library/events/file/FileTransferResponse;->maxChunkSize:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :goto_4
    add-int v0, v3, v2

    .line 128
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/file/FileTransferResponse;->error:Lcom/navdy/service/library/events/file/FileTransferError;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/navdy/service/library/events/file/FileTransferResponse;->error:Lcom/navdy/service/library/events/file/FileTransferError;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/file/FileTransferError;->hashCode()I

    move-result v2

    :goto_5
    add-int v0, v3, v2

    .line 129
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/file/FileTransferResponse;->fileType:Lcom/navdy/service/library/events/file/FileType;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/navdy/service/library/events/file/FileTransferResponse;->fileType:Lcom/navdy/service/library/events/file/FileType;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/file/FileType;->hashCode()I

    move-result v2

    :goto_6
    add-int v0, v3, v2

    .line 130
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/file/FileTransferResponse;->destinationFileName:Ljava/lang/String;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/navdy/service/library/events/file/FileTransferResponse;->destinationFileName:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_7
    add-int v0, v3, v2

    .line 131
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/file/FileTransferResponse;->supportsAcks:Ljava/lang/Boolean;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/file/FileTransferResponse;->supportsAcks:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 132
    iput v0, p0, Lcom/navdy/service/library/events/file/FileTransferResponse;->hashCode:I

    .line 134
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 123
    goto :goto_0

    :cond_3
    move v2, v1

    .line 124
    goto :goto_1

    :cond_4
    move v2, v1

    .line 125
    goto :goto_2

    :cond_5
    move v2, v1

    .line 126
    goto :goto_3

    :cond_6
    move v2, v1

    .line 127
    goto :goto_4

    :cond_7
    move v2, v1

    .line 128
    goto :goto_5

    :cond_8
    move v2, v1

    .line 129
    goto :goto_6

    :cond_9
    move v2, v1

    .line 130
    goto :goto_7
.end method

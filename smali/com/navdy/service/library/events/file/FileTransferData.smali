.class public final Lcom/navdy/service/library/events/file/FileTransferData;
.super Lcom/squareup/wire/Message;
.source "FileTransferData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/file/FileTransferData$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_CHUNKINDEX:Ljava/lang/Integer;

.field public static final DEFAULT_DATABYTES:Lokio/ByteString;

.field public static final DEFAULT_FILECHECKSUM:Ljava/lang/String; = ""

.field public static final DEFAULT_LASTCHUNK:Ljava/lang/Boolean;

.field public static final DEFAULT_TRANSFERID:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final chunkIndex:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT32:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final dataBytes:Lokio/ByteString;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x3
        type = .enum Lcom/squareup/wire/Message$Datatype;->BYTES:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final fileCheckSum:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x5
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final lastChunk:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x4
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final transferId:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT32:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 18
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/file/FileTransferData;->DEFAULT_TRANSFERID:Ljava/lang/Integer;

    .line 19
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/file/FileTransferData;->DEFAULT_CHUNKINDEX:Ljava/lang/Integer;

    .line 20
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    sput-object v0, Lcom/navdy/service/library/events/file/FileTransferData;->DEFAULT_DATABYTES:Lokio/ByteString;

    .line 21
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/file/FileTransferData;->DEFAULT_LASTCHUNK:Ljava/lang/Boolean;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/file/FileTransferData$Builder;)V
    .locals 6
    .param p1, "builder"    # Lcom/navdy/service/library/events/file/FileTransferData$Builder;

    .prologue
    .line 58
    iget-object v1, p1, Lcom/navdy/service/library/events/file/FileTransferData$Builder;->transferId:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/navdy/service/library/events/file/FileTransferData$Builder;->chunkIndex:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/navdy/service/library/events/file/FileTransferData$Builder;->dataBytes:Lokio/ByteString;

    iget-object v4, p1, Lcom/navdy/service/library/events/file/FileTransferData$Builder;->lastChunk:Ljava/lang/Boolean;

    iget-object v5, p1, Lcom/navdy/service/library/events/file/FileTransferData$Builder;->fileCheckSum:Ljava/lang/String;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/navdy/service/library/events/file/FileTransferData;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;Ljava/lang/Boolean;Ljava/lang/String;)V

    .line 59
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/file/FileTransferData;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 60
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/file/FileTransferData$Builder;Lcom/navdy/service/library/events/file/FileTransferData$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/file/FileTransferData$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/file/FileTransferData$1;

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/file/FileTransferData;-><init>(Lcom/navdy/service/library/events/file/FileTransferData$Builder;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;Ljava/lang/Boolean;Ljava/lang/String;)V
    .locals 0
    .param p1, "transferId"    # Ljava/lang/Integer;
    .param p2, "chunkIndex"    # Ljava/lang/Integer;
    .param p3, "dataBytes"    # Lokio/ByteString;
    .param p4, "lastChunk"    # Ljava/lang/Boolean;
    .param p5, "fileCheckSum"    # Ljava/lang/String;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 50
    iput-object p1, p0, Lcom/navdy/service/library/events/file/FileTransferData;->transferId:Ljava/lang/Integer;

    .line 51
    iput-object p2, p0, Lcom/navdy/service/library/events/file/FileTransferData;->chunkIndex:Ljava/lang/Integer;

    .line 52
    iput-object p3, p0, Lcom/navdy/service/library/events/file/FileTransferData;->dataBytes:Lokio/ByteString;

    .line 53
    iput-object p4, p0, Lcom/navdy/service/library/events/file/FileTransferData;->lastChunk:Ljava/lang/Boolean;

    .line 54
    iput-object p5, p0, Lcom/navdy/service/library/events/file/FileTransferData;->fileCheckSum:Ljava/lang/String;

    .line 55
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 64
    if-ne p1, p0, :cond_1

    .line 71
    :cond_0
    :goto_0
    return v1

    .line 65
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/file/FileTransferData;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 66
    check-cast v0, Lcom/navdy/service/library/events/file/FileTransferData;

    .line 67
    .local v0, "o":Lcom/navdy/service/library/events/file/FileTransferData;
    iget-object v3, p0, Lcom/navdy/service/library/events/file/FileTransferData;->transferId:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/navdy/service/library/events/file/FileTransferData;->transferId:Ljava/lang/Integer;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/file/FileTransferData;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/file/FileTransferData;->chunkIndex:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/navdy/service/library/events/file/FileTransferData;->chunkIndex:Ljava/lang/Integer;

    .line 68
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/file/FileTransferData;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/file/FileTransferData;->dataBytes:Lokio/ByteString;

    iget-object v4, v0, Lcom/navdy/service/library/events/file/FileTransferData;->dataBytes:Lokio/ByteString;

    .line 69
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/file/FileTransferData;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/file/FileTransferData;->lastChunk:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/file/FileTransferData;->lastChunk:Ljava/lang/Boolean;

    .line 70
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/file/FileTransferData;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/file/FileTransferData;->fileCheckSum:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/file/FileTransferData;->fileCheckSum:Ljava/lang/String;

    .line 71
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/file/FileTransferData;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 76
    iget v0, p0, Lcom/navdy/service/library/events/file/FileTransferData;->hashCode:I

    .line 77
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 78
    iget-object v2, p0, Lcom/navdy/service/library/events/file/FileTransferData;->transferId:Ljava/lang/Integer;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/file/FileTransferData;->transferId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    .line 79
    :goto_0
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/file/FileTransferData;->chunkIndex:Ljava/lang/Integer;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/file/FileTransferData;->chunkIndex:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 80
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/file/FileTransferData;->dataBytes:Lokio/ByteString;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/navdy/service/library/events/file/FileTransferData;->dataBytes:Lokio/ByteString;

    invoke-virtual {v2}, Lokio/ByteString;->hashCode()I

    move-result v2

    :goto_2
    add-int v0, v3, v2

    .line 81
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/file/FileTransferData;->lastChunk:Ljava/lang/Boolean;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/navdy/service/library/events/file/FileTransferData;->lastChunk:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :goto_3
    add-int v0, v3, v2

    .line 82
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/file/FileTransferData;->fileCheckSum:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/file/FileTransferData;->fileCheckSum:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 83
    iput v0, p0, Lcom/navdy/service/library/events/file/FileTransferData;->hashCode:I

    .line 85
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 78
    goto :goto_0

    :cond_3
    move v2, v1

    .line 79
    goto :goto_1

    :cond_4
    move v2, v1

    .line 80
    goto :goto_2

    :cond_5
    move v2, v1

    .line 81
    goto :goto_3
.end method

.class public final Lcom/navdy/service/library/events/file/FileTransferStatus;
.super Lcom/squareup/wire/Message;
.source "FileTransferStatus.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_CHUNKINDEX:Ljava/lang/Integer;

.field public static final DEFAULT_ERROR:Lcom/navdy/service/library/events/file/FileTransferError;

.field public static final DEFAULT_SUCCESS:Ljava/lang/Boolean;

.field public static final DEFAULT_TOTALBYTESTRANSFERRED:Ljava/lang/Long;

.field public static final DEFAULT_TRANSFERCOMPLETE:Ljava/lang/Boolean;

.field public static final DEFAULT_TRANSFERID:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final chunkIndex:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x5
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT32:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final error:Lcom/navdy/service/library/events/file/FileTransferError;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x4
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final success:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final totalBytesTransferred:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x3
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT64:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final transferComplete:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x6
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final transferId:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT32:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 17
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/file/FileTransferStatus;->DEFAULT_TRANSFERID:Ljava/lang/Integer;

    .line 18
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/file/FileTransferStatus;->DEFAULT_SUCCESS:Ljava/lang/Boolean;

    .line 19
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/file/FileTransferStatus;->DEFAULT_TOTALBYTESTRANSFERRED:Ljava/lang/Long;

    .line 20
    sget-object v0, Lcom/navdy/service/library/events/file/FileTransferError;->FILE_TRANSFER_NO_ERROR:Lcom/navdy/service/library/events/file/FileTransferError;

    sput-object v0, Lcom/navdy/service/library/events/file/FileTransferStatus;->DEFAULT_ERROR:Lcom/navdy/service/library/events/file/FileTransferError;

    .line 21
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/file/FileTransferStatus;->DEFAULT_CHUNKINDEX:Ljava/lang/Integer;

    .line 22
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/file/FileTransferStatus;->DEFAULT_TRANSFERCOMPLETE:Ljava/lang/Boolean;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;)V
    .locals 7
    .param p1, "builder"    # Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;

    .prologue
    .line 70
    iget-object v1, p1, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->transferId:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->success:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->totalBytesTransferred:Ljava/lang/Long;

    iget-object v4, p1, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->error:Lcom/navdy/service/library/events/file/FileTransferError;

    iget-object v5, p1, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->chunkIndex:Ljava/lang/Integer;

    iget-object v6, p1, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->transferComplete:Ljava/lang/Boolean;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/navdy/service/library/events/file/FileTransferStatus;-><init>(Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Long;Lcom/navdy/service/library/events/file/FileTransferError;Ljava/lang/Integer;Ljava/lang/Boolean;)V

    .line 71
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/file/FileTransferStatus;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 72
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;Lcom/navdy/service/library/events/file/FileTransferStatus$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/file/FileTransferStatus$1;

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/file/FileTransferStatus;-><init>(Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Long;Lcom/navdy/service/library/events/file/FileTransferError;Ljava/lang/Integer;Ljava/lang/Boolean;)V
    .locals 0
    .param p1, "transferId"    # Ljava/lang/Integer;
    .param p2, "success"    # Ljava/lang/Boolean;
    .param p3, "totalBytesTransferred"    # Ljava/lang/Long;
    .param p4, "error"    # Lcom/navdy/service/library/events/file/FileTransferError;
    .param p5, "chunkIndex"    # Ljava/lang/Integer;
    .param p6, "transferComplete"    # Ljava/lang/Boolean;

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 61
    iput-object p1, p0, Lcom/navdy/service/library/events/file/FileTransferStatus;->transferId:Ljava/lang/Integer;

    .line 62
    iput-object p2, p0, Lcom/navdy/service/library/events/file/FileTransferStatus;->success:Ljava/lang/Boolean;

    .line 63
    iput-object p3, p0, Lcom/navdy/service/library/events/file/FileTransferStatus;->totalBytesTransferred:Ljava/lang/Long;

    .line 64
    iput-object p4, p0, Lcom/navdy/service/library/events/file/FileTransferStatus;->error:Lcom/navdy/service/library/events/file/FileTransferError;

    .line 65
    iput-object p5, p0, Lcom/navdy/service/library/events/file/FileTransferStatus;->chunkIndex:Ljava/lang/Integer;

    .line 66
    iput-object p6, p0, Lcom/navdy/service/library/events/file/FileTransferStatus;->transferComplete:Ljava/lang/Boolean;

    .line 67
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 76
    if-ne p1, p0, :cond_1

    .line 84
    :cond_0
    :goto_0
    return v1

    .line 77
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/file/FileTransferStatus;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 78
    check-cast v0, Lcom/navdy/service/library/events/file/FileTransferStatus;

    .line 79
    .local v0, "o":Lcom/navdy/service/library/events/file/FileTransferStatus;
    iget-object v3, p0, Lcom/navdy/service/library/events/file/FileTransferStatus;->transferId:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/navdy/service/library/events/file/FileTransferStatus;->transferId:Ljava/lang/Integer;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/file/FileTransferStatus;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/file/FileTransferStatus;->success:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/file/FileTransferStatus;->success:Ljava/lang/Boolean;

    .line 80
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/file/FileTransferStatus;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/file/FileTransferStatus;->totalBytesTransferred:Ljava/lang/Long;

    iget-object v4, v0, Lcom/navdy/service/library/events/file/FileTransferStatus;->totalBytesTransferred:Ljava/lang/Long;

    .line 81
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/file/FileTransferStatus;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/file/FileTransferStatus;->error:Lcom/navdy/service/library/events/file/FileTransferError;

    iget-object v4, v0, Lcom/navdy/service/library/events/file/FileTransferStatus;->error:Lcom/navdy/service/library/events/file/FileTransferError;

    .line 82
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/file/FileTransferStatus;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/file/FileTransferStatus;->chunkIndex:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/navdy/service/library/events/file/FileTransferStatus;->chunkIndex:Ljava/lang/Integer;

    .line 83
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/file/FileTransferStatus;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/file/FileTransferStatus;->transferComplete:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/file/FileTransferStatus;->transferComplete:Ljava/lang/Boolean;

    .line 84
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/file/FileTransferStatus;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 89
    iget v0, p0, Lcom/navdy/service/library/events/file/FileTransferStatus;->hashCode:I

    .line 90
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 91
    iget-object v2, p0, Lcom/navdy/service/library/events/file/FileTransferStatus;->transferId:Ljava/lang/Integer;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/file/FileTransferStatus;->transferId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    .line 92
    :goto_0
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/file/FileTransferStatus;->success:Ljava/lang/Boolean;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/file/FileTransferStatus;->success:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 93
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/file/FileTransferStatus;->totalBytesTransferred:Ljava/lang/Long;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/navdy/service/library/events/file/FileTransferStatus;->totalBytesTransferred:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->hashCode()I

    move-result v2

    :goto_2
    add-int v0, v3, v2

    .line 94
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/file/FileTransferStatus;->error:Lcom/navdy/service/library/events/file/FileTransferError;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/navdy/service/library/events/file/FileTransferStatus;->error:Lcom/navdy/service/library/events/file/FileTransferError;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/file/FileTransferError;->hashCode()I

    move-result v2

    :goto_3
    add-int v0, v3, v2

    .line 95
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/file/FileTransferStatus;->chunkIndex:Ljava/lang/Integer;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/navdy/service/library/events/file/FileTransferStatus;->chunkIndex:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :goto_4
    add-int v0, v3, v2

    .line 96
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/file/FileTransferStatus;->transferComplete:Ljava/lang/Boolean;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/file/FileTransferStatus;->transferComplete:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 97
    iput v0, p0, Lcom/navdy/service/library/events/file/FileTransferStatus;->hashCode:I

    .line 99
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 91
    goto :goto_0

    :cond_3
    move v2, v1

    .line 92
    goto :goto_1

    :cond_4
    move v2, v1

    .line 93
    goto :goto_2

    :cond_5
    move v2, v1

    .line 94
    goto :goto_3

    :cond_6
    move v2, v1

    .line 95
    goto :goto_4
.end method

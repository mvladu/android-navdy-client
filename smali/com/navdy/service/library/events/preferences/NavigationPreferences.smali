.class public final Lcom/navdy/service/library/events/preferences/NavigationPreferences;
.super Lcom/squareup/wire/Message;
.source "NavigationPreferences.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;,
        Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;,
        Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_ALLOWAUTOTRAINS:Ljava/lang/Boolean;

.field public static final DEFAULT_ALLOWFERRIES:Ljava/lang/Boolean;

.field public static final DEFAULT_ALLOWHIGHWAYS:Ljava/lang/Boolean;

.field public static final DEFAULT_ALLOWHOVLANES:Ljava/lang/Boolean;

.field public static final DEFAULT_ALLOWTOLLROADS:Ljava/lang/Boolean;

.field public static final DEFAULT_ALLOWTUNNELS:Ljava/lang/Boolean;

.field public static final DEFAULT_ALLOWUNPAVEDROADS:Ljava/lang/Boolean;

.field public static final DEFAULT_REROUTEFORTRAFFIC:Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;

.field public static final DEFAULT_ROUTINGTYPE:Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;

.field public static final DEFAULT_SERIAL_NUMBER:Ljava/lang/Long;

.field public static final DEFAULT_SHOWTRAFFICINOPENMAP:Ljava/lang/Boolean;

.field public static final DEFAULT_SHOWTRAFFICWHILENAVIGATING:Ljava/lang/Boolean;

.field public static final DEFAULT_SPOKENCAMERAWARNINGS:Ljava/lang/Boolean;

.field public static final DEFAULT_SPOKENSPEEDLIMITWARNINGS:Ljava/lang/Boolean;

.field public static final DEFAULT_SPOKENTURNBYTURN:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final allowAutoTrains:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0xd
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final allowFerries:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0xa
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final allowHOVLanes:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0xe
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final allowHighways:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x8
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final allowTollRoads:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x9
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final allowTunnels:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0xb
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final allowUnpavedRoads:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0xc
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final rerouteForTraffic:Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final routingType:Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x3
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final serial_number:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT64:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final showTrafficInOpenMap:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x6
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final showTrafficWhileNavigating:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x7
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final spokenCameraWarnings:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0xf
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final spokenSpeedLimitWarnings:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x5
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final spokenTurnByTurn:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x4
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 30
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->DEFAULT_SERIAL_NUMBER:Ljava/lang/Long;

    .line 31
    sget-object v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;->REROUTE_CONFIRM:Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;

    sput-object v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->DEFAULT_REROUTEFORTRAFFIC:Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;

    .line 32
    sget-object v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;->ROUTING_FASTEST:Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;

    sput-object v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->DEFAULT_ROUTINGTYPE:Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;

    .line 33
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->DEFAULT_SPOKENTURNBYTURN:Ljava/lang/Boolean;

    .line 34
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->DEFAULT_SPOKENSPEEDLIMITWARNINGS:Ljava/lang/Boolean;

    .line 35
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->DEFAULT_SHOWTRAFFICINOPENMAP:Ljava/lang/Boolean;

    .line 36
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->DEFAULT_SHOWTRAFFICWHILENAVIGATING:Ljava/lang/Boolean;

    .line 37
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->DEFAULT_ALLOWHIGHWAYS:Ljava/lang/Boolean;

    .line 38
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->DEFAULT_ALLOWTOLLROADS:Ljava/lang/Boolean;

    .line 39
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->DEFAULT_ALLOWFERRIES:Ljava/lang/Boolean;

    .line 40
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->DEFAULT_ALLOWTUNNELS:Ljava/lang/Boolean;

    .line 41
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->DEFAULT_ALLOWUNPAVEDROADS:Ljava/lang/Boolean;

    .line 42
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->DEFAULT_ALLOWAUTOTRAINS:Ljava/lang/Boolean;

    .line 43
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->DEFAULT_ALLOWHOVLANES:Ljava/lang/Boolean;

    .line 44
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->DEFAULT_SPOKENCAMERAWARNINGS:Ljava/lang/Boolean;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;)V
    .locals 17
    .param p1, "builder"    # Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;

    .prologue
    .line 114
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->serial_number:Ljava/lang/Long;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->rerouteForTraffic:Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->routingType:Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->spokenTurnByTurn:Ljava/lang/Boolean;

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->spokenSpeedLimitWarnings:Ljava/lang/Boolean;

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->showTrafficInOpenMap:Ljava/lang/Boolean;

    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->showTrafficWhileNavigating:Ljava/lang/Boolean;

    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->allowHighways:Ljava/lang/Boolean;

    move-object/from16 v0, p1

    iget-object v10, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->allowTollRoads:Ljava/lang/Boolean;

    move-object/from16 v0, p1

    iget-object v11, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->allowFerries:Ljava/lang/Boolean;

    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->allowTunnels:Ljava/lang/Boolean;

    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->allowUnpavedRoads:Ljava/lang/Boolean;

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->allowAutoTrains:Ljava/lang/Boolean;

    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->allowHOVLanes:Ljava/lang/Boolean;

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->spokenCameraWarnings:Ljava/lang/Boolean;

    move-object/from16 v16, v0

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v16}, Lcom/navdy/service/library/events/preferences/NavigationPreferences;-><init>(Ljava/lang/Long;Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    .line 115
    invoke-virtual/range {p0 .. p1}, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 116
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;Lcom/navdy/service/library/events/preferences/NavigationPreferences$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/preferences/NavigationPreferences$1;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/preferences/NavigationPreferences;-><init>(Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 0
    .param p1, "serial_number"    # Ljava/lang/Long;
    .param p2, "rerouteForTraffic"    # Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;
    .param p3, "routingType"    # Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;
    .param p4, "spokenTurnByTurn"    # Ljava/lang/Boolean;
    .param p5, "spokenSpeedLimitWarnings"    # Ljava/lang/Boolean;
    .param p6, "showTrafficInOpenMap"    # Ljava/lang/Boolean;
    .param p7, "showTrafficWhileNavigating"    # Ljava/lang/Boolean;
    .param p8, "allowHighways"    # Ljava/lang/Boolean;
    .param p9, "allowTollRoads"    # Ljava/lang/Boolean;
    .param p10, "allowFerries"    # Ljava/lang/Boolean;
    .param p11, "allowTunnels"    # Ljava/lang/Boolean;
    .param p12, "allowUnpavedRoads"    # Ljava/lang/Boolean;
    .param p13, "allowAutoTrains"    # Ljava/lang/Boolean;
    .param p14, "allowHOVLanes"    # Ljava/lang/Boolean;
    .param p15, "spokenCameraWarnings"    # Ljava/lang/Boolean;

    .prologue
    .line 95
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 96
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->serial_number:Ljava/lang/Long;

    .line 97
    iput-object p2, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->rerouteForTraffic:Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;

    .line 98
    iput-object p3, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->routingType:Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;

    .line 99
    iput-object p4, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->spokenTurnByTurn:Ljava/lang/Boolean;

    .line 100
    iput-object p5, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->spokenSpeedLimitWarnings:Ljava/lang/Boolean;

    .line 101
    iput-object p6, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->showTrafficInOpenMap:Ljava/lang/Boolean;

    .line 102
    iput-object p7, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->showTrafficWhileNavigating:Ljava/lang/Boolean;

    .line 103
    iput-object p8, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowHighways:Ljava/lang/Boolean;

    .line 104
    iput-object p9, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowTollRoads:Ljava/lang/Boolean;

    .line 105
    iput-object p10, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowFerries:Ljava/lang/Boolean;

    .line 106
    iput-object p11, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowTunnels:Ljava/lang/Boolean;

    .line 107
    iput-object p12, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowUnpavedRoads:Ljava/lang/Boolean;

    .line 108
    iput-object p13, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowAutoTrains:Ljava/lang/Boolean;

    .line 109
    iput-object p14, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowHOVLanes:Ljava/lang/Boolean;

    .line 110
    iput-object p15, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->spokenCameraWarnings:Ljava/lang/Boolean;

    .line 111
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 120
    if-ne p1, p0, :cond_1

    .line 137
    :cond_0
    :goto_0
    return v1

    .line 121
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/preferences/NavigationPreferences;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 122
    check-cast v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;

    .line 123
    .local v0, "o":Lcom/navdy/service/library/events/preferences/NavigationPreferences;
    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->serial_number:Ljava/lang/Long;

    iget-object v4, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->serial_number:Ljava/lang/Long;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->rerouteForTraffic:Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;

    iget-object v4, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->rerouteForTraffic:Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;

    .line 124
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->routingType:Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;

    iget-object v4, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->routingType:Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;

    .line 125
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->spokenTurnByTurn:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->spokenTurnByTurn:Ljava/lang/Boolean;

    .line 126
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->spokenSpeedLimitWarnings:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->spokenSpeedLimitWarnings:Ljava/lang/Boolean;

    .line 127
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->showTrafficInOpenMap:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->showTrafficInOpenMap:Ljava/lang/Boolean;

    .line 128
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->showTrafficWhileNavigating:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->showTrafficWhileNavigating:Ljava/lang/Boolean;

    .line 129
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowHighways:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowHighways:Ljava/lang/Boolean;

    .line 130
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowTollRoads:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowTollRoads:Ljava/lang/Boolean;

    .line 131
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowFerries:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowFerries:Ljava/lang/Boolean;

    .line 132
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowTunnels:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowTunnels:Ljava/lang/Boolean;

    .line 133
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowUnpavedRoads:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowUnpavedRoads:Ljava/lang/Boolean;

    .line 134
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowAutoTrains:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowAutoTrains:Ljava/lang/Boolean;

    .line 135
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowHOVLanes:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowHOVLanes:Ljava/lang/Boolean;

    .line 136
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->spokenCameraWarnings:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->spokenCameraWarnings:Ljava/lang/Boolean;

    .line 137
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto/16 :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 142
    iget v0, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->hashCode:I

    .line 143
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 144
    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->serial_number:Ljava/lang/Long;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->serial_number:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->hashCode()I

    move-result v0

    .line 145
    :goto_0
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->rerouteForTraffic:Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->rerouteForTraffic:Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 146
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->routingType:Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->routingType:Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;->hashCode()I

    move-result v2

    :goto_2
    add-int v0, v3, v2

    .line 147
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->spokenTurnByTurn:Ljava/lang/Boolean;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->spokenTurnByTurn:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :goto_3
    add-int v0, v3, v2

    .line 148
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->spokenSpeedLimitWarnings:Ljava/lang/Boolean;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->spokenSpeedLimitWarnings:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :goto_4
    add-int v0, v3, v2

    .line 149
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->showTrafficInOpenMap:Ljava/lang/Boolean;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->showTrafficInOpenMap:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :goto_5
    add-int v0, v3, v2

    .line 150
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->showTrafficWhileNavigating:Ljava/lang/Boolean;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->showTrafficWhileNavigating:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :goto_6
    add-int v0, v3, v2

    .line 151
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowHighways:Ljava/lang/Boolean;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowHighways:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :goto_7
    add-int v0, v3, v2

    .line 152
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowTollRoads:Ljava/lang/Boolean;

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowTollRoads:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :goto_8
    add-int v0, v3, v2

    .line 153
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowFerries:Ljava/lang/Boolean;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowFerries:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :goto_9
    add-int v0, v3, v2

    .line 154
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowTunnels:Ljava/lang/Boolean;

    if-eqz v2, :cond_c

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowTunnels:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :goto_a
    add-int v0, v3, v2

    .line 155
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowUnpavedRoads:Ljava/lang/Boolean;

    if-eqz v2, :cond_d

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowUnpavedRoads:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :goto_b
    add-int v0, v3, v2

    .line 156
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowAutoTrains:Ljava/lang/Boolean;

    if-eqz v2, :cond_e

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowAutoTrains:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :goto_c
    add-int v0, v3, v2

    .line 157
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowHOVLanes:Ljava/lang/Boolean;

    if-eqz v2, :cond_f

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->allowHOVLanes:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :goto_d
    add-int v0, v3, v2

    .line 158
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->spokenCameraWarnings:Ljava/lang/Boolean;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->spokenCameraWarnings:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 159
    iput v0, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->hashCode:I

    .line 161
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 144
    goto/16 :goto_0

    :cond_3
    move v2, v1

    .line 145
    goto/16 :goto_1

    :cond_4
    move v2, v1

    .line 146
    goto/16 :goto_2

    :cond_5
    move v2, v1

    .line 147
    goto/16 :goto_3

    :cond_6
    move v2, v1

    .line 148
    goto/16 :goto_4

    :cond_7
    move v2, v1

    .line 149
    goto/16 :goto_5

    :cond_8
    move v2, v1

    .line 150
    goto/16 :goto_6

    :cond_9
    move v2, v1

    .line 151
    goto :goto_7

    :cond_a
    move v2, v1

    .line 152
    goto :goto_8

    :cond_b
    move v2, v1

    .line 153
    goto :goto_9

    :cond_c
    move v2, v1

    .line 154
    goto :goto_a

    :cond_d
    move v2, v1

    .line 155
    goto :goto_b

    :cond_e
    move v2, v1

    .line 156
    goto :goto_c

    :cond_f
    move v2, v1

    .line 157
    goto :goto_d
.end method

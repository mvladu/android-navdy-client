.class public abstract Lcom/navdy/service/library/device/connection/ConnectionInfo;
.super Ljava/lang/Object;
.source "ConnectionInfo.java"


# static fields
.field private static final DEVICE_ID_GROUP:I = 0x1

.field public static final connectionInfoAdapter:Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory",
            "<",
            "Lcom/navdy/service/library/device/connection/ConnectionInfo;",
            ">;"
        }
    .end annotation
.end field

.field private static final deviceIdFromServiceName:Ljava/util/regex/Pattern;

.field public static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private final connectionType:Lcom/navdy/service/library/device/connection/ConnectionType;

.field protected final mDeviceId:Lcom/navdy/service/library/device/NavdyDeviceId;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "deviceId"
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 18
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/service/library/device/connection/ConnectionInfo;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/service/library/device/connection/ConnectionInfo;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 20
    const-string v0, "Navdy\\s(.*)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/device/connection/ConnectionInfo;->deviceIdFromServiceName:Ljava/util/regex/Pattern;

    .line 176
    const-class v0, Lcom/navdy/service/library/device/connection/ConnectionInfo;

    invoke-static {v0}, Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;->of(Ljava/lang/Class;)Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;

    move-result-object v0

    const-class v1, Lcom/navdy/service/library/device/connection/BTConnectionInfo;

    const-string v2, "BT"

    .line 177
    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;->registerSubtype(Ljava/lang/Class;Ljava/lang/String;)Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;

    move-result-object v0

    const-class v1, Lcom/navdy/service/library/device/connection/TCPConnectionInfo;

    const-string v2, "TCP"

    .line 178
    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;->registerSubtype(Ljava/lang/Class;Ljava/lang/String;)Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;

    move-result-object v0

    const-class v1, Lcom/navdy/service/library/device/connection/BTConnectionInfo;

    const-string v2, "EA"

    .line 180
    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;->registerSubtypeAlias(Ljava/lang/Class;Ljava/lang/String;)Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/device/connection/ConnectionInfo;->connectionInfoAdapter:Lcom/navdy/service/library/util/RuntimeTypeAdapterFactory;

    .line 176
    return-void
.end method

.method public constructor <init>(Landroid/net/nsd/NsdServiceInfo;)V
    .locals 3
    .param p1, "serviceInfo"    # Landroid/net/nsd/NsdServiceInfo;

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    if-nez p1, :cond_0

    .line 79
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "serviceInfo can\'t be null"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 82
    :cond_0
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/device/connection/ConnectionInfo;->parseDeviceIdStringFromServiceInfo(Landroid/net/nsd/NsdServiceInfo;)Ljava/lang/String;

    move-result-object v0

    .line 84
    .local v0, "deviceIdString":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 85
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Service info doesn\'t contain valid device id"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 88
    :cond_1
    sget-object v1, Lcom/navdy/service/library/device/connection/ConnectionType;->TCP_PROTOBUF:Lcom/navdy/service/library/device/connection/ConnectionType;

    iput-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionInfo;->connectionType:Lcom/navdy/service/library/device/connection/ConnectionType;

    .line 89
    new-instance v1, Lcom/navdy/service/library/device/NavdyDeviceId;

    invoke-direct {v1, v0}, Lcom/navdy/service/library/device/NavdyDeviceId;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionInfo;->mDeviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

    .line 90
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/device/NavdyDeviceId;Lcom/navdy/service/library/device/connection/ConnectionType;)V
    .locals 2
    .param p1, "id"    # Lcom/navdy/service/library/device/NavdyDeviceId;
    .param p2, "connectionType"    # Lcom/navdy/service/library/device/connection/ConnectionType;

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    if-nez p1, :cond_0

    .line 68
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Device id required."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 70
    :cond_0
    iput-object p1, p0, Lcom/navdy/service/library/device/connection/ConnectionInfo;->mDeviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

    .line 71
    iput-object p2, p0, Lcom/navdy/service/library/device/connection/ConnectionInfo;->connectionType:Lcom/navdy/service/library/device/connection/ConnectionType;

    .line 72
    return-void
.end method

.method protected constructor <init>(Lcom/navdy/service/library/device/connection/ConnectionType;)V
    .locals 1
    .param p1, "connectionType"    # Lcom/navdy/service/library/device/connection/ConnectionType;

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lcom/navdy/service/library/device/connection/ConnectionInfo;->connectionType:Lcom/navdy/service/library/device/connection/ConnectionType;

    .line 51
    sget-object v0, Lcom/navdy/service/library/device/NavdyDeviceId;->UNKNOWN_ID:Lcom/navdy/service/library/device/NavdyDeviceId;

    iput-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionInfo;->mDeviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

    .line 52
    return-void
.end method

.method public static fromConnection(Lcom/navdy/service/library/device/connection/Connection;)Lcom/navdy/service/library/device/connection/ConnectionInfo;
    .locals 1
    .param p0, "connection"    # Lcom/navdy/service/library/device/connection/Connection;

    .prologue
    .line 61
    if-nez p0, :cond_0

    .line 62
    const/4 v0, 0x0

    .line 64
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/navdy/service/library/device/connection/Connection;->getConnectionInfo()Lcom/navdy/service/library/device/connection/ConnectionInfo;

    move-result-object v0

    goto :goto_0
.end method

.method public static fromServiceInfo(Landroid/net/nsd/NsdServiceInfo;)Lcom/navdy/service/library/device/connection/ConnectionInfo;
    .locals 6
    .param p0, "serviceInfo"    # Landroid/net/nsd/NsdServiceInfo;

    .prologue
    const/4 v2, 0x0

    .line 97
    invoke-virtual {p0}, Landroid/net/nsd/NsdServiceInfo;->getServiceType()Ljava/lang/String;

    move-result-object v0

    .line 99
    .local v0, "serviceType":Ljava/lang/String;
    const-string v3, "."

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 100
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 103
    :cond_0
    const-string v3, "."

    invoke-virtual {v0, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 104
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 107
    :cond_1
    invoke-static {v0}, Lcom/navdy/service/library/device/connection/ConnectionType;->fromServiceType(Ljava/lang/String;)Lcom/navdy/service/library/device/connection/ConnectionType;

    move-result-object v1

    .line 109
    .local v1, "type":Lcom/navdy/service/library/device/connection/ConnectionType;
    if-nez v1, :cond_2

    .line 118
    :goto_0
    return-object v2

    .line 113
    :cond_2
    sget-object v3, Lcom/navdy/service/library/device/connection/ConnectionInfo$1;->$SwitchMap$com$navdy$service$library$device$connection$ConnectionType:[I

    invoke-virtual {v1}, Lcom/navdy/service/library/device/connection/ConnectionType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 117
    sget-object v3, Lcom/navdy/service/library/device/connection/ConnectionInfo;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "No connectioninfo from mDNS info for type: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 115
    :pswitch_0
    new-instance v2, Lcom/navdy/service/library/device/connection/TCPConnectionInfo;

    invoke-direct {v2, p0}, Lcom/navdy/service/library/device/connection/TCPConnectionInfo;-><init>(Landroid/net/nsd/NsdServiceInfo;)V

    goto :goto_0

    .line 113
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public static isValidNavdyServiceInfo(Landroid/net/nsd/NsdServiceInfo;)Z
    .locals 5
    .param p0, "serviceInfo"    # Landroid/net/nsd/NsdServiceInfo;

    .prologue
    const/4 v1, 0x0

    .line 33
    invoke-static {}, Lcom/navdy/service/library/device/connection/ConnectionType;->getServiceTypes()Ljava/util/Set;

    move-result-object v2

    invoke-virtual {p0}, Landroid/net/nsd/NsdServiceInfo;->getServiceType()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 34
    sget-object v2, Lcom/navdy/service/library/device/connection/ConnectionInfo;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unknown service type: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Landroid/net/nsd/NsdServiceInfo;->getServiceType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 46
    :goto_0
    return v1

    .line 38
    :cond_0
    sget-object v2, Lcom/navdy/service/library/device/connection/ConnectionInfo;->deviceIdFromServiceName:Ljava/util/regex/Pattern;

    invoke-virtual {p0}, Landroid/net/nsd/NsdServiceInfo;->getServiceName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 40
    .local v0, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 41
    const/4 v1, 0x1

    goto :goto_0

    .line 43
    :cond_1
    sget-object v2, Lcom/navdy/service/library/device/connection/ConnectionInfo;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Not a Navdy: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Landroid/net/nsd/NsdServiceInfo;->getServiceName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 162
    if-ne p0, p1, :cond_0

    const/4 v1, 0x1

    .line 167
    :goto_0
    return v1

    .line 163
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-eq v1, v2, :cond_2

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 165
    check-cast v0, Lcom/navdy/service/library/device/connection/ConnectionInfo;

    .line 167
    .local v0, "that":Lcom/navdy/service/library/device/connection/ConnectionInfo;
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionInfo;->mDeviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

    iget-object v2, v0, Lcom/navdy/service/library/device/connection/ConnectionInfo;->mDeviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/device/NavdyDeviceId;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public abstract getAddress()Lcom/navdy/service/library/device/connection/ServiceAddress;
.end method

.method public getDeviceId()Lcom/navdy/service/library/device/NavdyDeviceId;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionInfo;->mDeviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

    return-object v0
.end method

.method public getType()Lcom/navdy/service/library/device/connection/ConnectionType;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionInfo;->connectionType:Lcom/navdy/service/library/device/connection/ConnectionType;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionInfo;->mDeviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

    invoke-virtual {v0}, Lcom/navdy/service/library/device/NavdyDeviceId;->hashCode()I

    move-result v0

    return v0
.end method

.method protected parseDeviceIdStringFromServiceInfo(Landroid/net/nsd/NsdServiceInfo;)Ljava/lang/String;
    .locals 6
    .param p1, "serviceInfo"    # Landroid/net/nsd/NsdServiceInfo;

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x1

    .line 139
    invoke-virtual {p1}, Landroid/net/nsd/NsdServiceInfo;->getServiceName()Ljava/lang/String;

    move-result-object v1

    .line 145
    .local v1, "serviceName":Ljava/lang/String;
    const-string v3, "\\032"

    const-string v4, " "

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 147
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 157
    :cond_0
    :goto_0
    return-object v2

    .line 151
    :cond_1
    sget-object v3, Lcom/navdy/service/library/device/connection/ConnectionInfo;->deviceIdFromServiceName:Ljava/util/regex/Pattern;

    invoke-virtual {v3, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 153
    .local v0, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->groupCount()I

    move-result v3

    if-lt v3, v5, :cond_0

    .line 157
    invoke-virtual {v0, v5}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 135
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/navdy/service/library/device/connection/ConnectionInfo;->getType()Lcom/navdy/service/library/device/connection/ConnectionType;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " id: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ConnectionInfo;->mDeviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/navdy/service/library/device/connection/ConnectionInfo;->getAddress()Lcom/navdy/service/library/device/connection/ServiceAddress;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

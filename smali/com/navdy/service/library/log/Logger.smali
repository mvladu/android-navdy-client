.class public final Lcom/navdy/service/library/log/Logger;
.super Ljava/lang/Object;
.source "Logger.java"


# static fields
.field public static final ACTION_RELOAD:Ljava/lang/String; = "com.navdy.service.library.log.action.RELOAD"

.field public static final DEFAULT_TAG:Ljava/lang/String; = "Navdy"

.field private static volatile logLevelChange:J

.field private static sAppenders:[Lcom/navdy/service/library/log/LogAppender;


# instance fields
.field private volatile loggable:[Z

.field private startTime:J

.field private final tagName:Ljava/lang/String;

.field private volatile timestamp:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 13
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/navdy/service/library/log/LogAppender;

    sput-object v0, Lcom/navdy/service/library/log/Logger;->sAppenders:[Lcom/navdy/service/library/log/LogAppender;

    .line 15
    const-wide/16 v0, -0x1

    sput-wide v0, Lcom/navdy/service/library/log/Logger;->logLevelChange:J

    return-void
.end method

.method public constructor <init>(Ljava/lang/Class;)V
    .locals 4
    .param p1, "clazz"    # Ljava/lang/Class;

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    const-wide/16 v2, -0x2

    iput-wide v2, p0, Lcom/navdy/service/library/log/Logger;->timestamp:J

    .line 80
    if-eqz p1, :cond_0

    .line 81
    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 82
    .local v0, "name":Ljava/lang/String;
    const/4 v1, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v3, 0x17

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/service/library/log/Logger;->tagName:Ljava/lang/String;

    .line 86
    .end local v0    # "name":Ljava/lang/String;
    :goto_0
    return-void

    .line 84
    :cond_0
    const-string v1, "Navdy"

    iput-object v1, p0, Lcom/navdy/service/library/log/Logger;->tagName:Ljava/lang/String;

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 3
    .param p1, "tagName"    # Ljava/lang/String;

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    const-wide/16 v0, -0x2

    iput-wide v0, p0, Lcom/navdy/service/library/log/Logger;->timestamp:J

    .line 69
    if-eqz p1, :cond_0

    .line 70
    const/4 v0, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0x17

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/log/Logger;->tagName:Ljava/lang/String;

    .line 74
    :goto_0
    return-void

    .line 72
    :cond_0
    const-string v0, "Navdy"

    iput-object v0, p0, Lcom/navdy/service/library/log/Logger;->tagName:Ljava/lang/String;

    goto :goto_0
.end method

.method public static declared-synchronized addAppender(Lcom/navdy/service/library/log/LogAppender;)V
    .locals 4
    .param p0, "appender"    # Lcom/navdy/service/library/log/LogAppender;

    .prologue
    .line 33
    const-class v3, Lcom/navdy/service/library/log/Logger;

    monitor-enter v3

    if-nez p0, :cond_0

    .line 34
    :try_start_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-direct {v2}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 33
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2

    .line 36
    :cond_0
    :try_start_1
    sget-object v2, Lcom/navdy/service/library/log/Logger;->sAppenders:[Lcom/navdy/service/library/log/LogAppender;

    array-length v2, v2

    add-int/lit8 v2, v2, 0x1

    new-array v0, v2, [Lcom/navdy/service/library/log/LogAppender;

    .line 38
    .local v0, "appenders":[Lcom/navdy/service/library/log/LogAppender;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_2

    .line 39
    array-length v2, v0

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_1

    .line 40
    sget-object v2, Lcom/navdy/service/library/log/Logger;->sAppenders:[Lcom/navdy/service/library/log/LogAppender;

    aget-object v2, v2, v1

    aput-object v2, v0, v1

    .line 38
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 42
    :cond_1
    aput-object p0, v0, v1

    goto :goto_1

    .line 45
    :cond_2
    sput-object v0, Lcom/navdy/service/library/log/Logger;->sAppenders:[Lcom/navdy/service/library/log/LogAppender;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 46
    monitor-exit v3

    return-void
.end method

.method public static close()V
    .locals 2

    .prologue
    .line 55
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v1, Lcom/navdy/service/library/log/Logger;->sAppenders:[Lcom/navdy/service/library/log/LogAppender;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 56
    sget-object v1, Lcom/navdy/service/library/log/Logger;->sAppenders:[Lcom/navdy/service/library/log/LogAppender;

    aget-object v1, v1, v0

    invoke-interface {v1}, Lcom/navdy/service/library/log/LogAppender;->close()V

    .line 55
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 58
    :cond_0
    return-void
.end method

.method public static flush()V
    .locals 2

    .prologue
    .line 49
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v1, Lcom/navdy/service/library/log/Logger;->sAppenders:[Lcom/navdy/service/library/log/LogAppender;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 50
    sget-object v1, Lcom/navdy/service/library/log/Logger;->sAppenders:[Lcom/navdy/service/library/log/LogAppender;

    aget-object v1, v1, v0

    invoke-interface {v1}, Lcom/navdy/service/library/log/LogAppender;->flush()V

    .line 49
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 52
    :cond_0
    return-void
.end method

.method public static init([Lcom/navdy/service/library/log/LogAppender;)V
    .locals 1
    .param p0, "appenders"    # [Lcom/navdy/service/library/log/LogAppender;

    .prologue
    .line 26
    if-eqz p0, :cond_0

    array-length v0, p0

    if-nez v0, :cond_1

    .line 27
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 29
    :cond_1
    invoke-virtual {p0}, [Lcom/navdy/service/library/log/LogAppender;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/log/LogAppender;

    sput-object v0, Lcom/navdy/service/library/log/Logger;->sAppenders:[Lcom/navdy/service/library/log/LogAppender;

    .line 30
    return-void
.end method

.method public static reloadLogLevels()V
    .locals 2

    .prologue
    .line 20
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/service/library/log/Logger;->logLevelChange:J

    .line 21
    return-void
.end method


# virtual methods
.method public d(Ljava/lang/String;)V
    .locals 3
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 142
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v1, Lcom/navdy/service/library/log/Logger;->sAppenders:[Lcom/navdy/service/library/log/LogAppender;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 143
    sget-object v1, Lcom/navdy/service/library/log/Logger;->sAppenders:[Lcom/navdy/service/library/log/LogAppender;

    aget-object v1, v1, v0

    iget-object v2, p0, Lcom/navdy/service/library/log/Logger;->tagName:Ljava/lang/String;

    invoke-interface {v1, v2, p1}, Lcom/navdy/service/library/log/LogAppender;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 145
    :cond_0
    return-void
.end method

.method public d(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 3
    .param p1, "msg"    # Ljava/lang/String;
    .param p2, "tr"    # Ljava/lang/Throwable;

    .prologue
    .line 154
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v1, Lcom/navdy/service/library/log/Logger;->sAppenders:[Lcom/navdy/service/library/log/LogAppender;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 155
    sget-object v1, Lcom/navdy/service/library/log/Logger;->sAppenders:[Lcom/navdy/service/library/log/LogAppender;

    aget-object v1, v1, v0

    iget-object v2, p0, Lcom/navdy/service/library/log/Logger;->tagName:Ljava/lang/String;

    invoke-interface {v1, v2, p1, p2}, Lcom/navdy/service/library/log/LogAppender;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 154
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 157
    :cond_0
    return-void
.end method

.method public d(Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "tr"    # Ljava/lang/Throwable;

    .prologue
    .line 238
    const-string v0, ""

    invoke-virtual {p0, v0, p1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 239
    return-void
.end method

.method public e(Ljava/lang/String;)V
    .locals 3
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 211
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v1, Lcom/navdy/service/library/log/Logger;->sAppenders:[Lcom/navdy/service/library/log/LogAppender;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 212
    sget-object v1, Lcom/navdy/service/library/log/Logger;->sAppenders:[Lcom/navdy/service/library/log/LogAppender;

    aget-object v1, v1, v0

    iget-object v2, p0, Lcom/navdy/service/library/log/Logger;->tagName:Ljava/lang/String;

    invoke-interface {v1, v2, p1}, Lcom/navdy/service/library/log/LogAppender;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 214
    :cond_0
    return-void
.end method

.method public e(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 3
    .param p1, "msg"    # Ljava/lang/String;
    .param p2, "tr"    # Ljava/lang/Throwable;

    .prologue
    .line 223
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v1, Lcom/navdy/service/library/log/Logger;->sAppenders:[Lcom/navdy/service/library/log/LogAppender;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 224
    sget-object v1, Lcom/navdy/service/library/log/Logger;->sAppenders:[Lcom/navdy/service/library/log/LogAppender;

    aget-object v1, v1, v0

    iget-object v2, p0, Lcom/navdy/service/library/log/Logger;->tagName:Ljava/lang/String;

    invoke-interface {v1, v2, p1, p2}, Lcom/navdy/service/library/log/LogAppender;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 223
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 226
    :cond_0
    return-void
.end method

.method public e(Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "tr"    # Ljava/lang/Throwable;

    .prologue
    .line 230
    const-string v0, ""

    invoke-virtual {p0, v0, p1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 231
    return-void
.end method

.method public i(Ljava/lang/String;)V
    .locals 3
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 165
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v1, Lcom/navdy/service/library/log/Logger;->sAppenders:[Lcom/navdy/service/library/log/LogAppender;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 166
    sget-object v1, Lcom/navdy/service/library/log/Logger;->sAppenders:[Lcom/navdy/service/library/log/LogAppender;

    aget-object v1, v1, v0

    iget-object v2, p0, Lcom/navdy/service/library/log/Logger;->tagName:Ljava/lang/String;

    invoke-interface {v1, v2, p1}, Lcom/navdy/service/library/log/LogAppender;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 168
    :cond_0
    return-void
.end method

.method public i(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 3
    .param p1, "msg"    # Ljava/lang/String;
    .param p2, "tr"    # Ljava/lang/Throwable;

    .prologue
    .line 177
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v1, Lcom/navdy/service/library/log/Logger;->sAppenders:[Lcom/navdy/service/library/log/LogAppender;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 178
    sget-object v1, Lcom/navdy/service/library/log/Logger;->sAppenders:[Lcom/navdy/service/library/log/LogAppender;

    aget-object v1, v1, v0

    iget-object v2, p0, Lcom/navdy/service/library/log/Logger;->tagName:Ljava/lang/String;

    invoke-interface {v1, v2, p1, p2}, Lcom/navdy/service/library/log/LogAppender;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 177
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 180
    :cond_0
    return-void
.end method

.method public i(Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "tr"    # Ljava/lang/Throwable;

    .prologue
    .line 246
    const-string v0, ""

    invoke-virtual {p0, v0, p1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 247
    return-void
.end method

.method public isLoggable(I)Z
    .locals 6
    .param p1, "level"    # I

    .prologue
    .line 101
    iget-wide v2, p0, Lcom/navdy/service/library/log/Logger;->timestamp:J

    sget-wide v4, Lcom/navdy/service/library/log/Logger;->logLevelChange:J

    cmp-long v1, v2, v4

    if-gez v1, :cond_2

    .line 102
    iget-object v1, p0, Lcom/navdy/service/library/log/Logger;->loggable:[Z

    if-nez v1, :cond_0

    .line 103
    const/16 v1, 0x8

    new-array v1, v1, [Z

    iput-object v1, p0, Lcom/navdy/service/library/log/Logger;->loggable:[Z

    .line 105
    :cond_0
    const/4 v0, 0x2

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x7

    if-gt v0, v1, :cond_1

    .line 106
    iget-object v1, p0, Lcom/navdy/service/library/log/Logger;->loggable:[Z

    iget-object v2, p0, Lcom/navdy/service/library/log/Logger;->tagName:Ljava/lang/String;

    invoke-static {v2, v0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    aput-boolean v2, v1, v0

    .line 105
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 108
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/navdy/service/library/log/Logger;->timestamp:J

    .line 110
    .end local v0    # "i":I
    :cond_2
    iget-object v1, p0, Lcom/navdy/service/library/log/Logger;->loggable:[Z

    aget-boolean v1, v1, p1

    return v1
.end method

.method public logTimeTaken(Ljava/lang/String;)V
    .locals 6
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 259
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/navdy/service/library/log/Logger;->startTime:J

    sub-long v0, v2, v4

    .line 260
    .local v0, "timeTaken":J
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ms"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 261
    return-void
.end method

.method public recordStartTime()V
    .locals 2

    .prologue
    .line 250
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/navdy/service/library/log/Logger;->startTime:J

    .line 251
    return-void
.end method

.method public v(Ljava/lang/String;)V
    .locals 3
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 119
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v1, Lcom/navdy/service/library/log/Logger;->sAppenders:[Lcom/navdy/service/library/log/LogAppender;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 120
    sget-object v1, Lcom/navdy/service/library/log/Logger;->sAppenders:[Lcom/navdy/service/library/log/LogAppender;

    aget-object v1, v1, v0

    iget-object v2, p0, Lcom/navdy/service/library/log/Logger;->tagName:Ljava/lang/String;

    invoke-interface {v1, v2, p1}, Lcom/navdy/service/library/log/LogAppender;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 122
    :cond_0
    return-void
.end method

.method public v(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 3
    .param p1, "msg"    # Ljava/lang/String;
    .param p2, "tr"    # Ljava/lang/Throwable;

    .prologue
    .line 131
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v1, Lcom/navdy/service/library/log/Logger;->sAppenders:[Lcom/navdy/service/library/log/LogAppender;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 132
    sget-object v1, Lcom/navdy/service/library/log/Logger;->sAppenders:[Lcom/navdy/service/library/log/LogAppender;

    aget-object v1, v1, v0

    iget-object v2, p0, Lcom/navdy/service/library/log/Logger;->tagName:Ljava/lang/String;

    invoke-interface {v1, v2, p1, p2}, Lcom/navdy/service/library/log/LogAppender;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 131
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 134
    :cond_0
    return-void
.end method

.method public v(Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "tr"    # Ljava/lang/Throwable;

    .prologue
    .line 242
    const-string v0, ""

    invoke-virtual {p0, v0, p1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 243
    return-void
.end method

.method public w(Ljava/lang/String;)V
    .locals 3
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 188
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v1, Lcom/navdy/service/library/log/Logger;->sAppenders:[Lcom/navdy/service/library/log/LogAppender;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 189
    sget-object v1, Lcom/navdy/service/library/log/Logger;->sAppenders:[Lcom/navdy/service/library/log/LogAppender;

    aget-object v1, v1, v0

    iget-object v2, p0, Lcom/navdy/service/library/log/Logger;->tagName:Ljava/lang/String;

    invoke-interface {v1, v2, p1}, Lcom/navdy/service/library/log/LogAppender;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 191
    :cond_0
    return-void
.end method

.method public w(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 3
    .param p1, "msg"    # Ljava/lang/String;
    .param p2, "tr"    # Ljava/lang/Throwable;

    .prologue
    .line 200
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v1, Lcom/navdy/service/library/log/Logger;->sAppenders:[Lcom/navdy/service/library/log/LogAppender;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 201
    sget-object v1, Lcom/navdy/service/library/log/Logger;->sAppenders:[Lcom/navdy/service/library/log/LogAppender;

    aget-object v1, v1, v0

    iget-object v2, p0, Lcom/navdy/service/library/log/Logger;->tagName:Ljava/lang/String;

    invoke-interface {v1, v2, p1, p2}, Lcom/navdy/service/library/log/LogAppender;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 200
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 203
    :cond_0
    return-void
.end method

.method public w(Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "tr"    # Ljava/lang/Throwable;

    .prologue
    .line 234
    const-string v0, ""

    invoke-virtual {p0, v0, p1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 235
    return-void
.end method

.class public Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler;
.super Ljava/lang/Object;
.source "PlaceTypeSearchServiceHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler$PlaceTypeSearchListener;
    }
.end annotation


# static fields
.field private static final GOOGLE_PLACES_ID_HEADER:Ljava/lang/String; = "google-places-id:"

.field private static final logger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 37
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler;->logger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/navdy/client/app/framework/util/BusProvider;->register(Ljava/lang/Object;)V

    .line 44
    return-void
.end method

.method private EnsurePhoneNumbersAreNotLost(Lcom/navdy/service/library/events/destination/Destination;Lcom/navdy/service/library/events/destination/Destination;)Lcom/navdy/service/library/events/destination/Destination;
    .locals 2
    .param p1, "input"    # Lcom/navdy/service/library/events/destination/Destination;
    .param p2, "output"    # Lcom/navdy/service/library/events/destination/Destination;

    .prologue
    .line 128
    new-instance v0, Lcom/navdy/service/library/events/destination/Destination$Builder;

    invoke-direct {v0, p2}, Lcom/navdy/service/library/events/destination/Destination$Builder;-><init>(Lcom/navdy/service/library/events/destination/Destination;)V

    .line 129
    .local v0, "builder":Lcom/navdy/service/library/events/destination/Destination$Builder;
    iget-object v1, p2, Lcom/navdy/service/library/events/destination/Destination;->phoneNumbers:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p2, Lcom/navdy/service/library/events/destination/Destination;->phoneNumbers:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-gtz v1, :cond_1

    .line 130
    :cond_0
    iget-object v1, p1, Lcom/navdy/service/library/events/destination/Destination;->phoneNumbers:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/destination/Destination$Builder;->phoneNumbers(Ljava/util/List;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    .line 132
    :cond_1
    iget-object v1, p2, Lcom/navdy/service/library/events/destination/Destination;->contacts:Ljava/util/List;

    if-eqz v1, :cond_2

    iget-object v1, p2, Lcom/navdy/service/library/events/destination/Destination;->contacts:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-gtz v1, :cond_3

    .line 133
    :cond_2
    iget-object v1, p1, Lcom/navdy/service/library/events/destination/Destination;->contacts:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/destination/Destination$Builder;->contacts(Ljava/util/List;)Lcom/navdy/service/library/events/destination/Destination$Builder;

    .line 135
    :cond_3
    invoke-virtual {v0}, Lcom/navdy/service/library/events/destination/Destination$Builder;->build()Lcom/navdy/service/library/events/destination/Destination;

    move-result-object v1

    return-object v1
.end method

.method static synthetic access$000(Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler;Lcom/navdy/service/library/events/destination/Destination;Lcom/navdy/service/library/events/destination/Destination;)Lcom/navdy/service/library/events/destination/Destination;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler;
    .param p1, "x1"    # Lcom/navdy/service/library/events/destination/Destination;
    .param p2, "x2"    # Lcom/navdy/service/library/events/destination/Destination;

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler;->EnsurePhoneNumbersAreNotLost(Lcom/navdy/service/library/events/destination/Destination;Lcom/navdy/service/library/events/destination/Destination;)Lcom/navdy/service/library/events/destination/Destination;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/service/library/events/places/DestinationSelectedResponse;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/service/library/events/places/DestinationSelectedResponse;

    .prologue
    .line 35
    invoke-static {p0}, Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler;->printDestinationSelectedResponse(Lcom/navdy/service/library/events/places/DestinationSelectedResponse;)V

    return-void
.end method

.method static synthetic access$300(Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse;

    .prologue
    .line 35
    invoke-static {p0}, Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler;->printPlaceTypeSearchResponse(Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse;)V

    return-void
.end method

.method static synthetic access$400()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method private getGoogleServiceTypeForPlaceType(Lcom/navdy/service/library/events/places/PlaceType;)Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;
    .locals 2
    .param p1, "placeType"    # Lcom/navdy/service/library/events/places/PlaceType;

    .prologue
    .line 144
    sget-object v0, Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler$2;->$SwitchMap$com$navdy$service$library$events$places$PlaceType:[I

    invoke-virtual {p1}, Lcom/navdy/service/library/events/places/PlaceType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 160
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 146
    :pswitch_0
    sget-object v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;->GAS:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;

    goto :goto_0

    .line 148
    :pswitch_1
    sget-object v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;->PARKING:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;

    goto :goto_0

    .line 150
    :pswitch_2
    sget-object v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;->FOOD:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;

    goto :goto_0

    .line 152
    :pswitch_3
    sget-object v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;->COFFEE:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;

    goto :goto_0

    .line 154
    :pswitch_4
    sget-object v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;->ATM:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;

    goto :goto_0

    .line 156
    :pswitch_5
    sget-object v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;->HOSPITAL:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;

    goto :goto_0

    .line 158
    :pswitch_6
    sget-object v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;->STORE:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;

    goto :goto_0

    .line 144
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private performSearch(Ljava/lang/String;Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;)V
    .locals 2
    .param p1, "requestId"    # Ljava/lang/String;
    .param p2, "serviceType"    # Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;

    .prologue
    .line 139
    new-instance v0, Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler$PlaceTypeSearchListener;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, v1}, Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler$PlaceTypeSearchListener;-><init>(Ljava/lang/String;Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler$1;)V

    .line 140
    .local v0, "placeTypeSearchListener":Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler$PlaceTypeSearchListener;
    invoke-virtual {v0}, Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler$PlaceTypeSearchListener;->runSearch()V

    .line 141
    return-void
.end method

.method private static printDestinationSelectedResponse(Lcom/navdy/service/library/events/places/DestinationSelectedResponse;)V
    .locals 3
    .param p0, "destinationSelectedResponse"    # Lcom/navdy/service/library/events/places/DestinationSelectedResponse;

    .prologue
    .line 56
    if-nez p0, :cond_0

    .line 57
    sget-object v0, Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "printDestinationSelectedResponse, destinationSelectedResponse is null!"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 61
    :goto_0
    return-void

    .line 60
    :cond_0
    sget-object v0, Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "destinationSelectedResponse: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static printPlaceTypeSearchResponse(Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse;)V
    .locals 3
    .param p0, "placeTypeSearchResponse"    # Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse;

    .prologue
    .line 47
    if-nez p0, :cond_0

    .line 48
    sget-object v0, Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "printPlaceTypeSearchResponse, placeTypeSearchResponse is null!"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 53
    :goto_0
    return-void

    .line 52
    :cond_0
    sget-object v0, Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "placeTypeSearchResponse: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public onDestinationSelectedRequest(Lcom/navdy/service/library/events/places/DestinationSelectedRequest;)V
    .locals 4
    .param p1, "destinationSelectedRequest"    # Lcom/navdy/service/library/events/places/DestinationSelectedRequest;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 89
    if-eqz p1, :cond_0

    iget-object v1, p1, Lcom/navdy/service/library/events/places/DestinationSelectedRequest;->destination:Lcom/navdy/service/library/events/destination/Destination;

    if-nez v1, :cond_1

    .line 90
    :cond_0
    sget-object v1, Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "event or its destination is null on handleDestinationSelectedRequest"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 125
    :goto_0
    return-void

    .line 94
    :cond_1
    sget-object v1, Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "received new DestinationSelectedRequest event for destination "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 95
    new-instance v0, Lcom/navdy/client/app/framework/models/Destination;

    iget-object v1, p1, Lcom/navdy/service/library/events/places/DestinationSelectedRequest;->destination:Lcom/navdy/service/library/events/destination/Destination;

    invoke-direct {v0, v1}, Lcom/navdy/client/app/framework/models/Destination;-><init>(Lcom/navdy/service/library/events/destination/Destination;)V

    .line 97
    .local v0, "destination":Lcom/navdy/client/app/framework/models/Destination;
    new-instance v1, Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler$1;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler$1;-><init>(Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler;Lcom/navdy/service/library/events/places/DestinationSelectedRequest;)V

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->processDestination(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;)V

    goto :goto_0
.end method

.method public onPlaceTypeSearch(Lcom/navdy/service/library/events/places/PlaceTypeSearchRequest;)V
    .locals 5
    .param p1, "placeTypeSearchRequest"    # Lcom/navdy/service/library/events/places/PlaceTypeSearchRequest;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 65
    if-nez p1, :cond_0

    .line 66
    sget-object v2, Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "event is null on handlePlaceTypeSearch"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 85
    :goto_0
    return-void

    .line 70
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/places/PlaceTypeSearchRequest;->place_type:Lcom/navdy/service/library/events/places/PlaceType;

    .line 71
    .local v0, "placeType":Lcom/navdy/service/library/events/places/PlaceType;
    sget-object v2, Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "received new PlaceTypeSearchRequest for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 72
    invoke-direct {p0, v0}, Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler;->getGoogleServiceTypeForPlaceType(Lcom/navdy/service/library/events/places/PlaceType;)Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;

    move-result-object v1

    .line 74
    .local v1, "serviceType":Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;
    if-eqz v1, :cond_1

    .line 75
    iget-object v2, p1, Lcom/navdy/service/library/events/places/PlaceTypeSearchRequest;->request_id:Ljava/lang/String;

    invoke-direct {p0, v2, v1}, Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler;->performSearch(Ljava/lang/String;Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;)V

    goto :goto_0

    .line 77
    :cond_1
    sget-object v2, Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "invalid PlaceType, returning REQUEST_SERVICE_ERROR to HUD"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 78
    new-instance v2, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse$Builder;

    invoke-direct {v2}, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse$Builder;-><init>()V

    iget-object v3, p1, Lcom/navdy/service/library/events/places/PlaceTypeSearchRequest;->request_id:Ljava/lang/String;

    .line 79
    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse$Builder;->request_id(Ljava/lang/String;)Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse$Builder;

    move-result-object v2

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 80
    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse$Builder;->destinations(Ljava/util/List;)Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse$Builder;

    move-result-object v2

    sget-object v3, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SERVICE_ERROR:Lcom/navdy/service/library/events/RequestStatus;

    .line 81
    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse$Builder;->request_status(Lcom/navdy/service/library/events/RequestStatus;)Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse$Builder;

    move-result-object v2

    .line 82
    invoke-virtual {v2}, Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse$Builder;->build()Lcom/navdy/service/library/events/places/PlaceTypeSearchResponse;

    move-result-object v2

    .line 78
    invoke-static {v2}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z

    goto :goto_0
.end method

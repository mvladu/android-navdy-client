.class public Lcom/navdy/client/app/ui/settings/HFPAudioDelaySettingsActivity;
.super Lcom/navdy/client/app/ui/base/BaseEditActivity;
.source "HFPAudioDelaySettingsActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field btnPlaySequence:Landroid/widget/Button;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f100293
    .end annotation
.end field

.field multipleChoiceLayout:Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f100292
    .end annotation
.end field

.field private playingSequenceStatus:Ljava/lang/String;

.field sharedPreferences:Landroid/content/SharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field ttsAudioRouter:Lcom/navdy/client/app/framework/util/TTSAudioRouter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private userPreferredDelayLevel:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseEditActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/navdy/client/app/ui/settings/HFPAudioDelaySettingsActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/client/app/ui/settings/HFPAudioDelaySettingsActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/HFPAudioDelaySettingsActivity;

    .prologue
    .line 25
    iget v0, p0, Lcom/navdy/client/app/ui/settings/HFPAudioDelaySettingsActivity;->userPreferredDelayLevel:I

    return v0
.end method

.method static synthetic access$102(Lcom/navdy/client/app/ui/settings/HFPAudioDelaySettingsActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/HFPAudioDelaySettingsActivity;
    .param p1, "x1"    # I

    .prologue
    .line 25
    iput p1, p0, Lcom/navdy/client/app/ui/settings/HFPAudioDelaySettingsActivity;->userPreferredDelayLevel:I

    return p1
.end method

.method static synthetic access$202(Lcom/navdy/client/app/ui/settings/HFPAudioDelaySettingsActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/HFPAudioDelaySettingsActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 25
    iput-boolean p1, p0, Lcom/navdy/client/app/ui/settings/HFPAudioDelaySettingsActivity;->somethingChanged:Z

    return p1
.end method

.method public static start(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 72
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/navdy/client/app/ui/settings/HFPAudioDelaySettingsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 73
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 74
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/HFPAudioDelaySettingsActivity;->btnPlaySequence:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    .line 66
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/HFPAudioDelaySettingsActivity;->ttsAudioRouter:Lcom/navdy/client/app/framework/util/TTSAudioRouter;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->playSpeechDelaySequence()V

    .line 67
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/HFPAudioDelaySettingsActivity;->playingSequenceStatus:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;->startAudioStatusActivity(Landroid/content/Context;Ljava/lang/String;)V

    .line 69
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 43
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseEditActivity;->onCreate(Landroid/os/Bundle;)V

    .line 44
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/navdy/client/app/framework/Injector;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 45
    const v0, 0x7f08039f

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/settings/HFPAudioDelaySettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/HFPAudioDelaySettingsActivity;->playingSequenceStatus:Ljava/lang/String;

    .line 46
    const v0, 0x7f0300a1

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/settings/HFPAudioDelaySettingsActivity;->setContentView(I)V

    .line 47
    new-instance v0, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;-><init>(Lcom/navdy/client/app/ui/base/BaseToolbarActivity;)V

    const v1, 0x7f080482

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->title(I)Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->build()Landroid/support/v7/widget/Toolbar;

    .line 48
    invoke-static {p0, p0}, Lbutterknife/ButterKnife;->inject(Ljava/lang/Object;Landroid/app/Activity;)V

    .line 49
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/HFPAudioDelaySettingsActivity;->btnPlaySequence:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 50
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/HFPAudioDelaySettingsActivity;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "audio_hfp_delay_level"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/navdy/client/app/ui/settings/HFPAudioDelaySettingsActivity;->userPreferredDelayLevel:I

    .line 51
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/HFPAudioDelaySettingsActivity;->multipleChoiceLayout:Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout;

    iget v1, p0, Lcom/navdy/client/app/ui/settings/HFPAudioDelaySettingsActivity;->userPreferredDelayLevel:I

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout;->setSelectedIndex(I)V

    .line 52
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/HFPAudioDelaySettingsActivity;->multipleChoiceLayout:Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout;

    new-instance v1, Lcom/navdy/client/app/ui/settings/HFPAudioDelaySettingsActivity$1;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/ui/settings/HFPAudioDelaySettingsActivity$1;-><init>(Lcom/navdy/client/app/ui/settings/HFPAudioDelaySettingsActivity;)V

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout;->setChoiceListener(Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$ChoiceListener;)V

    .line 61
    return-void
.end method

.method protected saveChanges()V
    .locals 3

    .prologue
    .line 79
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/HFPAudioDelaySettingsActivity;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "audio_hfp_delay_level"

    iget v2, p0, Lcom/navdy/client/app/ui/settings/HFPAudioDelaySettingsActivity;->userPreferredDelayLevel:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 80
    return-void
.end method

.class public Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;
.super Lcom/navdy/client/app/ui/base/BaseSupportFragment;
.source "SuggestionsFragment.java"

# interfaces
.implements Lcom/navdy/client/ota/OTAUpdateUIClient;
.implements Landroid/content/ServiceConnection;
.implements Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteListener;
.implements Lcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;,
        Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;
    }
.end annotation


# static fields
.field private static final HERE_PADDING:I

.field private static final HERE_TOP_PADDING:I

.field private static final LAST_ARRIVAL_MARKER_STRING:Ljava/lang/String;

.field private static final MIN_DISTANCE_REBUILD_SUGGESTIONS:I = 0x1f4

.field private static final VERBOSE:Z = true

.field private static final logger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private final clickListener:Lcom/navdy/client/app/framework/util/CustomItemClickListener;

.field private destinationMarker:Lcom/here/android/mpa/mapping/MapMarker;

.field private googleMapFragment:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

.field private hereMapFragment:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

.field private lastArrivalMarker:Lcom/google/android/gms/maps/model/Marker;

.field private lastUpdate:Lcom/navdy/service/library/events/location/Coordinate;

.field private layoutManager:Landroid/support/v7/widget/LinearLayoutManager;

.field private final locationListener:Lcom/navdy/client/app/framework/location/NavdyLocationManager$OnNavdyLocationChangedListener;

.field private final lock:Ljava/lang/Object;

.field private final navdyLocationManager:Lcom/navdy/client/app/framework/location/NavdyLocationManager;

.field private final navdyRouteHandler:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

.field private offlineBanner:Landroid/widget/RelativeLayout;

.field private final onLongClickListener:Lcom/navdy/client/app/framework/util/CustomSuggestionsLongItemClickListener;

.field private otaUpdateService:Lcom/navdy/client/ota/OTAUpdateServiceInterface;

.field private routePolyline:Lcom/here/android/mpa/mapping/MapPolyline;

.field private routeProgressPolyline:Lcom/here/android/mpa/mapping/MapPolyline;

.field private scrollListener:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;

.field suggestionAdapter:Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

.field private suggestionListener:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;",
            ">;"
        }
    .end annotation
.end field

.field private suggestionRecycler:Landroid/support/v7/widget/RecyclerView;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 101
    new-instance v1, Lcom/navdy/service/library/log/Logger;

    const-class v2, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    invoke-direct {v1, v2}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v1, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->logger:Lcom/navdy/service/library/log/Logger;

    .line 111
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 113
    .local v0, "res":Landroid/content/res/Resources;
    const v1, 0x7f0b012c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    const v2, 0x7f0b012d

    .line 114
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    sget v2, Lcom/navdy/client/app/framework/map/MapUtils;->hereMapTopDownPadding:I

    add-int/2addr v1, v2

    sput v1, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->HERE_TOP_PADDING:I

    .line 116
    sget v1, Lcom/navdy/client/app/framework/map/MapUtils;->hereMapSidePadding:I

    sput v1, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->HERE_PADDING:I

    .line 118
    const v1, 0x7f080275

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->LAST_ARRIVAL_MARKER_STRING:Ljava/lang/String;

    .line 119
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 369
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseSupportFragment;-><init>()V

    .line 142
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->lock:Ljava/lang/Object;

    .line 144
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->suggestionListener:Ljava/lang/ref/WeakReference;

    .line 147
    new-instance v0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$1;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$1;-><init>(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;)V

    iput-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->locationListener:Lcom/navdy/client/app/framework/location/NavdyLocationManager$OnNavdyLocationChangedListener;

    .line 168
    new-instance v0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$2;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$2;-><init>(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;)V

    iput-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->onLongClickListener:Lcom/navdy/client/app/framework/util/CustomSuggestionsLongItemClickListener;

    .line 190
    new-instance v0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$3;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$3;-><init>(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;)V

    iput-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->clickListener:Lcom/navdy/client/app/framework/util/CustomItemClickListener;

    .line 370
    invoke-static {}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->getInstance()Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->navdyRouteHandler:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    .line 371
    invoke-static {}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->getInstance()Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->navdyLocationManager:Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    .line 372
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;)Lcom/navdy/service/library/events/location/Coordinate;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    .prologue
    .line 95
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->lastUpdate:Lcom/navdy/service/library/events/location/Coordinate;

    return-object v0
.end method

.method static synthetic access$002(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;Lcom/navdy/service/library/events/location/Coordinate;)Lcom/navdy/service/library/events/location/Coordinate;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;
    .param p1, "x1"    # Lcom/navdy/service/library/events/location/Coordinate;

    .prologue
    .line 95
    iput-object p1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->lastUpdate:Lcom/navdy/service/library/events/location/Coordinate;

    return-object p1
.end method

.method static synthetic access$100()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 95
    sget-object v0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;)Landroid/support/v7/widget/LinearLayoutManager;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    .prologue
    .line 95
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->layoutManager:Landroid/support/v7/widget/LinearLayoutManager;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;)Landroid/support/v7/widget/RecyclerView;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    .prologue
    .line 95
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->suggestionRecycler:Landroid/support/v7/widget/RecyclerView;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    .prologue
    .line 95
    invoke-direct {p0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->scrollToTheTop()V

    return-void
.end method

.method static synthetic access$200(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;)Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    .prologue
    .line 95
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->navdyRouteHandler:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;Lcom/here/android/mpa/mapping/Map;Lcom/here/android/mpa/common/GeoPolyline;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;
    .param p1, "x1"    # Lcom/here/android/mpa/mapping/Map;
    .param p2, "x2"    # Lcom/here/android/mpa/common/GeoPolyline;

    .prologue
    .line 95
    invoke-direct {p0, p1, p2}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->setProgressPolyline(Lcom/here/android/mpa/mapping/Map;Lcom/here/android/mpa/common/GeoPolyline;)V

    return-void
.end method

.method static synthetic access$400(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;)Lcom/google/android/gms/maps/model/Marker;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    .prologue
    .line 95
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->lastArrivalMarker:Lcom/google/android/gms/maps/model/Marker;

    return-object v0
.end method

.method static synthetic access$402(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;Lcom/google/android/gms/maps/model/Marker;)Lcom/google/android/gms/maps/model/Marker;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;
    .param p1, "x1"    # Lcom/google/android/gms/maps/model/Marker;

    .prologue
    .line 95
    iput-object p1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->lastArrivalMarker:Lcom/google/android/gms/maps/model/Marker;

    return-object p1
.end method

.method static synthetic access$500()Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    sget-object v0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->LAST_ARRIVAL_MARKER_STRING:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;Lcom/here/android/mpa/mapping/Map;Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;
    .param p1, "x1"    # Lcom/here/android/mpa/mapping/Map;
    .param p2, "x2"    # Lcom/navdy/client/app/framework/models/Destination;

    .prologue
    .line 95
    invoke-direct {p0, p1, p2}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->setDestinationMarker(Lcom/here/android/mpa/mapping/Map;Lcom/navdy/client/app/framework/models/Destination;)V

    return-void
.end method

.method static synthetic access$800(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;)Lcom/navdy/client/app/ui/base/BaseHereMapFragment;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    .prologue
    .line 95
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->hereMapFragment:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    return-object v0
.end method

.method static synthetic access$900(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;Lcom/here/android/mpa/mapping/Map;Lcom/here/android/mpa/common/GeoPolyline;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;
    .param p1, "x1"    # Lcom/here/android/mpa/mapping/Map;
    .param p2, "x2"    # Lcom/here/android/mpa/common/GeoPolyline;

    .prologue
    .line 95
    invoke-direct {p0, p1, p2}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->setRoutePolyline(Lcom/here/android/mpa/mapping/Map;Lcom/here/android/mpa/common/GeoPolyline;)V

    return-void
.end method

.method private addTripDestinationMarkerAndCenterMap(Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 2
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;

    .prologue
    .line 684
    invoke-direct {p0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->scrollToTheTop()V

    .line 685
    invoke-direct {p0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->showHereMap()V

    .line 687
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->hereMapFragment:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    if-nez v0, :cond_0

    .line 688
    sget-object v0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Calling addTripDestinationMarkerAndCenterMap with a null hereMapFragment"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 705
    :goto_0
    return-void

    .line 692
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->hereMapFragment:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    new-instance v1, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$8;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$8;-><init>(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;Lcom/navdy/client/app/framework/models/Destination;)V

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->whenInitialized(Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentInitialized;)V

    goto :goto_0
.end method

.method private addTripRouteAndCenterMap(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V
    .locals 2
    .param p1, "route"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    .prologue
    .line 708
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->hereMapFragment:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    if-nez v0, :cond_0

    .line 709
    sget-object v0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Calling addTripRouteAndCenterMap with a null hereMapFragment"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 729
    :goto_0
    return-void

    .line 716
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->hereMapFragment:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    new-instance v1, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$9;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$9;-><init>(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->whenReady(Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentReady;)V

    goto :goto_0
.end method

.method private clearLastArrival()V
    .locals 1

    .prologue
    .line 806
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->lastArrivalMarker:Lcom/google/android/gms/maps/model/Marker;

    if-eqz v0, :cond_0

    .line 807
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->lastArrivalMarker:Lcom/google/android/gms/maps/model/Marker;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/model/Marker;->remove()V

    .line 808
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->lastArrivalMarker:Lcom/google/android/gms/maps/model/Marker;

    .line 810
    :cond_0
    return-void
.end method

.method private clearMapObjects()V
    .locals 2

    .prologue
    .line 732
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->hereMapFragment:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    if-nez v0, :cond_0

    .line 733
    sget-object v0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Calling clearMapObjects with a null hereMapFragment"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 749
    :goto_0
    return-void

    .line 737
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->hereMapFragment:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    new-instance v1, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$10;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$10;-><init>(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;)V

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->whenInitialized(Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentInitialized;)V

    goto :goto_0
.end method

.method private connectToService()V
    .locals 4

    .prologue
    .line 860
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 861
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0}, Lcom/navdy/client/ota/OTAUpdateService;->getServiceIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v2

    .line 863
    .local v2, "serviceIntent":Landroid/content/Intent;
    const/4 v3, 0x1

    :try_start_0
    invoke-virtual {v0, v2, p0, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 867
    :goto_0
    return-void

    .line 864
    :catch_0
    move-exception v1

    .line 865
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private disconnectService()V
    .locals 2

    .prologue
    .line 870
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 872
    .local v0, "context":Landroid/content/Context;
    :try_start_0
    invoke-virtual {v0, p0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 876
    :goto_0
    return-void

    .line 873
    :catch_0
    move-exception v1

    .line 874
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private initSuggestionAdapter()V
    .locals 12

    .prologue
    .line 659
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 660
    .local v7, "resources":Landroid/content/res/Resources;
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 662
    .local v0, "activity":Landroid/app/Activity;
    invoke-static {v0}, Lcom/navdy/client/app/ui/UiUtils;->getDisplayHeight(Landroid/app/Activity;)I

    move-result v1

    .line 663
    .local v1, "displayHeight":I
    invoke-static {v0}, Lcom/navdy/client/app/ui/UiUtils;->getActionBarHeight(Landroid/app/Activity;)I

    move-result v9

    .line 664
    .local v9, "toolbarHeight":I
    invoke-static {v7}, Lcom/navdy/client/app/ui/UiUtils;->getTabsHeight(Landroid/content/res/Resources;)I

    move-result v8

    .line 665
    .local v8, "tabsHeight":I
    invoke-static {v7}, Lcom/navdy/client/app/ui/UiUtils;->getGoogleMapBottomPadding(Landroid/content/res/Resources;)I

    move-result v2

    .line 666
    .local v2, "googleBottomPadding":I
    sub-int v10, v1, v9

    sub-int/2addr v10, v8

    sub-int v3, v10, v2

    .line 667
    .local v3, "googleMapHeight":I
    invoke-static {v7}, Lcom/navdy/client/app/ui/UiUtils;->getHereMapBottomPadding(Landroid/content/res/Resources;)I

    move-result v4

    .line 668
    .local v4, "hereBottomPadding":I
    sub-int v10, v1, v9

    sub-int/2addr v10, v8

    sub-int v5, v10, v4

    .line 669
    .local v5, "hereMapHeight":I
    const v10, 0x7f0b00e5

    invoke-virtual {v7, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 671
    .local v6, "offlineBannerHeight":I
    new-instance v10, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->getContext()Landroid/content/Context;

    move-result-object v11

    invoke-direct {v10, v11, v3, v5, v6}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;-><init>(Landroid/content/Context;III)V

    iput-object v10, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->suggestionAdapter:Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

    .line 673
    iget-object v10, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->suggestionRecycler:Landroid/support/v7/widget/RecyclerView;

    if-eqz v10, :cond_0

    .line 674
    iget-object v10, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->suggestionRecycler:Landroid/support/v7/widget/RecyclerView;

    iget-object v11, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->suggestionAdapter:Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

    invoke-virtual {v10, v11}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 676
    :cond_0
    iget-object v10, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->suggestionAdapter:Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

    iget-object v11, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->clickListener:Lcom/navdy/client/app/framework/util/CustomItemClickListener;

    invoke-virtual {v10, v11}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->setClickListener(Lcom/navdy/client/app/framework/util/CustomItemClickListener;)V

    .line 677
    iget-object v10, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->suggestionAdapter:Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

    iget-object v11, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->onLongClickListener:Lcom/navdy/client/app/framework/util/CustomSuggestionsLongItemClickListener;

    invoke-virtual {v10, v11}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->setSuggestionLongClickListener(Lcom/navdy/client/app/framework/util/CustomSuggestionsLongItemClickListener;)V

    .line 678
    return-void
.end method

.method private scrollToTheTop()V
    .locals 2

    .prologue
    .line 975
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->layoutManager:Landroid/support/v7/widget/LinearLayoutManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/LinearLayoutManager;->scrollToPosition(I)V

    .line 976
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->scrollListener:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;->reset()V

    .line 977
    return-void
.end method

.method private setDestinationMarker(Lcom/here/android/mpa/mapping/Map;Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 3
    .param p1, "hereMap"    # Lcom/here/android/mpa/mapping/Map;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "destination"    # Lcom/navdy/client/app/framework/models/Destination;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 753
    if-eqz p2, :cond_2

    .line 754
    invoke-virtual {p2}, Lcom/navdy/client/app/framework/models/Destination;->getHereMapMarker()Lcom/here/android/mpa/mapping/MapMarker;

    move-result-object v0

    .line 758
    .local v0, "marker":Lcom/here/android/mpa/mapping/MapMarker;
    :goto_0
    iget-object v2, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->lock:Ljava/lang/Object;

    monitor-enter v2

    .line 759
    :try_start_0
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->destinationMarker:Lcom/here/android/mpa/mapping/MapMarker;

    if-eqz v1, :cond_0

    .line 760
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->destinationMarker:Lcom/here/android/mpa/mapping/MapMarker;

    invoke-virtual {p1, v1}, Lcom/here/android/mpa/mapping/Map;->removeMapObject(Lcom/here/android/mpa/mapping/MapObject;)Z

    .line 762
    :cond_0
    iput-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->destinationMarker:Lcom/here/android/mpa/mapping/MapMarker;

    .line 763
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->destinationMarker:Lcom/here/android/mpa/mapping/MapMarker;

    if-eqz v1, :cond_1

    .line 764
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->destinationMarker:Lcom/here/android/mpa/mapping/MapMarker;

    invoke-virtual {p1, v1}, Lcom/here/android/mpa/mapping/Map;->addMapObject(Lcom/here/android/mpa/mapping/MapObject;)Z

    .line 766
    :cond_1
    monitor-exit v2

    .line 767
    return-void

    .line 756
    .end local v0    # "marker":Lcom/here/android/mpa/mapping/MapMarker;
    :cond_2
    const/4 v0, 0x0

    .restart local v0    # "marker":Lcom/here/android/mpa/mapping/MapMarker;
    goto :goto_0

    .line 766
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private declared-synchronized setProgressPolyline(Lcom/here/android/mpa/mapping/Map;Lcom/here/android/mpa/common/GeoPolyline;)V
    .locals 3
    .param p1, "hereMap"    # Lcom/here/android/mpa/mapping/Map;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "progressPolyline"    # Lcom/here/android/mpa/common/GeoPolyline;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 789
    monitor-enter p0

    if-eqz p2, :cond_2

    .line 790
    :try_start_0
    invoke-static {p2}, Lcom/navdy/client/app/framework/map/MapUtils;->generateProgressPolyline(Lcom/here/android/mpa/common/GeoPolyline;)Lcom/here/android/mpa/mapping/MapPolyline;

    move-result-object v0

    .line 794
    .local v0, "polyline":Lcom/here/android/mpa/mapping/MapPolyline;
    :goto_0
    iget-object v2, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->lock:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 795
    :try_start_1
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->routeProgressPolyline:Lcom/here/android/mpa/mapping/MapPolyline;

    if-eqz v1, :cond_0

    .line 796
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->routeProgressPolyline:Lcom/here/android/mpa/mapping/MapPolyline;

    invoke-virtual {p1, v1}, Lcom/here/android/mpa/mapping/Map;->removeMapObject(Lcom/here/android/mpa/mapping/MapObject;)Z

    .line 798
    :cond_0
    iput-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->routeProgressPolyline:Lcom/here/android/mpa/mapping/MapPolyline;

    .line 799
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->routeProgressPolyline:Lcom/here/android/mpa/mapping/MapPolyline;

    if-eqz v1, :cond_1

    .line 800
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->routeProgressPolyline:Lcom/here/android/mpa/mapping/MapPolyline;

    invoke-virtual {p1, v1}, Lcom/here/android/mpa/mapping/Map;->addMapObject(Lcom/here/android/mpa/mapping/MapObject;)Z

    .line 802
    :cond_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 803
    monitor-exit p0

    return-void

    .line 792
    .end local v0    # "polyline":Lcom/here/android/mpa/mapping/MapPolyline;
    :cond_2
    const/4 v0, 0x0

    .restart local v0    # "polyline":Lcom/here/android/mpa/mapping/MapPolyline;
    goto :goto_0

    .line 802
    :catchall_0
    move-exception v1

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 789
    .end local v0    # "polyline":Lcom/here/android/mpa/mapping/MapPolyline;
    :catchall_1
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private setRecyclerView(Landroid/view/View;)V
    .locals 2
    .param p1, "rootView"    # Landroid/view/View;

    .prologue
    .line 648
    const v0, 0x7f1002ee

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->suggestionRecycler:Landroid/support/v7/widget/RecyclerView;

    .line 649
    new-instance v0, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->layoutManager:Landroid/support/v7/widget/LinearLayoutManager;

    .line 650
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->layoutManager:Landroid/support/v7/widget/LinearLayoutManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/LinearLayoutManager;->setOrientation(I)V

    .line 651
    new-instance v0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;-><init>(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$1;)V

    iput-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->scrollListener:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;

    .line 652
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->suggestionRecycler:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    .line 653
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->suggestionRecycler:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->layoutManager:Landroid/support/v7/widget/LinearLayoutManager;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 654
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->suggestionRecycler:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->scrollListener:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->addOnScrollListener(Landroid/support/v7/widget/RecyclerView$OnScrollListener;)V

    .line 656
    :cond_0
    return-void
.end method

.method private setRoutePolyline(Lcom/here/android/mpa/mapping/Map;Lcom/here/android/mpa/common/GeoPolyline;)V
    .locals 3
    .param p1, "hereMap"    # Lcom/here/android/mpa/mapping/Map;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "route"    # Lcom/here/android/mpa/common/GeoPolyline;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 771
    if-eqz p2, :cond_2

    .line 772
    invoke-static {p2}, Lcom/navdy/client/app/framework/map/MapUtils;->generateRoutePolyline(Lcom/here/android/mpa/common/GeoPolyline;)Lcom/here/android/mpa/mapping/MapPolyline;

    move-result-object v0

    .line 776
    .local v0, "polyline":Lcom/here/android/mpa/mapping/MapPolyline;
    :goto_0
    iget-object v2, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->lock:Ljava/lang/Object;

    monitor-enter v2

    .line 777
    :try_start_0
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->routePolyline:Lcom/here/android/mpa/mapping/MapPolyline;

    if-eqz v1, :cond_0

    .line 778
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->routePolyline:Lcom/here/android/mpa/mapping/MapPolyline;

    invoke-virtual {p1, v1}, Lcom/here/android/mpa/mapping/Map;->removeMapObject(Lcom/here/android/mpa/mapping/MapObject;)Z

    .line 780
    :cond_0
    iput-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->routePolyline:Lcom/here/android/mpa/mapping/MapPolyline;

    .line 781
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->routePolyline:Lcom/here/android/mpa/mapping/MapPolyline;

    if-eqz v1, :cond_1

    .line 782
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->routePolyline:Lcom/here/android/mpa/mapping/MapPolyline;

    invoke-virtual {p1, v1}, Lcom/here/android/mpa/mapping/Map;->addMapObject(Lcom/here/android/mpa/mapping/MapObject;)Z

    .line 784
    :cond_1
    monitor-exit v2

    .line 785
    return-void

    .line 774
    .end local v0    # "polyline":Lcom/here/android/mpa/mapping/MapPolyline;
    :cond_2
    const/4 v0, 0x0

    .restart local v0    # "polyline":Lcom/here/android/mpa/mapping/MapPolyline;
    goto :goto_0

    .line 784
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private showGoogleMap()V
    .locals 2

    .prologue
    .line 830
    sget-object v0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "showGoogleMap"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 832
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->isInForeground()Z

    move-result v0

    if-nez v0, :cond_0

    .line 833
    sget-object v0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "showGoogleMap, not in foreground, no-op"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 849
    :goto_0
    return-void

    .line 837
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->hereMapFragment:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    if-eqz v0, :cond_1

    .line 838
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->hereMapFragment:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->hide()V

    .line 840
    :cond_1
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->googleMapFragment:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    if-eqz v0, :cond_2

    .line 841
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->googleMapFragment:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    new-instance v1, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$11;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$11;-><init>(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;)V

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->show(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$OnShowMapListener;)V

    .line 848
    :cond_2
    invoke-direct {p0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->scrollToTheTop()V

    goto :goto_0
.end method

.method private showHereMap()V
    .locals 2

    .prologue
    .line 813
    sget-object v0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "showHereMap"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 815
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->isInForeground()Z

    move-result v0

    if-nez v0, :cond_0

    .line 816
    sget-object v0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "showHereMap, not in foreground, no-op"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 827
    :goto_0
    return-void

    .line 820
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->googleMapFragment:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    if-eqz v0, :cond_1

    .line 821
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->googleMapFragment:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->hide()V

    .line 823
    :cond_1
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->hereMapFragment:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    if-eqz v0, :cond_2

    .line 824
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->hereMapFragment:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->show()V

    .line 826
    :cond_2
    invoke-direct {p0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->scrollToTheTop()V

    goto :goto_0
.end method

.method private startMapRoute(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V
    .locals 1
    .param p1, "route"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 852
    invoke-direct {p0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->clearMapObjects()V

    .line 853
    invoke-virtual {p1}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->getDestination()Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->addTripDestinationMarkerAndCenterMap(Lcom/navdy/client/app/framework/models/Destination;)V

    .line 854
    invoke-direct {p0, p1}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->addTripRouteAndCenterMap(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V

    .line 855
    return-void
.end method

.method private updateOfflineBannerVisibility()V
    .locals 3

    .prologue
    .line 1199
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->offlineBanner:Landroid/widget/RelativeLayout;

    if-eqz v1, :cond_0

    .line 1200
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/AppInstance;->canReachInternet()Z

    move-result v0

    .line 1201
    .local v0, "canReachInternet":Z
    iget-object v2, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->offlineBanner:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_1

    const/16 v1, 0x8

    :goto_0
    invoke-virtual {v2, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1203
    .end local v0    # "canReachInternet":Z
    :cond_0
    return-void

    .line 1201
    .restart local v0    # "canReachInternet":Z
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public handleReachabilityStateChange(Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$ReachabilityEvent;)V
    .locals 3
    .param p1, "reachabilityEvent"    # Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$ReachabilityEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 1186
    if-eqz p1, :cond_1

    .line 1188
    sget-object v0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "reachability event received: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p1, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$ReachabilityEvent;->isReachable:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1190
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->suggestionAdapter:Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

    if-eqz v0, :cond_0

    .line 1191
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->suggestionAdapter:Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->notifyDataSetChanged()V

    .line 1194
    :cond_0
    invoke-direct {p0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->updateOfflineBannerVisibility()V

    .line 1196
    :cond_1
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 381
    const/4 v1, 0x0

    .line 382
    .local v1, "rootView":Landroid/view/View;
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 385
    .local v0, "activity":Landroid/support/v4/app/FragmentActivity;
    const v3, 0x7f0300ae

    const/4 v4, 0x0

    :try_start_0
    invoke-virtual {p1, v3, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 386
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, p0}, Lcom/navdy/client/app/framework/Injector;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 387
    const v3, 0x7f1000aa

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->offlineBanner:Landroid/widget/RelativeLayout;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 395
    :goto_0
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const v4, 0x7f1002ed

    .line 396
    invoke-virtual {v3, v4}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v3

    check-cast v3, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    iput-object v3, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->googleMapFragment:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    .line 397
    iget-object v3, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->googleMapFragment:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    if-eqz v3, :cond_0

    .line 398
    iget-object v3, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->googleMapFragment:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    invoke-virtual {v3}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->hide()V

    .line 403
    :cond_0
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const v4, 0x7f1002eb

    .line 404
    invoke-virtual {v3, v4}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v3

    check-cast v3, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    iput-object v3, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->hereMapFragment:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    .line 405
    iget-object v3, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->hereMapFragment:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    if-eqz v3, :cond_1

    .line 406
    iget-object v3, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->hereMapFragment:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    invoke-virtual {v3}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->hide()V

    .line 407
    iget-object v3, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->hereMapFragment:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    sget v4, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->HERE_TOP_PADDING:I

    sget v5, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->HERE_PADDING:I

    sget v6, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->HERE_PADDING:I

    sget v7, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->HERE_PADDING:I

    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->setUsableArea(IIII)V

    .line 410
    :cond_1
    if-eqz v1, :cond_2

    .line 412
    invoke-direct {p0, v1}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->setRecyclerView(Landroid/view/View;)V

    .line 415
    :cond_2
    iget-object v3, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->suggestionListener:Ljava/lang/ref/WeakReference;

    invoke-static {v3}, Lcom/navdy/client/app/framework/util/SuggestionManager;->addListener(Ljava/lang/ref/WeakReference;)V

    .line 417
    return-object v1

    .line 388
    :catch_0
    move-exception v2

    .line 389
    .local v2, "t":Ljava/lang/Throwable;
    sget-object v3, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Error while inflating the suggestions fragment."

    invoke-virtual {v3, v4, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 390
    invoke-static {v0}, Lcom/navdy/client/app/framework/util/GmsUtils;->finishIfGmsIsNotUpToDate(Landroid/app/Activity;)Z

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 491
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->suggestionListener:Ljava/lang/ref/WeakReference;

    invoke-static {v0}, Lcom/navdy/client/app/framework/util/SuggestionManager;->removeListener(Ljava/lang/ref/WeakReference;)V

    .line 492
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseSupportFragment;->onDestroy()V

    .line 493
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 1175
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->suggestionAdapter:Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

    if-eqz v0, :cond_0

    .line 1176
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->suggestionAdapter:Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->clearAllAnimations()V

    .line 1177
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->suggestionAdapter:Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->stopRefreshingAllEtas()V

    .line 1179
    :cond_0
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseSupportFragment;->onDetach()V

    .line 1180
    return-void
.end method

.method public onDownloadProgress(Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;JB)V
    .locals 0
    .param p1, "progress"    # Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;
    .param p2, "bytesDownloaded"    # J
    .param p4, "percentage"    # B

    .prologue
    .line 536
    return-void
.end method

.method public onErrorCheckingForUpdate(Lcom/navdy/client/ota/OTAUpdateUIClient$Error;)V
    .locals 0
    .param p1, "error"    # Lcom/navdy/client/ota/OTAUpdateUIClient$Error;

    .prologue
    .line 521
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 472
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->navdyLocationManager:Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->locationListener:Lcom/navdy/client/app/framework/location/NavdyLocationManager$OnNavdyLocationChangedListener;

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->removeListener(Lcom/navdy/client/app/framework/location/NavdyLocationManager$OnNavdyLocationChangedListener;)V

    .line 473
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->navdyRouteHandler:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    invoke-virtual {v0, p0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->removeListener(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteListener;)V

    .line 475
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->suggestionAdapter:Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

    if-eqz v0, :cond_0

    .line 476
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->suggestionAdapter:Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->endSelectionMode()V

    .line 477
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->suggestionAdapter:Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->stopRefreshingAllEtas()V

    .line 481
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->otaUpdateService:Lcom/navdy/client/ota/OTAUpdateServiceInterface;

    if-eqz v0, :cond_1

    .line 482
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->otaUpdateService:Lcom/navdy/client/ota/OTAUpdateServiceInterface;

    invoke-interface {v0}, Lcom/navdy/client/ota/OTAUpdateServiceInterface;->unregisterUIClient()V

    .line 484
    :cond_1
    invoke-direct {p0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->disconnectService()V

    .line 486
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseSupportFragment;->onPause()V

    .line 487
    return-void
.end method

.method public onPendingRouteCalculated(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V
    .locals 1
    .param p1, "error"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "pendingRoute"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 554
    invoke-direct {p0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->clearMapObjects()V

    .line 555
    if-eqz p2, :cond_0

    .line 556
    invoke-virtual {p2}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->getDestination()Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->addTripDestinationMarkerAndCenterMap(Lcom/navdy/client/app/framework/models/Destination;)V

    .line 558
    :cond_0
    sget-object v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;->NONE:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;

    if-ne p1, v0, :cond_1

    .line 559
    invoke-direct {p0, p2}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->addTripRouteAndCenterMap(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V

    .line 561
    :cond_1
    return-void
.end method

.method public onPendingRouteCalculating(Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 0
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 547
    invoke-direct {p0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->clearMapObjects()V

    .line 548
    invoke-direct {p0, p1}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->addTripDestinationMarkerAndCenterMap(Lcom/navdy/client/app/framework/models/Destination;)V

    .line 549
    return-void
.end method

.method public onReroute()V
    .locals 0

    .prologue
    .line 605
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 447
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseSupportFragment;->onResume()V

    .line 449
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->navdyLocationManager:Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->getSmartStartCoordinates()Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->lastUpdate:Lcom/navdy/service/library/events/location/Coordinate;

    .line 451
    invoke-direct {p0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->updateOfflineBannerVisibility()V

    .line 454
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->suggestionAdapter:Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

    if-nez v1, :cond_0

    .line 455
    invoke-direct {p0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->initSuggestionAdapter()V

    .line 465
    :goto_0
    invoke-direct {p0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->connectToService()V

    .line 466
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->navdyRouteHandler:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    invoke-virtual {v1, p0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->addListener(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteListener;)V

    .line 467
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->navdyLocationManager:Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    iget-object v2, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->locationListener:Lcom/navdy/client/app/framework/location/NavdyLocationManager$OnNavdyLocationChangedListener;

    invoke-virtual {v1, v2}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->addListener(Lcom/navdy/client/app/framework/location/NavdyLocationManager$OnNavdyLocationChangedListener;)V

    .line 468
    return-void

    .line 458
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->suggestionAdapter:Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

    invoke-virtual {v1}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->rebuildSuggestions()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 459
    :catch_0
    move-exception v0

    .line 460
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "rebuilding suggestions failed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->suggestionAdapter:Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onRouteArrived(Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 2
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 609
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->googleMapFragment:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    if-nez v0, :cond_0

    .line 610
    sget-object v0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Calling onRouteArrived with a null googleMapFragment"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 632
    :goto_0
    return-void

    .line 614
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->googleMapFragment:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    new-instance v1, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$7;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$7;-><init>(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;Lcom/navdy/client/app/framework/models/Destination;)V

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->getMapAsync(Lcom/google/android/gms/maps/OnMapReadyCallback;)V

    goto :goto_0
.end method

.method public onRouteCalculated(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V
    .locals 0
    .param p1, "error"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "route"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 574
    invoke-direct {p0, p2}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->startMapRoute(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V

    .line 575
    return-void
.end method

.method public onRouteCalculating(Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 1
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 565
    invoke-direct {p0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->clearLastArrival()V

    .line 566
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->hereMapFragment:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    if-eqz v0, :cond_0

    .line 567
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->hereMapFragment:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->centerOnUserLocation()V

    .line 569
    :cond_0
    return-void
.end method

.method public onRouteStarted(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V
    .locals 2
    .param p1, "route"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 579
    sget-object v0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onRouteStarted"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 581
    invoke-direct {p0, p1}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->startMapRoute(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V

    .line 582
    return-void
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 500
    if-eqz p2, :cond_0

    .line 501
    check-cast p2, Lcom/navdy/client/ota/OTAUpdateServiceInterface;

    .end local p2    # "service":Landroid/os/IBinder;
    iput-object p2, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->otaUpdateService:Lcom/navdy/client/ota/OTAUpdateServiceInterface;

    .line 502
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->otaUpdateService:Lcom/navdy/client/ota/OTAUpdateServiceInterface;

    invoke-interface {v1, p0}, Lcom/navdy/client/ota/OTAUpdateServiceInterface;->registerUIClient(Lcom/navdy/client/ota/OTAUpdateUIClient;)V

    .line 504
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->otaUpdateService:Lcom/navdy/client/ota/OTAUpdateServiceInterface;

    invoke-interface {v1}, Lcom/navdy/client/ota/OTAUpdateServiceInterface;->isCheckingForUpdate()Z

    move-result v1

    if-nez v1, :cond_0

    .line 505
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->otaUpdateService:Lcom/navdy/client/ota/OTAUpdateServiceInterface;

    invoke-interface {v1}, Lcom/navdy/client/ota/OTAUpdateServiceInterface;->getOTAUpdateState()Lcom/navdy/client/ota/OTAUpdateService$State;

    move-result-object v0

    .line 506
    .local v0, "state":Lcom/navdy/client/ota/OTAUpdateService$State;
    sget-object v1, Lcom/navdy/client/ota/OTAUpdateService$State;->UPLOADING:Lcom/navdy/client/ota/OTAUpdateService$State;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/navdy/client/ota/OTAUpdateService$State;->DOWNLOADING_UPDATE:Lcom/navdy/client/ota/OTAUpdateService$State;

    if-eq v0, v1, :cond_0

    .line 508
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->otaUpdateService:Lcom/navdy/client/ota/OTAUpdateServiceInterface;

    invoke-interface {v1}, Lcom/navdy/client/ota/OTAUpdateServiceInterface;->checkForUpdate()Z

    .line 512
    .end local v0    # "state":Lcom/navdy/client/ota/OTAUpdateService$State;
    :cond_0
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 1
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 516
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->otaUpdateService:Lcom/navdy/client/ota/OTAUpdateServiceInterface;

    .line 517
    return-void
.end method

.method public onStateChanged(Lcom/navdy/client/ota/OTAUpdateService$State;Lcom/navdy/client/ota/model/UpdateInfo;)V
    .locals 3
    .param p1, "state"    # Lcom/navdy/client/ota/OTAUpdateService$State;
    .param p2, "updateInfo"    # Lcom/navdy/client/ota/model/UpdateInfo;

    .prologue
    .line 525
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$5;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$5;-><init>(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;Lcom/navdy/client/ota/OTAUpdateService$State;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 531
    return-void
.end method

.method public onStopRoute()V
    .locals 2

    .prologue
    .line 636
    sget-object v0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onStopRoute"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 640
    invoke-direct {p0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->clearMapObjects()V

    .line 641
    invoke-direct {p0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->showGoogleMap()V

    .line 642
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->suggestionAdapter:Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->rebuildSuggestions()V

    .line 643
    return-void
.end method

.method public onSuggestionBuildComplete(Ljava/util/ArrayList;)V
    .locals 2
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/client/app/framework/models/Suggestion;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 423
    .local p1, "suggestions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Suggestion;>;"
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$4;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$4;-><init>(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;Ljava/util/ArrayList;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 443
    return-void
.end method

.method public onTripProgress(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V
    .locals 2
    .param p1, "progress"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 586
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->hereMapFragment:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    if-nez v0, :cond_0

    .line 587
    sget-object v0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Calling onTripProgress with a null hereMapFragment"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 602
    :goto_0
    return-void

    .line 592
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->hereMapFragment:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    new-instance v1, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$6;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$6;-><init>(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->whenInitialized(Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentInitialized;)V

    goto :goto_0
.end method

.method public onUploadProgress(Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;JB)V
    .locals 0
    .param p1, "progress"    # Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;
    .param p2, "completed"    # J
    .param p4, "percentage"    # B

    .prologue
    .line 541
    return-void
.end method

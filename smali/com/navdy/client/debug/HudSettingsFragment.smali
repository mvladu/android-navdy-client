.class public Lcom/navdy/client/debug/HudSettingsFragment;
.super Lcom/navdy/client/app/ui/base/BaseFragment;
.source "HudSettingsFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/debug/HudSettingsFragment$SettingSpinnerListener;
    }
.end annotation


# static fields
.field public static final AUTO_BRIGHTNESS:Ljava/lang/String; = "screen.auto_brightness"

.field public static final BRIGHTNESS:Ljava/lang/String; = "screen.brightness"

.field public static final GESTURE_ENGINE:Ljava/lang/String; = "gesture.engine"

.field public static final GESTURE_PREVIEW:Ljava/lang/String; = "gesture.preview"

.field public static final LED_BRIGHTNESS:Ljava/lang/String; = "screen.led_brightness"

.field public static final MAP_TILT:Ljava/lang/String; = "map.tilt"

.field public static final MAP_ZOOM:Ljava/lang/String; = "map.zoom"

.field private static final MAX_ZOOM_LEVEL:D = 20.0

.field private static final MIN_ZOOM_LEVEL:D = 2.0

.field private static final SCALE_FACTOR:D = 11.11111111111111

.field public static final START_VIDEO_ON_BOOT:Ljava/lang/String; = "start_video_on_boot_preference"

.field private static final ZOOM_MAX_PROGRESS:I = 0xc8

.field private static final ZOOM_RANGE:D = 18.0


# instance fields
.field autoBrightness:Landroid/widget/CheckBox;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f100255
    .end annotation
.end field

.field brightness:Landroid/widget/SeekBar;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f100257
    .end annotation
.end field

.field brightnessTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f100256
    .end annotation
.end field

.field enableGesture:Landroid/widget/CheckBox;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f10025b
    .end annotation
.end field

.field enablePreview:Landroid/widget/CheckBox;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f10025c
    .end annotation
.end field

.field enableSpecialVoiceSearch:Landroid/widget/CheckBox;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f100253
    .end annotation
.end field

.field enableVideoLoop:Landroid/widget/CheckBox;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f10025d
    .end annotation
.end field

.field private hudSettings:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/settings/Setting;",
            ">;"
        }
    .end annotation
.end field

.field ledBrightness:Landroid/widget/SeekBar;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f10025a
    .end annotation
.end field

.field ledBrightnessTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f100259
    .end annotation
.end field

.field mapTilt:Landroid/widget/SeekBar;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f100261
    .end annotation
.end field

.field mapZoomLevel:Landroid/widget/SeekBar;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f10025f
    .end annotation
.end field

.field private sharedPreferences:Landroid/content/SharedPreferences;

.field private sliderListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field tiltLevelTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f100260
    .end annotation
.end field

.field voiceSearchLabel:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f100252
    .end annotation
.end field

.field zoomLevelTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f10025e
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseFragment;-><init>()V

    .line 166
    new-instance v0, Lcom/navdy/client/debug/HudSettingsFragment$2;

    invoke-direct {v0, p0}, Lcom/navdy/client/debug/HudSettingsFragment$2;-><init>(Lcom/navdy/client/debug/HudSettingsFragment;)V

    iput-object v0, p0, Lcom/navdy/client/debug/HudSettingsFragment;->sliderListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/debug/HudSettingsFragment;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/debug/HudSettingsFragment;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Lcom/navdy/client/debug/HudSettingsFragment;->updateSetting(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private getSettings()V
    .locals 2

    .prologue
    .line 257
    iget-object v1, p0, Lcom/navdy/client/debug/HudSettingsFragment;->hudSettings:Ljava/util/List;

    if-nez v1, :cond_0

    .line 258
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 259
    .local v0, "settings":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v1, "screen.auto_brightness"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 260
    const-string v1, "screen.brightness"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 261
    const-string v1, "screen.led_brightness"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 262
    const-string v1, "gesture.engine"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 263
    const-string v1, "gesture.preview"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 264
    const-string v1, "map.tilt"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 265
    const-string v1, "map.zoom"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 266
    const-string v1, "start_video_on_boot_preference"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 267
    new-instance v1, Lcom/navdy/service/library/events/settings/ReadSettingsRequest;

    invoke-direct {v1, v0}, Lcom/navdy/service/library/events/settings/ReadSettingsRequest;-><init>(Ljava/util/List;)V

    invoke-direct {p0, v1}, Lcom/navdy/client/debug/HudSettingsFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    .line 269
    .end local v0    # "settings":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    return-void
.end method

.method private sendEvent(Lcom/squareup/wire/Message;)V
    .locals 0
    .param p1, "event"    # Lcom/squareup/wire/Message;

    .prologue
    .line 272
    invoke-static {p1}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 273
    return-void
.end method

.method private setupSpinner(Landroid/widget/Spinner;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 4
    .param p1, "spinner"    # Landroid/widget/Spinner;
    .param p2, "setting"    # Ljava/lang/String;
    .param p3, "value"    # Ljava/lang/String;
    .param p4, "optionsArrayId"    # I

    .prologue
    .line 328
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 329
    invoke-virtual {p0}, Lcom/navdy/client/debug/HudSettingsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, p4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 330
    .local v1, "optionsArray":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v2, Lcom/navdy/client/debug/HudSettingsFragment$SettingSpinnerListener;

    invoke-direct {v2, p0, p2, p3}, Lcom/navdy/client/debug/HudSettingsFragment$SettingSpinnerListener;-><init>(Lcom/navdy/client/debug/HudSettingsFragment;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1, v2}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 331
    new-instance v0, Landroid/widget/ArrayAdapter;

    .line 332
    invoke-virtual {p0}, Lcom/navdy/client/debug/HudSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x1090008

    invoke-direct {v0, v2, v3, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 335
    .local v0, "adapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    const v2, 0x1090009

    invoke-virtual {v0, v2}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 336
    invoke-virtual {p1, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 337
    invoke-interface {v1, p3}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    invoke-virtual {p1, v2}, Landroid/widget/Spinner;->setSelection(I)V

    .line 338
    return-void
.end method

.method private updateSeekBar(Landroid/widget/SeekBar;Ljava/lang/String;)V
    .locals 8
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 341
    const-wide/16 v4, 0x0

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v1 .. v7}, Lcom/navdy/client/debug/HudSettingsFragment;->updateSeekBar(Landroid/widget/SeekBar;Ljava/lang/String;DD)V

    .line 342
    return-void
.end method

.method private updateSeekBar(Landroid/widget/SeekBar;Ljava/lang/String;DD)V
    .locals 5
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "value"    # Ljava/lang/String;
    .param p3, "offset"    # D
    .param p5, "scaleFactor"    # D

    .prologue
    .line 345
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Landroid/widget/SeekBar;->setEnabled(Z)V

    .line 346
    invoke-static {p2}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    .line 347
    .local v0, "numericValue":D
    sub-double v2, v0, p3

    mul-double/2addr v2, p5

    double-to-int v2, v2

    invoke-virtual {p1, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 348
    return-void
.end method

.method private updateSetting(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 244
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 245
    .local v0, "settings":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/settings/Setting;>;"
    new-instance v1, Lcom/navdy/service/library/events/settings/Setting;

    invoke-direct {v1, p1, p2}, Lcom/navdy/service/library/events/settings/Setting;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 246
    new-instance v1, Lcom/navdy/service/library/events/settings/UpdateSettings;

    const/4 v2, 0x0

    invoke-direct {v1, v2, v0}, Lcom/navdy/service/library/events/settings/UpdateSettings;-><init>(Lcom/navdy/service/library/events/settings/ScreenConfiguration;Ljava/util/List;)V

    invoke-direct {p0, v1}, Lcom/navdy/client/debug/HudSettingsFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    .line 247
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "button"    # Landroid/view/View;
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f100262
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 233
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 234
    .local v0, "settings":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/settings/Setting;>;"
    new-instance v1, Lcom/navdy/service/library/events/settings/Setting;

    const-string v2, "map.zoom"

    const-string v3, "-1"

    invoke-direct {v1, v2, v3}, Lcom/navdy/service/library/events/settings/Setting;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 235
    new-instance v1, Lcom/navdy/service/library/events/settings/Setting;

    const-string v2, "map.tilt"

    const-string v3, "-1"

    invoke-direct {v1, v2, v3}, Lcom/navdy/service/library/events/settings/Setting;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 236
    new-instance v1, Lcom/navdy/service/library/events/settings/UpdateSettings;

    invoke-direct {v1, v4, v0}, Lcom/navdy/service/library/events/settings/UpdateSettings;-><init>(Lcom/navdy/service/library/events/settings/ScreenConfiguration;Ljava/util/List;)V

    invoke-direct {p0, v1}, Lcom/navdy/client/debug/HudSettingsFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    .line 239
    iput-object v4, p0, Lcom/navdy/client/debug/HudSettingsFragment;->hudSettings:Ljava/util/List;

    .line 240
    invoke-direct {p0}, Lcom/navdy/client/debug/HudSettingsFragment;->getSettings()V

    .line 241
    return-void
.end method

.method public onClick(Landroid/widget/CheckBox;)V
    .locals 4
    .param p1, "box"    # Landroid/widget/CheckBox;
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f100255,
            0x7f10025c,
            0x7f10025b,
            0x7f10025d
        }
    .end annotation

    .prologue
    .line 209
    invoke-virtual {p1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v0

    .line 210
    .local v0, "checkedValue":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/widget/CheckBox;->getId()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 224
    :pswitch_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "invalid checkbox"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 212
    :pswitch_1
    const-string v1, "screen.auto_brightness"

    .line 226
    .local v1, "name":Ljava/lang/String;
    :goto_0
    invoke-direct {p0, v1, v0}, Lcom/navdy/client/debug/HudSettingsFragment;->updateSetting(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    return-void

    .line 215
    .end local v1    # "name":Ljava/lang/String;
    :pswitch_2
    const-string v1, "gesture.preview"

    .line 216
    .restart local v1    # "name":Ljava/lang/String;
    goto :goto_0

    .line 218
    .end local v1    # "name":Ljava/lang/String;
    :pswitch_3
    const-string v1, "gesture.engine"

    .line 219
    .restart local v1    # "name":Ljava/lang/String;
    goto :goto_0

    .line 221
    .end local v1    # "name":Ljava/lang/String;
    :pswitch_4
    const-string v1, "start_video_on_boot_preference"

    .line 222
    .restart local v1    # "name":Ljava/lang/String;
    goto :goto_0

    .line 210
    :pswitch_data_0
    .packed-switch 0x7f100255
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 107
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    .line 108
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    .line 113
    const v1, 0x7f03008e

    invoke-virtual {p1, v1, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 114
    .local v0, "rootView":Landroid/view/View;
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->inject(Ljava/lang/Object;Landroid/view/View;)V

    .line 116
    iget-object v1, p0, Lcom/navdy/client/debug/HudSettingsFragment;->mapTilt:Landroid/widget/SeekBar;

    iget-object v2, p0, Lcom/navdy/client/debug/HudSettingsFragment;->sliderListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 117
    iget-object v1, p0, Lcom/navdy/client/debug/HudSettingsFragment;->mapZoomLevel:Landroid/widget/SeekBar;

    iget-object v2, p0, Lcom/navdy/client/debug/HudSettingsFragment;->sliderListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 118
    iget-object v1, p0, Lcom/navdy/client/debug/HudSettingsFragment;->brightness:Landroid/widget/SeekBar;

    iget-object v2, p0, Lcom/navdy/client/debug/HudSettingsFragment;->sliderListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 119
    iget-object v1, p0, Lcom/navdy/client/debug/HudSettingsFragment;->ledBrightness:Landroid/widget/SeekBar;

    iget-object v2, p0, Lcom/navdy/client/debug/HudSettingsFragment;->sliderListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 121
    iget-object v1, p0, Lcom/navdy/client/debug/HudSettingsFragment;->brightness:Landroid/widget/SeekBar;

    invoke-virtual {v1, v3}, Landroid/widget/SeekBar;->setEnabled(Z)V

    .line 122
    iget-object v1, p0, Lcom/navdy/client/debug/HudSettingsFragment;->ledBrightness:Landroid/widget/SeekBar;

    invoke-virtual {v1, v3}, Landroid/widget/SeekBar;->setEnabled(Z)V

    .line 124
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/client/debug/HudSettingsFragment;->sharedPreferences:Landroid/content/SharedPreferences;

    .line 141
    return-object v0
.end method

.method public onDeviceConnectedEvent(Lcom/navdy/client/app/framework/DeviceConnection$DeviceConnectedEvent;)V
    .locals 0
    .param p1, "event"    # Lcom/navdy/client/app/framework/DeviceConnection$DeviceConnectedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 277
    invoke-direct {p0}, Lcom/navdy/client/debug/HudSettingsFragment;->getSettings()V

    .line 278
    return-void
.end method

.method public onDeviceDisconnectedEvent(Lcom/navdy/client/app/framework/DeviceConnection$DeviceDisconnectedEvent;)V
    .locals 1
    .param p1, "event"    # Lcom/navdy/client/app/framework/DeviceConnection$DeviceDisconnectedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 282
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/client/debug/HudSettingsFragment;->hudSettings:Ljava/util/List;

    .line 283
    return-void
.end method

.method public onReadSettingsResponse(Lcom/navdy/service/library/events/settings/ReadSettingsResponse;)V
    .locals 10
    .param p1, "response"    # Lcom/navdy/service/library/events/settings/ReadSettingsResponse;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/4 v8, 0x1

    .line 287
    iget-object v1, p1, Lcom/navdy/service/library/events/settings/ReadSettingsResponse;->settings:Ljava/util/List;

    iput-object v1, p0, Lcom/navdy/client/debug/HudSettingsFragment;->hudSettings:Ljava/util/List;

    .line 289
    iget-object v1, p0, Lcom/navdy/client/debug/HudSettingsFragment;->hudSettings:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/settings/Setting;

    .line 290
    .local v0, "setting":Lcom/navdy/service/library/events/settings/Setting;
    iget-object v2, v0, Lcom/navdy/service/library/events/settings/Setting;->key:Ljava/lang/String;

    const/4 v1, -0x1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_0
    :goto_1
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 292
    :pswitch_0
    iget-object v1, p0, Lcom/navdy/client/debug/HudSettingsFragment;->brightness:Landroid/widget/SeekBar;

    iget-object v2, v0, Lcom/navdy/service/library/events/settings/Setting;->value:Ljava/lang/String;

    invoke-direct {p0, v1, v2}, Lcom/navdy/client/debug/HudSettingsFragment;->updateSeekBar(Landroid/widget/SeekBar;Ljava/lang/String;)V

    goto :goto_0

    .line 290
    :sswitch_0
    const-string v3, "screen.brightness"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x0

    goto :goto_1

    :sswitch_1
    const-string v3, "screen.auto_brightness"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v8

    goto :goto_1

    :sswitch_2
    const-string v3, "screen.led_brightness"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x2

    goto :goto_1

    :sswitch_3
    const-string v3, "gesture.preview"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x3

    goto :goto_1

    :sswitch_4
    const-string v3, "gesture.engine"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x4

    goto :goto_1

    :sswitch_5
    const-string v3, "map.zoom"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x5

    goto :goto_1

    :sswitch_6
    const-string v3, "map.tilt"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x6

    goto :goto_1

    :sswitch_7
    const-string v3, "start_video_on_boot_preference"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x7

    goto :goto_1

    .line 295
    :pswitch_1
    iget-object v1, p0, Lcom/navdy/client/debug/HudSettingsFragment;->autoBrightness:Landroid/widget/CheckBox;

    invoke-virtual {v1, v8}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 296
    iget-object v1, p0, Lcom/navdy/client/debug/HudSettingsFragment;->autoBrightness:Landroid/widget/CheckBox;

    iget-object v2, v0, Lcom/navdy/service/library/events/settings/Setting;->value:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto/16 :goto_0

    .line 299
    :pswitch_2
    iget-object v1, p0, Lcom/navdy/client/debug/HudSettingsFragment;->ledBrightness:Landroid/widget/SeekBar;

    iget-object v2, v0, Lcom/navdy/service/library/events/settings/Setting;->value:Ljava/lang/String;

    invoke-direct {p0, v1, v2}, Lcom/navdy/client/debug/HudSettingsFragment;->updateSeekBar(Landroid/widget/SeekBar;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 302
    :pswitch_3
    iget-object v1, p0, Lcom/navdy/client/debug/HudSettingsFragment;->enablePreview:Landroid/widget/CheckBox;

    invoke-virtual {v1, v8}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 303
    iget-object v1, p0, Lcom/navdy/client/debug/HudSettingsFragment;->enablePreview:Landroid/widget/CheckBox;

    iget-object v2, v0, Lcom/navdy/service/library/events/settings/Setting;->value:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto/16 :goto_0

    .line 306
    :pswitch_4
    iget-object v1, p0, Lcom/navdy/client/debug/HudSettingsFragment;->enableGesture:Landroid/widget/CheckBox;

    invoke-virtual {v1, v8}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 307
    iget-object v1, p0, Lcom/navdy/client/debug/HudSettingsFragment;->enableGesture:Landroid/widget/CheckBox;

    iget-object v2, v0, Lcom/navdy/service/library/events/settings/Setting;->value:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto/16 :goto_0

    .line 310
    :pswitch_5
    iget-object v2, p0, Lcom/navdy/client/debug/HudSettingsFragment;->mapZoomLevel:Landroid/widget/SeekBar;

    iget-object v3, v0, Lcom/navdy/service/library/events/settings/Setting;->value:Ljava/lang/String;

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    const-wide v6, 0x402638e38e38e38eL    # 11.11111111111111

    move-object v1, p0

    invoke-direct/range {v1 .. v7}, Lcom/navdy/client/debug/HudSettingsFragment;->updateSeekBar(Landroid/widget/SeekBar;Ljava/lang/String;DD)V

    goto/16 :goto_0

    .line 313
    :pswitch_6
    iget-object v1, p0, Lcom/navdy/client/debug/HudSettingsFragment;->mapTilt:Landroid/widget/SeekBar;

    iget-object v2, v0, Lcom/navdy/service/library/events/settings/Setting;->value:Ljava/lang/String;

    invoke-direct {p0, v1, v2}, Lcom/navdy/client/debug/HudSettingsFragment;->updateSeekBar(Landroid/widget/SeekBar;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 316
    :pswitch_7
    iget-object v1, p0, Lcom/navdy/client/debug/HudSettingsFragment;->enableVideoLoop:Landroid/widget/CheckBox;

    invoke-virtual {v1, v8}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 317
    iget-object v1, p0, Lcom/navdy/client/debug/HudSettingsFragment;->enableVideoLoop:Landroid/widget/CheckBox;

    iget-object v2, v0, Lcom/navdy/service/library/events/settings/Setting;->value:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto/16 :goto_0

    .line 323
    .end local v0    # "setting":Lcom/navdy/service/library/events/settings/Setting;
    :cond_1
    return-void

    .line 290
    nop

    :sswitch_data_0
    .sparse-switch
        -0x3dcc9179 -> :sswitch_4
        -0x2f2407dd -> :sswitch_3
        -0x1851b0ed -> :sswitch_0
        0x7f9e00f -> :sswitch_6
        0x7fcb125 -> :sswitch_5
        0x1100acc7 -> :sswitch_2
        0x4c368569 -> :sswitch_7
        0x6dabd75f -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 251
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseFragment;->onResume()V

    .line 253
    invoke-direct {p0}, Lcom/navdy/client/debug/HudSettingsFragment;->getSettings()V

    .line 254
    return-void
.end method

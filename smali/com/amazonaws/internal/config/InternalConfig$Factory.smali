.class public Lcom/amazonaws/internal/config/InternalConfig$Factory;
.super Ljava/lang/Object;
.source "InternalConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/amazonaws/internal/config/InternalConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# static fields
.field private static final SINGELTON:Lcom/amazonaws/internal/config/InternalConfig;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 185
    const/4 v0, 0x0

    .line 187
    .local v0, "config":Lcom/amazonaws/internal/config/InternalConfig;
    :try_start_0
    new-instance v0, Lcom/amazonaws/internal/config/InternalConfig;

    .end local v0    # "config":Lcom/amazonaws/internal/config/InternalConfig;
    invoke-direct {v0}, Lcom/amazonaws/internal/config/InternalConfig;-><init>()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 195
    .restart local v0    # "config":Lcom/amazonaws/internal/config/InternalConfig;
    sput-object v0, Lcom/amazonaws/internal/config/InternalConfig$Factory;->SINGELTON:Lcom/amazonaws/internal/config/InternalConfig;

    .line 196
    return-void

    .line 188
    .end local v0    # "config":Lcom/amazonaws/internal/config/InternalConfig;
    :catch_0
    move-exception v1

    .line 189
    .local v1, "ex":Ljava/lang/RuntimeException;
    throw v1

    .line 190
    .end local v1    # "ex":Ljava/lang/RuntimeException;
    :catch_1
    move-exception v1

    .line 191
    .local v1, "ex":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Fatal: Failed to load the internal config for AWS Android SDK"

    invoke-direct {v2, v3, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 182
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInternalConfig()Lcom/amazonaws/internal/config/InternalConfig;
    .locals 1

    .prologue
    .line 203
    sget-object v0, Lcom/amazonaws/internal/config/InternalConfig$Factory;->SINGELTON:Lcom/amazonaws/internal/config/InternalConfig;

    return-object v0
.end method

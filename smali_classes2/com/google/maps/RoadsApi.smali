.class public Lcom/google/maps/RoadsApi;
.super Ljava/lang/Object;
.source "RoadsApi.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/maps/RoadsApi$CombinedResponse;,
        Lcom/google/maps/RoadsApi$SpeedsResponse;,
        Lcom/google/maps/RoadsApi$RoadsResponse;
    }
.end annotation


# static fields
.field static final API_BASE_URL:Ljava/lang/String; = "https://roads.googleapis.com"

.field static final ROADS_API_CONFIG:Lcom/google/maps/internal/ApiConfig;

.field static final SPEEDS_API_CONFIG:Lcom/google/maps/internal/ApiConfig;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 40
    new-instance v0, Lcom/google/maps/internal/ApiConfig;

    const-string v1, "/v1/snapToRoads"

    invoke-direct {v0, v1}, Lcom/google/maps/internal/ApiConfig;-><init>(Ljava/lang/String;)V

    const-string v1, "https://roads.googleapis.com"

    invoke-virtual {v0, v1}, Lcom/google/maps/internal/ApiConfig;->hostName(Ljava/lang/String;)Lcom/google/maps/internal/ApiConfig;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/maps/internal/ApiConfig;->supportsClientId(Z)Lcom/google/maps/internal/ApiConfig;

    move-result-object v0

    sget-object v1, Lcom/google/gson/FieldNamingPolicy;->IDENTITY:Lcom/google/gson/FieldNamingPolicy;

    invoke-virtual {v0, v1}, Lcom/google/maps/internal/ApiConfig;->fieldNamingPolicy(Lcom/google/gson/FieldNamingPolicy;)Lcom/google/maps/internal/ApiConfig;

    move-result-object v0

    sput-object v0, Lcom/google/maps/RoadsApi;->ROADS_API_CONFIG:Lcom/google/maps/internal/ApiConfig;

    .line 45
    new-instance v0, Lcom/google/maps/internal/ApiConfig;

    const-string v1, "/v1/speedLimits"

    invoke-direct {v0, v1}, Lcom/google/maps/internal/ApiConfig;-><init>(Ljava/lang/String;)V

    const-string v1, "https://roads.googleapis.com"

    invoke-virtual {v0, v1}, Lcom/google/maps/internal/ApiConfig;->hostName(Ljava/lang/String;)Lcom/google/maps/internal/ApiConfig;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/maps/internal/ApiConfig;->supportsClientId(Z)Lcom/google/maps/internal/ApiConfig;

    move-result-object v0

    sget-object v1, Lcom/google/gson/FieldNamingPolicy;->IDENTITY:Lcom/google/gson/FieldNamingPolicy;

    invoke-virtual {v0, v1}, Lcom/google/maps/internal/ApiConfig;->fieldNamingPolicy(Lcom/google/gson/FieldNamingPolicy;)Lcom/google/maps/internal/ApiConfig;

    move-result-object v0

    sput-object v0, Lcom/google/maps/RoadsApi;->SPEEDS_API_CONFIG:Lcom/google/maps/internal/ApiConfig;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static varargs snapToRoads(Lcom/google/maps/GeoApiContext;Z[Lcom/google/maps/model/LatLng;)Lcom/google/maps/PendingResult;
    .locals 5
    .param p0, "context"    # Lcom/google/maps/GeoApiContext;
    .param p1, "interpolate"    # Z
    .param p2, "path"    # [Lcom/google/maps/model/LatLng;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/maps/GeoApiContext;",
            "Z[",
            "Lcom/google/maps/model/LatLng;",
            ")",
            "Lcom/google/maps/PendingResult",
            "<[",
            "Lcom/google/maps/model/SnappedPoint;",
            ">;"
        }
    .end annotation

    .prologue
    .line 75
    sget-object v0, Lcom/google/maps/RoadsApi;->ROADS_API_CONFIG:Lcom/google/maps/internal/ApiConfig;

    const-class v1, Lcom/google/maps/RoadsApi$RoadsResponse;

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "path"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const/16 v4, 0x7c

    invoke-static {v4, p2}, Lcom/google/maps/internal/StringJoin;->join(C[Lcom/google/maps/internal/StringJoin$UrlValue;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "interpolate"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-static {p1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/maps/GeoApiContext;->get(Lcom/google/maps/internal/ApiConfig;Ljava/lang/Class;[Ljava/lang/String;)Lcom/google/maps/PendingResult;

    move-result-object v0

    return-object v0
.end method

.method public static varargs snapToRoads(Lcom/google/maps/GeoApiContext;[Lcom/google/maps/model/LatLng;)Lcom/google/maps/PendingResult;
    .locals 5
    .param p0, "context"    # Lcom/google/maps/GeoApiContext;
    .param p1, "path"    # [Lcom/google/maps/model/LatLng;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/maps/GeoApiContext;",
            "[",
            "Lcom/google/maps/model/LatLng;",
            ")",
            "Lcom/google/maps/PendingResult",
            "<[",
            "Lcom/google/maps/model/SnappedPoint;",
            ">;"
        }
    .end annotation

    .prologue
    .line 58
    sget-object v0, Lcom/google/maps/RoadsApi;->ROADS_API_CONFIG:Lcom/google/maps/internal/ApiConfig;

    const-class v1, Lcom/google/maps/RoadsApi$RoadsResponse;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "path"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const/16 v4, 0x7c

    invoke-static {v4, p1}, Lcom/google/maps/internal/StringJoin;->join(C[Lcom/google/maps/internal/StringJoin$UrlValue;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/maps/GeoApiContext;->get(Lcom/google/maps/internal/ApiConfig;Ljava/lang/Class;[Ljava/lang/String;)Lcom/google/maps/PendingResult;

    move-result-object v0

    return-object v0
.end method

.method public static varargs snappedSpeedLimits(Lcom/google/maps/GeoApiContext;[Lcom/google/maps/model/LatLng;)Lcom/google/maps/PendingResult;
    .locals 5
    .param p0, "context"    # Lcom/google/maps/GeoApiContext;
    .param p1, "path"    # [Lcom/google/maps/model/LatLng;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/maps/GeoApiContext;",
            "[",
            "Lcom/google/maps/model/LatLng;",
            ")",
            "Lcom/google/maps/PendingResult",
            "<",
            "Lcom/google/maps/model/SnappedSpeedLimitResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 123
    sget-object v0, Lcom/google/maps/RoadsApi;->SPEEDS_API_CONFIG:Lcom/google/maps/internal/ApiConfig;

    const-class v1, Lcom/google/maps/RoadsApi$CombinedResponse;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "path"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const/16 v4, 0x7c

    invoke-static {v4, p1}, Lcom/google/maps/internal/StringJoin;->join(C[Lcom/google/maps/internal/StringJoin$UrlValue;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/maps/GeoApiContext;->get(Lcom/google/maps/internal/ApiConfig;Ljava/lang/Class;[Ljava/lang/String;)Lcom/google/maps/PendingResult;

    move-result-object v0

    return-object v0
.end method

.method public static varargs speedLimits(Lcom/google/maps/GeoApiContext;[Lcom/google/maps/model/LatLng;)Lcom/google/maps/PendingResult;
    .locals 5
    .param p0, "context"    # Lcom/google/maps/GeoApiContext;
    .param p1, "path"    # [Lcom/google/maps/model/LatLng;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/maps/GeoApiContext;",
            "[",
            "Lcom/google/maps/model/LatLng;",
            ")",
            "Lcom/google/maps/PendingResult",
            "<[",
            "Lcom/google/maps/model/SpeedLimit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 90
    sget-object v0, Lcom/google/maps/RoadsApi;->SPEEDS_API_CONFIG:Lcom/google/maps/internal/ApiConfig;

    const-class v1, Lcom/google/maps/RoadsApi$SpeedsResponse;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "path"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const/16 v4, 0x7c

    invoke-static {v4, p1}, Lcom/google/maps/internal/StringJoin;->join(C[Lcom/google/maps/internal/StringJoin$UrlValue;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/maps/GeoApiContext;->get(Lcom/google/maps/internal/ApiConfig;Ljava/lang/Class;[Ljava/lang/String;)Lcom/google/maps/PendingResult;

    move-result-object v0

    return-object v0
.end method

.method public static varargs speedLimits(Lcom/google/maps/GeoApiContext;[Ljava/lang/String;)Lcom/google/maps/PendingResult;
    .locals 9
    .param p0, "context"    # Lcom/google/maps/GeoApiContext;
    .param p1, "placeIds"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/maps/GeoApiContext;",
            "[",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/maps/PendingResult",
            "<[",
            "Lcom/google/maps/model/SpeedLimit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 107
    array-length v7, p1

    mul-int/lit8 v7, v7, 0x2

    new-array v6, v7, [Ljava/lang/String;

    .line 108
    .local v6, "placeParams":[Ljava/lang/String;
    const/4 v1, 0x0

    .line 109
    .local v1, "i":I
    move-object v0, p1

    .local v0, "arr$":[Ljava/lang/String;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    move v2, v1

    .end local v1    # "i":I
    .local v2, "i":I
    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v5, v0, v3

    .line 110
    .local v5, "placeId":Ljava/lang/String;
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "i":I
    .restart local v1    # "i":I
    const-string v7, "placeId"

    aput-object v7, v6, v2

    .line 111
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "i":I
    .restart local v2    # "i":I
    aput-object v5, v6, v1

    .line 109
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 114
    .end local v5    # "placeId":Ljava/lang/String;
    :cond_0
    sget-object v7, Lcom/google/maps/RoadsApi;->SPEEDS_API_CONFIG:Lcom/google/maps/internal/ApiConfig;

    const-class v8, Lcom/google/maps/RoadsApi$SpeedsResponse;

    invoke-virtual {p0, v7, v8, v6}, Lcom/google/maps/GeoApiContext;->get(Lcom/google/maps/internal/ApiConfig;Ljava/lang/Class;[Ljava/lang/String;)Lcom/google/maps/PendingResult;

    move-result-object v7

    return-object v7
.end method

.class public Lcom/google/maps/internal/OkHttpPendingResult;
.super Ljava/lang/Object;
.source "OkHttpPendingResult.java"

# interfaces
.implements Lcom/google/maps/PendingResult;
.implements Lcom/squareup/okhttp/Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/maps/internal/OkHttpPendingResult$QueuedResponse;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R::",
        "Lcom/google/maps/internal/ApiResponse",
        "<TT;>;>",
        "Ljava/lang/Object;",
        "Lcom/google/maps/PendingResult",
        "<TT;>;",
        "Lcom/squareup/okhttp/Callback;"
    }
.end annotation


# static fields
.field private static final RETRY_ERROR_CODES:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static log:Ljava/util/logging/Logger;


# instance fields
.field private call:Lcom/squareup/okhttp/Call;

.field private callback:Lcom/google/maps/PendingResult$Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/maps/PendingResult$Callback",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final client:Lcom/squareup/okhttp/OkHttpClient;

.field private cumulativeSleepTime:J

.field private errorTimeOut:J

.field private final fieldNamingPolicy:Lcom/google/gson/FieldNamingPolicy;

.field private final request:Lcom/squareup/okhttp/Request;

.field private final responseClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TR;>;"
        }
    .end annotation
.end field

.field private retryCounter:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 64
    const-class v0, Lcom/google/maps/internal/OkHttpPendingResult;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/google/maps/internal/OkHttpPendingResult;->log:Ljava/util/logging/Logger;

    .line 65
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Integer;

    const/4 v1, 0x0

    const/16 v2, 0x1f4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const/16 v2, 0x1f7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const/16 v2, 0x1f8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/google/maps/internal/OkHttpPendingResult;->RETRY_ERROR_CODES:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/okhttp/Request;Lcom/squareup/okhttp/OkHttpClient;Ljava/lang/Class;Lcom/google/gson/FieldNamingPolicy;J)V
    .locals 3
    .param p1, "request"    # Lcom/squareup/okhttp/Request;
    .param p2, "client"    # Lcom/squareup/okhttp/OkHttpClient;
    .param p4, "fieldNamingPolicy"    # Lcom/google/gson/FieldNamingPolicy;
    .param p5, "errorTimeOut"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/okhttp/Request;",
            "Lcom/squareup/okhttp/OkHttpClient;",
            "Ljava/lang/Class",
            "<TR;>;",
            "Lcom/google/gson/FieldNamingPolicy;",
            "J)V"
        }
    .end annotation

    .prologue
    .line 75
    .local p0, "this":Lcom/google/maps/internal/OkHttpPendingResult;, "Lcom/google/maps/internal/OkHttpPendingResult<TT;TR;>;"
    .local p3, "responseClass":Ljava/lang/Class;, "Ljava/lang/Class<TR;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/internal/OkHttpPendingResult;->retryCounter:I

    .line 62
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/maps/internal/OkHttpPendingResult;->cumulativeSleepTime:J

    .line 76
    iput-object p1, p0, Lcom/google/maps/internal/OkHttpPendingResult;->request:Lcom/squareup/okhttp/Request;

    .line 77
    iput-object p2, p0, Lcom/google/maps/internal/OkHttpPendingResult;->client:Lcom/squareup/okhttp/OkHttpClient;

    .line 78
    iput-object p3, p0, Lcom/google/maps/internal/OkHttpPendingResult;->responseClass:Ljava/lang/Class;

    .line 79
    iput-object p4, p0, Lcom/google/maps/internal/OkHttpPendingResult;->fieldNamingPolicy:Lcom/google/gson/FieldNamingPolicy;

    .line 80
    iput-wide p5, p0, Lcom/google/maps/internal/OkHttpPendingResult;->errorTimeOut:J

    .line 82
    invoke-virtual {p2, p1}, Lcom/squareup/okhttp/OkHttpClient;->newCall(Lcom/squareup/okhttp/Request;)Lcom/squareup/okhttp/Call;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/internal/OkHttpPendingResult;->call:Lcom/squareup/okhttp/Call;

    .line 83
    return-void
.end method

.method private getBytes(Lcom/squareup/okhttp/Response;)[B
    .locals 6
    .param p1, "response"    # Lcom/squareup/okhttp/Response;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/google/maps/internal/OkHttpPendingResult;, "Lcom/google/maps/internal/OkHttpPendingResult<TT;TR;>;"
    const/4 v5, 0x0

    .line 248
    invoke-virtual {p1}, Lcom/squareup/okhttp/Response;->body()Lcom/squareup/okhttp/ResponseBody;

    move-result-object v4

    invoke-virtual {v4}, Lcom/squareup/okhttp/ResponseBody;->byteStream()Ljava/io/InputStream;

    move-result-object v3

    .line 249
    .local v3, "in":Ljava/io/InputStream;
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 251
    .local v0, "buffer":Ljava/io/ByteArrayOutputStream;
    const/16 v4, 0x2000

    new-array v2, v4, [B

    .line 252
    .local v2, "data":[B
    :goto_0
    array-length v4, v2

    invoke-virtual {v3, v2, v5, v4}, Ljava/io/InputStream;->read([BII)I

    move-result v1

    .local v1, "bytesRead":I
    const/4 v4, -0x1

    if-eq v1, v4, :cond_0

    .line 253
    invoke-virtual {v0, v2, v5, v1}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_0

    .line 255
    :cond_0
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->flush()V

    .line 256
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    return-object v4
.end method

.method private parseResponse(Lcom/google/maps/internal/OkHttpPendingResult;Lcom/squareup/okhttp/Response;)Ljava/lang/Object;
    .locals 9
    .param p2, "response"    # Lcom/squareup/okhttp/Response;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/maps/internal/OkHttpPendingResult",
            "<TT;TR;>;",
            "Lcom/squareup/okhttp/Response;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 191
    .local p0, "this":Lcom/google/maps/internal/OkHttpPendingResult;, "Lcom/google/maps/internal/OkHttpPendingResult<TT;TR;>;"
    .local p1, "request":Lcom/google/maps/internal/OkHttpPendingResult;, "Lcom/google/maps/internal/OkHttpPendingResult<TT;TR;>;"
    sget-object v4, Lcom/google/maps/internal/OkHttpPendingResult;->RETRY_ERROR_CODES:Ljava/util/List;

    invoke-virtual {p2}, Lcom/squareup/okhttp/Response;->code()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-wide v4, p0, Lcom/google/maps/internal/OkHttpPendingResult;->cumulativeSleepTime:J

    iget-wide v6, p0, Lcom/google/maps/internal/OkHttpPendingResult;->errorTimeOut:J

    cmp-long v4, v4, v6

    if-gez v4, :cond_0

    .line 194
    invoke-direct {p1}, Lcom/google/maps/internal/OkHttpPendingResult;->retry()Ljava/lang/Object;

    move-result-object v4

    .line 239
    :goto_0
    return-object v4

    .line 197
    :cond_0
    new-instance v4, Lcom/google/gson/GsonBuilder;

    invoke-direct {v4}, Lcom/google/gson/GsonBuilder;-><init>()V

    const-class v5, Lorg/joda/time/DateTime;

    new-instance v6, Lcom/google/maps/internal/DateTimeAdapter;

    invoke-direct {v6}, Lcom/google/maps/internal/DateTimeAdapter;-><init>()V

    invoke-virtual {v4, v5, v6}, Lcom/google/gson/GsonBuilder;->registerTypeAdapter(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/GsonBuilder;

    move-result-object v4

    const-class v5, Lcom/google/maps/model/Distance;

    new-instance v6, Lcom/google/maps/internal/DistanceAdapter;

    invoke-direct {v6}, Lcom/google/maps/internal/DistanceAdapter;-><init>()V

    invoke-virtual {v4, v5, v6}, Lcom/google/gson/GsonBuilder;->registerTypeAdapter(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/GsonBuilder;

    move-result-object v4

    const-class v5, Lcom/google/maps/model/Duration;

    new-instance v6, Lcom/google/maps/internal/DurationAdapter;

    invoke-direct {v6}, Lcom/google/maps/internal/DurationAdapter;-><init>()V

    invoke-virtual {v4, v5, v6}, Lcom/google/gson/GsonBuilder;->registerTypeAdapter(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/GsonBuilder;

    move-result-object v4

    const-class v5, Lcom/google/maps/model/Fare;

    new-instance v6, Lcom/google/maps/internal/FareAdapter;

    invoke-direct {v6}, Lcom/google/maps/internal/FareAdapter;-><init>()V

    invoke-virtual {v4, v5, v6}, Lcom/google/gson/GsonBuilder;->registerTypeAdapter(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/GsonBuilder;

    move-result-object v4

    const-class v5, Lcom/google/maps/model/LatLng;

    new-instance v6, Lcom/google/maps/internal/LatLngAdapter;

    invoke-direct {v6}, Lcom/google/maps/internal/LatLngAdapter;-><init>()V

    invoke-virtual {v4, v5, v6}, Lcom/google/gson/GsonBuilder;->registerTypeAdapter(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/GsonBuilder;

    move-result-object v4

    const-class v5, Lcom/google/maps/model/AddressComponentType;

    new-instance v6, Lcom/google/maps/internal/SafeEnumAdapter;

    sget-object v7, Lcom/google/maps/model/AddressComponentType;->UNKNOWN:Lcom/google/maps/model/AddressComponentType;

    invoke-direct {v6, v7}, Lcom/google/maps/internal/SafeEnumAdapter;-><init>(Ljava/lang/Enum;)V

    invoke-virtual {v4, v5, v6}, Lcom/google/gson/GsonBuilder;->registerTypeAdapter(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/GsonBuilder;

    move-result-object v4

    const-class v5, Lcom/google/maps/model/AddressType;

    new-instance v6, Lcom/google/maps/internal/SafeEnumAdapter;

    sget-object v7, Lcom/google/maps/model/AddressType;->UNKNOWN:Lcom/google/maps/model/AddressType;

    invoke-direct {v6, v7}, Lcom/google/maps/internal/SafeEnumAdapter;-><init>(Ljava/lang/Enum;)V

    invoke-virtual {v4, v5, v6}, Lcom/google/gson/GsonBuilder;->registerTypeAdapter(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/GsonBuilder;

    move-result-object v4

    const-class v5, Lcom/google/maps/model/TravelMode;

    new-instance v6, Lcom/google/maps/internal/SafeEnumAdapter;

    sget-object v7, Lcom/google/maps/model/TravelMode;->UNKNOWN:Lcom/google/maps/model/TravelMode;

    invoke-direct {v6, v7}, Lcom/google/maps/internal/SafeEnumAdapter;-><init>(Ljava/lang/Enum;)V

    invoke-virtual {v4, v5, v6}, Lcom/google/gson/GsonBuilder;->registerTypeAdapter(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/GsonBuilder;

    move-result-object v4

    const-class v5, Lcom/google/maps/model/LocationType;

    new-instance v6, Lcom/google/maps/internal/SafeEnumAdapter;

    sget-object v7, Lcom/google/maps/model/LocationType;->UNKNOWN:Lcom/google/maps/model/LocationType;

    invoke-direct {v6, v7}, Lcom/google/maps/internal/SafeEnumAdapter;-><init>(Ljava/lang/Enum;)V

    invoke-virtual {v4, v5, v6}, Lcom/google/gson/GsonBuilder;->registerTypeAdapter(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/GsonBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/maps/internal/OkHttpPendingResult;->fieldNamingPolicy:Lcom/google/gson/FieldNamingPolicy;

    invoke-virtual {v4, v5}, Lcom/google/gson/GsonBuilder;->setFieldNamingPolicy(Lcom/google/gson/FieldNamingPolicy;)Lcom/google/gson/GsonBuilder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/gson/GsonBuilder;->create()Lcom/google/gson/Gson;

    move-result-object v2

    .line 211
    .local v2, "gson":Lcom/google/gson/Gson;
    invoke-direct {p0, p2}, Lcom/google/maps/internal/OkHttpPendingResult;->getBytes(Lcom/squareup/okhttp/Response;)[B

    move-result-object v0

    .line 217
    .local v0, "bytes":[B
    :try_start_0
    new-instance v4, Ljava/lang/String;

    const-string v5, "utf8"

    invoke-direct {v4, v0, v5}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    iget-object v5, p0, Lcom/google/maps/internal/OkHttpPendingResult;->responseClass:Ljava/lang/Class;

    invoke-virtual {v2, v4, v5}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/maps/internal/ApiResponse;
    :try_end_0
    .catch Lcom/google/gson/JsonSyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    .line 232
    .local v3, "resp":Lcom/google/maps/internal/ApiResponse;, "TR;"
    invoke-interface {v3}, Lcom/google/maps/internal/ApiResponse;->successful()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 234
    invoke-interface {v3}, Lcom/google/maps/internal/ApiResponse;->getResult()Ljava/lang/Object;

    move-result-object v4

    goto/16 :goto_0

    .line 218
    .end local v3    # "resp":Lcom/google/maps/internal/ApiResponse;, "TR;"
    :catch_0
    move-exception v1

    .line 220
    .local v1, "e":Lcom/google/gson/JsonSyntaxException;
    invoke-virtual {p2}, Lcom/squareup/okhttp/Response;->isSuccessful()Z

    move-result v4

    if-nez v4, :cond_1

    .line 224
    new-instance v4, Ljava/io/IOException;

    const-string v5, "Server Error: %d %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {p2}, Lcom/squareup/okhttp/Response;->code()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-virtual {p2}, Lcom/squareup/okhttp/Response;->message()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 229
    :cond_1
    throw v1

    .line 236
    .end local v1    # "e":Lcom/google/gson/JsonSyntaxException;
    .restart local v3    # "resp":Lcom/google/maps/internal/ApiResponse;, "TR;"
    :cond_2
    invoke-interface {v3}, Lcom/google/maps/internal/ApiResponse;->getError()Lcom/google/maps/errors/ApiException;

    move-result-object v1

    .line 237
    .local v1, "e":Lcom/google/maps/errors/ApiException;
    instance-of v4, v1, Lcom/google/maps/errors/OverQueryLimitException;

    if-eqz v4, :cond_3

    iget-wide v4, p0, Lcom/google/maps/internal/OkHttpPendingResult;->cumulativeSleepTime:J

    iget-wide v6, p0, Lcom/google/maps/internal/OkHttpPendingResult;->errorTimeOut:J

    cmp-long v4, v4, v6

    if-gez v4, :cond_3

    .line 239
    invoke-direct {p1}, Lcom/google/maps/internal/OkHttpPendingResult;->retry()Ljava/lang/Object;

    move-result-object v4

    goto/16 :goto_0

    .line 242
    :cond_3
    throw v1
.end method

.method private retry()Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 260
    .local p0, "this":Lcom/google/maps/internal/OkHttpPendingResult;, "Lcom/google/maps/internal/OkHttpPendingResult<TT;TR;>;"
    iget v0, p0, Lcom/google/maps/internal/OkHttpPendingResult;->retryCounter:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/maps/internal/OkHttpPendingResult;->retryCounter:I

    .line 261
    sget-object v0, Lcom/google/maps/internal/OkHttpPendingResult;->log:Ljava/util/logging/Logger;

    iget v1, p0, Lcom/google/maps/internal/OkHttpPendingResult;->retryCounter:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x24

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Retrying request. Retry #"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    .line 262
    iget-object v0, p0, Lcom/google/maps/internal/OkHttpPendingResult;->client:Lcom/squareup/okhttp/OkHttpClient;

    iget-object v1, p0, Lcom/google/maps/internal/OkHttpPendingResult;->request:Lcom/squareup/okhttp/Request;

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/OkHttpClient;->newCall(Lcom/squareup/okhttp/Request;)Lcom/squareup/okhttp/Call;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/internal/OkHttpPendingResult;->call:Lcom/squareup/okhttp/Call;

    .line 263
    invoke-virtual {p0}, Lcom/google/maps/internal/OkHttpPendingResult;->await()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public await()Ljava/lang/Object;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/google/maps/internal/OkHttpPendingResult;, "Lcom/google/maps/internal/OkHttpPendingResult<TT;TR;>;"
    const/4 v14, 0x1

    const-wide/high16 v12, 0x3fe0000000000000L    # 0.5

    .line 114
    iget v7, p0, Lcom/google/maps/internal/OkHttpPendingResult;->retryCounter:I

    if-lez v7, :cond_0

    .line 118
    const-wide/high16 v8, 0x3ff8000000000000L    # 1.5

    iget v7, p0, Lcom/google/maps/internal/OkHttpPendingResult;->retryCounter:I

    add-int/lit8 v7, v7, -0x1

    int-to-double v10, v7

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v8

    mul-double v2, v12, v8

    .line 121
    .local v2, "delaySecs":D
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v8

    add-double/2addr v8, v12

    mul-double/2addr v8, v2

    const-wide v10, 0x408f400000000000L    # 1000.0

    mul-double/2addr v8, v10

    double-to-long v0, v8

    .line 123
    .local v0, "delayMillis":J
    sget-object v7, Lcom/google/maps/internal/OkHttpPendingResult;->log:Ljava/util/logging/Logger;

    const-string v8, "Sleeping between errors for %dms (retry #%d, already slept %dms)"

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    iget v10, p0, Lcom/google/maps/internal/OkHttpPendingResult;->retryCounter:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v14

    const/4 v10, 0x2

    iget-wide v12, p0, Lcom/google/maps/internal/OkHttpPendingResult;->cumulativeSleepTime:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/logging/Logger;->config(Ljava/lang/String;)V

    .line 125
    iget-wide v8, p0, Lcom/google/maps/internal/OkHttpPendingResult;->cumulativeSleepTime:J

    add-long/2addr v8, v0

    iput-wide v8, p0, Lcom/google/maps/internal/OkHttpPendingResult;->cumulativeSleepTime:J

    .line 127
    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 133
    .end local v0    # "delayMillis":J
    .end local v2    # "delaySecs":D
    :cond_0
    :goto_0
    new-instance v6, Ljava/util/concurrent/ArrayBlockingQueue;

    invoke-direct {v6, v14}, Ljava/util/concurrent/ArrayBlockingQueue;-><init>(I)V

    .line 134
    .local v6, "waiter":Ljava/util/concurrent/BlockingQueue;, "Ljava/util/concurrent/BlockingQueue<Lcom/google/maps/internal/OkHttpPendingResult<TT;TR;>.QueuedResponse;>;"
    move-object v4, p0

    .line 138
    .local v4, "parent":Lcom/google/maps/internal/OkHttpPendingResult;, "Lcom/google/maps/internal/OkHttpPendingResult<TT;TR;>;"
    iget-object v7, p0, Lcom/google/maps/internal/OkHttpPendingResult;->call:Lcom/squareup/okhttp/Call;

    new-instance v8, Lcom/google/maps/internal/OkHttpPendingResult$1;

    invoke-direct {v8, p0, v6, v4}, Lcom/google/maps/internal/OkHttpPendingResult$1;-><init>(Lcom/google/maps/internal/OkHttpPendingResult;Ljava/util/concurrent/BlockingQueue;Lcom/google/maps/internal/OkHttpPendingResult;)V

    invoke-virtual {v7, v8}, Lcom/squareup/okhttp/Call;->enqueue(Lcom/squareup/okhttp/Callback;)V

    .line 150
    invoke-interface {v6}, Ljava/util/concurrent/BlockingQueue;->take()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/maps/internal/OkHttpPendingResult$QueuedResponse;

    .line 151
    .local v5, "r":Lcom/google/maps/internal/OkHttpPendingResult$QueuedResponse;, "Lcom/google/maps/internal/OkHttpPendingResult<TT;TR;>.QueuedResponse;"
    invoke-static {v5}, Lcom/google/maps/internal/OkHttpPendingResult$QueuedResponse;->access$000(Lcom/google/maps/internal/OkHttpPendingResult$QueuedResponse;)Lcom/squareup/okhttp/Response;

    move-result-object v7

    if-eqz v7, :cond_1

    .line 152
    invoke-static {v5}, Lcom/google/maps/internal/OkHttpPendingResult$QueuedResponse;->access$100(Lcom/google/maps/internal/OkHttpPendingResult$QueuedResponse;)Lcom/google/maps/internal/OkHttpPendingResult;

    move-result-object v7

    invoke-static {v5}, Lcom/google/maps/internal/OkHttpPendingResult$QueuedResponse;->access$000(Lcom/google/maps/internal/OkHttpPendingResult$QueuedResponse;)Lcom/squareup/okhttp/Response;

    move-result-object v8

    invoke-direct {p0, v7, v8}, Lcom/google/maps/internal/OkHttpPendingResult;->parseResponse(Lcom/google/maps/internal/OkHttpPendingResult;Lcom/squareup/okhttp/Response;)Ljava/lang/Object;

    move-result-object v7

    return-object v7

    .line 154
    :cond_1
    invoke-static {v5}, Lcom/google/maps/internal/OkHttpPendingResult$QueuedResponse;->access$200(Lcom/google/maps/internal/OkHttpPendingResult$QueuedResponse;)Ljava/lang/Exception;

    move-result-object v7

    throw v7

    .line 128
    .end local v4    # "parent":Lcom/google/maps/internal/OkHttpPendingResult;, "Lcom/google/maps/internal/OkHttpPendingResult<TT;TR;>;"
    .end local v5    # "r":Lcom/google/maps/internal/OkHttpPendingResult$QueuedResponse;, "Lcom/google/maps/internal/OkHttpPendingResult<TT;TR;>.QueuedResponse;"
    .end local v6    # "waiter":Ljava/util/concurrent/BlockingQueue;, "Ljava/util/concurrent/BlockingQueue<Lcom/google/maps/internal/OkHttpPendingResult<TT;TR;>.QueuedResponse;>;"
    .restart local v0    # "delayMillis":J
    .restart local v2    # "delaySecs":D
    :catch_0
    move-exception v7

    goto :goto_0
.end method

.method public awaitIgnoreError()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 161
    .local p0, "this":Lcom/google/maps/internal/OkHttpPendingResult;, "Lcom/google/maps/internal/OkHttpPendingResult<TT;TR;>;"
    :try_start_0
    invoke-virtual {p0}, Lcom/google/maps/internal/OkHttpPendingResult;->await()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 163
    :goto_0
    return-object v1

    .line 162
    :catch_0
    move-exception v0

    .line 163
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public cancel()V
    .locals 1

    .prologue
    .line 169
    .local p0, "this":Lcom/google/maps/internal/OkHttpPendingResult;, "Lcom/google/maps/internal/OkHttpPendingResult<TT;TR;>;"
    iget-object v0, p0, Lcom/google/maps/internal/OkHttpPendingResult;->call:Lcom/squareup/okhttp/Call;

    invoke-virtual {v0}, Lcom/squareup/okhttp/Call;->cancel()V

    .line 170
    return-void
.end method

.method public onFailure(Lcom/squareup/okhttp/Request;Ljava/io/IOException;)V
    .locals 1
    .param p1, "request"    # Lcom/squareup/okhttp/Request;
    .param p2, "ioe"    # Ljava/io/IOException;

    .prologue
    .line 174
    .local p0, "this":Lcom/google/maps/internal/OkHttpPendingResult;, "Lcom/google/maps/internal/OkHttpPendingResult<TT;TR;>;"
    iget-object v0, p0, Lcom/google/maps/internal/OkHttpPendingResult;->callback:Lcom/google/maps/PendingResult$Callback;

    if-eqz v0, :cond_0

    .line 175
    iget-object v0, p0, Lcom/google/maps/internal/OkHttpPendingResult;->callback:Lcom/google/maps/PendingResult$Callback;

    invoke-interface {v0, p2}, Lcom/google/maps/PendingResult$Callback;->onFailure(Ljava/lang/Throwable;)V

    .line 177
    :cond_0
    return-void
.end method

.method public onResponse(Lcom/squareup/okhttp/Response;)V
    .locals 3
    .param p1, "response"    # Lcom/squareup/okhttp/Response;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 181
    .local p0, "this":Lcom/google/maps/internal/OkHttpPendingResult;, "Lcom/google/maps/internal/OkHttpPendingResult<TT;TR;>;"
    iget-object v1, p0, Lcom/google/maps/internal/OkHttpPendingResult;->callback:Lcom/google/maps/PendingResult$Callback;

    if-eqz v1, :cond_0

    .line 183
    :try_start_0
    iget-object v1, p0, Lcom/google/maps/internal/OkHttpPendingResult;->callback:Lcom/google/maps/PendingResult$Callback;

    invoke-direct {p0, p0, p1}, Lcom/google/maps/internal/OkHttpPendingResult;->parseResponse(Lcom/google/maps/internal/OkHttpPendingResult;Lcom/squareup/okhttp/Response;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/maps/PendingResult$Callback;->onResult(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 188
    :cond_0
    :goto_0
    return-void

    .line 184
    :catch_0
    move-exception v0

    .line 185
    .local v0, "e":Ljava/lang/Exception;
    iget-object v1, p0, Lcom/google/maps/internal/OkHttpPendingResult;->callback:Lcom/google/maps/PendingResult$Callback;

    invoke-interface {v1, v0}, Lcom/google/maps/PendingResult$Callback;->onFailure(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public setCallback(Lcom/google/maps/PendingResult$Callback;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/maps/PendingResult$Callback",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 87
    .local p0, "this":Lcom/google/maps/internal/OkHttpPendingResult;, "Lcom/google/maps/internal/OkHttpPendingResult<TT;TR;>;"
    .local p1, "callback":Lcom/google/maps/PendingResult$Callback;, "Lcom/google/maps/PendingResult$Callback<TT;>;"
    iput-object p1, p0, Lcom/google/maps/internal/OkHttpPendingResult;->callback:Lcom/google/maps/PendingResult$Callback;

    .line 88
    iget-object v0, p0, Lcom/google/maps/internal/OkHttpPendingResult;->call:Lcom/squareup/okhttp/Call;

    invoke-virtual {v0, p0}, Lcom/squareup/okhttp/Call;->enqueue(Lcom/squareup/okhttp/Callback;)V

    .line 89
    return-void
.end method

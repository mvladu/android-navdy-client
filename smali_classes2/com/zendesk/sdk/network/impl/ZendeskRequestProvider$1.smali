.class Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$1;
.super Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback;
.source "ZendeskRequestProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;->createRequest(Lcom/zendesk/sdk/model/request/CreateRequest;Lcom/zendesk/service/ZendeskCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback",
        "<",
        "Lcom/zendesk/sdk/model/SdkConfiguration;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;

.field final synthetic val$callback:Lcom/zendesk/service/ZendeskCallback;

.field final synthetic val$request:Lcom/zendesk/sdk/model/request/CreateRequest;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/sdk/model/request/CreateRequest;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;
    .param p2, "callback"    # Lcom/zendesk/service/ZendeskCallback;

    .prologue
    .line 82
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$1;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;

    iput-object p3, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$1;->val$request:Lcom/zendesk/sdk/model/request/CreateRequest;

    iput-object p4, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$1;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    invoke-direct {p0, p2}, Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback;-><init>(Lcom/zendesk/service/ZendeskCallback;)V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/zendesk/sdk/model/SdkConfiguration;)V
    .locals 5
    .param p1, "config"    # Lcom/zendesk/sdk/model/SdkConfiguration;

    .prologue
    .line 85
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$1;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/SdkConfiguration;->getBearerAuthorizationHeader()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$1;->val$request:Lcom/zendesk/sdk/model/request/CreateRequest;

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/SdkConfiguration;->getMobileSettings()Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    move-result-object v3

    invoke-virtual {v3}, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->getAuthenticationType()Lcom/zendesk/sdk/model/access/AuthenticationType;

    move-result-object v3

    iget-object v4, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$1;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;->internalCreateRequest(Ljava/lang/String;Lcom/zendesk/sdk/model/request/CreateRequest;Lcom/zendesk/sdk/model/access/AuthenticationType;Lcom/zendesk/service/ZendeskCallback;)V

    .line 86
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 82
    check-cast p1, Lcom/zendesk/sdk/model/SdkConfiguration;

    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$1;->onSuccess(Lcom/zendesk/sdk/model/SdkConfiguration;)V

    return-void
.end method

.class Lcom/zendesk/sdk/network/impl/StubProviderStore;
.super Ljava/lang/Object;
.source "StubProviderStore.java"

# interfaces
.implements Lcom/zendesk/sdk/network/impl/ProviderStore;


# instance fields
.field final helpCenterProvider:Lcom/zendesk/sdk/network/HelpCenterProvider;

.field final networkInfoProvider:Lcom/zendesk/sdk/network/NetworkInfoProvider;

.field final pushRegistrationProvider:Lcom/zendesk/sdk/network/PushRegistrationProvider;

.field final requestProvider:Lcom/zendesk/sdk/network/RequestProvider;

.field final sdkSettingsProvider:Lcom/zendesk/sdk/network/SdkSettingsProvider;

.field final settingsHelper:Lcom/zendesk/sdk/network/SettingsHelper;

.field final uploadProvider:Lcom/zendesk/sdk/network/UploadProvider;

.field final userProvider:Lcom/zendesk/sdk/network/UserProvider;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const-class v0, Lcom/zendesk/sdk/network/UserProvider;

    invoke-static {v0}, Lcom/zendesk/sdk/network/impl/StubProviderFactory;->getStubProvider(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/zendesk/sdk/network/UserProvider;

    iput-object v0, p0, Lcom/zendesk/sdk/network/impl/StubProviderStore;->userProvider:Lcom/zendesk/sdk/network/UserProvider;

    .line 29
    const-class v0, Lcom/zendesk/sdk/network/HelpCenterProvider;

    invoke-static {v0}, Lcom/zendesk/sdk/network/impl/StubProviderFactory;->getStubProvider(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/zendesk/sdk/network/HelpCenterProvider;

    iput-object v0, p0, Lcom/zendesk/sdk/network/impl/StubProviderStore;->helpCenterProvider:Lcom/zendesk/sdk/network/HelpCenterProvider;

    .line 30
    const-class v0, Lcom/zendesk/sdk/network/PushRegistrationProvider;

    invoke-static {v0}, Lcom/zendesk/sdk/network/impl/StubProviderFactory;->getStubProvider(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/zendesk/sdk/network/PushRegistrationProvider;

    iput-object v0, p0, Lcom/zendesk/sdk/network/impl/StubProviderStore;->pushRegistrationProvider:Lcom/zendesk/sdk/network/PushRegistrationProvider;

    .line 31
    const-class v0, Lcom/zendesk/sdk/network/RequestProvider;

    invoke-static {v0}, Lcom/zendesk/sdk/network/impl/StubProviderFactory;->getStubProvider(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/zendesk/sdk/network/RequestProvider;

    iput-object v0, p0, Lcom/zendesk/sdk/network/impl/StubProviderStore;->requestProvider:Lcom/zendesk/sdk/network/RequestProvider;

    .line 32
    const-class v0, Lcom/zendesk/sdk/network/UploadProvider;

    invoke-static {v0}, Lcom/zendesk/sdk/network/impl/StubProviderFactory;->getStubProvider(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/zendesk/sdk/network/UploadProvider;

    iput-object v0, p0, Lcom/zendesk/sdk/network/impl/StubProviderStore;->uploadProvider:Lcom/zendesk/sdk/network/UploadProvider;

    .line 33
    const-class v0, Lcom/zendesk/sdk/network/SdkSettingsProvider;

    invoke-static {v0}, Lcom/zendesk/sdk/network/impl/StubProviderFactory;->getStubProvider(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/zendesk/sdk/network/SdkSettingsProvider;

    iput-object v0, p0, Lcom/zendesk/sdk/network/impl/StubProviderStore;->sdkSettingsProvider:Lcom/zendesk/sdk/network/SdkSettingsProvider;

    .line 34
    const-class v0, Lcom/zendesk/sdk/network/NetworkInfoProvider;

    invoke-static {v0}, Lcom/zendesk/sdk/network/impl/StubProviderFactory;->getStubProvider(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/zendesk/sdk/network/NetworkInfoProvider;

    iput-object v0, p0, Lcom/zendesk/sdk/network/impl/StubProviderStore;->networkInfoProvider:Lcom/zendesk/sdk/network/NetworkInfoProvider;

    .line 35
    const-class v0, Lcom/zendesk/sdk/network/SettingsHelper;

    invoke-static {v0}, Lcom/zendesk/sdk/network/impl/StubProviderFactory;->getStubProvider(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/zendesk/sdk/network/SettingsHelper;

    iput-object v0, p0, Lcom/zendesk/sdk/network/impl/StubProviderStore;->settingsHelper:Lcom/zendesk/sdk/network/SettingsHelper;

    .line 36
    return-void
.end method


# virtual methods
.method public helpCenterProvider()Lcom/zendesk/sdk/network/HelpCenterProvider;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/StubProviderStore;->helpCenterProvider:Lcom/zendesk/sdk/network/HelpCenterProvider;

    return-object v0
.end method

.method public networkInfoProvider()Lcom/zendesk/sdk/network/NetworkInfoProvider;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/StubProviderStore;->networkInfoProvider:Lcom/zendesk/sdk/network/NetworkInfoProvider;

    return-object v0
.end method

.method public pushRegistrationProvider()Lcom/zendesk/sdk/network/PushRegistrationProvider;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/StubProviderStore;->pushRegistrationProvider:Lcom/zendesk/sdk/network/PushRegistrationProvider;

    return-object v0
.end method

.method public requestProvider()Lcom/zendesk/sdk/network/RequestProvider;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/StubProviderStore;->requestProvider:Lcom/zendesk/sdk/network/RequestProvider;

    return-object v0
.end method

.method public sdkSettingsProvider()Lcom/zendesk/sdk/network/SdkSettingsProvider;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/StubProviderStore;->sdkSettingsProvider:Lcom/zendesk/sdk/network/SdkSettingsProvider;

    return-object v0
.end method

.method public uiSettingsHelper()Lcom/zendesk/sdk/network/SettingsHelper;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/StubProviderStore;->settingsHelper:Lcom/zendesk/sdk/network/SettingsHelper;

    return-object v0
.end method

.method public uploadProvider()Lcom/zendesk/sdk/network/UploadProvider;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/StubProviderStore;->uploadProvider:Lcom/zendesk/sdk/network/UploadProvider;

    return-object v0
.end method

.method public userProvider()Lcom/zendesk/sdk/network/UserProvider;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/StubProviderStore;->userProvider:Lcom/zendesk/sdk/network/UserProvider;

    return-object v0
.end method

.class Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider;
.super Ljava/lang/Object;
.source "ZendeskBaseProvider.java"

# interfaces
.implements Lcom/zendesk/sdk/network/BaseProvider;


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "BaseProvider"

.field private static final SDK_SETTINGS_MAX_AGE_HOURS:I = 0x1


# instance fields
.field private final accessProvider:Lcom/zendesk/sdk/network/AccessProvider;

.field private final identityStorage:Lcom/zendesk/sdk/storage/IdentityStorage;

.field private final sdkSettingsStorage:Lcom/zendesk/sdk/storage/SdkSettingsStorage;

.field private settingsProvider:Lcom/zendesk/sdk/network/SdkSettingsProvider;


# direct methods
.method public constructor <init>(Lcom/zendesk/sdk/network/AccessProvider;Lcom/zendesk/sdk/storage/SdkSettingsStorage;Lcom/zendesk/sdk/storage/IdentityStorage;Lcom/zendesk/sdk/network/SdkSettingsProvider;)V
    .locals 0
    .param p1, "accessProvider"    # Lcom/zendesk/sdk/network/AccessProvider;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "settingsStorage"    # Lcom/zendesk/sdk/storage/SdkSettingsStorage;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "identityStorage"    # Lcom/zendesk/sdk/storage/IdentityStorage;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p4, "sdkSettingsProvider"    # Lcom/zendesk/sdk/network/SdkSettingsProvider;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider;->accessProvider:Lcom/zendesk/sdk/network/AccessProvider;

    .line 46
    iput-object p3, p0, Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider;->identityStorage:Lcom/zendesk/sdk/storage/IdentityStorage;

    .line 47
    iput-object p2, p0, Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider;->sdkSettingsStorage:Lcom/zendesk/sdk/storage/SdkSettingsStorage;

    .line 48
    iput-object p4, p0, Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider;->settingsProvider:Lcom/zendesk/sdk/network/SdkSettingsProvider;

    .line 49
    return-void
.end method


# virtual methods
.method public configureSdk(Lcom/zendesk/service/ZendeskCallback;)V
    .locals 1
    .param p1    # Lcom/zendesk/service/ZendeskCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/SdkConfiguration;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 53
    .local p1, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Lcom/zendesk/sdk/model/SdkConfiguration;>;"
    new-instance v0, Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider$1;

    invoke-direct {v0, p0, p1}, Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider$1;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider;Lcom/zendesk/service/ZendeskCallback;)V

    invoke-virtual {p0, v0}, Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider;->getSdkSettings(Lcom/zendesk/service/ZendeskCallback;)V

    .line 81
    return-void
.end method

.method public getAccessToken(Lcom/zendesk/sdk/model/settings/SafeMobileSettings;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 9
    .param p1, "mobileSettings"    # Lcom/zendesk/sdk/model/settings/SafeMobileSettings;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/zendesk/service/ZendeskCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/zendesk/sdk/model/settings/SafeMobileSettings;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/access/AccessToken;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Lcom/zendesk/sdk/model/access/AccessToken;>;"
    const/4 v8, 0x0

    .line 101
    iget-object v5, p0, Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider;->identityStorage:Lcom/zendesk/sdk/storage/IdentityStorage;

    invoke-interface {v5}, Lcom/zendesk/sdk/storage/IdentityStorage;->getStoredAccessToken()Lcom/zendesk/sdk/model/access/AccessToken;

    move-result-object v0

    .line 103
    .local v0, "accessToken":Lcom/zendesk/sdk/model/access/AccessToken;
    if-eqz v0, :cond_1

    .line 104
    const-string v5, "BaseProvider"

    const-string v6, "We have a stored access token so we will use that."

    new-array v7, v8, [Ljava/lang/Object;

    invoke-static {v5, v6, v7}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 106
    if-eqz p2, :cond_1

    .line 107
    invoke-virtual {p2, v0}, Lcom/zendesk/service/ZendeskCallback;->onSuccess(Ljava/lang/Object;)V

    .line 154
    :cond_0
    :goto_0
    return-void

    .line 112
    :cond_1
    const-string v5, "BaseProvider"

    const-string v6, "We do not have a stored access token."

    new-array v7, v8, [Ljava/lang/Object;

    invoke-static {v5, v6, v7}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 114
    iget-object v5, p0, Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider;->identityStorage:Lcom/zendesk/sdk/storage/IdentityStorage;

    invoke-interface {v5}, Lcom/zendesk/sdk/storage/IdentityStorage;->getIdentity()Lcom/zendesk/sdk/model/access/Identity;

    move-result-object v4

    .line 115
    .local v4, "identity":Lcom/zendesk/sdk/model/access/Identity;
    invoke-virtual {p1}, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->getAuthenticationType()Lcom/zendesk/sdk/model/access/AuthenticationType;

    move-result-object v1

    .line 117
    .local v1, "authenticationType":Lcom/zendesk/sdk/model/access/AuthenticationType;
    sget-object v5, Lcom/zendesk/sdk/model/access/AuthenticationType;->ANONYMOUS:Lcom/zendesk/sdk/model/access/AuthenticationType;

    if-ne v5, v1, :cond_2

    instance-of v5, v4, Lcom/zendesk/sdk/model/access/AnonymousIdentity;

    if-eqz v5, :cond_2

    .line 118
    iget-object v5, p0, Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider;->accessProvider:Lcom/zendesk/sdk/network/AccessProvider;

    check-cast v4, Lcom/zendesk/sdk/model/access/AnonymousIdentity;

    .end local v4    # "identity":Lcom/zendesk/sdk/model/access/Identity;
    new-instance v6, Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider$2;

    invoke-direct {v6, p0, p2, p2}, Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider$2;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider;Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/service/ZendeskCallback;)V

    invoke-interface {v5, v4, v6}, Lcom/zendesk/sdk/network/AccessProvider;->getAndStoreAuthTokenViaAnonymous(Lcom/zendesk/sdk/model/access/AnonymousIdentity;Lcom/zendesk/service/ZendeskCallback;)V

    goto :goto_0

    .line 127
    .restart local v4    # "identity":Lcom/zendesk/sdk/model/access/Identity;
    :cond_2
    sget-object v5, Lcom/zendesk/sdk/model/access/AuthenticationType;->JWT:Lcom/zendesk/sdk/model/access/AuthenticationType;

    if-ne v5, v1, :cond_3

    instance-of v5, v4, Lcom/zendesk/sdk/model/access/JwtIdentity;

    if-eqz v5, :cond_3

    .line 128
    iget-object v5, p0, Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider;->accessProvider:Lcom/zendesk/sdk/network/AccessProvider;

    check-cast v4, Lcom/zendesk/sdk/model/access/JwtIdentity;

    .end local v4    # "identity":Lcom/zendesk/sdk/model/access/Identity;
    new-instance v6, Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider$3;

    invoke-direct {v6, p0, p2, p2}, Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider$3;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider;Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/service/ZendeskCallback;)V

    invoke-interface {v5, v4, v6}, Lcom/zendesk/sdk/network/AccessProvider;->getAndStoreAuthTokenViaJwt(Lcom/zendesk/sdk/model/access/JwtIdentity;Lcom/zendesk/service/ZendeskCallback;)V

    goto :goto_0

    .line 144
    .restart local v4    # "identity":Lcom/zendesk/sdk/model/access/Identity;
    :cond_3
    iget-object v5, p0, Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider;->sdkSettingsStorage:Lcom/zendesk/sdk/storage/SdkSettingsStorage;

    invoke-interface {v5}, Lcom/zendesk/sdk/storage/SdkSettingsStorage;->deleteStoredSettings()V

    .line 146
    new-instance v3, Lcom/zendesk/sdk/network/impl/AuthenticationLoggerHelper;

    invoke-direct {v3, v1, v4}, Lcom/zendesk/sdk/network/impl/AuthenticationLoggerHelper;-><init>(Lcom/zendesk/sdk/model/access/AuthenticationType;Lcom/zendesk/sdk/model/access/Identity;)V

    .line 147
    .local v3, "helper":Lcom/zendesk/sdk/network/impl/AuthenticationLoggerHelper;
    invoke-virtual {v3}, Lcom/zendesk/sdk/network/impl/AuthenticationLoggerHelper;->getLogMessage()Ljava/lang/String;

    move-result-object v2

    .line 148
    .local v2, "error":Ljava/lang/String;
    const-string v5, "BaseProvider"

    new-array v6, v8, [Ljava/lang/Object;

    invoke-static {v5, v2, v6}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 150
    if-eqz p2, :cond_0

    .line 151
    new-instance v5, Lcom/zendesk/service/ErrorResponseAdapter;

    invoke-direct {v5, v2}, Lcom/zendesk/service/ErrorResponseAdapter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v5}, Lcom/zendesk/service/ZendeskCallback;->onError(Lcom/zendesk/service/ErrorResponse;)V

    goto :goto_0
.end method

.method public getSdkSettings(Lcom/zendesk/service/ZendeskCallback;)V
    .locals 7
    .param p1    # Lcom/zendesk/service/ZendeskCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/settings/SafeMobileSettings;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Lcom/zendesk/sdk/model/settings/SafeMobileSettings;>;"
    const/4 v6, 0x0

    .line 85
    iget-object v2, p0, Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider;->sdkSettingsStorage:Lcom/zendesk/sdk/storage/SdkSettingsStorage;

    invoke-interface {v2}, Lcom/zendesk/sdk/storage/SdkSettingsStorage;->hasStoredSdkSettings()Z

    move-result v1

    .line 86
    .local v1, "hasStoredSetting":Z
    iget-object v2, p0, Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider;->sdkSettingsStorage:Lcom/zendesk/sdk/storage/SdkSettingsStorage;

    const-wide/16 v4, 0x1

    sget-object v3, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v2, v4, v5, v3}, Lcom/zendesk/sdk/storage/SdkSettingsStorage;->areSettingsUpToDate(JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    .line 88
    .local v0, "areSettingsUpToDate":Z
    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    .line 89
    const-string v2, "BaseProvider"

    const-string v3, "Settings available - skipping download"

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 90
    if-eqz p1, :cond_0

    .line 91
    iget-object v2, p0, Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider;->sdkSettingsStorage:Lcom/zendesk/sdk/storage/SdkSettingsStorage;

    invoke-interface {v2}, Lcom/zendesk/sdk/storage/SdkSettingsStorage;->getStoredSettings()Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/zendesk/service/ZendeskCallback;->onSuccess(Ljava/lang/Object;)V

    .line 97
    :cond_0
    :goto_0
    return-void

    .line 94
    :cond_1
    const-string v2, "BaseProvider"

    const-string v3, "Downloading settings: \'hasStoredSetting\': %b | \'areSettingsUpToDate:\' %b"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v4, v6

    const/4 v5, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 95
    iget-object v2, p0, Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider;->settingsProvider:Lcom/zendesk/sdk/network/SdkSettingsProvider;

    invoke-interface {v2, p1}, Lcom/zendesk/sdk/network/SdkSettingsProvider;->getSettings(Lcom/zendesk/service/ZendeskCallback;)V

    goto :goto_0
.end method

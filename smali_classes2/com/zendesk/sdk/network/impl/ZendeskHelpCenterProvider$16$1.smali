.class Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$16$1;
.super Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback;
.source "ZendeskHelpCenterProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$16;->onSuccess(Lcom/zendesk/sdk/model/SdkConfiguration;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$16;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$16;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 0
    .param p1, "this$1"    # Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$16;
    .param p2, "callback"    # Lcom/zendesk/service/ZendeskCallback;

    .prologue
    .line 485
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$16$1;->this$1:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$16;

    invoke-direct {p0, p2}, Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback;-><init>(Lcom/zendesk/service/ZendeskCallback;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 485
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$16$1;->onSuccess(Ljava/lang/Void;)V

    return-void
.end method

.method public onSuccess(Ljava/lang/Void;)V
    .locals 1
    .param p1, "genericResponse"    # Ljava/lang/Void;

    .prologue
    .line 488
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$16$1;->this$1:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$16;

    iget-object v0, v0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$16;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;

    invoke-static {v0}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;->access$100(Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;)Lcom/zendesk/sdk/storage/HelpCenterSessionCache;

    move-result-object v0

    invoke-interface {v0}, Lcom/zendesk/sdk/storage/HelpCenterSessionCache;->unsetUniqueSearchResultClick()V

    .line 490
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$16$1;->this$1:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$16;

    iget-object v0, v0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$16;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    if-eqz v0, :cond_0

    .line 491
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$16$1;->this$1:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$16;

    iget-object v0, v0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$16;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    invoke-virtual {v0, p1}, Lcom/zendesk/service/ZendeskCallback;->onSuccess(Ljava/lang/Object;)V

    .line 493
    :cond_0
    return-void
.end method

.class Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$2;
.super Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback;
.source "ZendeskRequestProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;->internalCreateRequest(Ljava/lang/String;Lcom/zendesk/sdk/model/request/CreateRequest;Lcom/zendesk/sdk/model/access/AuthenticationType;Lcom/zendesk/service/ZendeskCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback",
        "<",
        "Lcom/zendesk/sdk/model/request/Request;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;

.field final synthetic val$authentication:Lcom/zendesk/sdk/model/access/AuthenticationType;

.field final synthetic val$callback:Lcom/zendesk/service/ZendeskCallback;

.field final synthetic val$request:Lcom/zendesk/sdk/model/request/CreateRequest;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/sdk/model/request/CreateRequest;Lcom/zendesk/sdk/model/access/AuthenticationType;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;
    .param p2, "callback"    # Lcom/zendesk/service/ZendeskCallback;

    .prologue
    .line 91
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$2;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;

    iput-object p3, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$2;->val$request:Lcom/zendesk/sdk/model/request/CreateRequest;

    iput-object p4, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$2;->val$authentication:Lcom/zendesk/sdk/model/access/AuthenticationType;

    iput-object p5, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$2;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    invoke-direct {p0, p2}, Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback;-><init>(Lcom/zendesk/service/ZendeskCallback;)V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/zendesk/sdk/model/request/Request;)V
    .locals 2
    .param p1, "result"    # Lcom/zendesk/sdk/model/request/Request;

    .prologue
    .line 95
    invoke-virtual {p1}, Lcom/zendesk/sdk/model/request/Request;->getId()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 96
    new-instance v0, Lcom/zendesk/service/ErrorResponseAdapter;

    const-string v1, "The request was created, but the ID is unknown."

    invoke-direct {v0, v1}, Lcom/zendesk/service/ErrorResponseAdapter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$2;->onError(Lcom/zendesk/service/ErrorResponse;)V

    .line 108
    :cond_0
    :goto_0
    return-void

    .line 98
    :cond_1
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$2;->val$request:Lcom/zendesk/sdk/model/request/CreateRequest;

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/request/Request;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/zendesk/sdk/model/request/CreateRequest;->setId(Ljava/lang/String;)V

    .line 100
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$2;->val$authentication:Lcom/zendesk/sdk/model/access/AuthenticationType;

    sget-object v1, Lcom/zendesk/sdk/model/access/AuthenticationType;->ANONYMOUS:Lcom/zendesk/sdk/model/access/AuthenticationType;

    if-ne v0, v1, :cond_2

    .line 101
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$2;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;

    invoke-static {v0}, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;->access$000(Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;)Lcom/zendesk/sdk/storage/RequestStorage;

    move-result-object v0

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/request/Request;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/zendesk/sdk/storage/RequestStorage;->storeRequestId(Ljava/lang/String;)V

    .line 104
    :cond_2
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$2;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$2;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$2;->val$request:Lcom/zendesk/sdk/model/request/CreateRequest;

    invoke-virtual {v0, v1}, Lcom/zendesk/service/ZendeskCallback;->onSuccess(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 91
    check-cast p1, Lcom/zendesk/sdk/model/request/Request;

    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$2;->onSuccess(Lcom/zendesk/sdk/model/request/Request;)V

    return-void
.end method

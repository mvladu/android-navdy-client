.class Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService$1;
.super Ljava/lang/Object;
.source "ZendeskHelpCenterService.java"

# interfaces
.implements Lcom/zendesk/service/RetrofitZendeskCallbackAdapter$RequestExtractor;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;->getCategories(Ljava/lang/String;Ljava/util/Locale;Lcom/zendesk/service/ZendeskCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/zendesk/service/RetrofitZendeskCallbackAdapter$RequestExtractor",
        "<",
        "Lcom/zendesk/sdk/model/helpcenter/CategoriesResponse;",
        "Ljava/util/List",
        "<",
        "Lcom/zendesk/sdk/model/helpcenter/Category;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;

    .prologue
    .line 77
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService$1;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic extract(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 77
    check-cast p1, Lcom/zendesk/sdk/model/helpcenter/CategoriesResponse;

    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService$1;->extract(Lcom/zendesk/sdk/model/helpcenter/CategoriesResponse;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public extract(Lcom/zendesk/sdk/model/helpcenter/CategoriesResponse;)Ljava/util/List;
    .locals 1
    .param p1, "data"    # Lcom/zendesk/sdk/model/helpcenter/CategoriesResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/zendesk/sdk/model/helpcenter/CategoriesResponse;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/Category;",
            ">;"
        }
    .end annotation

    .prologue
    .line 80
    invoke-virtual {p1}, Lcom/zendesk/sdk/model/helpcenter/CategoriesResponse;->getCategories()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

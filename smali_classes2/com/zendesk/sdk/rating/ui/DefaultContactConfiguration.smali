.class Lcom/zendesk/sdk/rating/ui/DefaultContactConfiguration;
.super Lcom/zendesk/sdk/feedback/BaseZendeskFeedbackConfiguration;
.source "RateMyAppDialog.java"


# instance fields
.field private mSubject:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 604
    invoke-direct {p0}, Lcom/zendesk/sdk/feedback/BaseZendeskFeedbackConfiguration;-><init>()V

    .line 605
    if-nez p1, :cond_0

    const-string v0, ""

    :goto_0
    iput-object v0, p0, Lcom/zendesk/sdk/rating/ui/DefaultContactConfiguration;->mSubject:Ljava/lang/String;

    .line 606
    return-void

    .line 605
    :cond_0
    sget v0, Lcom/zendesk/sdk/R$string;->rate_my_app_dialog_feedback_request_subject:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public getRequestSubject()Ljava/lang/String;
    .locals 1

    .prologue
    .line 610
    iget-object v0, p0, Lcom/zendesk/sdk/rating/ui/DefaultContactConfiguration;->mSubject:Ljava/lang/String;

    return-object v0
.end method

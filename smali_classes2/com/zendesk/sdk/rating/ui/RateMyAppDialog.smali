.class public Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;
.super Lcom/zendesk/sdk/ui/ZendeskDialog;
.source "RateMyAppDialog.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$Builder;,
        Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$OnShowListener;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String;

.field public static final RMA_DIALOG_TAG:Ljava/lang/String; = "rma_dialog"


# instance fields
.field private mHandler:Landroid/os/Handler;

.field private mOnShowListener:Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$OnShowListener;

.field private mRateMyAppButtonContainer:Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;

.field private mRateMyAppButtonList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/rating/RateMyAppButton;",
            ">;"
        }
    .end annotation
.end field

.field private mRateMyAppRules:Lcom/zendesk/sdk/rating/impl/RateMyAppRules;

.field private mSelectionListener:Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer$RateMyAppSelectionListener;

.field private mShowAlways:Z

.field private mShowPending:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 78
    const-class v0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/zendesk/sdk/ui/ZendeskDialog;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;Landroid/support/v4/app/FragmentActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;
    .param p1, "x1"    # Landroid/support/v4/app/FragmentActivity;

    .prologue
    .line 74
    invoke-direct {p0, p1}, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;->showInternal(Landroid/support/v4/app/FragmentActivity;)V

    return-void
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    sget-object v0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;Landroid/support/v4/app/FragmentActivity;ZLcom/zendesk/sdk/model/settings/SafeMobileSettings;)V
    .locals 0
    .param p0, "x0"    # Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;
    .param p1, "x1"    # Landroid/support/v4/app/FragmentActivity;
    .param p2, "x2"    # Z
    .param p3, "x3"    # Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    .prologue
    .line 74
    invoke-direct {p0, p1, p2, p3}, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;->show(Landroid/support/v4/app/FragmentActivity;ZLcom/zendesk/sdk/model/settings/SafeMobileSettings;)V

    return-void
.end method

.method static synthetic access$302(Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;
    .param p1, "x1"    # Z

    .prologue
    .line 74
    iput-boolean p1, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;->mShowPending:Z

    return p1
.end method

.method private show(Landroid/support/v4/app/FragmentActivity;ZLcom/zendesk/sdk/model/settings/SafeMobileSettings;)V
    .locals 7
    .param p1, "fragmentActivity"    # Landroid/support/v4/app/FragmentActivity;
    .param p2, "overrideShowDelay"    # Z
    .param p3, "mobileSettings"    # Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    .prologue
    const/4 v6, 0x0

    .line 203
    sget-object v1, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;->LOG_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "show called, show immediate override is: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v1, v4, v5}, Lcom/zendesk/logger/Logger;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 205
    if-nez p1, :cond_0

    .line 206
    sget-object v1, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;->LOG_TAG:Ljava/lang/String;

    const-string v4, "Cannot show dialog, activity is null"

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v1, v4, v5}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 233
    :goto_0
    return-void

    .line 210
    :cond_0
    invoke-virtual {p3}, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->isRateMyAppEnabled()Z

    move-result v1

    if-nez v1, :cond_1

    .line 211
    sget-object v1, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;->LOG_TAG:Ljava/lang/String;

    const-string v4, "Cannot show dialog, no rate my app settings have been retrieved from the server or rate my app is not enabled."

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v1, v4, v5}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 215
    :cond_1
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 218
    .local v0, "activityReference":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Landroid/support/v4/app/FragmentActivity;>;"
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-direct {v1, v4}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;->mHandler:Landroid/os/Handler;

    .line 220
    if-eqz p2, :cond_2

    const-wide/16 v2, 0x0

    .line 222
    .local v2, "showDelay":J
    :goto_1
    iget-object v1, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;->mHandler:Landroid/os/Handler;

    new-instance v4, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$4;

    invoke-direct {v4, p0, v0}, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$4;-><init>(Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;Ljava/lang/ref/WeakReference;)V

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 220
    .end local v2    # "showDelay":J
    :cond_2
    invoke-virtual {p3}, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->getRateMyAppDelay()J

    move-result-wide v2

    goto :goto_1
.end method

.method private showInternal(Landroid/support/v4/app/FragmentActivity;)V
    .locals 8
    .param p1, "fragmentActivity"    # Landroid/support/v4/app/FragmentActivity;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 256
    iget-boolean v7, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;->mShowPending:Z

    if-eqz v7, :cond_1

    .line 257
    sget-object v6, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;->LOG_TAG:Ljava/lang/String;

    const-string v7, "Can\'t show the dialog because another call to show is pending"

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v6, v7, v5}, Lcom/zendesk/logger/Logger;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 300
    :cond_0
    :goto_0
    return-void

    .line 261
    :cond_1
    new-instance v7, Lcom/zendesk/sdk/storage/RateMyAppStorage;

    invoke-direct {v7, p1}, Lcom/zendesk/sdk/storage/RateMyAppStorage;-><init>(Landroid/content/Context;)V

    invoke-virtual {v7}, Lcom/zendesk/sdk/storage/RateMyAppStorage;->incrementNumberOfLaunches()V

    .line 263
    iget-boolean v7, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;->mShowAlways:Z

    if-nez v7, :cond_2

    iget-object v7, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;->mRateMyAppRules:Lcom/zendesk/sdk/rating/impl/RateMyAppRules;

    invoke-virtual {v7}, Lcom/zendesk/sdk/rating/impl/RateMyAppRules;->checkRules()Z

    move-result v7

    if-eqz v7, :cond_3

    :cond_2
    move v0, v6

    .line 265
    .local v0, "canShowDialog":Z
    :goto_1
    if-nez v0, :cond_4

    .line 266
    sget-object v6, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;->LOG_TAG:Ljava/lang/String;

    const-string v7, "Can\'t show the dialog due to configuration"

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v6, v7, v5}, Lcom/zendesk/logger/Logger;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .end local v0    # "canShowDialog":Z
    :cond_3
    move v0, v5

    .line 263
    goto :goto_1

    .line 270
    .restart local v0    # "canShowDialog":Z
    :cond_4
    iput-boolean v6, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;->mShowPending:Z

    .line 271
    iput-boolean v5, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;->mShowAlways:Z

    .line 273
    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    .line 274
    .local v2, "fragmentManager":Landroid/support/v4/app/FragmentManager;
    const-string v6, "rma_dialog"

    invoke-virtual {v2, v6}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v4

    check-cast v4, Landroid/support/v4/app/DialogFragment;

    .line 275
    .local v4, "prev":Landroid/support/v4/app/DialogFragment;
    invoke-virtual {v2}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v3

    .line 277
    .local v3, "ft":Landroid/support/v4/app/FragmentTransaction;
    if-eqz v4, :cond_6

    .line 278
    invoke-virtual {v4}, Landroid/support/v4/app/DialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v6

    if-eqz v6, :cond_5

    .line 279
    invoke-virtual {v4}, Landroid/support/v4/app/DialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/Dialog;->dismiss()V

    .line 281
    :cond_5
    invoke-virtual {v3, v4}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 290
    :cond_6
    :try_start_0
    const-string v6, "rma_dialog"

    invoke-virtual {p0, v3, v6}, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;->show(Landroid/support/v4/app/FragmentTransaction;Ljava/lang/String;)I

    .line 291
    invoke-virtual {p0}, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v6

    invoke-virtual {v6}, Landroid/support/v4/app/FragmentManager;->executePendingTransactions()Z

    .line 293
    iget-object v6, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;->mOnShowListener:Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$OnShowListener;

    if-eqz v6, :cond_0

    .line 294
    iget-object v6, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;->mOnShowListener:Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$OnShowListener;

    invoke-virtual {p0}, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v7

    invoke-interface {v6, v7}, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$OnShowListener;->onShow(Landroid/content/DialogInterface;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 297
    :catch_0
    move-exception v1

    .line 298
    .local v1, "e":Ljava/lang/Exception;
    sget-object v6, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;->LOG_TAG:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v6, v7, v1, v5}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;[Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 306
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;->mShowPending:Z

    .line 307
    invoke-super {p0, p1}, Lcom/zendesk/sdk/ui/ZendeskDialog;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 107
    iget-object v1, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;->mRateMyAppButtonList:Ljava/util/List;

    if-eqz v1, :cond_0

    .line 108
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;->mRateMyAppButtonList:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 114
    .local v0, "buttons":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/zendesk/sdk/rating/RateMyAppButton;>;"
    new-instance v1, Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;

    .line 115
    invoke-virtual {p0}, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v1, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;->mRateMyAppButtonContainer:Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;

    .line 117
    iget-object v1, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;->mRateMyAppButtonContainer:Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;

    new-instance v2, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$1;

    invoke-direct {v2, p0}, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$1;-><init>(Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;)V

    invoke-virtual {v1, v2}, Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;->setDismissableListener(Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer$DismissableListener;)V

    .line 124
    iget-object v1, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;->mRateMyAppButtonContainer:Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;

    iget-object v2, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;->mSelectionListener:Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer$RateMyAppSelectionListener;

    invoke-virtual {v1, v2}, Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;->setRateMyAppSelectionListener(Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer$RateMyAppSelectionListener;)V

    .line 125
    iget-object v1, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;->mRateMyAppButtonContainer:Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;

    sget v2, Lcom/zendesk/sdk/R$id;->rma_dialog_root:I

    invoke-virtual {v1, v2}, Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;->setId(I)V

    .line 127
    iget-object v1, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;->mRateMyAppButtonContainer:Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;

    return-object v1

    .line 110
    .end local v0    # "buttons":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/zendesk/sdk/rating/RateMyAppButton;>;"
    :cond_0
    sget-object v1, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;->LOG_TAG:Ljava/lang/String;

    const-string v2, "buttons extra was not an instance of ArrayList<RateMyAppButton>"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 111
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "buttons extra was not an instance of ArrayList<RateMyAppButton>"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method setRateMyAppButtonList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/rating/RateMyAppButton;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 247
    .local p1, "rateMyAppButtonList":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/rating/RateMyAppButton;>;"
    iput-object p1, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;->mRateMyAppButtonList:Ljava/util/List;

    .line 248
    return-void
.end method

.method setRateMyAppOnSelectionListener(Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer$RateMyAppSelectionListener;)V
    .locals 0
    .param p1, "mSelectionListener"    # Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer$RateMyAppSelectionListener;

    .prologue
    .line 243
    iput-object p1, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;->mSelectionListener:Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer$RateMyAppSelectionListener;

    .line 244
    return-void
.end method

.method setRateMyAppOnShowListener(Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$OnShowListener;)V
    .locals 2
    .param p1, "onShowListener"    # Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$OnShowListener;

    .prologue
    .line 236
    iput-object p1, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;->mOnShowListener:Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$OnShowListener;

    .line 237
    iget-object v0, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;->mRateMyAppButtonContainer:Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;

    if-eqz v0, :cond_0

    .line 238
    iget-object v1, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;->mRateMyAppButtonContainer:Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;

    iget-object v0, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;->mOnShowListener:Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$OnShowListener;

    check-cast v0, Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer$RateMyAppSelectionListener;

    invoke-virtual {v1, v0}, Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;->setRateMyAppSelectionListener(Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer$RateMyAppSelectionListener;)V

    .line 240
    :cond_0
    return-void
.end method

.method setRateMyAppRules(Lcom/zendesk/sdk/rating/impl/RateMyAppRules;)V
    .locals 0
    .param p1, "rateMyAppRules"    # Lcom/zendesk/sdk/rating/impl/RateMyAppRules;

    .prologue
    .line 251
    iput-object p1, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;->mRateMyAppRules:Lcom/zendesk/sdk/rating/impl/RateMyAppRules;

    .line 252
    return-void
.end method

.method public show(Landroid/support/v4/app/FragmentActivity;)V
    .locals 1
    .param p1, "fragmentActivity"    # Landroid/support/v4/app/FragmentActivity;

    .prologue
    .line 166
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;->show(Landroid/support/v4/app/FragmentActivity;Z)V

    .line 167
    return-void
.end method

.method public show(Landroid/support/v4/app/FragmentActivity;Z)V
    .locals 3
    .param p1, "fragmentActivity"    # Landroid/support/v4/app/FragmentActivity;
    .param p2, "overrideShowDelay"    # Z

    .prologue
    .line 180
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 181
    .local v0, "fragmentActivityRef":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Landroid/support/v4/app/FragmentActivity;>;"
    sget-object v2, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    invoke-virtual {v2}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->provider()Lcom/zendesk/sdk/network/impl/ProviderStore;

    move-result-object v2

    invoke-interface {v2}, Lcom/zendesk/sdk/network/impl/ProviderStore;->uiSettingsHelper()Lcom/zendesk/sdk/network/SettingsHelper;

    move-result-object v1

    .line 183
    .local v1, "settingsHelper":Lcom/zendesk/sdk/network/SettingsHelper;
    new-instance v2, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$3;

    invoke-direct {v2, p0, v0, p2}, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$3;-><init>(Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;Ljava/lang/ref/WeakReference;Z)V

    invoke-interface {v1, v2}, Lcom/zendesk/sdk/network/SettingsHelper;->loadSetting(Lcom/zendesk/service/ZendeskCallback;)V

    .line 199
    return-void
.end method

.method public showAlways(Landroid/support/v4/app/FragmentActivity;)V
    .locals 3
    .param p1, "fragmentActivity"    # Landroid/support/v4/app/FragmentActivity;

    .prologue
    .line 138
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;->mShowAlways:Z

    .line 139
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 141
    .local v0, "fragmentActivityRef":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Landroid/support/v4/app/FragmentActivity;>;"
    sget-object v2, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    invoke-virtual {v2}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->provider()Lcom/zendesk/sdk/network/impl/ProviderStore;

    move-result-object v2

    invoke-interface {v2}, Lcom/zendesk/sdk/network/impl/ProviderStore;->uiSettingsHelper()Lcom/zendesk/sdk/network/SettingsHelper;

    move-result-object v1

    .line 142
    .local v1, "settingsHelper":Lcom/zendesk/sdk/network/SettingsHelper;
    new-instance v2, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$2;

    invoke-direct {v2, p0, v0}, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$2;-><init>(Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;Ljava/lang/ref/WeakReference;)V

    invoke-interface {v1, v2}, Lcom/zendesk/sdk/network/SettingsHelper;->loadSetting(Lcom/zendesk/service/ZendeskCallback;)V

    .line 158
    return-void
.end method

.method public tearDownDialog(Landroid/support/v4/app/FragmentManager;)V
    .locals 7
    .param p1, "fragmentManager"    # Landroid/support/v4/app/FragmentManager;

    .prologue
    const/4 v6, 0x0

    .line 319
    if-nez p1, :cond_1

    .line 320
    sget-object v3, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;->LOG_TAG:Ljava/lang/String;

    const-string v4, "Supplied FragmentManager was null, cannot continue..."

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v3, v4, v5}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 351
    :cond_0
    :goto_0
    return-void

    .line 324
    :cond_1
    invoke-virtual {p1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 325
    .local v1, "fragmentTransaction":Landroid/support/v4/app/FragmentTransaction;
    const-string v3, "feedback"

    invoke-virtual {p1, v3}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 327
    .local v0, "feedbackDialogFragment":Landroid/support/v4/app/Fragment;
    if-eqz v0, :cond_2

    .line 328
    sget-object v3, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;->LOG_TAG:Ljava/lang/String;

    const-string v4, "feedback dialog found for removal"

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v3, v4, v5}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 329
    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 332
    :cond_2
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 334
    invoke-virtual {p1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 335
    const-string v3, "rma_dialog"

    invoke-virtual {p1, v3}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    .line 337
    .local v2, "rmaDialog":Landroid/support/v4/app/Fragment;
    if-eqz v2, :cond_3

    .line 338
    sget-object v3, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;->LOG_TAG:Ljava/lang/String;

    const-string v4, "RateMyApp dialog found for removal"

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v3, v4, v5}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 339
    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 342
    :cond_3
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 343
    invoke-virtual {p1}, Landroid/support/v4/app/FragmentManager;->popBackStackImmediate()Z

    .line 345
    invoke-virtual {p1}, Landroid/support/v4/app/FragmentManager;->executePendingTransactions()Z

    .line 348
    iget-object v3, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;->mHandler:Landroid/os/Handler;

    if-eqz v3, :cond_0

    .line 349
    iget-object v3, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;->mHandler:Landroid/os/Handler;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    goto :goto_0
.end method

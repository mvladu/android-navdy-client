.class public Lcom/zendesk/sdk/rating/impl/RateMyAppStoreButton;
.super Lcom/zendesk/sdk/rating/impl/BaseRateMyAppButton;
.source "RateMyAppStoreButton.java"


# static fields
.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private final mButtonLabel:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/zendesk/sdk/rating/impl/RateMyAppStoreButton;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/zendesk/sdk/rating/impl/RateMyAppStoreButton;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/zendesk/sdk/rating/impl/BaseRateMyAppButton;-><init>()V

    .line 34
    sget v0, Lcom/zendesk/sdk/R$string;->rate_my_app_dialog_positive_action_label:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/rating/impl/RateMyAppStoreButton;->mButtonLabel:Ljava/lang/String;

    .line 35
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/zendesk/sdk/rating/impl/RateMyAppStoreButton;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public getId()I
    .locals 1

    .prologue
    .line 76
    sget v0, Lcom/zendesk/sdk/R$id;->rma_store_button:I

    return v0
.end method

.method public getLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/zendesk/sdk/rating/impl/RateMyAppStoreButton;->mButtonLabel:Ljava/lang/String;

    return-object v0
.end method

.method public getOnClickListener()Landroid/view/View$OnClickListener;
    .locals 4

    .prologue
    .line 45
    sget-object v1, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    invoke-virtual {v1}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->getMobileSettings()Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    move-result-object v0

    .line 47
    .local v0, "safeMobileSettings":Lcom/zendesk/sdk/model/settings/SafeMobileSettings;
    invoke-virtual {v0}, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->getRateMyAppStoreUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/zendesk/util/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 48
    sget-object v1, Lcom/zendesk/sdk/rating/impl/RateMyAppStoreButton;->LOG_TAG:Ljava/lang/String;

    const-string v2, "Rate my app settings and / or store url is null, disabling button"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 49
    const/4 v1, 0x0

    .line 52
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lcom/zendesk/sdk/rating/impl/RateMyAppStoreButton$1;

    invoke-direct {v1, p0, v0}, Lcom/zendesk/sdk/rating/impl/RateMyAppStoreButton$1;-><init>(Lcom/zendesk/sdk/rating/impl/RateMyAppStoreButton;Lcom/zendesk/sdk/model/settings/SafeMobileSettings;)V

    goto :goto_0
.end method

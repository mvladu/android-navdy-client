.class public Lcom/zendesk/sdk/attachment/ImageUploadHelper;
.super Ljava/lang/Object;
.source "ImageUploadHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/zendesk/sdk/attachment/ImageUploadHelper$ImageUploadProgressListener;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private mAddedForUpload:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private mDeleteAllUploadsCalled:Z

.field private progressListener:Lcom/zendesk/sdk/attachment/ImageUploadHelper$ImageUploadProgressListener;

.field private uploadCounter:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final uploadProvider:Lcom/zendesk/sdk/network/UploadProvider;

.field private uploadedAttachments:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/io/File;",
            "Lcom/zendesk/sdk/model/request/UploadResponse;",
            ">;"
        }
    .end annotation
.end field

.field private uploadedCounter:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/zendesk/sdk/attachment/ImageUploadHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/zendesk/sdk/attachment/ImageUploadHelper$ImageUploadProgressListener;Lcom/zendesk/sdk/network/UploadProvider;)V
    .locals 2
    .param p1, "imageUploadProgressListener"    # Lcom/zendesk/sdk/attachment/ImageUploadHelper$ImageUploadProgressListener;
    .param p2, "uploadProvider"    # Lcom/zendesk/sdk/network/UploadProvider;

    .prologue
    const/4 v1, 0x0

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->uploadCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 33
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->uploadedCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 38
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->uploadedAttachments:Ljava/util/Map;

    .line 43
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->mAddedForUpload:Ljava/util/List;

    .line 45
    iput-boolean v1, p0, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->mDeleteAllUploadsCalled:Z

    .line 50
    iput-object p1, p0, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->progressListener:Lcom/zendesk/sdk/attachment/ImageUploadHelper$ImageUploadProgressListener;

    .line 51
    iput-object p2, p0, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->uploadProvider:Lcom/zendesk/sdk/network/UploadProvider;

    .line 52
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/zendesk/sdk/attachment/ImageUploadHelper;)Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/attachment/ImageUploadHelper;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->uploadedCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object v0
.end method

.method static synthetic access$200(Lcom/zendesk/sdk/attachment/ImageUploadHelper;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/attachment/ImageUploadHelper;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->uploadedAttachments:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$300(Lcom/zendesk/sdk/attachment/ImageUploadHelper;)Lcom/zendesk/sdk/attachment/ImageUploadHelper$ImageUploadProgressListener;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/attachment/ImageUploadHelper;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->progressListener:Lcom/zendesk/sdk/attachment/ImageUploadHelper$ImageUploadProgressListener;

    return-object v0
.end method

.method static synthetic access$400(Lcom/zendesk/sdk/attachment/ImageUploadHelper;)Z
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/attachment/ImageUploadHelper;

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->mDeleteAllUploadsCalled:Z

    return v0
.end method

.method static synthetic access$500(Lcom/zendesk/sdk/attachment/ImageUploadHelper;)Lcom/zendesk/sdk/network/UploadProvider;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/attachment/ImageUploadHelper;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->uploadProvider:Lcom/zendesk/sdk/network/UploadProvider;

    return-object v0
.end method

.method static synthetic access$600(Lcom/zendesk/sdk/attachment/ImageUploadHelper;)V
    .locals 0
    .param p0, "x0"    # Lcom/zendesk/sdk/attachment/ImageUploadHelper;

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->checkAndNotifyState()V

    return-void
.end method

.method static synthetic access$700(Lcom/zendesk/sdk/attachment/ImageUploadHelper;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/attachment/ImageUploadHelper;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->mAddedForUpload:Ljava/util/List;

    return-object v0
.end method

.method private declared-synchronized checkAndNotifyState()V
    .locals 2

    .prologue
    .line 211
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->isImageUploadCompleted()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->progressListener:Lcom/zendesk/sdk/attachment/ImageUploadHelper$ImageUploadProgressListener;

    if-eqz v0, :cond_0

    .line 212
    iget-object v0, p0, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->progressListener:Lcom/zendesk/sdk/attachment/ImageUploadHelper$ImageUploadProgressListener;

    iget-object v1, p0, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->uploadedAttachments:Ljava/util/Map;

    invoke-interface {v0, v1}, Lcom/zendesk/sdk/attachment/ImageUploadHelper$ImageUploadProgressListener;->allImagesUploaded(Ljava/util/Map;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 214
    :cond_0
    monitor-exit p0

    return-void

    .line 211
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private newZendeskCallback(Lcom/zendesk/belvedere/BelvedereResult;)Lcom/zendesk/service/ZendeskCallback;
    .locals 1
    .param p1, "file"    # Lcom/zendesk/belvedere/BelvedereResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/zendesk/belvedere/BelvedereResult;",
            ")",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/request/UploadResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 217
    new-instance v0, Lcom/zendesk/sdk/attachment/ImageUploadHelper$1;

    invoke-direct {v0, p0, p1}, Lcom/zendesk/sdk/attachment/ImageUploadHelper$1;-><init>(Lcom/zendesk/sdk/attachment/ImageUploadHelper;Lcom/zendesk/belvedere/BelvedereResult;)V

    return-object v0
.end method


# virtual methods
.method public deleteAllAttachmentsBeforeShutdown()V
    .locals 5

    .prologue
    .line 197
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->mDeleteAllUploadsCalled:Z

    .line 198
    invoke-virtual {p0}, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->getUploadTokens()Ljava/util/List;

    move-result-object v1

    .line 200
    .local v1, "uploadTokens":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {v1}, Lcom/zendesk/util/CollectionUtils;->isNotEmpty(Ljava/util/Collection;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 201
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 202
    .local v0, "uploadToken":Ljava/lang/String;
    iget-object v3, p0, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->uploadProvider:Lcom/zendesk/sdk/network/UploadProvider;

    const/4 v4, 0x0

    invoke-interface {v3, v0, v4}, Lcom/zendesk/sdk/network/UploadProvider;->deleteAttachment(Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V

    goto :goto_0

    .line 205
    .end local v0    # "uploadToken":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public getRecentState()Ljava/util/HashMap;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;",
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 106
    new-instance v2, Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->uploadedAttachments:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 107
    .local v2, "uploaded":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 109
    .local v3, "uploading":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
    iget-object v4, p0, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->mAddedForUpload:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/File;

    .line 110
    .local v1, "f":Ljava/io/File;
    invoke-interface {v2, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 111
    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 115
    .end local v1    # "f":Ljava/io/File;
    :cond_1
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 116
    .local v0, "attachmentTypeListHashMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;Ljava/util/List<Ljava/io/File;>;>;"
    sget-object v4, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;->UPLOADED:Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;

    invoke-virtual {v0, v4, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    sget-object v4, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;->UPLOADING:Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;

    invoke-virtual {v0, v4, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    return-object v0
.end method

.method public getUploadTokens()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 83
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 84
    .local v0, "tokens":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->uploadedAttachments:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/zendesk/sdk/model/request/UploadResponse;

    .line 85
    .local v1, "uploadResponse":Lcom/zendesk/sdk/model/request/UploadResponse;
    invoke-virtual {v1}, Lcom/zendesk/sdk/model/request/UploadResponse;->getToken()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 87
    .end local v1    # "uploadResponse":Lcom/zendesk/sdk/model/request/UploadResponse;
    :cond_0
    return-object v0
.end method

.method public getUploadedAttachments()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/request/Attachment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 97
    iget-object v3, p0, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->uploadedAttachments:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    .line 98
    .local v2, "values":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/zendesk/sdk/model/request/UploadResponse;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 99
    .local v0, "uploadedAttachments":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/model/request/Attachment;>;"
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/zendesk/sdk/model/request/UploadResponse;

    .line 100
    .local v1, "value":Lcom/zendesk/sdk/model/request/UploadResponse;
    invoke-virtual {v1}, Lcom/zendesk/sdk/model/request/UploadResponse;->getAttachment()Lcom/zendesk/sdk/model/request/Attachment;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 102
    .end local v1    # "value":Lcom/zendesk/sdk/model/request/UploadResponse;
    :cond_0
    return-object v0
.end method

.method public isImageUploadCompleted()Z
    .locals 2

    .prologue
    .line 72
    iget-object v0, p0, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->uploadCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    iget-object v1, p0, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->uploadedCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public removeDuplicateFilesFromList(Ljava/util/List;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/belvedere/BelvedereResult;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/belvedere/BelvedereResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 128
    .local p1, "files":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/belvedere/BelvedereResult;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 130
    .local v3, "uniqueFiles":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/belvedere/BelvedereResult;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/zendesk/belvedere/BelvedereResult;

    .line 131
    .local v1, "file":Lcom/zendesk/belvedere/BelvedereResult;
    if-eqz v1, :cond_0

    .line 132
    const/4 v0, 0x0

    .line 133
    .local v0, "contains":Z
    iget-object v5, p0, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->mAddedForUpload:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/io/File;

    .line 134
    .local v2, "fn":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1}, Lcom/zendesk/belvedere/BelvedereResult;->getFile()Ljava/io/File;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 135
    const/4 v0, 0x1

    goto :goto_1

    .line 138
    .end local v2    # "fn":Ljava/io/File;
    :cond_2
    if-nez v0, :cond_0

    .line 139
    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 143
    .end local v0    # "contains":Z
    .end local v1    # "file":Lcom/zendesk/belvedere/BelvedereResult;
    :cond_3
    return-object v3
.end method

.method public removeImage(Ljava/io/File;)V
    .locals 4
    .param p1, "file"    # Ljava/io/File;

    .prologue
    .line 174
    iget-object v1, p0, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->uploadedAttachments:Ljava/util/Map;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->uploadedAttachments:Ljava/util/Map;

    .line 175
    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->uploadedAttachments:Ljava/util/Map;

    .line 176
    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/zendesk/sdk/model/request/UploadResponse;

    invoke-virtual {v1}, Lcom/zendesk/sdk/model/request/UploadResponse;->getToken()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/zendesk/util/StringUtils;->hasLength(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    .line 178
    .local v0, "fileHasToken":Z
    :goto_0
    if-eqz v0, :cond_0

    .line 180
    iget-object v2, p0, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->uploadProvider:Lcom/zendesk/sdk/network/UploadProvider;

    iget-object v1, p0, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->uploadedAttachments:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/zendesk/sdk/model/request/UploadResponse;

    invoke-virtual {v1}, Lcom/zendesk/sdk/model/request/UploadResponse;->getToken()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x0

    invoke-interface {v2, v1, v3}, Lcom/zendesk/sdk/network/UploadProvider;->deleteAttachment(Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V

    .line 183
    :cond_0
    iget-object v1, p0, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->mAddedForUpload:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 184
    iget-object v1, p0, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->uploadedAttachments:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 185
    return-void

    .line 176
    .end local v0    # "fileHasToken":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public reset()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 150
    iget-object v0, p0, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->uploadCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    iget-object v1, p0, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->uploadedCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 151
    iget-object v0, p0, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->uploadCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 152
    iget-object v0, p0, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->uploadedCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 153
    iget-object v0, p0, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->uploadedAttachments:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 154
    iget-object v0, p0, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->mAddedForUpload:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 156
    :cond_0
    return-void
.end method

.method public setImageUploadProgressListener(Lcom/zendesk/sdk/attachment/ImageUploadHelper$ImageUploadProgressListener;)V
    .locals 0
    .param p1, "imageUploadProgressListener"    # Lcom/zendesk/sdk/attachment/ImageUploadHelper$ImageUploadProgressListener;

    .prologue
    .line 164
    iput-object p1, p0, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->progressListener:Lcom/zendesk/sdk/attachment/ImageUploadHelper$ImageUploadProgressListener;

    .line 165
    return-void
.end method

.method public uploadImage(Lcom/zendesk/belvedere/BelvedereResult;Ljava/lang/String;)V
    .locals 4
    .param p1, "file"    # Lcom/zendesk/belvedere/BelvedereResult;
    .param p2, "mimeType"    # Ljava/lang/String;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->mAddedForUpload:Ljava/util/List;

    invoke-virtual {p1}, Lcom/zendesk/belvedere/BelvedereResult;->getFile()Ljava/io/File;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 62
    iget-object v0, p0, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->uploadCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 63
    iget-object v0, p0, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->uploadProvider:Lcom/zendesk/sdk/network/UploadProvider;

    invoke-virtual {p1}, Lcom/zendesk/belvedere/BelvedereResult;->getFile()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/zendesk/belvedere/BelvedereResult;->getFile()Ljava/io/File;

    move-result-object v2

    invoke-direct {p0, p1}, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->newZendeskCallback(Lcom/zendesk/belvedere/BelvedereResult;)Lcom/zendesk/service/ZendeskCallback;

    move-result-object v3

    invoke-interface {v0, v1, v2, p2, v3}, Lcom/zendesk/sdk/network/UploadProvider;->uploadAttachment(Ljava/lang/String;Ljava/io/File;Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V

    .line 64
    return-void
.end method

.class public Lcom/zendesk/sdk/model/settings/AttachmentSettings;
.super Ljava/lang/Object;
.source "AttachmentSettings.java"


# instance fields
.field private enabled:Z

.field private maxAttachmentSize:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getMaxAttachmentSize()J
    .locals 2

    .prologue
    .line 29
    iget-wide v0, p0, Lcom/zendesk/sdk/model/settings/AttachmentSettings;->maxAttachmentSize:J

    return-wide v0
.end method

.method public isEnabled()Z
    .locals 1

    .prologue
    .line 20
    iget-boolean v0, p0, Lcom/zendesk/sdk/model/settings/AttachmentSettings;->enabled:Z

    return v0
.end method

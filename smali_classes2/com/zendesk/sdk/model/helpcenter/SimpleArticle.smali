.class public Lcom/zendesk/sdk/model/helpcenter/SimpleArticle;
.super Ljava/lang/Object;
.source "SimpleArticle.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private id:Ljava/lang/Long;

.field private title:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/Long;Ljava/lang/String;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/Long;
    .param p2, "title"    # Ljava/lang/String;

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/zendesk/sdk/model/helpcenter/SimpleArticle;->id:Ljava/lang/Long;

    .line 28
    iput-object p2, p0, Lcom/zendesk/sdk/model/helpcenter/SimpleArticle;->title:Ljava/lang/String;

    .line 29
    return-void
.end method


# virtual methods
.method public getId()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/SimpleArticle;->id:Ljava/lang/Long;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/SimpleArticle;->title:Ljava/lang/String;

    return-object v0
.end method

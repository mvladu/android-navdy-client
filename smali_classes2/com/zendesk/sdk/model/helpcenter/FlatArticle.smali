.class public Lcom/zendesk/sdk/model/helpcenter/FlatArticle;
.super Ljava/lang/Object;
.source "FlatArticle.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/zendesk/sdk/model/helpcenter/FlatArticle;",
        ">;"
    }
.end annotation


# instance fields
.field private article:Lcom/zendesk/sdk/model/helpcenter/Article;

.field private category:Lcom/zendesk/sdk/model/helpcenter/Category;

.field private section:Lcom/zendesk/sdk/model/helpcenter/Section;


# direct methods
.method public constructor <init>(Lcom/zendesk/sdk/model/helpcenter/Category;Lcom/zendesk/sdk/model/helpcenter/Section;Lcom/zendesk/sdk/model/helpcenter/Article;)V
    .locals 0
    .param p1, "category"    # Lcom/zendesk/sdk/model/helpcenter/Category;
    .param p2, "section"    # Lcom/zendesk/sdk/model/helpcenter/Section;
    .param p3, "article"    # Lcom/zendesk/sdk/model/helpcenter/Article;

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    if-nez p1, :cond_0

    new-instance p1, Lcom/zendesk/sdk/model/helpcenter/Category;

    .end local p1    # "category":Lcom/zendesk/sdk/model/helpcenter/Category;
    invoke-direct {p1}, Lcom/zendesk/sdk/model/helpcenter/Category;-><init>()V

    :cond_0
    iput-object p1, p0, Lcom/zendesk/sdk/model/helpcenter/FlatArticle;->category:Lcom/zendesk/sdk/model/helpcenter/Category;

    .line 30
    if-nez p2, :cond_1

    new-instance p2, Lcom/zendesk/sdk/model/helpcenter/Section;

    .end local p2    # "section":Lcom/zendesk/sdk/model/helpcenter/Section;
    invoke-direct {p2}, Lcom/zendesk/sdk/model/helpcenter/Section;-><init>()V

    :cond_1
    iput-object p2, p0, Lcom/zendesk/sdk/model/helpcenter/FlatArticle;->section:Lcom/zendesk/sdk/model/helpcenter/Section;

    .line 31
    if-nez p3, :cond_2

    new-instance p3, Lcom/zendesk/sdk/model/helpcenter/Article;

    .end local p3    # "article":Lcom/zendesk/sdk/model/helpcenter/Article;
    invoke-direct {p3}, Lcom/zendesk/sdk/model/helpcenter/Article;-><init>()V

    :cond_2
    iput-object p3, p0, Lcom/zendesk/sdk/model/helpcenter/FlatArticle;->article:Lcom/zendesk/sdk/model/helpcenter/Article;

    .line 32
    return-void
.end method


# virtual methods
.method public compareTo(Lcom/zendesk/sdk/model/helpcenter/FlatArticle;)I
    .locals 2
    .param p1, "flatArticle"    # Lcom/zendesk/sdk/model/helpcenter/FlatArticle;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 73
    if-nez p1, :cond_0

    .line 74
    const/4 v0, -0x1

    .line 77
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/zendesk/sdk/model/helpcenter/FlatArticle;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/helpcenter/FlatArticle;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 15
    check-cast p1, Lcom/zendesk/sdk/model/helpcenter/FlatArticle;

    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/model/helpcenter/FlatArticle;->compareTo(Lcom/zendesk/sdk/model/helpcenter/FlatArticle;)I

    move-result v0

    return v0
.end method

.method public getArticle()Lcom/zendesk/sdk/model/helpcenter/Article;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 61
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/FlatArticle;->article:Lcom/zendesk/sdk/model/helpcenter/Article;

    return-object v0
.end method

.method public getCategory()Lcom/zendesk/sdk/model/helpcenter/Category;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 41
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/FlatArticle;->category:Lcom/zendesk/sdk/model/helpcenter/Category;

    return-object v0
.end method

.method public getSection()Lcom/zendesk/sdk/model/helpcenter/Section;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 51
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/FlatArticle;->section:Lcom/zendesk/sdk/model/helpcenter/Section;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 66
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/zendesk/sdk/model/helpcenter/FlatArticle;->category:Lcom/zendesk/sdk/model/helpcenter/Category;

    invoke-virtual {v1}, Lcom/zendesk/sdk/model/helpcenter/Category;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/zendesk/sdk/model/helpcenter/FlatArticle;->section:Lcom/zendesk/sdk/model/helpcenter/Section;

    invoke-virtual {v1}, Lcom/zendesk/sdk/model/helpcenter/Section;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/zendesk/sdk/model/helpcenter/FlatArticle;->article:Lcom/zendesk/sdk/model/helpcenter/Article;

    invoke-virtual {v1}, Lcom/zendesk/sdk/model/helpcenter/Article;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.class public Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;
.super Ljava/lang/Object;
.source "HelpRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private articlesPerSectionLimit:I

.field private categoryIds:Ljava/lang/String;

.field private includes:Ljava/lang/String;

.field private labelNames:[Ljava/lang/String;

.field private sectionIds:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    const/4 v0, 0x5

    iput v0, p0, Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;->articlesPerSectionLimit:I

    return-void
.end method

.method static synthetic access$000(Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;->categoryIds:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;->sectionIds:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;->includes:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;

    .prologue
    .line 88
    iget v0, p0, Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;->articlesPerSectionLimit:I

    return v0
.end method

.method static synthetic access$400(Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;->labelNames:[Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public build()Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest;
    .locals 2

    .prologue
    .line 172
    new-instance v0, Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest;-><init>(Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$1;)V

    return-object v0
.end method

.method public includeCategories()Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;
    .locals 2

    .prologue
    .line 123
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;->includes:Ljava/lang/String;

    invoke-static {v0}, Lcom/zendesk/util/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 124
    const-string v0, "categories"

    iput-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;->includes:Ljava/lang/String;

    .line 128
    :cond_0
    :goto_0
    return-object p0

    .line 125
    :cond_1
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;->includes:Ljava/lang/String;

    const-string v1, "sections"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 126
    const-string v0, "categories,sections"

    iput-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;->includes:Ljava/lang/String;

    goto :goto_0
.end method

.method public includeSections()Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;
    .locals 2

    .prologue
    .line 137
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;->includes:Ljava/lang/String;

    invoke-static {v0}, Lcom/zendesk/util/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 138
    const-string v0, "sections"

    iput-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;->includes:Ljava/lang/String;

    .line 142
    :cond_0
    :goto_0
    return-object p0

    .line 139
    :cond_1
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;->includes:Ljava/lang/String;

    const-string v1, "categories"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 140
    const-string v0, "categories,sections"

    iput-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;->includes:Ljava/lang/String;

    goto :goto_0
.end method

.method public withArticlesPerSectionLimit(I)Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;
    .locals 0
    .param p1, "articlesPerSection"    # I

    .prologue
    .line 152
    iput p1, p0, Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;->articlesPerSectionLimit:I

    .line 153
    return-object p0
.end method

.method public withCategoryIds(Ljava/util/List;)Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;"
        }
    .end annotation

    .prologue
    .line 102
    .local p1, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    invoke-static {p1}, Lcom/zendesk/util/StringUtils;->toCsvStringNumber(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;->categoryIds:Ljava/lang/String;

    .line 103
    return-object p0
.end method

.method public varargs withLabelNames([Ljava/lang/String;)Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;
    .locals 0
    .param p1, "labelNames"    # [Ljava/lang/String;

    .prologue
    .line 162
    iput-object p1, p0, Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;->labelNames:[Ljava/lang/String;

    .line 163
    return-object p0
.end method

.method public withSectionIds(Ljava/util/List;)Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;"
        }
    .end annotation

    .prologue
    .line 113
    .local p1, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    invoke-static {p1}, Lcom/zendesk/util/StringUtils;->toCsvStringNumber(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest$Builder;->sectionIds:Ljava/lang/String;

    .line 114
    return-object p0
.end method

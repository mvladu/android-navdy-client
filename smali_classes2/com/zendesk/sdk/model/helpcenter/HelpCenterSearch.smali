.class public Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;
.super Ljava/lang/Object;
.source "HelpCenterSearch.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch$Builder;
    }
.end annotation


# instance fields
.field private categoryIds:Ljava/lang/String;

.field private include:Ljava/lang/String;

.field private labelNames:Ljava/lang/String;

.field private locale:Ljava/util/Locale;

.field private page:Ljava/lang/Integer;

.field private perPage:Ljava/lang/Integer;

.field private query:Ljava/lang/String;

.field private sectionIds:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    return-void
.end method

.method synthetic constructor <init>(Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch$1;

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;-><init>()V

    return-void
.end method

.method static synthetic access$102(Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 24
    iput-object p1, p0, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;->query:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$202(Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;Ljava/util/Locale;)Ljava/util/Locale;
    .locals 0
    .param p0, "x0"    # Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;
    .param p1, "x1"    # Ljava/util/Locale;

    .prologue
    .line 24
    iput-object p1, p0, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;->locale:Ljava/util/Locale;

    return-object p1
.end method

.method static synthetic access$302(Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 24
    iput-object p1, p0, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;->include:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$402(Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 24
    iput-object p1, p0, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;->labelNames:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$502(Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 24
    iput-object p1, p0, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;->categoryIds:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$602(Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 24
    iput-object p1, p0, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;->sectionIds:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$702(Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 0
    .param p0, "x0"    # Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;
    .param p1, "x1"    # Ljava/lang/Integer;

    .prologue
    .line 24
    iput-object p1, p0, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;->page:Ljava/lang/Integer;

    return-object p1
.end method

.method static synthetic access$802(Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 0
    .param p0, "x0"    # Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;
    .param p1, "x1"    # Ljava/lang/Integer;

    .prologue
    .line 24
    iput-object p1, p0, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;->perPage:Ljava/lang/Integer;

    return-object p1
.end method


# virtual methods
.method public getCategoryIds()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 94
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;->categoryIds:Ljava/lang/String;

    return-object v0
.end method

.method public getInclude()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 74
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;->include:Ljava/lang/String;

    return-object v0
.end method

.method public getLabelNames()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 84
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;->labelNames:Ljava/lang/String;

    return-object v0
.end method

.method public getLocale()Ljava/util/Locale;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 64
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;->locale:Ljava/util/Locale;

    return-object v0
.end method

.method public getPage()Ljava/lang/Integer;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 114
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;->page:Ljava/lang/Integer;

    return-object v0
.end method

.method public getPerPage()Ljava/lang/Integer;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 124
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;->perPage:Ljava/lang/Integer;

    return-object v0
.end method

.method public getQuery()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 53
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;->query:Ljava/lang/String;

    return-object v0
.end method

.method public getSectionIds()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 104
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;->sectionIds:Ljava/lang/String;

    return-object v0
.end method

.method public withQuery(Ljava/lang/String;)Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;
    .locals 4
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 135
    new-instance v2, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;

    invoke-direct {v2}, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;-><init>()V

    .line 138
    .local v2, "search":Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;

    move-object v2, v0

    .line 139
    iput-object p1, v2, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;->query:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 144
    :goto_0
    return-object v2

    .line 140
    :catch_0
    move-exception v1

    .line 141
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    invoke-virtual {v1}, Ljava/lang/CloneNotSupportedException;->printStackTrace()V

    goto :goto_0
.end method

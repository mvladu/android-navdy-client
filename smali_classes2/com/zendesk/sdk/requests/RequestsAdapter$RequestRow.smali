.class Lcom/zendesk/sdk/requests/RequestsAdapter$RequestRow;
.super Landroid/widget/RelativeLayout;
.source "RequestsAdapter.java"

# interfaces
.implements Lcom/zendesk/sdk/ui/ListRowView;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/sdk/requests/RequestsAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "RequestRow"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/RelativeLayout;",
        "Lcom/zendesk/sdk/ui/ListRowView",
        "<",
        "Lcom/zendesk/sdk/model/request/Request;",
        ">;"
    }
.end annotation


# static fields
.field private static final CREATED_AT_DATE_FORMAT:Ljava/lang/String; = "dd MMMM yyy"

.field private static final LOG_TAG:Ljava/lang/String; = "RequestRow"


# instance fields
.field private final context:Landroid/content/Context;

.field private date:Landroid/widget/TextView;

.field private description:Landroid/widget/TextView;

.field private unreadIndicator:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 68
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 69
    iput-object p1, p0, Lcom/zendesk/sdk/requests/RequestsAdapter$RequestRow;->context:Landroid/content/Context;

    .line 70
    invoke-direct {p0}, Lcom/zendesk/sdk/requests/RequestsAdapter$RequestRow;->initialise()V

    .line 71
    return-void
.end method

.method private initialise()V
    .locals 3

    .prologue
    .line 75
    iget-object v1, p0, Lcom/zendesk/sdk/requests/RequestsAdapter$RequestRow;->context:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    sget v2, Lcom/zendesk/sdk/R$layout;->row_request:I

    invoke-virtual {v1, v2, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 77
    .local v0, "container":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 78
    sget v1, Lcom/zendesk/sdk/R$id;->row_request_description:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/zendesk/sdk/requests/RequestsAdapter$RequestRow;->description:Landroid/widget/TextView;

    .line 79
    sget v1, Lcom/zendesk/sdk/R$id;->row_request_date:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/zendesk/sdk/requests/RequestsAdapter$RequestRow;->date:Landroid/widget/TextView;

    .line 80
    sget v1, Lcom/zendesk/sdk/R$id;->row_request_unread_indicator:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/zendesk/sdk/requests/RequestsAdapter$RequestRow;->unreadIndicator:Landroid/widget/ImageView;

    .line 82
    :cond_0
    return-void
.end method


# virtual methods
.method public bind(Lcom/zendesk/sdk/model/request/Request;)V
    .locals 7
    .param p1, "request"    # Lcom/zendesk/sdk/model/request/Request;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 92
    if-nez p1, :cond_1

    .line 93
    const-string v1, "RequestRow"

    const-string v2, "request is null, nothing to bind."

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 122
    :cond_0
    :goto_0
    return-void

    .line 97
    :cond_1
    iget-object v1, p0, Lcom/zendesk/sdk/requests/RequestsAdapter$RequestRow;->description:Landroid/widget/TextView;

    if-eqz v1, :cond_2

    .line 98
    iget-object v1, p0, Lcom/zendesk/sdk/requests/RequestsAdapter$RequestRow;->description:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/request/Request;->getDescription()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 101
    :cond_2
    iget-object v1, p0, Lcom/zendesk/sdk/requests/RequestsAdapter$RequestRow;->date:Landroid/widget/TextView;

    if-eqz v1, :cond_3

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/request/Request;->getCreatedAt()Ljava/util/Date;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 102
    iget-object v1, p0, Lcom/zendesk/sdk/requests/RequestsAdapter$RequestRow;->date:Landroid/widget/TextView;

    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v3, "dd MMMM yyy"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/request/Request;->getUpdatedAt()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 105
    :cond_3
    iget-object v1, p0, Lcom/zendesk/sdk/requests/RequestsAdapter$RequestRow;->unreadIndicator:Landroid/widget/ImageView;

    if-eqz v1, :cond_0

    .line 107
    invoke-virtual {p1}, Lcom/zendesk/sdk/model/request/Request;->getId()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_4

    const/4 v0, 0x0

    .line 111
    .local v0, "storedCommentCount":Ljava/lang/Integer;
    :goto_1
    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/request/Request;->getCommentCount()Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 113
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/request/Request;->getCommentCount()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ge v1, v2, :cond_5

    .line 114
    invoke-virtual {p0, v6}, Lcom/zendesk/sdk/requests/RequestsAdapter$RequestRow;->setUnreadIndicator(Z)V

    goto :goto_0

    .line 107
    .end local v0    # "storedCommentCount":Ljava/lang/Integer;
    :cond_4
    sget-object v1, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    .line 109
    invoke-virtual {v1}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->storage()Lcom/zendesk/sdk/storage/StorageStore;

    move-result-object v1

    invoke-interface {v1}, Lcom/zendesk/sdk/storage/StorageStore;->requestStorage()Lcom/zendesk/sdk/storage/RequestStorage;

    move-result-object v1

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/request/Request;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/zendesk/sdk/storage/RequestStorage;->getCommentCount(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_1

    .line 116
    .restart local v0    # "storedCommentCount":Ljava/lang/Integer;
    :cond_5
    invoke-virtual {p0, v5}, Lcom/zendesk/sdk/requests/RequestsAdapter$RequestRow;->setUnreadIndicator(Z)V

    goto :goto_0

    .line 119
    :cond_6
    invoke-virtual {p0, v6}, Lcom/zendesk/sdk/requests/RequestsAdapter$RequestRow;->setUnreadIndicator(Z)V

    goto :goto_0
.end method

.method public bridge synthetic bind(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 52
    check-cast p1, Lcom/zendesk/sdk/model/request/Request;

    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/requests/RequestsAdapter$RequestRow;->bind(Lcom/zendesk/sdk/model/request/Request;)V

    return-void
.end method

.method public getView()Landroid/view/View;
    .locals 0

    .prologue
    .line 126
    return-object p0
.end method

.method public setUnreadIndicator(Z)V
    .locals 2
    .param p1, "isUnread"    # Z

    .prologue
    .line 140
    iget-object v0, p0, Lcom/zendesk/sdk/requests/RequestsAdapter$RequestRow;->unreadIndicator:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 141
    if-eqz p1, :cond_1

    .line 142
    iget-object v0, p0, Lcom/zendesk/sdk/requests/RequestsAdapter$RequestRow;->unreadIndicator:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 147
    :cond_0
    :goto_0
    return-void

    .line 144
    :cond_1
    iget-object v0, p0, Lcom/zendesk/sdk/requests/RequestsAdapter$RequestRow;->unreadIndicator:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

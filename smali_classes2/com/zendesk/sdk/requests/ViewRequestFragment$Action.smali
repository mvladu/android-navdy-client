.class final enum Lcom/zendesk/sdk/requests/ViewRequestFragment$Action;
.super Ljava/lang/Enum;
.source "ViewRequestFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/sdk/requests/ViewRequestFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "Action"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/zendesk/sdk/requests/ViewRequestFragment$Action;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/zendesk/sdk/requests/ViewRequestFragment$Action;

.field public static final enum LOAD_COMMENTS:Lcom/zendesk/sdk/requests/ViewRequestFragment$Action;

.field public static final enum LOAD_IMAGE:Lcom/zendesk/sdk/requests/ViewRequestFragment$Action;

.field public static final enum SEND_COMMENT:Lcom/zendesk/sdk/requests/ViewRequestFragment$Action;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 675
    new-instance v0, Lcom/zendesk/sdk/requests/ViewRequestFragment$Action;

    const-string v1, "SEND_COMMENT"

    invoke-direct {v0, v1, v2}, Lcom/zendesk/sdk/requests/ViewRequestFragment$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/zendesk/sdk/requests/ViewRequestFragment$Action;->SEND_COMMENT:Lcom/zendesk/sdk/requests/ViewRequestFragment$Action;

    .line 676
    new-instance v0, Lcom/zendesk/sdk/requests/ViewRequestFragment$Action;

    const-string v1, "LOAD_COMMENTS"

    invoke-direct {v0, v1, v3}, Lcom/zendesk/sdk/requests/ViewRequestFragment$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/zendesk/sdk/requests/ViewRequestFragment$Action;->LOAD_COMMENTS:Lcom/zendesk/sdk/requests/ViewRequestFragment$Action;

    .line 677
    new-instance v0, Lcom/zendesk/sdk/requests/ViewRequestFragment$Action;

    const-string v1, "LOAD_IMAGE"

    invoke-direct {v0, v1, v4}, Lcom/zendesk/sdk/requests/ViewRequestFragment$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/zendesk/sdk/requests/ViewRequestFragment$Action;->LOAD_IMAGE:Lcom/zendesk/sdk/requests/ViewRequestFragment$Action;

    .line 674
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/zendesk/sdk/requests/ViewRequestFragment$Action;

    sget-object v1, Lcom/zendesk/sdk/requests/ViewRequestFragment$Action;->SEND_COMMENT:Lcom/zendesk/sdk/requests/ViewRequestFragment$Action;

    aput-object v1, v0, v2

    sget-object v1, Lcom/zendesk/sdk/requests/ViewRequestFragment$Action;->LOAD_COMMENTS:Lcom/zendesk/sdk/requests/ViewRequestFragment$Action;

    aput-object v1, v0, v3

    sget-object v1, Lcom/zendesk/sdk/requests/ViewRequestFragment$Action;->LOAD_IMAGE:Lcom/zendesk/sdk/requests/ViewRequestFragment$Action;

    aput-object v1, v0, v4

    sput-object v0, Lcom/zendesk/sdk/requests/ViewRequestFragment$Action;->$VALUES:[Lcom/zendesk/sdk/requests/ViewRequestFragment$Action;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 674
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/zendesk/sdk/requests/ViewRequestFragment$Action;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 674
    const-class v0, Lcom/zendesk/sdk/requests/ViewRequestFragment$Action;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/zendesk/sdk/requests/ViewRequestFragment$Action;

    return-object v0
.end method

.method public static values()[Lcom/zendesk/sdk/requests/ViewRequestFragment$Action;
    .locals 1

    .prologue
    .line 674
    sget-object v0, Lcom/zendesk/sdk/requests/ViewRequestFragment$Action;->$VALUES:[Lcom/zendesk/sdk/requests/ViewRequestFragment$Action;

    invoke-virtual {v0}, [Lcom/zendesk/sdk/requests/ViewRequestFragment$Action;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/zendesk/sdk/requests/ViewRequestFragment$Action;

    return-object v0
.end method

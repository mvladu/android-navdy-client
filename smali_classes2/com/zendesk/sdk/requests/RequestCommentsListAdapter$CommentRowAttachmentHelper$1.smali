.class Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRowAttachmentHelper$1;
.super Ljava/lang/Object;
.source "RequestCommentsListAdapter.java"

# interfaces
.implements Lcom/squareup/picasso/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRowAttachmentHelper;->attachPhotoToCommentRow(Landroid/view/ViewGroup;Lcom/zendesk/sdk/model/request/CommentResponse;Landroid/content/Context;Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$AttachmentOnClickListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRowAttachmentHelper;

.field final synthetic val$attachment:Lcom/zendesk/sdk/model/request/Attachment;

.field final synthetic val$attachmentHeightMap:Ljava/util/Map;

.field final synthetic val$finalBottomMargin:I

.field final synthetic val$imageView:Landroid/widget/ImageView;

.field final synthetic val$progressBar:Landroid/widget/ProgressBar;

.field final synthetic val$verticalMargin:I


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRowAttachmentHelper;Ljava/util/Map;Lcom/zendesk/sdk/model/request/Attachment;IILandroid/widget/ImageView;Landroid/widget/ProgressBar;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRowAttachmentHelper;

    .prologue
    .line 354
    iput-object p1, p0, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRowAttachmentHelper$1;->this$0:Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRowAttachmentHelper;

    iput-object p2, p0, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRowAttachmentHelper$1;->val$attachmentHeightMap:Ljava/util/Map;

    iput-object p3, p0, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRowAttachmentHelper$1;->val$attachment:Lcom/zendesk/sdk/model/request/Attachment;

    iput p4, p0, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRowAttachmentHelper$1;->val$verticalMargin:I

    iput p5, p0, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRowAttachmentHelper$1;->val$finalBottomMargin:I

    iput-object p6, p0, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRowAttachmentHelper$1;->val$imageView:Landroid/widget/ImageView;

    iput-object p7, p0, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRowAttachmentHelper$1;->val$progressBar:Landroid/widget/ProgressBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError()V
    .locals 2

    .prologue
    .line 368
    iget-object v0, p0, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRowAttachmentHelper$1;->val$progressBar:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 369
    return-void
.end method

.method public onSuccess()V
    .locals 4

    .prologue
    .line 361
    iget-object v0, p0, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRowAttachmentHelper$1;->val$attachmentHeightMap:Ljava/util/Map;

    iget-object v1, p0, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRowAttachmentHelper$1;->val$attachment:Lcom/zendesk/sdk/model/request/Attachment;

    invoke-virtual {v1}, Lcom/zendesk/sdk/model/request/Attachment;->getId()Ljava/lang/Long;

    move-result-object v1

    iget v2, p0, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRowAttachmentHelper$1;->val$verticalMargin:I

    iget v3, p0, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRowAttachmentHelper$1;->val$finalBottomMargin:I

    add-int/2addr v2, v3

    iget-object v3, p0, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRowAttachmentHelper$1;->val$imageView:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    add-int/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 362
    iget-object v0, p0, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRowAttachmentHelper$1;->val$progressBar:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 363
    return-void
.end method

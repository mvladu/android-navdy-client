.class interface abstract Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$IdHolder;
.super Ljava/lang/Object;
.source "RequestCommentsListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/sdk/requests/RequestCommentsListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x60a
    name = "IdHolder"
.end annotation


# virtual methods
.method public abstract getAttachmentsContainerId()I
.end method

.method public abstract getAvatarId()I
.end method

.method public abstract getContainerId()I
.end method

.method public abstract getDateId()I
.end method

.method public abstract getNameId()I
.end method

.method public abstract getResponseId()I
.end method

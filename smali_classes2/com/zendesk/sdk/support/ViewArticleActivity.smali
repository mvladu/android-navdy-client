.class public Lcom/zendesk/sdk/support/ViewArticleActivity;
.super Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;
.source "ViewArticleActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/zendesk/sdk/support/ViewArticleActivity$ArticleAttachmentRow;,
        Lcom/zendesk/sdk/support/ViewArticleActivity$ArticleAttachmentAdapter;,
        Lcom/zendesk/sdk/support/ViewArticleActivity$AttachmentRequestCallback;
    }
.end annotation


# static fields
.field private static final ARTICLE_DATE_FORMAT:Ljava/lang/String; = "MMMM d, yyyy hh:mm"

.field private static final ARTICLE_DETAIL_FORMAT_STRING:Ljava/lang/String; = "%s %s %s"

.field private static final CSS_FILE:Ljava/lang/String; = "file:///android_asset/help_center_article_style.css"

.field public static final EXTRA_ARTICLE:Ljava/lang/String; = "article"

.field public static final EXTRA_LOCALE:Ljava/lang/String; = "locale"

.field public static final EXTRA_SIMPLE_ARTICLE:Ljava/lang/String; = "article_simple"

.field private static final FETCH_ATTACHMENTS_DELAY_MILLIS:J = 0xfaL

.field private static final LOG_TAG:Ljava/lang/String;

.field private static final TYPE_TEXT_HTML:Ljava/lang/String; = "text/html"

.field private static final UTF_8_ENCODING_TYPE:Ljava/lang/String; = "UTF-8"


# instance fields
.field private mArticle:Lcom/zendesk/sdk/model/helpcenter/Article;

.field private mArticleContentWebView:Landroid/webkit/WebView;

.field private mArticleId:Ljava/lang/Long;

.field private mAttachmentListView:Landroid/widget/ListView;

.field private mAttachmentRequestCallback:Lcom/zendesk/service/SafeZendeskCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/zendesk/service/SafeZendeskCallback",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/Attachment;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mHandler:Landroid/os/Handler;

.field private mLocale:Ljava/util/Locale;

.field private mProgressView:Landroid/widget/ProgressBar;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 76
    const-class v0, Lcom/zendesk/sdk/support/ViewArticleActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/zendesk/sdk/support/ViewArticleActivity;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;-><init>()V

    .line 95
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/zendesk/sdk/support/ViewArticleActivity;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    sget-object v0, Lcom/zendesk/sdk/support/ViewArticleActivity;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/zendesk/sdk/support/ViewArticleActivity;)Lcom/zendesk/sdk/model/helpcenter/Article;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/support/ViewArticleActivity;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/zendesk/sdk/support/ViewArticleActivity;->mArticle:Lcom/zendesk/sdk/model/helpcenter/Article;

    return-object v0
.end method

.method static synthetic access$102(Lcom/zendesk/sdk/support/ViewArticleActivity;Lcom/zendesk/sdk/model/helpcenter/Article;)Lcom/zendesk/sdk/model/helpcenter/Article;
    .locals 0
    .param p0, "x0"    # Lcom/zendesk/sdk/support/ViewArticleActivity;
    .param p1, "x1"    # Lcom/zendesk/sdk/model/helpcenter/Article;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/zendesk/sdk/support/ViewArticleActivity;->mArticle:Lcom/zendesk/sdk/model/helpcenter/Article;

    return-object p1
.end method

.method static synthetic access$200(Lcom/zendesk/sdk/support/ViewArticleActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/zendesk/sdk/support/ViewArticleActivity;

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/zendesk/sdk/support/ViewArticleActivity;->loadArticleBody()V

    return-void
.end method

.method static synthetic access$300(Lcom/zendesk/sdk/support/ViewArticleActivity;J)V
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/support/ViewArticleActivity;
    .param p1, "x1"    # J

    .prologue
    .line 70
    invoke-direct {p0, p1, p2}, Lcom/zendesk/sdk/support/ViewArticleActivity;->fetchAttachmentsForArticle(J)V

    return-void
.end method

.method static synthetic access$400(Lcom/zendesk/sdk/support/ViewArticleActivity;)Ljava/lang/Long;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/support/ViewArticleActivity;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/zendesk/sdk/support/ViewArticleActivity;->mArticleId:Ljava/lang/Long;

    return-object v0
.end method

.method static synthetic access$500(Lcom/zendesk/sdk/support/ViewArticleActivity;J)V
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/support/ViewArticleActivity;
    .param p1, "x1"    # J

    .prologue
    .line 70
    invoke-direct {p0, p1, p2}, Lcom/zendesk/sdk/support/ViewArticleActivity;->fetchArticle(J)V

    return-void
.end method

.method static synthetic access$600(Lcom/zendesk/sdk/support/ViewArticleActivity;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/support/ViewArticleActivity;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/zendesk/sdk/support/ViewArticleActivity;->mAttachmentListView:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$700(Landroid/widget/ListView;)V
    .locals 0
    .param p0, "x0"    # Landroid/widget/ListView;

    .prologue
    .line 70
    invoke-static {p0}, Lcom/zendesk/sdk/support/ViewArticleActivity;->setListViewHeightBasedOnChildren(Landroid/widget/ListView;)V

    return-void
.end method

.method private fetchArticle(J)V
    .locals 3
    .param p1, "articleId"    # J

    .prologue
    .line 360
    sget-object v0, Lcom/zendesk/sdk/ui/LoadingState;->LOADING:Lcom/zendesk/sdk/ui/LoadingState;

    invoke-virtual {p0, v0}, Lcom/zendesk/sdk/support/ViewArticleActivity;->setLoadingState(Lcom/zendesk/sdk/ui/LoadingState;)V

    .line 361
    sget-object v0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    invoke-virtual {v0}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->provider()Lcom/zendesk/sdk/network/impl/ProviderStore;

    move-result-object v0

    invoke-interface {v0}, Lcom/zendesk/sdk/network/impl/ProviderStore;->helpCenterProvider()Lcom/zendesk/sdk/network/HelpCenterProvider;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    new-instance v2, Lcom/zendesk/sdk/support/ViewArticleActivity$2;

    invoke-direct {v2, p0}, Lcom/zendesk/sdk/support/ViewArticleActivity$2;-><init>(Lcom/zendesk/sdk/support/ViewArticleActivity;)V

    invoke-interface {v0, v1, v2}, Lcom/zendesk/sdk/network/HelpCenterProvider;->getArticle(Ljava/lang/Long;Lcom/zendesk/service/ZendeskCallback;)V

    .line 373
    return-void
.end method

.method private fetchAttachmentsForArticle(J)V
    .locals 5
    .param p1, "articleId"    # J

    .prologue
    .line 354
    sget-object v1, Lcom/zendesk/sdk/ui/LoadingState;->LOADING:Lcom/zendesk/sdk/ui/LoadingState;

    invoke-virtual {p0, v1}, Lcom/zendesk/sdk/support/ViewArticleActivity;->setLoadingState(Lcom/zendesk/sdk/ui/LoadingState;)V

    .line 355
    sget-object v1, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    invoke-virtual {v1}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->provider()Lcom/zendesk/sdk/network/impl/ProviderStore;

    move-result-object v1

    invoke-interface {v1}, Lcom/zendesk/sdk/network/impl/ProviderStore;->helpCenterProvider()Lcom/zendesk/sdk/network/HelpCenterProvider;

    move-result-object v0

    .line 356
    .local v0, "helpCenterProvider":Lcom/zendesk/sdk/network/HelpCenterProvider;
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    sget-object v2, Lcom/zendesk/sdk/model/helpcenter/AttachmentType;->BLOCK:Lcom/zendesk/sdk/model/helpcenter/AttachmentType;

    iget-object v3, p0, Lcom/zendesk/sdk/support/ViewArticleActivity;->mAttachmentRequestCallback:Lcom/zendesk/service/SafeZendeskCallback;

    invoke-interface {v0, v1, v2, v3}, Lcom/zendesk/sdk/network/HelpCenterProvider;->getAttachments(Ljava/lang/Long;Lcom/zendesk/sdk/model/helpcenter/AttachmentType;Lcom/zendesk/service/ZendeskCallback;)V

    .line 357
    return-void
.end method

.method private loadArticleBody()V
    .locals 12

    .prologue
    .line 377
    sget-object v0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    invoke-virtual {v0}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->provider()Lcom/zendesk/sdk/network/impl/ProviderStore;

    move-result-object v0

    invoke-interface {v0}, Lcom/zendesk/sdk/network/impl/ProviderStore;->helpCenterProvider()Lcom/zendesk/sdk/network/HelpCenterProvider;

    move-result-object v11

    .line 378
    .local v11, "zendeskHelpCenterProvider":Lcom/zendesk/sdk/network/HelpCenterProvider;
    iget-object v0, p0, Lcom/zendesk/sdk/support/ViewArticleActivity;->mArticle:Lcom/zendesk/sdk/model/helpcenter/Article;

    .line 379
    invoke-virtual {v0}, Lcom/zendesk/sdk/model/helpcenter/Article;->getId()Ljava/lang/Long;

    move-result-object v0

    iget-object v1, p0, Lcom/zendesk/sdk/support/ViewArticleActivity;->mArticle:Lcom/zendesk/sdk/model/helpcenter/Article;

    .line 380
    invoke-virtual {v1}, Lcom/zendesk/sdk/model/helpcenter/Article;->getLocale()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/zendesk/util/LocaleUtil;->forLanguageTag(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v1

    new-instance v3, Lcom/zendesk/sdk/support/ViewArticleActivity$3;

    invoke-direct {v3, p0}, Lcom/zendesk/sdk/support/ViewArticleActivity$3;-><init>(Lcom/zendesk/sdk/support/ViewArticleActivity;)V

    .line 378
    invoke-interface {v11, v0, v1, v3}, Lcom/zendesk/sdk/network/HelpCenterProvider;->submitRecordArticleView(Ljava/lang/Long;Ljava/util/Locale;Lcom/zendesk/service/ZendeskCallback;)V

    .line 401
    sget-object v0, Lcom/zendesk/sdk/ui/LoadingState;->DISPLAYING:Lcom/zendesk/sdk/ui/LoadingState;

    invoke-virtual {p0, v0}, Lcom/zendesk/sdk/support/ViewArticleActivity;->setLoadingState(Lcom/zendesk/sdk/ui/LoadingState;)V

    .line 402
    invoke-virtual {p0}, Lcom/zendesk/sdk/support/ViewArticleActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v6

    .line 403
    .local v6, "actionBar":Landroid/support/v7/app/ActionBar;
    if-eqz v6, :cond_0

    .line 404
    iget-object v0, p0, Lcom/zendesk/sdk/support/ViewArticleActivity;->mArticle:Lcom/zendesk/sdk/model/helpcenter/Article;

    invoke-virtual {v0}, Lcom/zendesk/sdk/model/helpcenter/Article;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/support/v7/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 407
    :cond_0
    const/4 v8, 0x0

    .line 408
    .local v8, "dateFormatted":Ljava/lang/String;
    iget-object v0, p0, Lcom/zendesk/sdk/support/ViewArticleActivity;->mArticle:Lcom/zendesk/sdk/model/helpcenter/Article;

    invoke-virtual {v0}, Lcom/zendesk/sdk/model/helpcenter/Article;->getAuthor()Lcom/zendesk/sdk/model/helpcenter/User;

    move-result-object v0

    if-nez v0, :cond_2

    const/4 v7, 0x0

    .line 410
    .local v7, "authorName":Ljava/lang/String;
    :goto_0
    iget-object v0, p0, Lcom/zendesk/sdk/support/ViewArticleActivity;->mArticle:Lcom/zendesk/sdk/model/helpcenter/Article;

    invoke-virtual {v0}, Lcom/zendesk/sdk/model/helpcenter/Article;->getCreatedAt()Ljava/util/Date;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 411
    new-instance v10, Ljava/text/SimpleDateFormat;

    const-string v0, "MMMM d, yyyy hh:mm"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-direct {v10, v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 412
    .local v10, "df":Ljava/text/SimpleDateFormat;
    iget-object v0, p0, Lcom/zendesk/sdk/support/ViewArticleActivity;->mArticle:Lcom/zendesk/sdk/model/helpcenter/Article;

    invoke-virtual {v0}, Lcom/zendesk/sdk/model/helpcenter/Article;->getCreatedAt()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v10, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v8

    .line 415
    .end local v10    # "df":Ljava/text/SimpleDateFormat;
    :cond_1
    if-eqz v8, :cond_3

    if-eqz v7, :cond_3

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%s %s %s"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v7, v3, v4

    const/4 v4, 0x1

    sget v5, Lcom/zendesk/sdk/R$string;->view_article_seperator:I

    .line 418
    invoke-virtual {p0, v5}, Lcom/zendesk/sdk/support/ViewArticleActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    aput-object v8, v3, v4

    .line 416
    invoke-static {v0, v1, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    .line 422
    .local v9, "details":Ljava/lang/String;
    :goto_1
    const-string v2, "<HTML><HEAD><LINK href=\'%s\' type=\'text/css\' rel=\'stylesheet\'/></HEAD><body><h1>%s</h1>%s<footer><p>%s</p></footer></body></HTML>"

    .line 430
    .local v2, "html":Ljava/lang/String;
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v3, "file:///android_asset/help_center_article_style.css"

    aput-object v3, v0, v1

    const/4 v1, 0x1

    iget-object v3, p0, Lcom/zendesk/sdk/support/ViewArticleActivity;->mArticle:Lcom/zendesk/sdk/model/helpcenter/Article;

    invoke-virtual {v3}, Lcom/zendesk/sdk/model/helpcenter/Article;->getTitle()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v1

    const/4 v1, 0x2

    iget-object v3, p0, Lcom/zendesk/sdk/support/ViewArticleActivity;->mArticle:Lcom/zendesk/sdk/model/helpcenter/Article;

    invoke-virtual {v3}, Lcom/zendesk/sdk/model/helpcenter/Article;->getBody()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v1

    const/4 v1, 0x3

    aput-object v9, v0, v1

    invoke-static {v2, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 432
    iget-object v0, p0, Lcom/zendesk/sdk/support/ViewArticleActivity;->mArticleContentWebView:Landroid/webkit/WebView;

    sget-object v1, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    .line 433
    invoke-virtual {v1}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->getZendeskUrl()Ljava/lang/String;

    move-result-object v1

    const-string v3, "text/html"

    const-string v4, "UTF-8"

    const/4 v5, 0x0

    .line 432
    invoke-virtual/range {v0 .. v5}, Landroid/webkit/WebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 441
    iget-object v0, p0, Lcom/zendesk/sdk/support/ViewArticleActivity;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/zendesk/sdk/support/ViewArticleActivity$4;

    invoke-direct {v1, p0}, Lcom/zendesk/sdk/support/ViewArticleActivity$4;-><init>(Lcom/zendesk/sdk/support/ViewArticleActivity;)V

    const-wide/16 v4, 0xfa

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 447
    return-void

    .line 408
    .end local v2    # "html":Ljava/lang/String;
    .end local v7    # "authorName":Ljava/lang/String;
    .end local v9    # "details":Ljava/lang/String;
    :cond_2
    iget-object v0, p0, Lcom/zendesk/sdk/support/ViewArticleActivity;->mArticle:Lcom/zendesk/sdk/model/helpcenter/Article;

    invoke-virtual {v0}, Lcom/zendesk/sdk/model/helpcenter/Article;->getAuthor()Lcom/zendesk/sdk/model/helpcenter/User;

    move-result-object v0

    invoke-virtual {v0}, Lcom/zendesk/sdk/model/helpcenter/User;->getName()Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_0

    .line 416
    .restart local v7    # "authorName":Ljava/lang/String;
    :cond_3
    const-string v9, ""

    goto :goto_1
.end method

.method private static setListViewHeightBasedOnChildren(Landroid/widget/ListView;)V
    .locals 9
    .param p0, "listView"    # Landroid/widget/ListView;

    .prologue
    const/4 v8, 0x0

    .line 305
    invoke-virtual {p0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v2

    .line 306
    .local v2, "listAdapter":Landroid/widget/ListAdapter;
    if-nez v2, :cond_0

    .line 324
    :goto_0
    return-void

    .line 309
    :cond_0
    invoke-virtual {p0}, Landroid/widget/ListView;->getWidth()I

    move-result v6

    invoke-static {v6, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 310
    .local v0, "desiredWidth":I
    const/4 v4, 0x0

    .line 311
    .local v4, "totalHeight":I
    const/4 v5, 0x0

    .line 312
    .local v5, "view":Landroid/view/View;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-interface {v2}, Landroid/widget/ListAdapter;->getCount()I

    move-result v6

    if-ge v1, v6, :cond_2

    .line 313
    invoke-interface {v2, v1, v5, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 314
    if-nez v1, :cond_1

    .line 315
    new-instance v6, Landroid/view/ViewGroup$LayoutParams;

    const/4 v7, -0x2

    invoke-direct {v6, v0, v7}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v5, v6}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 317
    :cond_1
    invoke-virtual {v5, v0, v8}, Landroid/view/View;->measure(II)V

    .line 318
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v4, v6

    .line 312
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 320
    :cond_2
    invoke-virtual {p0}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    .line 321
    .local v3, "params":Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {p0}, Landroid/widget/ListView;->getDividerHeight()I

    move-result v6

    invoke-interface {v2}, Landroid/widget/ListAdapter;->getCount()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    mul-int/2addr v6, v7

    add-int/2addr v6, v4

    iput v6, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 322
    invoke-virtual {p0, v3}, Landroid/widget/ListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 323
    invoke-virtual {p0}, Landroid/widget/ListView;->requestLayout()V

    goto :goto_0
.end method

.method private setupRequestInterceptor()V
    .locals 6

    .prologue
    .line 197
    iget-object v3, p0, Lcom/zendesk/sdk/support/ViewArticleActivity;->mArticleContentWebView:Landroid/webkit/WebView;

    if-nez v3, :cond_0

    .line 198
    sget-object v3, Lcom/zendesk/sdk/support/ViewArticleActivity;->LOG_TAG:Ljava/lang/String;

    const-string v4, "The webview is null. Make sure you initialise it before trying to add the interceptor"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v3, v4, v5}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 296
    :goto_0
    return-void

    .line 202
    :cond_0
    new-instance v1, Lokhttp3/OkHttpClient;

    invoke-direct {v1}, Lokhttp3/OkHttpClient;-><init>()V

    .line 203
    .local v1, "httpClient":Lokhttp3/OkHttpClient;
    new-instance v0, Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinkingParser;

    invoke-direct {v0}, Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinkingParser;-><init>()V

    .line 204
    .local v0, "deepLinkingParser":Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinkingParser;
    sget-object v3, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    invoke-virtual {v3}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->getSdkOptions()Lcom/zendesk/sdk/network/SdkOptions;

    move-result-object v3

    invoke-interface {v3}, Lcom/zendesk/sdk/network/SdkOptions;->overrideResourceLoadingInWebview()Z

    move-result v2

    .line 206
    .local v2, "interceptRequests":Z
    iget-object v3, p0, Lcom/zendesk/sdk/support/ViewArticleActivity;->mArticleContentWebView:Landroid/webkit/WebView;

    new-instance v4, Lcom/zendesk/sdk/support/ViewArticleActivity$1;

    invoke-direct {v4, p0, v2, v1, v0}, Lcom/zendesk/sdk/support/ViewArticleActivity$1;-><init>(Lcom/zendesk/sdk/support/ViewArticleActivity;ZLokhttp3/OkHttpClient;Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinkingParser;)V

    invoke-virtual {v3, v4}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    goto :goto_0
.end method

.method public static startActivity(Landroid/content/Context;Lcom/zendesk/sdk/model/helpcenter/Article;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "article"    # Lcom/zendesk/sdk/model/helpcenter/Article;

    .prologue
    .line 104
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/zendesk/sdk/support/ViewArticleActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 105
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "article"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 106
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 107
    return-void
.end method

.method public static startActivity(Landroid/content/Context;Lcom/zendesk/sdk/model/helpcenter/SimpleArticle;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "simpleArticle"    # Lcom/zendesk/sdk/model/helpcenter/SimpleArticle;

    .prologue
    .line 110
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/zendesk/sdk/support/ViewArticleActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 111
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "article_simple"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 112
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 113
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetJavaScriptEnabled"
        }
    .end annotation

    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v7, 0x0

    .line 120
    invoke-super {p0, p1}, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 121
    sget v6, Lcom/zendesk/sdk/R$layout;->activity_view_article:I

    invoke-virtual {p0, v6}, Lcom/zendesk/sdk/support/ViewArticleActivity;->setContentView(I)V

    .line 122
    invoke-static {p0}, Lcom/zendesk/sdk/ui/ToolbarSherlock;->installToolBar(Landroid/support/v7/app/AppCompatActivity;)V

    .line 124
    invoke-virtual {p0}, Lcom/zendesk/sdk/support/ViewArticleActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    .line 126
    .local v0, "actionBar":Landroid/support/v7/app/ActionBar;
    if-eqz v0, :cond_0

    .line 127
    invoke-virtual {v0, v3}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 130
    :cond_0
    sget v6, Lcom/zendesk/sdk/R$id;->view_article_content_webview:I

    invoke-virtual {p0, v6}, Lcom/zendesk/sdk/support/ViewArticleActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/webkit/WebView;

    iput-object v6, p0, Lcom/zendesk/sdk/support/ViewArticleActivity;->mArticleContentWebView:Landroid/webkit/WebView;

    .line 132
    iget-object v6, p0, Lcom/zendesk/sdk/support/ViewArticleActivity;->mArticleContentWebView:Landroid/webkit/WebView;

    new-instance v8, Landroid/webkit/WebChromeClient;

    invoke-direct {v8}, Landroid/webkit/WebChromeClient;-><init>()V

    invoke-virtual {v6, v8}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 133
    iget-object v6, p0, Lcom/zendesk/sdk/support/ViewArticleActivity;->mArticleContentWebView:Landroid/webkit/WebView;

    invoke-virtual {v6}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v6

    invoke-virtual {v6, v3}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 135
    invoke-direct {p0}, Lcom/zendesk/sdk/support/ViewArticleActivity;->setupRequestInterceptor()V

    .line 143
    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v8, 0x15

    if-lt v6, v8, :cond_1

    .line 144
    iget-object v6, p0, Lcom/zendesk/sdk/support/ViewArticleActivity;->mArticleContentWebView:Landroid/webkit/WebView;

    invoke-virtual {v6}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v6

    invoke-virtual {v6, v7}, Landroid/webkit/WebSettings;->setMixedContentMode(I)V

    .line 147
    :cond_1
    sget v6, Lcom/zendesk/sdk/R$id;->view_article_progress:I

    invoke-virtual {p0, v6}, Lcom/zendesk/sdk/support/ViewArticleActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ProgressBar;

    iput-object v6, p0, Lcom/zendesk/sdk/support/ViewArticleActivity;->mProgressView:Landroid/widget/ProgressBar;

    .line 148
    sget v6, Lcom/zendesk/sdk/R$id;->view_article_attachment_list:I

    invoke-virtual {p0, v6}, Lcom/zendesk/sdk/support/ViewArticleActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ListView;

    iput-object v6, p0, Lcom/zendesk/sdk/support/ViewArticleActivity;->mAttachmentListView:Landroid/widget/ListView;

    .line 150
    invoke-virtual {p0}, Lcom/zendesk/sdk/support/ViewArticleActivity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 152
    .local v1, "extras":Landroid/os/Bundle;
    if-eqz v1, :cond_2

    const-string v6, "locale"

    invoke-virtual {v1, v6}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 154
    const-string v6, "locale"

    invoke-virtual {v1, v6}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v4

    .line 155
    .local v4, "serializable":Ljava/io/Serializable;
    instance-of v6, v4, Ljava/util/Locale;

    if-eqz v6, :cond_2

    .line 156
    const-string v6, "locale"

    invoke-virtual {v1, v6}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v6

    check-cast v6, Ljava/util/Locale;

    iput-object v6, p0, Lcom/zendesk/sdk/support/ViewArticleActivity;->mLocale:Ljava/util/Locale;

    .line 159
    .end local v4    # "serializable":Ljava/io/Serializable;
    :cond_2
    iget-object v6, p0, Lcom/zendesk/sdk/support/ViewArticleActivity;->mLocale:Ljava/util/Locale;

    if-nez v6, :cond_3

    .line 160
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    iput-object v6, p0, Lcom/zendesk/sdk/support/ViewArticleActivity;->mLocale:Ljava/util/Locale;

    .line 163
    :cond_3
    if-eqz v1, :cond_4

    const-string v6, "article"

    .line 164
    invoke-virtual {v1, v6}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    const-string v6, "article"

    .line 165
    invoke-virtual {v1, v6}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v6

    instance-of v6, v6, Lcom/zendesk/sdk/model/helpcenter/Article;

    if-eqz v6, :cond_4

    move v2, v3

    .line 167
    .local v2, "hasArticleExtra":Z
    :goto_0
    if-eqz v1, :cond_5

    const-string v6, "article_simple"

    .line 168
    invoke-virtual {v1, v6}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    const-string v6, "article_simple"

    .line 169
    invoke-virtual {v1, v6}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v6

    instance-of v6, v6, Lcom/zendesk/sdk/model/helpcenter/SimpleArticle;

    if-eqz v6, :cond_5

    .line 171
    .local v3, "hasSimpleArticleExtra":Z
    :goto_1
    if-eqz v2, :cond_6

    .line 172
    const-string v6, "article"

    invoke-virtual {v1, v6}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v6

    check-cast v6, Lcom/zendesk/sdk/model/helpcenter/Article;

    iput-object v6, p0, Lcom/zendesk/sdk/support/ViewArticleActivity;->mArticle:Lcom/zendesk/sdk/model/helpcenter/Article;

    .line 173
    invoke-direct {p0}, Lcom/zendesk/sdk/support/ViewArticleActivity;->loadArticleBody()V

    .line 192
    :goto_2
    return-void

    .end local v2    # "hasArticleExtra":Z
    .end local v3    # "hasSimpleArticleExtra":Z
    :cond_4
    move v2, v7

    .line 165
    goto :goto_0

    .restart local v2    # "hasArticleExtra":Z
    :cond_5
    move v3, v7

    .line 169
    goto :goto_1

    .line 174
    .restart local v3    # "hasSimpleArticleExtra":Z
    :cond_6
    if-eqz v3, :cond_8

    .line 176
    const-string v6, "article_simple"

    .line 177
    invoke-virtual {v1, v6}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v5

    check-cast v5, Lcom/zendesk/sdk/model/helpcenter/SimpleArticle;

    .line 180
    .local v5, "simpleArticle":Lcom/zendesk/sdk/model/helpcenter/SimpleArticle;
    invoke-virtual {v5}, Lcom/zendesk/sdk/model/helpcenter/SimpleArticle;->getId()Ljava/lang/Long;

    move-result-object v6

    if-eqz v6, :cond_7

    .line 181
    invoke-virtual {v5}, Lcom/zendesk/sdk/model/helpcenter/SimpleArticle;->getId()Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-direct {p0, v6, v7}, Lcom/zendesk/sdk/support/ViewArticleActivity;->fetchArticle(J)V

    .line 182
    invoke-virtual {v5}, Lcom/zendesk/sdk/model/helpcenter/SimpleArticle;->getId()Ljava/lang/Long;

    move-result-object v6

    iput-object v6, p0, Lcom/zendesk/sdk/support/ViewArticleActivity;->mArticleId:Ljava/lang/Long;

    goto :goto_2

    .line 184
    :cond_7
    sget-object v6, Lcom/zendesk/sdk/support/ViewArticleActivity;->LOG_TAG:Ljava/lang/String;

    const-string v8, "EXTRA_SIMPLE_ARTICLE is present, but missing an article ID. Cannot continue."

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v6, v8, v7}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 185
    invoke-virtual {p0}, Lcom/zendesk/sdk/support/ViewArticleActivity;->finish()V

    goto :goto_2

    .line 189
    .end local v5    # "simpleArticle":Lcom/zendesk/sdk/model/helpcenter/SimpleArticle;
    :cond_8
    sget-object v6, Lcom/zendesk/sdk/support/ViewArticleActivity;->LOG_TAG:Ljava/lang/String;

    const-string v8, "EXTRA_ARTICLE or EXTRA_SIMPLE_ARTICLE is a required extra for this activity. Cannot continue."

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v6, v8, v7}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 190
    invoke-virtual {p0}, Lcom/zendesk/sdk/support/ViewArticleActivity;->finish()V

    goto :goto_2
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 618
    invoke-super {p0}, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->onDestroy()V

    .line 620
    iget-object v0, p0, Lcom/zendesk/sdk/support/ViewArticleActivity;->mArticleContentWebView:Landroid/webkit/WebView;

    if-eqz v0, :cond_0

    .line 621
    iget-object v0, p0, Lcom/zendesk/sdk/support/ViewArticleActivity;->mArticleContentWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->destroy()V

    .line 623
    :cond_0
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 7
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 508
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v2

    .line 511
    .local v2, "item":Ljava/lang/Object;
    instance-of v4, v2, Lcom/zendesk/sdk/model/helpcenter/Attachment;

    if-eqz v4, :cond_0

    move-object v0, v2

    .line 512
    check-cast v0, Lcom/zendesk/sdk/model/helpcenter/Attachment;

    .line 514
    .local v0, "attachment":Lcom/zendesk/sdk/model/helpcenter/Attachment;
    invoke-virtual {v0}, Lcom/zendesk/sdk/model/helpcenter/Attachment;->getContentUrl()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 516
    invoke-virtual {v0}, Lcom/zendesk/sdk/model/helpcenter/Attachment;->getContentUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 518
    .local v1, "attachmentUri":Landroid/net/Uri;
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 519
    .local v3, "viewAttachment":Landroid/content/Intent;
    const-string v4, "android.intent.action.VIEW"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 520
    invoke-virtual {v3, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 521
    invoke-virtual {p0, v3}, Lcom/zendesk/sdk/support/ViewArticleActivity;->startActivity(Landroid/content/Intent;)V

    .line 526
    .end local v0    # "attachment":Lcom/zendesk/sdk/model/helpcenter/Attachment;
    .end local v1    # "attachmentUri":Landroid/net/Uri;
    .end local v3    # "viewAttachment":Landroid/content/Intent;
    :cond_0
    :goto_0
    return-void

    .line 523
    .restart local v0    # "attachment":Lcom/zendesk/sdk/model/helpcenter/Attachment;
    :cond_1
    sget-object v4, Lcom/zendesk/sdk/support/ViewArticleActivity;->LOG_TAG:Ljava/lang/String;

    const-string v5, "Unable to launch viewer, unable to parse URI for attachment"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 342
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 343
    invoke-virtual {p0}, Lcom/zendesk/sdk/support/ViewArticleActivity;->onBackPressed()V

    .line 344
    const/4 v0, 0x1

    .line 347
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 334
    invoke-super {p0}, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->onPause()V

    .line 335
    iget-object v0, p0, Lcom/zendesk/sdk/support/ViewArticleActivity;->mAttachmentRequestCallback:Lcom/zendesk/service/SafeZendeskCallback;

    invoke-virtual {v0}, Lcom/zendesk/service/SafeZendeskCallback;->cancel()V

    .line 336
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/zendesk/sdk/support/ViewArticleActivity;->mAttachmentRequestCallback:Lcom/zendesk/service/SafeZendeskCallback;

    .line 337
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 328
    invoke-super {p0}, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->onResume()V

    .line 329
    new-instance v0, Lcom/zendesk/sdk/support/ViewArticleActivity$AttachmentRequestCallback;

    invoke-direct {v0, p0}, Lcom/zendesk/sdk/support/ViewArticleActivity$AttachmentRequestCallback;-><init>(Lcom/zendesk/sdk/support/ViewArticleActivity;)V

    invoke-static {v0}, Lcom/zendesk/service/SafeZendeskCallback;->from(Lcom/zendesk/service/ZendeskCallback;)Lcom/zendesk/service/SafeZendeskCallback;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/support/ViewArticleActivity;->mAttachmentRequestCallback:Lcom/zendesk/service/SafeZendeskCallback;

    .line 330
    return-void
.end method

.method protected setLoadingState(Lcom/zendesk/sdk/ui/LoadingState;)V
    .locals 4
    .param p1, "loadingState"    # Lcom/zendesk/sdk/ui/LoadingState;

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 460
    if-nez p1, :cond_0

    .line 461
    sget-object v0, Lcom/zendesk/sdk/support/ViewArticleActivity;->LOG_TAG:Ljava/lang/String;

    const-string v1, "LoadingState was null, nothing to do"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 504
    :goto_0
    return-void

    .line 465
    :cond_0
    sget-object v0, Lcom/zendesk/sdk/support/ViewArticleActivity$6;->$SwitchMap$com$zendesk$sdk$ui$LoadingState:[I

    invoke-virtual {p1}, Lcom/zendesk/sdk/ui/LoadingState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 467
    :pswitch_0
    iget-object v0, p0, Lcom/zendesk/sdk/support/ViewArticleActivity;->mProgressView:Landroid/widget/ProgressBar;

    invoke-static {v0, v3}, Lcom/zendesk/sdk/util/UiUtils;->setVisibility(Landroid/view/View;I)V

    .line 468
    iget-object v0, p0, Lcom/zendesk/sdk/support/ViewArticleActivity;->mAttachmentListView:Landroid/widget/ListView;

    invoke-static {v0, v2}, Lcom/zendesk/sdk/util/UiUtils;->setVisibility(Landroid/view/View;I)V

    .line 470
    invoke-virtual {p0}, Lcom/zendesk/sdk/support/ViewArticleActivity;->onRetryUnavailable()V

    goto :goto_0

    .line 475
    :pswitch_1
    iget-object v0, p0, Lcom/zendesk/sdk/support/ViewArticleActivity;->mProgressView:Landroid/widget/ProgressBar;

    invoke-static {v0, v2}, Lcom/zendesk/sdk/util/UiUtils;->setVisibility(Landroid/view/View;I)V

    .line 476
    iget-object v0, p0, Lcom/zendesk/sdk/support/ViewArticleActivity;->mAttachmentListView:Landroid/widget/ListView;

    invoke-static {v0, v3}, Lcom/zendesk/sdk/util/UiUtils;->setVisibility(Landroid/view/View;I)V

    .line 478
    invoke-virtual {p0}, Lcom/zendesk/sdk/support/ViewArticleActivity;->onRetryUnavailable()V

    goto :goto_0

    .line 483
    :pswitch_2
    iget-object v0, p0, Lcom/zendesk/sdk/support/ViewArticleActivity;->mProgressView:Landroid/widget/ProgressBar;

    invoke-static {v0, v2}, Lcom/zendesk/sdk/util/UiUtils;->setVisibility(Landroid/view/View;I)V

    .line 484
    iget-object v0, p0, Lcom/zendesk/sdk/support/ViewArticleActivity;->mAttachmentListView:Landroid/widget/ListView;

    invoke-static {v0, v2}, Lcom/zendesk/sdk/util/UiUtils;->setVisibility(Landroid/view/View;I)V

    .line 486
    sget v0, Lcom/zendesk/sdk/R$string;->view_article_attachments_error:I

    invoke-virtual {p0, v0}, Lcom/zendesk/sdk/support/ViewArticleActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/zendesk/sdk/support/ViewArticleActivity$5;

    invoke-direct {v1, p0}, Lcom/zendesk/sdk/support/ViewArticleActivity$5;-><init>(Lcom/zendesk/sdk/support/ViewArticleActivity;)V

    invoke-virtual {p0, v0, v1}, Lcom/zendesk/sdk/support/ViewArticleActivity;->onRetryAvailable(Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 465
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

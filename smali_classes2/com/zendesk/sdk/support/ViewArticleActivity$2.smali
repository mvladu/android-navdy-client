.class Lcom/zendesk/sdk/support/ViewArticleActivity$2;
.super Lcom/zendesk/service/ZendeskCallback;
.source "ViewArticleActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/support/ViewArticleActivity;->fetchArticle(J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/zendesk/service/ZendeskCallback",
        "<",
        "Lcom/zendesk/sdk/model/helpcenter/Article;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/support/ViewArticleActivity;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/support/ViewArticleActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/support/ViewArticleActivity;

    .prologue
    .line 361
    iput-object p1, p0, Lcom/zendesk/sdk/support/ViewArticleActivity$2;->this$0:Lcom/zendesk/sdk/support/ViewArticleActivity;

    invoke-direct {p0}, Lcom/zendesk/service/ZendeskCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/zendesk/service/ErrorResponse;)V
    .locals 2
    .param p1, "error"    # Lcom/zendesk/service/ErrorResponse;

    .prologue
    .line 370
    iget-object v0, p0, Lcom/zendesk/sdk/support/ViewArticleActivity$2;->this$0:Lcom/zendesk/sdk/support/ViewArticleActivity;

    sget-object v1, Lcom/zendesk/sdk/ui/LoadingState;->ERRORED:Lcom/zendesk/sdk/ui/LoadingState;

    invoke-virtual {v0, v1}, Lcom/zendesk/sdk/support/ViewArticleActivity;->setLoadingState(Lcom/zendesk/sdk/ui/LoadingState;)V

    .line 371
    return-void
.end method

.method public onSuccess(Lcom/zendesk/sdk/model/helpcenter/Article;)V
    .locals 1
    .param p1, "result"    # Lcom/zendesk/sdk/model/helpcenter/Article;

    .prologue
    .line 364
    iget-object v0, p0, Lcom/zendesk/sdk/support/ViewArticleActivity$2;->this$0:Lcom/zendesk/sdk/support/ViewArticleActivity;

    invoke-static {v0, p1}, Lcom/zendesk/sdk/support/ViewArticleActivity;->access$102(Lcom/zendesk/sdk/support/ViewArticleActivity;Lcom/zendesk/sdk/model/helpcenter/Article;)Lcom/zendesk/sdk/model/helpcenter/Article;

    .line 365
    iget-object v0, p0, Lcom/zendesk/sdk/support/ViewArticleActivity$2;->this$0:Lcom/zendesk/sdk/support/ViewArticleActivity;

    invoke-static {v0}, Lcom/zendesk/sdk/support/ViewArticleActivity;->access$200(Lcom/zendesk/sdk/support/ViewArticleActivity;)V

    .line 366
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 361
    check-cast p1, Lcom/zendesk/sdk/model/helpcenter/Article;

    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/support/ViewArticleActivity$2;->onSuccess(Lcom/zendesk/sdk/model/helpcenter/Article;)V

    return-void
.end method

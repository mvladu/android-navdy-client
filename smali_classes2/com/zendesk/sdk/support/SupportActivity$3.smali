.class Lcom/zendesk/sdk/support/SupportActivity$3;
.super Ljava/lang/Object;
.source "SupportActivity.java"

# interfaces
.implements Landroid/support/v7/widget/SearchView$OnQueryTextListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/support/SupportActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/support/SupportActivity;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/support/SupportActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/support/SupportActivity;

    .prologue
    .line 193
    iput-object p1, p0, Lcom/zendesk/sdk/support/SupportActivity$3;->this$0:Lcom/zendesk/sdk/support/SupportActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onQueryTextChange(Ljava/lang/String;)Z
    .locals 1
    .param p1, "newText"    # Ljava/lang/String;

    .prologue
    .line 207
    const/4 v0, 0x0

    return v0
.end method

.method public onQueryTextSubmit(Ljava/lang/String;)Z
    .locals 1
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 197
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportActivity$3;->this$0:Lcom/zendesk/sdk/support/SupportActivity;

    invoke-static {v0}, Lcom/zendesk/sdk/support/SupportActivity;->access$100(Lcom/zendesk/sdk/support/SupportActivity;)Lcom/zendesk/sdk/support/SupportMvp$Presenter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 198
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportActivity$3;->this$0:Lcom/zendesk/sdk/support/SupportActivity;

    invoke-static {v0}, Lcom/zendesk/sdk/support/SupportActivity;->access$100(Lcom/zendesk/sdk/support/SupportActivity;)Lcom/zendesk/sdk/support/SupportMvp$Presenter;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/zendesk/sdk/support/SupportMvp$Presenter;->onSearchSubmit(Ljava/lang/String;)V

    .line 199
    const/4 v0, 0x1

    .line 201
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

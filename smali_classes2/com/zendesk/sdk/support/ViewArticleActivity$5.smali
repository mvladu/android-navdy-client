.class Lcom/zendesk/sdk/support/ViewArticleActivity$5;
.super Ljava/lang/Object;
.source "ViewArticleActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/support/ViewArticleActivity;->setLoadingState(Lcom/zendesk/sdk/ui/LoadingState;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/support/ViewArticleActivity;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/support/ViewArticleActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/support/ViewArticleActivity;

    .prologue
    .line 486
    iput-object p1, p0, Lcom/zendesk/sdk/support/ViewArticleActivity$5;->this$0:Lcom/zendesk/sdk/support/ViewArticleActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 490
    iget-object v0, p0, Lcom/zendesk/sdk/support/ViewArticleActivity$5;->this$0:Lcom/zendesk/sdk/support/ViewArticleActivity;

    invoke-static {v0}, Lcom/zendesk/sdk/support/ViewArticleActivity;->access$400(Lcom/zendesk/sdk/support/ViewArticleActivity;)Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/zendesk/sdk/support/ViewArticleActivity$5;->this$0:Lcom/zendesk/sdk/support/ViewArticleActivity;

    invoke-static {v0}, Lcom/zendesk/sdk/support/ViewArticleActivity;->access$100(Lcom/zendesk/sdk/support/ViewArticleActivity;)Lcom/zendesk/sdk/model/helpcenter/Article;

    move-result-object v0

    if-nez v0, :cond_1

    .line 491
    iget-object v0, p0, Lcom/zendesk/sdk/support/ViewArticleActivity$5;->this$0:Lcom/zendesk/sdk/support/ViewArticleActivity;

    iget-object v1, p0, Lcom/zendesk/sdk/support/ViewArticleActivity$5;->this$0:Lcom/zendesk/sdk/support/ViewArticleActivity;

    invoke-static {v1}, Lcom/zendesk/sdk/support/ViewArticleActivity;->access$400(Lcom/zendesk/sdk/support/ViewArticleActivity;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/zendesk/sdk/support/ViewArticleActivity;->access$500(Lcom/zendesk/sdk/support/ViewArticleActivity;J)V

    .line 498
    :cond_0
    :goto_0
    return-void

    .line 493
    :cond_1
    iget-object v0, p0, Lcom/zendesk/sdk/support/ViewArticleActivity$5;->this$0:Lcom/zendesk/sdk/support/ViewArticleActivity;

    invoke-static {v0}, Lcom/zendesk/sdk/support/ViewArticleActivity;->access$100(Lcom/zendesk/sdk/support/ViewArticleActivity;)Lcom/zendesk/sdk/model/helpcenter/Article;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 494
    iget-object v0, p0, Lcom/zendesk/sdk/support/ViewArticleActivity$5;->this$0:Lcom/zendesk/sdk/support/ViewArticleActivity;

    iget-object v1, p0, Lcom/zendesk/sdk/support/ViewArticleActivity$5;->this$0:Lcom/zendesk/sdk/support/ViewArticleActivity;

    invoke-static {v1}, Lcom/zendesk/sdk/support/ViewArticleActivity;->access$100(Lcom/zendesk/sdk/support/ViewArticleActivity;)Lcom/zendesk/sdk/model/helpcenter/Article;

    move-result-object v1

    invoke-virtual {v1}, Lcom/zendesk/sdk/model/helpcenter/Article;->getId()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/zendesk/sdk/support/ViewArticleActivity;->access$300(Lcom/zendesk/sdk/support/ViewArticleActivity;J)V

    goto :goto_0
.end method

.class Lcom/zendesk/sdk/support/help/HelpAdapterPresenter$2;
.super Lcom/zendesk/service/ZendeskCallback;
.source "HelpAdapterPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/zendesk/service/ZendeskCallback",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;

    .prologue
    .line 171
    iput-object p1, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter$2;->this$0:Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;

    invoke-direct {p0}, Lcom/zendesk/service/ZendeskCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/zendesk/service/ErrorResponse;)V
    .locals 3
    .param p1, "error"    # Lcom/zendesk/service/ErrorResponse;

    .prologue
    .line 194
    iget-object v1, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter$2;->this$0:Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;

    invoke-static {v1}, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->access$300(Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;)Lcom/zendesk/sdk/support/SupportUiConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/zendesk/sdk/support/SupportUiConfig;->getCategoryIds()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lcom/zendesk/util/CollectionUtils;->isNotEmpty(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 195
    sget-object v0, Lcom/zendesk/sdk/support/SupportMvp$ErrorType;->CATEGORY_LOAD:Lcom/zendesk/sdk/support/SupportMvp$ErrorType;

    .line 202
    .local v0, "errorType":Lcom/zendesk/sdk/support/SupportMvp$ErrorType;
    :goto_0
    iget-object v1, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter$2;->this$0:Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;

    invoke-static {v1}, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->access$800(Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;)Lcom/zendesk/sdk/support/SupportMvp$Presenter;

    move-result-object v1

    new-instance v2, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter$2$1;

    invoke-direct {v2, p0}, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter$2$1;-><init>(Lcom/zendesk/sdk/support/help/HelpAdapterPresenter$2;)V

    invoke-interface {v1, v0, v2}, Lcom/zendesk/sdk/support/SupportMvp$Presenter;->onErrorWithRetry(Lcom/zendesk/sdk/support/SupportMvp$ErrorType;Lcom/zendesk/sdk/network/RetryAction;)V

    .line 211
    iget-object v1, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter$2;->this$0:Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->access$102(Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;Z)Z

    .line 212
    iget-object v1, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter$2;->this$0:Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;

    invoke-static {v1}, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->access$700(Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;)Lcom/zendesk/sdk/support/help/HelpMvp$View;

    move-result-object v1

    iget-object v2, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter$2;->this$0:Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;

    invoke-static {v2}, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->access$400(Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/zendesk/sdk/support/help/HelpMvp$View;->showItems(Ljava/util/List;)V

    .line 213
    return-void

    .line 196
    .end local v0    # "errorType":Lcom/zendesk/sdk/support/SupportMvp$ErrorType;
    :cond_0
    iget-object v1, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter$2;->this$0:Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;

    invoke-static {v1}, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->access$300(Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;)Lcom/zendesk/sdk/support/SupportUiConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/zendesk/sdk/support/SupportUiConfig;->getSectionIds()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lcom/zendesk/util/CollectionUtils;->isNotEmpty(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 197
    sget-object v0, Lcom/zendesk/sdk/support/SupportMvp$ErrorType;->SECTION_LOAD:Lcom/zendesk/sdk/support/SupportMvp$ErrorType;

    .restart local v0    # "errorType":Lcom/zendesk/sdk/support/SupportMvp$ErrorType;
    goto :goto_0

    .line 199
    .end local v0    # "errorType":Lcom/zendesk/sdk/support/SupportMvp$ErrorType;
    :cond_1
    sget-object v0, Lcom/zendesk/sdk/support/SupportMvp$ErrorType;->ARTICLES_LOAD:Lcom/zendesk/sdk/support/SupportMvp$ErrorType;

    .restart local v0    # "errorType":Lcom/zendesk/sdk/support/SupportMvp$ErrorType;
    goto :goto_0
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 171
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter$2;->onSuccess(Ljava/util/List;)V

    return-void
.end method

.method public onSuccess(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 174
    .local p1, "result":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;>;"
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter$2;->this$0:Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->access$102(Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;Z)Z

    .line 176
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter$2;->this$0:Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;

    invoke-static {p1}, Lcom/zendesk/util/CollectionUtils;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->access$202(Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;Ljava/util/List;)Ljava/util/List;

    .line 178
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter$2;->this$0:Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;

    invoke-static {v0}, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->access$300(Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;)Lcom/zendesk/sdk/support/SupportUiConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/zendesk/sdk/support/SupportUiConfig;->isCollapseCategories()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 179
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter$2;->this$0:Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;

    iget-object v1, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter$2;->this$0:Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;

    iget-object v2, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter$2;->this$0:Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;

    invoke-static {v2}, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->access$200(Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;)Ljava/util/List;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->access$500(Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->access$402(Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;Ljava/util/List;)Ljava/util/List;

    .line 183
    :goto_0
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter$2;->this$0:Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;

    iget-object v1, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter$2;->this$0:Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;

    invoke-static {v1}, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->access$400(Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;)Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lcom/zendesk/util/CollectionUtils;->isEmpty(Ljava/util/Collection;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->access$602(Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;Z)Z

    .line 185
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter$2;->this$0:Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;

    invoke-static {v0}, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->access$700(Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;)Lcom/zendesk/sdk/support/help/HelpMvp$View;

    move-result-object v0

    iget-object v1, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter$2;->this$0:Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;

    invoke-static {v1}, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->access$400(Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/zendesk/sdk/support/help/HelpMvp$View;->showItems(Ljava/util/List;)V

    .line 186
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter$2;->this$0:Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;

    invoke-static {v0}, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->access$800(Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;)Lcom/zendesk/sdk/support/SupportMvp$Presenter;

    move-result-object v0

    invoke-interface {v0}, Lcom/zendesk/sdk/support/SupportMvp$Presenter;->onLoad()V

    .line 187
    return-void

    .line 181
    :cond_0
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter$2;->this$0:Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;

    iget-object v1, p0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter$2;->this$0:Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;

    invoke-static {v1}, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->access$200(Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;)Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lcom/zendesk/util/CollectionUtils;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;->access$402(Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;Ljava/util/List;)Ljava/util/List;

    goto :goto_0
.end method

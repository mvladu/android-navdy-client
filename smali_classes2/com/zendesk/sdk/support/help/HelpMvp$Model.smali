.class public interface abstract Lcom/zendesk/sdk/support/help/HelpMvp$Model;
.super Ljava/lang/Object;
.source "HelpMvp.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/sdk/support/help/HelpMvp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Model"
.end annotation


# virtual methods
.method public abstract getArticles(Ljava/util/List;Ljava/util/List;[Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;[",
            "Ljava/lang/String;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;",
            ">;>;)V"
        }
    .end annotation
.end method

.method public abstract getArticlesForSection(Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;[Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;",
            "[",
            "Ljava/lang/String;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;",
            ">;>;)V"
        }
    .end annotation
.end method

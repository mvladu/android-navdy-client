.class public final enum Lcom/zendesk/sdk/util/UiUtils$ScreenSize;
.super Ljava/lang/Enum;
.source "UiUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/sdk/util/UiUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ScreenSize"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/zendesk/sdk/util/UiUtils$ScreenSize;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/zendesk/sdk/util/UiUtils$ScreenSize;

.field public static final enum LARGE:Lcom/zendesk/sdk/util/UiUtils$ScreenSize;

.field public static final enum NORMAL:Lcom/zendesk/sdk/util/UiUtils$ScreenSize;

.field public static final enum SMALL:Lcom/zendesk/sdk/util/UiUtils$ScreenSize;

.field public static final enum UNDEFINED:Lcom/zendesk/sdk/util/UiUtils$ScreenSize;

.field public static final enum UNKNOWN:Lcom/zendesk/sdk/util/UiUtils$ScreenSize;

.field public static final enum X_LARGE:Lcom/zendesk/sdk/util/UiUtils$ScreenSize;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 29
    new-instance v0, Lcom/zendesk/sdk/util/UiUtils$ScreenSize;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v3}, Lcom/zendesk/sdk/util/UiUtils$ScreenSize;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/zendesk/sdk/util/UiUtils$ScreenSize;->UNKNOWN:Lcom/zendesk/sdk/util/UiUtils$ScreenSize;

    new-instance v0, Lcom/zendesk/sdk/util/UiUtils$ScreenSize;

    const-string v1, "UNDEFINED"

    invoke-direct {v0, v1, v4}, Lcom/zendesk/sdk/util/UiUtils$ScreenSize;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/zendesk/sdk/util/UiUtils$ScreenSize;->UNDEFINED:Lcom/zendesk/sdk/util/UiUtils$ScreenSize;

    new-instance v0, Lcom/zendesk/sdk/util/UiUtils$ScreenSize;

    const-string v1, "X_LARGE"

    invoke-direct {v0, v1, v5}, Lcom/zendesk/sdk/util/UiUtils$ScreenSize;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/zendesk/sdk/util/UiUtils$ScreenSize;->X_LARGE:Lcom/zendesk/sdk/util/UiUtils$ScreenSize;

    new-instance v0, Lcom/zendesk/sdk/util/UiUtils$ScreenSize;

    const-string v1, "LARGE"

    invoke-direct {v0, v1, v6}, Lcom/zendesk/sdk/util/UiUtils$ScreenSize;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/zendesk/sdk/util/UiUtils$ScreenSize;->LARGE:Lcom/zendesk/sdk/util/UiUtils$ScreenSize;

    new-instance v0, Lcom/zendesk/sdk/util/UiUtils$ScreenSize;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v7}, Lcom/zendesk/sdk/util/UiUtils$ScreenSize;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/zendesk/sdk/util/UiUtils$ScreenSize;->NORMAL:Lcom/zendesk/sdk/util/UiUtils$ScreenSize;

    new-instance v0, Lcom/zendesk/sdk/util/UiUtils$ScreenSize;

    const-string v1, "SMALL"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/zendesk/sdk/util/UiUtils$ScreenSize;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/zendesk/sdk/util/UiUtils$ScreenSize;->SMALL:Lcom/zendesk/sdk/util/UiUtils$ScreenSize;

    .line 28
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/zendesk/sdk/util/UiUtils$ScreenSize;

    sget-object v1, Lcom/zendesk/sdk/util/UiUtils$ScreenSize;->UNKNOWN:Lcom/zendesk/sdk/util/UiUtils$ScreenSize;

    aput-object v1, v0, v3

    sget-object v1, Lcom/zendesk/sdk/util/UiUtils$ScreenSize;->UNDEFINED:Lcom/zendesk/sdk/util/UiUtils$ScreenSize;

    aput-object v1, v0, v4

    sget-object v1, Lcom/zendesk/sdk/util/UiUtils$ScreenSize;->X_LARGE:Lcom/zendesk/sdk/util/UiUtils$ScreenSize;

    aput-object v1, v0, v5

    sget-object v1, Lcom/zendesk/sdk/util/UiUtils$ScreenSize;->LARGE:Lcom/zendesk/sdk/util/UiUtils$ScreenSize;

    aput-object v1, v0, v6

    sget-object v1, Lcom/zendesk/sdk/util/UiUtils$ScreenSize;->NORMAL:Lcom/zendesk/sdk/util/UiUtils$ScreenSize;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/zendesk/sdk/util/UiUtils$ScreenSize;->SMALL:Lcom/zendesk/sdk/util/UiUtils$ScreenSize;

    aput-object v2, v0, v1

    sput-object v0, Lcom/zendesk/sdk/util/UiUtils$ScreenSize;->$VALUES:[Lcom/zendesk/sdk/util/UiUtils$ScreenSize;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/zendesk/sdk/util/UiUtils$ScreenSize;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 28
    const-class v0, Lcom/zendesk/sdk/util/UiUtils$ScreenSize;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/zendesk/sdk/util/UiUtils$ScreenSize;

    return-object v0
.end method

.method public static values()[Lcom/zendesk/sdk/util/UiUtils$ScreenSize;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/zendesk/sdk/util/UiUtils$ScreenSize;->$VALUES:[Lcom/zendesk/sdk/util/UiUtils$ScreenSize;

    invoke-virtual {v0}, [Lcom/zendesk/sdk/util/UiUtils$ScreenSize;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/zendesk/sdk/util/UiUtils$ScreenSize;

    return-object v0
.end method

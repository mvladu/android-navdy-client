.class public Lcom/zendesk/sdk/util/LibraryInjector;
.super Ljava/lang/Object;
.source "LibraryInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static injectCachedGson(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/google/gson/Gson;
    .locals 1
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 26
    invoke-static {p0}, Lcom/zendesk/sdk/util/ModuleInjector;->injectCachedLibraryModule(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/util/LibraryModule;

    move-result-object v0

    invoke-virtual {v0}, Lcom/zendesk/sdk/util/LibraryModule;->getGson()Lcom/google/gson/Gson;

    move-result-object v0

    return-object v0
.end method

.method private static injectDefaultZendeskUnauthorizedInterceptor(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lokhttp3/Interceptor;
    .locals 2
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 63
    new-instance v0, Lcom/zendesk/sdk/network/impl/DefaultZendeskUnauthorizedInterceptor;

    .line 64
    invoke-static {p0}, Lcom/zendesk/sdk/storage/StorageInjector;->injectCachedSdkStorage(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/storage/SdkStorage;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/zendesk/sdk/network/impl/DefaultZendeskUnauthorizedInterceptor;-><init>(Lcom/zendesk/sdk/storage/SdkStorage;)V

    return-object v0
.end method

.method public static injectGson(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/google/gson/Gson;
    .locals 4
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 30
    new-instance v0, Lcom/google/gson/GsonBuilder;

    invoke-direct {v0}, Lcom/google/gson/GsonBuilder;-><init>()V

    sget-object v1, Lcom/google/gson/FieldNamingPolicy;->LOWER_CASE_WITH_UNDERSCORES:Lcom/google/gson/FieldNamingPolicy;

    .line 31
    invoke-virtual {v0, v1}, Lcom/google/gson/GsonBuilder;->setFieldNamingPolicy(Lcom/google/gson/FieldNamingPolicy;)Lcom/google/gson/GsonBuilder;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v2, 0x0

    const/16 v3, 0x80

    aput v3, v1, v2

    .line 32
    invoke-virtual {v0, v1}, Lcom/google/gson/GsonBuilder;->excludeFieldsWithModifiers([I)Lcom/google/gson/GsonBuilder;

    move-result-object v0

    .line 33
    invoke-virtual {v0}, Lcom/google/gson/GsonBuilder;->create()Lcom/google/gson/Gson;

    move-result-object v0

    return-object v0
.end method

.method private static injectHttpLoggingInterceptor(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lokhttp3/Interceptor;
    .locals 2
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 49
    new-instance v0, Lokhttp3/logging/HttpLoggingInterceptor;

    invoke-direct {v0}, Lokhttp3/logging/HttpLoggingInterceptor;-><init>()V

    .line 51
    .local v0, "loggingInterceptor":Lokhttp3/logging/HttpLoggingInterceptor;
    invoke-static {}, Lcom/zendesk/logger/Logger;->isLoggable()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lokhttp3/logging/HttpLoggingInterceptor$Level;->HEADERS:Lokhttp3/logging/HttpLoggingInterceptor$Level;

    .line 50
    :goto_0
    invoke-virtual {v0, v1}, Lokhttp3/logging/HttpLoggingInterceptor;->setLevel(Lokhttp3/logging/HttpLoggingInterceptor$Level;)Lokhttp3/logging/HttpLoggingInterceptor;

    .line 52
    return-object v0

    .line 51
    :cond_0
    sget-object v1, Lokhttp3/logging/HttpLoggingInterceptor$Level;->NONE:Lokhttp3/logging/HttpLoggingInterceptor$Level;

    goto :goto_0
.end method

.method public static injectOkHttpClient(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lokhttp3/OkHttpClient;
    .locals 4
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    const-wide/16 v2, 0x1e

    .line 37
    new-instance v0, Lokhttp3/OkHttpClient$Builder;

    invoke-direct {v0}, Lokhttp3/OkHttpClient$Builder;-><init>()V

    .line 38
    invoke-static {p0}, Lcom/zendesk/sdk/util/LibraryInjector;->injectDefaultZendeskUnauthorizedInterceptor(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lokhttp3/Interceptor;

    move-result-object v1

    invoke-virtual {v0, v1}, Lokhttp3/OkHttpClient$Builder;->addInterceptor(Lokhttp3/Interceptor;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 39
    invoke-static {p0}, Lcom/zendesk/sdk/util/LibraryInjector;->injectZendeskRequestInterceptor(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lokhttp3/Interceptor;

    move-result-object v1

    invoke-virtual {v0, v1}, Lokhttp3/OkHttpClient$Builder;->addInterceptor(Lokhttp3/Interceptor;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 40
    invoke-static {p0}, Lcom/zendesk/sdk/util/LibraryInjector;->injectHttpLoggingInterceptor(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lokhttp3/Interceptor;

    move-result-object v1

    invoke-virtual {v0, v1}, Lokhttp3/OkHttpClient$Builder;->addInterceptor(Lokhttp3/Interceptor;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 41
    invoke-virtual {v0, v2, v3, v1}, Lokhttp3/OkHttpClient$Builder;->connectTimeout(JLjava/util/concurrent/TimeUnit;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 42
    invoke-virtual {v0, v2, v3, v1}, Lokhttp3/OkHttpClient$Builder;->readTimeout(JLjava/util/concurrent/TimeUnit;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 43
    invoke-virtual {v0, v2, v3, v1}, Lokhttp3/OkHttpClient$Builder;->writeTimeout(JLjava/util/concurrent/TimeUnit;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 44
    invoke-static {p0}, Lcom/zendesk/sdk/util/BaseInjector;->injectConnectionSpec(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lokhttp3/OkHttpClient$Builder;->connectionSpecs(Ljava/util/List;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 45
    invoke-virtual {v0}, Lokhttp3/OkHttpClient$Builder;->build()Lokhttp3/OkHttpClient;

    move-result-object v0

    return-object v0
.end method

.method private static injectZendeskRequestInterceptor(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lokhttp3/Interceptor;
    .locals 3
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 56
    new-instance v0, Lcom/zendesk/sdk/network/impl/ZendeskRequestInterceptor;

    .line 57
    invoke-static {p0}, Lcom/zendesk/sdk/util/BaseInjector;->injectOAuthToken(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Ljava/lang/String;

    move-result-object v1

    .line 58
    invoke-static {p0}, Lcom/zendesk/sdk/util/BaseInjector;->injectUserAgentHeader(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/zendesk/sdk/network/impl/ZendeskRequestInterceptor;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

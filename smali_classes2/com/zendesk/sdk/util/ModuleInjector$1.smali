.class final Lcom/zendesk/sdk/util/ModuleInjector$1;
.super Ljava/lang/Object;
.source "ModuleInjector.java"

# interfaces
.implements Lcom/zendesk/sdk/util/DependencyProvider;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/util/ModuleInjector;->injectCachedLibraryModule(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/util/LibraryModule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/zendesk/sdk/util/DependencyProvider",
        "<",
        "Lcom/zendesk/sdk/util/LibraryModule;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$applicationScope:Lcom/zendesk/sdk/network/impl/ApplicationScope;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/network/impl/ApplicationScope;)V
    .locals 0

    .prologue
    .line 17
    iput-object p1, p0, Lcom/zendesk/sdk/util/ModuleInjector$1;->val$applicationScope:Lcom/zendesk/sdk/network/impl/ApplicationScope;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public provideDependency()Lcom/zendesk/sdk/util/LibraryModule;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 21
    iget-object v0, p0, Lcom/zendesk/sdk/util/ModuleInjector$1;->val$applicationScope:Lcom/zendesk/sdk/network/impl/ApplicationScope;

    invoke-static {v0}, Lcom/zendesk/sdk/util/ModuleInjector;->access$000(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/util/LibraryModule;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic provideDependency()Ljava/lang/Object;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 17
    invoke-virtual {p0}, Lcom/zendesk/sdk/util/ModuleInjector$1;->provideDependency()Lcom/zendesk/sdk/util/LibraryModule;

    move-result-object v0

    return-object v0
.end method

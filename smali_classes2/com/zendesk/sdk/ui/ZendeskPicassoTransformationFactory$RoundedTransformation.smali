.class Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory$RoundedTransformation;
.super Ljava/lang/Object;
.source "ZendeskPicassoTransformationFactory.java"

# interfaces
.implements Lcom/squareup/picasso/Transformation;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RoundedTransformation"
.end annotation


# instance fields
.field private final margin:I

.field private final radius:I

.field final synthetic this$0:Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory;


# direct methods
.method public constructor <init>(Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory;II)V
    .locals 0
    .param p2, "radius"    # I
    .param p3, "margin"    # I

    .prologue
    .line 122
    iput-object p1, p0, Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory$RoundedTransformation;->this$0:Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 123
    iput p2, p0, Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory$RoundedTransformation;->radius:I

    .line 124
    iput p3, p0, Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory$RoundedTransformation;->margin:I

    .line 125
    return-void
.end method


# virtual methods
.method public key()Ljava/lang/String;
    .locals 5

    .prologue
    .line 146
    const-string v0, "rounded-%s-%s"

    .line 148
    .local v0, "keyFormat":Ljava/lang/String;
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory$RoundedTransformation;->radius:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget v4, p0, Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory$RoundedTransformation;->margin:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v0, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public transform(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 9
    .param p1, "source"    # Landroid/graphics/Bitmap;

    .prologue
    .line 129
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 130
    .local v2, "paint":Landroid/graphics/Paint;
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 131
    new-instance v3, Landroid/graphics/BitmapShader;

    sget-object v4, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    sget-object v5, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    invoke-direct {v3, p1, v4, v5}, Landroid/graphics/BitmapShader;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 133
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 134
    .local v1, "output":Landroid/graphics/Bitmap;
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 135
    .local v0, "canvas":Landroid/graphics/Canvas;
    new-instance v3, Landroid/graphics/RectF;

    iget v4, p0, Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory$RoundedTransformation;->margin:I

    int-to-float v4, v4

    iget v5, p0, Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory$RoundedTransformation;->margin:I

    int-to-float v5, v5

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    iget v7, p0, Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory$RoundedTransformation;->margin:I

    sub-int/2addr v6, v7

    int-to-float v6, v6

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    iget v8, p0, Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory$RoundedTransformation;->margin:I

    sub-int/2addr v7, v8

    int-to-float v7, v7

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/graphics/RectF;-><init>(FFFF)V

    iget v4, p0, Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory$RoundedTransformation;->radius:I

    int-to-float v4, v4

    iget v5, p0, Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory$RoundedTransformation;->radius:I

    int-to-float v5, v5

    invoke-virtual {v0, v3, v4, v5, v2}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 137
    if-eq p1, v1, :cond_0

    .line 138
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    .line 141
    :cond_0
    return-object v1
.end method

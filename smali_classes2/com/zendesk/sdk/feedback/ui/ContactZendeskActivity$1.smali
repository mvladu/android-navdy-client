.class Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity$1;
.super Ljava/lang/Object;
.source "ContactZendeskActivity.java"

# interfaces
.implements Lcom/zendesk/sdk/network/SubmissionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity;

    .prologue
    .line 194
    iput-object p1, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity$1;->this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSubmissionCancel()V
    .locals 0

    .prologue
    .line 209
    return-void
.end method

.method public onSubmissionCompleted()V
    .locals 3

    .prologue
    .line 202
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity$1;->this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity;

    const/4 v1, -0x1

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity;->setResult(ILandroid/content/Intent;)V

    .line 203
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity$1;->this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity;

    invoke-virtual {v0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity;->finish()V

    .line 204
    return-void
.end method

.method public onSubmissionError(Lcom/zendesk/service/ErrorResponse;)V
    .locals 3
    .param p1, "errorResponse"    # Lcom/zendesk/service/ErrorResponse;

    .prologue
    .line 213
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity$1;->this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity$1;->this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity;

    invoke-static {v2, p1}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity;->access$000(Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity;Lcom/zendesk/service/ErrorResponse;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity;->setResult(ILandroid/content/Intent;)V

    .line 214
    return-void
.end method

.method public onSubmissionStarted()V
    .locals 0

    .prologue
    .line 198
    return-void
.end method

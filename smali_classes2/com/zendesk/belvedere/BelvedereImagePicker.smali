.class Lcom/zendesk/belvedere/BelvedereImagePicker;
.super Ljava/lang/Object;
.source "BelvedereImagePicker.java"


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "BelvedereImagePicker"


# instance fields
.field private final belvedereConfig:Lcom/zendesk/belvedere/BelvedereConfig;

.field private final belvedereStorage:Lcom/zendesk/belvedere/BelvedereStorage;

.field private final cameraImages:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/zendesk/belvedere/BelvedereResult;",
            ">;"
        }
    .end annotation
.end field

.field private final log:Lcom/zendesk/belvedere/BelvedereLogger;


# direct methods
.method constructor <init>(Lcom/zendesk/belvedere/BelvedereConfig;Lcom/zendesk/belvedere/BelvedereStorage;)V
    .locals 1
    .param p1, "belvedereConfig"    # Lcom/zendesk/belvedere/BelvedereConfig;
    .param p2, "belvedereStorage"    # Lcom/zendesk/belvedere/BelvedereStorage;

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/zendesk/belvedere/BelvedereImagePicker;->cameraImages:Ljava/util/Map;

    .line 44
    iput-object p1, p0, Lcom/zendesk/belvedere/BelvedereImagePicker;->belvedereConfig:Lcom/zendesk/belvedere/BelvedereConfig;

    .line 45
    iput-object p2, p0, Lcom/zendesk/belvedere/BelvedereImagePicker;->belvedereStorage:Lcom/zendesk/belvedere/BelvedereStorage;

    .line 46
    invoke-virtual {p1}, Lcom/zendesk/belvedere/BelvedereConfig;->getBelvedereLogger()Lcom/zendesk/belvedere/BelvedereLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/belvedere/BelvedereImagePicker;->log:Lcom/zendesk/belvedere/BelvedereLogger;

    .line 47
    return-void
.end method

.method private canPickImageFromCamera(Landroid/content/Context;)Z
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 136
    invoke-direct {p0, p1}, Lcom/zendesk/belvedere/BelvedereImagePicker;->hasCamera(Landroid/content/Context;)Z

    move-result v0

    .line 138
    .local v0, "hasCamera":Z
    if-eqz v0, :cond_3

    .line 140
    const-string v5, "android.permission.CAMERA"

    invoke-direct {p0, p1, v5}, Lcom/zendesk/belvedere/BelvedereImagePicker;->hasPermissionInManifest(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    .line 141
    .local v2, "hasCameraPermissionInManifest":Z
    if-eqz v2, :cond_0

    .line 143
    const-string v5, "android.permission.CAMERA"

    .line 144
    invoke-static {p1, v5}, Landroid/support/v4/content/ContextCompat;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_1

    move v1, v3

    .line 145
    .local v1, "hasCameraPermissionGranted":Z
    :goto_0
    if-eqz v1, :cond_2

    .line 161
    .end local v1    # "hasCameraPermissionGranted":Z
    .end local v2    # "hasCameraPermissionInManifest":Z
    :cond_0
    :goto_1
    return v3

    .restart local v2    # "hasCameraPermissionInManifest":Z
    :cond_1
    move v1, v4

    .line 144
    goto :goto_0

    .line 148
    .restart local v1    # "hasCameraPermissionGranted":Z
    :cond_2
    iget-object v3, p0, Lcom/zendesk/belvedere/BelvedereImagePicker;->log:Lcom/zendesk/belvedere/BelvedereLogger;

    const-string v5, "BelvedereImagePicker"

    const-string v6, "Found Camera permission declared in AndroidManifest.xml and the user hasn\'t granted that permission. Not doing any further efforts to acquire that permission."

    invoke-interface {v3, v5, v6}, Lcom/zendesk/belvedere/BelvedereLogger;->w(Ljava/lang/String;Ljava/lang/String;)V

    .end local v1    # "hasCameraPermissionGranted":Z
    .end local v2    # "hasCameraPermissionInManifest":Z
    :cond_3
    move v3, v4

    .line 161
    goto :goto_1
.end method

.method private extractUrisFromIntent(Landroid/content/Intent;)Ljava/util/List;
    .locals 7
    .param p1, "intent"    # Landroid/content/Intent;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 331
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 337
    .local v2, "images":Ljava/util/List;, "Ljava/util/List<Landroid/net/Uri;>;"
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x10

    if-lt v5, v6, :cond_1

    invoke-virtual {p1}, Landroid/content/Intent;->getClipData()Landroid/content/ClipData;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 338
    invoke-virtual {p1}, Landroid/content/Intent;->getClipData()Landroid/content/ClipData;

    move-result-object v0

    .line 340
    .local v0, "clipData":Landroid/content/ClipData;
    const/4 v1, 0x0

    .local v1, "i":I
    invoke-virtual {v0}, Landroid/content/ClipData;->getItemCount()I

    move-result v4

    .local v4, "itemCount":I
    :goto_0
    if-ge v1, v4, :cond_2

    .line 341
    invoke-virtual {v0, v1}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v3

    .line 343
    .local v3, "itemAt":Landroid/content/ClipData$Item;
    invoke-virtual {v3}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 344
    invoke-virtual {v3}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 340
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 348
    .end local v0    # "clipData":Landroid/content/ClipData;
    .end local v1    # "i":I
    .end local v3    # "itemAt":Landroid/content/ClipData$Item;
    .end local v4    # "itemCount":I
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 349
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 352
    :cond_2
    return-object v2
.end method

.method private getCameraIntent(Landroid/content/Context;)Lcom/zendesk/belvedere/BelvedereIntent;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 110
    invoke-direct {p0, p1}, Lcom/zendesk/belvedere/BelvedereImagePicker;->canPickImageFromCamera(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 111
    invoke-direct {p0, p1}, Lcom/zendesk/belvedere/BelvedereImagePicker;->pickImageFromCameraInternal(Landroid/content/Context;)Lcom/zendesk/belvedere/BelvedereIntent;

    move-result-object v0

    .line 114
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getGalleryIntent()Landroid/content/Intent;
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 417
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-lt v1, v2, :cond_1

    .line 418
    iget-object v1, p0, Lcom/zendesk/belvedere/BelvedereImagePicker;->log:Lcom/zendesk/belvedere/BelvedereLogger;

    const-string v2, "BelvedereImagePicker"

    const-string v3, "Gallery Intent, using \'ACTION_OPEN_DOCUMENT\'"

    invoke-interface {v1, v2, v3}, Lcom/zendesk/belvedere/BelvedereLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 419
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.OPEN_DOCUMENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 425
    .local v0, "intent":Landroid/content/Intent;
    :goto_0
    iget-object v1, p0, Lcom/zendesk/belvedere/BelvedereImagePicker;->belvedereConfig:Lcom/zendesk/belvedere/BelvedereConfig;

    invoke-virtual {v1}, Lcom/zendesk/belvedere/BelvedereConfig;->getContentType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 426
    const-string v1, "android.intent.category.OPENABLE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 428
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x12

    if-lt v1, v2, :cond_0

    .line 430
    const-string v1, "android.intent.extra.ALLOW_MULTIPLE"

    iget-object v2, p0, Lcom/zendesk/belvedere/BelvedereImagePicker;->belvedereConfig:Lcom/zendesk/belvedere/BelvedereConfig;

    invoke-virtual {v2}, Lcom/zendesk/belvedere/BelvedereConfig;->allowMultiple()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 433
    :cond_0
    return-object v0

    .line 421
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_1
    iget-object v1, p0, Lcom/zendesk/belvedere/BelvedereImagePicker;->log:Lcom/zendesk/belvedere/BelvedereLogger;

    const-string v2, "BelvedereImagePicker"

    const-string v3, "Gallery Intent, using \'ACTION_GET_CONTENT\'"

    invoke-interface {v1, v2, v3}, Lcom/zendesk/belvedere/BelvedereLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 422
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.GET_CONTENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .restart local v0    # "intent":Landroid/content/Intent;
    goto :goto_0
.end method

.method private hasCamera(Landroid/content/Context;)Z
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 180
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 181
    .local v2, "mockIntent":Landroid/content/Intent;
    const-string v6, "android.media.action.IMAGE_CAPTURE"

    invoke-virtual {v2, v6}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 184
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 185
    .local v3, "packageManager":Landroid/content/pm/PackageManager;
    const-string v6, "android.hardware.camera"

    invoke-virtual {v3, v6}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    const-string v6, "android.hardware.camera.front"

    .line 186
    invoke-virtual {v3, v6}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    :cond_0
    move v0, v5

    .line 188
    .local v0, "hasCamera":Z
    :goto_0
    invoke-direct {p0, v2, p1}, Lcom/zendesk/belvedere/BelvedereImagePicker;->isIntentResolvable(Landroid/content/Intent;Landroid/content/Context;)Z

    move-result v1

    .line 190
    .local v1, "hasCameraApp":Z
    iget-object v6, p0, Lcom/zendesk/belvedere/BelvedereImagePicker;->log:Lcom/zendesk/belvedere/BelvedereLogger;

    const-string v7, "BelvedereImagePicker"

    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v9, "Camera present: %b, Camera App present: %b"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    aput-object v11, v10, v4

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    aput-object v11, v10, v5

    invoke-static {v8, v9, v10}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v6, v7, v8}, Lcom/zendesk/belvedere/BelvedereLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    :goto_1
    return v5

    .end local v0    # "hasCamera":Z
    .end local v1    # "hasCameraApp":Z
    :cond_1
    move v0, v4

    .line 186
    goto :goto_0

    .restart local v0    # "hasCamera":Z
    .restart local v1    # "hasCameraApp":Z
    :cond_2
    move v5, v4

    .line 192
    goto :goto_1
.end method

.method private hasGalleryApp(Landroid/content/Context;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 209
    invoke-direct {p0}, Lcom/zendesk/belvedere/BelvedereImagePicker;->getGalleryIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/zendesk/belvedere/BelvedereImagePicker;->isIntentResolvable(Landroid/content/Intent;Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method private hasPermissionInManifest(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "permissionName"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x0

    .line 445
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    .line 447
    .local v4, "packageName":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    const/16 v7, 0x1000

    .line 448
    invoke-virtual {v6, v4, v7}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    .line 449
    .local v3, "packageInfo":Landroid/content/pm/PackageInfo;
    iget-object v0, v3, Landroid/content/pm/PackageInfo;->requestedPermissions:[Ljava/lang/String;

    .line 450
    .local v0, "declaredPermissions":[Ljava/lang/String;
    if-eqz v0, :cond_0

    array-length v6, v0

    if-lez v6, :cond_0

    .line 451
    array-length v7, v0

    move v6, v5

    :goto_0
    if-ge v6, v7, :cond_0

    aget-object v2, v0, v6

    .line 452
    .local v2, "p":Ljava/lang/String;
    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v8

    if-eqz v8, :cond_1

    .line 453
    const/4 v5, 0x1

    .line 461
    .end local v0    # "declaredPermissions":[Ljava/lang/String;
    .end local v2    # "p":Ljava/lang/String;
    .end local v3    # "packageInfo":Landroid/content/pm/PackageInfo;
    :cond_0
    :goto_1
    return v5

    .line 451
    .restart local v0    # "declaredPermissions":[Ljava/lang/String;
    .restart local v2    # "p":Ljava/lang/String;
    .restart local v3    # "packageInfo":Landroid/content/pm/PackageInfo;
    :cond_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 457
    .end local v0    # "declaredPermissions":[Ljava/lang/String;
    .end local v2    # "p":Ljava/lang/String;
    .end local v3    # "packageInfo":Landroid/content/pm/PackageInfo;
    :catch_0
    move-exception v1

    .line 458
    .local v1, "e":Ljava/lang/Exception;
    iget-object v6, p0, Lcom/zendesk/belvedere/BelvedereImagePicker;->log:Lcom/zendesk/belvedere/BelvedereLogger;

    const-string v7, "BelvedereImagePicker"

    const-string v8, "Not able to find permissions in manifest"

    invoke-interface {v6, v7, v8, v1}, Lcom/zendesk/belvedere/BelvedereLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method private isIntentResolvable(Landroid/content/Intent;Landroid/content/Context;)Z
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "context"    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 306
    invoke-virtual {p2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private pickImageFromCameraInternal(Landroid/content/Context;)Lcom/zendesk/belvedere/BelvedereIntent;
    .locals 14
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v13, 0x3

    .line 369
    iget-object v7, p0, Lcom/zendesk/belvedere/BelvedereImagePicker;->cameraImages:Ljava/util/Map;

    invoke-interface {v7}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    .line 371
    .local v2, "integers":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    iget-object v7, p0, Lcom/zendesk/belvedere/BelvedereImagePicker;->belvedereConfig:Lcom/zendesk/belvedere/BelvedereConfig;

    invoke-virtual {v7}, Lcom/zendesk/belvedere/BelvedereConfig;->getCameraRequestCodeEnd()I

    move-result v4

    .line 372
    .local v4, "requestId":I
    iget-object v7, p0, Lcom/zendesk/belvedere/BelvedereImagePicker;->belvedereConfig:Lcom/zendesk/belvedere/BelvedereConfig;

    invoke-virtual {v7}, Lcom/zendesk/belvedere/BelvedereConfig;->getCameraRequestCodeStart()I

    move-result v0

    .local v0, "i":I
    :goto_0
    iget-object v7, p0, Lcom/zendesk/belvedere/BelvedereImagePicker;->belvedereConfig:Lcom/zendesk/belvedere/BelvedereConfig;

    invoke-virtual {v7}, Lcom/zendesk/belvedere/BelvedereConfig;->getCameraRequestCodeEnd()I

    move-result v7

    if-ge v0, v7, :cond_0

    .line 373
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v2, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 374
    move v4, v0

    .line 379
    :cond_0
    iget-object v7, p0, Lcom/zendesk/belvedere/BelvedereImagePicker;->belvedereStorage:Lcom/zendesk/belvedere/BelvedereStorage;

    invoke-virtual {v7, p1}, Lcom/zendesk/belvedere/BelvedereStorage;->getFileForCamera(Landroid/content/Context;)Ljava/io/File;

    move-result-object v1

    .line 381
    .local v1, "imagePath":Ljava/io/File;
    if-nez v1, :cond_2

    .line 382
    iget-object v7, p0, Lcom/zendesk/belvedere/BelvedereImagePicker;->log:Lcom/zendesk/belvedere/BelvedereLogger;

    const-string v8, "BelvedereImagePicker"

    const-string v9, "Camera Intent. Image path is null. There\'s something wrong with the storage."

    invoke-interface {v7, v8, v9}, Lcom/zendesk/belvedere/BelvedereLogger;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 402
    :goto_1
    return-object v6

    .line 372
    .end local v1    # "imagePath":Ljava/io/File;
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 386
    .restart local v1    # "imagePath":Ljava/io/File;
    :cond_2
    iget-object v7, p0, Lcom/zendesk/belvedere/BelvedereImagePicker;->belvedereStorage:Lcom/zendesk/belvedere/BelvedereStorage;

    invoke-virtual {v7, p1, v1}, Lcom/zendesk/belvedere/BelvedereStorage;->getFileProviderUri(Landroid/content/Context;Ljava/io/File;)Landroid/net/Uri;

    move-result-object v5

    .line 388
    .local v5, "uriForFile":Landroid/net/Uri;
    if-nez v5, :cond_3

    .line 389
    iget-object v7, p0, Lcom/zendesk/belvedere/BelvedereImagePicker;->log:Lcom/zendesk/belvedere/BelvedereLogger;

    const-string v8, "BelvedereImagePicker"

    const-string v9, "Camera Intent: Uri to file is null. There\'s something wrong with the storage or FileProvider configuration."

    invoke-interface {v7, v8, v9}, Lcom/zendesk/belvedere/BelvedereLogger;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 393
    :cond_3
    iget-object v6, p0, Lcom/zendesk/belvedere/BelvedereImagePicker;->cameraImages:Ljava/util/Map;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    new-instance v8, Lcom/zendesk/belvedere/BelvedereResult;

    invoke-direct {v8, v1, v5}, Lcom/zendesk/belvedere/BelvedereResult;-><init>(Ljava/io/File;Landroid/net/Uri;)V

    invoke-interface {v6, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 395
    iget-object v6, p0, Lcom/zendesk/belvedere/BelvedereImagePicker;->log:Lcom/zendesk/belvedere/BelvedereLogger;

    const-string v7, "BelvedereImagePicker"

    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v9, "Camera Intent: Request Id: %s - File: %s - Uri: %s"

    new-array v10, v13, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    aput-object v1, v10, v11

    const/4 v11, 0x2

    aput-object v5, v10, v11

    invoke-static {v8, v9, v10}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v6, v7, v8}, Lcom/zendesk/belvedere/BelvedereLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 397
    new-instance v3, Landroid/content/Intent;

    const-string v6, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v3, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 398
    .local v3, "intent":Landroid/content/Intent;
    const-string v6, "output"

    invoke-virtual {v3, v6, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 399
    iget-object v6, p0, Lcom/zendesk/belvedere/BelvedereImagePicker;->belvedereStorage:Lcom/zendesk/belvedere/BelvedereStorage;

    invoke-virtual {v6, p1, v3, v5, v13}, Lcom/zendesk/belvedere/BelvedereStorage;->grantPermissionsForUri(Landroid/content/Context;Landroid/content/Intent;Landroid/net/Uri;I)V

    .line 402
    new-instance v6, Lcom/zendesk/belvedere/BelvedereIntent;

    sget-object v7, Lcom/zendesk/belvedere/BelvedereSource;->Camera:Lcom/zendesk/belvedere/BelvedereSource;

    invoke-direct {v6, v3, v4, v7}, Lcom/zendesk/belvedere/BelvedereIntent;-><init>(Landroid/content/Intent;ILcom/zendesk/belvedere/BelvedereSource;)V

    goto :goto_1
.end method


# virtual methods
.method getBelvedereIntents(Landroid/content/Context;)Ljava/util/List;
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/belvedere/BelvedereIntent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 60
    iget-object v4, p0, Lcom/zendesk/belvedere/BelvedereImagePicker;->belvedereConfig:Lcom/zendesk/belvedere/BelvedereConfig;

    invoke-virtual {v4}, Lcom/zendesk/belvedere/BelvedereConfig;->getBelvedereSources()Ljava/util/TreeSet;

    move-result-object v2

    .line 61
    .local v2, "belvedereSources":Ljava/util/TreeSet;, "Ljava/util/TreeSet<Lcom/zendesk/belvedere/BelvedereSource;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 63
    .local v0, "belvedereIntents":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/belvedere/BelvedereIntent;>;"
    invoke-virtual {v2}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/zendesk/belvedere/BelvedereSource;

    .line 64
    .local v1, "belvedereSource":Lcom/zendesk/belvedere/BelvedereSource;
    const/4 v3, 0x0

    .line 66
    .local v3, "intent":Lcom/zendesk/belvedere/BelvedereIntent;
    sget-object v5, Lcom/zendesk/belvedere/BelvedereImagePicker$1;->$SwitchMap$com$zendesk$belvedere$BelvedereSource:[I

    invoke-virtual {v1}, Lcom/zendesk/belvedere/BelvedereSource;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 75
    :goto_1
    if-eqz v3, :cond_0

    .line 76
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 68
    :pswitch_0
    invoke-virtual {p0, p1}, Lcom/zendesk/belvedere/BelvedereImagePicker;->getGalleryIntent(Landroid/content/Context;)Lcom/zendesk/belvedere/BelvedereIntent;

    move-result-object v3

    .line 69
    goto :goto_1

    .line 71
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/zendesk/belvedere/BelvedereImagePicker;->getCameraIntent(Landroid/content/Context;)Lcom/zendesk/belvedere/BelvedereIntent;

    move-result-object v3

    goto :goto_1

    .line 80
    .end local v1    # "belvedereSource":Lcom/zendesk/belvedere/BelvedereSource;
    .end local v3    # "intent":Lcom/zendesk/belvedere/BelvedereIntent;
    :cond_1
    return-object v0

    .line 66
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method getFilesFromActivityOnResult(Landroid/content/Context;IILandroid/content/Intent;Lcom/zendesk/belvedere/BelvedereCallback;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "requestCode"    # I
    .param p3, "resultCode"    # I
    .param p4, "data"    # Landroid/content/Intent;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p5    # Lcom/zendesk/belvedere/BelvedereCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "II",
            "Landroid/content/Intent;",
            "Lcom/zendesk/belvedere/BelvedereCallback",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/belvedere/BelvedereResult;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 223
    .local p5, "callback":Lcom/zendesk/belvedere/BelvedereCallback;, "Lcom/zendesk/belvedere/BelvedereCallback<Ljava/util/List<Lcom/zendesk/belvedere/BelvedereResult;>;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 225
    .local v1, "result":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/belvedere/BelvedereResult;>;"
    iget-object v3, p0, Lcom/zendesk/belvedere/BelvedereImagePicker;->belvedereConfig:Lcom/zendesk/belvedere/BelvedereConfig;

    invoke-virtual {v3}, Lcom/zendesk/belvedere/BelvedereConfig;->getGalleryRequestCode()I

    move-result v3

    if-ne p2, v3, :cond_2

    .line 226
    iget-object v4, p0, Lcom/zendesk/belvedere/BelvedereImagePicker;->log:Lcom/zendesk/belvedere/BelvedereLogger;

    const-string v5, "BelvedereImagePicker"

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v7, "Parsing activity result - Gallery - Ok: %s"

    const/4 v3, 0x1

    new-array v8, v3, [Ljava/lang/Object;

    const/4 v9, 0x0

    const/4 v3, -0x1

    if-ne p3, v3, :cond_1

    const/4 v3, 0x1

    :goto_0
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v8, v9

    invoke-static {v6, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v4, v5, v3}, Lcom/zendesk/belvedere/BelvedereLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    const/4 v3, -0x1

    if-ne p3, v3, :cond_4

    .line 229
    invoke-direct {p0, p4}, Lcom/zendesk/belvedere/BelvedereImagePicker;->extractUrisFromIntent(Landroid/content/Intent;)Ljava/util/List;

    move-result-object v2

    .line 230
    .local v2, "uris":Ljava/util/List;, "Ljava/util/List<Landroid/net/Uri;>;"
    iget-object v3, p0, Lcom/zendesk/belvedere/BelvedereImagePicker;->log:Lcom/zendesk/belvedere/BelvedereLogger;

    const-string v4, "BelvedereImagePicker"

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "Number of items received from gallery: %s"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lcom/zendesk/belvedere/BelvedereLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    new-instance v3, Lcom/zendesk/belvedere/BelvedereResolveUriTask;

    iget-object v4, p0, Lcom/zendesk/belvedere/BelvedereImagePicker;->log:Lcom/zendesk/belvedere/BelvedereLogger;

    iget-object v5, p0, Lcom/zendesk/belvedere/BelvedereImagePicker;->belvedereStorage:Lcom/zendesk/belvedere/BelvedereStorage;

    invoke-direct {v3, p1, v4, v5, p5}, Lcom/zendesk/belvedere/BelvedereResolveUriTask;-><init>(Landroid/content/Context;Lcom/zendesk/belvedere/BelvedereLogger;Lcom/zendesk/belvedere/BelvedereStorage;Lcom/zendesk/belvedere/BelvedereCallback;)V

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    new-array v4, v4, [Landroid/net/Uri;

    invoke-interface {v2, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/zendesk/belvedere/BelvedereResolveUriTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 253
    .end local v2    # "uris":Ljava/util/List;, "Ljava/util/List<Landroid/net/Uri;>;"
    :cond_0
    :goto_1
    return-void

    .line 226
    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    .line 235
    :cond_2
    iget-object v3, p0, Lcom/zendesk/belvedere/BelvedereImagePicker;->cameraImages:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 236
    iget-object v4, p0, Lcom/zendesk/belvedere/BelvedereImagePicker;->log:Lcom/zendesk/belvedere/BelvedereLogger;

    const-string v5, "BelvedereImagePicker"

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v7, "Parsing activity result - Camera - Ok: %s"

    const/4 v3, 0x1

    new-array v8, v3, [Ljava/lang/Object;

    const/4 v9, 0x0

    const/4 v3, -0x1

    if-ne p3, v3, :cond_5

    const/4 v3, 0x1

    :goto_2
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v8, v9

    invoke-static {v6, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v4, v5, v3}, Lcom/zendesk/belvedere/BelvedereLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    iget-object v3, p0, Lcom/zendesk/belvedere/BelvedereImagePicker;->cameraImages:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/zendesk/belvedere/BelvedereResult;

    .line 239
    .local v0, "belvedereResult":Lcom/zendesk/belvedere/BelvedereResult;
    iget-object v3, p0, Lcom/zendesk/belvedere/BelvedereImagePicker;->belvedereStorage:Lcom/zendesk/belvedere/BelvedereStorage;

    invoke-virtual {v0}, Lcom/zendesk/belvedere/BelvedereResult;->getUri()Landroid/net/Uri;

    move-result-object v4

    const/4 v5, 0x3

    invoke-virtual {v3, p1, v4, v5}, Lcom/zendesk/belvedere/BelvedereStorage;->revokePermissionsFromUri(Landroid/content/Context;Landroid/net/Uri;I)V

    .line 242
    const/4 v3, -0x1

    if-ne p3, v3, :cond_3

    .line 243
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 244
    iget-object v3, p0, Lcom/zendesk/belvedere/BelvedereImagePicker;->log:Lcom/zendesk/belvedere/BelvedereLogger;

    const-string v4, "BelvedereImagePicker"

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "Image from camera: %s"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-virtual {v0}, Lcom/zendesk/belvedere/BelvedereResult;->getFile()Ljava/io/File;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lcom/zendesk/belvedere/BelvedereLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    :cond_3
    iget-object v3, p0, Lcom/zendesk/belvedere/BelvedereImagePicker;->cameraImages:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 250
    .end local v0    # "belvedereResult":Lcom/zendesk/belvedere/BelvedereResult;
    :cond_4
    if-eqz p5, :cond_0

    .line 251
    invoke-virtual {p5, v1}, Lcom/zendesk/belvedere/BelvedereCallback;->internalSuccess(Ljava/lang/Object;)V

    goto :goto_1

    .line 236
    :cond_5
    const/4 v3, 0x0

    goto :goto_2
.end method

.method getGalleryIntent(Landroid/content/Context;)Lcom/zendesk/belvedere/BelvedereIntent;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 94
    invoke-direct {p0, p1}, Lcom/zendesk/belvedere/BelvedereImagePicker;->hasGalleryApp(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 95
    new-instance v0, Lcom/zendesk/belvedere/BelvedereIntent;

    invoke-direct {p0}, Lcom/zendesk/belvedere/BelvedereImagePicker;->getGalleryIntent()Landroid/content/Intent;

    move-result-object v1

    iget-object v2, p0, Lcom/zendesk/belvedere/BelvedereImagePicker;->belvedereConfig:Lcom/zendesk/belvedere/BelvedereConfig;

    invoke-virtual {v2}, Lcom/zendesk/belvedere/BelvedereConfig;->getGalleryRequestCode()I

    move-result v2

    sget-object v3, Lcom/zendesk/belvedere/BelvedereSource;->Gallery:Lcom/zendesk/belvedere/BelvedereSource;

    invoke-direct {v0, v1, v2, v3}, Lcom/zendesk/belvedere/BelvedereIntent;-><init>(Landroid/content/Intent;ILcom/zendesk/belvedere/BelvedereSource;)V

    .line 97
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFunctionalityAvailable(Lcom/zendesk/belvedere/BelvedereSource;Landroid/content/Context;)Z
    .locals 4
    .param p1, "source"    # Lcom/zendesk/belvedere/BelvedereSource;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "context"    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 280
    iget-object v2, p0, Lcom/zendesk/belvedere/BelvedereImagePicker;->belvedereConfig:Lcom/zendesk/belvedere/BelvedereConfig;

    invoke-virtual {v2}, Lcom/zendesk/belvedere/BelvedereConfig;->getBelvedereSources()Ljava/util/TreeSet;

    move-result-object v0

    .line 282
    .local v0, "belvedereSources":Ljava/util/TreeSet;, "Ljava/util/TreeSet<Lcom/zendesk/belvedere/BelvedereSource;>;"
    invoke-virtual {v0, p1}, Ljava/util/TreeSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 292
    :goto_0
    return v1

    .line 286
    :cond_0
    sget-object v2, Lcom/zendesk/belvedere/BelvedereImagePicker$1;->$SwitchMap$com$zendesk$belvedere$BelvedereSource:[I

    invoke-virtual {p1}, Lcom/zendesk/belvedere/BelvedereSource;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 290
    :pswitch_0
    invoke-direct {p0, p2}, Lcom/zendesk/belvedere/BelvedereImagePicker;->hasGalleryApp(Landroid/content/Context;)Z

    move-result v1

    goto :goto_0

    .line 288
    :pswitch_1
    invoke-direct {p0, p2}, Lcom/zendesk/belvedere/BelvedereImagePicker;->canPickImageFromCamera(Landroid/content/Context;)Z

    move-result v1

    goto :goto_0

    .line 286
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public oneOrMoreSourceAvailable(Landroid/content/Context;)Z
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 264
    invoke-static {}, Lcom/zendesk/belvedere/BelvedereSource;->values()[Lcom/zendesk/belvedere/BelvedereSource;

    move-result-object v3

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v0, v3, v2

    .line 265
    .local v0, "s":Lcom/zendesk/belvedere/BelvedereSource;
    invoke-virtual {p0, v0, p1}, Lcom/zendesk/belvedere/BelvedereImagePicker;->isFunctionalityAvailable(Lcom/zendesk/belvedere/BelvedereSource;Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 266
    const/4 v1, 0x1

    .line 269
    .end local v0    # "s":Lcom/zendesk/belvedere/BelvedereSource;
    :cond_0
    return v1

    .line 264
    .restart local v0    # "s":Lcom/zendesk/belvedere/BelvedereSource;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

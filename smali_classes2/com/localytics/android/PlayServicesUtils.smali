.class Lcom/localytics/android/PlayServicesUtils;
.super Ljava/lang/Object;
.source "PlayServicesUtils.java"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static isAdvertisingAvailable()Z
    .locals 2

    .prologue
    .line 22
    :try_start_0
    const-string v1, "com.google.android.gms.ads.identifier.AdvertisingIdClient"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 23
    const/4 v1, 0x1

    .line 27
    .local v0, "e":Ljava/lang/Exception;
    :goto_0
    return v1

    .line 25
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_0
    move-exception v0

    .line 27
    .restart local v0    # "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method static isLocationAvailable()Z
    .locals 2

    .prologue
    .line 9
    :try_start_0
    const-string v1, "com.google.android.gms.location.LocationServices"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 10
    const/4 v1, 0x1

    .line 14
    .local v0, "e":Ljava/lang/Exception;
    :goto_0
    return v1

    .line 12
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_0
    move-exception v0

    .line 14
    .restart local v0    # "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    goto :goto_0
.end method

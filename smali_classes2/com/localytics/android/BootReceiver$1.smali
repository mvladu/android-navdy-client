.class Lcom/localytics/android/BootReceiver$1;
.super Landroid/os/AsyncTask;
.source "BootReceiver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/localytics/android/BootReceiver;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/localytics/android/BootReceiver;


# direct methods
.method constructor <init>(Lcom/localytics/android/BootReceiver;)V
    .locals 0

    .prologue
    .line 26
    iput-object p1, p0, Lcom/localytics/android/BootReceiver$1;->this$0:Lcom/localytics/android/BootReceiver;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 26
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/localytics/android/BootReceiver$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 3
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 32
    :try_start_0
    invoke-static {}, Lcom/localytics/android/Localytics;->isLocationMonitoringEnabled()Z

    move-result v1

    .line 33
    .local v1, "locationMonitoringEnabled":Z
    if-eqz v1, :cond_0

    .line 35
    const/4 v2, 0x1

    invoke-static {v2}, Lcom/localytics/android/Localytics;->setLocationMonitoringEnabled(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 43
    .end local v1    # "locationMonitoringEnabled":Z
    :cond_0
    :goto_0
    const/4 v2, 0x0

    return-object v2

    .line 38
    :catch_0
    move-exception v0

    .line 40
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "Exception while handling boot receive"

    invoke-static {v2, v0}, Lcom/localytics/android/Localytics$Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

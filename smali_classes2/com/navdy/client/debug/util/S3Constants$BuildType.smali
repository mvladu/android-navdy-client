.class public final enum Lcom/navdy/client/debug/util/S3Constants$BuildType;
.super Ljava/lang/Enum;
.source "S3Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/debug/util/S3Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "BuildType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/client/debug/util/S3Constants$BuildType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/client/debug/util/S3Constants$BuildType;

.field public static final enum eng:Lcom/navdy/client/debug/util/S3Constants$BuildType;

.field public static final enum user:Lcom/navdy/client/debug/util/S3Constants$BuildType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 29
    new-instance v0, Lcom/navdy/client/debug/util/S3Constants$BuildType;

    const-string v1, "user"

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/debug/util/S3Constants$BuildType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/debug/util/S3Constants$BuildType;->user:Lcom/navdy/client/debug/util/S3Constants$BuildType;

    .line 30
    new-instance v0, Lcom/navdy/client/debug/util/S3Constants$BuildType;

    const-string v1, "eng"

    invoke-direct {v0, v1, v3}, Lcom/navdy/client/debug/util/S3Constants$BuildType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/debug/util/S3Constants$BuildType;->eng:Lcom/navdy/client/debug/util/S3Constants$BuildType;

    .line 28
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/navdy/client/debug/util/S3Constants$BuildType;

    sget-object v1, Lcom/navdy/client/debug/util/S3Constants$BuildType;->user:Lcom/navdy/client/debug/util/S3Constants$BuildType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/client/debug/util/S3Constants$BuildType;->eng:Lcom/navdy/client/debug/util/S3Constants$BuildType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/navdy/client/debug/util/S3Constants$BuildType;->$VALUES:[Lcom/navdy/client/debug/util/S3Constants$BuildType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/client/debug/util/S3Constants$BuildType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 28
    const-class v0, Lcom/navdy/client/debug/util/S3Constants$BuildType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/debug/util/S3Constants$BuildType;

    return-object v0
.end method

.method public static values()[Lcom/navdy/client/debug/util/S3Constants$BuildType;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/navdy/client/debug/util/S3Constants$BuildType;->$VALUES:[Lcom/navdy/client/debug/util/S3Constants$BuildType;

    invoke-virtual {v0}, [Lcom/navdy/client/debug/util/S3Constants$BuildType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/client/debug/util/S3Constants$BuildType;

    return-object v0
.end method

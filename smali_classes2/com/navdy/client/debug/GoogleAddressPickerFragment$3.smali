.class Lcom/navdy/client/debug/GoogleAddressPickerFragment$3;
.super Ljava/lang/Object;
.source "GoogleAddressPickerFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/debug/GoogleAddressPickerFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/debug/GoogleAddressPickerFragment;


# direct methods
.method constructor <init>(Lcom/navdy/client/debug/GoogleAddressPickerFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    .prologue
    .line 283
    iput-object p1, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$3;->this$0:Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2, "view"    # Landroid/view/View;
    .param p3, "i"    # I
    .param p4, "l"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 286
    .local p1, "adapterView":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v1, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$3;->this$0:Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    iget-object v1, v1, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->listView:Landroid/widget/ListView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setVisibility(I)V

    .line 287
    iget-object v1, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$3;->this$0:Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    invoke-virtual {v1, p3}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->showRouteDetails(I)V

    .line 288
    iget-object v1, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$3;->this$0:Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    invoke-static {v1}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->access$500(Lcom/navdy/client/debug/GoogleAddressPickerFragment;)V

    .line 289
    iget-object v1, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$3;->this$0:Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    invoke-static {v1}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->access$600(Lcom/navdy/client/debug/GoogleAddressPickerFragment;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/model/Polyline;

    .line 290
    .local v0, "p":Lcom/google/android/gms/maps/model/Polyline;
    iget-object v1, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$3;->this$0:Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    invoke-virtual {v1}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f0016

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/model/Polyline;->setColor(I)V

    .line 291
    const/high16 v1, 0x40400000    # 3.0f

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/model/Polyline;->setZIndex(F)V

    .line 292
    const/high16 v1, 0x41200000    # 10.0f

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/model/Polyline;->setWidth(F)V

    .line 293
    invoke-static {}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->access$700()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Number of routes : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$3;->this$0:Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    invoke-static {v3}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->access$600(Lcom/navdy/client/debug/GoogleAddressPickerFragment;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 294
    return-void
.end method

.class Lcom/navdy/client/debug/view/S3BrowserFragment$ObjectAdapter;
.super Landroid/widget/ArrayAdapter;
.source "S3BrowserFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/debug/view/S3BrowserFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ObjectAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/debug/view/S3BrowserFragment$ObjectAdapter$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/navdy/client/debug/view/S3BrowserFragment$MyS3Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/debug/view/S3BrowserFragment;


# direct methods
.method public constructor <init>(Lcom/navdy/client/debug/view/S3BrowserFragment;Landroid/content/Context;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 311
    iput-object p1, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$ObjectAdapter;->this$0:Lcom/navdy/client/debug/view/S3BrowserFragment;

    .line 312
    const v0, 0x7f0300b3

    invoke-direct {p0, p2, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 313
    return-void
.end method


# virtual methods
.method public add(Lcom/navdy/client/debug/view/S3BrowserFragment$MyS3Object;)V
    .locals 2
    .param p1, "object"    # Lcom/navdy/client/debug/view/S3BrowserFragment$MyS3Object;

    .prologue
    .line 338
    invoke-virtual {p1}, Lcom/navdy/client/debug/view/S3BrowserFragment$MyS3Object;->getKey()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$ObjectAdapter;->this$0:Lcom/navdy/client/debug/view/S3BrowserFragment;

    invoke-static {v1}, Lcom/navdy/client/debug/view/S3BrowserFragment;->access$300(Lcom/navdy/client/debug/view/S3BrowserFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 339
    invoke-super {p0, p1}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 341
    :cond_0
    return-void
.end method

.method public bridge synthetic add(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 310
    check-cast p1, Lcom/navdy/client/debug/view/S3BrowserFragment$MyS3Object;

    invoke-virtual {p0, p1}, Lcom/navdy/client/debug/view/S3BrowserFragment$ObjectAdapter;->add(Lcom/navdy/client/debug/view/S3BrowserFragment$MyS3Object;)V

    return-void
.end method

.method public addAll(Ljava/util/Collection;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+",
            "Lcom/navdy/client/debug/view/S3BrowserFragment$MyS3Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 344
    .local p1, "collection":Ljava/util/Collection;, "Ljava/util/Collection<+Lcom/navdy/client/debug/view/S3BrowserFragment$MyS3Object;>;"
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/debug/view/S3BrowserFragment$MyS3Object;

    .line 346
    .local v0, "obj":Lcom/navdy/client/debug/view/S3BrowserFragment$MyS3Object;
    invoke-virtual {v0}, Lcom/navdy/client/debug/view/S3BrowserFragment$MyS3Object;->getKey()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$ObjectAdapter;->this$0:Lcom/navdy/client/debug/view/S3BrowserFragment;

    invoke-static {v3}, Lcom/navdy/client/debug/view/S3BrowserFragment;->access$300(Lcom/navdy/client/debug/view/S3BrowserFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 347
    invoke-virtual {p0, v0}, Lcom/navdy/client/debug/view/S3BrowserFragment$ObjectAdapter;->add(Lcom/navdy/client/debug/view/S3BrowserFragment$MyS3Object;)V

    goto :goto_0

    .line 350
    .end local v0    # "obj":Lcom/navdy/client/debug/view/S3BrowserFragment$MyS3Object;
    :cond_1
    return-void
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "pos"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v4, 0x0

    .line 318
    if-nez p2, :cond_0

    .line 319
    invoke-virtual {p0}, Lcom/navdy/client/debug/view/S3BrowserFragment$ObjectAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f0300b3

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 321
    new-instance v0, Lcom/navdy/client/debug/view/S3BrowserFragment$ObjectAdapter$ViewHolder;

    invoke-direct {v0, p0, p2, v4}, Lcom/navdy/client/debug/view/S3BrowserFragment$ObjectAdapter$ViewHolder;-><init>(Lcom/navdy/client/debug/view/S3BrowserFragment$ObjectAdapter;Landroid/view/View;Lcom/navdy/client/debug/view/S3BrowserFragment$1;)V

    .line 322
    .local v0, "holder":Lcom/navdy/client/debug/view/S3BrowserFragment$ObjectAdapter$ViewHolder;
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 326
    :goto_0
    invoke-virtual {p0, p1}, Lcom/navdy/client/debug/view/S3BrowserFragment$ObjectAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/client/debug/view/S3BrowserFragment$MyS3Object;

    .line 327
    .local v1, "object":Lcom/navdy/client/debug/view/S3BrowserFragment$MyS3Object;
    invoke-static {v0}, Lcom/navdy/client/debug/view/S3BrowserFragment$ObjectAdapter$ViewHolder;->access$600(Lcom/navdy/client/debug/view/S3BrowserFragment$ObjectAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v1}, Lcom/navdy/client/debug/view/S3BrowserFragment$MyS3Object;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 328
    invoke-virtual {v1}, Lcom/navdy/client/debug/view/S3BrowserFragment$MyS3Object;->isFolder()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 329
    invoke-static {v0}, Lcom/navdy/client/debug/view/S3BrowserFragment$ObjectAdapter$ViewHolder;->access$700(Lcom/navdy/client/debug/view/S3BrowserFragment$ObjectAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 333
    :goto_1
    return-object p2

    .line 324
    .end local v0    # "holder":Lcom/navdy/client/debug/view/S3BrowserFragment$ObjectAdapter$ViewHolder;
    .end local v1    # "object":Lcom/navdy/client/debug/view/S3BrowserFragment$MyS3Object;
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/debug/view/S3BrowserFragment$ObjectAdapter$ViewHolder;

    .restart local v0    # "holder":Lcom/navdy/client/debug/view/S3BrowserFragment$ObjectAdapter$ViewHolder;
    goto :goto_0

    .line 331
    .restart local v1    # "object":Lcom/navdy/client/debug/view/S3BrowserFragment$MyS3Object;
    :cond_1
    invoke-static {v0}, Lcom/navdy/client/debug/view/S3BrowserFragment$ObjectAdapter$ViewHolder;->access$700(Lcom/navdy/client/debug/view/S3BrowserFragment$ObjectAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v1}, Lcom/navdy/client/debug/view/S3BrowserFragment$MyS3Object;->getSize()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/navdy/client/debug/util/FormatUtils;->readableFileSize(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

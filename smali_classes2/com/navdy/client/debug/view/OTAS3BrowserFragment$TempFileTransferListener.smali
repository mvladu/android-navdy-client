.class Lcom/navdy/client/debug/view/OTAS3BrowserFragment$TempFileTransferListener;
.super Ljava/lang/Object;
.source "OTAS3BrowserFragment.java"

# interfaces
.implements Lcom/navdy/client/debug/file/FileTransferManager$FileTransferListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/debug/view/OTAS3BrowserFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TempFileTransferListener"
.end annotation


# static fields
.field private static final REPORT_ON_CHUNKS_STEP:I = 0x5


# instance fields
.field fileSize:J

.field fileSizeString:Ljava/lang/String;

.field previousThroughPutReportSize:J

.field previousThroughPutReportTime:J

.field startTime:J

.field final synthetic this$0:Lcom/navdy/client/debug/view/OTAS3BrowserFragment;

.field transferredFile:Ljava/io/File;


# direct methods
.method constructor <init>(Lcom/navdy/client/debug/view/OTAS3BrowserFragment;Ljava/io/File;)V
    .locals 2
    .param p2, "transferredFile"    # Ljava/io/File;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/navdy/client/debug/view/OTAS3BrowserFragment$TempFileTransferListener;->this$0:Lcom/navdy/client/debug/view/OTAS3BrowserFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p2, p0, Lcom/navdy/client/debug/view/OTAS3BrowserFragment$TempFileTransferListener;->transferredFile:Ljava/io/File;

    .line 49
    invoke-virtual {p2}, Ljava/io/File;->length()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/navdy/client/debug/view/OTAS3BrowserFragment$TempFileTransferListener;->fileSize:J

    .line 50
    iget-wide v0, p0, Lcom/navdy/client/debug/view/OTAS3BrowserFragment$TempFileTransferListener;->fileSize:J

    invoke-static {v0, v1}, Lcom/navdy/client/debug/util/FormatUtils;->readableFileSize(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/debug/view/OTAS3BrowserFragment$TempFileTransferListener;->fileSizeString:Ljava/lang/String;

    .line 51
    return-void
.end method

.method private reportCurrentTransferStatus(Lcom/navdy/service/library/events/file/FileTransferStatus;)V
    .locals 18
    .param p1, "fileTransferStatus"    # Lcom/navdy/service/library/events/file/FileTransferStatus;

    .prologue
    .line 116
    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/navdy/service/library/events/file/FileTransferStatus;->totalBytesTransferred:Ljava/lang/Long;

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    .line 117
    .local v10, "sentPartSize":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    const-wide/16 v14, 0x3e8

    div-long v6, v12, v14

    .line 118
    .local v6, "currentTime":J
    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/navdy/client/debug/view/OTAS3BrowserFragment$TempFileTransferListener;->startTime:J

    sub-long v12, v6, v12

    long-to-int v8, v12

    .line 119
    .local v8, "passedTime":I
    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/navdy/client/debug/view/OTAS3BrowserFragment$TempFileTransferListener;->previousThroughPutReportSize:J

    sub-long v12, v10, v12

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/navdy/client/debug/view/OTAS3BrowserFragment$TempFileTransferListener;->previousThroughPutReportTime:J

    sub-long v14, v6, v14

    div-long v4, v12, v14

    .line 122
    .local v4, "currentThroughput":J
    int-to-double v12, v8

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/navdy/client/debug/view/OTAS3BrowserFragment$TempFileTransferListener;->fileSize:J

    long-to-double v14, v14

    const-wide/high16 v16, 0x3ff0000000000000L    # 1.0

    mul-double v14, v14, v16

    long-to-double v0, v10

    move-wide/from16 v16, v0

    div-double v14, v14, v16

    const-wide/high16 v16, 0x3ff0000000000000L    # 1.0

    sub-double v14, v14, v16

    mul-double/2addr v12, v14

    double-to-int v9, v12

    invoke-static {v9}, Lcom/navdy/client/debug/util/FormatUtils;->formatDurationFromSecondsToSecondsMinutesHours(I)Ljava/lang/String;

    move-result-object v2

    .line 124
    .local v2, "currentETA":Ljava/lang/String;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v9

    const-string v12, "Transfer %d (chunk %d):%n%s out of %s sent.%nETA: %s (current speed: %s/s)"

    const/4 v13, 0x6

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/navdy/service/library/events/file/FileTransferStatus;->transferId:Ljava/lang/Integer;

    aput-object v15, v13, v14

    const/4 v14, 0x1

    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/navdy/service/library/events/file/FileTransferStatus;->chunkIndex:Ljava/lang/Integer;

    aput-object v15, v13, v14

    const/4 v14, 0x2

    .line 127
    invoke-static {v10, v11}, Lcom/navdy/client/debug/util/FormatUtils;->readableFileSize(J)Ljava/lang/String;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x3

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/navdy/client/debug/view/OTAS3BrowserFragment$TempFileTransferListener;->fileSizeString:Ljava/lang/String;

    aput-object v15, v13, v14

    const/4 v14, 0x4

    aput-object v2, v13, v14

    const/4 v14, 0x5

    .line 128
    invoke-static {v4, v5}, Lcom/navdy/client/debug/util/FormatUtils;->readableFileSize(J)Ljava/lang/String;

    move-result-object v15

    aput-object v15, v13, v14

    .line 124
    invoke-static {v9, v12, v13}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 130
    .local v3, "message":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/navdy/client/debug/view/OTAS3BrowserFragment$TempFileTransferListener;->this$0:Lcom/navdy/client/debug/view/OTAS3BrowserFragment;

    iget-object v9, v9, Lcom/navdy/client/debug/view/OTAS3BrowserFragment;->mAppInstance:Lcom/navdy/client/app/framework/AppInstance;

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-virtual {v9, v3, v12, v13}, Lcom/navdy/client/app/framework/AppInstance;->showToast(Ljava/lang/String;ZI)V

    .line 131
    move-object/from16 v0, p0

    iput-wide v6, v0, Lcom/navdy/client/debug/view/OTAS3BrowserFragment$TempFileTransferListener;->previousThroughPutReportTime:J

    .line 132
    move-object/from16 v0, p0

    iput-wide v10, v0, Lcom/navdy/client/debug/view/OTAS3BrowserFragment$TempFileTransferListener;->previousThroughPutReportSize:J

    .line 133
    return-void
.end method

.method private showMessageAndDeleteFile(Ljava/lang/String;)V
    .locals 4
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 64
    iget-object v1, p0, Lcom/navdy/client/debug/view/OTAS3BrowserFragment$TempFileTransferListener;->transferredFile:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    move-result v0

    .line 65
    .local v0, "worked":Z
    if-nez v0, :cond_0

    .line 66
    invoke-static {}, Lcom/navdy/client/debug/view/OTAS3BrowserFragment;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to delete transferred file: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/client/debug/view/OTAS3BrowserFragment$TempFileTransferListener;->transferredFile:Ljava/io/File;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 68
    :cond_0
    iget-object v1, p0, Lcom/navdy/client/debug/view/OTAS3BrowserFragment$TempFileTransferListener;->this$0:Lcom/navdy/client/debug/view/OTAS3BrowserFragment;

    iget-object v1, v1, Lcom/navdy/client/debug/view/OTAS3BrowserFragment;->mAppInstance:Lcom/navdy/client/app/framework/AppInstance;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Lcom/navdy/client/app/framework/AppInstance;->showToast(Ljava/lang/String;Z)V

    .line 69
    return-void
.end method


# virtual methods
.method public onError(Lcom/navdy/service/library/events/file/FileTransferError;Ljava/lang/String;)V
    .locals 4
    .param p1, "errorCode"    # Lcom/navdy/service/library/events/file/FileTransferError;
    .param p2, "error"    # Ljava/lang/String;

    .prologue
    .line 55
    iget-object v1, p0, Lcom/navdy/client/debug/view/OTAS3BrowserFragment$TempFileTransferListener;->transferredFile:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    move-result v0

    .line 56
    .local v0, "worked":Z
    if-nez v0, :cond_0

    .line 57
    invoke-static {}, Lcom/navdy/client/debug/view/OTAS3BrowserFragment;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to delete transferred file: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/client/debug/view/OTAS3BrowserFragment$TempFileTransferListener;->transferredFile:Ljava/io/File;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 59
    :cond_0
    invoke-static {}, Lcom/navdy/client/debug/view/OTAS3BrowserFragment;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 60
    iget-object v1, p0, Lcom/navdy/client/debug/view/OTAS3BrowserFragment$TempFileTransferListener;->this$0:Lcom/navdy/client/debug/view/OTAS3BrowserFragment;

    iget-object v1, v1, Lcom/navdy/client/debug/view/OTAS3BrowserFragment;->mAppInstance:Lcom/navdy/client/app/framework/AppInstance;

    const-string v2, "Error while sending file. Check log."

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/navdy/client/app/framework/AppInstance;->showToast(Ljava/lang/String;Z)V

    .line 61
    return-void
.end method

.method public onFileTransferResponse(Lcom/navdy/service/library/events/file/FileTransferResponse;)V
    .locals 7
    .param p1, "fileTransferResponse"    # Lcom/navdy/service/library/events/file/FileTransferResponse;

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 73
    invoke-static {}, Lcom/navdy/client/debug/view/OTAS3BrowserFragment;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "onFileTransferResponse: %s transferId: %s"

    new-array v2, v6, [Ljava/lang/Object;

    aput-object p1, v2, v4

    iget-object v3, p1, Lcom/navdy/service/library/events/file/FileTransferResponse;->transferId:Ljava/lang/Integer;

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 75
    iget-object v0, p1, Lcom/navdy/service/library/events/file/FileTransferResponse;->success:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/navdy/client/debug/view/OTAS3BrowserFragment$TempFileTransferListener;->this$0:Lcom/navdy/client/debug/view/OTAS3BrowserFragment;

    iget-object v0, v0, Lcom/navdy/client/debug/view/OTAS3BrowserFragment;->mAppInstance:Lcom/navdy/client/app/framework/AppInstance;

    const-string v1, "File transfer (%s) request acknowledged"

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p1, Lcom/navdy/service/library/events/file/FileTransferResponse;->transferId:Ljava/lang/Integer;

    aput-object v3, v2, v4

    .line 77
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 76
    invoke-virtual {v0, v1, v4, v4}, Lcom/navdy/client/app/framework/AppInstance;->showToast(Ljava/lang/String;ZI)V

    .line 81
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    iput-wide v0, p0, Lcom/navdy/client/debug/view/OTAS3BrowserFragment$TempFileTransferListener;->startTime:J

    .line 82
    iget-wide v0, p0, Lcom/navdy/client/debug/view/OTAS3BrowserFragment$TempFileTransferListener;->startTime:J

    iput-wide v0, p0, Lcom/navdy/client/debug/view/OTAS3BrowserFragment$TempFileTransferListener;->previousThroughPutReportTime:J

    .line 83
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/navdy/client/debug/view/OTAS3BrowserFragment$TempFileTransferListener;->previousThroughPutReportSize:J

    .line 89
    :goto_0
    return-void

    .line 85
    :cond_0
    const-string v0, "File transfer (%s) request declined with status %s"

    new-array v1, v6, [Ljava/lang/Object;

    iget-object v2, p1, Lcom/navdy/service/library/events/file/FileTransferResponse;->transferId:Ljava/lang/Integer;

    aput-object v2, v1, v4

    iget-object v2, p1, Lcom/navdy/service/library/events/file/FileTransferResponse;->error:Lcom/navdy/service/library/events/file/FileTransferError;

    aput-object v2, v1, v5

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/navdy/client/debug/view/OTAS3BrowserFragment$TempFileTransferListener;->showMessageAndDeleteFile(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onFileTransferStatus(Lcom/navdy/service/library/events/file/FileTransferStatus;)V
    .locals 11
    .param p1, "fileTransferStatus"    # Lcom/navdy/service/library/events/file/FileTransferStatus;

    .prologue
    const/4 v6, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 93
    invoke-static {}, Lcom/navdy/client/debug/view/OTAS3BrowserFragment;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "onFileTransferStatus: %s transferId: %s chunk: %s"

    new-array v4, v6, [Ljava/lang/Object;

    iget-object v5, p1, Lcom/navdy/service/library/events/file/FileTransferStatus;->error:Lcom/navdy/service/library/events/file/FileTransferError;

    aput-object v5, v4, v8

    iget-object v5, p1, Lcom/navdy/service/library/events/file/FileTransferStatus;->transferId:Ljava/lang/Integer;

    aput-object v5, v4, v9

    iget-object v5, p1, Lcom/navdy/service/library/events/file/FileTransferStatus;->chunkIndex:Ljava/lang/Integer;

    aput-object v5, v4, v10

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 96
    iget-object v2, p1, Lcom/navdy/service/library/events/file/FileTransferStatus;->success:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 97
    iget-wide v2, p0, Lcom/navdy/client/debug/view/OTAS3BrowserFragment$TempFileTransferListener;->fileSize:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    iget-wide v6, p0, Lcom/navdy/client/debug/view/OTAS3BrowserFragment$TempFileTransferListener;->startTime:J

    sub-long/2addr v4, v6

    div-long v0, v2, v4

    .line 98
    .local v0, "averageThroughput":J
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    const-string v3, "File transfer %d fully completed (average speed: %s/s)"

    new-array v4, v10, [Ljava/lang/Object;

    iget-object v5, p1, Lcom/navdy/service/library/events/file/FileTransferStatus;->transferId:Ljava/lang/Integer;

    aput-object v5, v4, v8

    .line 101
    invoke-static {v0, v1}, Lcom/navdy/client/debug/util/FormatUtils;->readableFileSize(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v9

    .line 98
    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/navdy/client/debug/view/OTAS3BrowserFragment$TempFileTransferListener;->showMessageAndDeleteFile(Ljava/lang/String;)V

    .line 113
    .end local v0    # "averageThroughput":J
    :cond_0
    :goto_0
    return-void

    .line 103
    :cond_1
    iget-object v2, p1, Lcom/navdy/service/library/events/file/FileTransferStatus;->error:Lcom/navdy/service/library/events/file/FileTransferError;

    sget-object v3, Lcom/navdy/service/library/events/file/FileTransferError;->FILE_TRANSFER_NO_ERROR:Lcom/navdy/service/library/events/file/FileTransferError;

    if-eq v2, v3, :cond_2

    .line 104
    iget-object v2, p1, Lcom/navdy/service/library/events/file/FileTransferStatus;->chunkIndex:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    rem-int/lit8 v2, v2, 0x5

    if-nez v2, :cond_0

    .line 105
    invoke-direct {p0, p1}, Lcom/navdy/client/debug/view/OTAS3BrowserFragment$TempFileTransferListener;->reportCurrentTransferStatus(Lcom/navdy/service/library/events/file/FileTransferStatus;)V

    goto :goto_0

    .line 108
    :cond_2
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    const-string v3, "Error: Status %s returned while sending chunk %d of file transfer %d"

    new-array v4, v6, [Ljava/lang/Object;

    iget-object v5, p1, Lcom/navdy/service/library/events/file/FileTransferStatus;->error:Lcom/navdy/service/library/events/file/FileTransferError;

    aput-object v5, v4, v8

    iget-object v5, p1, Lcom/navdy/service/library/events/file/FileTransferStatus;->chunkIndex:Ljava/lang/Integer;

    aput-object v5, v4, v9

    iget-object v5, p1, Lcom/navdy/service/library/events/file/FileTransferStatus;->transferId:Ljava/lang/Integer;

    aput-object v5, v4, v10

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/navdy/client/debug/view/OTAS3BrowserFragment$TempFileTransferListener;->showMessageAndDeleteFile(Ljava/lang/String;)V

    goto :goto_0
.end method

.class public Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;
.super Lcom/here/android/mpa/common/MapActivity;
.source "NavigationDemoActivity.java"


# static fields
.field private static final DEFAULT_DESTINATION:[D

.field private static final DEFAULT_START_POINT:[D

.field public static final EXTRA_COORDS_END:Ljava/lang/String; = "dest_coords"

.field public static final EXTRA_COORDS_START:Ljava/lang/String; = "start_coords"

.field public static final FT_IN_METER:F = 3.28084f

.field public static final METERS_IN_MI:F = 1609.34f

.field public static final SMALL_UNITS_THRESHOLD:F = 0.1f

.field private static final VOICE_MARC_CODE:Ljava/lang/String; = "eng"

.field public static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private currentRoadTV:Landroid/widget/TextView;

.field private currentSpeedTV:Landroid/widget/TextView;

.field private distToNextManeuverTV:Landroid/widget/TextView;

.field protected final etaFormatter:Ljava/text/SimpleDateFormat;

.field private etaTV:Landroid/widget/TextView;

.field protected laneInfo:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f1000cb
    .end annotation
.end field

.field private laneInformationListener:Lcom/here/android/mpa/guidance/NavigationManager$LaneInformationListener;

.field private mCurrentRoad:Ljava/lang/String;

.field private mDistanceToPendingStreet:Ljava/lang/String;

.field private mEndCoord:Lcom/here/android/mpa/common/GeoCoordinate;

.field protected mEngineTurnToNavigationTurnMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/here/android/mpa/routing/Maneuver$Turn;",
            "Lcom/navdy/service/library/events/navigation/NavigationTurn;",
            ">;"
        }
    .end annotation
.end field

.field private mEtaText:Ljava/lang/String;

.field private mLaneInfoList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/here/android/mpa/guidance/LaneInformation;",
            ">;"
        }
    .end annotation
.end field

.field private mMenu:Landroid/view/Menu;

.field private mPendingStreet:Ljava/lang/String;

.field private mPendingTurn:Lcom/navdy/service/library/events/navigation/NavigationTurn;

.field private mSpeedText:Ljava/lang/String;

.field private mStartCoord:Lcom/here/android/mpa/common/GeoCoordinate;

.field private maneuverDetailList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/here/android/mpa/routing/Maneuver;",
            ">;"
        }
    .end annotation
.end field

.field protected maneuverDetailListView:Landroid/widget/ListView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f1000cc
    .end annotation
.end field

.field private maneuverInfo:Landroid/widget/RelativeLayout;

.field private map:Lcom/here/android/mpa/mapping/Map;

.field private mapFragment:Lcom/here/android/mpa/mapping/MapFragment;

.field private mapRoute:Lcom/here/android/mpa/mapping/MapRoute;

.field private navigationInfo:Landroid/widget/RelativeLayout;

.field private navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

.field private navigationManagerListener:Lcom/here/android/mpa/guidance/NavigationManager$NavigationManagerEventListener;

.field private newInstructionEventListener:Lcom/here/android/mpa/guidance/NavigationManager$NewInstructionEventListener;

.field private nextManeuverTurnTV:Landroid/widget/TextView;

.field private nextRoadTV:Landroid/widget/TextView;

.field private positionListener:Lcom/here/android/mpa/guidance/NavigationManager$PositionListener;

.field private positioningManager:Lcom/here/android/mpa/common/PositioningManager;

.field private route:Lcom/here/android/mpa/routing/Route;

.field private routeManagerListener:Lcom/here/android/mpa/routing/RouteManager$Listener;

.field private speedLimitTV:Landroid/widget/TextView;

.field private speedWarningListener:Lcom/here/android/mpa/guidance/NavigationManager$SpeedWarningListener;

.field protected toggleButton:Landroid/widget/Button;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f1000cd
    .end annotation
.end field

.field private voiceCatalog:Lcom/here/android/mpa/guidance/VoiceCatalog;

.field private voiceSkin:Lcom/here/android/mpa/guidance/VoiceSkin;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 69
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 82
    new-array v0, v2, [D

    fill-array-data v0, :array_0

    sput-object v0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->DEFAULT_START_POINT:[D

    .line 83
    new-array v0, v2, [D

    fill-array-data v0, :array_1

    sput-object v0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->DEFAULT_DESTINATION:[D

    return-void

    .line 82
    nop

    :array_0
    .array-data 8
        0x4048a1487768166aL    # 49.260024
        -0x3fa13f8d92fb19e7L    # -123.006984
    .end array-data

    .line 83
    :array_1
    .array-data 8
        0x40489fc89f40a287L    # 49.24831
        -0x3fa14147778dd617L    # -122.980013
    .end array-data
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 67
    invoke-direct {p0}, Lcom/here/android/mpa/common/MapActivity;-><init>()V

    .line 77
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "h:mm"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->etaFormatter:Ljava/text/SimpleDateFormat;

    .line 88
    iput-object v2, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->map:Lcom/here/android/mpa/mapping/Map;

    .line 91
    iput-object v2, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mapFragment:Lcom/here/android/mpa/mapping/MapFragment;

    .line 103
    iput-object v2, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->route:Lcom/here/android/mpa/routing/Route;

    .line 104
    iput-object v2, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mapRoute:Lcom/here/android/mpa/mapping/MapRoute;

    .line 107
    iput-object v2, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->voiceSkin:Lcom/here/android/mpa/guidance/VoiceSkin;

    .line 131
    const-string v0, ""

    iput-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mCurrentRoad:Ljava/lang/String;

    .line 132
    sget-object v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_UNKNOWN:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    iput-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mPendingTurn:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    .line 133
    const-string v0, ""

    iput-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mDistanceToPendingStreet:Ljava/lang/String;

    .line 134
    const-string v0, ""

    iput-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mPendingStreet:Ljava/lang/String;

    .line 135
    const-string v0, ""

    iput-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mEtaText:Ljava/lang/String;

    .line 136
    const-string v0, ""

    iput-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mSpeedText:Ljava/lang/String;

    .line 426
    new-instance v0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$3;

    invoke-direct {v0, p0}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$3;-><init>(Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;)V

    iput-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->routeManagerListener:Lcom/here/android/mpa/routing/RouteManager$Listener;

    .line 450
    new-instance v0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$4;

    invoke-direct {v0, p0}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$4;-><init>(Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;)V

    iput-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->navigationManagerListener:Lcom/here/android/mpa/guidance/NavigationManager$NavigationManagerEventListener;

    .line 486
    new-instance v0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$5;

    invoke-direct {v0, p0}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$5;-><init>(Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;)V

    iput-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->laneInformationListener:Lcom/here/android/mpa/guidance/NavigationManager$LaneInformationListener;

    .line 495
    new-instance v0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$6;

    invoke-direct {v0, p0}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$6;-><init>(Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;)V

    iput-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->newInstructionEventListener:Lcom/here/android/mpa/guidance/NavigationManager$NewInstructionEventListener;

    .line 503
    new-instance v0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$7;

    invoke-direct {v0, p0}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$7;-><init>(Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;)V

    iput-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->positionListener:Lcom/here/android/mpa/guidance/NavigationManager$PositionListener;

    .line 511
    new-instance v0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$8;

    invoke-direct {v0, p0}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$8;-><init>(Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;)V

    iput-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->speedWarningListener:Lcom/here/android/mpa/guidance/NavigationManager$SpeedWarningListener;

    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->onMapEngineInitializationCompleted()V

    return-void
.end method

.method static synthetic access$100(Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;)Lcom/here/android/mpa/common/GeoCoordinate;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mStartCoord:Lcom/here/android/mpa/common/GeoCoordinate;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->resetUIOnNavigationEnds()V

    return-void
.end method

.method static synthetic access$1100(Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->updateManeuver()V

    return-void
.end method

.method static synthetic access$1200(Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;Lcom/here/android/mpa/common/GeoPosition;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;
    .param p1, "x1"    # Lcom/here/android/mpa/common/GeoPosition;

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->updatePositionInfo(Lcom/here/android/mpa/common/GeoPosition;)V

    return-void
.end method

.method static synthetic access$1300(Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->speedLimitTV:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;)Lcom/here/android/mpa/common/GeoCoordinate;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mEndCoord:Lcom/here/android/mpa/common/GeoCoordinate;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/common/GeoCoordinate;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;
    .param p1, "x1"    # Lcom/here/android/mpa/common/GeoCoordinate;
    .param p2, "x2"    # Lcom/here/android/mpa/common/GeoCoordinate;

    .prologue
    .line 67
    invoke-direct {p0, p1, p2}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->createRoute(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/common/GeoCoordinate;)V

    return-void
.end method

.method static synthetic access$400(Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;)Lcom/here/android/mpa/guidance/VoiceCatalog;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->voiceCatalog:Lcom/here/android/mpa/guidance/VoiceCatalog;

    return-object v0
.end method

.method static synthetic access$502(Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;Lcom/here/android/mpa/guidance/VoiceSkin;)Lcom/here/android/mpa/guidance/VoiceSkin;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;
    .param p1, "x1"    # Lcom/here/android/mpa/guidance/VoiceSkin;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->voiceSkin:Lcom/here/android/mpa/guidance/VoiceSkin;

    return-object p1
.end method

.method static synthetic access$600(Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;)Lcom/here/android/mpa/routing/Route;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->route:Lcom/here/android/mpa/routing/Route;

    return-object v0
.end method

.method static synthetic access$602(Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;Lcom/here/android/mpa/routing/Route;)Lcom/here/android/mpa/routing/Route;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;
    .param p1, "x1"    # Lcom/here/android/mpa/routing/Route;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->route:Lcom/here/android/mpa/routing/Route;

    return-object p1
.end method

.method static synthetic access$700(Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;)Lcom/here/android/mpa/mapping/MapRoute;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mapRoute:Lcom/here/android/mpa/mapping/MapRoute;

    return-object v0
.end method

.method static synthetic access$702(Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;Lcom/here/android/mpa/mapping/MapRoute;)Lcom/here/android/mpa/mapping/MapRoute;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;
    .param p1, "x1"    # Lcom/here/android/mpa/mapping/MapRoute;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mapRoute:Lcom/here/android/mpa/mapping/MapRoute;

    return-object p1
.end method

.method static synthetic access$800(Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;)Lcom/here/android/mpa/mapping/Map;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->map:Lcom/here/android/mpa/mapping/Map;

    return-object v0
.end method

.method static synthetic access$900(Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;)Lcom/here/android/mpa/guidance/NavigationManager;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    return-object v0
.end method

.method private clearNavigationInfo()V
    .locals 2

    .prologue
    .line 529
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->nextRoadTV:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 530
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->nextManeuverTurnTV:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 531
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->distToNextManeuverTV:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 532
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->currentRoadTV:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 533
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->speedLimitTV:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 534
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->currentSpeedTV:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 535
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->etaTV:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 536
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->laneInfo:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 537
    return-void
.end method

.method public static createIntentWithCoords(Landroid/content/Context;Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/common/GeoCoordinate;)Landroid/content/Intent;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "startCoord"    # Lcom/here/android/mpa/common/GeoCoordinate;
    .param p2, "endCoord"    # Lcom/here/android/mpa/common/GeoCoordinate;

    .prologue
    .line 217
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 218
    :cond_0
    sget-object v1, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "createIntentWithCoords: bad coordinates"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 219
    const/4 v0, 0x0

    .line 225
    :goto_0
    return-object v0

    .line 222
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 223
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "start_coords"

    invoke-static {p1}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->latLonArrayFromGeoCoordinate(Lcom/here/android/mpa/common/GeoCoordinate;)[D

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[D)Landroid/content/Intent;

    .line 224
    const-string v1, "dest_coords"

    invoke-static {p2}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->latLonArrayFromGeoCoordinate(Lcom/here/android/mpa/common/GeoCoordinate;)[D

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[D)Landroid/content/Intent;

    goto :goto_0
.end method

.method private createRoute(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/common/GeoCoordinate;)V
    .locals 4
    .param p1, "start"    # Lcom/here/android/mpa/common/GeoCoordinate;
    .param p2, "end"    # Lcom/here/android/mpa/common/GeoCoordinate;

    .prologue
    .line 310
    new-instance v0, Lcom/here/android/mpa/routing/RouteManager;

    invoke-direct {v0}, Lcom/here/android/mpa/routing/RouteManager;-><init>()V

    .line 312
    .local v0, "routeManager":Lcom/here/android/mpa/routing/RouteManager;
    new-instance v2, Lcom/here/android/mpa/routing/RoutePlan;

    invoke-direct {v2}, Lcom/here/android/mpa/routing/RoutePlan;-><init>()V

    .line 313
    .local v2, "routePlan":Lcom/here/android/mpa/routing/RoutePlan;
    new-instance v1, Lcom/here/android/mpa/routing/RouteOptions;

    invoke-direct {v1}, Lcom/here/android/mpa/routing/RouteOptions;-><init>()V

    .line 314
    .local v1, "routeOptions":Lcom/here/android/mpa/routing/RouteOptions;
    sget-object v3, Lcom/here/android/mpa/routing/RouteOptions$TransportMode;->CAR:Lcom/here/android/mpa/routing/RouteOptions$TransportMode;

    invoke-virtual {v1, v3}, Lcom/here/android/mpa/routing/RouteOptions;->setTransportMode(Lcom/here/android/mpa/routing/RouteOptions$TransportMode;)Lcom/here/android/mpa/routing/RouteOptions;

    .line 315
    sget-object v3, Lcom/here/android/mpa/routing/RouteOptions$Type;->FASTEST:Lcom/here/android/mpa/routing/RouteOptions$Type;

    invoke-virtual {v1, v3}, Lcom/here/android/mpa/routing/RouteOptions;->setRouteType(Lcom/here/android/mpa/routing/RouteOptions$Type;)Lcom/here/android/mpa/routing/RouteOptions;

    .line 316
    invoke-virtual {v2, v1}, Lcom/here/android/mpa/routing/RoutePlan;->setRouteOptions(Lcom/here/android/mpa/routing/RouteOptions;)Lcom/here/android/mpa/routing/RoutePlan;

    .line 318
    invoke-virtual {v2, p1}, Lcom/here/android/mpa/routing/RoutePlan;->addWaypoint(Lcom/here/android/mpa/common/GeoCoordinate;)Lcom/here/android/mpa/routing/RoutePlan;

    .line 319
    invoke-virtual {v2, p2}, Lcom/here/android/mpa/routing/RoutePlan;->addWaypoint(Lcom/here/android/mpa/common/GeoCoordinate;)Lcom/here/android/mpa/routing/RoutePlan;

    .line 321
    iget-object v3, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->routeManagerListener:Lcom/here/android/mpa/routing/RouteManager$Listener;

    invoke-virtual {v0, v2, v3}, Lcom/here/android/mpa/routing/RouteManager;->calculateRoute(Lcom/here/android/mpa/routing/RoutePlan;Lcom/here/android/mpa/routing/RouteManager$Listener;)Lcom/here/android/mpa/routing/RouteManager$Error;

    .line 322
    return-void
.end method

.method private downloadTargetVoiceSkin()V
    .locals 5

    .prologue
    .line 368
    iget-object v2, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->voiceCatalog:Lcom/here/android/mpa/guidance/VoiceCatalog;

    invoke-virtual {v2}, Lcom/here/android/mpa/guidance/VoiceCatalog;->getLocalVoiceSkins()Ljava/util/List;

    move-result-object v0

    .line 371
    .local v0, "localSkins":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/guidance/VoiceSkin;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/here/android/mpa/guidance/VoiceSkin;

    .line 372
    .local v1, "skin":Lcom/here/android/mpa/guidance/VoiceSkin;
    invoke-virtual {v1}, Lcom/here/android/mpa/guidance/VoiceSkin;->getMarcCode()Ljava/lang/String;

    move-result-object v3

    const-string v4, "eng"

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_0

    .line 373
    invoke-virtual {v1}, Lcom/here/android/mpa/guidance/VoiceSkin;->getOutputType()Lcom/here/android/mpa/guidance/VoiceSkin$OutputType;

    move-result-object v3

    sget-object v4, Lcom/here/android/mpa/guidance/VoiceSkin$OutputType;->TTS:Lcom/here/android/mpa/guidance/VoiceSkin$OutputType;

    if-ne v3, v4, :cond_0

    .line 374
    iput-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->voiceSkin:Lcom/here/android/mpa/guidance/VoiceSkin;

    .line 381
    .end local v1    # "skin":Lcom/here/android/mpa/guidance/VoiceSkin;
    :cond_1
    iget-object v2, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->voiceSkin:Lcom/here/android/mpa/guidance/VoiceSkin;

    if-nez v2, :cond_2

    .line 384
    iget-object v2, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->voiceCatalog:Lcom/here/android/mpa/guidance/VoiceCatalog;

    new-instance v3, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$2;

    invoke-direct {v3, p0}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$2;-><init>(Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;)V

    invoke-virtual {v2, v3}, Lcom/here/android/mpa/guidance/VoiceCatalog;->downloadCatalog(Lcom/here/android/mpa/guidance/VoiceCatalog$OnDownloadDoneListener;)Z

    .line 424
    :cond_2
    return-void
.end method

.method public static latLonArrayFromGeoCoordinate(Lcom/here/android/mpa/common/GeoCoordinate;)[D
    .locals 4
    .param p0, "coordinate"    # Lcom/here/android/mpa/common/GeoCoordinate;

    .prologue
    .line 212
    const/4 v1, 0x2

    new-array v0, v1, [D

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/here/android/mpa/common/GeoCoordinate;->getLatitude()D

    move-result-wide v2

    aput-wide v2, v0, v1

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/here/android/mpa/common/GeoCoordinate;->getLongitude()D

    move-result-wide v2

    aput-wide v2, v0, v1

    .line 213
    .local v0, "geoCoordArray":[D
    return-object v0
.end method

.method private navigationTurnFromEngineTurn(Lcom/here/android/mpa/routing/Maneuver$Turn;)Lcom/navdy/service/library/events/navigation/NavigationTurn;
    .locals 4
    .param p1, "turn"    # Lcom/here/android/mpa/routing/Maneuver$Turn;

    .prologue
    .line 689
    if-nez p1, :cond_1

    .line 690
    sget-object v1, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "directionInfoFromTurn: passed null"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 691
    const/4 v0, 0x0

    .line 729
    :cond_0
    :goto_0
    return-object v0

    .line 694
    :cond_1
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mEngineTurnToNavigationTurnMap:Ljava/util/HashMap;

    if-nez v1, :cond_2

    .line 695
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mEngineTurnToNavigationTurnMap:Ljava/util/HashMap;

    .line 696
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mEngineTurnToNavigationTurnMap:Ljava/util/HashMap;

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Turn;->UNDEFINED:Lcom/here/android/mpa/routing/Maneuver$Turn;

    sget-object v3, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_UNKNOWN:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 697
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mEngineTurnToNavigationTurnMap:Ljava/util/HashMap;

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Turn;->NO_TURN:Lcom/here/android/mpa/routing/Maneuver$Turn;

    sget-object v3, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_UNKNOWN:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 698
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mEngineTurnToNavigationTurnMap:Ljava/util/HashMap;

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Turn;->KEEP_MIDDLE:Lcom/here/android/mpa/routing/Maneuver$Turn;

    sget-object v3, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_STRAIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 699
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mEngineTurnToNavigationTurnMap:Ljava/util/HashMap;

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Turn;->KEEP_RIGHT:Lcom/here/android/mpa/routing/Maneuver$Turn;

    sget-object v3, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_KEEP_RIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 700
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mEngineTurnToNavigationTurnMap:Ljava/util/HashMap;

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Turn;->LIGHT_RIGHT:Lcom/here/android/mpa/routing/Maneuver$Turn;

    sget-object v3, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_EASY_RIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 701
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mEngineTurnToNavigationTurnMap:Ljava/util/HashMap;

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Turn;->QUITE_RIGHT:Lcom/here/android/mpa/routing/Maneuver$Turn;

    sget-object v3, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_RIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 702
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mEngineTurnToNavigationTurnMap:Ljava/util/HashMap;

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Turn;->HEAVY_RIGHT:Lcom/here/android/mpa/routing/Maneuver$Turn;

    sget-object v3, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_SHARP_RIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 703
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mEngineTurnToNavigationTurnMap:Ljava/util/HashMap;

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Turn;->KEEP_LEFT:Lcom/here/android/mpa/routing/Maneuver$Turn;

    sget-object v3, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_KEEP_LEFT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 704
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mEngineTurnToNavigationTurnMap:Ljava/util/HashMap;

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Turn;->LIGHT_LEFT:Lcom/here/android/mpa/routing/Maneuver$Turn;

    sget-object v3, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_EASY_LEFT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 705
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mEngineTurnToNavigationTurnMap:Ljava/util/HashMap;

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Turn;->QUITE_LEFT:Lcom/here/android/mpa/routing/Maneuver$Turn;

    sget-object v3, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_LEFT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 706
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mEngineTurnToNavigationTurnMap:Ljava/util/HashMap;

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Turn;->HEAVY_LEFT:Lcom/here/android/mpa/routing/Maneuver$Turn;

    sget-object v3, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_SHARP_LEFT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 707
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mEngineTurnToNavigationTurnMap:Ljava/util/HashMap;

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Turn;->RETURN:Lcom/here/android/mpa/routing/Maneuver$Turn;

    sget-object v3, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_UTURN_LEFT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 708
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mEngineTurnToNavigationTurnMap:Ljava/util/HashMap;

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Turn;->ROUNDABOUT_1:Lcom/here/android/mpa/routing/Maneuver$Turn;

    sget-object v3, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_ROUNDABOUT_S:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 709
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mEngineTurnToNavigationTurnMap:Ljava/util/HashMap;

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Turn;->ROUNDABOUT_2:Lcom/here/android/mpa/routing/Maneuver$Turn;

    sget-object v3, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_ROUNDABOUT_S:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 710
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mEngineTurnToNavigationTurnMap:Ljava/util/HashMap;

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Turn;->ROUNDABOUT_3:Lcom/here/android/mpa/routing/Maneuver$Turn;

    sget-object v3, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_ROUNDABOUT_S:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 711
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mEngineTurnToNavigationTurnMap:Ljava/util/HashMap;

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Turn;->ROUNDABOUT_4:Lcom/here/android/mpa/routing/Maneuver$Turn;

    sget-object v3, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_ROUNDABOUT_S:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 712
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mEngineTurnToNavigationTurnMap:Ljava/util/HashMap;

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Turn;->ROUNDABOUT_5:Lcom/here/android/mpa/routing/Maneuver$Turn;

    sget-object v3, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_ROUNDABOUT_S:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 713
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mEngineTurnToNavigationTurnMap:Ljava/util/HashMap;

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Turn;->ROUNDABOUT_6:Lcom/here/android/mpa/routing/Maneuver$Turn;

    sget-object v3, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_ROUNDABOUT_S:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 714
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mEngineTurnToNavigationTurnMap:Ljava/util/HashMap;

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Turn;->ROUNDABOUT_7:Lcom/here/android/mpa/routing/Maneuver$Turn;

    sget-object v3, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_ROUNDABOUT_S:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 715
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mEngineTurnToNavigationTurnMap:Ljava/util/HashMap;

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Turn;->ROUNDABOUT_8:Lcom/here/android/mpa/routing/Maneuver$Turn;

    sget-object v3, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_ROUNDABOUT_S:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 716
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mEngineTurnToNavigationTurnMap:Ljava/util/HashMap;

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Turn;->ROUNDABOUT_9:Lcom/here/android/mpa/routing/Maneuver$Turn;

    sget-object v3, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_ROUNDABOUT_S:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 717
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mEngineTurnToNavigationTurnMap:Ljava/util/HashMap;

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Turn;->ROUNDABOUT_10:Lcom/here/android/mpa/routing/Maneuver$Turn;

    sget-object v3, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_ROUNDABOUT_S:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 718
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mEngineTurnToNavigationTurnMap:Ljava/util/HashMap;

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Turn;->ROUNDABOUT_11:Lcom/here/android/mpa/routing/Maneuver$Turn;

    sget-object v3, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_ROUNDABOUT_S:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 719
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mEngineTurnToNavigationTurnMap:Ljava/util/HashMap;

    sget-object v2, Lcom/here/android/mpa/routing/Maneuver$Turn;->ROUNDABOUT_12:Lcom/here/android/mpa/routing/Maneuver$Turn;

    sget-object v3, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_ROUNDABOUT_S:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 722
    :cond_2
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mEngineTurnToNavigationTurnMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;

    .line 724
    .local v0, "navigationTurn":Lcom/navdy/service/library/events/navigation/NavigationTurn;
    if-nez v0, :cond_0

    .line 725
    sget-object v1, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "navigationTurnFromEngineTurn: unknown & defaulting to straight for: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 726
    sget-object v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_STRAIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    goto/16 :goto_0
.end method

.method private onMapEngineInitializationCompleted()V
    .locals 3

    .prologue
    .line 280
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mapFragment:Lcom/here/android/mpa/mapping/MapFragment;

    if-eqz v0, :cond_0

    .line 281
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mapFragment:Lcom/here/android/mpa/mapping/MapFragment;

    invoke-virtual {v0}, Lcom/here/android/mpa/mapping/MapFragment;->getMap()Lcom/here/android/mpa/mapping/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->map:Lcom/here/android/mpa/mapping/Map;

    .line 285
    :cond_0
    invoke-static {}, Lcom/here/android/mpa/common/PositioningManager;->getInstance()Lcom/here/android/mpa/common/PositioningManager;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->positioningManager:Lcom/here/android/mpa/common/PositioningManager;

    .line 286
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->positioningManager:Lcom/here/android/mpa/common/PositioningManager;

    invoke-virtual {v0}, Lcom/here/android/mpa/common/PositioningManager;->isActive()Z

    move-result v0

    if-nez v0, :cond_1

    .line 287
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->positioningManager:Lcom/here/android/mpa/common/PositioningManager;

    sget-object v1, Lcom/here/android/mpa/common/PositioningManager$LocationMethod;->GPS_NETWORK:Lcom/here/android/mpa/common/PositioningManager$LocationMethod;

    invoke-virtual {v0, v1}, Lcom/here/android/mpa/common/PositioningManager;->start(Lcom/here/android/mpa/common/PositioningManager$LocationMethod;)Z

    .line 289
    :cond_1
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->map:Lcom/here/android/mpa/mapping/Map;

    if-eqz v0, :cond_2

    .line 290
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->map:Lcom/here/android/mpa/mapping/Map;

    invoke-virtual {v0}, Lcom/here/android/mpa/mapping/Map;->getPositionIndicator()Lcom/here/android/mpa/mapping/PositionIndicator;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/here/android/mpa/mapping/PositionIndicator;->setVisible(Z)Lcom/here/android/mpa/mapping/PositionIndicator;

    .line 294
    :cond_2
    invoke-static {}, Lcom/here/android/mpa/guidance/NavigationManager;->getInstance()Lcom/here/android/mpa/guidance/NavigationManager;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    .line 295
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    new-instance v1, Ljava/lang/ref/WeakReference;

    iget-object v2, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->navigationManagerListener:Lcom/here/android/mpa/guidance/NavigationManager$NavigationManagerEventListener;

    invoke-direct {v1, v2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lcom/here/android/mpa/guidance/NavigationManager;->addNavigationManagerEventListener(Ljava/lang/ref/WeakReference;)V

    .line 296
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    new-instance v1, Ljava/lang/ref/WeakReference;

    iget-object v2, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->positionListener:Lcom/here/android/mpa/guidance/NavigationManager$PositionListener;

    invoke-direct {v1, v2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lcom/here/android/mpa/guidance/NavigationManager;->addPositionListener(Ljava/lang/ref/WeakReference;)V

    .line 297
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    new-instance v1, Ljava/lang/ref/WeakReference;

    iget-object v2, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->laneInformationListener:Lcom/here/android/mpa/guidance/NavigationManager$LaneInformationListener;

    invoke-direct {v1, v2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lcom/here/android/mpa/guidance/NavigationManager;->addLaneInformationListener(Ljava/lang/ref/WeakReference;)V

    .line 298
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    new-instance v1, Ljava/lang/ref/WeakReference;

    iget-object v2, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->newInstructionEventListener:Lcom/here/android/mpa/guidance/NavigationManager$NewInstructionEventListener;

    invoke-direct {v1, v2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lcom/here/android/mpa/guidance/NavigationManager;->addNewInstructionEventListener(Ljava/lang/ref/WeakReference;)V

    .line 299
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    new-instance v1, Ljava/lang/ref/WeakReference;

    iget-object v2, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->speedWarningListener:Lcom/here/android/mpa/guidance/NavigationManager$SpeedWarningListener;

    invoke-direct {v1, v2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lcom/here/android/mpa/guidance/NavigationManager;->addSpeedWarningListener(Ljava/lang/ref/WeakReference;)V

    .line 302
    invoke-static {}, Lcom/here/android/mpa/guidance/VoiceCatalog;->getInstance()Lcom/here/android/mpa/guidance/VoiceCatalog;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->voiceCatalog:Lcom/here/android/mpa/guidance/VoiceCatalog;

    .line 303
    invoke-direct {p0}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->downloadTargetVoiceSkin()V

    .line 304
    return-void
.end method

.method private resetUIOnNavigationEnds()V
    .locals 1

    .prologue
    .line 543
    invoke-direct {p0}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->clearNavigationInfo()V

    .line 544
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->updateMenuForNavigationActive(Z)V

    .line 545
    return-void
.end method

.method private sendNavigationEvent()V
    .locals 12

    .prologue
    .line 658
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;

    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mCurrentRoad:Ljava/lang/String;

    iget-object v2, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mPendingTurn:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    iget-object v3, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mDistanceToPendingStreet:Ljava/lang/String;

    iget-object v4, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mPendingStreet:Ljava/lang/String;

    iget-object v5, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mEtaText:Ljava/lang/String;

    iget-object v6, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mSpeedText:Ljava/lang/String;

    const/4 v7, 0x0

    const-wide/16 v10, 0x0

    .line 659
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-direct/range {v0 .. v8}, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;-><init>(Ljava/lang/String;Lcom/navdy/service/library/events/navigation/NavigationTurn;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/events/navigation/NavigationSessionState;Ljava/lang/Long;)V

    .line 660
    .local v0, "navEvent":Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;
    sget-object v1, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Maneuver: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 662
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/AppInstance;->getRemoteDevice()Lcom/navdy/service/library/device/RemoteDevice;

    move-result-object v9

    .line 664
    .local v9, "remoteDevice":Lcom/navdy/service/library/device/RemoteDevice;
    if-eqz v9, :cond_0

    .line 665
    invoke-virtual {v9, v0}, Lcom/navdy/service/library/device/RemoteDevice;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 667
    :cond_0
    return-void
.end method

.method private startNavigation(Z)V
    .locals 7
    .param p1, "simulated"    # Z

    .prologue
    const/4 v6, 0x0

    .line 328
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->updateMenuForNavigationActive(Z)V

    .line 331
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    iget-object v2, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->map:Lcom/here/android/mpa/mapping/Map;

    invoke-virtual {v1, v2}, Lcom/here/android/mpa/guidance/NavigationManager;->setMap(Lcom/here/android/mpa/mapping/Map;)V

    .line 334
    const/4 v0, 0x0

    .line 335
    .local v0, "error":Lcom/here/android/mpa/guidance/NavigationManager$Error;
    if-eqz p1, :cond_1

    .line 336
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    iget-object v2, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->route:Lcom/here/android/mpa/routing/Route;

    invoke-static {}, Lcom/navdy/client/debug/DebugActionsFragment;->getSimulationSpeed()I

    move-result v3

    int-to-long v4, v3

    invoke-virtual {v1, v2, v4, v5}, Lcom/here/android/mpa/guidance/NavigationManager;->simulate(Lcom/here/android/mpa/routing/Route;J)Lcom/here/android/mpa/guidance/NavigationManager$Error;

    move-result-object v0

    .line 341
    :goto_0
    sget-object v1, Lcom/here/android/mpa/guidance/NavigationManager$Error;->NONE:Lcom/here/android/mpa/guidance/NavigationManager$Error;

    if-ne v0, v1, :cond_2

    .line 343
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    sget-object v2, Lcom/here/android/mpa/guidance/NavigationManager$MapUpdateMode;->ROADVIEW:Lcom/here/android/mpa/guidance/NavigationManager$MapUpdateMode;

    invoke-virtual {v1, v2}, Lcom/here/android/mpa/guidance/NavigationManager;->setMapUpdateMode(Lcom/here/android/mpa/guidance/NavigationManager$MapUpdateMode;)Lcom/here/android/mpa/guidance/NavigationManager$Error;

    .line 345
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->voiceSkin:Lcom/here/android/mpa/guidance/VoiceSkin;

    if-eqz v1, :cond_0

    .line 346
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    iget-object v2, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->voiceSkin:Lcom/here/android/mpa/guidance/VoiceSkin;

    invoke-virtual {v1, v2}, Lcom/here/android/mpa/guidance/NavigationManager;->setVoiceSkin(Lcom/here/android/mpa/guidance/VoiceSkin;)Lcom/here/android/mpa/guidance/NavigationManager$Error;

    .line 350
    :cond_0
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->maneuverInfo:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 351
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->navigationInfo:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 355
    :goto_1
    return-void

    .line 338
    :cond_1
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    iget-object v2, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->route:Lcom/here/android/mpa/routing/Route;

    invoke-virtual {v1, v2}, Lcom/here/android/mpa/guidance/NavigationManager;->startNavigation(Lcom/here/android/mpa/routing/Route;)Lcom/here/android/mpa/guidance/NavigationManager$Error;

    move-result-object v0

    goto :goto_0

    .line 353
    :cond_2
    sget-object v1, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Start simulation failed:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/here/android/mpa/guidance/NavigationManager$Error;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private updateManeuver()V
    .locals 5

    .prologue
    .line 592
    iget-object v3, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    invoke-virtual {v3}, Lcom/here/android/mpa/guidance/NavigationManager;->getNextManeuver()Lcom/here/android/mpa/routing/Maneuver;

    move-result-object v2

    .line 593
    .local v2, "nextManeuver":Lcom/here/android/mpa/routing/Maneuver;
    if-eqz v2, :cond_1

    .line 594
    invoke-virtual {v2}, Lcom/here/android/mpa/routing/Maneuver;->getNextRoadName()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mPendingStreet:Ljava/lang/String;

    .line 596
    invoke-virtual {v2}, Lcom/here/android/mpa/routing/Maneuver;->getTurn()Lcom/here/android/mpa/routing/Maneuver$Turn;

    move-result-object v1

    .line 598
    .local v1, "engineNextTurn":Lcom/here/android/mpa/routing/Maneuver$Turn;
    iget-object v3, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mPendingTurn:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    if-eqz v3, :cond_0

    .line 599
    invoke-direct {p0, v1}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->navigationTurnFromEngineTurn(Lcom/here/android/mpa/routing/Maneuver$Turn;)Lcom/navdy/service/library/events/navigation/NavigationTurn;

    move-result-object v3

    iput-object v3, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mPendingTurn:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    .line 602
    :cond_0
    invoke-virtual {v2}, Lcom/here/android/mpa/routing/Maneuver;->getRoadName()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mCurrentRoad:Ljava/lang/String;

    .line 604
    iget-object v3, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->nextRoadTV:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mPendingStreet:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 605
    iget-object v3, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->nextManeuverTurnTV:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mPendingTurn:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 606
    iget-object v3, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->currentRoadTV:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mCurrentRoad:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 608
    iget-object v3, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->maneuverDetailList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 609
    iget-object v3, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->maneuverDetailList:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 610
    iget-object v3, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->maneuverDetailListView:Landroid/widget/ListView;

    invoke-virtual {v3}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/debug/navdebug/ManeuverArrayAdapter;

    .line 611
    .local v0, "adapter":Lcom/navdy/client/debug/navdebug/ManeuverArrayAdapter;
    invoke-virtual {v0}, Lcom/navdy/client/debug/navdebug/ManeuverArrayAdapter;->notifyDataSetChanged()V

    .line 613
    invoke-direct {p0}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->sendNavigationEvent()V

    .line 618
    .end local v0    # "adapter":Lcom/navdy/client/debug/navdebug/ManeuverArrayAdapter;
    .end local v1    # "engineNextTurn":Lcom/here/android/mpa/routing/Maneuver$Turn;
    :goto_0
    return-void

    .line 616
    :cond_1
    sget-object v3, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "No next maneuver."

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private updateMenuForNavigationActive(Z)V
    .locals 4
    .param p1, "active"    # Z

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 358
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mMenu:Landroid/view/Menu;

    const v3, 0x7f100406

    invoke-interface {v0, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    if-nez p1, :cond_0

    move v0, v1

    :goto_0
    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 359
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mMenu:Landroid/view/Menu;

    const v3, 0x7f100407

    invoke-interface {v0, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-nez p1, :cond_1

    :goto_1
    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 360
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mMenu:Landroid/view/Menu;

    const v1, 0x7f100408

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 361
    return-void

    :cond_0
    move v0, v2

    .line 358
    goto :goto_0

    :cond_1
    move v1, v2

    .line 359
    goto :goto_1
.end method

.method private updatePositionInfo(Lcom/here/android/mpa/common/GeoPosition;)V
    .locals 13
    .param p1, "loc"    # Lcom/here/android/mpa/common/GeoPosition;

    .prologue
    const/4 v12, 0x1

    .line 554
    invoke-virtual {p1}, Lcom/here/android/mpa/common/GeoPosition;->getSpeed()D

    move-result-wide v8

    const-wide v10, 0x4099255c20000000L    # 1609.3399658203125

    div-double/2addr v8, v10

    double-to-int v1, v8

    .line 555
    .local v1, "avgSpeed":I
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    const-string v8, "%d mph"

    new-array v9, v12, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v5, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mSpeedText:Ljava/lang/String;

    .line 558
    iget-object v5, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    sget-object v8, Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;->DISABLED:Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;

    invoke-virtual {v5, v12, v8}, Lcom/here/android/mpa/guidance/NavigationManager;->getEta(ZLcom/here/android/mpa/routing/Route$TrafficPenaltyMode;)Ljava/util/Date;

    move-result-object v0

    .line 559
    .local v0, "ETADate":Ljava/util/Date;
    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    .line 560
    .local v4, "now":Ljava/util/Date;
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v10

    sub-long v6, v8, v10

    .line 561
    .local v6, "remainingTimeMs":J
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 562
    .local v2, "eta":Ljava/util/Calendar;
    const/16 v5, 0xd

    const-wide/16 v8, 0x3e8

    div-long v8, v6, v8

    long-to-int v8, v8

    invoke-virtual {v2, v5, v8}, Ljava/util/Calendar;->add(II)V

    .line 563
    iget-object v5, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->etaFormatter:Ljava/text/SimpleDateFormat;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mEtaText:Ljava/lang/String;

    .line 565
    iget-object v5, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    invoke-virtual {v5}, Lcom/here/android/mpa/guidance/NavigationManager;->getNextManeuverDistance()J

    move-result-wide v8

    invoke-virtual {p0, v8, v9}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->imperialUSDistanceStringFromMeters(J)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mDistanceToPendingStreet:Ljava/lang/String;

    .line 567
    iget-object v5, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->distToNextManeuverTV:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mDistanceToPendingStreet:Ljava/lang/String;

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 568
    iget-object v5, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->etaTV:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mEtaText:Ljava/lang/String;

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 569
    iget-object v5, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->currentSpeedTV:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mSpeedText:Ljava/lang/String;

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 571
    iget-object v5, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    invoke-virtual {v5}, Lcom/here/android/mpa/guidance/NavigationManager;->getNextManeuver()Lcom/here/android/mpa/routing/Maneuver;

    move-result-object v3

    .line 572
    .local v3, "nextManeuver":Lcom/here/android/mpa/routing/Maneuver;
    if-eqz v3, :cond_0

    .line 573
    invoke-virtual {v3}, Lcom/here/android/mpa/routing/Maneuver;->getNextRoadName()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mPendingStreet:Ljava/lang/String;

    .line 574
    invoke-virtual {v3}, Lcom/here/android/mpa/routing/Maneuver;->getRoadName()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mCurrentRoad:Ljava/lang/String;

    .line 576
    iget-object v5, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->nextRoadTV:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mPendingStreet:Ljava/lang/String;

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 577
    iget-object v5, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->currentRoadTV:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mCurrentRoad:Ljava/lang/String;

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 579
    :cond_0
    invoke-direct {p0}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->sendNavigationEvent()V

    .line 580
    return-void
.end method


# virtual methods
.method public addLaneInfo(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/guidance/LaneInformation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 621
    .local p1, "laneInfoList":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/guidance/LaneInformation;>;"
    if-nez p1, :cond_0

    .line 622
    sget-object v1, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "empty lane info"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 635
    :goto_0
    return-void

    .line 626
    :cond_0
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mLaneInfoList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 628
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/here/android/mpa/guidance/LaneInformation;

    .line 629
    .local v0, "laneInfo":Lcom/here/android/mpa/guidance/LaneInformation;
    iget-object v2, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mLaneInfoList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 630
    iget-object v2, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mLaneInfoList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 634
    .end local v0    # "laneInfo":Lcom/here/android/mpa/guidance/LaneInformation;
    :cond_2
    invoke-virtual {p0}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->refreshLaneInfoDisplay()V

    goto :goto_0
.end method

.method public clearLaneInfo()V
    .locals 1

    .prologue
    .line 639
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mLaneInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 640
    invoke-virtual {p0}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->refreshLaneInfoDisplay()V

    .line 641
    return-void
.end method

.method public imperialUSDistanceStringFromMeters(J)Ljava/lang/String;
    .locals 7
    .param p1, "meters"    # J

    .prologue
    .line 671
    const/4 v2, 0x0

    .line 675
    .local v2, "distanceValue":F
    long-to-float v3, p1

    const v4, 0x4320ef1b

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_0

    .line 676
    long-to-float v3, p1

    const v4, 0x44c92ae1

    div-float v2, v3, v4

    .line 677
    const-string v1, "mi"

    .line 678
    .local v1, "distanceUnits":Ljava/lang/String;
    const-string v0, "%.1f %s"

    .line 685
    .local v0, "distanceFormat":Ljava/lang/String;
    :goto_0
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object v1, v3, v4

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 680
    .end local v0    # "distanceFormat":Ljava/lang/String;
    .end local v1    # "distanceUnits":Ljava/lang/String;
    :cond_0
    long-to-float v3, p1

    const v4, 0x4051f948

    mul-float v2, v3, v4

    .line 681
    const-string v1, "ft"

    .line 682
    .restart local v1    # "distanceUnits":Ljava/lang/String;
    const-string v0, "%.0f %s"

    .restart local v0    # "distanceFormat":Ljava/lang/String;
    goto :goto_0
.end method

.method public loadBundleArguments()V
    .locals 10

    .prologue
    const/4 v4, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 194
    invoke-virtual {p0}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "start_coords"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getDoubleArrayExtra(Ljava/lang/String;)[D

    move-result-object v1

    .line 195
    .local v1, "startLatLon":[D
    invoke-virtual {p0}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "dest_coords"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getDoubleArrayExtra(Ljava/lang/String;)[D

    move-result-object v0

    .line 197
    .local v0, "endLatLon":[D
    if-eqz v1, :cond_0

    array-length v2, v1

    if-eq v2, v4, :cond_1

    .line 198
    :cond_0
    sget-object v2, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Missing valid start coordinates. Falling back."

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 199
    sget-object v1, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->DEFAULT_START_POINT:[D

    .line 202
    :cond_1
    if-eqz v0, :cond_2

    array-length v2, v0

    if-eq v2, v4, :cond_3

    .line 203
    :cond_2
    sget-object v2, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Missing end coordinates. Falling back."

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 204
    sget-object v0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->DEFAULT_DESTINATION:[D

    .line 207
    :cond_3
    new-instance v2, Lcom/here/android/mpa/common/GeoCoordinate;

    aget-wide v4, v1, v8

    aget-wide v6, v1, v9

    invoke-direct {v2, v4, v5, v6, v7}, Lcom/here/android/mpa/common/GeoCoordinate;-><init>(DD)V

    iput-object v2, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mStartCoord:Lcom/here/android/mpa/common/GeoCoordinate;

    .line 208
    new-instance v2, Lcom/here/android/mpa/common/GeoCoordinate;

    aget-wide v4, v0, v8

    aget-wide v6, v0, v9

    invoke-direct {v2, v4, v5, v6, v7}, Lcom/here/android/mpa/common/GeoCoordinate;-><init>(DD)V

    iput-object v2, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mEndCoord:Lcom/here/android/mpa/common/GeoCoordinate;

    .line 209
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 145
    invoke-super {p0, p1}, Lcom/here/android/mpa/common/MapActivity;->onCreate(Landroid/os/Bundle;)V

    .line 146
    const v1, 0x7f030021

    invoke-virtual {p0, v1}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->setContentView(I)V

    .line 148
    invoke-virtual {p0}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->loadBundleArguments()V

    .line 151
    invoke-virtual {p0}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const v2, 0x7f1000ad

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/here/android/mpa/mapping/MapFragment;

    iput-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mapFragment:Lcom/here/android/mpa/mapping/MapFragment;

    .line 152
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mapFragment:Lcom/here/android/mpa/mapping/MapFragment;

    if-eqz v1, :cond_0

    .line 153
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mapFragment:Lcom/here/android/mpa/mapping/MapFragment;

    new-instance v2, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$1;

    invoke-direct {v2, p0}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$1;-><init>(Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;)V

    invoke-virtual {v1, v2}, Lcom/here/android/mpa/mapping/MapFragment;->init(Lcom/here/android/mpa/common/OnEngineInitListener;)V

    .line 167
    :cond_0
    const v1, 0x7f1000c7

    invoke-virtual {p0, v1}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->maneuverInfo:Landroid/widget/RelativeLayout;

    .line 168
    const v1, 0x7f1000ce

    invoke-virtual {p0, v1}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->navigationInfo:Landroid/widget/RelativeLayout;

    .line 169
    const v1, 0x7f1000cf

    invoke-virtual {p0, v1}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->currentRoadTV:Landroid/widget/TextView;

    .line 170
    const v1, 0x7f1000d0

    invoke-virtual {p0, v1}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->currentSpeedTV:Landroid/widget/TextView;

    .line 171
    const v1, 0x7f1000d2

    invoke-virtual {p0, v1}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->etaTV:Landroid/widget/TextView;

    .line 172
    const v1, 0x7f1000c9

    invoke-virtual {p0, v1}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->nextManeuverTurnTV:Landroid/widget/TextView;

    .line 173
    const v1, 0x7f1000c8

    invoke-virtual {p0, v1}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->nextRoadTV:Landroid/widget/TextView;

    .line 174
    const v1, 0x7f1000ca

    invoke-virtual {p0, v1}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->distToNextManeuverTV:Landroid/widget/TextView;

    .line 175
    const v1, 0x7f1000d1

    invoke-virtual {p0, v1}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->speedLimitTV:Landroid/widget/TextView;

    .line 177
    invoke-static {p0}, Lbutterknife/ButterKnife;->inject(Landroid/app/Activity;)V

    .line 179
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->maneuverDetailList:Ljava/util/ArrayList;

    .line 180
    new-instance v0, Lcom/navdy/client/debug/navdebug/ManeuverArrayAdapter;

    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->maneuverDetailList:Ljava/util/ArrayList;

    invoke-direct {v0, p0, v1}, Lcom/navdy/client/debug/navdebug/ManeuverArrayAdapter;-><init>(Landroid/app/Activity;Ljava/util/List;)V

    .line 181
    .local v0, "maneuverArrayAdapter":Lcom/navdy/client/debug/navdebug/ManeuverArrayAdapter;
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->maneuverDetailListView:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 183
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mLaneInfoList:Ljava/util/ArrayList;

    .line 184
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 241
    invoke-virtual {p0}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const/high16 v1, 0x7f110000

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 242
    iput-object p1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mMenu:Landroid/view/Menu;

    .line 243
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 188
    invoke-super {p0}, Lcom/here/android/mpa/common/MapActivity;->onDestroy()V

    .line 189
    sget-object v0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "destroy!"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 190
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->navigationManagerListener:Lcom/here/android/mpa/guidance/NavigationManager$NavigationManagerEventListener;

    invoke-virtual {v0, v1}, Lcom/here/android/mpa/guidance/NavigationManager;->removeNavigationManagerEventListener(Lcom/here/android/mpa/guidance/NavigationManager$NavigationManagerEventListener;)V

    .line 191
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 248
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    move v0, v1

    .line 261
    :goto_0
    return v0

    .line 250
    :pswitch_0
    invoke-direct {p0, v1}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->startNavigation(Z)V

    goto :goto_0

    .line 253
    :pswitch_1
    invoke-direct {p0, v0}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->startNavigation(Z)V

    goto :goto_0

    .line 256
    :pswitch_2
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    invoke-virtual {v1}, Lcom/here/android/mpa/guidance/NavigationManager;->stop()V

    .line 257
    invoke-direct {p0}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->resetUIOnNavigationEnds()V

    goto :goto_0

    .line 248
    :pswitch_data_0
    .packed-switch 0x7f100406
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v2, 0x1

    .line 266
    invoke-super {p0, p1}, Lcom/here/android/mpa/common/MapActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    .line 267
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    if-eqz v0, :cond_0

    .line 269
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    invoke-virtual {v0}, Lcom/here/android/mpa/guidance/NavigationManager;->getRunningState()Lcom/here/android/mpa/guidance/NavigationManager$NavigationState;

    move-result-object v0

    sget-object v1, Lcom/here/android/mpa/guidance/NavigationManager$NavigationState;->RUNNING:Lcom/here/android/mpa/guidance/NavigationManager$NavigationState;

    invoke-virtual {v0, v1}, Lcom/here/android/mpa/guidance/NavigationManager$NavigationState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 270
    invoke-direct {p0, v2}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->updateMenuForNavigationActive(Z)V

    .line 273
    :cond_0
    return v2
.end method

.method public onResume()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 230
    invoke-super {p0}, Lcom/here/android/mpa/common/MapActivity;->onResume()V

    .line 231
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    if-eqz v0, :cond_0

    .line 232
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    invoke-virtual {v0}, Lcom/here/android/mpa/guidance/NavigationManager;->getRunningState()Lcom/here/android/mpa/guidance/NavigationManager$NavigationState;

    move-result-object v0

    sget-object v1, Lcom/here/android/mpa/guidance/NavigationManager$NavigationState;->RUNNING:Lcom/here/android/mpa/guidance/NavigationManager$NavigationState;

    invoke-virtual {v0, v1}, Lcom/here/android/mpa/guidance/NavigationManager$NavigationState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 233
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->maneuverInfo:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 234
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->navigationInfo:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 237
    :cond_0
    return-void
.end method

.method protected onToggleClicked()V
    .locals 2
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f1000cd
        }
    .end annotation

    .prologue
    .line 584
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    invoke-virtual {v0}, Lcom/here/android/mpa/guidance/NavigationManager;->getRunningState()Lcom/here/android/mpa/guidance/NavigationManager$NavigationState;

    move-result-object v0

    sget-object v1, Lcom/here/android/mpa/guidance/NavigationManager$NavigationState;->PAUSED:Lcom/here/android/mpa/guidance/NavigationManager$NavigationState;

    invoke-virtual {v0, v1}, Lcom/here/android/mpa/guidance/NavigationManager$NavigationState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 585
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    invoke-virtual {v0}, Lcom/here/android/mpa/guidance/NavigationManager;->resume()Lcom/here/android/mpa/guidance/NavigationManager$Error;

    .line 589
    :cond_0
    :goto_0
    return-void

    .line 586
    :cond_1
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    invoke-virtual {v0}, Lcom/here/android/mpa/guidance/NavigationManager;->getRunningState()Lcom/here/android/mpa/guidance/NavigationManager$NavigationState;

    move-result-object v0

    sget-object v1, Lcom/here/android/mpa/guidance/NavigationManager$NavigationState;->RUNNING:Lcom/here/android/mpa/guidance/NavigationManager$NavigationState;

    invoke-virtual {v0, v1}, Lcom/here/android/mpa/guidance/NavigationManager$NavigationState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 587
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->navigationManager:Lcom/here/android/mpa/guidance/NavigationManager;

    invoke-virtual {v0}, Lcom/here/android/mpa/guidance/NavigationManager;->pause()V

    goto :goto_0
.end method

.method public refreshLaneInfoDisplay()V
    .locals 8

    .prologue
    .line 644
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 645
    .local v4, "laneTextList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v5, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->mLaneInfoList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/here/android/mpa/guidance/LaneInformation;

    .line 646
    .local v2, "laneInfo":Lcom/here/android/mpa/guidance/LaneInformation;
    invoke-virtual {v2}, Lcom/here/android/mpa/guidance/LaneInformation;->getDirections()Ljava/util/EnumSet;

    move-result-object v0

    .line 647
    .local v0, "directionSet":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/here/android/mpa/guidance/LaneInformation$Direction;>;"
    const-string v6, ", "

    invoke-static {v6, v0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    .line 648
    .local v1, "directions":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 651
    .end local v0    # "directionSet":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/here/android/mpa/guidance/LaneInformation$Direction;>;"
    .end local v1    # "directions":Ljava/lang/String;
    .end local v2    # "laneInfo":Lcom/here/android/mpa/guidance/LaneInformation;
    :cond_0
    const-string v5, "; "

    invoke-static {v5, v4}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v3

    .line 652
    .local v3, "laneText":Ljava/lang/String;
    sget-object v5, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Lanes: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 653
    iget-object v5, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->laneInfo:Landroid/widget/TextView;

    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 655
    return-void
.end method

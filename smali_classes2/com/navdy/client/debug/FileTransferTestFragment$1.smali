.class Lcom/navdy/client/debug/FileTransferTestFragment$1;
.super Ljava/lang/Object;
.source "FileTransferTestFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/debug/FileTransferTestFragment;->recordTransferComplete()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/debug/FileTransferTestFragment;


# direct methods
.method constructor <init>(Lcom/navdy/client/debug/FileTransferTestFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/debug/FileTransferTestFragment;

    .prologue
    .line 102
    iput-object p1, p0, Lcom/navdy/client/debug/FileTransferTestFragment$1;->this$0:Lcom/navdy/client/debug/FileTransferTestFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .prologue
    const/4 v10, 0x1

    .line 104
    iget-object v3, p0, Lcom/navdy/client/debug/FileTransferTestFragment$1;->this$0:Lcom/navdy/client/debug/FileTransferTestFragment;

    iget-object v3, v3, Lcom/navdy/client/debug/FileTransferTestFragment;->transferProgress:Landroid/widget/ProgressBar;

    iget-object v4, p0, Lcom/navdy/client/debug/FileTransferTestFragment$1;->this$0:Lcom/navdy/client/debug/FileTransferTestFragment;

    invoke-static {v4}, Lcom/navdy/client/debug/FileTransferTestFragment;->access$000(Lcom/navdy/client/debug/FileTransferTestFragment;)J

    move-result-wide v4

    long-to-int v4, v4

    invoke-virtual {v3, v4}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 105
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-object v3, p0, Lcom/navdy/client/debug/FileTransferTestFragment$1;->this$0:Lcom/navdy/client/debug/FileTransferTestFragment;

    invoke-static {v3}, Lcom/navdy/client/debug/FileTransferTestFragment;->access$100(Lcom/navdy/client/debug/FileTransferTestFragment;)J

    move-result-wide v6

    sub-long v0, v4, v6

    .line 106
    .local v0, "elapsedMs":J
    iget-object v3, p0, Lcom/navdy/client/debug/FileTransferTestFragment$1;->this$0:Lcom/navdy/client/debug/FileTransferTestFragment;

    invoke-static {v3}, Lcom/navdy/client/debug/FileTransferTestFragment;->access$000(Lcom/navdy/client/debug/FileTransferTestFragment;)J

    move-result-wide v4

    long-to-float v3, v4

    long-to-float v4, v0

    div-float v2, v3, v4

    .line 107
    .local v2, "rate":F
    iget-object v3, p0, Lcom/navdy/client/debug/FileTransferTestFragment$1;->this$0:Lcom/navdy/client/debug/FileTransferTestFragment;

    iget-object v3, v3, Lcom/navdy/client/debug/FileTransferTestFragment;->transferMessage:Landroid/widget/TextView;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    const-string v5, "transferred %d bytes in %d ms, %6.2f Kb/sec."

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/navdy/client/debug/FileTransferTestFragment$1;->this$0:Lcom/navdy/client/debug/FileTransferTestFragment;

    .line 109
    invoke-static {v8}, Lcom/navdy/client/debug/FileTransferTestFragment;->access$000(Lcom/navdy/client/debug/FileTransferTestFragment;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    .line 110
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v6, v10

    const/4 v7, 0x2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    aput-object v8, v6, v7

    .line 107
    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 111
    iget-object v3, p0, Lcom/navdy/client/debug/FileTransferTestFragment$1;->this$0:Lcom/navdy/client/debug/FileTransferTestFragment;

    iget-object v3, v3, Lcom/navdy/client/debug/FileTransferTestFragment;->transferButton:Landroid/widget/Button;

    invoke-virtual {v3, v10}, Landroid/widget/Button;->setEnabled(Z)V

    .line 112
    return-void
.end method

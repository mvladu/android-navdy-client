.class Lcom/navdy/client/debug/SubmitTicketFragment$1$1;
.super Ljava/lang/Object;
.source "SubmitTicketFragment.java"

# interfaces
.implements Lcom/navdy/client/debug/file/FileTransferManager$FileTransferListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/debug/SubmitTicketFragment$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/client/debug/SubmitTicketFragment$1;

.field final synthetic val$logsFolder:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/navdy/client/debug/SubmitTicketFragment$1;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/client/debug/SubmitTicketFragment$1;

    .prologue
    .line 155
    iput-object p1, p0, Lcom/navdy/client/debug/SubmitTicketFragment$1$1;->this$1:Lcom/navdy/client/debug/SubmitTicketFragment$1;

    iput-object p2, p0, Lcom/navdy/client/debug/SubmitTicketFragment$1$1;->val$logsFolder:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/navdy/service/library/events/file/FileTransferError;Ljava/lang/String;)V
    .locals 5
    .param p1, "errorCode"    # Lcom/navdy/service/library/events/file/FileTransferError;
    .param p2, "error"    # Ljava/lang/String;

    .prologue
    .line 184
    iget-object v0, p0, Lcom/navdy/client/debug/SubmitTicketFragment$1$1;->this$1:Lcom/navdy/client/debug/SubmitTicketFragment$1;

    iget-object v0, v0, Lcom/navdy/client/debug/SubmitTicketFragment$1;->this$0:Lcom/navdy/client/debug/SubmitTicketFragment;

    invoke-virtual {v0}, Lcom/navdy/client/debug/SubmitTicketFragment;->removeDialog()V

    .line 185
    iget-object v0, p0, Lcom/navdy/client/debug/SubmitTicketFragment$1$1;->this$1:Lcom/navdy/client/debug/SubmitTicketFragment$1;

    iget-object v0, v0, Lcom/navdy/client/debug/SubmitTicketFragment$1;->this$0:Lcom/navdy/client/debug/SubmitTicketFragment;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/navdy/client/debug/SubmitTicketFragment$1$1;->this$1:Lcom/navdy/client/debug/SubmitTicketFragment$1;

    iget-object v2, v2, Lcom/navdy/client/debug/SubmitTicketFragment$1;->this$0:Lcom/navdy/client/debug/SubmitTicketFragment;

    const v3, 0x7f080160

    invoke-virtual {v2, v3}, Lcom/navdy/client/debug/SubmitTicketFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error collecting display logs "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/navdy/client/debug/SubmitTicketFragment;->showSimpleDialog(ILjava/lang/String;Ljava/lang/String;)V

    .line 186
    return-void
.end method

.method public onFileTransferResponse(Lcom/navdy/service/library/events/file/FileTransferResponse;)V
    .locals 5
    .param p1, "response"    # Lcom/navdy/service/library/events/file/FileTransferResponse;

    .prologue
    .line 158
    iget-object v1, p1, Lcom/navdy/service/library/events/file/FileTransferResponse;->success:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_0

    .line 159
    iget-object v1, p0, Lcom/navdy/client/debug/SubmitTicketFragment$1$1;->this$1:Lcom/navdy/client/debug/SubmitTicketFragment$1;

    iget-object v1, v1, Lcom/navdy/client/debug/SubmitTicketFragment$1;->this$0:Lcom/navdy/client/debug/SubmitTicketFragment;

    invoke-virtual {v1}, Lcom/navdy/client/debug/SubmitTicketFragment;->removeDialog()V

    .line 160
    iget-object v1, p0, Lcom/navdy/client/debug/SubmitTicketFragment$1$1;->this$1:Lcom/navdy/client/debug/SubmitTicketFragment$1;

    iget-object v1, v1, Lcom/navdy/client/debug/SubmitTicketFragment$1;->this$0:Lcom/navdy/client/debug/SubmitTicketFragment;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/navdy/client/debug/SubmitTicketFragment$1$1;->this$1:Lcom/navdy/client/debug/SubmitTicketFragment$1;

    iget-object v3, v3, Lcom/navdy/client/debug/SubmitTicketFragment$1;->this$0:Lcom/navdy/client/debug/SubmitTicketFragment;

    const v4, 0x7f080160

    invoke-virtual {v3, v4}, Lcom/navdy/client/debug/SubmitTicketFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "Error collecting display logs"

    invoke-virtual {v1, v2, v3, v4}, Lcom/navdy/client/debug/SubmitTicketFragment;->showSimpleDialog(ILjava/lang/String;Ljava/lang/String;)V

    .line 165
    :goto_0
    return-void

    .line 162
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/file/FileTransferResponse;->destinationFileName:Ljava/lang/String;

    .line 163
    .local v0, "receivedFileName":Ljava/lang/String;
    iget-object v1, p0, Lcom/navdy/client/debug/SubmitTicketFragment$1$1;->this$1:Lcom/navdy/client/debug/SubmitTicketFragment$1;

    iget-object v1, v1, Lcom/navdy/client/debug/SubmitTicketFragment$1;->this$0:Lcom/navdy/client/debug/SubmitTicketFragment;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/navdy/client/debug/SubmitTicketFragment$1$1;->val$logsFolder:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/navdy/client/debug/SubmitTicketFragment;->access$202(Lcom/navdy/client/debug/SubmitTicketFragment;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0
.end method

.method public onFileTransferStatus(Lcom/navdy/service/library/events/file/FileTransferStatus;)V
    .locals 4
    .param p1, "status"    # Lcom/navdy/service/library/events/file/FileTransferStatus;

    .prologue
    .line 169
    iget-object v0, p1, Lcom/navdy/service/library/events/file/FileTransferStatus;->success:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/navdy/service/library/events/file/FileTransferStatus;->transferComplete:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 170
    iget-object v0, p0, Lcom/navdy/client/debug/SubmitTicketFragment$1$1;->this$1:Lcom/navdy/client/debug/SubmitTicketFragment$1;

    iget-object v0, v0, Lcom/navdy/client/debug/SubmitTicketFragment$1;->this$0:Lcom/navdy/client/debug/SubmitTicketFragment;

    invoke-virtual {v0}, Lcom/navdy/client/debug/SubmitTicketFragment;->removeDialog()V

    .line 171
    iget-object v0, p0, Lcom/navdy/client/debug/SubmitTicketFragment$1$1;->this$1:Lcom/navdy/client/debug/SubmitTicketFragment$1;

    iget-object v0, v0, Lcom/navdy/client/debug/SubmitTicketFragment$1;->this$0:Lcom/navdy/client/debug/SubmitTicketFragment;

    invoke-static {v0}, Lcom/navdy/client/debug/SubmitTicketFragment;->access$300(Lcom/navdy/client/debug/SubmitTicketFragment;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/debug/SubmitTicketFragment$1$1$1;

    invoke-direct {v1, p0}, Lcom/navdy/client/debug/SubmitTicketFragment$1$1$1;-><init>(Lcom/navdy/client/debug/SubmitTicketFragment$1$1;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 177
    iget-object v0, p0, Lcom/navdy/client/debug/SubmitTicketFragment$1$1;->this$1:Lcom/navdy/client/debug/SubmitTicketFragment$1;

    iget-object v0, v0, Lcom/navdy/client/debug/SubmitTicketFragment$1;->this$0:Lcom/navdy/client/debug/SubmitTicketFragment;

    const/4 v1, 0x3

    const-string v2, ""

    const-string v3, "Submitting ticket"

    invoke-virtual {v0, v1, v2, v3}, Lcom/navdy/client/debug/SubmitTicketFragment;->showSimpleDialog(ILjava/lang/String;Ljava/lang/String;)V

    .line 178
    iget-object v0, p0, Lcom/navdy/client/debug/SubmitTicketFragment$1$1;->this$1:Lcom/navdy/client/debug/SubmitTicketFragment$1;

    iget-object v0, v0, Lcom/navdy/client/debug/SubmitTicketFragment$1;->this$0:Lcom/navdy/client/debug/SubmitTicketFragment;

    invoke-static {v0}, Lcom/navdy/client/debug/SubmitTicketFragment;->access$400(Lcom/navdy/client/debug/SubmitTicketFragment;)V

    .line 180
    :cond_0
    return-void
.end method

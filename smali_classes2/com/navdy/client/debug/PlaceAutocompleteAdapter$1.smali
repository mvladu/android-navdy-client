.class Lcom/navdy/client/debug/PlaceAutocompleteAdapter$1;
.super Landroid/widget/Filter;
.source "PlaceAutocompleteAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/debug/PlaceAutocompleteAdapter;->getFilter()Landroid/widget/Filter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/debug/PlaceAutocompleteAdapter;


# direct methods
.method constructor <init>(Lcom/navdy/client/debug/PlaceAutocompleteAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/debug/PlaceAutocompleteAdapter;

    .prologue
    .line 134
    iput-object p1, p0, Lcom/navdy/client/debug/PlaceAutocompleteAdapter$1;->this$0:Lcom/navdy/client/debug/PlaceAutocompleteAdapter;

    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    return-void
.end method


# virtual methods
.method protected performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .locals 3
    .param p1, "constraint"    # Ljava/lang/CharSequence;

    .prologue
    .line 137
    new-instance v0, Landroid/widget/Filter$FilterResults;

    invoke-direct {v0}, Landroid/widget/Filter$FilterResults;-><init>()V

    .line 139
    .local v0, "results":Landroid/widget/Filter$FilterResults;
    if-eqz p1, :cond_0

    .line 141
    iget-object v1, p0, Lcom/navdy/client/debug/PlaceAutocompleteAdapter$1;->this$0:Lcom/navdy/client/debug/PlaceAutocompleteAdapter;

    iget-object v2, p0, Lcom/navdy/client/debug/PlaceAutocompleteAdapter$1;->this$0:Lcom/navdy/client/debug/PlaceAutocompleteAdapter;

    invoke-static {v2, p1}, Lcom/navdy/client/debug/PlaceAutocompleteAdapter;->access$100(Lcom/navdy/client/debug/PlaceAutocompleteAdapter;Ljava/lang/CharSequence;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/navdy/client/debug/PlaceAutocompleteAdapter;->access$002(Lcom/navdy/client/debug/PlaceAutocompleteAdapter;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 142
    iget-object v1, p0, Lcom/navdy/client/debug/PlaceAutocompleteAdapter$1;->this$0:Lcom/navdy/client/debug/PlaceAutocompleteAdapter;

    invoke-static {v1}, Lcom/navdy/client/debug/PlaceAutocompleteAdapter;->access$000(Lcom/navdy/client/debug/PlaceAutocompleteAdapter;)Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 144
    iget-object v1, p0, Lcom/navdy/client/debug/PlaceAutocompleteAdapter$1;->this$0:Lcom/navdy/client/debug/PlaceAutocompleteAdapter;

    invoke-static {v1}, Lcom/navdy/client/debug/PlaceAutocompleteAdapter;->access$000(Lcom/navdy/client/debug/PlaceAutocompleteAdapter;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, v0, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 145
    iget-object v1, p0, Lcom/navdy/client/debug/PlaceAutocompleteAdapter$1;->this$0:Lcom/navdy/client/debug/PlaceAutocompleteAdapter;

    invoke-static {v1}, Lcom/navdy/client/debug/PlaceAutocompleteAdapter;->access$000(Lcom/navdy/client/debug/PlaceAutocompleteAdapter;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iput v1, v0, Landroid/widget/Filter$FilterResults;->count:I

    .line 148
    :cond_0
    return-object v0
.end method

.method protected publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .locals 1
    .param p1, "constraint"    # Ljava/lang/CharSequence;
    .param p2, "results"    # Landroid/widget/Filter$FilterResults;

    .prologue
    .line 153
    if-eqz p2, :cond_0

    iget v0, p2, Landroid/widget/Filter$FilterResults;->count:I

    if-lez v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/navdy/client/debug/PlaceAutocompleteAdapter$1;->this$0:Lcom/navdy/client/debug/PlaceAutocompleteAdapter;

    invoke-virtual {v0}, Lcom/navdy/client/debug/PlaceAutocompleteAdapter;->notifyDataSetChanged()V

    .line 160
    :goto_0
    return-void

    .line 158
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/debug/PlaceAutocompleteAdapter$1;->this$0:Lcom/navdy/client/debug/PlaceAutocompleteAdapter;

    invoke-virtual {v0}, Lcom/navdy/client/debug/PlaceAutocompleteAdapter;->notifyDataSetInvalidated()V

    goto :goto_0
.end method

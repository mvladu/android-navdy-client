.class Lcom/navdy/client/app/providers/NavdyContentProvider$DatabaseHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "NavdyContentProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/providers/NavdyContentProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DatabaseHelper"
.end annotation


# instance fields
.field context:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 464
    const-string v0, "navdy"

    const/4 v1, 0x0

    const/16 v2, 0x13

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 466
    iput-object p1, p0, Lcom/navdy/client/app/providers/NavdyContentProvider$DatabaseHelper;->context:Landroid/content/Context;

    .line 467
    return-void
.end method

.method private dumpAndRecreateDb(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 538
    const-string v0, "DROP TABLE IF EXISTS destinations"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 539
    const-string v0, "DROP TABLE IF EXISTS trips"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 540
    const-string v0, "DROP TABLE IF EXISTS search_history"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 541
    const-string v0, "DROP TABLE IF EXISTS destination_cache"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 542
    const-string v0, "DROP TABLE IF EXISTS playlists"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 543
    const-string v0, "DROP TABLE IF EXISTS playlist_members"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 544
    invoke-virtual {p0, p1}, Lcom/navdy/client/app/providers/NavdyContentProvider$DatabaseHelper;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 545
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 471
    const-string v0, " CREATE TABLE IF NOT EXISTS destinations ( _id INTEGER PRIMARY KEY, place_id TEXT, last_place_id_refresh INTEGER, displat REAL, displong REAL, navlat REAL, navlong REAL, place_name TEXT NOT NULL, address TEXT NOT NULL, street_number TEXT, street_name TEXT, city TEXT, state TEXT, zip_code TEXT, country TEXT, countryCode TEXT, do_not_suggest INT, last_routed_date INTEGER, favorite_label TEXT, favorite_listing_order INTEGER, is_special INTEGER, place_detail_json TEXT, precision_level INTEGER, type INTEGER, contact_lookup_key TEXT, last_known_contact_id INTEGER, last_contact_lookup INTEGER);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 472
    const-string v0, " CREATE TABLE IF NOT EXISTS trips ( _id INTEGER PRIMARY KEY, trip_number INTEGER, start_time INTEGER, start_time_zone_n_dst INTEGER, start_odometer INTEGER, start_lat REAL, start_long REAL, end_time INTEGER, end_odometer INTEGER, end_lat REAL, end_long REAL, destination_id INTEGER, arrived_at_destination INTEGER);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 473
    const-string v0, " CREATE TABLE IF NOT EXISTS search_history ( _id INTEGER PRIMARY KEY, last_searched_on INTEGER, query TEXT);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 474
    const-string v0, " CREATE TABLE IF NOT EXISTS destination_cache ( _id INTEGER PRIMARY KEY, last_response_date INTEGER, location TEXT, destination_id INTEGER);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 475
    const-string v0, " CREATE TABLE IF NOT EXISTS playlists ( playlist_id INTEGER, playlist_name STRING );"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 476
    const-string v0, " CREATE TABLE IF NOT EXISTS playlist_members ( playlist_id INTEGER, SourceId STRING, artist STRING, album STRING, title STRING );"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 477
    return-void
.end method

.method public onDowngrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 534
    invoke-direct {p0, p1}, Lcom/navdy/client/app/providers/NavdyContentProvider$DatabaseHelper;->dumpAndRecreateDb(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 535
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 5
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 481
    invoke-static {}, Lcom/navdy/client/app/providers/NavdyContentProvider;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onUpgrade oldVersion: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " newVersion: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 483
    const/4 v2, 0x7

    if-gt p2, v2, :cond_0

    .line 484
    const-string v2, "ALTER TABLE destinations ADD COLUMN place_detail_json TEXT"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 487
    :cond_0
    const/16 v2, 0x8

    if-gt p2, v2, :cond_1

    .line 488
    const-string v2, " CREATE TABLE IF NOT EXISTS search_history ( _id INTEGER PRIMARY KEY, last_searched_on INTEGER, query TEXT);"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 490
    :cond_1
    const/16 v2, 0x9

    if-gt p2, v2, :cond_2

    .line 491
    const-string v2, " CREATE TABLE IF NOT EXISTS destination_cache ( _id INTEGER PRIMARY KEY, last_response_date INTEGER, location TEXT, destination_id INTEGER);"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 492
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ALTER TABLE destinations ADD COLUMN precision_level INTEGER DEFAULT "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/navdy/client/app/framework/models/Destination$Precision;->UNKNOWN:Lcom/navdy/client/app/framework/models/Destination$Precision;

    .line 494
    invoke-virtual {v3}, Lcom/navdy/client/app/framework/models/Destination$Precision;->getValue()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 492
    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 496
    :cond_2
    const/16 v2, 0xa

    if-gt p2, v2, :cond_3

    .line 497
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ALTER TABLE destinations ADD COLUMN type INTEGER DEFAULT "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/navdy/client/app/framework/models/Destination$Type;->UNKNOWN:Lcom/navdy/client/app/framework/models/Destination$Type;

    .line 499
    invoke-virtual {v3}, Lcom/navdy/client/app/framework/models/Destination$Type;->getValue()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 497
    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 501
    invoke-static {p1}, Lcom/navdy/client/app/providers/NavdyContentProvider;->access$100(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 503
    :cond_3
    const/16 v2, 0xb

    if-gt p2, v2, :cond_4

    .line 504
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    .line 505
    .local v0, "time":J
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ALTER TABLE trips ADD COLUMN start_time_zone_n_dst INTEGER DEFAULT "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 507
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v3}, Lcom/navdy/client/app/framework/util/SystemUtils;->getTimeZoneAndDaylightSavingOffset(Ljava/lang/Long;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 505
    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 509
    .end local v0    # "time":J
    :cond_4
    const/16 v2, 0xc

    if-gt p2, v2, :cond_5

    .line 510
    const-string v2, " CREATE TABLE IF NOT EXISTS playlists ( playlist_id INTEGER, playlist_name STRING );"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 511
    const-string v2, " CREATE TABLE IF NOT EXISTS playlist_members ( playlist_id INTEGER, SourceId STRING, artist STRING, album STRING, title STRING );"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 513
    :cond_5
    const/16 v2, 0xd

    if-gt p2, v2, :cond_6

    .line 514
    const-string v2, "ALTER TABLE destinations ADD COLUMN countryCode TEXT"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 517
    invoke-static {p1}, Lcom/navdy/client/app/providers/NavdyContentProvider;->access$200(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 519
    :cond_6
    const/16 v2, 0xe

    if-gt p2, v2, :cond_7

    .line 520
    const-string v2, "ALTER TABLE destinations ADD COLUMN contact_lookup_key TEXT"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 522
    const-string v2, "ALTER TABLE destinations ADD COLUMN last_known_contact_id INTEGER"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 524
    const-string v2, "ALTER TABLE destinations ADD COLUMN last_contact_lookup INTEGER"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 527
    :cond_7
    const/16 v2, 0x12

    if-gt p2, v2, :cond_8

    .line 528
    iget-object v2, p0, Lcom/navdy/client/app/providers/NavdyContentProvider$DatabaseHelper;->context:Landroid/content/Context;

    invoke-static {p1, v2}, Lcom/navdy/client/app/providers/NavdyContentProvider;->access$300(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;)V

    .line 530
    :cond_8
    return-void
.end method

.class Lcom/navdy/client/app/ui/glances/AppNotificationsActivity$LoadApplications;
.super Landroid/os/AsyncTask;
.source "AppNotificationsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadApplications"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;


# direct methods
.method private constructor <init>(Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;)V
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Lcom/navdy/client/app/ui/glances/AppNotificationsActivity$LoadApplications;->this$0:Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;Lcom/navdy/client/app/ui/glances/AppNotificationsActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;
    .param p2, "x1"    # Lcom/navdy/client/app/ui/glances/AppNotificationsActivity$1;

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/navdy/client/app/ui/glances/AppNotificationsActivity$LoadApplications;-><init>(Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 62
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/navdy/client/app/ui/glances/AppNotificationsActivity$LoadApplications;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 9
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    const/4 v8, 0x1

    .line 66
    iget-object v5, p0, Lcom/navdy/client/app/ui/glances/AppNotificationsActivity$LoadApplications;->this$0:Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;

    invoke-static {v5}, Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;->access$100(Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;)Landroid/content/pm/PackageManager;

    move-result-object v5

    invoke-static {v5}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getDialerPackage(Landroid/content/pm/PackageManager;)Ljava/lang/String;

    move-result-object v2

    .line 67
    .local v2, "dialerPackage":Ljava/lang/String;
    iget-object v5, p0, Lcom/navdy/client/app/ui/glances/AppNotificationsActivity$LoadApplications;->this$0:Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;

    invoke-static {v5}, Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;->access$200(Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;)Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "GLANCES: The dialer package is: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 68
    iget-object v5, p0, Lcom/navdy/client/app/ui/glances/AppNotificationsActivity$LoadApplications;->this$0:Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;

    invoke-static {v5}, Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;->access$300(Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;)Landroid/content/SharedPreferences;

    move-result-object v5

    invoke-interface {v5, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 69
    invoke-static {v2, v8}, Lcom/navdy/client/app/ui/glances/GlanceUtils;->saveGlancesConfigurationChanges(Ljava/lang/String;Z)V

    .line 72
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSmsPackage()Ljava/lang/String;

    move-result-object v4

    .line 73
    .local v4, "messengerPackage":Ljava/lang/String;
    iget-object v5, p0, Lcom/navdy/client/app/ui/glances/AppNotificationsActivity$LoadApplications;->this$0:Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;

    invoke-static {v5}, Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;->access$400(Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;)Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "GLANCES: The messenger package is: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 74
    iget-object v5, p0, Lcom/navdy/client/app/ui/glances/AppNotificationsActivity$LoadApplications;->this$0:Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;

    invoke-static {v5}, Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;->access$300(Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;)Landroid/content/SharedPreferences;

    move-result-object v5

    invoke-interface {v5, v4}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 75
    invoke-static {v4, v8}, Lcom/navdy/client/app/ui/glances/GlanceUtils;->saveGlancesConfigurationChanges(Ljava/lang/String;Z)V

    .line 78
    :cond_1
    iget-object v5, p0, Lcom/navdy/client/app/ui/glances/AppNotificationsActivity$LoadApplications;->this$0:Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;

    invoke-static {v5}, Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;->access$100(Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;)Landroid/content/pm/PackageManager;

    move-result-object v5

    const/16 v6, 0x80

    invoke-virtual {v5, v6}, Landroid/content/pm/PackageManager;->getInstalledApplications(I)Ljava/util/List;

    move-result-object v0

    .line 79
    .local v0, "appList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ApplicationInfo;>;"
    iget-object v5, p0, Lcom/navdy/client/app/ui/glances/AppNotificationsActivity$LoadApplications;->this$0:Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;

    invoke-static {v5, v0}, Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;->access$500(Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 81
    :try_start_0
    new-instance v5, Lcom/navdy/client/app/ui/glances/AppNotificationsActivity$LoadApplications$1;

    invoke-direct {v5, p0}, Lcom/navdy/client/app/ui/glances/AppNotificationsActivity$LoadApplications$1;-><init>(Lcom/navdy/client/app/ui/glances/AppNotificationsActivity$LoadApplications;)V

    invoke-static {v0, v5}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 122
    :goto_0
    iget-object v5, p0, Lcom/navdy/client/app/ui/glances/AppNotificationsActivity$LoadApplications;->this$0:Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;

    invoke-virtual {v5}, Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 123
    .local v1, "context":Landroid/content/Context;
    if-nez v1, :cond_2

    .line 124
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    .line 126
    :cond_2
    if-eqz v1, :cond_3

    .line 127
    iget-object v5, p0, Lcom/navdy/client/app/ui/glances/AppNotificationsActivity$LoadApplications;->this$0:Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;

    new-instance v6, Lcom/navdy/client/app/ui/glances/GlancesAppAdapter;

    const v7, 0x7f030027

    invoke-direct {v6, v1, v7, v0}, Lcom/navdy/client/app/ui/glances/GlancesAppAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    invoke-static {v5, v6}, Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;->access$702(Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;Lcom/navdy/client/app/ui/glances/GlancesAppAdapter;)Lcom/navdy/client/app/ui/glances/GlancesAppAdapter;

    .line 129
    :cond_3
    const/4 v5, 0x0

    return-object v5

    .line 118
    .end local v1    # "context":Landroid/content/Context;
    :catch_0
    move-exception v3

    .line 119
    .local v3, "e":Ljava/lang/Exception;
    iget-object v5, p0, Lcom/navdy/client/app/ui/glances/AppNotificationsActivity$LoadApplications;->this$0:Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;

    invoke-static {v5}, Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;->access$600(Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;)Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    const-string v6, "Unable to sort the list of apps."

    invoke-virtual {v5, v6, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 62
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/navdy/client/app/ui/glances/AppNotificationsActivity$LoadApplications;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 3
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    .line 134
    iget-object v1, p0, Lcom/navdy/client/app/ui/glances/AppNotificationsActivity$LoadApplications;->this$0:Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;

    const v2, 0x7f1000f7

    invoke-virtual {v1, v2}, Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 135
    .local v0, "listView":Landroid/widget/ListView;
    if-eqz v0, :cond_0

    .line 136
    iget-object v1, p0, Lcom/navdy/client/app/ui/glances/AppNotificationsActivity$LoadApplications;->this$0:Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;

    invoke-static {v1}, Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;->access$700(Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;)Lcom/navdy/client/app/ui/glances/GlancesAppAdapter;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 138
    :cond_0
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 139
    return-void
.end method

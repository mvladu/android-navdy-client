.class Lcom/navdy/client/app/ui/glances/AppNotificationsActivity$LoadApplications$1;
.super Ljava/lang/Object;
.source "AppNotificationsActivity.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/glances/AppNotificationsActivity$LoadApplications;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Landroid/content/pm/ApplicationInfo;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/client/app/ui/glances/AppNotificationsActivity$LoadApplications;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/glances/AppNotificationsActivity$LoadApplications;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/client/app/ui/glances/AppNotificationsActivity$LoadApplications;

    .prologue
    .line 81
    iput-object p1, p0, Lcom/navdy/client/app/ui/glances/AppNotificationsActivity$LoadApplications$1;->this$1:Lcom/navdy/client/app/ui/glances/AppNotificationsActivity$LoadApplications;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Landroid/content/pm/ApplicationInfo;Landroid/content/pm/ApplicationInfo;)I
    .locals 10
    .param p1, "lhs"    # Landroid/content/pm/ApplicationInfo;
    .param p2, "rhs"    # Landroid/content/pm/ApplicationInfo;

    .prologue
    const/4 v7, 0x1

    const/4 v5, 0x0

    const/4 v6, -0x1

    .line 84
    if-ne p1, p2, :cond_1

    .line 114
    :cond_0
    :goto_0
    return v5

    .line 87
    :cond_1
    if-nez p1, :cond_2

    move v5, v6

    .line 88
    goto :goto_0

    .line 90
    :cond_2
    if-nez p2, :cond_3

    move v5, v7

    .line 91
    goto :goto_0

    .line 94
    :cond_3
    :try_start_0
    const-string v8, "/system/priv-app/Keyguard.apk"

    iget-object v9, p1, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_4

    const-string v8, "/system/priv-app/Keyguard.apk"

    iget-object v9, p2, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    .line 95
    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 96
    :cond_4
    iget-object v5, p1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v6, p2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v5

    goto :goto_0

    .line 98
    :cond_5
    iget-object v8, p0, Lcom/navdy/client/app/ui/glances/AppNotificationsActivity$LoadApplications$1;->this$1:Lcom/navdy/client/app/ui/glances/AppNotificationsActivity$LoadApplications;

    iget-object v8, v8, Lcom/navdy/client/app/ui/glances/AppNotificationsActivity$LoadApplications;->this$0:Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;

    invoke-static {v8}, Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;->access$100(Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;)Landroid/content/pm/PackageManager;

    move-result-object v8

    invoke-virtual {p1, v8}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 99
    .local v1, "obj1":Ljava/lang/CharSequence;
    iget-object v8, p0, Lcom/navdy/client/app/ui/glances/AppNotificationsActivity$LoadApplications$1;->this$1:Lcom/navdy/client/app/ui/glances/AppNotificationsActivity$LoadApplications;

    iget-object v8, v8, Lcom/navdy/client/app/ui/glances/AppNotificationsActivity$LoadApplications;->this$0:Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;

    invoke-static {v8}, Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;->access$100(Lcom/navdy/client/app/ui/glances/AppNotificationsActivity;)Landroid/content/pm/PackageManager;

    move-result-object v8

    invoke-virtual {p2, v8}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 100
    .local v2, "obj2":Ljava/lang/CharSequence;
    if-eq v1, v2, :cond_0

    .line 103
    if-nez v1, :cond_6

    move v5, v6

    .line 104
    goto :goto_0

    .line 106
    :cond_6
    if-nez v2, :cond_7

    move v5, v7

    .line 107
    goto :goto_0

    .line 109
    :cond_7
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    .line 110
    .local v3, "s1":Ljava/lang/String;
    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    .line 111
    .local v4, "s2":Ljava/lang/String;
    invoke-virtual {v3, v4}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    goto :goto_0

    .line 112
    .end local v1    # "obj1":Ljava/lang/CharSequence;
    .end local v2    # "obj2":Ljava/lang/CharSequence;
    .end local v3    # "s1":Ljava/lang/String;
    .end local v4    # "s2":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 114
    .local v0, "e":Ljava/lang/Exception;
    iget-object v5, p1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v6, p2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v5

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 81
    check-cast p1, Landroid/content/pm/ApplicationInfo;

    check-cast p2, Landroid/content/pm/ApplicationInfo;

    invoke-virtual {p0, p1, p2}, Lcom/navdy/client/app/ui/glances/AppNotificationsActivity$LoadApplications$1;->compare(Landroid/content/pm/ApplicationInfo;Landroid/content/pm/ApplicationInfo;)I

    move-result v0

    return v0
.end method

.class Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity$1;
.super Ljava/lang/Object;
.source "FavoritesEditActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity$1;->this$0:Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 132
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 120
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 124
    iget-object v0, p0, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity$1;->this$0:Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->access$000(Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Destination;->getFavoriteLabel()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity$1;->this$0:Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->access$102(Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;Z)Z

    .line 127
    :cond_0
    return-void
.end method

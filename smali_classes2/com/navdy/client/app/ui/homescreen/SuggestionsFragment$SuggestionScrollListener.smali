.class Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;
.super Landroid/support/v7/widget/RecyclerView$OnScrollListener;
.source "SuggestionsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SuggestionScrollListener"
.end annotation


# instance fields
.field private drawerIsOpen:Z

.field private drawerWasOpen:Z

.field private mapSizeInPixels:I

.field final synthetic this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

.field private y:I


# direct methods
.method private constructor <init>(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 880
    iput-object p1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$OnScrollListener;-><init>()V

    .line 882
    iput v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;->mapSizeInPixels:I

    .line 883
    iput v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;->y:I

    .line 884
    iput-boolean v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;->drawerIsOpen:Z

    .line 885
    iput-boolean v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;->drawerWasOpen:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;
    .param p2, "x1"    # Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$1;

    .prologue
    .line 880
    invoke-direct {p0, p1}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;-><init>(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;)V

    return-void
.end method

.method private updateChevron(Z)V
    .locals 4
    .param p1, "drawerIsOpen"    # Z

    .prologue
    .line 955
    const/4 v1, 0x0

    .line 956
    .local v1, "chevron":Landroid/widget/ImageView;
    iget-object v2, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    invoke-static {v2}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->access$1100(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;)Landroid/support/v7/widget/RecyclerView;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 957
    iget-object v2, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    invoke-static {v2}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->access$1100(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;)Landroid/support/v7/widget/RecyclerView;

    move-result-object v2

    const v3, 0x7f1000b8

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .end local v1    # "chevron":Landroid/widget/ImageView;
    check-cast v1, Landroid/widget/ImageView;

    .line 959
    .restart local v1    # "chevron":Landroid/widget/ImageView;
    :cond_0
    if-eqz v1, :cond_1

    .line 961
    if-eqz p1, :cond_3

    .line 962
    const v2, 0x7f0201c8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 967
    :cond_1
    :goto_0
    iget-object v2, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    invoke-virtual {v2}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 968
    .local v0, "activity":Landroid/support/v4/app/FragmentActivity;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->isFinishing()Z

    move-result v2

    if-nez v2, :cond_2

    .line 969
    check-cast v0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    .end local v0    # "activity":Landroid/support/v4/app/FragmentActivity;
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->hideConnectionBanner(Landroid/view/View;)V

    .line 971
    :cond_2
    return-void

    .line 964
    :cond_3
    const v2, 0x7f0200f0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method


# virtual methods
.method public onScrollStateChanged(Landroid/support/v7/widget/RecyclerView;I)V
    .locals 4
    .param p1, "recyclerView"    # Landroid/support/v7/widget/RecyclerView;
    .param p2, "newState"    # I

    .prologue
    const/4 v3, 0x0

    .line 904
    if-nez p2, :cond_0

    .line 905
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    iget-object v0, v0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->suggestionAdapter:Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

    if-nez v0, :cond_1

    .line 906
    iget v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;->mapSizeInPixels:I

    if-nez v0, :cond_2

    .line 946
    :cond_0
    :goto_0
    return-void

    .line 911
    :cond_1
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    invoke-static {v0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->access$800(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;)Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    invoke-static {v0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->access$800(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;)Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 912
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    iget-object v0, v0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->suggestionAdapter:Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->getHereMapHeight()I

    move-result v0

    iput v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;->mapSizeInPixels:I

    .line 917
    :cond_2
    :goto_1
    invoke-static {}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Scrolled to: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;->y:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " drawer was opened: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;->drawerIsOpen:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mapSizeInPixels: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;->mapSizeInPixels:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 919
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    invoke-static {v0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->access$1000(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;)Landroid/support/v7/widget/LinearLayoutManager;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 920
    iget-boolean v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;->drawerIsOpen:Z

    if-nez v0, :cond_6

    iget v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;->y:I

    if-lez v0, :cond_6

    .line 921
    iget v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;->y:I

    iget v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;->mapSizeInPixels:I

    if-ge v0, v1, :cond_3

    .line 922
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    invoke-static {v0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->access$1000(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;)Landroid/support/v7/widget/LinearLayoutManager;

    move-result-object v0

    iget v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;->mapSizeInPixels:I

    neg-int v1, v1

    invoke-virtual {v0, v3, v1}, Landroid/support/v7/widget/LinearLayoutManager;->scrollToPositionWithOffset(II)V

    .line 923
    iget v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;->mapSizeInPixels:I

    iput v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;->y:I

    .line 925
    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;->drawerIsOpen:Z

    .line 940
    :cond_4
    :goto_2
    iget-boolean v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;->drawerIsOpen:Z

    iput-boolean v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;->drawerWasOpen:Z

    .line 941
    iget-boolean v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;->drawerIsOpen:Z

    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;->updateChevron(Z)V

    goto/16 :goto_0

    .line 914
    :cond_5
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    iget-object v0, v0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->suggestionAdapter:Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->getGoogleMapHeight()I

    move-result v0

    iput v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;->mapSizeInPixels:I

    goto :goto_1

    .line 926
    :cond_6
    iget-boolean v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;->drawerIsOpen:Z

    if-eqz v0, :cond_4

    iget v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;->y:I

    iget v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;->mapSizeInPixels:I

    if-ge v0, v1, :cond_4

    .line 927
    iget v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;->y:I

    if-eqz v0, :cond_8

    .line 928
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    invoke-static {v0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->access$1100(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;)Landroid/support/v7/widget/RecyclerView;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 929
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    invoke-static {v0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->access$1100(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;)Landroid/support/v7/widget/RecyclerView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->smoothScrollToPosition(I)V

    .line 936
    :cond_7
    iput v3, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;->y:I

    .line 938
    :cond_8
    iput-boolean v3, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;->drawerIsOpen:Z

    goto :goto_2

    .line 943
    :cond_9
    invoke-static {}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "Unable to get layoutManager !"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public onScrolled(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 3
    .param p1, "recyclerView"    # Landroid/support/v7/widget/RecyclerView;
    .param p2, "dx"    # I
    .param p3, "dy"    # I

    .prologue
    const/4 v0, 0x0

    .line 889
    iget v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;->y:I

    add-int/2addr v1, p3

    iput v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;->y:I

    .line 890
    iget v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;->y:I

    if-gez v1, :cond_0

    .line 891
    iput v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;->y:I

    .line 893
    :cond_0
    iget-boolean v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;->drawerIsOpen:Z

    iget-boolean v2, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;->drawerWasOpen:Z

    if-ne v1, v2, :cond_4

    iget-boolean v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;->drawerWasOpen:Z

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;->y:I

    iget v2, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;->mapSizeInPixels:I

    if-lt v1, v2, :cond_2

    :cond_1
    iget-boolean v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;->drawerWasOpen:Z

    if-nez v1, :cond_4

    iget v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;->y:I

    if-lez v1, :cond_4

    .line 897
    :cond_2
    iget-boolean v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;->drawerWasOpen:Z

    if-nez v1, :cond_3

    const/4 v0, 0x1

    :cond_3
    iput-boolean v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;->drawerWasOpen:Z

    .line 898
    iget-boolean v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;->drawerWasOpen:Z

    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;->updateChevron(Z)V

    .line 900
    :cond_4
    return-void
.end method

.method public reset()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 949
    iput v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;->y:I

    .line 950
    iput-boolean v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;->drawerIsOpen:Z

    .line 951
    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$SuggestionScrollListener;->updateChevron(Z)V

    .line 952
    return-void
.end method

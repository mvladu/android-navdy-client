.class Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter$3;
.super Ljava/lang/Object;
.source "SuggestedDestinationsAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->onBindActiveTrip(Lcom/navdy/client/app/ui/homescreen/ActiveTripViewHolder;Lcom/navdy/client/app/framework/models/Suggestion;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

.field final synthetic val$activeTripViewHolder:Lcom/navdy/client/app/ui/homescreen/ActiveTripViewHolder;

.field final synthetic val$suggestion:Lcom/navdy/client/app/framework/models/Suggestion;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;Lcom/navdy/client/app/ui/homescreen/ActiveTripViewHolder;Lcom/navdy/client/app/framework/models/Suggestion;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

    .prologue
    .line 593
    iput-object p1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter$3;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

    iput-object p2, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter$3;->val$activeTripViewHolder:Lcom/navdy/client/app/ui/homescreen/ActiveTripViewHolder;

    iput-object p3, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter$3;->val$suggestion:Lcom/navdy/client/app/framework/models/Suggestion;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 596
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter$3;->val$activeTripViewHolder:Lcom/navdy/client/app/ui/homescreen/ActiveTripViewHolder;

    invoke-virtual {v1}, Lcom/navdy/client/app/ui/homescreen/ActiveTripViewHolder;->getAdapterPosition()I

    move-result v0

    .line 597
    .local v0, "adapterPosition":I
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter$3;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

    invoke-static {v1}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->access$000(Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;)I

    move-result v1

    if-ltz v1, :cond_1

    .line 598
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter$3;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter$3;->val$activeTripViewHolder:Lcom/navdy/client/app/ui/homescreen/ActiveTripViewHolder;

    iget-object v3, v3, Lcom/navdy/client/app/ui/homescreen/ActiveTripViewHolder;->itemView:Landroid/view/View;

    iget-object v4, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter$3;->val$suggestion:Lcom/navdy/client/app/framework/models/Suggestion;

    invoke-static {v1, v2, v3, v0, v4}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->access$200(Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;ZLandroid/view/View;ILcom/navdy/client/app/framework/models/Suggestion;)Z

    .line 603
    :cond_0
    :goto_0
    return-void

    .line 600
    :cond_1
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter$3;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

    invoke-static {v1}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->access$100(Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;)Lcom/navdy/client/app/framework/util/CustomItemClickListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 601
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter$3;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

    invoke-static {v1}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->access$100(Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;)Lcom/navdy/client/app/framework/util/CustomItemClickListener;

    move-result-object v1

    invoke-interface {v1, p1, v0}, Lcom/navdy/client/app/framework/util/CustomItemClickListener;->onClick(Landroid/view/View;I)V

    goto :goto_0
.end method

.class public Lcom/navdy/client/app/ui/homescreen/InfoDialogActivity;
.super Lcom/navdy/client/app/ui/base/BaseActivity;
.source "InfoDialogActivity.java"


# static fields
.field public static final EXTRA_LAYOUT:Ljava/lang/String; = "layout"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public onButtonClick(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/homescreen/InfoDialogActivity;->finish()V

    .line 33
    return-void
.end method

.method public onCloseClick(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/homescreen/InfoDialogActivity;->onBackPressed()V

    .line 37
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 20
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 22
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/homescreen/InfoDialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 23
    .local v0, "intent":Landroid/content/Intent;
    if-nez v0, :cond_0

    .line 24
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/homescreen/InfoDialogActivity;->finish()V

    .line 29
    :goto_0
    return-void

    .line 27
    :cond_0
    const-string v2, "layout"

    const v3, 0x7f030053

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 28
    .local v1, "layout":I
    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/homescreen/InfoDialogActivity;->setContentView(I)V

    goto :goto_0
.end method

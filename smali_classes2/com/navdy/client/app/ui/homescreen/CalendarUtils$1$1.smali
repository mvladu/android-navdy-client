.class Lcom/navdy/client/app/ui/homescreen/CalendarUtils$1$1;
.super Ljava/lang/Object;
.source "CalendarUtils.java"

# interfaces
.implements Lcom/navdy/client/app/ui/homescreen/CalendarUtils$CalendarEventReader;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/homescreen/CalendarUtils$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/homescreen/CalendarUtils$1;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/homescreen/CalendarUtils$1;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/homescreen/CalendarUtils$1;

    .prologue
    .line 78
    iput-object p1, p0, Lcom/navdy/client/app/ui/homescreen/CalendarUtils$1$1;->this$0:Lcom/navdy/client/app/ui/homescreen/CalendarUtils$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCalendarEventsRead(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/CalendarEvent;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 81
    .local p1, "calendarEvents":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/CalendarEvent;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 82
    .local v1, "calendarEventProtoObjects":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/calendars/CalendarEvent;>;"
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    .line 83
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_1

    const/16 v4, 0x14

    if-ge v3, v4, :cond_1

    .line 84
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/models/CalendarEvent;

    .line 85
    .local v0, "calendarEvent":Lcom/navdy/client/app/framework/models/CalendarEvent;
    new-instance v2, Lcom/navdy/client/app/framework/models/Destination;

    iget-object v4, v0, Lcom/navdy/client/app/framework/models/CalendarEvent;->displayName:Ljava/lang/String;

    iget-object v5, v0, Lcom/navdy/client/app/framework/models/CalendarEvent;->location:Ljava/lang/String;

    invoke-direct {v2, v4, v5}, Lcom/navdy/client/app/framework/models/Destination;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    .local v2, "destination":Lcom/navdy/client/app/framework/models/Destination;
    invoke-static {v2}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getThisDestination(Lcom/navdy/client/app/framework/models/Destination;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v2

    .line 87
    if-eqz v2, :cond_0

    .line 88
    invoke-virtual {v2}, Lcom/navdy/client/app/framework/models/Destination;->toProtobufDestinationObject()Lcom/navdy/service/library/events/destination/Destination;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/navdy/client/app/framework/models/CalendarEvent;->setDestination(Lcom/navdy/service/library/events/destination/Destination;)V

    .line 90
    :cond_0
    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/CalendarEvent;->toProtobufObject()Lcom/navdy/service/library/events/calendars/CalendarEvent;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 83
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 92
    .end local v0    # "calendarEvent":Lcom/navdy/client/app/framework/models/CalendarEvent;
    .end local v2    # "destination":Lcom/navdy/client/app/framework/models/Destination;
    :cond_1
    iget-object v4, p0, Lcom/navdy/client/app/ui/homescreen/CalendarUtils$1$1;->this$0:Lcom/navdy/client/app/ui/homescreen/CalendarUtils$1;

    iget-object v4, v4, Lcom/navdy/client/app/ui/homescreen/CalendarUtils$1;->val$remoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    new-instance v5, Lcom/navdy/service/library/events/calendars/CalendarEventUpdates;

    invoke-direct {v5, v1}, Lcom/navdy/service/library/events/calendars/CalendarEventUpdates;-><init>(Ljava/util/List;)V

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/device/RemoteDevice;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 94
    .end local v3    # "i":I
    :cond_2
    return-void
.end method

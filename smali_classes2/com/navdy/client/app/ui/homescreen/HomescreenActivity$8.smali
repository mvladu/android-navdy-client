.class Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$8;
.super Ljava/lang/Object;
.source "HomescreenActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->onFavoriteListChanged()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    .prologue
    .line 754
    iput-object p1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$8;->this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 757
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$8;->this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    invoke-static {v1}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->access$800(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$8;->this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    iget-object v1, v1, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->pagerAdapter:Lcom/navdy/client/app/ui/homescreen/HomescreenPagerAdapter;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$8;->this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    iget-object v1, v1, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->pagerAdapter:Lcom/navdy/client/app/ui/homescreen/HomescreenPagerAdapter;

    invoke-virtual {v1}, Lcom/navdy/client/app/ui/homescreen/HomescreenPagerAdapter;->getCount()I

    move-result v1

    const/4 v2, 0x2

    if-lt v1, v2, :cond_2

    .line 758
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$8;->this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    iget-object v1, v1, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->pagerAdapter:Lcom/navdy/client/app/ui/homescreen/HomescreenPagerAdapter;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/navdy/client/app/ui/homescreen/HomescreenPagerAdapter;->getItem(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/ui/favorites/FavoritesFragment;

    .line 759
    .local v0, "favoritesFragment":Lcom/navdy/client/app/ui/favorites/FavoritesFragment;
    if-eqz v0, :cond_1

    .line 760
    iget-object v1, v0, Lcom/navdy/client/app/ui/favorites/FavoritesFragment;->favoritesAdapter:Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;

    if-eqz v1, :cond_0

    .line 761
    iget-object v1, v0, Lcom/navdy/client/app/ui/favorites/FavoritesFragment;->favoritesAdapter:Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;

    invoke-virtual {v1}, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;->notifyDbChanged()V

    .line 763
    :cond_0
    invoke-virtual {v0}, Lcom/navdy/client/app/ui/favorites/FavoritesFragment;->showOrHideSplashScreen()V

    .line 765
    :cond_1
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$8;->this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    invoke-virtual {v1}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->rebuildSuggestions()V

    .line 767
    .end local v0    # "favoritesFragment":Lcom/navdy/client/app/ui/favorites/FavoritesFragment;
    :cond_2
    return-void
.end method

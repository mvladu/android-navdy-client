.class Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$9;
.super Ljava/lang/Object;
.source "HomescreenActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->setUpLogoEasterEgg()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field count:I

.field final synthetic this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;)V
    .locals 1
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    .prologue
    .line 790
    iput-object p1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$9;->this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 791
    const/4 v0, 0x0

    iput v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$9;->count:I

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x0

    .line 795
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$9;->this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->access$900(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;)Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Clicked on the logo "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$9;->count:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " time(s)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 796
    iget v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$9;->count:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$9;->count:I

    const/16 v1, 0xa

    if-ne v0, v1, :cond_2

    .line 797
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$9;->this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    iget-object v0, v0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->pagerAdapter:Lcom/navdy/client/app/ui/homescreen/HomescreenPagerAdapter;

    if-eqz v0, :cond_0

    .line 798
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$9;->this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    iget-object v0, v0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->pagerAdapter:Lcom/navdy/client/app/ui/homescreen/HomescreenPagerAdapter;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/homescreen/HomescreenPagerAdapter;->showHiddenTabs()V

    .line 799
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$9;->this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->access$1000(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;)Landroid/support/design/widget/TabLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$9;->this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    invoke-static {v1}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->access$800(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TabLayout;->setupWithViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 802
    :cond_0
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$9;->this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$9;->this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    const v2, 0x7f1002b3

    invoke-virtual {v0, v2}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/NavigationView;

    invoke-static {v1, v0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->access$1102(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;Landroid/support/design/widget/NavigationView;)Landroid/support/design/widget/NavigationView;

    .line 803
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$9;->this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->access$1100(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;)Landroid/support/design/widget/NavigationView;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 804
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$9;->this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    const/4 v1, 0x1

    const v2, 0x7f100413

    invoke-static {v0, v1, v2}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->access$1200(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;ZI)V

    .line 808
    :cond_1
    invoke-static {}, Lcom/navdy/client/app/framework/util/SuggestionManager;->forceSuggestionFullRefresh()V

    .line 809
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$9;->this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->rebuildSuggestions()V

    .line 812
    invoke-static {}, Lcom/navdy/client/app/providers/NavdyContentProvider;->clearCache()V

    .line 815
    :cond_2
    iget v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$9;->count:I

    const/16 v1, 0xf

    if-lt v0, v1, :cond_3

    .line 816
    const v0, 0x7f0801f5

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/base/BaseActivity;->showShortToast(I[Ljava/lang/Object;)V

    .line 819
    invoke-static {}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->hideAllTips()V

    .line 822
    invoke-static {}, Lcom/navdy/client/app/framework/util/SuggestionManager;->forceSuggestionFullRefresh()V

    .line 823
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$9;->this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->rebuildSuggestions()V

    .line 826
    :cond_3
    iget v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$9;->count:I

    const/16 v1, 0x14

    if-ne v0, v1, :cond_4

    .line 827
    const v0, 0x7f08047a

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/base/BaseActivity;->showShortToast(I[Ljava/lang/Object;)V

    .line 829
    iput v3, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$9;->count:I

    .line 831
    invoke-static {}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->access$1300()V

    .line 834
    invoke-static {}, Lcom/navdy/client/app/framework/util/SuggestionManager;->forceSuggestionFullRefresh()V

    .line 835
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$9;->this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->rebuildSuggestions()V

    .line 837
    :cond_4
    return-void
.end method

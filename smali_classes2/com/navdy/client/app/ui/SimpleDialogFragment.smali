.class public Lcom/navdy/client/app/ui/SimpleDialogFragment;
.super Landroid/app/DialogFragment;
.source "SimpleDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/ui/SimpleDialogFragment$DialogProvider;
    }
.end annotation


# static fields
.field public static final EXTRA_ID:Ljava/lang/String; = "_id"

.field public static final EXTRA_MESSAGE:Ljava/lang/String; = "message"

.field public static final EXTRA_POSITIVE_BUTTON_TITLE:Ljava/lang/String; = "positive_button_title"

.field public static final EXTRA_TITLE:Ljava/lang/String; = "title"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static newInstance(ILandroid/os/Bundle;)Lcom/navdy/client/app/ui/SimpleDialogFragment;
    .locals 2
    .param p0, "id"    # I
    .param p1, "args"    # Landroid/os/Bundle;

    .prologue
    .line 23
    if-nez p1, :cond_0

    .line 24
    new-instance p1, Landroid/os/Bundle;

    .end local p1    # "args":Landroid/os/Bundle;
    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    .line 26
    .restart local p1    # "args":Landroid/os/Bundle;
    :cond_0
    const-string v1, "_id"

    invoke-virtual {p1, v1, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 27
    new-instance v0, Lcom/navdy/client/app/ui/SimpleDialogFragment;

    invoke-direct {v0}, Lcom/navdy/client/app/ui/SimpleDialogFragment;-><init>()V

    .line 28
    .local v0, "simpleDialogFragment":Lcom/navdy/client/app/ui/SimpleDialogFragment;
    invoke-virtual {v0, p1}, Lcom/navdy/client/app/ui/SimpleDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 29
    return-object v0
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/SimpleDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 35
    .local v0, "activity":Landroid/app/Activity;
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/SimpleDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 36
    .local v1, "arguments":Landroid/os/Bundle;
    if-eqz v1, :cond_0

    .line 37
    const-string v3, "_id"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 38
    .local v2, "id":I
    if-eqz v0, :cond_0

    instance-of v3, v0, Lcom/navdy/client/app/ui/SimpleDialogFragment$DialogProvider;

    if-eqz v3, :cond_0

    .line 39
    check-cast v0, Lcom/navdy/client/app/ui/SimpleDialogFragment$DialogProvider;

    .end local v0    # "activity":Landroid/app/Activity;
    invoke-interface {v0, v2, v1}, Lcom/navdy/client/app/ui/SimpleDialogFragment$DialogProvider;->createDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v3

    .line 42
    .end local v2    # "id":I
    :goto_0
    return-object v3

    .restart local v0    # "activity":Landroid/app/Activity;
    :cond_0
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v3

    goto :goto_0
.end method

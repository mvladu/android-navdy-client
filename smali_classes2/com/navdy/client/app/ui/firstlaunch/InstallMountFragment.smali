.class public Lcom/navdy/client/app/ui/firstlaunch/InstallMountFragment;
.super Lcom/navdy/client/app/ui/firstlaunch/InstallCardFragment;
.source "InstallMountFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallCardFragment;-><init>()V

    .line 29
    const v0, 0x7f030073

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/InstallMountFragment;->setLayoutId(I)V

    .line 30
    return-void
.end method


# virtual methods
.method public getScreen()Ljava/lang/String;
    .locals 2

    .prologue
    .line 92
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallMountFragment;->mountType:Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    sget-object v1, Lcom/navdy/client/app/framework/models/MountInfo$MountType;->SHORT:Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    if-ne v0, v1, :cond_0

    .line 93
    const-string v0, "Installation_Short_Mount"

    .line 95
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "Installation_Medium_Tall_Mount"

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 69
    const v6, 0x7f030073

    invoke-virtual {p1, v6, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    iput-object v6, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallMountFragment;->rootView:Landroid/view/View;

    .line 70
    const/4 v3, 0x0

    .line 71
    .local v3, "imageCache":Lcom/navdy/client/app/framework/util/ImageCache;
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallMountFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;

    .line 72
    .local v0, "activity":Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;
    if-eqz v0, :cond_0

    .line 73
    iget-object v3, v0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->imageCache:Lcom/navdy/client/app/framework/util/ImageCache;

    .line 75
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v6

    const-string v7, "box"

    const-string v8, "Old_Box"

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 76
    .local v2, "box":Ljava/lang/String;
    const-string v6, "Old_Box"

    invoke-static {v2, v6}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    const/4 v5, 0x1

    .line 78
    .local v5, "showNewShortMountAssets":Z
    :cond_1
    iget-object v6, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallMountFragment;->rootView:Landroid/view/View;

    const v7, 0x7f1001e1

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 79
    .local v4, "imageView":Landroid/widget/ImageView;
    const v6, 0x7f020057

    invoke-static {v4, v6, v3}, Lcom/navdy/client/app/framework/util/ImageUtils;->loadImage(Landroid/widget/ImageView;ILcom/navdy/client/app/framework/util/ImageCache;)V

    .line 80
    iget-object v6, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallMountFragment;->rootView:Landroid/view/View;

    const v7, 0x7f1000b1

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .end local v4    # "imageView":Landroid/widget/ImageView;
    check-cast v4, Landroid/widget/ImageView;

    .line 81
    .restart local v4    # "imageView":Landroid/widget/ImageView;
    const v1, 0x7f020053

    .line 82
    .local v1, "asset":I
    if-eqz v5, :cond_2

    .line 83
    const v1, 0x7f020055

    .line 85
    :cond_2
    invoke-static {v4, v1, v3}, Lcom/navdy/client/app/framework/util/ImageUtils;->loadImage(Landroid/widget/ImageView;ILcom/navdy/client/app/framework/util/ImageCache;)V

    .line 86
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallMountFragment;->updateMountShown()V

    .line 87
    iget-object v6, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallMountFragment;->rootView:Landroid/view/View;

    return-object v6
.end method

.method public setMountType(Lcom/navdy/client/app/framework/models/MountInfo$MountType;)V
    .locals 0
    .param p1, "type"    # Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallMountFragment;->mountType:Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    .line 34
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallMountFragment;->updateMountShown()V

    .line 35
    return-void
.end method

.method public updateMountShown()V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 38
    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallMountFragment;->rootView:Landroid/view/View;

    if-eqz v2, :cond_1

    .line 39
    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallMountFragment;->rootView:Landroid/view/View;

    const v3, 0x7f1001e6

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 40
    .local v0, "shortMount":Landroid/view/View;
    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallMountFragment;->rootView:Landroid/view/View;

    const v3, 0x7f1001e5

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 42
    .local v1, "tallOrMediumMount":Landroid/view/View;
    if-eqz v0, :cond_0

    if-nez v1, :cond_2

    .line 43
    :cond_0
    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallMountFragment;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Missing layout elements !"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 62
    .end local v0    # "shortMount":Landroid/view/View;
    .end local v1    # "tallOrMediumMount":Landroid/view/View;
    :cond_1
    :goto_0
    return-void

    .line 47
    .restart local v0    # "shortMount":Landroid/view/View;
    .restart local v1    # "tallOrMediumMount":Landroid/view/View;
    :cond_2
    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallMountFragment;->mountType:Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    if-eqz v2, :cond_1

    .line 48
    sget-object v2, Lcom/navdy/client/app/ui/firstlaunch/InstallMountFragment$1;->$SwitchMap$com$navdy$client$app$framework$models$MountInfo$MountType:[I

    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallMountFragment;->mountType:Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    invoke-virtual {v3}, Lcom/navdy/client/app/framework/models/MountInfo$MountType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 51
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 52
    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 56
    :pswitch_0
    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 57
    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 48
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.class Lcom/navdy/client/app/ui/firstlaunch/CheckMountActivity$1;
.super Landroid/os/AsyncTask;
.source "CheckMountActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/firstlaunch/CheckMountActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/navdy/client/app/framework/models/MountInfo;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/firstlaunch/CheckMountActivity;

.field final synthetic val$box:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/firstlaunch/CheckMountActivity;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/firstlaunch/CheckMountActivity;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/navdy/client/app/ui/firstlaunch/CheckMountActivity$1;->this$0:Lcom/navdy/client/app/ui/firstlaunch/CheckMountActivity;

    iput-object p2, p0, Lcom/navdy/client/app/ui/firstlaunch/CheckMountActivity$1;->val$box:Ljava/lang/String;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Lcom/navdy/client/app/framework/models/MountInfo;
    .locals 1
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 40
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getMountInfo()Lcom/navdy/client/app/framework/models/MountInfo;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 36
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/navdy/client/app/ui/firstlaunch/CheckMountActivity$1;->doInBackground([Ljava/lang/Void;)Lcom/navdy/client/app/framework/models/MountInfo;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/navdy/client/app/framework/models/MountInfo;)V
    .locals 4
    .param p1, "mountInfo"    # Lcom/navdy/client/app/framework/models/MountInfo;

    .prologue
    .line 45
    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/CheckMountActivity$1;->this$0:Lcom/navdy/client/app/ui/firstlaunch/CheckMountActivity;

    invoke-virtual {v2}, Lcom/navdy/client/app/ui/firstlaunch/CheckMountActivity;->hideProgressDialog()V

    .line 46
    invoke-virtual {p1}, Lcom/navdy/client/app/framework/models/MountInfo;->noneOfTheMountsWillWork()Z

    move-result v1

    .line 47
    .local v1, "noneOfTheMountsWillWork":Z
    iget-boolean v2, p1, Lcom/navdy/client/app/framework/models/MountInfo;->shortSupported:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/CheckMountActivity$1;->val$box:Ljava/lang/String;

    const-string v3, "New_Box"

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    if-eqz v1, :cond_3

    .line 48
    :cond_1
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/CheckMountActivity$1;->this$0:Lcom/navdy/client/app/ui/firstlaunch/CheckMountActivity;

    invoke-virtual {v2}, Lcom/navdy/client/app/ui/firstlaunch/CheckMountActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 49
    .local v0, "intent":Landroid/content/Intent;
    if-eqz v1, :cond_2

    .line 50
    const-string v2, "extra_use_case"

    const/4 v3, 0x3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 52
    :cond_2
    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/CheckMountActivity$1;->this$0:Lcom/navdy/client/app/ui/firstlaunch/CheckMountActivity;

    invoke-virtual {v2, v0}, Lcom/navdy/client/app/ui/firstlaunch/CheckMountActivity;->startActivity(Landroid/content/Intent;)V

    .line 66
    :goto_0
    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/CheckMountActivity$1;->this$0:Lcom/navdy/client/app/ui/firstlaunch/CheckMountActivity;

    invoke-virtual {v2}, Lcom/navdy/client/app/ui/firstlaunch/CheckMountActivity;->finish()V

    .line 67
    return-void

    .line 54
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_3
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/CheckMountActivity$1;->this$0:Lcom/navdy/client/app/ui/firstlaunch/CheckMountActivity;

    invoke-virtual {v2}, Lcom/navdy/client/app/ui/firstlaunch/CheckMountActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 55
    .restart local v0    # "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/CheckMountActivity$1;->val$box:Ljava/lang/String;

    const-string v3, "New_Box"

    invoke-static {v2, v3}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 58
    const-string v2, "extra_mount"

    iget-object v3, p1, Lcom/navdy/client/app/framework/models/MountInfo;->recommendedMount:Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    .line 59
    invoke-virtual {v3}, Lcom/navdy/client/app/framework/models/MountInfo$MountType;->getValue()Ljava/lang/String;

    move-result-object v3

    .line 58
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 64
    :goto_1
    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/CheckMountActivity$1;->this$0:Lcom/navdy/client/app/ui/firstlaunch/CheckMountActivity;

    invoke-virtual {v2, v0}, Lcom/navdy/client/app/ui/firstlaunch/CheckMountActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 61
    :cond_4
    const-string v2, "extra_mount"

    sget-object v3, Lcom/navdy/client/app/framework/models/MountInfo$MountType;->SHORT:Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    .line 62
    invoke-virtual {v3}, Lcom/navdy/client/app/framework/models/MountInfo$MountType;->getValue()Ljava/lang/String;

    move-result-object v3

    .line 61
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 36
    check-cast p1, Lcom/navdy/client/app/framework/models/MountInfo;

    invoke-virtual {p0, p1}, Lcom/navdy/client/app/ui/firstlaunch/CheckMountActivity$1;->onPostExecute(Lcom/navdy/client/app/framework/models/MountInfo;)V

    return-void
.end method

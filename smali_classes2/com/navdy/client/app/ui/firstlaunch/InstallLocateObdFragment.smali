.class public Lcom/navdy/client/app/ui/firstlaunch/InstallLocateObdFragment;
.super Lcom/navdy/client/app/ui/firstlaunch/InstallCardFragment;
.source "InstallLocateObdFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallCardFragment;-><init>()V

    .line 21
    const v0, 0x7f030071

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/InstallLocateObdFragment;->setLayoutId(I)V

    .line 22
    return-void
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 29
    const/4 v1, 0x0

    .line 30
    .local v1, "imageCache":Lcom/navdy/client/app/framework/util/ImageCache;
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallLocateObdFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;

    .line 31
    .local v0, "activity":Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;
    if-eqz v0, :cond_0

    .line 32
    iget-object v1, v0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->imageCache:Lcom/navdy/client/app/framework/util/ImageCache;

    .line 34
    :cond_0
    const v3, 0x7f030071

    const/4 v4, 0x0

    invoke-virtual {p1, v3, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallLocateObdFragment;->rootView:Landroid/view/View;

    .line 35
    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallLocateObdFragment;->rootView:Landroid/view/View;

    const v4, 0x7f1000b1

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 36
    .local v2, "imageView":Landroid/widget/ImageView;
    const v3, 0x7f0201e4

    invoke-static {v2, v3, v1}, Lcom/navdy/client/app/framework/util/ImageUtils;->loadImage(Landroid/widget/ImageView;ILcom/navdy/client/app/framework/util/ImageCache;)V

    .line 37
    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallLocateObdFragment;->rootView:Landroid/view/View;

    return-object v3
.end method

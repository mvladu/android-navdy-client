.class public Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowTextPagerAdaper;
.super Landroid/support/v4/view/PagerAdapter;
.source "MarketingFlowTextPagerAdaper.java"


# instance fields
.field private descs:[I

.field private mLayoutInflater:Landroid/view/LayoutInflater;

.field private onClickListener:Landroid/view/View$OnClickListener;

.field private titles:[I


# direct methods
.method public constructor <init>(Landroid/content/Context;[I[ILandroid/view/View$OnClickListener;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "titles"    # [I
    .param p3, "descs"    # [I
    .param p4, "onClickListener"    # Landroid/view/View$OnClickListener;

    .prologue
    const/4 v1, 0x0

    .line 24
    invoke-direct {p0}, Landroid/support/v4/view/PagerAdapter;-><init>()V

    .line 20
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowTextPagerAdaper;->titles:[I

    .line 21
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowTextPagerAdaper;->descs:[I

    .line 25
    invoke-virtual {p2}, [I->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    iput-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowTextPagerAdaper;->titles:[I

    .line 26
    invoke-virtual {p3}, [I->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    iput-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowTextPagerAdaper;->descs:[I

    .line 27
    iput-object p4, p0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowTextPagerAdaper;->onClickListener:Landroid/view/View$OnClickListener;

    .line 28
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowTextPagerAdaper;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 29
    return-void
.end method


# virtual methods
.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I
    .param p3, "object"    # Ljava/lang/Object;

    .prologue
    .line 67
    check-cast p3, Landroid/widget/RelativeLayout;

    .end local p3    # "object":Ljava/lang/Object;
    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 68
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowTextPagerAdaper;->titles:[I

    array-length v0, v0

    return v0
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 6
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I

    .prologue
    .line 43
    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowTextPagerAdaper;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v4, 0x7f03007d

    const/4 v5, 0x0

    invoke-virtual {v3, v4, p1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 44
    .local v1, "itemView":Landroid/view/View;
    if-nez v1, :cond_0

    .line 45
    const/4 v1, 0x0

    .line 62
    .end local v1    # "itemView":Landroid/view/View;
    :goto_0
    return-object v1

    .line 48
    .restart local v1    # "itemView":Landroid/view/View;
    :cond_0
    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowTextPagerAdaper;->onClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 50
    const v3, 0x7f10007f

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 51
    .local v2, "title":Landroid/widget/TextView;
    if-eqz v2, :cond_1

    .line 52
    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowTextPagerAdaper;->titles:[I

    aget v3, v3, p2

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 55
    :cond_1
    const v3, 0x7f10012f

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 56
    .local v0, "desc":Landroid/widget/TextView;
    if-eqz v0, :cond_2

    .line 57
    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowTextPagerAdaper;->descs:[I

    aget v3, v3, p2

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 60
    :cond_2
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "object"    # Ljava/lang/Object;

    .prologue
    .line 38
    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

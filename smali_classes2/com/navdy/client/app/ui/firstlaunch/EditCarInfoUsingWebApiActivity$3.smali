.class Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$3;
.super Ljava/lang/Object;
.source "EditCarInfoUsingWebApiActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->initCarSelectorScreen()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;

.field final synthetic val$carMdClient:Lcom/navdy/client/app/framework/util/CarMdClient;

.field final synthetic val$make:Landroid/widget/Spinner;

.field final synthetic val$model:Landroid/widget/Spinner;

.field final synthetic val$modelAdapter:Lorg/droidparts/adapter/widget/StringSpinnerAdapter;

.field final synthetic val$modelString:Ljava/lang/String;

.field final synthetic val$year:Landroid/widget/Spinner;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;Landroid/widget/Spinner;Landroid/widget/Spinner;Landroid/widget/Spinner;Lorg/droidparts/adapter/widget/StringSpinnerAdapter;Lcom/navdy/client/app/framework/util/CarMdClient;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;

    .prologue
    .line 247
    iput-object p1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$3;->this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;

    iput-object p2, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$3;->val$make:Landroid/widget/Spinner;

    iput-object p3, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$3;->val$year:Landroid/widget/Spinner;

    iput-object p4, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$3;->val$model:Landroid/widget/Spinner;

    iput-object p5, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$3;->val$modelAdapter:Lorg/droidparts/adapter/widget/StringSpinnerAdapter;

    iput-object p6, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$3;->val$carMdClient:Lcom/navdy/client/app/framework/util/CarMdClient;

    iput-object p7, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$3;->val$modelString:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .param p2, "view"    # Landroid/view/View;
    .param p3, "selectedYearPosition"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 253
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$3;->this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;

    invoke-static {v3}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->access$900(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;)Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onItemSelected selectedYearPosition: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 254
    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$3;->val$make:Landroid/widget/Spinner;

    invoke-virtual {v3}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 255
    .local v0, "selectedMake":Ljava/lang/String;
    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$3;->val$make:Landroid/widget/Spinner;

    invoke-virtual {v3}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v1

    .line 256
    .local v1, "selectedMakePosition":I
    if-lez v1, :cond_0

    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$3;->val$make:Landroid/widget/Spinner;

    invoke-virtual {v3}, Landroid/widget/Spinner;->getCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-lt v1, v3, :cond_1

    .line 318
    :cond_0
    :goto_0
    return-void

    .line 260
    :cond_1
    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$3;->val$year:Landroid/widget/Spinner;

    invoke-virtual {v3}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 261
    .local v2, "selectedYear":Ljava/lang/String;
    if-gtz p3, :cond_2

    .line 262
    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$3;->val$model:Landroid/widget/Spinner;

    iget-object v4, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$3;->val$modelAdapter:Lorg/droidparts/adapter/widget/StringSpinnerAdapter;

    invoke-virtual {v3, v4}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    goto :goto_0

    .line 265
    :cond_2
    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$3;->val$year:Landroid/widget/Spinner;

    invoke-virtual {v3}, Landroid/widget/Spinner;->getCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-lt p3, v3, :cond_3

    .line 266
    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$3;->this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;

    invoke-static {v3}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->access$500(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;)V

    .line 269
    :cond_3
    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$3;->this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;

    invoke-virtual {v3}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->showProgressDialog()V

    .line 270
    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$3;->val$carMdClient:Lcom/navdy/client/app/framework/util/CarMdClient;

    new-instance v4, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$3$1;

    invoke-direct {v4, p0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$3$1;-><init>(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$3;)V

    invoke-virtual {v3, v0, v2, v4}, Lcom/navdy/client/app/framework/util/CarMdClient;->getModels(Ljava/lang/String;Ljava/lang/String;Lcom/navdy/client/app/framework/util/CarMdCallBack;)V

    goto :goto_0
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 321
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    return-void
.end method

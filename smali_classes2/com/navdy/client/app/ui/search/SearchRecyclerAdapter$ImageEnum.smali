.class public final enum Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;
.super Ljava/lang/Enum;
.source "SearchRecyclerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ImageEnum"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;

.field public static final enum CONTACT:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;

.field public static final enum CONTACT_PHOTO:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;

.field public static final enum NONE:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;

.field public static final enum PLACE:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;

.field public static final enum SEARCH:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;

.field public static final enum SEARCH_HISTORY:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 135
    new-instance v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;

    const-string v1, "PLACE"

    invoke-direct {v0, v1, v3}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;->PLACE:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;

    .line 136
    new-instance v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;

    const-string v1, "CONTACT"

    invoke-direct {v0, v1, v4}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;->CONTACT:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;

    .line 137
    new-instance v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;

    const-string v1, "CONTACT_PHOTO"

    invoke-direct {v0, v1, v5}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;->CONTACT_PHOTO:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;

    .line 138
    new-instance v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;

    const-string v1, "SEARCH"

    invoke-direct {v0, v1, v6}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;->SEARCH:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;

    .line 139
    new-instance v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;

    const-string v1, "SEARCH_HISTORY"

    invoke-direct {v0, v1, v7}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;->SEARCH_HISTORY:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;

    .line 140
    new-instance v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;

    const-string v1, "NONE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;->NONE:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;

    .line 134
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;

    sget-object v1, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;->PLACE:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;->CONTACT:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;->CONTACT_PHOTO:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;->SEARCH:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;->SEARCH_HISTORY:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;->NONE:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;->$VALUES:[Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 134
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 134
    const-class v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;

    return-object v0
.end method

.method public static values()[Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;
    .locals 1

    .prologue
    .line 134
    sget-object v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;->$VALUES:[Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;

    invoke-virtual {v0}, [Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;

    return-object v0
.end method

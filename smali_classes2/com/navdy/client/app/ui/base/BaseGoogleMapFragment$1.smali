.class Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$1;
.super Ljava/lang/Object;
.source "BaseGoogleMapFragment.java"

# interfaces
.implements Lcom/navdy/client/app/framework/location/NavdyLocationManager$OnNavdyLocationChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    .prologue
    .line 107
    iput-object p1, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$1;->this$0:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCarLocationChanged(Lcom/navdy/service/library/events/location/Coordinate;)V
    .locals 1
    .param p1, "carLocation"    # Lcom/navdy/service/library/events/location/Coordinate;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 126
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$1;->this$0:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    invoke-static {v0, p1}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->access$200(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;Lcom/navdy/service/library/events/location/Coordinate;)V

    .line 127
    return-void
.end method

.method public onPhoneLocationChanged(Lcom/navdy/service/library/events/location/Coordinate;)V
    .locals 2
    .param p1, "phoneLocation"    # Lcom/navdy/service/library/events/location/Coordinate;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 110
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$1;->this$0:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    new-instance v1, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$1$1;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$1$1;-><init>(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$1;)V

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->getMapAsync(Lcom/google/android/gms/maps/OnMapReadyCallback;)V

    .line 122
    return-void
.end method

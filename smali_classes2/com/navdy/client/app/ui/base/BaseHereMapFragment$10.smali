.class Lcom/navdy/client/app/ui/base/BaseHereMapFragment$10;
.super Ljava/lang/Object;
.source "BaseHereMapFragment.java"

# interfaces
.implements Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentReady;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->centerOnUserLocationAndDestination(Lcom/navdy/client/app/framework/models/Destination;Lcom/here/android/mpa/mapping/Map$Animation;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

.field final synthetic val$animation:Lcom/here/android/mpa/mapping/Map$Animation;

.field final synthetic val$destination:Lcom/navdy/client/app/framework/models/Destination;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;Lcom/navdy/client/app/framework/models/Destination;Lcom/here/android/mpa/mapping/Map$Animation;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    .prologue
    .line 359
    iput-object p1, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$10;->this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    iput-object p2, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$10;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    iput-object p3, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$10;->val$animation:Lcom/here/android/mpa/mapping/Map$Animation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;)V
    .locals 0
    .param p1, "error"    # Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 380
    return-void
.end method

.method public onReady(Lcom/here/android/mpa/mapping/Map;)V
    .locals 8
    .param p1, "hereMap"    # Lcom/here/android/mpa/mapping/Map;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 362
    iget-object v3, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$10;->this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    invoke-static {v3}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->access$1400(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;)Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->getSmartStartCoordinates()Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v1

    .line 365
    .local v1, "location":Lcom/navdy/service/library/events/location/Coordinate;
    iget-object v3, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$10;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    iget-wide v4, v3, Lcom/navdy/client/app/framework/models/Destination;->displayLat:D

    iget-object v3, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$10;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    iget-wide v6, v3, Lcom/navdy/client/app/framework/models/Destination;->displayLong:D

    invoke-static {v4, v5, v6, v7}, Lcom/navdy/client/app/framework/map/MapUtils;->isValidSetOfCoordinates(DD)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 366
    new-instance v0, Lcom/here/android/mpa/common/GeoCoordinate;

    iget-object v3, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$10;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    iget-wide v4, v3, Lcom/navdy/client/app/framework/models/Destination;->displayLat:D

    iget-object v3, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$10;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    iget-wide v6, v3, Lcom/navdy/client/app/framework/models/Destination;->displayLong:D

    invoke-direct {v0, v4, v5, v6, v7}, Lcom/here/android/mpa/common/GeoCoordinate;-><init>(DD)V

    .line 371
    .local v0, "destinationGeo":Lcom/here/android/mpa/common/GeoCoordinate;
    :goto_0
    if-eqz v1, :cond_1

    .line 372
    new-instance v2, Lcom/here/android/mpa/common/GeoCoordinate;

    iget-object v3, v1, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    iget-object v3, v1, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    invoke-direct {v2, v4, v5, v6, v7}, Lcom/here/android/mpa/common/GeoCoordinate;-><init>(DD)V

    .line 373
    .local v2, "startGeo":Lcom/here/android/mpa/common/GeoCoordinate;
    iget-object v3, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$10;->this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    iget-object v4, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$10;->val$animation:Lcom/here/android/mpa/mapping/Map$Animation;

    invoke-static {v3, v2, v0, v4}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->access$1600(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/mapping/Map$Animation;)V

    .line 377
    .end local v2    # "startGeo":Lcom/here/android/mpa/common/GeoCoordinate;
    :goto_1
    return-void

    .line 368
    .end local v0    # "destinationGeo":Lcom/here/android/mpa/common/GeoCoordinate;
    :cond_0
    new-instance v0, Lcom/here/android/mpa/common/GeoCoordinate;

    iget-object v3, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$10;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    iget-wide v4, v3, Lcom/navdy/client/app/framework/models/Destination;->navigationLat:D

    iget-object v3, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$10;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    iget-wide v6, v3, Lcom/navdy/client/app/framework/models/Destination;->navigationLong:D

    invoke-direct {v0, v4, v5, v6, v7}, Lcom/here/android/mpa/common/GeoCoordinate;-><init>(DD)V

    .restart local v0    # "destinationGeo":Lcom/here/android/mpa/common/GeoCoordinate;
    goto :goto_0

    .line 375
    :cond_1
    iget-object v3, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$10;->this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    iget-object v4, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$10;->val$animation:Lcom/here/android/mpa/mapping/Map$Animation;

    invoke-static {v3, v0, v4}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->access$1500(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/mapping/Map$Animation;)V

    goto :goto_1
.end method

.class Lcom/navdy/client/app/ui/base/BaseHereMapFragment$3$1;
.super Ljava/lang/Object;
.source "BaseHereMapFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/base/BaseHereMapFragment$3;->onEngineInitializationCompleted(Lcom/here/android/mpa/common/OnEngineInitListener$Error;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/client/app/ui/base/BaseHereMapFragment$3;

.field final synthetic val$error:Lcom/here/android/mpa/common/OnEngineInitListener$Error;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/base/BaseHereMapFragment$3;Lcom/here/android/mpa/common/OnEngineInitListener$Error;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/client/app/ui/base/BaseHereMapFragment$3;

    .prologue
    .line 134
    iput-object p1, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$3$1;->this$1:Lcom/navdy/client/app/ui/base/BaseHereMapFragment$3;

    iput-object p2, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$3$1;->val$error:Lcom/here/android/mpa/common/OnEngineInitListener$Error;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 137
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$3$1;->val$error:Lcom/here/android/mpa/common/OnEngineInitListener$Error;

    sget-object v1, Lcom/here/android/mpa/common/OnEngineInitListener$Error;->NONE:Lcom/here/android/mpa/common/OnEngineInitListener$Error;

    if-eq v0, v1, :cond_0

    .line 138
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Could not initialize HereMapFragment: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$3$1;->val$error:Lcom/here/android/mpa/common/OnEngineInitListener$Error;

    invoke-virtual {v2}, Lcom/here/android/mpa/common/OnEngineInitListener$Error;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 139
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$3$1;->this$1:Lcom/navdy/client/app/ui/base/BaseHereMapFragment$3;

    iget-object v0, v0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$3;->this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    sget-object v1, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$State;->FAILED:Lcom/navdy/client/app/ui/base/BaseHereMapFragment$State;

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->access$302(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;Lcom/navdy/client/app/ui/base/BaseHereMapFragment$State;)Lcom/navdy/client/app/ui/base/BaseHereMapFragment$State;

    .line 140
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$3$1;->this$1:Lcom/navdy/client/app/ui/base/BaseHereMapFragment$3;

    iget-object v0, v0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$3;->this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    sget-object v1, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;->MAP_ENGINE_ERROR:Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->access$602(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;)Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;

    .line 141
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$3$1;->this$1:Lcom/navdy/client/app/ui/base/BaseHereMapFragment$3;

    iget-object v0, v0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$3;->this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    sget-object v1, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;->MAP_ENGINE_ERROR:Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->access$700(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;)V

    .line 170
    :goto_0
    return-void

    .line 145
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$3$1;->this$1:Lcom/navdy/client/app/ui/base/BaseHereMapFragment$3;

    iget-object v0, v0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$3;->this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    iget-object v1, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$3$1;->this$1:Lcom/navdy/client/app/ui/base/BaseHereMapFragment$3;

    iget-object v1, v1, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$3;->this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    invoke-virtual {v1}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->getMap()Lcom/here/android/mpa/mapping/Map;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->access$202(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;Lcom/here/android/mpa/mapping/Map;)Lcom/here/android/mpa/mapping/Map;

    .line 147
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$3$1;->this$1:Lcom/navdy/client/app/ui/base/BaseHereMapFragment$3;

    iget-object v0, v0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$3;->this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    invoke-static {v0}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->access$200(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;)Lcom/here/android/mpa/mapping/Map;

    move-result-object v0

    if-nez v0, :cond_1

    .line 148
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "hereMap is null at fragment init"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 149
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$3$1;->this$1:Lcom/navdy/client/app/ui/base/BaseHereMapFragment$3;

    iget-object v0, v0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$3;->this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    sget-object v1, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$State;->FAILED:Lcom/navdy/client/app/ui/base/BaseHereMapFragment$State;

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->access$302(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;Lcom/navdy/client/app/ui/base/BaseHereMapFragment$State;)Lcom/navdy/client/app/ui/base/BaseHereMapFragment$State;

    .line 150
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$3$1;->this$1:Lcom/navdy/client/app/ui/base/BaseHereMapFragment$3;

    iget-object v0, v0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$3;->this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    sget-object v1, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;->MAP_ENGINE_ERROR:Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->access$602(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;)Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;

    .line 151
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$3$1;->this$1:Lcom/navdy/client/app/ui/base/BaseHereMapFragment$3;

    iget-object v0, v0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$3;->this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    sget-object v1, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;->NO_MAP_ERROR:Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->access$700(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;)V

    goto :goto_0

    .line 155
    :cond_1
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$3$1;->this$1:Lcom/navdy/client/app/ui/base/BaseHereMapFragment$3;

    iget-object v0, v0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$3;->this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    invoke-static {v0}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->access$800(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;)V

    .line 156
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$3$1;->this$1:Lcom/navdy/client/app/ui/base/BaseHereMapFragment$3;

    iget-object v0, v0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$3;->this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    sget-object v1, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$State;->INITIALIZED:Lcom/navdy/client/app/ui/base/BaseHereMapFragment$State;

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->access$302(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;Lcom/navdy/client/app/ui/base/BaseHereMapFragment$State;)Lcom/navdy/client/app/ui/base/BaseHereMapFragment$State;

    .line 157
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "fragment initialized, calling initCompleteListeners"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 159
    :goto_1
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$3$1;->this$1:Lcom/navdy/client/app/ui/base/BaseHereMapFragment$3;

    iget-object v0, v0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$3;->this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    invoke-static {v0}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->access$900(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;)Ljava/util/Queue;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 160
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$3$1;->this$1:Lcom/navdy/client/app/ui/base/BaseHereMapFragment$3;

    iget-object v0, v0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$3;->this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    invoke-static {v0}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->access$200(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;)Lcom/here/android/mpa/mapping/Map;

    move-result-object v0

    if-nez v0, :cond_3

    .line 161
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "Calling initCompleteListeners.onInit with a null hereMap!"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 169
    :cond_2
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$3$1;->this$1:Lcom/navdy/client/app/ui/base/BaseHereMapFragment$3;

    iget-object v0, v0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$3;->this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    invoke-static {v0}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->access$500(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$3$1;->this$1:Lcom/navdy/client/app/ui/base/BaseHereMapFragment$3;

    iget-object v1, v1, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$3;->this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    invoke-static {v1}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->access$1000(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 164
    :cond_3
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$3$1;->this$1:Lcom/navdy/client/app/ui/base/BaseHereMapFragment$3;

    iget-object v0, v0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$3;->this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    invoke-static {v0}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->access$900(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;)Ljava/util/Queue;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentInitialized;

    iget-object v1, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$3$1;->this$1:Lcom/navdy/client/app/ui/base/BaseHereMapFragment$3;

    iget-object v1, v1, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$3;->this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    invoke-static {v1}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->access$200(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;)Lcom/here/android/mpa/mapping/Map;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentInitialized;->onInit(Lcom/here/android/mpa/mapping/Map;)V

    goto :goto_1
.end method

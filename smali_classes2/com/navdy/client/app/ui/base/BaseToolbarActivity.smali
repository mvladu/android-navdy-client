.class public abstract Lcom/navdy/client/app/ui/base/BaseToolbarActivity;
.super Lcom/navdy/client/app/ui/base/BaseActivity;
.source "BaseToolbarActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/ui/base/BaseToolbarActivity;I)Landroid/support/v7/widget/Toolbar;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/base/BaseToolbarActivity;
    .param p1, "x1"    # I

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->initActionBar(I)Landroid/support/v7/widget/Toolbar;

    move-result-object v0

    return-object v0
.end method

.method private initActionBar(I)Landroid/support/v7/widget/Toolbar;
    .locals 3
    .param p1, "toolbarResId"    # I
        .annotation build Landroid/support/annotation/IdRes;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x1

    .line 48
    invoke-virtual {p0, p1}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/Toolbar;

    .line 49
    .local v1, "myToolbar":Landroid/support/v7/widget/Toolbar;
    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 50
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    .line 51
    .local v0, "actionBar":Landroid/support/v7/app/ActionBar;
    if-eqz v0, :cond_0

    .line 53
    invoke-virtual {v0, v2}, Landroid/support/v7/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 54
    invoke-virtual {v0, v2}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 56
    :cond_0
    return-object v1
.end method


# virtual methods
.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 61
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 66
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 63
    :pswitch_0
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->onBackPressed()V

    .line 64
    const/4 v0, 0x1

    goto :goto_0

    .line 61
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

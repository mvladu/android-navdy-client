.class Lcom/navdy/client/app/ui/base/BaseActivity$4;
.super Ljava/lang/Object;
.source "BaseActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/base/BaseActivity;->showExplicitObdSettingForBlacklistDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/base/BaseActivity;

.field final synthetic val$sharedPrefs:Landroid/content/SharedPreferences;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/base/BaseActivity;Landroid/content/SharedPreferences;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/base/BaseActivity;

    .prologue
    .line 353
    iput-object p1, p0, Lcom/navdy/client/app/ui/base/BaseActivity$4;->this$0:Lcom/navdy/client/app/ui/base/BaseActivity;

    iput-object p2, p0, Lcom/navdy/client/app/ui/base/BaseActivity$4;->val$sharedPrefs:Landroid/content/SharedPreferences;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialogInterface"    # Landroid/content/DialogInterface;
    .param p2, "i"    # I

    .prologue
    .line 356
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseActivity$4;->this$0:Lcom/navdy/client/app/ui/base/BaseActivity;

    iget-object v0, v0, Lcom/navdy/client/app/ui/base/BaseActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "user turned obd data off"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 357
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseActivity$4;->val$sharedPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "hud_obd_on_enabled"

    sget-object v2, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;->SCAN_OFF:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 358
    return-void
.end method

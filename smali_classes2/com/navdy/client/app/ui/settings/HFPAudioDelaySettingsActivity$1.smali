.class Lcom/navdy/client/app/ui/settings/HFPAudioDelaySettingsActivity$1;
.super Ljava/lang/Object;
.source "HFPAudioDelaySettingsActivity.java"

# interfaces
.implements Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$ChoiceListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/settings/HFPAudioDelaySettingsActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/settings/HFPAudioDelaySettingsActivity;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/settings/HFPAudioDelaySettingsActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/settings/HFPAudioDelaySettingsActivity;

    .prologue
    .line 52
    iput-object p1, p0, Lcom/navdy/client/app/ui/settings/HFPAudioDelaySettingsActivity$1;->this$0:Lcom/navdy/client/app/ui/settings/HFPAudioDelaySettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onChoiceSelected(Ljava/lang/String;I)V
    .locals 3
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 55
    invoke-static {}, Lcom/navdy/client/app/ui/settings/HFPAudioDelaySettingsActivity;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "User selected delay level "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 56
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/HFPAudioDelaySettingsActivity$1;->this$0:Lcom/navdy/client/app/ui/settings/HFPAudioDelaySettingsActivity;

    invoke-static {v0, p2}, Lcom/navdy/client/app/ui/settings/HFPAudioDelaySettingsActivity;->access$102(Lcom/navdy/client/app/ui/settings/HFPAudioDelaySettingsActivity;I)I

    .line 57
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/HFPAudioDelaySettingsActivity$1;->this$0:Lcom/navdy/client/app/ui/settings/HFPAudioDelaySettingsActivity;

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/HFPAudioDelaySettingsActivity;->ttsAudioRouter:Lcom/navdy/client/app/framework/util/TTSAudioRouter;

    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/HFPAudioDelaySettingsActivity$1;->this$0:Lcom/navdy/client/app/ui/settings/HFPAudioDelaySettingsActivity;

    invoke-static {v1}, Lcom/navdy/client/app/ui/settings/HFPAudioDelaySettingsActivity;->access$100(Lcom/navdy/client/app/ui/settings/HFPAudioDelaySettingsActivity;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->setPreferredHFPDelayLevel(I)V

    .line 58
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/HFPAudioDelaySettingsActivity$1;->this$0:Lcom/navdy/client/app/ui/settings/HFPAudioDelaySettingsActivity;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/settings/HFPAudioDelaySettingsActivity;->access$202(Lcom/navdy/client/app/ui/settings/HFPAudioDelaySettingsActivity;Z)Z

    .line 59
    return-void
.end method

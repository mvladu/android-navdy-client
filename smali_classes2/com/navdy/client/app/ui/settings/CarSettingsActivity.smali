.class public Lcom/navdy/client/app/ui/settings/CarSettingsActivity;
.super Lcom/navdy/client/app/ui/base/BaseEditActivity;
.source "CarSettingsActivity.java"


# instance fields
.field private autoOn:Landroid/widget/Switch;

.field private compact:Landroid/widget/RadioButton;

.field private compactUiLinearLayout:Landroid/widget/LinearLayout;

.field private customerPrefs:Landroid/content/SharedPreferences;

.field private normal:Landroid/widget/RadioButton;

.field private obdEnabledString:Ljava/lang/String;

.field private obdSwitch:Landroid/widget/Switch;

.field private onObdSwitchChangedListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field private sharedPrefs:Landroid/content/SharedPreferences;

.field private vehicleInfo:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseEditActivity;-><init>()V

    .line 37
    const-string v0, ""

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->obdEnabledString:Ljava/lang/String;

    .line 42
    new-instance v0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity$1;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/ui/settings/CarSettingsActivity$1;-><init>(Lcom/navdy/client/app/ui/settings/CarSettingsActivity;)V

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->onObdSwitchChangedListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/ui/settings/CarSettingsActivity;)Landroid/widget/Switch;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/CarSettingsActivity;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->obdSwitch:Landroid/widget/Switch;

    return-object v0
.end method

.method static synthetic access$102(Lcom/navdy/client/app/ui/settings/CarSettingsActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/CarSettingsActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 30
    iput-boolean p1, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->somethingChanged:Z

    return p1
.end method

.method static synthetic access$200(Lcom/navdy/client/app/ui/settings/CarSettingsActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/CarSettingsActivity;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->obdEnabledString:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$202(Lcom/navdy/client/app/ui/settings/CarSettingsActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/CarSettingsActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->obdEnabledString:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300(Lcom/navdy/client/app/ui/settings/CarSettingsActivity;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/CarSettingsActivity;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->sharedPrefs:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method private updateUiScalingSetting(Z)V
    .locals 2
    .param p1, "uiScalingIsOn"    # Z

    .prologue
    .line 114
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->normal:Landroid/widget/RadioButton;

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 115
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->compact:Landroid/widget/RadioButton;

    invoke-virtual {v0, p1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 116
    return-void

    .line 114
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onAutoOnClick(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 240
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->autoOn:Landroid/widget/Switch;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->autoOn:Landroid/widget/Switch;

    invoke-virtual {v0}, Landroid/widget/Switch;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 241
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->autoOn:Landroid/widget/Switch;

    invoke-virtual {v0}, Landroid/widget/Switch;->performClick()Z

    .line 243
    :cond_0
    return-void
.end method

.method public onCompactClick(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v0, 0x1

    .line 108
    iput-boolean v0, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->somethingChanged:Z

    .line 109
    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->updateUiScalingSetting(Z)V

    .line 110
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 122
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseEditActivity;->onCreate(Landroid/os/Bundle;)V

    .line 124
    const v0, 0x7f0300f0

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->setContentView(I)V

    .line 127
    new-instance v0, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;-><init>(Lcom/navdy/client/app/ui/base/BaseToolbarActivity;)V

    const v1, 0x7f0802b3

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->title(I)Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->build()Landroid/support/v7/widget/Toolbar;

    .line 130
    const v0, 0x7f100387

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->vehicleInfo:Landroid/widget/TextView;

    .line 131
    const v0, 0x7f100389

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Switch;

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->autoOn:Landroid/widget/Switch;

    .line 132
    const v0, 0x7f10038d

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Switch;

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->obdSwitch:Landroid/widget/Switch;

    .line 133
    const v0, 0x7f10001e

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->normal:Landroid/widget/RadioButton;

    .line 134
    const v0, 0x7f100393

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->compact:Landroid/widget/RadioButton;

    .line 135
    const v0, 0x7f100390

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->compactUiLinearLayout:Landroid/widget/LinearLayout;

    .line 136
    return-void
.end method

.method public onNormalClick(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 103
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->somethingChanged:Z

    .line 104
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->updateUiScalingSetting(Z)V

    .line 105
    return-void
.end method

.method public onObdSettingChangeClick(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 246
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->obdSwitch:Landroid/widget/Switch;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->obdSwitch:Landroid/widget/Switch;

    invoke-virtual {v0}, Landroid/widget/Switch;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 247
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->obdSwitch:Landroid/widget/Switch;

    invoke-virtual {v0}, Landroid/widget/Switch;->performClick()Z

    .line 249
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 178
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->obdSwitch:Landroid/widget/Switch;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 179
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseEditActivity;->onPause()V

    .line 180
    return-void
.end method

.method protected onResume()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 140
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseEditActivity;->onResume()V

    .line 142
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getCustomerPreferences()Landroid/content/SharedPreferences;

    move-result-object v4

    iput-object v4, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->customerPrefs:Landroid/content/SharedPreferences;

    .line 144
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v4

    iput-object v4, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->sharedPrefs:Landroid/content/SharedPreferences;

    .line 145
    iget-object v4, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->sharedPrefs:Landroid/content/SharedPreferences;

    const-string v5, "hud_auto_on_enabled"

    invoke-interface {v4, v5, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 146
    .local v0, "autoOnEnabled":Z
    iget-object v4, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->sharedPrefs:Landroid/content/SharedPreferences;

    const-string v5, "hud_obd_on_enabled"

    sget-object v6, Lcom/navdy/client/app/ui/settings/SettingsConstants;->HUD_OBD_ON_DEFAULT:Ljava/lang/String;

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->obdEnabledString:Ljava/lang/String;

    .line 149
    iget-object v4, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->vehicleInfo:Landroid/widget/TextView;

    if-eqz v4, :cond_1

    .line 150
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getCarYearMakeModelString()Ljava/lang/String;

    move-result-object v1

    .line 151
    .local v1, "carInfo":Ljava/lang/String;
    invoke-static {v1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 152
    const v4, 0x7f08049d

    invoke-virtual {p0, v4}, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 154
    :cond_0
    iget-object v4, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->vehicleInfo:Landroid/widget/TextView;

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 157
    .end local v1    # "carInfo":Ljava/lang/String;
    :cond_1
    iget-object v4, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->obdEnabledString:Ljava/lang/String;

    sget-object v5, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;->SCAN_DEFAULT_ON:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;

    invoke-virtual {v5}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->obdEnabledString:Ljava/lang/String;

    sget-object v5, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;->SCAN_ON:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;

    .line 158
    invoke-virtual {v5}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 159
    :cond_2
    iget-object v4, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->obdSwitch:Landroid/widget/Switch;

    iget-object v5, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->onObdSwitchChangedListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {p0, v4, v8, v5}, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->initCompoundButton(Landroid/widget/CompoundButton;ZLandroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 164
    :goto_0
    iget-object v4, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->autoOn:Landroid/widget/Switch;

    invoke-virtual {p0, v4, v0}, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->initCompoundButton(Landroid/widget/CompoundButton;Z)V

    .line 166
    iget-object v4, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->sharedPrefs:Landroid/content/SharedPreferences;

    const-string v5, "hud_compact_capable"

    invoke-interface {v4, v5, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 167
    .local v2, "compactCapable":Z
    if-eqz v2, :cond_4

    .line 168
    iget-object v4, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->compactUiLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 169
    iget-object v4, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->sharedPrefs:Landroid/content/SharedPreferences;

    const-string v5, "hud_ui_scaling"

    invoke-interface {v4, v5, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 170
    .local v3, "uiScalingIsOn":Z
    invoke-direct {p0, v3}, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->updateUiScalingSetting(Z)V

    .line 174
    .end local v3    # "uiScalingIsOn":Z
    :goto_1
    return-void

    .line 161
    .end local v2    # "compactCapable":Z
    :cond_3
    iget-object v4, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->obdSwitch:Landroid/widget/Switch;

    iget-object v5, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->onObdSwitchChangedListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {p0, v4, v7, v5}, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->initCompoundButton(Landroid/widget/CompoundButton;ZLandroid/widget/CompoundButton$OnCheckedChangeListener;)V

    goto :goto_0

    .line 172
    .restart local v2    # "compactCapable":Z
    :cond_4
    iget-object v4, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->compactUiLinearLayout:Landroid/widget/LinearLayout;

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_1
.end method

.method protected saveChanges()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 196
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->compact:Landroid/widget/RadioButton;

    invoke-virtual {v2}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v1

    .line 197
    .local v1, "uiScalingIsOn":Z
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->autoOn:Landroid/widget/Switch;

    invoke-virtual {v2}, Landroid/widget/Switch;->isChecked()Z

    move-result v0

    .line 199
    .local v0, "autoOnIsEnabled":Z
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "current obd setting: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->obdEnabledString:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 201
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->sharedPrefs:Landroid/content/SharedPreferences;

    if-eqz v2, :cond_0

    .line 202
    new-instance v2, Lcom/navdy/client/app/ui/settings/CarSettingsActivity$2;

    invoke-direct {v2, p0, v1, v0}, Lcom/navdy/client/app/ui/settings/CarSettingsActivity$2;-><init>(Lcom/navdy/client/app/ui/settings/CarSettingsActivity;ZZ)V

    new-array v3, v5, [Ljava/lang/Object;

    .line 233
    invoke-virtual {v2, v3}, Lcom/navdy/client/app/ui/settings/CarSettingsActivity$2;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 237
    :goto_0
    return-void

    .line 235
    :cond_0
    const v2, 0x7f08042a

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->showShortToast(I[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public startEditInfoActivity(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 185
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 186
    .local v0, "in":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->startActivity(Landroid/content/Intent;)V

    .line 187
    return-void
.end method

.method public startObdLocatorActivity(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 190
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/navdy/client/app/ui/firstlaunch/CarInfoActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 191
    .local v0, "in":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->startActivity(Landroid/content/Intent;)V

    .line 192
    return-void
.end method

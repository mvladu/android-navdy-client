.class public Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;
.super Lcom/navdy/client/app/ui/base/BaseEditActivity;
.source "NavigationSettingsActivity.java"


# instance fields
.field private autoRecalc:Landroid/widget/Switch;

.field private autoTrains:Landroid/widget/Switch;

.field private ferries:Landroid/widget/Switch;

.field private highways:Landroid/widget/Switch;

.field private routeCalculationFastest:Landroid/widget/RadioButton;

.field private routeCalculationShortest:Landroid/widget/RadioButton;

.field private sharedPrefs:Landroid/content/SharedPreferences;

.field private tollRoads:Landroid/widget/Switch;

.field private tunnels:Landroid/widget/Switch;

.field private unpavedRoads:Landroid/widget/Switch;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseEditActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public onAutoRecalcClick(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->autoRecalc:Landroid/widget/Switch;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->autoRecalc:Landroid/widget/Switch;

    invoke-virtual {v0}, Landroid/widget/Switch;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->autoRecalc:Landroid/widget/Switch;

    invoke-virtual {v0}, Landroid/widget/Switch;->performClick()Z

    .line 90
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 12
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 35
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseEditActivity;->onCreate(Landroid/os/Bundle;)V

    .line 36
    const v8, 0x7f0300f8

    invoke-virtual {p0, v8}, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->setContentView(I)V

    .line 39
    new-instance v8, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;

    invoke-direct {v8, p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;-><init>(Lcom/navdy/client/app/ui/base/BaseToolbarActivity;)V

    const v11, 0x7f0802be

    invoke-virtual {v8, v11}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->title(I)Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;

    move-result-object v8

    invoke-virtual {v8}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->build()Landroid/support/v7/widget/Toolbar;

    .line 42
    const v8, 0x7f1003be

    invoke-virtual {p0, v8}, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/RadioButton;

    iput-object v8, p0, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->routeCalculationFastest:Landroid/widget/RadioButton;

    .line 43
    const v8, 0x7f1003bf

    invoke-virtual {p0, v8}, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/RadioButton;

    iput-object v8, p0, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->routeCalculationShortest:Landroid/widget/RadioButton;

    .line 44
    const v8, 0x7f1003c0

    invoke-virtual {p0, v8}, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Switch;

    iput-object v8, p0, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->autoRecalc:Landroid/widget/Switch;

    .line 45
    const v8, 0x7f1003c3

    invoke-virtual {p0, v8}, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Switch;

    iput-object v8, p0, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->highways:Landroid/widget/Switch;

    .line 46
    const v8, 0x7f1003c4

    invoke-virtual {p0, v8}, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Switch;

    iput-object v8, p0, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->tollRoads:Landroid/widget/Switch;

    .line 47
    const v8, 0x7f1003c5

    invoke-virtual {p0, v8}, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Switch;

    iput-object v8, p0, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->ferries:Landroid/widget/Switch;

    .line 48
    const v8, 0x7f1003c6

    invoke-virtual {p0, v8}, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Switch;

    iput-object v8, p0, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->tunnels:Landroid/widget/Switch;

    .line 49
    const v8, 0x7f1003c7

    invoke-virtual {p0, v8}, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Switch;

    iput-object v8, p0, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->unpavedRoads:Landroid/widget/Switch;

    .line 50
    const v8, 0x7f1003c8

    invoke-virtual {p0, v8}, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Switch;

    iput-object v8, p0, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->autoTrains:Landroid/widget/Switch;

    .line 53
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v8

    iput-object v8, p0, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->sharedPrefs:Landroid/content/SharedPreferences;

    .line 54
    iget-object v8, p0, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->sharedPrefs:Landroid/content/SharedPreferences;

    const-string v11, "nav_route_calculation"

    invoke-interface {v8, v11, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 55
    .local v2, "calculateShortestRouteIsOn":Z
    iget-object v8, p0, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->sharedPrefs:Landroid/content/SharedPreferences;

    const-string v11, "nav_auto_recalc"

    invoke-interface {v8, v11, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 56
    .local v0, "autoRecalcIsOn":Z
    iget-object v8, p0, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->sharedPrefs:Landroid/content/SharedPreferences;

    const-string v11, "nav_highways"

    invoke-interface {v8, v11, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    .line 57
    .local v4, "highwaysIsOn":Z
    iget-object v8, p0, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->sharedPrefs:Landroid/content/SharedPreferences;

    const-string v11, "nav_toll_roads"

    invoke-interface {v8, v11, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    .line 58
    .local v5, "tollRoadsIsOn":Z
    iget-object v8, p0, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->sharedPrefs:Landroid/content/SharedPreferences;

    const-string v11, "nav_ferries"

    invoke-interface {v8, v11, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 59
    .local v3, "ferriesIsOn":Z
    iget-object v8, p0, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->sharedPrefs:Landroid/content/SharedPreferences;

    const-string v11, "nav_tunnels"

    invoke-interface {v8, v11, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    .line 60
    .local v6, "tunnelsIsOn":Z
    iget-object v8, p0, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->sharedPrefs:Landroid/content/SharedPreferences;

    const-string v11, "nav_unpaved_roads"

    invoke-interface {v8, v11, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    .line 61
    .local v7, "unpavedRoadsIsOn":Z
    iget-object v8, p0, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->sharedPrefs:Landroid/content/SharedPreferences;

    const-string v11, "nav_auto_trains"

    invoke-interface {v8, v11, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 64
    .local v1, "autoTrainsIsOn":Z
    iget-object v11, p0, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->routeCalculationFastest:Landroid/widget/RadioButton;

    if-nez v2, :cond_0

    move v8, v9

    :goto_0
    invoke-virtual {p0, v11, v8}, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->initCompoundButton(Landroid/widget/CompoundButton;Z)V

    .line 65
    iget-object v8, p0, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->routeCalculationShortest:Landroid/widget/RadioButton;

    invoke-virtual {p0, v8, v2}, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->initCompoundButton(Landroid/widget/CompoundButton;Z)V

    .line 66
    iget-object v8, p0, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->autoRecalc:Landroid/widget/Switch;

    invoke-virtual {p0, v8, v0}, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->initCompoundButton(Landroid/widget/CompoundButton;Z)V

    .line 67
    iget-object v8, p0, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->highways:Landroid/widget/Switch;

    invoke-virtual {p0, v8, v4}, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->initCompoundButton(Landroid/widget/CompoundButton;Z)V

    .line 68
    iget-object v8, p0, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->tollRoads:Landroid/widget/Switch;

    invoke-virtual {p0, v8, v5}, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->initCompoundButton(Landroid/widget/CompoundButton;Z)V

    .line 69
    iget-object v8, p0, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->ferries:Landroid/widget/Switch;

    invoke-virtual {p0, v8, v3}, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->initCompoundButton(Landroid/widget/CompoundButton;Z)V

    .line 70
    iget-object v8, p0, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->tunnels:Landroid/widget/Switch;

    invoke-virtual {p0, v8, v6}, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->initCompoundButton(Landroid/widget/CompoundButton;Z)V

    .line 71
    iget-object v8, p0, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->unpavedRoads:Landroid/widget/Switch;

    invoke-virtual {p0, v8, v7}, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->initCompoundButton(Landroid/widget/CompoundButton;Z)V

    .line 72
    iget-object v8, p0, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->autoTrains:Landroid/widget/Switch;

    invoke-virtual {p0, v8, v1}, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->initCompoundButton(Landroid/widget/CompoundButton;Z)V

    .line 73
    return-void

    :cond_0
    move v8, v10

    .line 64
    goto :goto_0
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 79
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseEditActivity;->onResume()V

    .line 81
    const-string v0, "Settings_Navigation"

    invoke-static {v0}, Lcom/navdy/client/app/tracking/Tracker;->tagScreen(Ljava/lang/String;)V

    .line 82
    return-void
.end method

.method protected saveChanges()V
    .locals 15

    .prologue
    const/4 v6, 0x1

    const/4 v14, 0x0

    .line 94
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->sharedPrefs:Landroid/content/SharedPreferences;

    if-eqz v2, :cond_1

    .line 95
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->sharedPrefs:Landroid/content/SharedPreferences;

    const-string v3, "nav_serial_number"

    const-wide/16 v4, 0x0

    invoke-interface {v2, v3, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 96
    .local v0, "serialNumber":J
    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    .line 98
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->sharedPrefs:Landroid/content/SharedPreferences;

    .line 99
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "nav_serial_number"

    .line 100
    invoke-interface {v2, v3, v0, v1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "nav_route_calculation"

    iget-object v4, p0, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->routeCalculationShortest:Landroid/widget/RadioButton;

    .line 101
    invoke-virtual {v4}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v4

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "nav_auto_recalc"

    iget-object v4, p0, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->autoRecalc:Landroid/widget/Switch;

    .line 102
    invoke-virtual {v4}, Landroid/widget/Switch;->isChecked()Z

    move-result v4

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "nav_highways"

    iget-object v4, p0, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->highways:Landroid/widget/Switch;

    .line 103
    invoke-virtual {v4}, Landroid/widget/Switch;->isChecked()Z

    move-result v4

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "nav_toll_roads"

    iget-object v4, p0, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->tollRoads:Landroid/widget/Switch;

    .line 104
    invoke-virtual {v4}, Landroid/widget/Switch;->isChecked()Z

    move-result v4

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "nav_ferries"

    iget-object v4, p0, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->ferries:Landroid/widget/Switch;

    .line 105
    invoke-virtual {v4}, Landroid/widget/Switch;->isChecked()Z

    move-result v4

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "nav_tunnels"

    iget-object v4, p0, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->tunnels:Landroid/widget/Switch;

    .line 106
    invoke-virtual {v4}, Landroid/widget/Switch;->isChecked()Z

    move-result v4

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "nav_unpaved_roads"

    iget-object v4, p0, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->unpavedRoads:Landroid/widget/Switch;

    .line 107
    invoke-virtual {v4}, Landroid/widget/Switch;->isChecked()Z

    move-result v4

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "nav_auto_trains"

    iget-object v4, p0, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->autoTrains:Landroid/widget/Switch;

    .line 108
    invoke-virtual {v4}, Landroid/widget/Switch;->isChecked()Z

    move-result v4

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 109
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 111
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->sharedPrefs:Landroid/content/SharedPreferences;

    const-string v3, "audio_speed_warnings"

    invoke-interface {v2, v3, v14}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v10

    .line 112
    .local v10, "spokenSpeedLimitWarningsAreOn":Z
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->sharedPrefs:Landroid/content/SharedPreferences;

    const-string v3, "audio_camera_warnings"

    invoke-interface {v2, v3, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v11

    .line 113
    .local v11, "spokenCameraWarningsAreOn":Z
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->sharedPrefs:Landroid/content/SharedPreferences;

    const-string v3, "audio_turn_by_turn_instructions"

    invoke-interface {v2, v3, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v12

    .line 115
    .local v12, "spokenTurnByTurnIsOn":Z
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->routeCalculationShortest:Landroid/widget/RadioButton;

    .line 116
    invoke-virtual {v2}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v2

    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->autoRecalc:Landroid/widget/Switch;

    .line 117
    invoke-virtual {v3}, Landroid/widget/Switch;->isChecked()Z

    move-result v3

    iget-object v4, p0, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->highways:Landroid/widget/Switch;

    .line 118
    invoke-virtual {v4}, Landroid/widget/Switch;->isChecked()Z

    move-result v4

    iget-object v5, p0, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->tollRoads:Landroid/widget/Switch;

    .line 119
    invoke-virtual {v5}, Landroid/widget/Switch;->isChecked()Z

    move-result v5

    iget-object v6, p0, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->ferries:Landroid/widget/Switch;

    .line 120
    invoke-virtual {v6}, Landroid/widget/Switch;->isChecked()Z

    move-result v6

    iget-object v7, p0, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->tunnels:Landroid/widget/Switch;

    .line 121
    invoke-virtual {v7}, Landroid/widget/Switch;->isChecked()Z

    move-result v7

    iget-object v8, p0, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->unpavedRoads:Landroid/widget/Switch;

    .line 122
    invoke-virtual {v8}, Landroid/widget/Switch;->isChecked()Z

    move-result v8

    iget-object v9, p0, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->autoTrains:Landroid/widget/Switch;

    .line 123
    invoke-virtual {v9}, Landroid/widget/Switch;->isChecked()Z

    move-result v9

    .line 115
    invoke-static/range {v0 .. v12}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->buildNavigationPreferences(JZZZZZZZZZZZ)Lcom/navdy/service/library/events/preferences/NavigationPreferences;

    move-result-object v13

    .line 129
    .local v13, "navPrefs":Lcom/navdy/service/library/events/preferences/NavigationPreferences;
    invoke-static {v13}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->sendNavSettingsToTheHud(Lcom/navdy/service/library/events/preferences/NavigationPreferences;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 130
    const v2, 0x7f08044f

    new-array v3, v14, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->showShortToast(I[Ljava/lang/Object;)V

    .line 131
    const-string v2, "Navigation_Settings_Changed"

    invoke-static {v2}, Lcom/navdy/client/app/tracking/Tracker;->tagEvent(Ljava/lang/String;)V

    .line 138
    .end local v0    # "serialNumber":J
    .end local v10    # "spokenSpeedLimitWarningsAreOn":Z
    .end local v11    # "spokenCameraWarningsAreOn":Z
    .end local v12    # "spokenTurnByTurnIsOn":Z
    .end local v13    # "navPrefs":Lcom/navdy/service/library/events/preferences/NavigationPreferences;
    :goto_0
    return-void

    .line 133
    .restart local v0    # "serialNumber":J
    .restart local v10    # "spokenSpeedLimitWarningsAreOn":Z
    .restart local v11    # "spokenCameraWarningsAreOn":Z
    .restart local v12    # "spokenTurnByTurnIsOn":Z
    .restart local v13    # "navPrefs":Lcom/navdy/service/library/events/preferences/NavigationPreferences;
    :cond_0
    const v2, 0x7f080455

    new-array v3, v14, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->showShortToast(I[Ljava/lang/Object;)V

    goto :goto_0

    .line 136
    .end local v0    # "serialNumber":J
    .end local v10    # "spokenSpeedLimitWarningsAreOn":Z
    .end local v11    # "spokenCameraWarningsAreOn":Z
    .end local v12    # "spokenTurnByTurnIsOn":Z
    .end local v13    # "navPrefs":Lcom/navdy/service/library/events/preferences/NavigationPreferences;
    :cond_1
    const v2, 0x7f080443

    new-array v3, v14, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;->showShortToast(I[Ljava/lang/Object;)V

    goto :goto_0
.end method

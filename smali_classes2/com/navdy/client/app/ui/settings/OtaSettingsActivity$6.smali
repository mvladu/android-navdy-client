.class Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$6;
.super Ljava/lang/Object;
.source "OtaSettingsActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->createDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;

    .prologue
    .line 673
    iput-object p1, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$6;->this$0:Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 676
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$6;->this$0:Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->access$700(Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;)Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "Download on LTE approved"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 677
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$6;->this$0:Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->access$800(Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;)Lcom/navdy/client/ota/OTAUpdateServiceInterface;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/navdy/client/ota/OTAUpdateServiceInterface;->setNetworkDownloadApproval(Z)V

    .line 678
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$6;->this$0:Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->access$800(Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;)Lcom/navdy/client/ota/OTAUpdateServiceInterface;

    move-result-object v0

    invoke-interface {v0}, Lcom/navdy/client/ota/OTAUpdateServiceInterface;->downloadOTAUpdate()V

    .line 679
    return-void
.end method

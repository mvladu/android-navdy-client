.class Lcom/navdy/client/app/ui/settings/AudioSettingsActivity$1;
.super Ljava/lang/Object;
.source "AudioSettingsActivity.java"

# interfaces
.implements Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$ChoiceListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;

    .prologue
    .line 258
    iput-object p1, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity$1;->this$0:Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onChoiceSelected(Ljava/lang/String;I)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 261
    packed-switch p2, :pswitch_data_0

    .line 274
    :goto_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity$1;->this$0:Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->access$000(Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;)V

    .line 275
    return-void

    .line 263
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity$1;->this$0:Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;

    const v1, 0x3e99999a    # 0.3f

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->setTtsLevel(F)V

    goto :goto_0

    .line 266
    :pswitch_1
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity$1;->this$0:Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;

    const v1, 0x3f19999a    # 0.6f

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->setTtsLevel(F)V

    goto :goto_0

    .line 269
    :pswitch_2
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity$1;->this$0:Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->setTtsLevel(F)V

    goto :goto_0

    .line 261
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

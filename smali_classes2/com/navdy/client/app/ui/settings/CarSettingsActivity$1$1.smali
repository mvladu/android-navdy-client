.class Lcom/navdy/client/app/ui/settings/CarSettingsActivity$1$1;
.super Ljava/lang/Object;
.source "CarSettingsActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/settings/CarSettingsActivity$1;->onCheckedChanged(Landroid/widget/CompoundButton;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/client/app/ui/settings/CarSettingsActivity$1;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/settings/CarSettingsActivity$1;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/client/app/ui/settings/CarSettingsActivity$1;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity$1$1;->this$1:Lcom/navdy/client/app/ui/settings/CarSettingsActivity$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 49
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity$1$1;->this$1:Lcom/navdy/client/app/ui/settings/CarSettingsActivity$1;

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity$1;->this$0:Lcom/navdy/client/app/ui/settings/CarSettingsActivity;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->access$102(Lcom/navdy/client/app/ui/settings/CarSettingsActivity;Z)Z

    .line 50
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity$1$1;->this$1:Lcom/navdy/client/app/ui/settings/CarSettingsActivity$1;

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity$1;->this$0:Lcom/navdy/client/app/ui/settings/CarSettingsActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->access$000(Lcom/navdy/client/app/ui/settings/CarSettingsActivity;)Landroid/widget/Switch;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity$1$1;->this$1:Lcom/navdy/client/app/ui/settings/CarSettingsActivity$1;

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity$1;->this$0:Lcom/navdy/client/app/ui/settings/CarSettingsActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->access$000(Lcom/navdy/client/app/ui/settings/CarSettingsActivity;)Landroid/widget/Switch;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Switch;->isChecked()Z

    move-result v0

    if-nez v0, :cond_1

    .line 53
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity$1$1;->this$1:Lcom/navdy/client/app/ui/settings/CarSettingsActivity$1;

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity$1;->this$0:Lcom/navdy/client/app/ui/settings/CarSettingsActivity;

    sget-object v1, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;->SCAN_OFF:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->access$202(Lcom/navdy/client/app/ui/settings/CarSettingsActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 90
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity$1$1;->this$1:Lcom/navdy/client/app/ui/settings/CarSettingsActivity$1;

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity$1;->this$0:Lcom/navdy/client/app/ui/settings/CarSettingsActivity;

    new-instance v1, Lcom/navdy/client/app/ui/settings/CarSettingsActivity$1$1$2;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/ui/settings/CarSettingsActivity$1$1$2;-><init>(Lcom/navdy/client/app/ui/settings/CarSettingsActivity$1$1;)V

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 96
    return-void

    .line 55
    :cond_1
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->carIsBlacklistedForObdData()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 56
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity$1$1;->this$1:Lcom/navdy/client/app/ui/settings/CarSettingsActivity$1;

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity$1;->this$0:Lcom/navdy/client/app/ui/settings/CarSettingsActivity;

    new-instance v1, Lcom/navdy/client/app/ui/settings/CarSettingsActivity$1$1$1;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/ui/settings/CarSettingsActivity$1$1$1;-><init>(Lcom/navdy/client/app/ui/settings/CarSettingsActivity$1$1;)V

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 84
    :cond_2
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity$1$1;->this$1:Lcom/navdy/client/app/ui/settings/CarSettingsActivity$1;

    iget-object v0, v0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity$1;->this$0:Lcom/navdy/client/app/ui/settings/CarSettingsActivity;

    sget-object v1, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;->SCAN_ON:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->access$202(Lcom/navdy/client/app/ui/settings/CarSettingsActivity;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0
.end method

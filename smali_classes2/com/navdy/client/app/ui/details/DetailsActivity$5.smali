.class Lcom/navdy/client/app/ui/details/DetailsActivity$5;
.super Ljava/lang/Object;
.source "DetailsActivity.java"

# interfaces
.implements Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/details/DetailsActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/details/DetailsActivity;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/details/DetailsActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/details/DetailsActivity;

    .prologue
    .line 314
    iput-object p1, p0, Lcom/navdy/client/app/ui/details/DetailsActivity$5;->this$0:Lcom/navdy/client/app/ui/details/DetailsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;)V
    .locals 6
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;
    .param p2, "error"    # Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;

    .prologue
    const/high16 v5, 0x41600000    # 14.0f

    const/4 v4, 0x0

    .line 331
    iget-object v2, p0, Lcom/navdy/client/app/ui/details/DetailsActivity$5;->this$0:Lcom/navdy/client/app/ui/details/DetailsActivity;

    invoke-static {v2, p1}, Lcom/navdy/client/app/ui/details/DetailsActivity;->access$102(Lcom/navdy/client/app/ui/details/DetailsActivity;Lcom/navdy/client/app/framework/models/Destination;)Lcom/navdy/client/app/framework/models/Destination;

    .line 333
    invoke-virtual {p1}, Lcom/navdy/client/app/framework/models/Destination;->hasOneValidSetOfCoordinates()Z

    move-result v2

    if-nez v2, :cond_0

    .line 334
    const v2, 0x7f08026e

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/navdy/client/app/ui/base/BaseActivity;->showLongToast(I[Ljava/lang/Object;)V

    .line 335
    iget-object v2, p0, Lcom/navdy/client/app/ui/details/DetailsActivity$5;->this$0:Lcom/navdy/client/app/ui/details/DetailsActivity;

    invoke-virtual {v2}, Lcom/navdy/client/app/ui/details/DetailsActivity;->finish()V

    .line 337
    :cond_0
    iget-object v2, p0, Lcom/navdy/client/app/ui/details/DetailsActivity$5;->this$0:Lcom/navdy/client/app/ui/details/DetailsActivity;

    invoke-static {v2}, Lcom/navdy/client/app/ui/details/DetailsActivity;->access$500(Lcom/navdy/client/app/ui/details/DetailsActivity;)Lcom/google/android/gms/maps/GoogleMap;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/navdy/client/app/ui/details/DetailsActivity$5;->this$0:Lcom/navdy/client/app/ui/details/DetailsActivity;

    invoke-static {v2}, Lcom/navdy/client/app/ui/details/DetailsActivity;->access$900(Lcom/navdy/client/app/ui/details/DetailsActivity;)Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 338
    invoke-virtual {p1}, Lcom/navdy/client/app/framework/models/Destination;->hasValidDisplayCoordinates()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 339
    new-instance v1, Landroid/location/Location;

    const-string v2, ""

    invoke-direct {v1, v2}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 340
    .local v1, "location":Landroid/location/Location;
    iget-wide v2, p1, Lcom/navdy/client/app/framework/models/Destination;->displayLat:D

    invoke-virtual {v1, v2, v3}, Landroid/location/Location;->setLatitude(D)V

    .line 341
    iget-wide v2, p1, Lcom/navdy/client/app/framework/models/Destination;->displayLong:D

    invoke-virtual {v1, v2, v3}, Landroid/location/Location;->setLongitude(D)V

    .line 342
    iget-object v2, p0, Lcom/navdy/client/app/ui/details/DetailsActivity$5;->this$0:Lcom/navdy/client/app/ui/details/DetailsActivity;

    invoke-static {v2}, Lcom/navdy/client/app/ui/details/DetailsActivity;->access$900(Lcom/navdy/client/app/ui/details/DetailsActivity;)Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    move-result-object v2

    invoke-virtual {v2, v1, v5, v4}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->moveMap(Landroid/location/Location;FZ)V

    .line 353
    .end local v1    # "location":Landroid/location/Location;
    :cond_1
    :goto_0
    return-void

    .line 343
    :cond_2
    invoke-virtual {p1}, Lcom/navdy/client/app/framework/models/Destination;->hasValidNavCoordinates()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 344
    new-instance v1, Landroid/location/Location;

    const-string v2, ""

    invoke-direct {v1, v2}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 345
    .restart local v1    # "location":Landroid/location/Location;
    iget-wide v2, p1, Lcom/navdy/client/app/framework/models/Destination;->navigationLat:D

    invoke-virtual {v1, v2, v3}, Landroid/location/Location;->setLatitude(D)V

    .line 346
    iget-wide v2, p1, Lcom/navdy/client/app/framework/models/Destination;->navigationLong:D

    invoke-virtual {v1, v2, v3}, Landroid/location/Location;->setLongitude(D)V

    .line 347
    iget-object v2, p0, Lcom/navdy/client/app/ui/details/DetailsActivity$5;->this$0:Lcom/navdy/client/app/ui/details/DetailsActivity;

    invoke-static {v2}, Lcom/navdy/client/app/ui/details/DetailsActivity;->access$900(Lcom/navdy/client/app/ui/details/DetailsActivity;)Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    move-result-object v2

    invoke-virtual {v2, v1, v5, v4}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->moveMap(Landroid/location/Location;FZ)V

    goto :goto_0

    .line 349
    .end local v1    # "location":Landroid/location/Location;
    :cond_3
    iget-object v2, p0, Lcom/navdy/client/app/ui/details/DetailsActivity$5;->this$0:Lcom/navdy/client/app/ui/details/DetailsActivity;

    invoke-static {v2}, Lcom/navdy/client/app/ui/details/DetailsActivity;->access$1000(Lcom/navdy/client/app/ui/details/DetailsActivity;)Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->getSmartStartLocation()Landroid/location/Location;

    move-result-object v0

    .line 350
    .local v0, "lastLocation":Landroid/location/Location;
    iget-object v2, p0, Lcom/navdy/client/app/ui/details/DetailsActivity$5;->this$0:Lcom/navdy/client/app/ui/details/DetailsActivity;

    invoke-static {v2}, Lcom/navdy/client/app/ui/details/DetailsActivity;->access$900(Lcom/navdy/client/app/ui/details/DetailsActivity;)Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    move-result-object v2

    invoke-virtual {v2, v0, v5, v4}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->moveMap(Landroid/location/Location;FZ)V

    goto :goto_0
.end method

.method public onSuccess(Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 2
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;

    .prologue
    .line 317
    iget-object v0, p0, Lcom/navdy/client/app/ui/details/DetailsActivity$5;->this$0:Lcom/navdy/client/app/ui/details/DetailsActivity;

    invoke-static {v0, p1}, Lcom/navdy/client/app/ui/details/DetailsActivity;->access$102(Lcom/navdy/client/app/ui/details/DetailsActivity;Lcom/navdy/client/app/framework/models/Destination;)Lcom/navdy/client/app/framework/models/Destination;

    .line 319
    iget-object v0, p0, Lcom/navdy/client/app/ui/details/DetailsActivity$5;->this$0:Lcom/navdy/client/app/ui/details/DetailsActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/details/DetailsActivity;->access$600(Lcom/navdy/client/app/ui/details/DetailsActivity;)V

    .line 320
    iget-object v0, p0, Lcom/navdy/client/app/ui/details/DetailsActivity$5;->this$0:Lcom/navdy/client/app/ui/details/DetailsActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/details/DetailsActivity;->access$700(Lcom/navdy/client/app/ui/details/DetailsActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {p1}, Lcom/navdy/client/app/framework/models/Destination;->getBadgeAsset()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 321
    iget-object v0, p0, Lcom/navdy/client/app/ui/details/DetailsActivity$5;->this$0:Lcom/navdy/client/app/ui/details/DetailsActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/details/DetailsActivity;->access$800(Lcom/navdy/client/app/ui/details/DetailsActivity;)V

    .line 322
    iget-object v0, p0, Lcom/navdy/client/app/ui/details/DetailsActivity$5;->this$0:Lcom/navdy/client/app/ui/details/DetailsActivity;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/details/DetailsActivity;->hideProgressDialog()V

    .line 323
    return-void
.end method

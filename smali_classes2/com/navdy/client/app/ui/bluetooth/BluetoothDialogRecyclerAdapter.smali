.class Lcom/navdy/client/app/ui/bluetooth/BluetoothDialogRecyclerAdapter;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "BluetoothDialogRecyclerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$Adapter",
        "<",
        "Lcom/navdy/client/app/ui/bluetooth/BluetoothDeviceViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private bluetoothDevices:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private listener:Landroid/view/View$OnClickListener;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 24
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothDialogRecyclerAdapter;->bluetoothDevices:Ljava/util/List;

    .line 25
    return-void
.end method


# virtual methods
.method addDevice(Lcom/navdy/client/debug/devicepicker/Device;)V
    .locals 2
    .param p1, "newDevice"    # Lcom/navdy/client/debug/devicepicker/Device;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothDialogRecyclerAdapter;->bluetoothDevices:Ljava/util/List;

    invoke-virtual {p1}, Lcom/navdy/client/debug/devicepicker/Device;->getPrettyName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 54
    iget-object v0, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothDialogRecyclerAdapter;->bluetoothDevices:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/bluetooth/BluetoothDialogRecyclerAdapter;->notifyItemInserted(I)V

    .line 55
    return-void
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothDialogRecyclerAdapter;->bluetoothDevices:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 60
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/bluetooth/BluetoothDialogRecyclerAdapter;->notifyDataSetChanged()V

    .line 61
    return-void
.end method

.method public contains(Ljava/lang/String;)Z
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothDialogRecyclerAdapter;->bluetoothDevices:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getItemCount()I
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothDialogRecyclerAdapter;->bluetoothDevices:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .prologue
    .line 18
    check-cast p1, Lcom/navdy/client/app/ui/bluetooth/BluetoothDeviceViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/navdy/client/app/ui/bluetooth/BluetoothDialogRecyclerAdapter;->onBindViewHolder(Lcom/navdy/client/app/ui/bluetooth/BluetoothDeviceViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/navdy/client/app/ui/bluetooth/BluetoothDeviceViewHolder;I)V
    .locals 3
    .param p1, "bluetoothDeviceViewHolder"    # Lcom/navdy/client/app/ui/bluetooth/BluetoothDeviceViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 44
    iget-object v1, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothDialogRecyclerAdapter;->bluetoothDevices:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 45
    iget-object v1, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothDialogRecyclerAdapter;->bluetoothDevices:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 46
    .local v0, "device":Ljava/lang/String;
    iget-object v1, p1, Lcom/navdy/client/app/ui/bluetooth/BluetoothDeviceViewHolder;->deviceMainTitle:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 48
    iget-object v1, p1, Lcom/navdy/client/app/ui/bluetooth/BluetoothDeviceViewHolder;->row:Landroid/view/View;

    iget-object v2, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothDialogRecyclerAdapter;->listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 50
    .end local v0    # "device":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p0, p1, p2}, Lcom/navdy/client/app/ui/bluetooth/BluetoothDialogRecyclerAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/navdy/client/app/ui/bluetooth/BluetoothDeviceViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/navdy/client/app/ui/bluetooth/BluetoothDeviceViewHolder;
    .locals 4
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    .line 38
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030059

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 39
    .local v0, "v":Landroid/view/View;
    new-instance v1, Lcom/navdy/client/app/ui/bluetooth/BluetoothDeviceViewHolder;

    invoke-direct {v1, v0}, Lcom/navdy/client/app/ui/bluetooth/BluetoothDeviceViewHolder;-><init>(Landroid/view/View;)V

    return-object v1
.end method

.method setClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0
    .param p1, "customItemClickListener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 28
    iput-object p1, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothDialogRecyclerAdapter;->listener:Landroid/view/View$OnClickListener;

    .line 29
    return-void
.end method

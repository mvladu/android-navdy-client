.class final enum Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$BluetoothState;
.super Ljava/lang/Enum;
.source "BluetoothFramelayoutFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "BluetoothState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$BluetoothState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$BluetoothState;

.field public static final enum DISABLED:Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$BluetoothState;

.field public static final enum ENABLED:Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$BluetoothState;

.field public static final enum NO_BLUETOOTH:Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$BluetoothState;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 69
    new-instance v0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$BluetoothState;

    const-string v1, "NO_BLUETOOTH"

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$BluetoothState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$BluetoothState;->NO_BLUETOOTH:Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$BluetoothState;

    new-instance v0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$BluetoothState;

    const-string v1, "DISABLED"

    invoke-direct {v0, v1, v3}, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$BluetoothState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$BluetoothState;->DISABLED:Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$BluetoothState;

    new-instance v0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$BluetoothState;

    const-string v1, "ENABLED"

    invoke-direct {v0, v1, v4}, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$BluetoothState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$BluetoothState;->ENABLED:Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$BluetoothState;

    .line 68
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$BluetoothState;

    sget-object v1, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$BluetoothState;->NO_BLUETOOTH:Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$BluetoothState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$BluetoothState;->DISABLED:Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$BluetoothState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$BluetoothState;->ENABLED:Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$BluetoothState;

    aput-object v1, v0, v4

    sput-object v0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$BluetoothState;->$VALUES:[Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$BluetoothState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 68
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$BluetoothState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 68
    const-class v0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$BluetoothState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$BluetoothState;

    return-object v0
.end method

.method public static values()[Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$BluetoothState;
    .locals 1

    .prologue
    .line 68
    sget-object v0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$BluetoothState;->$VALUES:[Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$BluetoothState;

    invoke-virtual {v0}, [Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$BluetoothState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$BluetoothState;

    return-object v0
.end method

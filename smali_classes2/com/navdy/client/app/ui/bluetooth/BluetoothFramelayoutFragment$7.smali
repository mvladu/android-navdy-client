.class Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$7;
.super Ljava/lang/Object;
.source "BluetoothFramelayoutFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->selectDialogBasedOnState()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;

    .prologue
    .line 276
    iput-object p1, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$7;->this$0:Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const v3, 0x7f030060

    .line 279
    iget-object v0, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$7;->this$0:Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->isAlive()Z

    move-result v0

    if-nez v0, :cond_0

    .line 295
    :goto_0
    return-void

    .line 282
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$7;->this$0:Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;

    invoke-static {v0}, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->access$400(Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;)Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "entered postDelayed"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 283
    iget-object v0, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$7;->this$0:Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;

    invoke-static {v0}, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->access$500(Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;)Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mDevices size: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$7;->this$0:Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;

    iget-object v2, v2, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->mDevices:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 284
    iget-object v0, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$7;->this$0:Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;

    iget-object v0, v0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->mDevices:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 285
    iget-object v0, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$7;->this$0:Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;

    invoke-static {v0}, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->access$600(Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;)Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "mDevices was empty"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 286
    iget-object v0, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$7;->this$0:Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;

    const v1, 0x7f030062

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->showDialog(I)V

    goto :goto_0

    .line 291
    :cond_1
    iget-object v0, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$7;->this$0:Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;

    invoke-static {v0}, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->access$100(Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;)I

    move-result v0

    if-eq v0, v3, :cond_2

    iget-object v0, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$7;->this$0:Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;

    invoke-static {v0}, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->access$100(Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;)I

    move-result v0

    const v1, 0x7f03005e

    if-eq v0, v1, :cond_2

    .line 292
    iget-object v0, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$7;->this$0:Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;

    invoke-virtual {v0, v3}, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->showDialog(I)V

    .line 293
    :cond_2
    iget-object v0, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$7;->this$0:Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;

    iget-object v1, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$7;->this$0:Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;

    iget-object v1, v1, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->mDevices:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->updateRecyclerView(Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.class public Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;
.super Landroid/view/View;
.source "FluctuatorAnimatorView.java"


# instance fields
.field private alphaAnimator:Landroid/animation/ValueAnimator;

.field private animationDelay:I

.field private animationDuration:I

.field private animationListener:Lcom/navdy/client/app/ui/customviews/DefaultAnimationListener;

.field private animatorSet:Landroid/animation/AnimatorSet;

.field currentCircle:F

.field private endRadius:F

.field private handler:Landroid/os/Handler;

.field private interpolator:Landroid/view/animation/LinearInterpolator;

.field private paint:Landroid/graphics/Paint;

.field private paintColor:I

.field private radiusAnimator:Landroid/animation/ValueAnimator;

.field private startRadius:F

.field public startRunnable:Ljava/lang/Runnable;

.field private strokeWidth:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 53
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 57
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 58
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 61
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31
    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    iput-object v1, p0, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;->interpolator:Landroid/view/animation/LinearInterpolator;

    .line 36
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;->handler:Landroid/os/Handler;

    .line 38
    new-instance v1, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView$1;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView$1;-><init>(Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;)V

    iput-object v1, p0, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;->animationListener:Lcom/navdy/client/app/ui/customviews/DefaultAnimationListener;

    .line 45
    new-instance v1, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView$2;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView$2;-><init>(Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;)V

    iput-object v1, p0, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;->startRunnable:Ljava/lang/Runnable;

    .line 62
    sget-object v1, Lcom/navdy/client/R$styleable;->FluctuatorAnimatorView:[I

    invoke-virtual {p1, p2, v1, p3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 63
    .local v0, "a":Landroid/content/res/TypedArray;
    if-eqz v0, :cond_0

    .line 64
    invoke-virtual {v0, v4, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;->paintColor:I

    .line 65
    invoke-virtual {v0, v5, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;->startRadius:F

    .line 66
    invoke-virtual {v0, v6, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;->endRadius:F

    .line 67
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;->strokeWidth:F

    .line 68
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;->animationDuration:I

    .line 69
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;->animationDelay:I

    .line 70
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 72
    :cond_0
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;->paint:Landroid/graphics/Paint;

    .line 73
    iget-object v1, p0, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;->paint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;->strokeWidth:F

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 74
    iget-object v1, p0, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;->paint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 75
    iget-object v1, p0, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;->paint:Landroid/graphics/Paint;

    invoke-virtual {v1, v5}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 76
    iget-object v1, p0, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;->paint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;->paintColor:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 78
    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v1, p0, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;->animatorSet:Landroid/animation/AnimatorSet;

    .line 79
    iget-object v1, p0, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;->animatorSet:Landroid/animation/AnimatorSet;

    iget v2, p0, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;->animationDuration:I

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 80
    new-array v1, v6, [F

    iget v2, p0, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;->startRadius:F

    aput v2, v1, v4

    iget v2, p0, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;->endRadius:F

    aput v2, v1, v5

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;->radiusAnimator:Landroid/animation/ValueAnimator;

    .line 81
    new-array v1, v6, [F

    fill-array-data v1, :array_0

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;->alphaAnimator:Landroid/animation/ValueAnimator;

    .line 82
    iget-object v1, p0, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;->animatorSet:Landroid/animation/AnimatorSet;

    iget-object v2, p0, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;->interpolator:Landroid/view/animation/LinearInterpolator;

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 83
    iget-object v1, p0, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;->radiusAnimator:Landroid/animation/ValueAnimator;

    new-instance v2, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView$3;

    invoke-direct {v2, p0}, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView$3;-><init>(Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 91
    iget-object v1, p0, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;->alphaAnimator:Landroid/animation/ValueAnimator;

    new-instance v2, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView$4;

    invoke-direct {v2, p0}, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView$4;-><init>(Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 98
    iget-object v1, p0, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;->animatorSet:Landroid/animation/AnimatorSet;

    new-array v2, v6, [Landroid/animation/Animator;

    iget-object v3, p0, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;->radiusAnimator:Landroid/animation/ValueAnimator;

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;->alphaAnimator:Landroid/animation/ValueAnimator;

    aput-object v3, v2, v5

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 99
    return-void

    .line 81
    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method static synthetic access$000(Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;

    .prologue
    .line 21
    iget v0, p0, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;->animationDelay:I

    return v0
.end method

.method static synthetic access$100(Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;)Landroid/animation/AnimatorSet;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;->animatorSet:Landroid/animation/AnimatorSet;

    return-object v0
.end method


# virtual methods
.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 107
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 108
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;->getWidth()I

    move-result v2

    int-to-float v1, v2

    .line 109
    .local v1, "width":F
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;->getHeight()I

    move-result v2

    int-to-float v0, v2

    .line 110
    .local v0, "height":F
    div-float v2, v1, v3

    div-float v3, v0, v3

    iget v4, p0, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;->currentCircle:F

    iget-object v5, p0, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;->paint:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 111
    return-void
.end method

.method public setColor(I)V
    .locals 1
    .param p1, "paintColor"    # I

    .prologue
    .line 102
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;->paint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 103
    return-void
.end method

.method public start()V
    .locals 2

    .prologue
    .line 114
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;->stop()V

    .line 115
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;->animatorSet:Landroid/animation/AnimatorSet;

    iget-object v1, p0, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;->animationListener:Lcom/navdy/client/app/ui/customviews/DefaultAnimationListener;

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 116
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;->animatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 117
    return-void
.end method

.method public stop()V
    .locals 2

    .prologue
    .line 120
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;->startRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 121
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;->animatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;->animatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->removeAllListeners()V

    .line 123
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/FluctuatorAnimatorView;->animatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    .line 125
    :cond_0
    return-void
.end method

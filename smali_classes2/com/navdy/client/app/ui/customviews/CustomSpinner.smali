.class public Lcom/navdy/client/app/ui/customviews/CustomSpinner;
.super Landroid/widget/Spinner;
.source "CustomSpinner.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/ui/customviews/CustomSpinner$OnSpinnerStateChangedListener;
    }
.end annotation


# instance fields
.field spinnerHasBeenOpened:Z

.field spinnerStateListener:Lcom/navdy/client/app/ui/customviews/CustomSpinner$OnSpinnerStateChangedListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 23
    invoke-direct {p0, p1}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;)V

    .line 24
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "mode"    # I

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;I)V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    return-void
.end method


# virtual methods
.method public hasBeenOpened()Z
    .locals 1

    .prologue
    .line 58
    iget-boolean v0, p0, Lcom/navdy/client/app/ui/customviews/CustomSpinner;->spinnerHasBeenOpened:Z

    return v0
.end method

.method public performClick()Z
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/client/app/ui/customviews/CustomSpinner;->spinnerHasBeenOpened:Z

    .line 41
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/CustomSpinner;->spinnerStateListener:Lcom/navdy/client/app/ui/customviews/CustomSpinner$OnSpinnerStateChangedListener;

    if-eqz v0, :cond_0

    .line 42
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/CustomSpinner;->spinnerStateListener:Lcom/navdy/client/app/ui/customviews/CustomSpinner$OnSpinnerStateChangedListener;

    invoke-interface {v0}, Lcom/navdy/client/app/ui/customviews/CustomSpinner$OnSpinnerStateChangedListener;->spinnerOpened()V

    .line 44
    :cond_0
    invoke-super {p0}, Landroid/widget/Spinner;->performClick()Z

    move-result v0

    return v0
.end method

.method public setListener(Lcom/navdy/client/app/ui/customviews/CustomSpinner$OnSpinnerStateChangedListener;)V
    .locals 0
    .param p1, "spinnerStateListener"    # Lcom/navdy/client/app/ui/customviews/CustomSpinner$OnSpinnerStateChangedListener;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/navdy/client/app/ui/customviews/CustomSpinner;->spinnerStateListener:Lcom/navdy/client/app/ui/customviews/CustomSpinner$OnSpinnerStateChangedListener;

    .line 63
    return-void
.end method

.method public spinnerClosed()V
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/client/app/ui/customviews/CustomSpinner;->spinnerHasBeenOpened:Z

    .line 52
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/CustomSpinner;->spinnerStateListener:Lcom/navdy/client/app/ui/customviews/CustomSpinner$OnSpinnerStateChangedListener;

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/CustomSpinner;->spinnerStateListener:Lcom/navdy/client/app/ui/customviews/CustomSpinner$OnSpinnerStateChangedListener;

    invoke-interface {v0}, Lcom/navdy/client/app/ui/customviews/CustomSpinner$OnSpinnerStateChangedListener;->spinnerClosed()V

    .line 55
    :cond_0
    return-void
.end method

.class public Lcom/navdy/client/app/framework/music/RemoteControllerSeekHelper;
.super Ljava/lang/Object;
.source "RemoteControllerSeekHelper.java"

# interfaces
.implements Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$MusicSeekHelper;
.implements Ljava/lang/Runnable;


# static fields
.field private static final handler:Landroid/os/Handler;


# instance fields
.field private final CONSECUTIVE_BREAK:I

.field private final STEP:I

.field private final logger:Lcom/navdy/service/library/log/Logger;

.field private final remoteController:Landroid/media/RemoteController;

.field private signedStep:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    sput-object v0, Lcom/navdy/client/app/framework/music/RemoteControllerSeekHelper;->handler:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>(Landroid/media/RemoteController;)V
    .locals 2
    .param p1, "remoteController"    # Landroid/media/RemoteController;

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/music/RemoteControllerSeekHelper;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/navdy/client/app/framework/music/RemoteControllerSeekHelper;->logger:Lcom/navdy/service/library/log/Logger;

    .line 18
    const/16 v0, 0x2710

    iput v0, p0, Lcom/navdy/client/app/framework/music/RemoteControllerSeekHelper;->STEP:I

    .line 20
    const/16 v0, 0x12c

    iput v0, p0, Lcom/navdy/client/app/framework/music/RemoteControllerSeekHelper;->CONSECUTIVE_BREAK:I

    .line 26
    iput-object p1, p0, Lcom/navdy/client/app/framework/music/RemoteControllerSeekHelper;->remoteController:Landroid/media/RemoteController;

    .line 27
    return-void
.end method


# virtual methods
.method public executeMusicSeekAction(Lcom/navdy/service/library/events/audio/MusicEvent$Action;)V
    .locals 3
    .param p1, "action"    # Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/navdy/client/app/framework/music/RemoteControllerSeekHelper;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Executing music action "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 32
    sget-object v0, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->MUSIC_ACTION_FAST_FORWARD_STOP:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    invoke-virtual {v0, p1}, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->MUSIC_ACTION_REWIND_STOP:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    .line 33
    invoke-virtual {v0, p1}, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 34
    :cond_0
    sget-object v0, Lcom/navdy/client/app/framework/music/RemoteControllerSeekHelper;->handler:Landroid/os/Handler;

    invoke-virtual {v0, p0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 39
    :goto_0
    return-void

    .line 36
    :cond_1
    sget-object v0, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->MUSIC_ACTION_FAST_FORWARD_START:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    invoke-virtual {v0, p1}, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v0, 0x2710

    :goto_1
    iput v0, p0, Lcom/navdy/client/app/framework/music/RemoteControllerSeekHelper;->signedStep:I

    .line 37
    sget-object v0, Lcom/navdy/client/app/framework/music/RemoteControllerSeekHelper;->handler:Landroid/os/Handler;

    invoke-virtual {v0, p0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 36
    :cond_2
    const/16 v0, -0x2710

    goto :goto_1
.end method

.method public run()V
    .locals 8

    .prologue
    .line 43
    iget-object v4, p0, Lcom/navdy/client/app/framework/music/RemoteControllerSeekHelper;->remoteController:Landroid/media/RemoteController;

    invoke-virtual {v4}, Landroid/media/RemoteController;->getEstimatedMediaPosition()J

    move-result-wide v4

    long-to-int v3, v4

    .line 44
    .local v3, "oldPosition":I
    if-gez v3, :cond_0

    .line 45
    iget-object v4, p0, Lcom/navdy/client/app/framework/music/RemoteControllerSeekHelper;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "Music position unavailable, can\'t seek"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 66
    :goto_0
    return-void

    .line 48
    :cond_0
    iget v4, p0, Lcom/navdy/client/app/framework/music/RemoteControllerSeekHelper;->signedStep:I

    add-int v2, v3, v4

    .line 50
    .local v2, "newPosition":I
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->getInstance()Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;

    move-result-object v1

    .line 51
    .local v1, "musicServiceHandler":Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;
    invoke-virtual {v1}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->isMusicPlayerActive()Z

    move-result v4

    if-nez v4, :cond_1

    .line 52
    iget-object v4, p0, Lcom/navdy/client/app/framework/music/RemoteControllerSeekHelper;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "Tried to seek with no current track"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 56
    :cond_1
    invoke-virtual {v1}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->getCurrentMediaTrackInfo()Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    move-result-object v0

    .line 57
    .local v0, "currentTrack":Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    if-gez v2, :cond_3

    .line 58
    const/4 v2, 0x0

    .line 63
    :cond_2
    :goto_1
    iget-object v4, p0, Lcom/navdy/client/app/framework/music/RemoteControllerSeekHelper;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Setting new position: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " -> "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 64
    iget-object v4, p0, Lcom/navdy/client/app/framework/music/RemoteControllerSeekHelper;->remoteController:Landroid/media/RemoteController;

    int-to-long v6, v2

    invoke-virtual {v4, v6, v7}, Landroid/media/RemoteController;->seekTo(J)Z

    .line 65
    sget-object v4, Lcom/navdy/client/app/framework/music/RemoteControllerSeekHelper;->handler:Landroid/os/Handler;

    const-wide/16 v6, 0x12c

    invoke-virtual {v4, p0, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 59
    :cond_3
    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->duration:Ljava/lang/Integer;

    if-eqz v4, :cond_2

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->duration:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-lez v4, :cond_2

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->duration:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-le v2, v4, :cond_2

    .line 60
    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->duration:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v2

    goto :goto_1
.end method

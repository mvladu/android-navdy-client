.class public Lcom/navdy/client/app/framework/receiver/SmsListener;
.super Landroid/content/BroadcastReceiver;
.source "SmsListener.java"


# static fields
.field private static final ACTION_SMS_RECEIVED:Ljava/lang/String; = "android.provider.Telephony.SMS_RECEIVED"

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 31
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/receiver/SmsListener;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/receiver/SmsListener;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method protected getMessagesFromIntent(Landroid/content/Intent;)[Landroid/telephony/SmsMessage;
    .locals 6
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 91
    const-string v5, "pdus"

    invoke-virtual {p1, v5}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v5

    check-cast v5, [Ljava/lang/Object;

    move-object v1, v5

    check-cast v1, [Ljava/lang/Object;

    .line 92
    .local v1, "messages":[Ljava/lang/Object;
    array-length v4, v1

    .line 93
    .local v4, "pduCount":I
    new-array v2, v4, [Landroid/telephony/SmsMessage;

    .line 94
    .local v2, "msgs":[Landroid/telephony/SmsMessage;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v4, :cond_0

    .line 95
    aget-object v5, v1, v0

    check-cast v5, [B

    move-object v3, v5

    check-cast v3, [B

    .line 96
    .local v3, "pdu":[B
    invoke-static {v3}, Landroid/telephony/SmsMessage;->createFromPdu([B)Landroid/telephony/SmsMessage;

    move-result-object v5

    aput-object v5, v2, v0

    .line 94
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 98
    .end local v3    # "pdu":[B
    :cond_0
    return-object v2
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 20
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 37
    const-string v15, "com.navdy.sms"

    invoke-static {v15}, Lcom/navdy/client/app/ui/glances/GlanceUtils;->isThisGlanceEnabledAsWellAsGlobal(Ljava/lang/String;)Z

    move-result v15

    if-nez v15, :cond_1

    .line 88
    :cond_0
    return-void

    .line 41
    :cond_1
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v15

    invoke-virtual {v15}, Lcom/navdy/client/app/framework/AppInstance;->getRemoteDevice()Lcom/navdy/service/library/device/RemoteDevice;

    move-result-object v14

    .line 43
    .local v14, "remoteDevice":Lcom/navdy/service/library/device/RemoteDevice;
    if-eqz v14, :cond_0

    invoke-virtual {v14}, Lcom/navdy/service/library/device/RemoteDevice;->getDeviceInfo()Lcom/navdy/service/library/events/DeviceInfo;

    move-result-object v15

    if-eqz v15, :cond_0

    invoke-virtual {v14}, Lcom/navdy/service/library/device/RemoteDevice;->getDeviceInfo()Lcom/navdy/service/library/events/DeviceInfo;

    move-result-object v15

    iget-object v15, v15, Lcom/navdy/service/library/events/DeviceInfo;->protocolVersion:Ljava/lang/String;

    invoke-static {v15}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_0

    .line 47
    invoke-virtual {v14}, Lcom/navdy/service/library/device/RemoteDevice;->getDeviceInfo()Lcom/navdy/service/library/events/DeviceInfo;

    move-result-object v15

    iget-object v13, v15, Lcom/navdy/service/library/events/DeviceInfo;->protocolVersion:Ljava/lang/String;

    .line 49
    .local v13, "protocol":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    .line 50
    .local v2, "action":Ljava/lang/String;
    const-string v15, "android.provider.Telephony.SMS_RECEIVED"

    invoke-virtual {v2, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_0

    .line 51
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/framework/receiver/SmsListener;->getMessagesFromIntent(Landroid/content/Intent;)[Landroid/telephony/SmsMessage;

    move-result-object v11

    .line 52
    .local v11, "msgs":[Landroid/telephony/SmsMessage;
    if-eqz v11, :cond_0

    .line 53
    sget-object v15, Lcom/navdy/client/app/framework/receiver/SmsListener;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v16, "[navdyinfo-sms] sms message received"

    invoke-virtual/range {v15 .. v16}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 54
    array-length v0, v11

    move/from16 v16, v0

    const/4 v15, 0x0

    :goto_0
    move/from16 v0, v16

    if-ge v15, v0, :cond_0

    aget-object v10, v11, v15

    .line 55
    .local v10, "msg":Landroid/telephony/SmsMessage;
    invoke-virtual {v10}, Landroid/telephony/SmsMessage;->getOriginatingAddress()Ljava/lang/String;

    move-result-object v4

    .line 56
    .local v4, "address":Ljava/lang/String;
    move-object/from16 v0, p1

    invoke-static {v0, v4}, Lcom/navdy/client/debug/util/Contacts;->lookupNameFromPhoneNumber(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 57
    .local v6, "contactName":Ljava/lang/String;
    invoke-virtual {v10}, Landroid/telephony/SmsMessage;->getMessageBody()Ljava/lang/String;

    move-result-object v5

    .line 59
    .local v5, "body":Ljava/lang/String;
    invoke-static {v13}, Lcom/navdy/client/app/framework/glances/GlancesHelper;->isHudVersionCompatible(Ljava/lang/String;)Z

    move-result v17

    if-nez v17, :cond_2

    .line 61
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-static {v6, v4, v5, v0}, Lcom/navdy/client/debug/util/NotificationBuilder;->buildSmsNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/navdy/service/library/events/notification/NotificationEvent;

    move-result-object v12

    .line 62
    .local v12, "notification":Lcom/navdy/service/library/events/notification/NotificationEvent;
    invoke-virtual {v14, v12}, Lcom/navdy/service/library/device/RemoteDevice;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 54
    .end local v12    # "notification":Lcom/navdy/service/library/events/notification/NotificationEvent;
    :goto_1
    add-int/lit8 v15, v15, 0x1

    goto :goto_0

    .line 64
    :cond_2
    invoke-static {}, Lcom/navdy/client/app/framework/glances/GlancesHelper;->getId()Ljava/lang/String;

    move-result-object v8

    .line 66
    .local v8, "id":Ljava/lang/String;
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 67
    .local v7, "data":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/KeyValue;>;"
    new-instance v17, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v18, Lcom/navdy/service/library/events/glances/MessageConstants;->MESSAGE_FROM:Lcom/navdy/service/library/events/glances/MessageConstants;

    invoke-virtual/range {v18 .. v18}, Lcom/navdy/service/library/events/glances/MessageConstants;->name()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-direct {v0, v1, v6}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 68
    new-instance v17, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v18, Lcom/navdy/service/library/events/glances/MessageConstants;->MESSAGE_FROM_NUMBER:Lcom/navdy/service/library/events/glances/MessageConstants;

    invoke-virtual/range {v18 .. v18}, Lcom/navdy/service/library/events/glances/MessageConstants;->name()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-direct {v0, v1, v4}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 69
    new-instance v17, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v18, Lcom/navdy/service/library/events/glances/MessageConstants;->MESSAGE_IS_SMS:Lcom/navdy/service/library/events/glances/MessageConstants;

    invoke-virtual/range {v18 .. v18}, Lcom/navdy/service/library/events/glances/MessageConstants;->name()Ljava/lang/String;

    move-result-object v18

    const-string v19, ""

    invoke-direct/range {v17 .. v19}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 70
    new-instance v17, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v18, Lcom/navdy/service/library/events/glances/MessageConstants;->MESSAGE_BODY:Lcom/navdy/service/library/events/glances/MessageConstants;

    invoke-virtual/range {v18 .. v18}, Lcom/navdy/service/library/events/glances/MessageConstants;->name()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-direct {v0, v1, v5}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 72
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 73
    .local v3, "actions":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;>;"
    sget-object v17, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;->REPLY:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;

    move-object/from16 v0, v17

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 75
    new-instance v17, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    invoke-direct/range {v17 .. v17}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;-><init>()V

    sget-object v18, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;->GLANCE_TYPE_MESSAGE:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    .line 76
    invoke-virtual/range {v17 .. v18}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceType(Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v17

    const-string v18, "com.navdy.sms"

    .line 77
    invoke-virtual/range {v17 .. v18}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->provider(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v17

    .line 78
    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->id(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v17

    .line 79
    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceData(Ljava/util/List;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v17

    .line 80
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->postTime(Ljava/lang/Long;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v17

    .line 81
    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->actions(Ljava/util/List;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->build()Lcom/navdy/service/library/events/glances/GlanceEvent;

    move-result-object v9

    .line 83
    .local v9, "messageEvent":Lcom/navdy/service/library/events/glances/GlanceEvent;
    invoke-static {v9}, Lcom/navdy/client/app/framework/glances/GlancesHelper;->sendEvent(Lcom/navdy/service/library/events/glances/GlanceEvent;)V

    goto/16 :goto_1
.end method

.class public Lcom/navdy/client/app/framework/navigation/HereRouteManager;
.super Ljava/lang/Object;
.source "HereRouteManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/framework/navigation/HereRouteManager$State;,
        Lcom/navdy/client/app/framework/navigation/HereRouteManager$OnHereRouteManagerInitialized;,
        Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;,
        Lcom/navdy/client/app/framework/navigation/HereRouteManager$Listener;,
        Lcom/navdy/client/app/framework/navigation/HereRouteManager$RouteHandle;
    }
.end annotation


# static fields
.field private static final VERBOSE:Z

.field private static final initListenersLock:Ljava/lang/Object;

.field private static final instance:Lcom/navdy/client/app/framework/navigation/HereRouteManager;

.field private static final logger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private final initListeners:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/navdy/client/app/framework/navigation/HereRouteManager$OnHereRouteManagerInitialized;",
            ">;"
        }
    .end annotation
.end field

.field private final navdyLocationManager:Lcom/navdy/client/app/framework/location/NavdyLocationManager;

.field private volatile state:Lcom/navdy/client/app/framework/navigation/HereRouteManager$State;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 35
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/navigation/HereRouteManager;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/navigation/HereRouteManager;->logger:Lcom/navdy/service/library/log/Logger;

    .line 38
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/navdy/client/app/framework/navigation/HereRouteManager;->initListenersLock:Ljava/lang/Object;

    .line 40
    new-instance v0, Lcom/navdy/client/app/framework/navigation/HereRouteManager;

    invoke-direct {v0}, Lcom/navdy/client/app/framework/navigation/HereRouteManager;-><init>()V

    sput-object v0, Lcom/navdy/client/app/framework/navigation/HereRouteManager;->instance:Lcom/navdy/client/app/framework/navigation/HereRouteManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/framework/navigation/HereRouteManager;->initListeners:Ljava/util/Queue;

    .line 53
    invoke-static {}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->getInstance()Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/framework/navigation/HereRouteManager;->navdyLocationManager:Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    .line 55
    sget-object v0, Lcom/navdy/client/app/framework/navigation/HereRouteManager$State;->INITIALIZING:Lcom/navdy/client/app/framework/navigation/HereRouteManager$State;

    iput-object v0, p0, Lcom/navdy/client/app/framework/navigation/HereRouteManager;->state:Lcom/navdy/client/app/framework/navigation/HereRouteManager$State;

    .line 56
    invoke-direct {p0}, Lcom/navdy/client/app/framework/navigation/HereRouteManager;->initialize()V

    .line 57
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/framework/navigation/HereRouteManager;)Lcom/navdy/client/app/framework/location/NavdyLocationManager;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/navigation/HereRouteManager;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/HereRouteManager;->navdyLocationManager:Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/client/app/framework/navigation/HereRouteManager;Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/common/GeoCoordinate;Lcom/navdy/client/app/framework/navigation/HereRouteManager$Listener;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/navigation/HereRouteManager;
    .param p1, "x1"    # Lcom/here/android/mpa/common/GeoCoordinate;
    .param p2, "x2"    # Lcom/here/android/mpa/common/GeoCoordinate;
    .param p3, "x3"    # Lcom/navdy/client/app/framework/navigation/HereRouteManager$Listener;

    .prologue
    .line 33
    invoke-direct {p0, p1, p2, p3}, Lcom/navdy/client/app/framework/navigation/HereRouteManager;->calculateRouteWithCoords(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/common/GeoCoordinate;Lcom/navdy/client/app/framework/navigation/HereRouteManager$Listener;)V

    return-void
.end method

.method static synthetic access$300()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/navdy/client/app/framework/navigation/HereRouteManager;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$400()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/navdy/client/app/framework/navigation/HereRouteManager;->initListenersLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$502(Lcom/navdy/client/app/framework/navigation/HereRouteManager;Lcom/navdy/client/app/framework/navigation/HereRouteManager$State;)Lcom/navdy/client/app/framework/navigation/HereRouteManager$State;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/navigation/HereRouteManager;
    .param p1, "x1"    # Lcom/navdy/client/app/framework/navigation/HereRouteManager$State;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/navdy/client/app/framework/navigation/HereRouteManager;->state:Lcom/navdy/client/app/framework/navigation/HereRouteManager$State;

    return-object p1
.end method

.method static synthetic access$600(Lcom/navdy/client/app/framework/navigation/HereRouteManager;)Ljava/util/Queue;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/navigation/HereRouteManager;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/HereRouteManager;->initListeners:Ljava/util/Queue;

    return-object v0
.end method

.method private calculateRouteWithCoords(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/common/GeoCoordinate;Lcom/navdy/client/app/framework/navigation/HereRouteManager$Listener;)V
    .locals 5
    .param p1, "navdyLocation"    # Lcom/here/android/mpa/common/GeoCoordinate;
    .param p2, "destination"    # Lcom/here/android/mpa/common/GeoCoordinate;
    .param p3, "listener"    # Lcom/navdy/client/app/framework/navigation/HereRouteManager$Listener;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 86
    new-instance v1, Lcom/here/android/mpa/routing/RoutePlan;

    invoke-direct {v1}, Lcom/here/android/mpa/routing/RoutePlan;-><init>()V

    .line 91
    .local v1, "routePlan":Lcom/here/android/mpa/routing/RoutePlan;
    new-instance v3, Lcom/here/android/mpa/routing/RouteWaypoint;

    invoke-direct {v3, p1}, Lcom/here/android/mpa/routing/RouteWaypoint;-><init>(Lcom/here/android/mpa/common/GeoCoordinate;)V

    invoke-virtual {v1, v3}, Lcom/here/android/mpa/routing/RoutePlan;->addWaypoint(Lcom/here/android/mpa/routing/RouteWaypoint;)Lcom/here/android/mpa/routing/RoutePlan;

    .line 92
    new-instance v3, Lcom/here/android/mpa/routing/RouteWaypoint;

    invoke-direct {v3, p2}, Lcom/here/android/mpa/routing/RouteWaypoint;-><init>(Lcom/here/android/mpa/common/GeoCoordinate;)V

    invoke-virtual {v1, v3}, Lcom/here/android/mpa/routing/RoutePlan;->addWaypoint(Lcom/here/android/mpa/routing/RouteWaypoint;)Lcom/here/android/mpa/routing/RoutePlan;

    .line 94
    new-instance v0, Lcom/here/android/mpa/routing/RouteOptions;

    invoke-direct {v0}, Lcom/here/android/mpa/routing/RouteOptions;-><init>()V

    .line 95
    .local v0, "routeOptions":Lcom/here/android/mpa/routing/RouteOptions;
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/here/android/mpa/routing/RouteOptions;->setRouteCount(I)Lcom/here/android/mpa/routing/RouteOptions;

    .line 96
    sget-object v3, Lcom/here/android/mpa/routing/RouteOptions$TransportMode;->CAR:Lcom/here/android/mpa/routing/RouteOptions$TransportMode;

    invoke-virtual {v0, v3}, Lcom/here/android/mpa/routing/RouteOptions;->setTransportMode(Lcom/here/android/mpa/routing/RouteOptions$TransportMode;)Lcom/here/android/mpa/routing/RouteOptions;

    .line 97
    sget-object v3, Lcom/here/android/mpa/routing/RouteOptions$Type;->FASTEST:Lcom/here/android/mpa/routing/RouteOptions$Type;

    invoke-virtual {v0, v3}, Lcom/here/android/mpa/routing/RouteOptions;->setRouteType(Lcom/here/android/mpa/routing/RouteOptions$Type;)Lcom/here/android/mpa/routing/RouteOptions;

    .line 99
    invoke-virtual {v1, v0}, Lcom/here/android/mpa/routing/RoutePlan;->setRouteOptions(Lcom/here/android/mpa/routing/RouteOptions;)Lcom/here/android/mpa/routing/RoutePlan;

    .line 101
    new-instance v2, Lcom/here/android/mpa/routing/CoreRouter;

    invoke-direct {v2}, Lcom/here/android/mpa/routing/CoreRouter;-><init>()V

    .line 102
    .local v2, "router":Lcom/here/android/mpa/routing/CoreRouter;
    new-instance v3, Lcom/navdy/client/app/framework/navigation/HereRouteManager$RouteHandle;

    const/4 v4, 0x0

    invoke-direct {v3, v2, v4}, Lcom/navdy/client/app/framework/navigation/HereRouteManager$RouteHandle;-><init>(Lcom/here/android/mpa/routing/CoreRouter;Lcom/navdy/client/app/framework/navigation/HereRouteManager$1;)V

    invoke-interface {p3, v3}, Lcom/navdy/client/app/framework/navigation/HereRouteManager$Listener;->onPreCalculation(Lcom/navdy/client/app/framework/navigation/HereRouteManager$RouteHandle;)V

    .line 104
    new-instance v3, Lcom/navdy/client/app/framework/navigation/HereRouteManager$2;

    invoke-direct {v3, p0, p3}, Lcom/navdy/client/app/framework/navigation/HereRouteManager$2;-><init>(Lcom/navdy/client/app/framework/navigation/HereRouteManager;Lcom/navdy/client/app/framework/navigation/HereRouteManager$Listener;)V

    invoke-virtual {v2, v1, v3}, Lcom/here/android/mpa/routing/CoreRouter;->calculateRoute(Lcom/here/android/mpa/routing/RoutePlan;Lcom/here/android/mpa/routing/Router$Listener;)V

    .line 127
    return-void
.end method

.method public static getInstance()Lcom/navdy/client/app/framework/navigation/HereRouteManager;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/navdy/client/app/framework/navigation/HereRouteManager;->instance:Lcom/navdy/client/app/framework/navigation/HereRouteManager;

    return-object v0
.end method

.method private initialize()V
    .locals 2

    .prologue
    .line 130
    invoke-static {}, Lcom/navdy/client/app/framework/map/HereMapsManager;->getInstance()Lcom/navdy/client/app/framework/map/HereMapsManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/framework/navigation/HereRouteManager$3;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/framework/navigation/HereRouteManager$3;-><init>(Lcom/navdy/client/app/framework/navigation/HereRouteManager;)V

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/framework/map/HereMapsManager;->addOnInitializedListener(Lcom/here/android/mpa/common/OnEngineInitListener;)V

    .line 152
    return-void
.end method

.method private whenInitialized(Lcom/navdy/client/app/framework/navigation/HereRouteManager$OnHereRouteManagerInitialized;)V
    .locals 2
    .param p1, "callback"    # Lcom/navdy/client/app/framework/navigation/HereRouteManager$OnHereRouteManagerInitialized;

    .prologue
    .line 155
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/HereRouteManager;->state:Lcom/navdy/client/app/framework/navigation/HereRouteManager$State;

    sget-object v1, Lcom/navdy/client/app/framework/navigation/HereRouteManager$State;->INITIALIZING:Lcom/navdy/client/app/framework/navigation/HereRouteManager$State;

    if-ne v0, v1, :cond_1

    .line 156
    sget-object v1, Lcom/navdy/client/app/framework/navigation/HereRouteManager;->initListenersLock:Ljava/lang/Object;

    monitor-enter v1

    .line 157
    :try_start_0
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/HereRouteManager;->initListeners:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 158
    monitor-exit v1

    .line 162
    :cond_0
    :goto_0
    return-void

    .line 158
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 159
    :cond_1
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/HereRouteManager;->state:Lcom/navdy/client/app/framework/navigation/HereRouteManager$State;

    sget-object v1, Lcom/navdy/client/app/framework/navigation/HereRouteManager$State;->INITIALIZED:Lcom/navdy/client/app/framework/navigation/HereRouteManager$State;

    if-ne v0, v1, :cond_0

    .line 160
    invoke-interface {p1}, Lcom/navdy/client/app/framework/navigation/HereRouteManager$OnHereRouteManagerInitialized;->onInit()V

    goto :goto_0
.end method


# virtual methods
.method public calculateRoute(DDLcom/navdy/client/app/framework/navigation/HereRouteManager$Listener;)V
    .locals 7
    .param p1, "latitude"    # D
    .param p3, "longitude"    # D
    .param p5, "listener"    # Lcom/navdy/client/app/framework/navigation/HereRouteManager$Listener;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 60
    new-instance v0, Lcom/navdy/client/app/framework/navigation/HereRouteManager$1;

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/navdy/client/app/framework/navigation/HereRouteManager$1;-><init>(Lcom/navdy/client/app/framework/navigation/HereRouteManager;DDLcom/navdy/client/app/framework/navigation/HereRouteManager$Listener;)V

    invoke-direct {p0, v0}, Lcom/navdy/client/app/framework/navigation/HereRouteManager;->whenInitialized(Lcom/navdy/client/app/framework/navigation/HereRouteManager$OnHereRouteManagerInitialized;)V

    .line 83
    return-void
.end method

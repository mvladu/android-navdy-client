.class final Lcom/navdy/client/app/framework/util/MusicUtils$1;
.super Ljava/util/HashMap;
.source "MusicUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/util/MusicUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap",
        "<",
        "Lcom/navdy/service/library/events/audio/MusicEvent$Action;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    .line 88
    sget-object v0, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->MUSIC_ACTION_PLAY:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    const/16 v1, 0x7e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/navdy/client/app/framework/util/MusicUtils$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    sget-object v0, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->MUSIC_ACTION_PAUSE:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    const/16 v1, 0x7f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/navdy/client/app/framework/util/MusicUtils$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    sget-object v0, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->MUSIC_ACTION_PREVIOUS:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    const/16 v1, 0x58

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/navdy/client/app/framework/util/MusicUtils$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    sget-object v0, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->MUSIC_ACTION_NEXT:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    const/16 v1, 0x57

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/navdy/client/app/framework/util/MusicUtils$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    return-void
.end method

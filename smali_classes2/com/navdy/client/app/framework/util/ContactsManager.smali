.class public Lcom/navdy/client/app/framework/util/ContactsManager;
.super Ljava/lang/Object;
.source "ContactsManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/framework/util/ContactsManager$Attributes;
    }
.end annotation


# static fields
.field private static final VERBOSE:Z

.field private static final addressProjection:[Ljava/lang/String;

.field private static final emptyAddressList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/client/app/framework/models/Address;",
            ">;"
        }
    .end annotation
.end field

.field private static final emptyContactList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/client/app/framework/models/ContactModel;",
            ">;"
        }
    .end annotation
.end field

.field private static final genericProjection:[Ljava/lang/String;

.field private static final instance:Lcom/navdy/client/app/framework/util/ContactsManager;

.field private static final logger:Lcom/navdy/service/library/log/Logger;

.field private static final phoneProjection:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 80
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/util/ContactsManager;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/util/ContactsManager;->logger:Lcom/navdy/service/library/log/Logger;

    .line 82
    new-instance v0, Lcom/navdy/client/app/framework/util/ContactsManager;

    invoke-direct {v0}, Lcom/navdy/client/app/framework/util/ContactsManager;-><init>()V

    sput-object v0, Lcom/navdy/client/app/framework/util/ContactsManager;->instance:Lcom/navdy/client/app/framework/util/ContactsManager;

    .line 83
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/navdy/client/app/framework/util/ContactsManager;->emptyContactList:Ljava/util/ArrayList;

    .line 84
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/navdy/client/app/framework/util/ContactsManager;->emptyAddressList:Ljava/util/ArrayList;

    .line 86
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "contact_id"

    aput-object v1, v0, v3

    const-string v1, "lookup"

    aput-object v1, v0, v4

    const-string v1, "display_name"

    aput-object v1, v0, v5

    const-string v1, "has_phone_number"

    aput-object v1, v0, v6

    const-string v1, "is_primary"

    aput-object v1, v0, v7

    sput-object v0, Lcom/navdy/client/app/framework/util/ContactsManager;->genericProjection:[Ljava/lang/String;

    .line 93
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "contact_id"

    aput-object v1, v0, v3

    const-string v1, "lookup"

    aput-object v1, v0, v4

    const-string v1, "data1"

    aput-object v1, v0, v5

    const-string v1, "data2"

    aput-object v1, v0, v6

    const-string v1, "display_name"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "starred"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "data3"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "is_primary"

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/client/app/framework/util/ContactsManager;->phoneProjection:[Ljava/lang/String;

    .line 104
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "contact_id"

    aput-object v1, v0, v3

    const-string v1, "lookup"

    aput-object v1, v0, v4

    const-string v1, "data5"

    aput-object v1, v0, v5

    const-string v1, "data4"

    aput-object v1, v0, v6

    const-string v1, "data7"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "data8"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "data9"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "data10"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "data2"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "display_name"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "data3"

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/client/app/framework/util/ContactsManager;->addressProjection:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static getAddress(Landroid/database/Cursor;)Lcom/navdy/client/app/framework/models/Address;
    .locals 10
    .param p0, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 518
    const-string v0, "data5"

    invoke-static {p0, v0}, Lcom/navdy/client/app/providers/DbUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 519
    .local v1, "pobox":Ljava/lang/String;
    const-string v0, "data4"

    invoke-static {p0, v0}, Lcom/navdy/client/app/providers/DbUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 520
    .local v2, "street":Ljava/lang/String;
    const-string v0, "data7"

    invoke-static {p0, v0}, Lcom/navdy/client/app/providers/DbUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 521
    .local v3, "city":Ljava/lang/String;
    const-string v0, "data8"

    invoke-static {p0, v0}, Lcom/navdy/client/app/providers/DbUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 522
    .local v4, "state":Ljava/lang/String;
    const-string v0, "data9"

    invoke-static {p0, v0}, Lcom/navdy/client/app/providers/DbUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 523
    .local v5, "postcode":Ljava/lang/String;
    const-string v0, "data10"

    invoke-static {p0, v0}, Lcom/navdy/client/app/providers/DbUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 524
    .local v6, "country":Ljava/lang/String;
    const-string v0, "data2"

    invoke-static {p0, v0}, Lcom/navdy/client/app/providers/DbUtils;->getInt(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v7

    .line 525
    .local v7, "type":I
    const-string v0, "display_name"

    invoke-static {p0, v0}, Lcom/navdy/client/app/providers/DbUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 526
    .local v8, "name":Ljava/lang/String;
    const-string v0, "data3"

    invoke-static {p0, v0}, Lcom/navdy/client/app/providers/DbUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 528
    .local v9, "label":Ljava/lang/String;
    new-instance v0, Lcom/navdy/client/app/framework/models/Address;

    invoke-direct/range {v0 .. v9}, Lcom/navdy/client/app/framework/models/Address;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private static getAddressCursor(Landroid/content/ContentResolver;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8
    .param p0, "contentResolver"    # Landroid/content/ContentResolver;
    .param p1, "startsWith"    # Ljava/lang/String;

    .prologue
    .line 610
    const/4 v3, 0x0

    .line 611
    .local v3, "selection":Ljava/lang/String;
    const/4 v4, 0x0

    .line 613
    .local v4, "selectionArgs":[Ljava/lang/String;
    invoke-static {p1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 614
    const-string v3, "display_name LIKE ?"

    .line 615
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    .end local v4    # "selectionArgs":[Ljava/lang/String;
    const/4 v0, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 617
    .restart local v4    # "selectionArgs":[Ljava/lang/String;
    :cond_0
    const/4 v6, 0x0

    .line 619
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    sget-object v1, Landroid/provider/ContactsContract$CommonDataKinds$StructuredPostal;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/navdy/client/app/framework/util/ContactsManager;->addressProjection:[Ljava/lang/String;

    const-string v5, "sort_key ASC"

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 628
    :goto_0
    return-object v6

    .line 625
    :catch_0
    move-exception v7

    .line 626
    .local v7, "e":Ljava/lang/Exception;
    sget-object v0, Lcom/navdy/client/app/framework/util/ContactsManager;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Something went wrong while trying to get an address cursor"

    invoke-virtual {v0, v1, v7}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static getAddressFromLookupKey(Landroid/content/ContentResolver;Ljava/lang/String;J)Ljava/util/List;
    .locals 14
    .param p0, "contentResolver"    # Landroid/content/ContentResolver;
    .param p1, "lookupKey"    # Ljava/lang/String;
    .param p2, "lastKnownContactId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "Ljava/lang/String;",
            "J)",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/Address;",
            ">;"
        }
    .end annotation

    .prologue
    .line 479
    invoke-static/range {p0 .. p3}, Lcom/navdy/client/app/framework/util/ContactsManager;->getContactIdFromLookupKey(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v8

    .line 480
    .local v8, "contactId":J
    const-wide/16 v0, 0x0

    cmp-long v0, v8, v0

    if-gez v0, :cond_0

    .line 486
    sget-object v7, Lcom/navdy/client/app/framework/util/ContactsManager;->emptyAddressList:Ljava/util/ArrayList;

    .line 511
    :goto_0
    return-object v7

    .line 489
    :cond_0
    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    .line 490
    .local v10, "contactIdString":Ljava/lang/String;
    const-string v3, "contact_id = ? AND mimetype = ?"

    .line 491
    .local v3, "selection":Ljava/lang/String;
    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object v10, v4, v0

    const/4 v0, 0x1

    const-string v1, "vnd.android.cursor.item/postal-address_v2"

    aput-object v1, v4, v0

    .line 492
    .local v4, "selectionArgs":[Ljava/lang/String;
    const/4 v11, 0x0

    .line 494
    .local v11, "cursor":Landroid/database/Cursor;
    :try_start_0
    sget-object v1, Landroid/provider/ContactsContract$CommonDataKinds$StructuredPostal;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/navdy/client/app/framework/util/ContactsManager;->addressProjection:[Ljava/lang/String;

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 500
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 501
    .local v7, "addresses":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/Address;>;"
    :goto_1
    if-eqz v11, :cond_1

    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 502
    invoke-static {v11}, Lcom/navdy/client/app/framework/util/ContactsManager;->getAddress(Landroid/database/Cursor;)Lcom/navdy/client/app/framework/models/Address;

    move-result-object v6

    .line 503
    .local v6, "address":Lcom/navdy/client/app/framework/models/Address;
    invoke-interface {v7, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 506
    .end local v6    # "address":Lcom/navdy/client/app/framework/models/Address;
    .end local v7    # "addresses":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/Address;>;"
    :catch_0
    move-exception v12

    .line 507
    .local v12, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v0, Lcom/navdy/client/app/framework/util/ContactsManager;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Something went wrong while trying to get an address cursor"

    invoke-virtual {v0, v1, v12}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 509
    invoke-static {v11}, Lcom/navdy/client/app/framework/util/SystemUtils;->closeCursor(Landroid/database/Cursor;)V

    .line 511
    sget-object v7, Lcom/navdy/client/app/framework/util/ContactsManager;->emptyAddressList:Ljava/util/ArrayList;

    goto :goto_0

    .line 509
    .end local v12    # "e":Ljava/lang/Exception;
    .restart local v7    # "addresses":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/Address;>;"
    :cond_1
    invoke-static {v11}, Lcom/navdy/client/app/framework/util/SystemUtils;->closeCursor(Landroid/database/Cursor;)V

    goto :goto_0

    .end local v7    # "addresses":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/Address;>;"
    :catchall_0
    move-exception v0

    invoke-static {v11}, Lcom/navdy/client/app/framework/util/SystemUtils;->closeCursor(Landroid/database/Cursor;)V

    throw v0
.end method

.method private static getAddressesFromContactsHelper(Landroid/database/Cursor;)Ljava/util/List;
    .locals 8
    .param p0, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/ContactModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 356
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 358
    .local v2, "contacts":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/navdy/client/app/framework/models/ContactModel;>;"
    if-nez p0, :cond_0

    .line 359
    sget-object v6, Lcom/navdy/client/app/framework/util/ContactsManager;->emptyContactList:Ljava/util/ArrayList;

    .line 392
    :goto_0
    return-object v6

    .line 362
    :cond_0
    :goto_1
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 366
    const-string v6, "display_name"

    invoke-static {p0, v6}, Lcom/navdy/client/app/providers/DbUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 367
    .local v5, "name":Ljava/lang/String;
    invoke-virtual {v2, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 368
    invoke-virtual {v2, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/client/app/framework/models/ContactModel;

    .line 375
    .local v1, "contact":Lcom/navdy/client/app/framework/models/ContactModel;
    :goto_2
    invoke-static {p0}, Lcom/navdy/client/app/framework/util/ContactsManager;->getAddress(Landroid/database/Cursor;)Lcom/navdy/client/app/framework/models/Address;

    move-result-object v0

    .line 376
    .local v0, "address":Lcom/navdy/client/app/framework/models/Address;
    invoke-virtual {v1, v0}, Lcom/navdy/client/app/framework/models/ContactModel;->addAddress(Lcom/navdy/client/app/framework/models/Address;)V

    .line 378
    const-string v6, "contact_id"

    invoke-static {p0, v6}, Lcom/navdy/client/app/providers/DbUtils;->getLong(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 379
    .local v3, "id":Ljava/lang/Long;
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v1, v6, v7}, Lcom/navdy/client/app/framework/models/ContactModel;->setID(J)V

    .line 381
    const-string v6, "lookup"

    invoke-static {p0, v6}, Lcom/navdy/client/app/providers/DbUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 382
    .local v4, "lookupKey":Ljava/lang/String;
    invoke-virtual {v1, v4}, Lcom/navdy/client/app/framework/models/ContactModel;->setLookupKey(Ljava/lang/String;)V

    .line 383
    invoke-virtual {v1}, Lcom/navdy/client/app/framework/models/ContactModel;->setLookupTimestampToNow()V

    .line 389
    invoke-virtual {v2, v5, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 370
    .end local v0    # "address":Lcom/navdy/client/app/framework/models/Address;
    .end local v1    # "contact":Lcom/navdy/client/app/framework/models/ContactModel;
    .end local v3    # "id":Ljava/lang/Long;
    .end local v4    # "lookupKey":Ljava/lang/String;
    :cond_1
    new-instance v1, Lcom/navdy/client/app/framework/models/ContactModel;

    invoke-direct {v1}, Lcom/navdy/client/app/framework/models/ContactModel;-><init>()V

    .line 371
    .restart local v1    # "contact":Lcom/navdy/client/app/framework/models/ContactModel;
    invoke-virtual {v1, v5}, Lcom/navdy/client/app/framework/models/ContactModel;->setName(Ljava/lang/String;)V

    goto :goto_2

    .line 392
    .end local v1    # "contact":Lcom/navdy/client/app/framework/models/ContactModel;
    .end local v5    # "name":Ljava/lang/String;
    :cond_2
    new-instance v6, Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    goto :goto_0
.end method

.method public static getContactIdFromLookupKey(Landroid/content/ContentResolver;Ljava/lang/String;J)J
    .locals 12
    .param p0, "contentResolver"    # Landroid/content/ContentResolver;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "lastKnownContactId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 429
    invoke-static {p1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 471
    .end local p2    # "lastKnownContactId":J
    :goto_0
    return-wide p2

    .line 434
    .restart local p2    # "lastKnownContactId":J
    :cond_0
    const-wide/16 v4, 0x0

    cmp-long v0, p2, v4

    if-lez v0, :cond_2

    .line 435
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Landroid/provider/ContactsContract$Contacts;->CONTENT_LOOKUP_URI:Landroid/net/Uri;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "/"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "/"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 440
    .local v1, "uri":Landroid/net/Uri;
    :goto_1
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "_id"

    aput-object v3, v2, v0

    const/4 v0, 0x1

    const-string v3, "lookup"

    aput-object v3, v2, v0

    .line 445
    .local v2, "projection":[Ljava/lang/String;
    const/4 v10, 0x0

    .line 447
    .local v10, "cursor":Landroid/database/Cursor;
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    :try_start_0
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 454
    if-eqz v10, :cond_1

    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_3

    .line 455
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v3, "contact no longer exists for key"

    invoke-direct {v0, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 466
    :catch_0
    move-exception v11

    .line 467
    .local v11, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v0, Lcom/navdy/client/app/framework/util/ContactsManager;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Something went wrong while trying to get a contact cursor"

    invoke-virtual {v0, v3, v11}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 469
    invoke-static {v10}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_0

    .line 437
    .end local v1    # "uri":Landroid/net/Uri;
    .end local v2    # "projection":[Ljava/lang/String;
    .end local v10    # "cursor":Landroid/database/Cursor;
    .end local v11    # "e":Ljava/lang/Exception;
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Landroid/provider/ContactsContract$Contacts;->CONTENT_LOOKUP_URI:Landroid/net/Uri;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "/"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .restart local v1    # "uri":Landroid/net/Uri;
    goto :goto_1

    .line 457
    .restart local v2    # "projection":[Ljava/lang/String;
    .restart local v10    # "cursor":Landroid/database/Cursor;
    :cond_3
    :try_start_2
    const-string v0, "_id"

    invoke-static {v10, v0}, Lcom/navdy/client/app/providers/DbUtils;->getLong(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v8

    .line 458
    .local v8, "latestContactId":J
    const-string v0, "lookup"

    invoke-static {v10, v0}, Lcom/navdy/client/app/providers/DbUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .local v7, "latestLookupKey":Ljava/lang/String;
    move-object v4, p1

    move-wide v5, p2

    .line 460
    invoke-static/range {v4 .. v9}, Lcom/navdy/client/app/framework/util/ContactsManager;->updateContactData(Ljava/lang/String;JLjava/lang/String;J)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 462
    const-wide/16 v4, 0x0

    cmp-long v0, v8, v4

    if-gtz v0, :cond_4

    .line 469
    invoke-static {v10}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto/16 :goto_0

    :cond_4
    invoke-static {v10}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    move-wide p2, v8

    goto/16 :goto_0

    .end local v7    # "latestLookupKey":Ljava/lang/String;
    .end local v8    # "latestContactId":J
    :catchall_0
    move-exception v0

    invoke-static {v10}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v0
.end method

.method public static getContactName(Landroid/content/ContentResolver;J)Ljava/lang/String;
    .locals 9
    .param p0, "cr"    # Landroid/content/ContentResolver;
    .param p1, "id"    # J
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 660
    const/4 v6, 0x0

    .line 662
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "display_name"

    aput-object v3, v2, v0

    const-string v3, "contact_id=?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    .line 666
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v5, 0x0

    move-object v0, p0

    .line 662
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 669
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 670
    const-string v0, "display_name"

    invoke-static {v6, v0}, Lcom/navdy/client/app/providers/DbUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 675
    invoke-static {v6}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 677
    :goto_0
    return-object v0

    .line 675
    :cond_0
    invoke-static {v6}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    :goto_1
    move-object v0, v8

    .line 677
    goto :goto_0

    .line 672
    :catch_0
    move-exception v7

    .line 673
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v0, Lcom/navdy/client/app/framework/util/ContactsManager;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Something went wrong while trying to get a contact cursor"

    invoke-virtual {v0, v1, v7}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 675
    invoke-static {v6}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_1

    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v0
.end method

.method private getContacts(Ljava/lang/String;Ljava/util/EnumSet;Z)Ljava/util/List;
    .locals 7
    .param p1, "filter"    # Ljava/lang/String;
    .param p3, "loadPhotos"    # Z
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/navdy/client/app/framework/util/ContactsManager$Attributes;",
            ">;Z)",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/ContactModel;",
            ">;"
        }
    .end annotation

    .prologue
    .local p2, "set":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/navdy/client/app/framework/util/ContactsManager$Attributes;>;"
    const/4 v6, 0x0

    .line 149
    invoke-static {}, Lcom/navdy/client/app/framework/util/SystemUtils;->ensureNotOnMainThread()V

    .line 151
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 152
    .local v1, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/ContactModel;>;"
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 154
    .local v2, "contentResolver":Landroid/content/ContentResolver;
    sget-object v3, Lcom/navdy/client/app/framework/util/ContactsManager$Attributes;->BOTH:Lcom/navdy/client/app/framework/util/ContactsManager$Attributes;

    invoke-virtual {p2, v3}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 155
    invoke-static {p1, v2}, Lcom/navdy/client/app/framework/util/ContactsManager;->getContactsWithBoth(Ljava/lang/String;Landroid/content/ContentResolver;)Ljava/util/List;

    move-result-object v1

    .line 162
    :cond_0
    :goto_0
    if-eqz p3, :cond_3

    .line 163
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/models/ContactModel;

    .line 164
    .local v0, "contact":Lcom/navdy/client/app/framework/models/ContactModel;
    iget-wide v4, v0, Lcom/navdy/client/app/framework/models/ContactModel;->id:J

    invoke-static {v4, v5, v6}, Lcom/navdy/client/app/framework/util/ContactsManager;->loadContactPhoto(JZ)Landroid/graphics/Bitmap;

    move-result-object v4

    iput-object v4, v0, Lcom/navdy/client/app/framework/models/ContactModel;->photo:Landroid/graphics/Bitmap;

    goto :goto_1

    .line 156
    .end local v0    # "contact":Lcom/navdy/client/app/framework/models/ContactModel;
    :cond_1
    sget-object v3, Lcom/navdy/client/app/framework/util/ContactsManager$Attributes;->ADDRESS:Lcom/navdy/client/app/framework/util/ContactsManager$Attributes;

    invoke-virtual {p2, v3}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 157
    invoke-direct {p0, p1, v2}, Lcom/navdy/client/app/framework/util/ContactsManager;->getContactsWithAddresses(Ljava/lang/String;Landroid/content/ContentResolver;)Ljava/util/List;

    move-result-object v1

    goto :goto_0

    .line 158
    :cond_2
    sget-object v3, Lcom/navdy/client/app/framework/util/ContactsManager$Attributes;->PHONE:Lcom/navdy/client/app/framework/util/ContactsManager$Attributes;

    invoke-virtual {p2, v3}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 159
    invoke-direct {p0, p1, v2, v6}, Lcom/navdy/client/app/framework/util/ContactsManager;->getContactsWithPhoneNumbers(Ljava/lang/String;Landroid/content/ContentResolver;Z)Ljava/util/List;

    move-result-object v1

    goto :goto_0

    .line 170
    :cond_3
    return-object v1
.end method

.method public static getContactsPreferablyWithPhoneNumber(Ljava/lang/String;Landroid/content/ContentResolver;)Ljava/util/List;
    .locals 14
    .param p0, "filter"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p1, "contentResolver"    # Landroid/content/ContentResolver;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/content/ContentResolver;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/ContactModel;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 266
    invoke-static {p0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 315
    :cond_0
    :goto_0
    return-object v7

    .line 271
    :cond_1
    const-string v3, "display_name LIKE ?"

    .line 272
    .local v3, "selection":Ljava/lang/String;
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 274
    .local v4, "selectionArgs":[Ljava/lang/String;
    const/4 v8, 0x0

    .line 276
    .local v8, "dataCursor":Landroid/database/Cursor;
    :try_start_0
    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/navdy/client/app/framework/util/ContactsManager;->genericProjection:[Ljava/lang/String;

    const-string v5, "has_phone_number DESC"

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    .line 286
    :goto_1
    if-eqz v8, :cond_0

    .line 291
    :try_start_1
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 292
    .local v7, "contactList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/ContactModel;>;"
    :goto_2
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 293
    const-string v0, "contact_id"

    invoke-static {v8, v0}, Lcom/navdy/client/app/providers/DbUtils;->getLong(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v10

    .line 294
    .local v10, "id":J
    const-string v0, "lookup"

    invoke-static {v8, v0}, Lcom/navdy/client/app/providers/DbUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 295
    .local v12, "lookupKey":Ljava/lang/String;
    const-string v0, "display_name"

    invoke-static {v8, v0}, Lcom/navdy/client/app/providers/DbUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 303
    .local v13, "name":Ljava/lang/String;
    new-instance v6, Lcom/navdy/client/app/framework/models/ContactModel;

    invoke-direct {v6}, Lcom/navdy/client/app/framework/models/ContactModel;-><init>()V

    .line 304
    .local v6, "contact":Lcom/navdy/client/app/framework/models/ContactModel;
    invoke-virtual {v6, v13}, Lcom/navdy/client/app/framework/models/ContactModel;->setName(Ljava/lang/String;)V

    .line 305
    invoke-virtual {v6, v10, v11}, Lcom/navdy/client/app/framework/models/ContactModel;->setID(J)V

    .line 306
    invoke-virtual {v6, v12}, Lcom/navdy/client/app/framework/models/ContactModel;->setLookupKey(Ljava/lang/String;)V

    .line 307
    invoke-virtual {v6}, Lcom/navdy/client/app/framework/models/ContactModel;->setLookupTimestampToNow()V

    .line 308
    invoke-interface {v7, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 315
    .end local v6    # "contact":Lcom/navdy/client/app/framework/models/ContactModel;
    .end local v7    # "contactList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/ContactModel;>;"
    .end local v10    # "id":J
    .end local v12    # "lookupKey":Ljava/lang/String;
    .end local v13    # "name":Ljava/lang/String;
    :catchall_0
    move-exception v0

    invoke-static {v8}, Lcom/navdy/client/app/framework/util/SystemUtils;->closeCursor(Landroid/database/Cursor;)V

    throw v0

    .line 282
    :catch_0
    move-exception v9

    .line 283
    .local v9, "e":Ljava/lang/Exception;
    sget-object v0, Lcom/navdy/client/app/framework/util/ContactsManager;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Something went wrong while trying to get a contact cursor"

    invoke-virtual {v0, v1, v9}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 315
    .end local v9    # "e":Ljava/lang/Exception;
    .restart local v7    # "contactList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/ContactModel;>;"
    :cond_2
    invoke-static {v8}, Lcom/navdy/client/app/framework/util/SystemUtils;->closeCursor(Landroid/database/Cursor;)V

    goto :goto_0
.end method

.method private getContactsWithAddresses(Ljava/lang/String;Landroid/content/ContentResolver;)Ljava/util/List;
    .locals 3
    .param p1, "filter"    # Ljava/lang/String;
    .param p2, "contentResolver"    # Landroid/content/ContentResolver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/content/ContentResolver;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/ContactModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 321
    const/4 v0, 0x0

    .line 324
    .local v0, "addressCursor":Landroid/database/Cursor;
    :try_start_0
    invoke-static {p2, p1}, Lcom/navdy/client/app/framework/util/ContactsManager;->getAddressCursor(Landroid/content/ContentResolver;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 325
    if-nez v0, :cond_0

    .line 326
    sget-object v1, Lcom/navdy/client/app/framework/util/ContactsManager;->emptyContactList:Ljava/util/ArrayList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 331
    invoke-static {v0}, Lcom/navdy/client/app/framework/util/SystemUtils;->closeCursor(Landroid/database/Cursor;)V

    .line 333
    :goto_0
    return-object v1

    .line 329
    :cond_0
    :try_start_1
    invoke-static {v0}, Lcom/navdy/client/app/framework/util/ContactsManager;->getAddressesFromContactsHelper(Landroid/database/Cursor;)Ljava/util/List;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    .line 331
    .local v1, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/ContactModel;>;"
    invoke-static {v0}, Lcom/navdy/client/app/framework/util/SystemUtils;->closeCursor(Landroid/database/Cursor;)V

    goto :goto_0

    .end local v1    # "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/ContactModel;>;"
    :catchall_0
    move-exception v2

    invoke-static {v0}, Lcom/navdy/client/app/framework/util/SystemUtils;->closeCursor(Landroid/database/Cursor;)V

    throw v2
.end method

.method private static getContactsWithBoth(Ljava/lang/String;Landroid/content/ContentResolver;)Ljava/util/List;
    .locals 18
    .param p0, "filter"    # Ljava/lang/String;
    .param p1, "contentResolver"    # Landroid/content/ContentResolver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/content/ContentResolver;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/ContactModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 191
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 192
    .local v7, "contacts":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/navdy/client/app/framework/models/ContactModel;>;"
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/ContactsManager;->getGenericCursor(Landroid/content/ContentResolver;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 194
    .local v8, "dataCursor":Landroid/database/Cursor;
    if-nez v8, :cond_0

    .line 195
    sget-object v6, Lcom/navdy/client/app/framework/util/ContactsManager;->emptyContactList:Ljava/util/ArrayList;

    .line 255
    :goto_0
    return-object v6

    .line 199
    :cond_0
    :goto_1
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v15

    if-eqz v15, :cond_4

    .line 200
    const-string v15, "contact_id"

    invoke-static {v8, v15}, Lcom/navdy/client/app/providers/DbUtils;->getLong(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v10

    .line 201
    .local v10, "id":J
    const-string v15, "lookup"

    invoke-static {v8, v15}, Lcom/navdy/client/app/providers/DbUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 202
    .local v12, "lookupKey":Ljava/lang/String;
    const-string v15, "display_name"

    invoke-static {v8, v15}, Lcom/navdy/client/app/providers/DbUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 203
    .local v13, "name":Ljava/lang/String;
    const-string v15, "has_phone_number"

    invoke-static {v8, v15}, Lcom/navdy/client/app/providers/DbUtils;->getBoolean(Landroid/database/Cursor;Ljava/lang/String;)Z

    move-result v9

    .line 205
    .local v9, "hasPhoneNumber":Z
    const/4 v14, 0x0

    .line 209
    .local v14, "phoneNumbers":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/PhoneNumberModel;>;"
    move-object/from16 v0, p1

    invoke-static {v0, v12, v10, v11}, Lcom/navdy/client/app/framework/util/ContactsManager;->getAddressFromLookupKey(Landroid/content/ContentResolver;Ljava/lang/String;J)Ljava/util/List;

    move-result-object v2

    .line 213
    .local v2, "addresses":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/Address;>;"
    if-eqz v9, :cond_2

    .line 217
    move-object/from16 v0, p1

    invoke-static {v0, v12, v10, v11}, Lcom/navdy/client/app/framework/util/ContactsManager;->getContactIdFromLookupKey(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v4

    .line 218
    .local v4, "contactId":J
    const-wide/16 v16, 0x0

    cmp-long v15, v4, v16

    if-gez v15, :cond_1

    .line 224
    move-wide v4, v10

    .line 227
    :cond_1
    move-object/from16 v0, p1

    invoke-static {v0, v4, v5}, Lcom/navdy/client/app/framework/util/ContactsManager;->getPhoneNumbers(Landroid/content/ContentResolver;J)Ljava/util/List;

    move-result-object v14

    .line 235
    .end local v4    # "contactId":J
    :cond_2
    invoke-virtual {v7, v13}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    if-eqz v15, :cond_3

    .line 236
    invoke-virtual {v7, v13}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/navdy/client/app/framework/models/ContactModel;

    .line 242
    .local v3, "contact":Lcom/navdy/client/app/framework/models/ContactModel;
    :goto_2
    invoke-virtual {v3, v10, v11}, Lcom/navdy/client/app/framework/models/ContactModel;->setID(J)V

    .line 243
    invoke-virtual {v3, v12}, Lcom/navdy/client/app/framework/models/ContactModel;->setLookupKey(Ljava/lang/String;)V

    .line 244
    invoke-virtual {v3}, Lcom/navdy/client/app/framework/models/ContactModel;->setLookupTimestampToNow()V

    .line 245
    invoke-virtual {v3, v2}, Lcom/navdy/client/app/framework/models/ContactModel;->addAddresses(Ljava/util/List;)V

    .line 246
    invoke-virtual {v3, v14}, Lcom/navdy/client/app/framework/models/ContactModel;->addPhoneNumbers(Ljava/util/List;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 255
    .end local v2    # "addresses":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/Address;>;"
    .end local v3    # "contact":Lcom/navdy/client/app/framework/models/ContactModel;
    .end local v9    # "hasPhoneNumber":Z
    .end local v10    # "id":J
    .end local v12    # "lookupKey":Ljava/lang/String;
    .end local v13    # "name":Ljava/lang/String;
    .end local v14    # "phoneNumbers":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/PhoneNumberModel;>;"
    :catchall_0
    move-exception v15

    invoke-static {v8}, Lcom/navdy/client/app/framework/util/SystemUtils;->closeCursor(Landroid/database/Cursor;)V

    throw v15

    .line 238
    .restart local v2    # "addresses":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/Address;>;"
    .restart local v9    # "hasPhoneNumber":Z
    .restart local v10    # "id":J
    .restart local v12    # "lookupKey":Ljava/lang/String;
    .restart local v13    # "name":Ljava/lang/String;
    .restart local v14    # "phoneNumbers":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/PhoneNumberModel;>;"
    :cond_3
    :try_start_1
    new-instance v3, Lcom/navdy/client/app/framework/models/ContactModel;

    invoke-direct {v3}, Lcom/navdy/client/app/framework/models/ContactModel;-><init>()V

    .line 239
    .restart local v3    # "contact":Lcom/navdy/client/app/framework/models/ContactModel;
    invoke-virtual {v3, v13}, Lcom/navdy/client/app/framework/models/ContactModel;->setName(Ljava/lang/String;)V

    .line 240
    invoke-virtual {v7, v13, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 249
    .end local v2    # "addresses":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/Address;>;"
    .end local v3    # "contact":Lcom/navdy/client/app/framework/models/ContactModel;
    .end local v9    # "hasPhoneNumber":Z
    .end local v10    # "id":J
    .end local v12    # "lookupKey":Ljava/lang/String;
    .end local v13    # "name":Ljava/lang/String;
    .end local v14    # "phoneNumbers":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/PhoneNumberModel;>;"
    :cond_4
    new-instance v6, Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v15

    invoke-direct {v6, v15}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 255
    .local v6, "contactList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/ContactModel;>;"
    invoke-static {v8}, Lcom/navdy/client/app/framework/util/SystemUtils;->closeCursor(Landroid/database/Cursor;)V

    goto :goto_0
.end method

.method private getContactsWithPhoneNumbers(Ljava/lang/String;Landroid/content/ContentResolver;Z)Ljava/util/List;
    .locals 3
    .param p1, "filter"    # Ljava/lang/String;
    .param p2, "contentResolver"    # Landroid/content/ContentResolver;
    .param p3, "starredOnly"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/content/ContentResolver;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/ContactModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 339
    const/4 v1, 0x0

    .line 341
    .local v1, "phoneCursor":Landroid/database/Cursor;
    :try_start_0
    invoke-static {p2, p1, p3}, Lcom/navdy/client/app/framework/util/ContactsManager;->getPhoneCursor(Landroid/content/ContentResolver;Ljava/lang/String;Z)Landroid/database/Cursor;

    move-result-object v1

    .line 342
    if-nez v1, :cond_0

    .line 343
    sget-object v0, Lcom/navdy/client/app/framework/util/ContactsManager;->emptyContactList:Ljava/util/ArrayList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 348
    invoke-static {v1}, Lcom/navdy/client/app/framework/util/SystemUtils;->closeCursor(Landroid/database/Cursor;)V

    .line 350
    :goto_0
    return-object v0

    .line 346
    :cond_0
    :try_start_1
    invoke-static {v1}, Lcom/navdy/client/app/framework/util/ContactsManager;->getPhoneNumbersFromContactsHelper(Landroid/database/Cursor;)Ljava/util/List;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 348
    .local v0, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/ContactModel;>;"
    invoke-static {v1}, Lcom/navdy/client/app/framework/util/SystemUtils;->closeCursor(Landroid/database/Cursor;)V

    goto :goto_0

    .end local v0    # "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/ContactModel;>;"
    :catchall_0
    move-exception v2

    invoke-static {v1}, Lcom/navdy/client/app/framework/util/SystemUtils;->closeCursor(Landroid/database/Cursor;)V

    throw v2
.end method

.method private static getGenericCursor(Landroid/content/ContentResolver;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8
    .param p0, "contentResolver"    # Landroid/content/ContentResolver;
    .param p1, "startsWith"    # Ljava/lang/String;

    .prologue
    .line 637
    const/4 v3, 0x0

    .line 638
    .local v3, "selection":Ljava/lang/String;
    const/4 v4, 0x0

    .line 640
    .local v4, "selectionArgs":[Ljava/lang/String;
    invoke-static {p1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 641
    const-string v3, "display_name LIKE ?"

    .line 642
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    .end local v4    # "selectionArgs":[Ljava/lang/String;
    const/4 v0, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 644
    .restart local v4    # "selectionArgs":[Ljava/lang/String;
    :cond_0
    const/4 v6, 0x0

    .line 646
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/navdy/client/app/framework/util/ContactsManager;->genericProjection:[Ljava/lang/String;

    const-string v5, "sort_key ASC"

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 655
    :goto_0
    return-object v6

    .line 652
    :catch_0
    move-exception v7

    .line 653
    .local v7, "e":Ljava/lang/Exception;
    sget-object v0, Lcom/navdy/client/app/framework/util/ContactsManager;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Something went wrong while trying to get a contact cursor"

    invoke-virtual {v0, v1, v7}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static getInstance()Lcom/navdy/client/app/framework/util/ContactsManager;
    .locals 1

    .prologue
    .line 132
    sget-object v0, Lcom/navdy/client/app/framework/util/ContactsManager;->instance:Lcom/navdy/client/app/framework/util/ContactsManager;

    return-object v0
.end method

.method private static getPhoneCursor(Landroid/content/ContentResolver;Ljava/lang/String;Z)Landroid/database/Cursor;
    .locals 8
    .param p0, "contentResolver"    # Landroid/content/ContentResolver;
    .param p1, "startsWith"    # Ljava/lang/String;
    .param p2, "starredOnly"    # Z

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 581
    const/4 v3, 0x0

    .line 582
    .local v3, "selection":Ljava/lang/String;
    const/4 v4, 0x0

    .line 583
    .local v4, "args":[Ljava/lang/String;
    if-eqz p2, :cond_0

    .line 584
    const-string v3, "starred>?"

    .line 585
    new-array v4, v1, [Ljava/lang/String;

    .end local v4    # "args":[Ljava/lang/String;
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v2

    .line 587
    .restart local v4    # "args":[Ljava/lang/String;
    :cond_0
    invoke-static {p1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 588
    const-string v3, "display_name LIKE ?"

    .line 589
    new-array v4, v1, [Ljava/lang/String;

    .end local v4    # "args":[Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v2

    .line 591
    .restart local v4    # "args":[Ljava/lang/String;
    :cond_1
    const/4 v6, 0x0

    .line 593
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    sget-object v1, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/navdy/client/app/framework/util/ContactsManager;->phoneProjection:[Ljava/lang/String;

    const-string v5, "sort_key ASC"

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 602
    :goto_0
    return-object v6

    .line 599
    :catch_0
    move-exception v7

    .line 600
    .local v7, "e":Ljava/lang/Exception;
    sget-object v0, Lcom/navdy/client/app/framework/util/ContactsManager;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Something went wrong while trying to get a phone number cursor"

    invoke-virtual {v0, v1, v7}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private static getPhoneNumber(Landroid/database/Cursor;)Lcom/navdy/client/app/framework/models/PhoneNumberModel;
    .locals 5
    .param p0, "cursor"    # Landroid/database/Cursor;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 568
    const-string v4, "data1"

    invoke-static {p0, v4}, Lcom/navdy/client/app/providers/DbUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 569
    .local v2, "phone":Ljava/lang/String;
    const-string v4, "data2"

    invoke-static {p0, v4}, Lcom/navdy/client/app/providers/DbUtils;->getInt(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v3

    .line 570
    .local v3, "type":I
    const-string v4, "data3"

    invoke-static {p0, v4}, Lcom/navdy/client/app/providers/DbUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 571
    .local v0, "customType":Ljava/lang/String;
    const-string v4, "is_primary"

    invoke-static {p0, v4}, Lcom/navdy/client/app/providers/DbUtils;->getBoolean(Landroid/database/Cursor;Ljava/lang/String;)Z

    move-result v1

    .line 573
    .local v1, "isPrimary":Z
    new-instance v4, Lcom/navdy/client/app/framework/models/PhoneNumberModel;

    invoke-direct {v4, v2, v3, v0, v1}, Lcom/navdy/client/app/framework/models/PhoneNumberModel;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    return-object v4
.end method

.method public static getPhoneNumbers(Landroid/content/ContentResolver;J)Ljava/util/List;
    .locals 11
    .param p0, "contentResolver"    # Landroid/content/ContentResolver;
    .param p1, "contactId"    # J
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "J)",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/PhoneNumberModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 536
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 538
    .local v9, "numbers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/PhoneNumberModel;>;"
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    .line 563
    :goto_0
    return-object v9

    .line 542
    :cond_0
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    .line 543
    .local v6, "contactIdString":Ljava/lang/String;
    const-string v3, "contact_id = ?"

    .line 544
    .local v3, "selection":Ljava/lang/String;
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object v6, v4, v0

    .line 546
    .local v4, "selectionArgs":[Ljava/lang/String;
    const/4 v7, 0x0

    .line 548
    .local v7, "cursor":Landroid/database/Cursor;
    :try_start_0
    sget-object v1, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/navdy/client/app/framework/util/ContactsManager;->phoneProjection:[Ljava/lang/String;

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 554
    :goto_1
    if-eqz v7, :cond_1

    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 555
    invoke-static {v7}, Lcom/navdy/client/app/framework/util/ContactsManager;->getPhoneNumber(Landroid/database/Cursor;)Lcom/navdy/client/app/framework/models/PhoneNumberModel;

    move-result-object v10

    .line 556
    .local v10, "phoneNumber":Lcom/navdy/client/app/framework/models/PhoneNumberModel;
    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 558
    .end local v10    # "phoneNumber":Lcom/navdy/client/app/framework/models/PhoneNumberModel;
    :catch_0
    move-exception v8

    .line 559
    .local v8, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v0, Lcom/navdy/client/app/framework/util/ContactsManager;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Something went wrong while trying to get a phone number cursor"

    invoke-virtual {v0, v1, v8}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 561
    invoke-static {v7}, Lcom/navdy/client/app/framework/util/SystemUtils;->closeCursor(Landroid/database/Cursor;)V

    goto :goto_0

    .end local v8    # "e":Ljava/lang/Exception;
    :cond_1
    invoke-static {v7}, Lcom/navdy/client/app/framework/util/SystemUtils;->closeCursor(Landroid/database/Cursor;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v7}, Lcom/navdy/client/app/framework/util/SystemUtils;->closeCursor(Landroid/database/Cursor;)V

    throw v0
.end method

.method private static getPhoneNumbersFromContactsHelper(Landroid/database/Cursor;)Ljava/util/List;
    .locals 8
    .param p0, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/ContactModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 397
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    .line 399
    .local v1, "contacts":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/navdy/client/app/framework/models/ContactModel;>;"
    :goto_0
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 402
    const-string v6, "display_name"

    invoke-static {p0, v6}, Lcom/navdy/client/app/providers/DbUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 404
    .local v4, "name":Ljava/lang/String;
    invoke-virtual {v1, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 405
    invoke-virtual {v1, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/models/ContactModel;

    .line 411
    .local v0, "contact":Lcom/navdy/client/app/framework/models/ContactModel;
    :goto_1
    invoke-static {p0}, Lcom/navdy/client/app/framework/util/ContactsManager;->getPhoneNumber(Landroid/database/Cursor;)Lcom/navdy/client/app/framework/models/PhoneNumberModel;

    move-result-object v5

    .line 412
    .local v5, "phoneNumber":Lcom/navdy/client/app/framework/models/PhoneNumberModel;
    invoke-virtual {v0, v5}, Lcom/navdy/client/app/framework/models/ContactModel;->addPhoneNumber(Lcom/navdy/client/app/framework/models/PhoneNumberModel;)V

    .line 414
    const-string v6, "starred"

    invoke-static {p0, v6}, Lcom/navdy/client/app/providers/DbUtils;->getInt(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v6

    if-lez v6, :cond_1

    const/4 v6, 0x1

    :goto_2
    iput-boolean v6, v0, Lcom/navdy/client/app/framework/models/ContactModel;->favorite:Z

    .line 416
    const-string v6, "contact_id"

    invoke-static {p0, v6}, Lcom/navdy/client/app/providers/DbUtils;->getLong(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 417
    .local v2, "id":Ljava/lang/Long;
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v0, v6, v7}, Lcom/navdy/client/app/framework/models/ContactModel;->setID(J)V

    .line 419
    const-string v6, "lookup"

    invoke-static {p0, v6}, Lcom/navdy/client/app/providers/DbUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 420
    .local v3, "lookupKey":Ljava/lang/String;
    invoke-virtual {v0, v3}, Lcom/navdy/client/app/framework/models/ContactModel;->setLookupKey(Ljava/lang/String;)V

    .line 421
    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/ContactModel;->setLookupTimestampToNow()V

    .line 423
    invoke-virtual {v1, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 407
    .end local v0    # "contact":Lcom/navdy/client/app/framework/models/ContactModel;
    .end local v2    # "id":Ljava/lang/Long;
    .end local v3    # "lookupKey":Ljava/lang/String;
    .end local v5    # "phoneNumber":Lcom/navdy/client/app/framework/models/PhoneNumberModel;
    :cond_0
    new-instance v0, Lcom/navdy/client/app/framework/models/ContactModel;

    invoke-direct {v0}, Lcom/navdy/client/app/framework/models/ContactModel;-><init>()V

    .line 408
    .restart local v0    # "contact":Lcom/navdy/client/app/framework/models/ContactModel;
    invoke-virtual {v0, v4}, Lcom/navdy/client/app/framework/models/ContactModel;->setName(Ljava/lang/String;)V

    goto :goto_1

    .line 414
    .restart local v5    # "phoneNumber":Lcom/navdy/client/app/framework/models/PhoneNumberModel;
    :cond_1
    const/4 v6, 0x0

    goto :goto_2

    .line 425
    .end local v0    # "contact":Lcom/navdy/client/app/framework/models/ContactModel;
    .end local v4    # "name":Ljava/lang/String;
    .end local v5    # "phoneNumber":Lcom/navdy/client/app/framework/models/PhoneNumberModel;
    :cond_2
    new-instance v6, Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v6
.end method

.method private static getUserProfileEmail()Ljava/lang/String;
    .locals 10
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 719
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 720
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    const/4 v6, 0x0

    .line 722
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    sget-object v1, Landroid/provider/ContactsContract$Profile;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "data"

    invoke-static {v1, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "data1"

    aput-object v4, v2, v3

    const-string v3, "mimetype=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v9, "vnd.android.cursor.item/email_v2"

    aput-object v9, v4, v5

    const-string v5, "is_primary DESC, is_primary DESC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 734
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-gtz v1, :cond_1

    .line 743
    :cond_0
    invoke-static {v6}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    move-object v1, v8

    .line 745
    :goto_0
    return-object v1

    .line 737
    :cond_1
    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 738
    const-string v1, "data1"

    invoke-static {v6, v1}, Lcom/navdy/client/app/providers/DbUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    .line 743
    invoke-static {v6}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_0

    :cond_2
    invoke-static {v6}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    :goto_1
    move-object v1, v8

    .line 745
    goto :goto_0

    .line 740
    :catch_0
    move-exception v7

    .line 741
    .local v7, "e":Ljava/lang/Exception;
    :try_start_2
    sget-object v1, Lcom/navdy/client/app/framework/util/ContactsManager;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Something went wrong while trying to get a contact cursor"

    invoke-virtual {v1, v2, v7}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 743
    invoke-static {v6}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_1

    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    invoke-static {v6}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v1
.end method

.method private static loadContactPhoto(JZ)Landroid/graphics/Bitmap;
    .locals 10
    .param p0, "id"    # J
    .param p2, "highRes"    # Z

    .prologue
    .line 682
    const/4 v3, 0x0

    .line 684
    .local v3, "input":Ljava/io/InputStream;
    :try_start_0
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 685
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    sget-object v7, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v7, p0, p1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v5

    .line 686
    .local v5, "uri":Landroid/net/Uri;
    invoke-static {v0, v5, p2}, Landroid/provider/ContactsContract$Contacts;->openContactPhotoInputStream(Landroid/content/ContentResolver;Landroid/net/Uri;Z)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 687
    if-nez v3, :cond_0

    .line 688
    const/4 v4, 0x0

    .line 708
    invoke-static {v3}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 710
    .end local v0    # "contentResolver":Landroid/content/ContentResolver;
    .end local v5    # "uri":Landroid/net/Uri;
    :goto_0
    return-object v4

    .line 694
    .restart local v0    # "contentResolver":Landroid/content/ContentResolver;
    .restart local v5    # "uri":Landroid/net/Uri;
    :cond_0
    :try_start_1
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b0092

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 695
    .local v6, "width":I
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b008f

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 696
    .local v2, "height":I
    new-instance v7, Lcom/navdy/client/app/framework/util/ContactsManager$1;

    invoke-direct {v7, v0, v5, p2}, Lcom/navdy/client/app/framework/util/ContactsManager$1;-><init>(Landroid/content/ContentResolver;Landroid/net/Uri;Z)V

    invoke-static {v7, v6, v2}, Lcom/navdy/client/app/framework/util/ImageUtils;->getScaledBitmap(Lcom/navdy/client/app/framework/util/ImageUtils$StreamFactory;II)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v4

    .line 708
    .local v4, "photo":Landroid/graphics/Bitmap;
    invoke-static {v3}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_0

    .line 704
    .end local v0    # "contentResolver":Landroid/content/ContentResolver;
    .end local v2    # "height":I
    .end local v4    # "photo":Landroid/graphics/Bitmap;
    .end local v5    # "uri":Landroid/net/Uri;
    .end local v6    # "width":I
    :catch_0
    move-exception v1

    .line 705
    .local v1, "e":Ljava/lang/Exception;
    :try_start_2
    sget-object v7, Lcom/navdy/client/app/framework/util/ContactsManager;->logger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v7, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 706
    const/4 v4, 0x0

    .line 708
    .restart local v4    # "photo":Landroid/graphics/Bitmap;
    invoke-static {v3}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_0

    .end local v1    # "e":Ljava/lang/Exception;
    .end local v4    # "photo":Landroid/graphics/Bitmap;
    :catchall_0
    move-exception v7

    invoke-static {v3}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v7
.end method

.method public static lookUpUserProfileInfo()Lcom/navdy/client/app/framework/models/UserAccountInfo;
    .locals 12

    .prologue
    const/4 v3, 0x0

    .line 752
    const/4 v8, 0x0

    .line 753
    .local v8, "cursor":Landroid/database/Cursor;
    new-instance v11, Lcom/navdy/client/app/framework/models/UserAccountInfo;

    const-string v1, ""

    const-string v2, ""

    invoke-direct {v11, v1, v2, v3}, Lcom/navdy/client/app/framework/models/UserAccountInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 754
    .local v11, "uai":Lcom/navdy/client/app/framework/models/UserAccountInfo;
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 756
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    :try_start_0
    sget-object v1, Landroid/provider/ContactsContract$Profile;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "display_name"

    aput-object v4, v2, v3

    const-string v3, "is_user_profile = 1"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 765
    if-eqz v8, :cond_1

    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 766
    const-string v1, "_id"

    invoke-static {v8, v1}, Lcom/navdy/client/app/providers/DbUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 767
    .local v10, "id":Ljava/lang/String;
    const-string v1, "display_name"

    invoke-static {v8, v1}, Lcom/navdy/client/app/providers/DbUtils;->getString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v11, Lcom/navdy/client/app/framework/models/UserAccountInfo;->fullName:Ljava/lang/String;

    .line 768
    sget-object v1, Lcom/navdy/client/app/framework/util/ContactsManager;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "***** name = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v11, Lcom/navdy/client/app/framework/models/UserAccountInfo;->fullName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 770
    invoke-static {v10}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 771
    invoke-static {v10}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    .line 772
    .local v6, "contactId":J
    invoke-static {}, Lcom/navdy/client/app/framework/util/ContactsManager;->getUserProfileEmail()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v11, Lcom/navdy/client/app/framework/models/UserAccountInfo;->email:Ljava/lang/String;

    .line 773
    sget-object v1, Lcom/navdy/client/app/framework/util/ContactsManager;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "***** email = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v11, Lcom/navdy/client/app/framework/models/UserAccountInfo;->email:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 774
    const/4 v1, 0x1

    invoke-static {v6, v7, v1}, Lcom/navdy/client/app/framework/util/ContactsManager;->loadContactPhoto(JZ)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, v11, Lcom/navdy/client/app/framework/models/UserAccountInfo;->photo:Landroid/graphics/Bitmap;

    .line 775
    sget-object v1, Lcom/navdy/client/app/framework/util/ContactsManager;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "***** photo = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v11, Lcom/navdy/client/app/framework/models/UserAccountInfo;->photo:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 784
    .end local v6    # "contactId":J
    .end local v10    # "id":Ljava/lang/String;
    :cond_0
    :goto_0
    invoke-static {v8}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 786
    :goto_1
    return-object v11

    .line 779
    :cond_1
    :try_start_1
    sget-object v1, Lcom/navdy/client/app/framework/util/ContactsManager;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "The profile cursor is null or empty!"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 781
    :catch_0
    move-exception v9

    .line 782
    .local v9, "e":Ljava/lang/Exception;
    :try_start_2
    sget-object v1, Lcom/navdy/client/app/framework/util/ContactsManager;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Something went wrong while trying to get a contact cursor"

    invoke-virtual {v1, v2, v9}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 784
    invoke-static {v8}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_1

    .end local v9    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    invoke-static {v8}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v1
.end method

.method private static updateContactData(Ljava/lang/String;JLjava/lang/String;J)V
    .locals 9
    .param p0, "oldLookupKey"    # Ljava/lang/String;
    .param p1, "oldContactId"    # J
    .param p3, "newLookupKey"    # Ljava/lang/String;
    .param p4, "newContactId"    # J

    .prologue
    .line 794
    invoke-static {p0, p3}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 795
    sget-object v4, Lcom/navdy/client/app/framework/util/ContactsManager;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "updateContactData: The lookup keys are the same so no-op"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 834
    :goto_0
    return-void

    .line 800
    :cond_0
    invoke-static {p0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    const-wide/16 v4, 0x0

    cmp-long v4, p1, v4

    if-lez v4, :cond_1

    .line 801
    new-instance v3, Landroid/util/Pair;

    const-string v4, "contact_lookup_key=? OR last_known_contact_id=?"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p0, v5, v6

    const/4 v6, 0x1

    .line 805
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-direct {v3, v4, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 820
    .local v3, "selection":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;[Ljava/lang/String;>;"
    :goto_1
    sget-object v5, Lcom/navdy/client/app/framework/util/ContactsManager;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Updating any destination that matches: selection = "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v4, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " args = "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v4, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, [Ljava/lang/Object;

    .line 821
    invoke-static {v4}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 820
    invoke-virtual {v5, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 823
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    .line 824
    .local v2, "context":Landroid/content/Context;
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 825
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 826
    .local v1, "contentValues":Landroid/content/ContentValues;
    const-string v4, "contact_lookup_key"

    invoke-virtual {v1, v4, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 827
    const-string v4, "last_known_contact_id"

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 828
    const-string v4, "last_contact_lookup"

    new-instance v5, Ljava/util/Date;

    invoke-direct {v5}, Ljava/util/Date;-><init>()V

    invoke-virtual {v5}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 829
    sget-object v6, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->DESTINATIONS_CONTENT_URI:Landroid/net/Uri;

    iget-object v4, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, Ljava/lang/String;

    iget-object v5, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v5, [Ljava/lang/String;

    invoke-virtual {v0, v6, v1, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_0

    .line 806
    .end local v0    # "contentResolver":Landroid/content/ContentResolver;
    .end local v1    # "contentValues":Landroid/content/ContentValues;
    .end local v2    # "context":Landroid/content/Context;
    .end local v3    # "selection":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;[Ljava/lang/String;>;"
    :cond_1
    invoke-static {p0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    const-wide/16 v4, 0x0

    cmp-long v4, p1, v4

    if-gtz v4, :cond_2

    .line 807
    new-instance v3, Landroid/util/Pair;

    const-string v4, "contact_lookup_key=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p0, v5, v6

    invoke-direct {v3, v4, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .restart local v3    # "selection":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;[Ljava/lang/String;>;"
    goto/16 :goto_1

    .line 810
    .end local v3    # "selection":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;[Ljava/lang/String;>;"
    :cond_2
    invoke-static {p0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    const-wide/16 v4, 0x0

    cmp-long v4, p1, v4

    if-lez v4, :cond_3

    .line 811
    new-instance v3, Landroid/util/Pair;

    const-string v4, "last_known_contact_id=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    .line 813
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-direct {v3, v4, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .restart local v3    # "selection":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;[Ljava/lang/String;>;"
    goto/16 :goto_1

    .line 815
    .end local v3    # "selection":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;[Ljava/lang/String;>;"
    :cond_3
    sget-object v4, Lcom/navdy/client/app/framework/util/ContactsManager;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Can\'t update contact data because oldLookupKey = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " and oldContactId = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public getContactsAndLoadPhotos(Ljava/lang/String;Ljava/util/EnumSet;)Ljava/util/List;
    .locals 1
    .param p1, "filter"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/navdy/client/app/framework/util/ContactsManager$Attributes;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/ContactModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 141
    .local p2, "set":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/navdy/client/app/framework/util/ContactsManager$Attributes;>;"
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/client/app/framework/util/ContactsManager;->getContacts(Ljava/lang/String;Ljava/util/EnumSet;Z)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getContactsWithoutLoadingPhotos(Ljava/lang/String;Ljava/util/EnumSet;)Ljava/util/List;
    .locals 1
    .param p1, "filter"    # Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/navdy/client/app/framework/util/ContactsManager$Attributes;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/ContactModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 137
    .local p2, "set":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/navdy/client/app/framework/util/ContactsManager$Attributes;>;"
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/client/app/framework/util/ContactsManager;->getContacts(Ljava/lang/String;Ljava/util/EnumSet;Z)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getFavoriteContactsWithPhone()Ljava/util/List;
    .locals 3
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/ContactModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 180
    invoke-static {}, Lcom/navdy/client/app/framework/util/SystemUtils;->ensureNotOnMainThread()V

    .line 181
    const/4 v0, 0x0

    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/navdy/client/app/framework/util/ContactsManager;->getContactsWithPhoneNumbers(Ljava/lang/String;Landroid/content/ContentResolver;Z)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

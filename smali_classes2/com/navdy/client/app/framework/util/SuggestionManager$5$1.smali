.class Lcom/navdy/client/app/framework/util/SuggestionManager$5$1;
.super Ljava/lang/Object;
.source "SuggestionManager.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/util/SuggestionManager$5;->onCalendarEventsRead(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/navdy/client/app/framework/models/CalendarEvent;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/util/SuggestionManager$5;

.field final synthetic val$now:J


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/util/SuggestionManager$5;J)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/util/SuggestionManager$5;

    .prologue
    .line 758
    iput-object p1, p0, Lcom/navdy/client/app/framework/util/SuggestionManager$5$1;->this$0:Lcom/navdy/client/app/framework/util/SuggestionManager$5;

    iput-wide p2, p0, Lcom/navdy/client/app/framework/util/SuggestionManager$5$1;->val$now:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/navdy/client/app/framework/models/CalendarEvent;Lcom/navdy/client/app/framework/models/CalendarEvent;)I
    .locals 6
    .param p1, "lhs"    # Lcom/navdy/client/app/framework/models/CalendarEvent;
    .param p2, "rhs"    # Lcom/navdy/client/app/framework/models/CalendarEvent;

    .prologue
    .line 761
    iget-wide v0, p0, Lcom/navdy/client/app/framework/util/SuggestionManager$5$1;->val$now:J

    iget-wide v2, p1, Lcom/navdy/client/app/framework/models/CalendarEvent;->startTimestamp:J

    sub-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    iget-wide v2, p0, Lcom/navdy/client/app/framework/util/SuggestionManager$5$1;->val$now:J

    iget-wide v4, p2, Lcom/navdy/client/app/framework/models/CalendarEvent;->startTimestamp:J

    sub-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v2

    sub-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 758
    check-cast p1, Lcom/navdy/client/app/framework/models/CalendarEvent;

    check-cast p2, Lcom/navdy/client/app/framework/models/CalendarEvent;

    invoke-virtual {p0, p1, p2}, Lcom/navdy/client/app/framework/util/SuggestionManager$5$1;->compare(Lcom/navdy/client/app/framework/models/CalendarEvent;Lcom/navdy/client/app/framework/models/CalendarEvent;)I

    move-result v0

    return v0
.end method

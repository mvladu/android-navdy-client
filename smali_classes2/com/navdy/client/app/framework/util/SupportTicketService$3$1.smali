.class Lcom/navdy/client/app/framework/util/SupportTicketService$3$1;
.super Ljava/lang/Object;
.source "SupportTicketService.java"

# interfaces
.implements Lcom/navdy/client/debug/file/FileTransferManager$FileTransferListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/util/SupportTicketService$3;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/util/SupportTicketService$3;

.field final synthetic val$attachments:Ljava/util/ArrayList;

.field final synthetic val$logsFolder:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/util/SupportTicketService$3;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/util/SupportTicketService$3;

    .prologue
    .line 619
    iput-object p1, p0, Lcom/navdy/client/app/framework/util/SupportTicketService$3$1;->this$0:Lcom/navdy/client/app/framework/util/SupportTicketService$3;

    iput-object p2, p0, Lcom/navdy/client/app/framework/util/SupportTicketService$3$1;->val$logsFolder:Ljava/lang/String;

    iput-object p3, p0, Lcom/navdy/client/app/framework/util/SupportTicketService$3$1;->val$attachments:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/navdy/service/library/events/file/FileTransferError;Ljava/lang/String;)V
    .locals 3
    .param p1, "errorCode"    # Lcom/navdy/service/library/events/file/FileTransferError;
    .param p2, "error"    # Ljava/lang/String;

    .prologue
    .line 654
    sget-object v0, Lcom/navdy/client/app/framework/util/SupportTicketService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error collecting display logs "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 655
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/SupportTicketService$3$1;->val$attachments:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 656
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/SupportTicketService$3$1;->this$0:Lcom/navdy/client/app/framework/util/SupportTicketService$3;

    iget-object v0, v0, Lcom/navdy/client/app/framework/util/SupportTicketService$3;->val$onLogCollectedInterface:Lcom/navdy/client/app/framework/util/SupportTicketService$OnLogCollectedInterface;

    iget-object v1, p0, Lcom/navdy/client/app/framework/util/SupportTicketService$3$1;->val$attachments:Ljava/util/ArrayList;

    invoke-interface {v0, v1}, Lcom/navdy/client/app/framework/util/SupportTicketService$OnLogCollectedInterface;->onLogCollectionSucceeded(Ljava/util/ArrayList;)V

    .line 659
    :goto_0
    return-void

    .line 658
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/SupportTicketService$3$1;->this$0:Lcom/navdy/client/app/framework/util/SupportTicketService$3;

    iget-object v0, v0, Lcom/navdy/client/app/framework/util/SupportTicketService$3;->val$onLogCollectedInterface:Lcom/navdy/client/app/framework/util/SupportTicketService$OnLogCollectedInterface;

    invoke-interface {v0}, Lcom/navdy/client/app/framework/util/SupportTicketService$OnLogCollectedInterface;->onLogCollectionFailed()V

    goto :goto_0
.end method

.method public onFileTransferResponse(Lcom/navdy/service/library/events/file/FileTransferResponse;)V
    .locals 5
    .param p1, "response"    # Lcom/navdy/service/library/events/file/FileTransferResponse;

    .prologue
    .line 622
    sget-object v2, Lcom/navdy/client/app/framework/util/SupportTicketService;->VERBOSE:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 623
    sget-object v2, Lcom/navdy/client/app/framework/util/SupportTicketService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onFileTransferResponse: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p1, Lcom/navdy/service/library/events/file/FileTransferResponse;->success:Ljava/lang/Boolean;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 625
    :cond_0
    iget-object v2, p1, Lcom/navdy/service/library/events/file/FileTransferResponse;->success:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_1

    .line 626
    iget-object v2, p1, Lcom/navdy/service/library/events/file/FileTransferResponse;->error:Lcom/navdy/service/library/events/file/FileTransferError;

    const-string v3, "Error collecting display logs"

    invoke-virtual {p0, v2, v3}, Lcom/navdy/client/app/framework/util/SupportTicketService$3$1;->onError(Lcom/navdy/service/library/events/file/FileTransferError;Ljava/lang/String;)V

    .line 635
    :goto_0
    return-void

    .line 628
    :cond_1
    sget-object v2, Lcom/navdy/client/app/framework/util/SupportTicketService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "HUD file name saved"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 629
    iget-object v0, p1, Lcom/navdy/service/library/events/file/FileTransferResponse;->destinationFileName:Ljava/lang/String;

    .line 631
    .local v0, "hudDisplayLogName":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/navdy/client/app/framework/util/SupportTicketService$3$1;->val$logsFolder:Ljava/lang/String;

    invoke-direct {v1, v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 633
    .local v1, "hudLog":Ljava/io/File;
    iget-object v2, p0, Lcom/navdy/client/app/framework/util/SupportTicketService$3$1;->val$attachments:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public onFileTransferStatus(Lcom/navdy/service/library/events/file/FileTransferStatus;)V
    .locals 3
    .param p1, "status"    # Lcom/navdy/service/library/events/file/FileTransferStatus;

    .prologue
    .line 639
    sget-object v0, Lcom/navdy/client/app/framework/util/SupportTicketService;->VERBOSE:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 640
    sget-object v0, Lcom/navdy/client/app/framework/util/SupportTicketService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onFileTransferStatus: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/navdy/service/library/events/file/FileTransferStatus;->success:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 642
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/file/FileTransferStatus;->success:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/navdy/service/library/events/file/FileTransferStatus;->transferComplete:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 643
    sget-object v0, Lcom/navdy/client/app/framework/util/SupportTicketService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Logs collected"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 644
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/SupportTicketService$3$1;->val$attachments:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 645
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/SupportTicketService$3$1;->this$0:Lcom/navdy/client/app/framework/util/SupportTicketService$3;

    iget-object v0, v0, Lcom/navdy/client/app/framework/util/SupportTicketService$3;->val$onLogCollectedInterface:Lcom/navdy/client/app/framework/util/SupportTicketService$OnLogCollectedInterface;

    iget-object v1, p0, Lcom/navdy/client/app/framework/util/SupportTicketService$3$1;->val$attachments:Ljava/util/ArrayList;

    invoke-interface {v0, v1}, Lcom/navdy/client/app/framework/util/SupportTicketService$OnLogCollectedInterface;->onLogCollectionSucceeded(Ljava/util/ArrayList;)V

    .line 650
    :cond_1
    :goto_0
    return-void

    .line 647
    :cond_2
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/SupportTicketService$3$1;->this$0:Lcom/navdy/client/app/framework/util/SupportTicketService$3;

    iget-object v0, v0, Lcom/navdy/client/app/framework/util/SupportTicketService$3;->val$onLogCollectedInterface:Lcom/navdy/client/app/framework/util/SupportTicketService$OnLogCollectedInterface;

    invoke-interface {v0}, Lcom/navdy/client/app/framework/util/SupportTicketService$OnLogCollectedInterface;->onLogCollectionFailed()V

    goto :goto_0
.end method

.class public Lcom/navdy/client/app/framework/util/ImageUtils;
.super Ljava/lang/Object;
.source "ImageUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/framework/util/ImageUtils$StreamFactory;
    }
.end annotation


# static fields
.field private static final VERBOSE:Z

.field private static final handler:Landroid/os/Handler;

.field private static final logger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 31
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/util/ImageUtils;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/util/ImageUtils;->logger:Lcom/navdy/service/library/log/Logger;

    .line 34
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/navdy/client/app/framework/util/ImageUtils;->handler:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static calculateInSampleSize(Landroid/graphics/BitmapFactory$Options;II)I
    .locals 6
    .param p0, "options"    # Landroid/graphics/BitmapFactory$Options;
    .param p1, "reqWidth"    # I
    .param p2, "reqHeight"    # I
    .annotation build Landroid/support/annotation/CheckResult;
    .end annotation

    .prologue
    .line 108
    iget v2, p0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 109
    .local v2, "height":I
    iget v4, p0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 110
    .local v4, "width":I
    const/4 v3, 0x1

    .line 112
    .local v3, "inSampleSize":I
    if-gt v2, p2, :cond_0

    if-le v4, p1, :cond_1

    .line 114
    :cond_0
    div-int/lit8 v0, v2, 0x2

    .line 115
    .local v0, "halfHeight":I
    div-int/lit8 v1, v4, 0x2

    .line 119
    .local v1, "halfWidth":I
    :goto_0
    div-int v5, v0, v3

    if-le v5, p2, :cond_1

    div-int v5, v1, v3

    if-le v5, p1, :cond_1

    .line 121
    mul-int/lit8 v3, v3, 0x2

    goto :goto_0

    .line 125
    .end local v0    # "halfHeight":I
    .end local v1    # "halfWidth":I
    :cond_1
    return v3
.end method

.method private static getBitmap(I)Landroid/graphics/Bitmap;
    .locals 3
    .param p0, "resId"    # I
    .annotation build Landroid/support/annotation/CheckResult;
    .end annotation

    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 159
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 160
    .local v0, "appContext":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 161
    .local v2, "res":Landroid/content/res/Resources;
    invoke-static {v2, p0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 162
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v1, :cond_0

    .line 166
    :cond_0
    return-object v1
.end method

.method public static getBitmap(III)Landroid/graphics/Bitmap;
    .locals 8
    .param p0, "resId"    # I
    .param p1, "width"    # I
    .param p2, "height"    # I
    .annotation build Landroid/support/annotation/CheckResult;
    .end annotation

    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 173
    :try_start_0
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 174
    .local v2, "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v5, 0x1

    iput-boolean v5, v2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 175
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 176
    .local v0, "appContext":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 177
    .local v3, "res":Landroid/content/res/Resources;
    invoke-static {v3, p0, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 180
    invoke-static {v2, p1, p2}, Lcom/navdy/client/app/framework/util/ImageUtils;->calculateInSampleSize(Landroid/graphics/BitmapFactory$Options;II)I

    move-result v5

    iput v5, v2, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 181
    const/4 v5, 0x0

    iput-boolean v5, v2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 183
    invoke-static {v3, p0, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 184
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v1, :cond_0

    .line 190
    .end local v0    # "appContext":Landroid/content/Context;
    .end local v1    # "bitmap":Landroid/graphics/Bitmap;
    .end local v2    # "options":Landroid/graphics/BitmapFactory$Options;
    .end local v3    # "res":Landroid/content/res/Resources;
    :cond_0
    :goto_0
    return-object v1

    .line 188
    :catch_0
    move-exception v4

    .line 189
    .local v4, "t":Ljava/lang/Throwable;
    sget-object v5, Lcom/navdy/client/app/framework/util/ImageUtils;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Fetching compressed Bitmap failed: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 190
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getScaledBitmap(Lcom/navdy/client/app/framework/util/ImageUtils$StreamFactory;II)Landroid/graphics/Bitmap;
    .locals 8
    .param p0, "streamFactory"    # Lcom/navdy/client/app/framework/util/ImageUtils$StreamFactory;
    .param p1, "width"    # I
    .param p2, "height"    # I
    .annotation build Landroid/support/annotation/CheckResult;
    .end annotation

    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 46
    if-nez p0, :cond_1

    .line 88
    :cond_0
    :goto_0
    return-object v4

    .line 50
    :cond_1
    const/4 v1, 0x0

    .line 54
    .local v1, "inputStream":Ljava/io/InputStream;
    :try_start_0
    invoke-interface {p0}, Lcom/navdy/client/app/framework/util/ImageUtils$StreamFactory;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    .line 56
    if-nez v1, :cond_2

    .line 57
    sget-object v5, Lcom/navdy/client/app/framework/util/ImageUtils;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "Failed to get input stream to scale"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 87
    if-eqz v1, :cond_0

    .line 88
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_0

    .line 62
    :cond_2
    :try_start_1
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 63
    .local v2, "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v5, 0x1

    iput-boolean v5, v2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 64
    const/4 v5, 0x0

    invoke-static {v1, v5, v2}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 67
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 68
    const/4 v1, 0x0

    .line 71
    invoke-static {v2, p1, p2}, Lcom/navdy/client/app/framework/util/ImageUtils;->calculateInSampleSize(Landroid/graphics/BitmapFactory$Options;II)I

    move-result v5

    iput v5, v2, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 72
    const/4 v5, 0x0

    iput-boolean v5, v2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 74
    invoke-interface {p0}, Lcom/navdy/client/app/framework/util/ImageUtils$StreamFactory;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    .line 75
    const/4 v5, 0x0

    invoke-static {v1, v5, v2}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 76
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_3

    .line 77
    sget-object v5, Lcom/navdy/service/library/util/ScalingUtilities$ScalingLogic;->FIT:Lcom/navdy/service/library/util/ScalingUtilities$ScalingLogic;

    invoke-static {v0, p1, p2, v5}, Lcom/navdy/service/library/util/ScalingUtilities;->createScaledBitmapAndRecycleOriginalIfScaled(Landroid/graphics/Bitmap;IILcom/navdy/service/library/util/ScalingUtilities$ScalingLogic;)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v4

    .line 87
    if-eqz v1, :cond_0

    .line 88
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_0

    .line 80
    :cond_3
    :try_start_2
    sget-object v5, Lcom/navdy/client/app/framework/util/ImageUtils;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "Fetching compressed bitmap failed"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 87
    if-eqz v1, :cond_0

    .line 88
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_0

    .line 83
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v2    # "options":Landroid/graphics/BitmapFactory$Options;
    :catch_0
    move-exception v3

    .line 84
    .local v3, "t":Ljava/lang/Throwable;
    :try_start_3
    sget-object v5, Lcom/navdy/client/app/framework/util/ImageUtils;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Fetching scaled Bitmap failed: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 87
    if-eqz v1, :cond_0

    .line 88
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_0

    .line 87
    .end local v3    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v4

    if-eqz v1, :cond_4

    .line 88
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    :cond_4
    throw v4
.end method

.method public static getScaledBitmap(Ljava/io/File;II)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "file"    # Ljava/io/File;
    .param p1, "width"    # I
    .param p2, "height"    # I
    .annotation build Landroid/support/annotation/CheckResult;
    .end annotation

    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 96
    new-instance v0, Lcom/navdy/client/app/framework/util/ImageUtils$1;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/framework/util/ImageUtils$1;-><init>(Ljava/io/File;)V

    invoke-static {v0, p1, p2}, Lcom/navdy/client/app/framework/util/ImageUtils;->getScaledBitmap(Lcom/navdy/client/app/framework/util/ImageUtils$StreamFactory;II)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static loadImage(Landroid/widget/ImageView;ILcom/navdy/client/app/framework/util/ImageCache;)V
    .locals 5
    .param p0, "imageView"    # Landroid/widget/ImageView;
    .param p1, "resId"    # I
    .param p2, "imageCache"    # Lcom/navdy/client/app/framework/util/ImageCache;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 197
    if-eqz p2, :cond_1

    .line 198
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Lcom/navdy/client/app/framework/util/ImageCache;->getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 199
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_1

    .line 200
    invoke-virtual {p0, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 218
    :cond_0
    :goto_0
    return-void

    .line 204
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_1
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p0, v2}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 205
    invoke-static {p1}, Lcom/navdy/client/app/framework/util/ImageUtils;->getBitmap(I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 206
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_2

    .line 207
    invoke-virtual {p0}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v1

    .line 208
    .local v1, "tag":Ljava/lang/Object;
    if-eqz v1, :cond_0

    instance-of v2, v1, Ljava/lang/Integer;

    if-eqz v2, :cond_0

    check-cast v1, Ljava/lang/Integer;

    .end local v1    # "tag":Ljava/lang/Object;
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v2, p1, :cond_0

    .line 209
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 210
    invoke-virtual {p0, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 211
    if-eqz p2, :cond_0

    .line 212
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2, v0}, Lcom/navdy/client/app/framework/util/ImageCache;->putBitmap(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 216
    :cond_2
    sget-object v2, Lcom/navdy/client/app/framework/util/ImageUtils;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "bitmap not loaded:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static resizeBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 6
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;
    .param p1, "maxSize"    # I
    .annotation build Landroid/support/annotation/CheckResult;
    .end annotation

    .prologue
    .line 130
    if-eqz p0, :cond_0

    if-gez p1, :cond_1

    .line 131
    :cond_0
    const/4 v2, 0x0

    .line 153
    :goto_0
    return-object v2

    .line 133
    :cond_1
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 134
    .local v3, "width":I
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    .line 139
    .local v0, "height":I
    int-to-float v4, v3

    int-to-float v5, v0

    div-float v1, v4, v5

    .line 140
    .local v1, "ratio":F
    const/high16 v4, 0x3f800000    # 1.0f

    cmpl-float v4, v1, v4

    if-lez v4, :cond_2

    .line 142
    move v3, p1

    .line 143
    int-to-float v4, v3

    div-float/2addr v4, v1

    float-to-int v0, v4

    .line 151
    :goto_1
    const/4 v4, 0x1

    invoke-static {p0, v3, v0, v4}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 152
    .local v2, "scaledBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    .line 145
    .end local v2    # "scaledBitmap":Landroid/graphics/Bitmap;
    :cond_2
    move v0, p1

    .line 146
    int-to-float v4, v0

    div-float/2addr v4, v1

    float-to-int v3, v4

    goto :goto_1
.end method

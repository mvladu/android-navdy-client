.class synthetic Lcom/navdy/client/app/framework/search/GooglePlacesSearch$3;
.super Ljava/lang/Object;
.source "GooglePlacesSearch.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/search/GooglePlacesSearch;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$navdy$client$app$framework$search$GooglePlacesSearch$Query:[I

.field static final synthetic $SwitchMap$com$navdy$client$app$framework$search$GooglePlacesSearch$RankByTypes:[I

.field static final synthetic $SwitchMap$com$navdy$client$app$framework$search$GooglePlacesSearch$ServiceTypes:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 312
    invoke-static {}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;->values()[Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$3;->$SwitchMap$com$navdy$client$app$framework$search$GooglePlacesSearch$ServiceTypes:[I

    :try_start_0
    sget-object v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$3;->$SwitchMap$com$navdy$client$app$framework$search$GooglePlacesSearch$ServiceTypes:[I

    sget-object v1, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;->GAS:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_8

    :goto_0
    :try_start_1
    sget-object v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$3;->$SwitchMap$com$navdy$client$app$framework$search$GooglePlacesSearch$ServiceTypes:[I

    sget-object v1, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;->PARKING:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_7

    .line 138
    :goto_1
    invoke-static {}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$RankByTypes;->values()[Lcom/navdy/client/app/framework/search/GooglePlacesSearch$RankByTypes;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$3;->$SwitchMap$com$navdy$client$app$framework$search$GooglePlacesSearch$RankByTypes:[I

    :try_start_2
    sget-object v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$3;->$SwitchMap$com$navdy$client$app$framework$search$GooglePlacesSearch$RankByTypes:[I

    sget-object v1, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$RankByTypes;->PROMINENCE:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$RankByTypes;

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$RankByTypes;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_6

    :goto_2
    :try_start_3
    sget-object v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$3;->$SwitchMap$com$navdy$client$app$framework$search$GooglePlacesSearch$RankByTypes:[I

    sget-object v1, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$RankByTypes;->DISTANCE:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$RankByTypes;

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$RankByTypes;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_5

    .line 91
    :goto_3
    invoke-static {}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;->values()[Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$3;->$SwitchMap$com$navdy$client$app$framework$search$GooglePlacesSearch$Query:[I

    :try_start_4
    sget-object v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$3;->$SwitchMap$com$navdy$client$app$framework$search$GooglePlacesSearch$Query:[I

    sget-object v1, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;->QUERY_AUTOCOMPLETE:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :goto_4
    :try_start_5
    sget-object v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$3;->$SwitchMap$com$navdy$client$app$framework$search$GooglePlacesSearch$Query:[I

    sget-object v1, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;->TEXTSEARCH:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_3

    :goto_5
    :try_start_6
    sget-object v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$3;->$SwitchMap$com$navdy$client$app$framework$search$GooglePlacesSearch$Query:[I

    sget-object v1, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;->DETAILS:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_2

    :goto_6
    :try_start_7
    sget-object v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$3;->$SwitchMap$com$navdy$client$app$framework$search$GooglePlacesSearch$Query:[I

    sget-object v1, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;->DIRECTIONS:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_1

    :goto_7
    :try_start_8
    sget-object v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$3;->$SwitchMap$com$navdy$client$app$framework$search$GooglePlacesSearch$Query:[I

    sget-object v1, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;->NEARBY:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_0

    :goto_8
    return-void

    :catch_0
    move-exception v0

    goto :goto_8

    :catch_1
    move-exception v0

    goto :goto_7

    :catch_2
    move-exception v0

    goto :goto_6

    :catch_3
    move-exception v0

    goto :goto_5

    :catch_4
    move-exception v0

    goto :goto_4

    .line 138
    :catch_5
    move-exception v0

    goto :goto_3

    :catch_6
    move-exception v0

    goto :goto_2

    .line 312
    :catch_7
    move-exception v0

    goto :goto_1

    :catch_8
    move-exception v0

    goto :goto_0
.end method

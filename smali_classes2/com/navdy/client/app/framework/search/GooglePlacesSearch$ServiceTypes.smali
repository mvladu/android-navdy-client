.class public final enum Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;
.super Ljava/lang/Enum;
.source "GooglePlacesSearch.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/search/GooglePlacesSearch;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ServiceTypes"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;

.field public static final enum ATM:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;

.field public static final enum COFFEE:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;

.field public static final enum FOOD:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;

.field public static final enum GAS:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;

.field public static final enum HOSPITAL:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;

.field public static final enum PARKING:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;

.field public static final enum STORE:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;


# instance fields
.field baseUrl:Ljava/lang/String;

.field hudServiceType:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 107
    new-instance v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;

    const-string v1, "GAS"

    const-string v2, "gas_station"

    const-string v3, "Gas"

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;->GAS:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;

    .line 108
    new-instance v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;

    const-string v1, "PARKING"

    const-string v2, "parking"

    const-string v3, "Parking"

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;->PARKING:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;

    .line 109
    new-instance v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;

    const-string v1, "FOOD"

    const-string v2, "restaurant"

    const-string v3, "Food"

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;->FOOD:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;

    .line 110
    new-instance v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;

    const-string v1, "COFFEE"

    const-string v2, "cafe"

    const-string v3, "Coffee"

    invoke-direct {v0, v1, v8, v2, v3}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;->COFFEE:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;

    .line 111
    new-instance v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;

    const-string v1, "ATM"

    const-string v2, "atm"

    const-string v3, "ATMs"

    invoke-direct {v0, v1, v9, v2, v3}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;->ATM:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;

    .line 112
    new-instance v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;

    const-string v1, "HOSPITAL"

    const/4 v2, 0x5

    const-string v3, "hospital"

    const-string v4, "Hospitals"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;->HOSPITAL:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;

    .line 113
    new-instance v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;

    const-string v1, "STORE"

    const/4 v2, 0x6

    const-string v3, "convenience_store"

    const-string v4, "Grocery Stores"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;->STORE:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;

    .line 106
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;

    sget-object v1, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;->GAS:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;->PARKING:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;->FOOD:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;

    aput-object v1, v0, v7

    sget-object v1, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;->COFFEE:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;

    aput-object v1, v0, v8

    sget-object v1, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;->ATM:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;->HOSPITAL:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;->STORE:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;->$VALUES:[Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p3, "baseUrl"    # Ljava/lang/String;
    .param p4, "hudServiceType"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 118
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 119
    iput-object p3, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;->baseUrl:Ljava/lang/String;

    .line 120
    iput-object p4, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;->hudServiceType:Ljava/lang/String;

    .line 121
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 106
    const-class v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;

    return-object v0
.end method

.method public static values()[Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;
    .locals 1

    .prologue
    .line 106
    sget-object v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;->$VALUES:[Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;

    invoke-virtual {v0}, [Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;

    return-object v0
.end method


# virtual methods
.method public getBaseUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;->baseUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getHudServiceType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;->hudServiceType:Ljava/lang/String;

    return-object v0
.end method

.class public Lcom/navdy/client/app/framework/glances/EmailNotificationHandler;
.super Ljava/lang/Object;
.source "EmailNotificationHandler.java"


# static fields
.field private static sLogger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->sLogger:Lcom/navdy/service/library/log/Logger;

    sput-object v0, Lcom/navdy/client/app/framework/glances/EmailNotificationHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static handleEmailNotification(Landroid/service/notification/StatusBarNotification;)V
    .locals 28
    .param p0, "sbn"    # Landroid/service/notification/StatusBarNotification;

    .prologue
    .line 27
    invoke-virtual/range {p0 .. p0}, Landroid/service/notification/StatusBarNotification;->getPackageName()Ljava/lang/String;

    move-result-object v17

    .line 29
    .local v17, "packageName":Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/navdy/client/app/ui/glances/GlanceUtils;->isThisGlanceEnabledAsWellAsGlobal(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_0

    sget-object v24, Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;->EMAIL_GROUP:Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

    move-object/from16 v0, v17

    move-object/from16 v1, v24

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/glances/GlanceConstants;->isPackageInGroup(Ljava/lang/String;Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;)Z

    move-result v24

    if-nez v24, :cond_1

    .line 30
    :cond_0
    sget-object v24, Lcom/navdy/client/app/framework/glances/EmailNotificationHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "email notification not handled ["

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, "]"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 155
    :goto_0
    return-void

    .line 34
    :cond_1
    invoke-virtual/range {p0 .. p0}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v15

    .line 35
    .local v15, "notification":Landroid/app/Notification;
    invoke-static {v15}, Landroid/support/v7/app/NotificationCompat;->getExtras(Landroid/app/Notification;)Landroid/os/Bundle;

    move-result-object v8

    .line 37
    .local v8, "extras":Landroid/os/Bundle;
    const-string v24, "android.title"

    move-object/from16 v0, v24

    invoke-static {v8, v0}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->getStringSafely(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 38
    .local v20, "senderOrTitle":Ljava/lang/String;
    const-string v24, "android.subText"

    move-object/from16 v0, v24

    invoke-static {v8, v0}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->getStringSafely(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    .line 39
    .local v23, "toEmail":Ljava/lang/String;
    const-string v24, "android.text"

    move-object/from16 v0, v24

    invoke-static {v8, v0}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->getStringSafely(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    .line 40
    .local v21, "subject":Ljava/lang/String;
    const-string v24, "android.bigText"

    move-object/from16 v0, v24

    invoke-static {v8, v0}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->getStringSafely(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 42
    .local v4, "body":Ljava/lang/String;
    const-string v24, "android.textLines"

    move-object/from16 v0, v24

    invoke-virtual {v8, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v22

    .line 44
    .local v22, "textLines":Ljava/lang/Object;
    const-string v24, "com.google.android.gm"

    move-object/from16 v0, v17

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-nez v24, :cond_8

    if-eqz v22, :cond_8

    move-object/from16 v0, v22

    instance-of v0, v0, [Ljava/lang/CharSequence;

    move/from16 v24, v0

    if-eqz v24, :cond_8

    .line 47
    check-cast v22, [Ljava/lang/CharSequence;

    .end local v22    # "textLines":Ljava/lang/Object;
    move-object/from16 v13, v22

    check-cast v13, [Ljava/lang/CharSequence;

    .line 48
    .local v13, "lines":[Ljava/lang/CharSequence;
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    .line 50
    .local v14, "newBody":Ljava/lang/StringBuilder;
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_1
    array-length v0, v13

    move/from16 v24, v0

    move/from16 v0, v24

    if-ge v9, v0, :cond_3

    .line 51
    aget-object v24, v13, v9

    move-object/from16 v0, v24

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 52
    add-int/lit8 v24, v9, 0x1

    array-length v0, v13

    move/from16 v25, v0

    move/from16 v0, v24

    move/from16 v1, v25

    if-ge v0, v1, :cond_2

    .line 53
    const-string v24, "\n"

    move-object/from16 v0, v24

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 50
    :cond_2
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 57
    :cond_3
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->length()I

    move-result v24

    if-lez v24, :cond_4

    .line 58
    const-string v24, "android.summaryText"

    move-object/from16 v0, v24

    invoke-static {v8, v0}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->getStringSafely(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    .line 59
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 77
    .end local v9    # "i":I
    .end local v13    # "lines":[Ljava/lang/CharSequence;
    .end local v14    # "newBody":Ljava/lang/StringBuilder;
    :cond_4
    :goto_2
    if-nez v4, :cond_5

    if-eqz v21, :cond_5

    .line 78
    const-string v24, "\n"

    move-object/from16 v0, v21

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v11

    .line 79
    .local v11, "index":I
    const/16 v24, -0x1

    move/from16 v0, v24

    if-eq v11, v0, :cond_5

    .line 80
    add-int/lit8 v24, v11, 0x1

    move-object/from16 v0, v21

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    .line 81
    const/16 v24, 0x0

    move-object/from16 v0, v21

    move/from16 v1, v24

    invoke-virtual {v0, v1, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v21

    .line 85
    .end local v11    # "index":I
    :cond_5
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 86
    .local v5, "builder":Ljava/lang/StringBuilder;
    const/16 v19, 0x0

    .line 87
    .local v19, "peopleStr":Ljava/lang/String;
    const-string v24, "android.people"

    move-object/from16 v0, v24

    invoke-virtual {v8, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, [Ljava/lang/String;

    move-object/from16 v18, v24

    check-cast v18, [Ljava/lang/String;

    .line 89
    .local v18, "people":[Ljava/lang/String;
    if-eqz v18, :cond_b

    .line 90
    const/4 v9, 0x0

    .restart local v9    # "i":I
    :goto_3
    move-object/from16 v0, v18

    array-length v0, v0

    move/from16 v24, v0

    move/from16 v0, v24

    if-ge v9, v0, :cond_a

    .line 91
    aget-object v16, v18, v9

    .line 92
    .local v16, "p":Ljava/lang/String;
    if-eqz v16, :cond_6

    const-string v24, "mailto:"

    move-object/from16 v0, v16

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_6

    .line 93
    const-string v24, "mailto:"

    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->length()I

    move-result v24

    move-object/from16 v0, v16

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v16

    .line 95
    :cond_6
    if-eqz v9, :cond_7

    .line 96
    const-string v24, ","

    move-object/from16 v0, v24

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 98
    :cond_7
    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 90
    add-int/lit8 v9, v9, 0x1

    goto :goto_3

    .line 61
    .end local v5    # "builder":Ljava/lang/StringBuilder;
    .end local v9    # "i":I
    .end local v16    # "p":Ljava/lang/String;
    .end local v18    # "people":[Ljava/lang/String;
    .end local v19    # "peopleStr":Ljava/lang/String;
    .restart local v22    # "textLines":Ljava/lang/Object;
    :cond_8
    if-eqz v4, :cond_4

    if-eqz v21, :cond_4

    .line 62
    const-string v24, "com.microsoft.office.outlook"

    move-object/from16 v0, v17

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_9

    .line 64
    const-string v24, "\n"

    move-object/from16 v0, v24

    invoke-virtual {v4, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v12

    .line 65
    .local v12, "indexOfBodyLineBreak":I
    add-int/lit8 v24, v12, 0x1

    move/from16 v0, v24

    invoke-virtual {v4, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 66
    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v24

    if-eqz v24, :cond_4

    .line 67
    const/16 v24, 0x0

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v25

    move-object/from16 v0, v21

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v21

    goto/16 :goto_2

    .line 69
    .end local v12    # "indexOfBodyLineBreak":I
    :cond_9
    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_4

    .line 70
    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v24

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "\n"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v4, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v11

    .line 71
    .restart local v11    # "index":I
    const/16 v24, -0x1

    move/from16 v0, v24

    if-eq v11, v0, :cond_4

    .line 72
    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v24

    add-int v24, v24, v11

    add-int/lit8 v24, v24, 0x1

    move/from16 v0, v24

    invoke-virtual {v4, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_2

    .line 100
    .end local v11    # "index":I
    .end local v22    # "textLines":Ljava/lang/Object;
    .restart local v5    # "builder":Ljava/lang/StringBuilder;
    .restart local v9    # "i":I
    .restart local v18    # "people":[Ljava/lang/String;
    .restart local v19    # "peopleStr":Ljava/lang/String;
    :cond_a
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 103
    .end local v9    # "i":I
    :cond_b
    if-nez v19, :cond_c

    if-eqz v20, :cond_c

    .line 104
    move-object/from16 v19, v20

    .line 107
    :cond_c
    sget-object v24, Lcom/navdy/client/app/framework/glances/EmailNotificationHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "[navdyinfo-gmail] senderOrTitle["

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, "] from["

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, "] to["

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, "] subject["

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, "] body["

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, "]"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 114
    if-nez v21, :cond_d

    if-eqz v4, :cond_e

    :cond_d
    if-nez v19, :cond_f

    .line 115
    :cond_e
    sget-object v24, Lcom/navdy/client/app/framework/glances/EmailNotificationHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v25, "[navdyinfo-gmail] invalid data"

    invoke-virtual/range {v24 .. v25}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 119
    :cond_f
    invoke-static {}, Lcom/navdy/client/app/framework/glances/GlancesHelper;->getId()Ljava/lang/String;

    move-result-object v10

    .line 120
    .local v10, "id":Ljava/lang/String;
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 122
    .local v6, "data":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/KeyValue;>;"
    invoke-static/range {v19 .. v19}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v24

    if-nez v24, :cond_10

    .line 123
    const-string v24, "@"

    move-object/from16 v0, v19

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v24

    if-eqz v24, :cond_14

    .line 124
    new-instance v24, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v25, Lcom/navdy/service/library/events/glances/EmailConstants;->EMAIL_FROM_NAME:Lcom/navdy/service/library/events/glances/EmailConstants;

    invoke-virtual/range {v25 .. v25}, Lcom/navdy/service/library/events/glances/EmailConstants;->name()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v2}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v24

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 125
    new-instance v24, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v25, Lcom/navdy/service/library/events/glances/EmailConstants;->EMAIL_FROM_EMAIL:Lcom/navdy/service/library/events/glances/EmailConstants;

    invoke-virtual/range {v25 .. v25}, Lcom/navdy/service/library/events/glances/EmailConstants;->name()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v24

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 131
    :cond_10
    :goto_4
    if-eqz v23, :cond_11

    .line 132
    const-string v24, "@"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v24

    if-eqz v24, :cond_15

    .line 133
    new-instance v24, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v25, Lcom/navdy/service/library/events/glances/EmailConstants;->EMAIL_TO_EMAIL:Lcom/navdy/service/library/events/glances/EmailConstants;

    invoke-virtual/range {v25 .. v25}, Lcom/navdy/service/library/events/glances/EmailConstants;->name()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v24

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 139
    :cond_11
    :goto_5
    invoke-static/range {v21 .. v21}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v24

    if-nez v24, :cond_12

    .line 140
    new-instance v24, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v25, Lcom/navdy/service/library/events/glances/EmailConstants;->EMAIL_SUBJECT:Lcom/navdy/service/library/events/glances/EmailConstants;

    invoke-virtual/range {v25 .. v25}, Lcom/navdy/service/library/events/glances/EmailConstants;->name()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v24

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 143
    :cond_12
    invoke-static {v4}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v24

    if-nez v24, :cond_13

    .line 144
    new-instance v24, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v25, Lcom/navdy/service/library/events/glances/EmailConstants;->EMAIL_BODY:Lcom/navdy/service/library/events/glances/EmailConstants;

    invoke-virtual/range {v25 .. v25}, Lcom/navdy/service/library/events/glances/EmailConstants;->name()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-direct {v0, v1, v4}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v24

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 147
    :cond_13
    new-instance v24, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    invoke-direct/range {v24 .. v24}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;-><init>()V

    sget-object v25, Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;->GLANCE_TYPE_EMAIL:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    .line 148
    invoke-virtual/range {v24 .. v25}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceType(Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v24

    .line 149
    move-object/from16 v0, v24

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->provider(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v24

    .line 150
    move-object/from16 v0, v24

    invoke-virtual {v0, v10}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->id(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v24

    iget-wide v0, v15, Landroid/app/Notification;->when:J

    move-wide/from16 v26, v0

    .line 151
    invoke-static/range {v26 .. v27}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->postTime(Ljava/lang/Long;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v24

    .line 152
    move-object/from16 v0, v24

    invoke-virtual {v0, v6}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceData(Ljava/util/List;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->build()Lcom/navdy/service/library/events/glances/GlanceEvent;

    move-result-object v7

    .line 154
    .local v7, "emailEvent":Lcom/navdy/service/library/events/glances/GlanceEvent;
    invoke-static {v7}, Lcom/navdy/client/app/framework/glances/GlancesHelper;->sendEvent(Lcom/navdy/service/library/events/glances/GlanceEvent;)V

    goto/16 :goto_0

    .line 127
    .end local v7    # "emailEvent":Lcom/navdy/service/library/events/glances/GlanceEvent;
    :cond_14
    new-instance v24, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v25, Lcom/navdy/service/library/events/glances/EmailConstants;->EMAIL_FROM_NAME:Lcom/navdy/service/library/events/glances/EmailConstants;

    invoke-virtual/range {v25 .. v25}, Lcom/navdy/service/library/events/glances/EmailConstants;->name()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v24

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    .line 135
    :cond_15
    new-instance v24, Lcom/navdy/service/library/events/glances/KeyValue;

    sget-object v25, Lcom/navdy/service/library/events/glances/EmailConstants;->EMAIL_TO_NAME:Lcom/navdy/service/library/events/glances/EmailConstants;

    invoke-virtual/range {v25 .. v25}, Lcom/navdy/service/library/events/glances/EmailConstants;->name()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2}, Lcom/navdy/service/library/events/glances/KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v24

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_5
.end method

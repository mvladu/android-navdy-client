.class final Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$3;
.super Ljava/lang/Object;
.source "NavCoordsAddressProcessor.java"

# interfaces
.implements Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->tryHerePlacesSearchNativeApiWithAddress(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$callback:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;

.field final synthetic val$processedDestination:Lcom/navdy/client/app/framework/models/Destination;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;)V
    .locals 0

    .prologue
    .line 244
    iput-object p1, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$3;->val$processedDestination:Lcom/navdy/client/app/framework/models/Destination;

    iput-object p2, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$3;->val$callback:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onComplete(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/search/Address;Lcom/navdy/client/app/framework/models/Destination$Precision;)V
    .locals 7
    .param p1, "displayCoords"    # Lcom/here/android/mpa/common/GeoCoordinate;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "navigationCoords"    # Lcom/here/android/mpa/common/GeoCoordinate;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "address"    # Lcom/here/android/mpa/search/Address;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4, "precision"    # Lcom/navdy/client/app/framework/models/Destination$Precision;

    .prologue
    .line 247
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v6

    new-instance v0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$3$1;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p1

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$3$1;-><init>(Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$3;Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/search/Address;Lcom/here/android/mpa/common/GeoCoordinate;Lcom/navdy/client/app/framework/models/Destination$Precision;)V

    const/16 v1, 0xd

    invoke-virtual {v6, v0, v1}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 271
    return-void
.end method

.method public onError(Lcom/here/android/mpa/search/ErrorCode;)V
    .locals 3
    .param p1, "error"    # Lcom/here/android/mpa/search/ErrorCode;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 275
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$3$2;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$3$2;-><init>(Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$3;Lcom/here/android/mpa/search/ErrorCode;)V

    const/16 v2, 0xd

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 283
    return-void
.end method

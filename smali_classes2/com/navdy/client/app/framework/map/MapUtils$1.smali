.class final Lcom/navdy/client/app/framework/map/MapUtils$1;
.super Landroid/os/AsyncTask;
.source "MapUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/map/MapUtils;->doReverseGeocodingFor(Lcom/google/android/gms/maps/model/LatLng;Lcom/navdy/client/app/framework/models/Destination;Ljava/lang/Runnable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Object;",
        "Ljava/util/List",
        "<",
        "Landroid/location/Address;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic val$callback:Ljava/lang/Runnable;

.field final synthetic val$center:Lcom/google/android/gms/maps/model/LatLng;

.field final synthetic val$destination:Lcom/navdy/client/app/framework/models/Destination;


# direct methods
.method constructor <init>(Lcom/google/android/gms/maps/model/LatLng;Lcom/navdy/client/app/framework/models/Destination;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 353
    iput-object p1, p0, Lcom/navdy/client/app/framework/map/MapUtils$1;->val$center:Lcom/google/android/gms/maps/model/LatLng;

    iput-object p2, p0, Lcom/navdy/client/app/framework/map/MapUtils$1;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    iput-object p3, p0, Lcom/navdy/client/app/framework/map/MapUtils$1;->val$callback:Ljava/lang/Runnable;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 353
    invoke-virtual {p0, p1}, Lcom/navdy/client/app/framework/map/MapUtils$1;->doInBackground([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected doInBackground([Ljava/lang/Object;)Ljava/util/List;
    .locals 8
    .param p1, "params"    # [Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Object;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/location/Address;",
            ">;"
        }
    .end annotation

    .prologue
    .line 356
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/AppInstance;->canReachInternet()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 357
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 358
    .local v0, "context":Landroid/content/Context;
    new-instance v1, Landroid/location/Geocoder;

    .line 359
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Landroid/location/Geocoder;-><init>(Landroid/content/Context;Ljava/util/Locale;)V

    .line 361
    .local v1, "geocoder":Landroid/location/Geocoder;
    :try_start_0
    iget-object v2, p0, Lcom/navdy/client/app/framework/map/MapUtils$1;->val$center:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v2, v2, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    iget-object v4, p0, Lcom/navdy/client/app/framework/map/MapUtils$1;->val$center:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v4, v4, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    const/4 v6, 0x1

    invoke-virtual/range {v1 .. v6}, Landroid/location/Geocoder;->getFromLocation(DDI)Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 376
    .end local v0    # "context":Landroid/content/Context;
    .end local v1    # "geocoder":Landroid/location/Geocoder;
    :goto_0
    return-object v2

    .line 364
    .restart local v0    # "context":Landroid/content/Context;
    .restart local v1    # "geocoder":Landroid/location/Geocoder;
    :catch_0
    move-exception v7

    .line 365
    .local v7, "e":Ljava/lang/Exception;
    invoke-static {}, Lcom/navdy/client/app/framework/map/MapUtils;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "Unable to reverse geocode the current position."

    invoke-virtual {v2, v3, v7}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 376
    .end local v0    # "context":Landroid/content/Context;
    .end local v1    # "geocoder":Landroid/location/Geocoder;
    .end local v7    # "e":Ljava/lang/Exception;
    :cond_0
    :goto_1
    const/4 v2, 0x0

    goto :goto_0

    .line 368
    :cond_1
    iget-object v2, p0, Lcom/navdy/client/app/framework/map/MapUtils$1;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/models/Destination;->hasValidDisplayCoordinates()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 369
    invoke-static {}, Lcom/navdy/client/app/framework/map/MapUtils;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "processDestination, destination does not have address, assigning display coordinates as address"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 370
    iget-object v2, p0, Lcom/navdy/client/app/framework/map/MapUtils$1;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/navdy/client/app/framework/map/MapUtils$1;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    iget-wide v4, v4, Lcom/navdy/client/app/framework/models/Destination;->displayLat:D

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/client/app/framework/map/MapUtils$1;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    iget-wide v4, v4, Lcom/navdy/client/app/framework/models/Destination;->displayLong:D

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/client/app/framework/models/Destination;->setRawAddressNotForDisplay(Ljava/lang/String;)V

    goto :goto_1

    .line 371
    :cond_2
    iget-object v2, p0, Lcom/navdy/client/app/framework/map/MapUtils$1;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/models/Destination;->hasValidNavCoordinates()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 372
    invoke-static {}, Lcom/navdy/client/app/framework/map/MapUtils;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "processDestination, destination does not have address, assigning nav coordinates as address"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 373
    iget-object v2, p0, Lcom/navdy/client/app/framework/map/MapUtils$1;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/navdy/client/app/framework/map/MapUtils$1;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    iget-wide v4, v4, Lcom/navdy/client/app/framework/models/Destination;->navigationLat:D

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/client/app/framework/map/MapUtils$1;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    iget-wide v4, v4, Lcom/navdy/client/app/framework/models/Destination;->navigationLong:D

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/client/app/framework/models/Destination;->setRawAddressNotForDisplay(Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 353
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/navdy/client/app/framework/map/MapUtils$1;->onPostExecute(Ljava/util/List;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/location/Address;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "addresses":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    const/4 v6, 0x0

    .line 381
    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_1

    .line 382
    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Address;

    .line 384
    .local v0, "address":Landroid/location/Address;
    iget-object v4, p0, Lcom/navdy/client/app/framework/map/MapUtils$1;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v0}, Landroid/location/Address;->getFeatureName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/client/app/framework/models/Destination;->setStreetNumber(Ljava/lang/String;)V

    .line 385
    iget-object v4, p0, Lcom/navdy/client/app/framework/map/MapUtils$1;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v0}, Landroid/location/Address;->getThoroughfare()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/client/app/framework/models/Destination;->setStreetName(Ljava/lang/String;)V

    .line 386
    iget-object v4, p0, Lcom/navdy/client/app/framework/map/MapUtils$1;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v0}, Landroid/location/Address;->getLocality()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/client/app/framework/models/Destination;->setCity(Ljava/lang/String;)V

    .line 387
    iget-object v4, p0, Lcom/navdy/client/app/framework/map/MapUtils$1;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v0}, Landroid/location/Address;->getPostalCode()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/client/app/framework/models/Destination;->setZipCode(Ljava/lang/String;)V

    .line 388
    iget-object v4, p0, Lcom/navdy/client/app/framework/map/MapUtils$1;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v0}, Landroid/location/Address;->getAdminArea()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/client/app/framework/models/Destination;->setState(Ljava/lang/String;)V

    .line 389
    iget-object v4, p0, Lcom/navdy/client/app/framework/map/MapUtils$1;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v0}, Landroid/location/Address;->getCountryName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/client/app/framework/models/Destination;->setCountry(Ljava/lang/String;)V

    .line 390
    iget-object v4, p0, Lcom/navdy/client/app/framework/map/MapUtils$1;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v0}, Landroid/location/Address;->getCountryCode()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/client/app/framework/models/Destination;->setCountryCode(Ljava/lang/String;)V

    .line 391
    iget-object v4, p0, Lcom/navdy/client/app/framework/map/MapUtils$1;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v0, v6}, Landroid/location/Address;->getAddressLine(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/client/app/framework/models/Destination;->setName(Ljava/lang/String;)V

    .line 393
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 394
    .local v1, "addressFragments":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v3, 0x0

    .line 395
    .local v3, "i":I
    :goto_0
    invoke-virtual {v0}, Landroid/location/Address;->getMaxAddressLineIndex()I

    move-result v4

    if-ge v3, v4, :cond_0

    .line 396
    invoke-virtual {v0, v3}, Landroid/location/Address;->getAddressLine(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 395
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 399
    :cond_0
    const-string v4, "line.separator"

    .line 400
    invoke-static {v4}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 399
    invoke-static {v4, v1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    .line 403
    .local v2, "addressString":Ljava/lang/String;
    invoke-static {}, Lcom/navdy/client/app/framework/map/MapUtils;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Full address = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 404
    iget-object v4, p0, Lcom/navdy/client/app/framework/map/MapUtils$1;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v4, v2}, Lcom/navdy/client/app/framework/models/Destination;->setRawAddressNotForDisplay(Ljava/lang/String;)V

    .line 406
    .end local v0    # "address":Landroid/location/Address;
    .end local v1    # "addressFragments":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v2    # "addressString":Ljava/lang/String;
    .end local v3    # "i":I
    :cond_1
    iget-object v4, p0, Lcom/navdy/client/app/framework/map/MapUtils$1;->val$callback:Ljava/lang/Runnable;

    invoke-interface {v4}, Ljava/lang/Runnable;->run()V

    .line 407
    return-void
.end method

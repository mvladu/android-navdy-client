.class Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$4$1;
.super Ljava/lang/Object;
.source "NavCoordsAddressProcessor.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$4;->onSuccess(Lcom/navdy/service/library/events/location/Coordinate;Lcom/navdy/service/library/events/location/Coordinate;Lcom/here/android/mpa/search/Address;Lcom/navdy/client/app/framework/models/Destination$Precision;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$4;

.field final synthetic val$address:Lcom/here/android/mpa/search/Address;

.field final synthetic val$displayCoords:Lcom/navdy/service/library/events/location/Coordinate;

.field final synthetic val$navCoords:Lcom/navdy/service/library/events/location/Coordinate;

.field final synthetic val$precisionLevel:Lcom/navdy/client/app/framework/models/Destination$Precision;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$4;Lcom/navdy/service/library/events/location/Coordinate;Lcom/navdy/service/library/events/location/Coordinate;Lcom/here/android/mpa/search/Address;Lcom/navdy/client/app/framework/models/Destination$Precision;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$4;

    .prologue
    .line 293
    iput-object p1, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$4$1;->this$0:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$4;

    iput-object p2, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$4$1;->val$navCoords:Lcom/navdy/service/library/events/location/Coordinate;

    iput-object p3, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$4$1;->val$displayCoords:Lcom/navdy/service/library/events/location/Coordinate;

    iput-object p4, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$4$1;->val$address:Lcom/here/android/mpa/search/Address;

    iput-object p5, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$4$1;->val$precisionLevel:Lcom/navdy/client/app/framework/models/Destination$Precision;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 296
    new-instance v0, Lcom/google/android/gms/maps/model/LatLng;

    iget-object v1, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$4$1;->val$navCoords:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v1, v1, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    iget-object v1, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$4$1;->val$navCoords:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v1, v1, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    iget-object v1, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$4$1;->this$0:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$4;

    iget-object v1, v1, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$4;->val$processedDestination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->access$700(Lcom/google/android/gms/maps/model/LatLng;Lcom/navdy/client/app/framework/models/Destination;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 297
    invoke-static {}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "callGoogleDirectionsWebApi returned good navCoords: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$4$1;->val$navCoords:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v2, v2, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$4$1;->val$navCoords:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v2, v2, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; saving and calling back success"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 300
    iget-object v0, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$4$1;->this$0:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$4;

    iget-object v0, v0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$4;->val$processedDestination:Lcom/navdy/client/app/framework/models/Destination;

    iget-object v1, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$4$1;->val$displayCoords:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v2, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$4$1;->val$navCoords:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v3, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$4$1;->val$address:Lcom/here/android/mpa/search/Address;

    iget-object v4, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$4$1;->val$precisionLevel:Lcom/navdy/client/app/framework/models/Destination$Precision;

    iget-object v5, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$4$1;->this$0:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$4;

    iget-object v5, v5, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$4;->val$callback:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;

    invoke-static/range {v0 .. v5}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->access$300(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/service/library/events/location/Coordinate;Lcom/navdy/service/library/events/location/Coordinate;Lcom/here/android/mpa/search/Address;Lcom/navdy/client/app/framework/models/Destination$Precision;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;)V

    .line 307
    :goto_0
    return-void

    .line 302
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "callGoogleDirectionsWebApi returned bad navCoords: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$4$1;->val$navCoords:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v2, v2, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$4$1;->val$navCoords:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v2, v2, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; can\'t do anything else, saving and calling back success"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 305
    iget-object v0, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$4$1;->this$0:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$4;

    iget-object v0, v0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$4;->val$processedDestination:Lcom/navdy/client/app/framework/models/Destination;

    iget-object v1, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$4$1;->val$displayCoords:Lcom/navdy/service/library/events/location/Coordinate;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$4$1;->val$address:Lcom/here/android/mpa/search/Address;

    iget-object v4, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$4$1;->val$precisionLevel:Lcom/navdy/client/app/framework/models/Destination$Precision;

    iget-object v5, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$4$1;->this$0:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$4;

    iget-object v5, v5, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$4;->val$callback:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;

    invoke-static/range {v0 .. v5}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->access$300(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/service/library/events/location/Coordinate;Lcom/navdy/service/library/events/location/Coordinate;Lcom/here/android/mpa/search/Address;Lcom/navdy/client/app/framework/models/Destination$Precision;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;)V

    goto :goto_0
.end method

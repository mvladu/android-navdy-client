.class public final enum Lcom/navdy/client/app/framework/models/Destination$SearchType;
.super Ljava/lang/Enum;
.source "Destination.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/models/Destination;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SearchType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/client/app/framework/models/Destination$SearchType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/client/app/framework/models/Destination$SearchType;

.field public static final enum AUTOCOMPLETE:Lcom/navdy/client/app/framework/models/Destination$SearchType;

.field public static final enum CONTACT:Lcom/navdy/client/app/framework/models/Destination$SearchType;

.field public static final enum CONTACT_ADDRESS_SELECTION:Lcom/navdy/client/app/framework/models/Destination$SearchType;

.field public static final enum DETAILS:Lcom/navdy/client/app/framework/models/Destination$SearchType;

.field public static final enum FOOTER:Lcom/navdy/client/app/framework/models/Destination$SearchType;

.field public static final enum MORE:Lcom/navdy/client/app/framework/models/Destination$SearchType;

.field public static final enum NO_RESULTS:Lcom/navdy/client/app/framework/models/Destination$SearchType;

.field public static final enum RECENT_PLACES:Lcom/navdy/client/app/framework/models/Destination$SearchType;

.field public static final enum SEARCH_HISTORY:Lcom/navdy/client/app/framework/models/Destination$SearchType;

.field public static final enum SEARCH_POWERED_BY_GOOGLE_HEADER:Lcom/navdy/client/app/framework/models/Destination$SearchType;

.field public static final enum SERVICES_SEARCH:Lcom/navdy/client/app/framework/models/Destination$SearchType;

.field public static final enum TEXT_SEARCH:Lcom/navdy/client/app/framework/models/Destination$SearchType;

.field public static final enum UNKNOWN:Lcom/navdy/client/app/framework/models/Destination$SearchType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 293
    new-instance v0, Lcom/navdy/client/app/framework/models/Destination$SearchType;

    const-string v1, "SEARCH_POWERED_BY_GOOGLE_HEADER"

    invoke-direct {v0, v1, v3}, Lcom/navdy/client/app/framework/models/Destination$SearchType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/models/Destination$SearchType;->SEARCH_POWERED_BY_GOOGLE_HEADER:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    .line 294
    new-instance v0, Lcom/navdy/client/app/framework/models/Destination$SearchType;

    const-string v1, "SERVICES_SEARCH"

    invoke-direct {v0, v1, v4}, Lcom/navdy/client/app/framework/models/Destination$SearchType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/models/Destination$SearchType;->SERVICES_SEARCH:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    .line 295
    new-instance v0, Lcom/navdy/client/app/framework/models/Destination$SearchType;

    const-string v1, "SEARCH_HISTORY"

    invoke-direct {v0, v1, v5}, Lcom/navdy/client/app/framework/models/Destination$SearchType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/models/Destination$SearchType;->SEARCH_HISTORY:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    .line 296
    new-instance v0, Lcom/navdy/client/app/framework/models/Destination$SearchType;

    const-string v1, "AUTOCOMPLETE"

    invoke-direct {v0, v1, v6}, Lcom/navdy/client/app/framework/models/Destination$SearchType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/models/Destination$SearchType;->AUTOCOMPLETE:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    .line 297
    new-instance v0, Lcom/navdy/client/app/framework/models/Destination$SearchType;

    const-string v1, "CONTACT"

    invoke-direct {v0, v1, v7}, Lcom/navdy/client/app/framework/models/Destination$SearchType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/models/Destination$SearchType;->CONTACT:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    .line 298
    new-instance v0, Lcom/navdy/client/app/framework/models/Destination$SearchType;

    const-string v1, "CONTACT_ADDRESS_SELECTION"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/models/Destination$SearchType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/models/Destination$SearchType;->CONTACT_ADDRESS_SELECTION:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    .line 299
    new-instance v0, Lcom/navdy/client/app/framework/models/Destination$SearchType;

    const-string v1, "TEXT_SEARCH"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/models/Destination$SearchType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/models/Destination$SearchType;->TEXT_SEARCH:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    .line 300
    new-instance v0, Lcom/navdy/client/app/framework/models/Destination$SearchType;

    const-string v1, "RECENT_PLACES"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/models/Destination$SearchType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/models/Destination$SearchType;->RECENT_PLACES:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    .line 301
    new-instance v0, Lcom/navdy/client/app/framework/models/Destination$SearchType;

    const-string v1, "DETAILS"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/models/Destination$SearchType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/models/Destination$SearchType;->DETAILS:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    .line 302
    new-instance v0, Lcom/navdy/client/app/framework/models/Destination$SearchType;

    const-string v1, "MORE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/models/Destination$SearchType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/models/Destination$SearchType;->MORE:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    .line 303
    new-instance v0, Lcom/navdy/client/app/framework/models/Destination$SearchType;

    const-string v1, "NO_RESULTS"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/models/Destination$SearchType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/models/Destination$SearchType;->NO_RESULTS:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    .line 304
    new-instance v0, Lcom/navdy/client/app/framework/models/Destination$SearchType;

    const-string v1, "UNKNOWN"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/models/Destination$SearchType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/models/Destination$SearchType;->UNKNOWN:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    .line 305
    new-instance v0, Lcom/navdy/client/app/framework/models/Destination$SearchType;

    const-string v1, "FOOTER"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/models/Destination$SearchType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/models/Destination$SearchType;->FOOTER:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    .line 292
    const/16 v0, 0xd

    new-array v0, v0, [Lcom/navdy/client/app/framework/models/Destination$SearchType;

    sget-object v1, Lcom/navdy/client/app/framework/models/Destination$SearchType;->SEARCH_POWERED_BY_GOOGLE_HEADER:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/client/app/framework/models/Destination$SearchType;->SERVICES_SEARCH:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/client/app/framework/models/Destination$SearchType;->SEARCH_HISTORY:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/client/app/framework/models/Destination$SearchType;->AUTOCOMPLETE:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/client/app/framework/models/Destination$SearchType;->CONTACT:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/navdy/client/app/framework/models/Destination$SearchType;->CONTACT_ADDRESS_SELECTION:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/navdy/client/app/framework/models/Destination$SearchType;->TEXT_SEARCH:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/navdy/client/app/framework/models/Destination$SearchType;->RECENT_PLACES:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/navdy/client/app/framework/models/Destination$SearchType;->DETAILS:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/navdy/client/app/framework/models/Destination$SearchType;->MORE:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/navdy/client/app/framework/models/Destination$SearchType;->NO_RESULTS:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/navdy/client/app/framework/models/Destination$SearchType;->UNKNOWN:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/navdy/client/app/framework/models/Destination$SearchType;->FOOTER:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/client/app/framework/models/Destination$SearchType;->$VALUES:[Lcom/navdy/client/app/framework/models/Destination$SearchType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 292
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/client/app/framework/models/Destination$SearchType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 292
    const-class v0, Lcom/navdy/client/app/framework/models/Destination$SearchType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/models/Destination$SearchType;

    return-object v0
.end method

.method public static values()[Lcom/navdy/client/app/framework/models/Destination$SearchType;
    .locals 1

    .prologue
    .line 292
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$SearchType;->$VALUES:[Lcom/navdy/client/app/framework/models/Destination$SearchType;

    invoke-virtual {v0}, [Lcom/navdy/client/app/framework/models/Destination$SearchType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/client/app/framework/models/Destination$SearchType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 308
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination$SearchType;->ordinal()I

    move-result v0

    return v0
.end method

.class public Lcom/navdy/client/app/framework/models/CalendarEvent;
.super Ljava/lang/Object;
.source "CalendarEvent.java"


# instance fields
.field public allDay:Z

.field public calendarId:J

.field public destination:Lcom/navdy/service/library/events/destination/Destination;

.field public displayColor:J

.field public displayName:Ljava/lang/String;

.field public endTimestamp:J

.field public latLng:Lcom/google/android/gms/maps/model/LatLng;

.field public location:Ljava/lang/String;

.field public startTimestamp:J


# direct methods
.method public constructor <init>(Ljava/lang/String;JJJLjava/lang/String;JZ)V
    .locals 4
    .param p1, "displayName"    # Ljava/lang/String;
    .param p2, "calendarId"    # J
    .param p4, "startTimestamp"    # J
    .param p6, "endTimestamp"    # J
    .param p8, "location"    # Ljava/lang/String;
    .param p9, "displayColor"    # J
    .param p11, "allDay"    # Z

    .prologue
    const-wide/16 v2, 0x0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    const-string v0, ""

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/CalendarEvent;->displayName:Ljava/lang/String;

    .line 14
    iput-wide v2, p0, Lcom/navdy/client/app/framework/models/CalendarEvent;->calendarId:J

    .line 15
    iput-wide v2, p0, Lcom/navdy/client/app/framework/models/CalendarEvent;->startTimestamp:J

    .line 16
    iput-wide v2, p0, Lcom/navdy/client/app/framework/models/CalendarEvent;->endTimestamp:J

    .line 17
    iput-wide v2, p0, Lcom/navdy/client/app/framework/models/CalendarEvent;->displayColor:J

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/CalendarEvent;->location:Ljava/lang/String;

    .line 20
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/client/app/framework/models/CalendarEvent;->allDay:Z

    .line 25
    iput-object p1, p0, Lcom/navdy/client/app/framework/models/CalendarEvent;->displayName:Ljava/lang/String;

    .line 26
    iput-wide p2, p0, Lcom/navdy/client/app/framework/models/CalendarEvent;->calendarId:J

    .line 27
    iput-wide p4, p0, Lcom/navdy/client/app/framework/models/CalendarEvent;->startTimestamp:J

    .line 28
    iput-object p8, p0, Lcom/navdy/client/app/framework/models/CalendarEvent;->location:Ljava/lang/String;

    .line 29
    iput-wide p6, p0, Lcom/navdy/client/app/framework/models/CalendarEvent;->endTimestamp:J

    .line 30
    iput-wide p9, p0, Lcom/navdy/client/app/framework/models/CalendarEvent;->displayColor:J

    .line 31
    iput-boolean p11, p0, Lcom/navdy/client/app/framework/models/CalendarEvent;->allDay:Z

    .line 32
    return-void
.end method


# virtual methods
.method public getDateAndTime()Ljava/lang/String;
    .locals 2

    .prologue
    .line 69
    iget-wide v0, p0, Lcom/navdy/client/app/framework/models/CalendarEvent;->startTimestamp:J

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/homescreen/CalendarUtils;->getDateAndTime(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEndTime()Ljava/lang/String;
    .locals 2

    .prologue
    .line 87
    iget-wide v0, p0, Lcom/navdy/client/app/framework/models/CalendarEvent;->endTimestamp:J

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/homescreen/CalendarUtils;->getTime(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getStartTime()Ljava/lang/String;
    .locals 2

    .prologue
    .line 78
    iget-wide v0, p0, Lcom/navdy/client/app/framework/models/CalendarEvent;->startTimestamp:J

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/homescreen/CalendarUtils;->getTime(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setDestination(Lcom/navdy/service/library/events/destination/Destination;)V
    .locals 0
    .param p1, "destination"    # Lcom/navdy/service/library/events/destination/Destination;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/navdy/client/app/framework/models/CalendarEvent;->destination:Lcom/navdy/service/library/events/destination/Destination;

    .line 61
    return-void
.end method

.method public toProtobufObject()Lcom/navdy/service/library/events/calendars/CalendarEvent;
    .locals 10

    .prologue
    .line 55
    new-instance v0, Lcom/navdy/service/library/events/calendars/CalendarEvent;

    iget-object v1, p0, Lcom/navdy/client/app/framework/models/CalendarEvent;->displayName:Ljava/lang/String;

    iget-wide v2, p0, Lcom/navdy/client/app/framework/models/CalendarEvent;->startTimestamp:J

    .line 56
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iget-wide v4, p0, Lcom/navdy/client/app/framework/models/CalendarEvent;->endTimestamp:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iget-boolean v4, p0, Lcom/navdy/client/app/framework/models/CalendarEvent;->allDay:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iget-wide v6, p0, Lcom/navdy/client/app/framework/models/CalendarEvent;->displayColor:J

    long-to-int v5, v6

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/client/app/framework/models/CalendarEvent;->location:Ljava/lang/String;

    iget-wide v8, p0, Lcom/navdy/client/app/framework/models/CalendarEvent;->calendarId:J

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/navdy/client/app/framework/models/CalendarEvent;->destination:Lcom/navdy/service/library/events/destination/Destination;

    invoke-direct/range {v0 .. v8}, Lcom/navdy/service/library/events/calendars/CalendarEvent;-><init>(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/events/destination/Destination;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 36
    const-string v0, "display_name: %s, \tcalendarId: %s\tlocation: %s\tstart_time: %s\tend_time: %s\tdisplay_color: %s\tall_day: %s"

    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/navdy/client/app/framework/models/CalendarEvent;->displayName:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-wide v4, p0, Lcom/navdy/client/app/framework/models/CalendarEvent;->calendarId:J

    .line 45
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/navdy/client/app/framework/models/CalendarEvent;->location:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-wide v4, p0, Lcom/navdy/client/app/framework/models/CalendarEvent;->startTimestamp:J

    .line 47
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget-wide v4, p0, Lcom/navdy/client/app/framework/models/CalendarEvent;->endTimestamp:J

    .line 48
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x5

    iget-wide v4, p0, Lcom/navdy/client/app/framework/models/CalendarEvent;->displayColor:J

    .line 49
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x6

    iget-boolean v3, p0, Lcom/navdy/client/app/framework/models/CalendarEvent;->allDay:Z

    .line 50
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    .line 36
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

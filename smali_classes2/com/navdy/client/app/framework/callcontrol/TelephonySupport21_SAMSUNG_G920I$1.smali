.class Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21_SAMSUNG_G920I$1;
.super Ljava/lang/Object;
.source "TelephonySupport21_SAMSUNG_G920I.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21_SAMSUNG_G920I;->accept(Landroid/media/session/MediaController;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21_SAMSUNG_G920I;

.field final synthetic val$m:Landroid/media/session/MediaController;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21_SAMSUNG_G920I;Landroid/media/session/MediaController;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21_SAMSUNG_G920I;

    .prologue
    .line 26
    iput-object p1, p0, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21_SAMSUNG_G920I$1;->this$0:Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21_SAMSUNG_G920I;

    iput-object p2, p0, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21_SAMSUNG_G920I$1;->val$m:Landroid/media/session/MediaController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 30
    :try_start_0
    new-instance v0, Landroid/view/KeyEvent;

    const/4 v2, 0x0

    const/16 v3, 0x4f

    invoke-direct {v0, v2, v3}, Landroid/view/KeyEvent;-><init>(II)V

    .line 31
    .local v0, "event":Landroid/view/KeyEvent;
    iget-object v2, p0, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21_SAMSUNG_G920I$1;->val$m:Landroid/media/session/MediaController;

    invoke-virtual {v2, v0}, Landroid/media/session/MediaController;->dispatchMediaButtonEvent(Landroid/view/KeyEvent;)Z

    .line 32
    invoke-static {}, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21_SAMSUNG_G920I;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "HEADSETHOOK keydown sent:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21_SAMSUNG_G920I$1;->val$m:Landroid/media/session/MediaController;

    invoke-virtual {v4}, Landroid/media/session/MediaController;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 34
    new-instance v0, Landroid/view/KeyEvent;

    .end local v0    # "event":Landroid/view/KeyEvent;
    const/4 v2, 0x1

    const/16 v3, 0x4f

    invoke-direct {v0, v2, v3}, Landroid/view/KeyEvent;-><init>(II)V

    .line 35
    .restart local v0    # "event":Landroid/view/KeyEvent;
    iget-object v2, p0, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21_SAMSUNG_G920I$1;->val$m:Landroid/media/session/MediaController;

    invoke-virtual {v2, v0}, Landroid/media/session/MediaController;->dispatchMediaButtonEvent(Landroid/view/KeyEvent;)Z

    .line 36
    invoke-static {}, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21_SAMSUNG_G920I;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "HEADSETHOOK keyup sent:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21_SAMSUNG_G920I$1;->val$m:Landroid/media/session/MediaController;

    invoke-virtual {v4}, Landroid/media/session/MediaController;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 40
    .end local v0    # "event":Landroid/view/KeyEvent;
    :goto_0
    return-void

    .line 37
    :catch_0
    move-exception v1

    .line 38
    .local v1, "t":Ljava/lang/Throwable;
    invoke-static {}, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21_SAMSUNG_G920I;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

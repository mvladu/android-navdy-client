.class public Lcom/navdy/client/app/framework/callcontrol/TelephonySupport26;
.super Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21;
.source "TelephonySupport26.java"


# static fields
.field private static sLogger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 16
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport26;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport26;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21;-><init>(Landroid/content/Context;)V

    .line 20
    return-void
.end method

.method private acceptRingingCallFallback()V
    .locals 0

    .prologue
    .line 43
    invoke-super {p0}, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21;->acceptRingingCall()V

    .line 44
    return-void
.end method

.method private callTelephonyManagerMethod(Ljava/lang/String;)Z
    .locals 5
    .param p1, "methodName"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 56
    iget-object v3, p0, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport26;->context:Landroid/content/Context;

    const-string v4, "phone"

    .line 57
    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 59
    .local v1, "tm":Landroid/telephony/TelephonyManager;
    if-nez v1, :cond_0

    .line 60
    sget-object v3, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport26;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Unable to get the Telephony Manager."

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 80
    :goto_0
    return v2

    .line 66
    :cond_0
    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v3, p1, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v3, v1, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 67
    const/4 v2, 0x1

    goto :goto_0

    .line 68
    :catch_0
    move-exception v0

    .line 79
    .local v0, "e":Ljava/lang/Exception;
    sget-object v3, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport26;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Unable to use the Telephony Manager directly."

    invoke-virtual {v3, v4, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public acceptRingingCall()V
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0x1a
    .end annotation

    .prologue
    .line 25
    iget-object v2, p0, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport26;->context:Landroid/content/Context;

    const-string v3, "telecom"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telecom/TelecomManager;

    .line 26
    .local v1, "telecomManager":Landroid/telecom/TelecomManager;
    if-nez v1, :cond_0

    .line 27
    sget-object v2, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport26;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Unable to handleCallAction. telecomManager is null"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 28
    invoke-direct {p0}, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport26;->acceptRingingCallFallback()V

    .line 40
    :goto_0
    return-void

    .line 35
    :cond_0
    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "acceptRingingCall"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v2, v1, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 36
    :catch_0
    move-exception v0

    .line 37
    .local v0, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport26;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Unable to use the Telecom Manager directly."

    invoke-virtual {v2, v3, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 38
    invoke-direct {p0}, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport26;->acceptRingingCallFallback()V

    goto :goto_0
.end method

.method public endCall()V
    .locals 2

    .prologue
    .line 48
    const-string v1, "endCall"

    invoke-direct {p0, v1}, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport26;->callTelephonyManagerMethod(Ljava/lang/String;)Z

    move-result v0

    .line 49
    .local v0, "success":Z
    if-nez v0, :cond_0

    .line 51
    invoke-super {p0}, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21;->endCall()V

    .line 53
    :cond_0
    return-void
.end method

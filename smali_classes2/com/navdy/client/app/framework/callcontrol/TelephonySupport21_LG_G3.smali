.class public Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21_LG_G3;
.super Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21;
.source "TelephonySupport21_LG_G3.java"


# static fields
.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 18
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21_LG_G3;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21_LG_G3;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21;-><init>(Landroid/content/Context;)V

    .line 22
    return-void
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21_LG_G3;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method


# virtual methods
.method protected accept(Landroid/media/session/MediaController;)V
    .locals 3
    .param p1, "m"    # Landroid/media/session/MediaController;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 27
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21_LG_G3$1;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21_LG_G3$1;-><init>(Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21_LG_G3;Landroid/media/session/MediaController;)V

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 43
    return-void
.end method

.method protected end(Landroid/media/session/MediaController;)V
    .locals 0
    .param p1, "m"    # Landroid/media/session/MediaController;

    .prologue
    .line 68
    invoke-virtual {p0, p1}, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21_LG_G3;->accept(Landroid/media/session/MediaController;)V

    .line 69
    return-void
.end method

.method protected reject(Landroid/media/session/MediaController;)V
    .locals 3
    .param p1, "m"    # Landroid/media/session/MediaController;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 47
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21_LG_G3$2;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21_LG_G3$2;-><init>(Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21_LG_G3;Landroid/media/session/MediaController;)V

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 65
    return-void
.end method

.class public Lcom/navdy/client/app/framework/servicehandler/CalendarHandler;
.super Ljava/lang/Object;
.source "CalendarHandler.java"


# static fields
.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private calendarObserver:Lcom/navdy/client/app/framework/util/CalendarObserver;

.field private context:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 17
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/servicehandler/CalendarHandler;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/servicehandler/CalendarHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    new-instance v0, Lcom/navdy/client/app/framework/util/CalendarObserver;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, v1}, Lcom/navdy/client/app/framework/util/CalendarObserver;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/CalendarHandler;->calendarObserver:Lcom/navdy/client/app/framework/util/CalendarObserver;

    .line 23
    iput-object p1, p0, Lcom/navdy/client/app/framework/servicehandler/CalendarHandler;->context:Landroid/content/Context;

    .line 24
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/servicehandler/CalendarHandler;->registerObserver()V

    .line 25
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/framework/servicehandler/CalendarHandler;)Lcom/navdy/client/app/framework/util/CalendarObserver;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/servicehandler/CalendarHandler;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/CalendarHandler;->calendarObserver:Lcom/navdy/client/app/framework/util/CalendarObserver;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/client/app/framework/servicehandler/CalendarHandler;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/servicehandler/CalendarHandler;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/CalendarHandler;->context:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public getCalendarObserver()Lcom/navdy/client/app/framework/util/CalendarObserver;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/CalendarHandler;->calendarObserver:Lcom/navdy/client/app/framework/util/CalendarObserver;

    return-object v0
.end method

.method public registerObserver()V
    .locals 3

    .prologue
    .line 28
    sget-object v0, Lcom/navdy/client/app/framework/servicehandler/CalendarHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "registerObserver"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 29
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/framework/servicehandler/CalendarHandler$1;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/framework/servicehandler/CalendarHandler$1;-><init>(Lcom/navdy/client/app/framework/servicehandler/CalendarHandler;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 39
    return-void
.end method

.method public unregisterObserver()V
    .locals 2

    .prologue
    .line 42
    sget-object v0, Lcom/navdy/client/app/framework/servicehandler/CalendarHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "unregisterObserver"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 43
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/CalendarHandler;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/framework/servicehandler/CalendarHandler;->calendarObserver:Lcom/navdy/client/app/framework/util/CalendarObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 44
    return-void
.end method

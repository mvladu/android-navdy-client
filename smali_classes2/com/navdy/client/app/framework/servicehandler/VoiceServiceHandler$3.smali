.class Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$3;
.super Ljava/lang/Object;
.source "VoiceServiceHandler.java"

# interfaces
.implements Lcom/navdy/client/app/framework/search/NavdySearch$SearchCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->handleVoiceCommand(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

.field final synthetic val$prefix:Ljava/lang/String;

.field final synthetic val$query:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    .prologue
    .line 664
    iput-object p1, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$3;->this$0:Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    iput-object p2, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$3;->val$query:Ljava/lang/String;

    iput-object p3, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$3;->val$prefix:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSearchCompleted(Lcom/navdy/client/app/framework/search/SearchResults;)V
    .locals 4
    .param p1, "searchResults"    # Lcom/navdy/client/app/framework/search/SearchResults;

    .prologue
    .line 676
    invoke-static {p1}, Lcom/navdy/client/app/framework/search/SearchResults;->isEmpty(Lcom/navdy/client/app/framework/search/SearchResults;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 677
    iget-object v1, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$3;->this$0:Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    invoke-virtual {p1}, Lcom/navdy/client/app/framework/search/SearchResults;->getQuery()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->access$1500(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;Ljava/lang/String;)V

    .line 682
    :goto_0
    return-void

    .line 679
    :cond_0
    invoke-virtual {p1}, Lcom/navdy/client/app/framework/search/SearchResults;->getDestinationsList()Ljava/util/ArrayList;

    move-result-object v0

    .line 680
    .local v0, "destinations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Destination;>;"
    iget-object v1, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$3;->this$0:Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    invoke-virtual {p1}, Lcom/navdy/client/app/framework/search/SearchResults;->getQuery()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$3;->val$prefix:Ljava/lang/String;

    invoke-static {v1, v0, v2, v3}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->access$1600(Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onSearchStarted()V
    .locals 3

    .prologue
    .line 668
    new-instance v0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;

    invoke-direct {v0}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;-><init>()V

    .line 669
    .local v0, "responseBuilder":Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;
    sget-object v1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;->VOICE_SEARCH_SEARCHING:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->state(Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;)Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler$3;->val$query:Ljava/lang/String;

    .line 670
    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->recognizedWords(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;

    .line 671
    invoke-virtual {v0}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->build()Lcom/navdy/service/library/events/audio/VoiceSearchResponse;

    move-result-object v1

    invoke-static {v1}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 672
    return-void
.end method

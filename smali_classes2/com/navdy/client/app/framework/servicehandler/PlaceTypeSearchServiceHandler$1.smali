.class Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler$1;
.super Ljava/lang/Object;
.source "PlaceTypeSearchServiceHandler.java"

# interfaces
.implements Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler;->onDestinationSelectedRequest(Lcom/navdy/service/library/events/places/DestinationSelectedRequest;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler;

.field final synthetic val$destinationSelectedRequest:Lcom/navdy/service/library/events/places/DestinationSelectedRequest;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler;Lcom/navdy/service/library/events/places/DestinationSelectedRequest;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler$1;->this$0:Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler;

    iput-object p2, p0, Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler$1;->val$destinationSelectedRequest:Lcom/navdy/service/library/events/places/DestinationSelectedRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private reply(Lcom/navdy/service/library/events/RequestStatus;Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 4
    .param p1, "status"    # Lcom/navdy/service/library/events/RequestStatus;
    .param p2, "destination"    # Lcom/navdy/client/app/framework/models/Destination;

    .prologue
    .line 111
    invoke-virtual {p2}, Lcom/navdy/client/app/framework/models/Destination;->toProtobufDestinationObject()Lcom/navdy/service/library/events/destination/Destination;

    move-result-object v1

    .line 113
    .local v1, "updatedDestination":Lcom/navdy/service/library/events/destination/Destination;
    iget-object v2, p0, Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler$1;->this$0:Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler;

    iget-object v3, p0, Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler$1;->val$destinationSelectedRequest:Lcom/navdy/service/library/events/places/DestinationSelectedRequest;

    iget-object v3, v3, Lcom/navdy/service/library/events/places/DestinationSelectedRequest;->destination:Lcom/navdy/service/library/events/destination/Destination;

    invoke-static {v2, v3, v1}, Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler;->access$000(Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler;Lcom/navdy/service/library/events/destination/Destination;Lcom/navdy/service/library/events/destination/Destination;)Lcom/navdy/service/library/events/destination/Destination;

    move-result-object v1

    .line 115
    new-instance v2, Lcom/navdy/service/library/events/places/DestinationSelectedResponse$Builder;

    invoke-direct {v2}, Lcom/navdy/service/library/events/places/DestinationSelectedResponse$Builder;-><init>()V

    iget-object v3, p0, Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler$1;->val$destinationSelectedRequest:Lcom/navdy/service/library/events/places/DestinationSelectedRequest;

    iget-object v3, v3, Lcom/navdy/service/library/events/places/DestinationSelectedRequest;->request_id:Ljava/lang/String;

    .line 116
    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/places/DestinationSelectedResponse$Builder;->request_id(Ljava/lang/String;)Lcom/navdy/service/library/events/places/DestinationSelectedResponse$Builder;

    move-result-object v2

    .line 117
    invoke-virtual {v2, p1}, Lcom/navdy/service/library/events/places/DestinationSelectedResponse$Builder;->request_status(Lcom/navdy/service/library/events/RequestStatus;)Lcom/navdy/service/library/events/places/DestinationSelectedResponse$Builder;

    move-result-object v2

    .line 118
    invoke-virtual {v2, v1}, Lcom/navdy/service/library/events/places/DestinationSelectedResponse$Builder;->destination(Lcom/navdy/service/library/events/destination/Destination;)Lcom/navdy/service/library/events/places/DestinationSelectedResponse$Builder;

    move-result-object v2

    .line 119
    invoke-virtual {v2}, Lcom/navdy/service/library/events/places/DestinationSelectedResponse$Builder;->build()Lcom/navdy/service/library/events/places/DestinationSelectedResponse;

    move-result-object v0

    .line 121
    .local v0, "destinationSelectedResponse":Lcom/navdy/service/library/events/places/DestinationSelectedResponse;
    invoke-static {v0}, Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler;->access$100(Lcom/navdy/service/library/events/places/DestinationSelectedResponse;)V

    .line 122
    invoke-static {v0}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 123
    return-void
.end method


# virtual methods
.method public onFailure(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;)V
    .locals 1
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;
    .param p2, "error"    # Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;

    .prologue
    .line 105
    sget-object v0, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SERVICE_ERROR:Lcom/navdy/service/library/events/RequestStatus;

    invoke-direct {p0, v0, p1}, Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler$1;->reply(Lcom/navdy/service/library/events/RequestStatus;Lcom/navdy/client/app/framework/models/Destination;)V

    .line 106
    return-void
.end method

.method public onSuccess(Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 1
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;

    .prologue
    .line 100
    sget-object v0, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    invoke-direct {p0, v0, p1}, Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler$1;->reply(Lcom/navdy/service/library/events/RequestStatus;Lcom/navdy/client/app/framework/models/Destination;)V

    .line 101
    return-void
.end method

.class Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$GpmPlaylistObserver;
.super Landroid/database/ContentObserver;
.source "MusicServiceHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GpmPlaylistObserver"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$GpmPlaylistObserver$GpmObserverRunnable;
    }
.end annotation


# instance fields
.field pattern:Ljava/util/regex/Pattern;

.field runnable:Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$GpmPlaylistObserver$GpmObserverRunnable;

.field final synthetic this$0:Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;)V
    .locals 2

    .prologue
    .line 913
    iput-object p1, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$GpmPlaylistObserver;->this$0:Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;

    .line 914
    invoke-static {p1}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->access$500(Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;)Landroid/os/Handler;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 910
    new-instance v0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$GpmPlaylistObserver$GpmObserverRunnable;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$GpmPlaylistObserver$GpmObserverRunnable;-><init>(Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$GpmPlaylistObserver;Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$1;)V

    iput-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$GpmPlaylistObserver;->runnable:Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$GpmPlaylistObserver$GpmObserverRunnable;

    .line 911
    const-string v0, "/playlists/(\\d+)/?.*"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$GpmPlaylistObserver;->pattern:Ljava/util/regex/Pattern;

    .line 915
    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 10
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    const-wide/16 v8, 0xc8

    .line 942
    invoke-virtual {p2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    .line 943
    .local v2, "path":Ljava/lang/String;
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onChange: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 945
    invoke-static {v2}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 948
    iget-object v5, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$GpmPlaylistObserver;->this$0:Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;

    invoke-static {v5}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->access$500(Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;)Landroid/os/Handler;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$GpmPlaylistObserver;->runnable:Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$GpmPlaylistObserver$GpmObserverRunnable;

    invoke-virtual {v5, v6}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 949
    iget-object v5, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$GpmPlaylistObserver;->runnable:Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$GpmPlaylistObserver$GpmObserverRunnable;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$GpmPlaylistObserver$GpmObserverRunnable;->setPlaylistId(Ljava/lang/Integer;)V

    .line 950
    iget-object v5, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$GpmPlaylistObserver;->this$0:Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;

    invoke-static {v5}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->access$500(Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;)Landroid/os/Handler;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$GpmPlaylistObserver;->runnable:Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$GpmPlaylistObserver$GpmObserverRunnable;

    invoke-virtual {v5, v6, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 968
    :cond_0
    :goto_0
    return-void

    .line 952
    :cond_1
    iget-object v5, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$GpmPlaylistObserver;->pattern:Ljava/util/regex/Pattern;

    invoke-virtual {v5, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 953
    .local v1, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 954
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Match: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 955
    const/4 v5, 0x1

    invoke-virtual {v1, v5}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v4

    .line 958
    .local v4, "playlistIdStr":Ljava/lang/String;
    :try_start_0
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 959
    .local v3, "playlistId":I
    iget-object v5, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$GpmPlaylistObserver;->this$0:Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;

    invoke-static {v5}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->access$500(Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;)Landroid/os/Handler;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$GpmPlaylistObserver;->runnable:Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$GpmPlaylistObserver$GpmObserverRunnable;

    invoke-virtual {v5, v6}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 960
    iget-object v5, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$GpmPlaylistObserver;->runnable:Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$GpmPlaylistObserver$GpmObserverRunnable;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$GpmPlaylistObserver$GpmObserverRunnable;->setPlaylistId(Ljava/lang/Integer;)V

    .line 961
    iget-object v5, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$GpmPlaylistObserver;->this$0:Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;

    invoke-static {v5}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->access$500(Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;)Landroid/os/Handler;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$GpmPlaylistObserver;->runnable:Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$GpmPlaylistObserver$GpmObserverRunnable;

    const-wide/16 v8, 0xc8

    invoke-virtual {v5, v6, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 962
    .end local v3    # "playlistId":I
    :catch_0
    move-exception v0

    .line 963
    .local v0, "e":Ljava/lang/NumberFormatException;
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Can\'t convert to integer: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

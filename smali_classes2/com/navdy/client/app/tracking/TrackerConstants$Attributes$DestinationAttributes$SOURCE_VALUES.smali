.class public Lcom/navdy/client/app/tracking/TrackerConstants$Attributes$DestinationAttributes$SOURCE_VALUES;
.super Ljava/lang/Object;
.source "TrackerConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/tracking/TrackerConstants$Attributes$DestinationAttributes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SOURCE_VALUES"
.end annotation


# static fields
.field public static final DETAILS:Ljava/lang/String; = "Details"

.field public static final DROP_PIN:Ljava/lang/String; = "Drop_Pin"

.field public static final FAVORITES_LIST:Ljava/lang/String; = "Favorites_List"

.field public static final SEARCH:Ljava/lang/String; = "Search"

.field public static final SUGGESTION_LIST:Ljava/lang/String; = "Suggestion_List"

.field public static final THIRD_PARTY_INTENT:Ljava/lang/String; = "Third_Party_Intent"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.class Lcom/navdy/client/app/tracking/SetDestinationTracker$1;
.super Ljava/lang/Object;
.source "SetDestinationTracker.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/tracking/SetDestinationTracker;->tagSetDestinationEvent(Lcom/navdy/client/app/framework/models/Destination;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/tracking/SetDestinationTracker;

.field final synthetic val$destination:Lcom/navdy/client/app/framework/models/Destination;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/tracking/SetDestinationTracker;Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/tracking/SetDestinationTracker;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/navdy/client/app/tracking/SetDestinationTracker$1;->this$0:Lcom/navdy/client/app/tracking/SetDestinationTracker;

    iput-object p2, p0, Lcom/navdy/client/app/tracking/SetDestinationTracker$1;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 81
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 83
    .local v1, "attributes":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    .line 84
    .local v2, "context":Landroid/content/Context;
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v0

    .line 85
    .local v0, "appInstance":Lcom/navdy/client/app/framework/AppInstance;
    invoke-virtual {v0}, Lcom/navdy/client/app/framework/AppInstance;->isDeviceConnected()Z

    move-result v3

    .line 86
    .local v3, "isConnected":Z
    invoke-static {v2}, Lcom/navdy/service/library/util/SystemUtils;->isConnectedToNetwork(Landroid/content/Context;)Z

    move-result v4

    .line 88
    .local v4, "isOnline":Z
    iget-object v5, p0, Lcom/navdy/client/app/tracking/SetDestinationTracker$1;->this$0:Lcom/navdy/client/app/tracking/SetDestinationTracker;

    invoke-static {v5}, Lcom/navdy/client/app/tracking/SetDestinationTracker;->access$000(Lcom/navdy/client/app/tracking/SetDestinationTracker;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 89
    iget-object v5, p0, Lcom/navdy/client/app/tracking/SetDestinationTracker$1;->this$0:Lcom/navdy/client/app/tracking/SetDestinationTracker;

    iget-object v6, p0, Lcom/navdy/client/app/tracking/SetDestinationTracker$1;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-static {v6}, Lcom/navdy/client/app/tracking/TrackerConstants$Attributes$DestinationAttributes$FAVORITE_TYPE_VALUES;->getFavoriteTypeValue(Lcom/navdy/client/app/framework/models/Destination;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/navdy/client/app/tracking/SetDestinationTracker;->access$002(Lcom/navdy/client/app/tracking/SetDestinationTracker;Ljava/lang/String;)Ljava/lang/String;

    .line 92
    :cond_0
    const-string v6, "Next_Trip"

    if-nez v3, :cond_1

    const/4 v5, 0x1

    :goto_0
    invoke-static {v5}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v6, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    const-string v5, "Online"

    invoke-static {v4}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    const-string v5, "Type"

    iget-object v6, p0, Lcom/navdy/client/app/tracking/SetDestinationTracker$1;->this$0:Lcom/navdy/client/app/tracking/SetDestinationTracker;

    invoke-static {v6}, Lcom/navdy/client/app/tracking/SetDestinationTracker;->access$100(Lcom/navdy/client/app/tracking/SetDestinationTracker;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    const-string v5, "Source"

    iget-object v6, p0, Lcom/navdy/client/app/tracking/SetDestinationTracker$1;->this$0:Lcom/navdy/client/app/tracking/SetDestinationTracker;

    invoke-static {v6}, Lcom/navdy/client/app/tracking/SetDestinationTracker;->access$200(Lcom/navdy/client/app/tracking/SetDestinationTracker;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    const-string v5, "Favorite_Type"

    iget-object v6, p0, Lcom/navdy/client/app/tracking/SetDestinationTracker$1;->this$0:Lcom/navdy/client/app/tracking/SetDestinationTracker;

    invoke-static {v6}, Lcom/navdy/client/app/tracking/SetDestinationTracker;->access$000(Lcom/navdy/client/app/tracking/SetDestinationTracker;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    const-string v5, "Set_Destination"

    invoke-static {v5, v1}, Lcom/navdy/client/app/tracking/Tracker;->tagEvent(Ljava/lang/String;Ljava/util/Map;)V

    .line 103
    iget-object v5, p0, Lcom/navdy/client/app/tracking/SetDestinationTracker$1;->this$0:Lcom/navdy/client/app/tracking/SetDestinationTracker;

    invoke-virtual {v5}, Lcom/navdy/client/app/tracking/SetDestinationTracker;->resetValues()V

    .line 104
    return-void

    .line 92
    :cond_1
    const/4 v5, 0x0

    goto :goto_0
.end method

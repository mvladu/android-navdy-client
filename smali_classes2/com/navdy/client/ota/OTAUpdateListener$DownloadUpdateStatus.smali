.class public final enum Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;
.super Ljava/lang/Enum;
.source "OTAUpdateListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/ota/OTAUpdateListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DownloadUpdateStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;

.field public static final enum COMPLETED:Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;

.field public static final enum DOWNLOADING:Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;

.field public static final enum DOWNLOAD_FAILED:Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;

.field public static final enum NOT_ENOUGH_SPACE:Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;

.field public static final enum NO_CONNECTIVITY:Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;

.field public static final enum PAUSED:Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;

.field public static final enum STARTED:Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 39
    new-instance v0, Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;

    const-string v1, "STARTED"

    invoke-direct {v0, v1, v3}, Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;->STARTED:Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;

    .line 43
    new-instance v0, Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;

    const-string v1, "PAUSED"

    invoke-direct {v0, v1, v4}, Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;->PAUSED:Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;

    .line 47
    new-instance v0, Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;

    const-string v1, "DOWNLOADING"

    invoke-direct {v0, v1, v5}, Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;->DOWNLOADING:Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;

    .line 51
    new-instance v0, Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;

    const-string v1, "NOT_ENOUGH_SPACE"

    invoke-direct {v0, v1, v6}, Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;->NOT_ENOUGH_SPACE:Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;

    .line 53
    new-instance v0, Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;

    const-string v1, "NO_CONNECTIVITY"

    invoke-direct {v0, v1, v7}, Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;->NO_CONNECTIVITY:Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;

    .line 57
    new-instance v0, Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;

    const-string v1, "DOWNLOAD_FAILED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;->DOWNLOAD_FAILED:Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;

    .line 61
    new-instance v0, Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;

    const-string v1, "COMPLETED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;->COMPLETED:Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;

    .line 35
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;

    sget-object v1, Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;->STARTED:Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;->PAUSED:Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;->DOWNLOADING:Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;->NOT_ENOUGH_SPACE:Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;->NO_CONNECTIVITY:Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;->DOWNLOAD_FAILED:Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;->COMPLETED:Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;->$VALUES:[Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 35
    const-class v0, Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;

    return-object v0
.end method

.method public static values()[Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;->$VALUES:[Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;

    invoke-virtual {v0}, [Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;

    return-object v0
.end method

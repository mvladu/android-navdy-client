.class Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1$2;
.super Ljava/lang/Object;
.source "OTAUpdateManagerImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1;->onProgressChanged(IJJ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1;

.field final synthetic val$bytesCurrent:J

.field final synthetic val$id:I


# direct methods
.method constructor <init>(Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1;IJ)V
    .locals 1
    .param p1, "this$1"    # Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1;

    .prologue
    .line 298
    iput-object p1, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1$2;->this$1:Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1;

    iput p2, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1$2;->val$id:I

    iput-wide p3, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1$2;->val$bytesCurrent:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 302
    :try_start_0
    iget-object v1, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1$2;->this$1:Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1;

    iget-object v1, v1, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1;->this$0:Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;

    invoke-static {v1}, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->access$000(Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;)Lcom/navdy/client/ota/OTAUpdateListener;

    move-result-object v1

    sget-object v2, Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;->DOWNLOADING:Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;

    iget v3, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1$2;->val$id:I

    iget-wide v4, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1$2;->val$bytesCurrent:J

    iget-object v6, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1$2;->this$1:Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1;

    iget-object v6, v6, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$1;->val$info:Lcom/navdy/client/ota/model/UpdateInfo;

    iget-wide v6, v6, Lcom/navdy/client/ota/model/UpdateInfo;->size:J

    invoke-interface/range {v1 .. v7}, Lcom/navdy/client/ota/OTAUpdateListener;->onDownloadProgress(Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;IJJ)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 306
    :goto_0
    return-void

    .line 303
    :catch_0
    move-exception v0

    .line 304
    .local v0, "t":Ljava/lang/Throwable;
    invoke-static {}, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "Bad listener "

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

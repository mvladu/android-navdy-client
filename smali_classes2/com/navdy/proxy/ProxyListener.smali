.class public Lcom/navdy/proxy/ProxyListener;
.super Lcom/navdy/service/library/device/connection/ProxyService;
.source "ProxyListener.java"


# static fields
.field public static final MAX_RETRY_INTERVAL:I = 0x3e80

.field public static final MIN_RETRY_INTERVAL:I = 0x3e8

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private volatile closing:Z

.field private mSocketAcceptor:Lcom/navdy/service/library/network/SocketAcceptor;

.field private retryInterval:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 15
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/proxy/ProxyListener;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/proxy/ProxyListener;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/network/SocketAcceptor;)V
    .locals 1
    .param p1, "acceptor"    # Lcom/navdy/service/library/network/SocketAcceptor;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/navdy/service/library/device/connection/ProxyService;-><init>()V

    .line 21
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/navdy/proxy/ProxyListener;->retryInterval:I

    .line 22
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/proxy/ProxyListener;->closing:Z

    .line 26
    const-class v0, Lcom/navdy/proxy/ProxyListener;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/navdy/proxy/ProxyListener;->setName(Ljava/lang/String;)V

    .line 27
    iput-object p1, p0, Lcom/navdy/proxy/ProxyListener;->mSocketAcceptor:Lcom/navdy/service/library/network/SocketAcceptor;

    .line 28
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 4

    .prologue
    .line 70
    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lcom/navdy/proxy/ProxyListener;->closing:Z

    .line 71
    iget-object v1, p0, Lcom/navdy/proxy/ProxyListener;->mSocketAcceptor:Lcom/navdy/service/library/network/SocketAcceptor;

    invoke-interface {v1}, Lcom/navdy/service/library/network/SocketAcceptor;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 75
    :goto_0
    return-void

    .line 72
    :catch_0
    move-exception v0

    .line 73
    .local v0, "e":Ljava/io/IOException;
    sget-object v1, Lcom/navdy/proxy/ProxyListener;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "error closing socket acceptor: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public run()V
    .locals 6

    .prologue
    .line 32
    sget-object v3, Lcom/navdy/proxy/ProxyListener;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "start: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/proxy/ProxyListener;->mSocketAcceptor:Lcom/navdy/service/library/network/SocketAcceptor;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 34
    :cond_0
    :goto_0
    iget-boolean v3, p0, Lcom/navdy/proxy/ProxyListener;->closing:Z

    if-nez v3, :cond_1

    .line 37
    :try_start_0
    iget-object v3, p0, Lcom/navdy/proxy/ProxyListener;->mSocketAcceptor:Lcom/navdy/service/library/network/SocketAcceptor;

    invoke-interface {v3}, Lcom/navdy/service/library/network/SocketAcceptor;->accept()Lcom/navdy/service/library/network/SocketAdapter;

    move-result-object v1

    .line 39
    .local v1, "fromSocket":Lcom/navdy/service/library/network/SocketAdapter;
    const/16 v3, 0x3e8

    iput v3, p0, Lcom/navdy/proxy/ProxyListener;->retryInterval:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 55
    sget-object v3, Lcom/navdy/proxy/ProxyListener;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "accepted connection ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 57
    :try_start_1
    new-instance v2, Lcom/navdy/proxy/ProxyThread;

    invoke-direct {v2, v1}, Lcom/navdy/proxy/ProxyThread;-><init>(Lcom/navdy/service/library/network/SocketAdapter;)V

    .line 58
    .local v2, "proxyThread":Lcom/navdy/proxy/ProxyThread;
    invoke-virtual {v2}, Lcom/navdy/proxy/ProxyThread;->start()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 59
    .end local v2    # "proxyThread":Lcom/navdy/proxy/ProxyThread;
    :catch_0
    move-exception v0

    .line 60
    .local v0, "e":Ljava/lang/Exception;
    sget-object v3, Lcom/navdy/proxy/ProxyListener;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Exception while starting proxy"

    invoke-virtual {v3, v4, v0}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 61
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_0

    .line 40
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "fromSocket":Lcom/navdy/service/library/network/SocketAdapter;
    :catch_1
    move-exception v0

    .line 41
    .restart local v0    # "e":Ljava/lang/Exception;
    iget-boolean v3, p0, Lcom/navdy/proxy/ProxyListener;->closing:Z

    if-nez v3, :cond_0

    .line 42
    sget-object v3, Lcom/navdy/proxy/ProxyListener;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 44
    :try_start_2
    iget v3, p0, Lcom/navdy/proxy/ProxyListener;->retryInterval:I

    int-to-long v4, v3

    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_2

    .line 47
    :goto_1
    iget v3, p0, Lcom/navdy/proxy/ProxyListener;->retryInterval:I

    mul-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/navdy/proxy/ProxyListener;->retryInterval:I

    .line 48
    iget v3, p0, Lcom/navdy/proxy/ProxyListener;->retryInterval:I

    const/16 v4, 0x3e80

    if-le v3, v4, :cond_0

    .line 49
    sget-object v3, Lcom/navdy/proxy/ProxyListener;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Giving up on proxy - assuming that we\'ll be restarted"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 64
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    sget-object v3, Lcom/navdy/proxy/ProxyListener;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "end: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/proxy/ProxyListener;->mSocketAcceptor:Lcom/navdy/service/library/network/SocketAcceptor;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 65
    return-void

    .line 45
    .restart local v0    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v3

    goto :goto_1
.end method

.class public Lcom/navdy/proxy/ProxyThread;
.super Ljava/lang/Thread;
.source "ProxyThread.java"


# static fields
.field private static final VERBOSE_DBG:Z

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field mInputStream:Ljava/io/InputStream;

.field mInputThread:Lcom/navdy/proxy/ProxyInputThread;

.field mOutputHandler:Landroid/os/Handler;

.field mOutputStream:Ljava/io/OutputStream;

.field mOutputThread:Lcom/navdy/proxy/ProxyOutputThread;

.field mSocket:Lcom/navdy/service/library/network/SocketAdapter;

.field private nativeStorage:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 21
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/proxy/ProxyThread;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/proxy/ProxyThread;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 25
    const-string v0, "proxy"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 26
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/network/SocketAdapter;)V
    .locals 3
    .param p1, "socket"    # Lcom/navdy/service/library/network/SocketAdapter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 41
    const-class v0, Lcom/navdy/proxy/ProxyThread;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/navdy/proxy/ProxyThread;->setName(Ljava/lang/String;)V

    .line 42
    iput-object p1, p0, Lcom/navdy/proxy/ProxyThread;->mSocket:Lcom/navdy/service/library/network/SocketAdapter;

    .line 43
    invoke-interface {p1}, Lcom/navdy/service/library/network/SocketAdapter;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/proxy/ProxyThread;->mInputStream:Ljava/io/InputStream;

    .line 44
    invoke-interface {p1}, Lcom/navdy/service/library/network/SocketAdapter;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/proxy/ProxyThread;->mOutputStream:Ljava/io/OutputStream;

    .line 45
    sget-object v0, Lcom/navdy/proxy/ProxyThread;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "initialized with input "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/proxy/ProxyThread;->mInputStream:Ljava/io/InputStream;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and output "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/proxy/ProxyThread;->mOutputStream:Ljava/io/OutputStream;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 46
    return-void
.end method

.method private native exitEventLoop()V
.end method

.method private native receiveBytesNative([BI)V
.end method

.method private sendOutput([B)V
    .locals 3
    .param p1, "buffer"    # [B

    .prologue
    .line 83
    iget-object v1, p0, Lcom/navdy/proxy/ProxyThread;->mOutputHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-static {v1, v2, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 84
    .local v0, "msg":Landroid/os/Message;
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 85
    return-void
.end method

.method private native setupEventLoop()V
.end method

.method private native startEventLoop()I
.end method


# virtual methods
.method public inputThreadWillFinish()V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/navdy/proxy/ProxyThread;->exitEventLoop()V

    return-void
.end method

.method public outputThreadWillFinish()V
    .locals 0

    .prologue
    .line 88
    invoke-direct {p0}, Lcom/navdy/proxy/ProxyThread;->exitEventLoop()V

    return-void
.end method

.method public receiveBytes([BI)V
    .locals 0
    .param p1, "buffer"    # [B
    .param p2, "bytesRead"    # I

    .prologue
    .line 77
    invoke-direct {p0, p1, p2}, Lcom/navdy/proxy/ProxyThread;->receiveBytesNative([BI)V

    .line 78
    return-void
.end method

.method public run()V
    .locals 2

    .prologue
    .line 50
    invoke-super {p0}, Ljava/lang/Thread;->run()V

    .line 52
    invoke-direct {p0}, Lcom/navdy/proxy/ProxyThread;->setupEventLoop()V

    .line 54
    new-instance v0, Lcom/navdy/proxy/ProxyInputThread;

    iget-object v1, p0, Lcom/navdy/proxy/ProxyThread;->mInputStream:Ljava/io/InputStream;

    invoke-direct {v0, p0, v1}, Lcom/navdy/proxy/ProxyInputThread;-><init>(Lcom/navdy/proxy/ProxyThread;Ljava/io/InputStream;)V

    iput-object v0, p0, Lcom/navdy/proxy/ProxyThread;->mInputThread:Lcom/navdy/proxy/ProxyInputThread;

    .line 55
    new-instance v0, Lcom/navdy/proxy/ProxyOutputThread;

    iget-object v1, p0, Lcom/navdy/proxy/ProxyThread;->mOutputStream:Ljava/io/OutputStream;

    invoke-direct {v0, p0, v1}, Lcom/navdy/proxy/ProxyOutputThread;-><init>(Lcom/navdy/proxy/ProxyThread;Ljava/io/OutputStream;)V

    iput-object v0, p0, Lcom/navdy/proxy/ProxyThread;->mOutputThread:Lcom/navdy/proxy/ProxyOutputThread;

    .line 57
    iget-object v0, p0, Lcom/navdy/proxy/ProxyThread;->mInputThread:Lcom/navdy/proxy/ProxyInputThread;

    invoke-virtual {v0}, Lcom/navdy/proxy/ProxyInputThread;->start()V

    .line 58
    iget-object v0, p0, Lcom/navdy/proxy/ProxyThread;->mOutputThread:Lcom/navdy/proxy/ProxyOutputThread;

    invoke-virtual {v0}, Lcom/navdy/proxy/ProxyOutputThread;->start()V

    .line 60
    iget-object v0, p0, Lcom/navdy/proxy/ProxyThread;->mOutputThread:Lcom/navdy/proxy/ProxyOutputThread;

    invoke-virtual {v0}, Lcom/navdy/proxy/ProxyOutputThread;->getHandler()Landroid/os/Handler;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/proxy/ProxyThread;->mOutputHandler:Landroid/os/Handler;

    .line 62
    invoke-direct {p0}, Lcom/navdy/proxy/ProxyThread;->startEventLoop()I

    .line 65
    iget-object v0, p0, Lcom/navdy/proxy/ProxyThread;->mInputStream:Ljava/io/InputStream;

    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 66
    iget-object v0, p0, Lcom/navdy/proxy/ProxyThread;->mOutputStream:Ljava/io/OutputStream;

    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 67
    iget-object v0, p0, Lcom/navdy/proxy/ProxyThread;->mSocket:Lcom/navdy/service/library/network/SocketAdapter;

    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 70
    return-void
.end method

.class public abstract Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster;
.super Ljava/lang/Object;
.source "MDNSDeviceBroadcaster.java"

# interfaces
.implements Lcom/navdy/service/library/device/discovery/RemoteDeviceBroadcaster;


# static fields
.field public static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field protected mNsdManager:Landroid/net/nsd/NsdManager;

.field protected mRegistrationListener:Landroid/net/nsd/NsdManager$RegistrationListener;

.field protected mServiceInfo:Landroid/net/nsd/NsdServiceInfo;

.field protected registered:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 16
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/net/nsd/NsdServiceInfo;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "serviceInfo"    # Landroid/net/nsd/NsdServiceInfo;

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    const-string v0, "servicediscovery"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/nsd/NsdManager;

    iput-object v0, p0, Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster;->mNsdManager:Landroid/net/nsd/NsdManager;

    .line 54
    if-nez p2, :cond_0

    .line 55
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Service info required."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 58
    :cond_0
    iput-object p2, p0, Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster;->mServiceInfo:Landroid/net/nsd/NsdServiceInfo;

    .line 59
    return-void
.end method


# virtual methods
.method protected getRegistrationListener()Landroid/net/nsd/NsdManager$RegistrationListener;
    .locals 1

    .prologue
    .line 62
    new-instance v0, Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster$1;

    invoke-direct {v0, p0}, Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster$1;-><init>(Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster;)V

    return-object v0
.end method

.method protected registerRecord(Landroid/net/nsd/NsdServiceInfo;Landroid/net/nsd/NsdManager$RegistrationListener;)Z
    .locals 4
    .param p1, "serviceInfo"    # Landroid/net/nsd/NsdServiceInfo;
    .param p2, "registrationListener"    # Landroid/net/nsd/NsdManager$RegistrationListener;

    .prologue
    .line 86
    sget-object v0, Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Registering: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/nsd/NsdServiceInfo;->getServiceType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 87
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    new-instance v1, Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster$2;

    invoke-direct {v1, p0, p1, p2}, Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster$2;-><init>(Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster;Landroid/net/nsd/NsdServiceInfo;Landroid/net/nsd/NsdManager$RegistrationListener;)V

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 97
    const/4 v0, 0x1

    return v0
.end method

.method public start()Z
    .locals 3

    .prologue
    .line 29
    iget-object v0, p0, Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster;->mRegistrationListener:Landroid/net/nsd/NsdManager$RegistrationListener;

    if-eqz v0, :cond_0

    .line 30
    sget-object v0, Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Already started: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster;->mServiceInfo:Landroid/net/nsd/NsdServiceInfo;

    invoke-virtual {v2}, Landroid/net/nsd/NsdServiceInfo;->getServiceType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 31
    const/4 v0, 0x0

    .line 36
    :goto_0
    return v0

    .line 34
    :cond_0
    invoke-virtual {p0}, Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster;->getRegistrationListener()Landroid/net/nsd/NsdManager$RegistrationListener;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster;->mRegistrationListener:Landroid/net/nsd/NsdManager$RegistrationListener;

    .line 36
    iget-object v0, p0, Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster;->mServiceInfo:Landroid/net/nsd/NsdServiceInfo;

    iget-object v1, p0, Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster;->mRegistrationListener:Landroid/net/nsd/NsdManager$RegistrationListener;

    invoke-virtual {p0, v0, v1}, Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster;->registerRecord(Landroid/net/nsd/NsdServiceInfo;Landroid/net/nsd/NsdManager$RegistrationListener;)Z

    move-result v0

    goto :goto_0
.end method

.method public stop()Z
    .locals 3

    .prologue
    .line 41
    iget-object v0, p0, Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster;->mRegistrationListener:Landroid/net/nsd/NsdManager$RegistrationListener;

    if-nez v0, :cond_0

    .line 42
    sget-object v0, Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Already stopped: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster;->mServiceInfo:Landroid/net/nsd/NsdServiceInfo;

    invoke-virtual {v2}, Landroid/net/nsd/NsdServiceInfo;->getServiceType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 44
    const/4 v0, 0x0

    .line 48
    :goto_0
    return v0

    .line 47
    :cond_0
    sget-object v0, Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unregistering: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster;->mServiceInfo:Landroid/net/nsd/NsdServiceInfo;

    invoke-virtual {v2}, Landroid/net/nsd/NsdServiceInfo;->getServiceType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 48
    iget-object v0, p0, Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster;->mRegistrationListener:Landroid/net/nsd/NsdManager$RegistrationListener;

    invoke-virtual {p0, v0}, Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster;->unregisterRecord(Landroid/net/nsd/NsdManager$RegistrationListener;)Z

    move-result v0

    goto :goto_0
.end method

.method protected unregisterRecord(Landroid/net/nsd/NsdManager$RegistrationListener;)Z
    .locals 1
    .param p1, "registrationListener"    # Landroid/net/nsd/NsdManager$RegistrationListener;

    .prologue
    .line 101
    iget-boolean v0, p0, Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster;->registered:Z

    if-eqz v0, :cond_0

    .line 102
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster;->registered:Z

    .line 103
    iget-object v0, p0, Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster;->mNsdManager:Landroid/net/nsd/NsdManager;

    invoke-virtual {v0, p1}, Landroid/net/nsd/NsdManager;->unregisterService(Landroid/net/nsd/NsdManager$RegistrationListener;)V

    .line 105
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.class public Lcom/navdy/service/library/events/NavdyEventWriter;
.super Ljava/lang/Object;
.source "NavdyEventWriter.java"


# static fields
.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field protected mOutputStream:Ljava/io/OutputStream;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 14
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/service/library/events/NavdyEventWriter;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEventWriter;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "output"    # Ljava/io/OutputStream;

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    if-nez p1, :cond_0

    .line 20
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 22
    :cond_0
    iput-object p1, p0, Lcom/navdy/service/library/events/NavdyEventWriter;->mOutputStream:Ljava/io/OutputStream;

    .line 23
    return-void
.end method


# virtual methods
.method public write([B)V
    .locals 4
    .param p1, "eventData"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 26
    new-instance v0, Lcom/navdy/service/library/events/Frame;

    array-length v1, p1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/navdy/service/library/events/Frame;-><init>(Ljava/lang/Integer;)V

    .line 27
    .local v0, "frame":Lcom/navdy/service/library/events/Frame;
    iget-object v1, p0, Lcom/navdy/service/library/events/NavdyEventWriter;->mOutputStream:Ljava/io/OutputStream;

    invoke-virtual {v0}, Lcom/navdy/service/library/events/Frame;->toByteArray()[B

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/OutputStream;->write([B)V

    .line 28
    iget-object v1, p0, Lcom/navdy/service/library/events/NavdyEventWriter;->mOutputStream:Ljava/io/OutputStream;

    invoke-virtual {v1, p1}, Ljava/io/OutputStream;->write([B)V

    .line 29
    iget-object v1, p0, Lcom/navdy/service/library/events/NavdyEventWriter;->mOutputStream:Ljava/io/OutputStream;

    invoke-virtual {v1}, Ljava/io/OutputStream;->flush()V

    .line 30
    sget-object v1, Lcom/navdy/service/library/events/NavdyEventWriter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 31
    sget-object v1, Lcom/navdy/service/library/events/NavdyEventWriter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[Outgoing-Event] size:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    array-length v3, p1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 33
    :cond_0
    return-void
.end method

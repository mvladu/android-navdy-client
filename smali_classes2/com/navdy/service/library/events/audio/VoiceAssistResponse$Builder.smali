.class public final Lcom/navdy/service/library/events/audio/VoiceAssistResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "VoiceAssistResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/audio/VoiceAssistResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/audio/VoiceAssistResponse;",
        ">;"
    }
.end annotation


# instance fields
.field public state:Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 50
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/audio/VoiceAssistResponse;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/audio/VoiceAssistResponse;

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 54
    if-nez p1, :cond_0

    .line 56
    :goto_0
    return-void

    .line 55
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/VoiceAssistResponse;->state:Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/VoiceAssistResponse$Builder;->state:Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/audio/VoiceAssistResponse;
    .locals 2

    .prologue
    .line 65
    invoke-virtual {p0}, Lcom/navdy/service/library/events/audio/VoiceAssistResponse$Builder;->checkRequiredFields()V

    .line 66
    new-instance v0, Lcom/navdy/service/library/events/audio/VoiceAssistResponse;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/audio/VoiceAssistResponse;-><init>(Lcom/navdy/service/library/events/audio/VoiceAssistResponse$Builder;Lcom/navdy/service/library/events/audio/VoiceAssistResponse$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 45
    invoke-virtual {p0}, Lcom/navdy/service/library/events/audio/VoiceAssistResponse$Builder;->build()Lcom/navdy/service/library/events/audio/VoiceAssistResponse;

    move-result-object v0

    return-object v0
.end method

.method public state(Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;)Lcom/navdy/service/library/events/audio/VoiceAssistResponse$Builder;
    .locals 0
    .param p1, "state"    # Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;

    .prologue
    .line 59
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/VoiceAssistResponse$Builder;->state:Lcom/navdy/service/library/events/audio/VoiceAssistResponse$VoiceAssistState;

    .line 60
    return-object p0
.end method

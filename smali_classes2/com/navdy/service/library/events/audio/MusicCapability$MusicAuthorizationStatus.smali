.class public final enum Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;
.super Ljava/lang/Enum;
.source "MusicCapability.java"

# interfaces
.implements Lcom/squareup/wire/ProtoEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/audio/MusicCapability;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MusicAuthorizationStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;",
        ">;",
        "Lcom/squareup/wire/ProtoEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;

.field public static final enum MUSIC_AUTHORIZATION_AUTHORIZED:Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;

.field public static final enum MUSIC_AUTHORIZATION_DENIED:Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;

.field public static final enum MUSIC_AUTHORIZATION_NOT_DETERMINED:Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 191
    new-instance v0, Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;

    const-string v1, "MUSIC_AUTHORIZATION_NOT_DETERMINED"

    invoke-direct {v0, v1, v4, v2}, Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;->MUSIC_AUTHORIZATION_NOT_DETERMINED:Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;

    .line 192
    new-instance v0, Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;

    const-string v1, "MUSIC_AUTHORIZATION_AUTHORIZED"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;->MUSIC_AUTHORIZATION_AUTHORIZED:Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;

    .line 193
    new-instance v0, Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;

    const-string v1, "MUSIC_AUTHORIZATION_DENIED"

    invoke-direct {v0, v1, v3, v5}, Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;->MUSIC_AUTHORIZATION_DENIED:Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;

    .line 189
    new-array v0, v5, [Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;->MUSIC_AUTHORIZATION_NOT_DETERMINED:Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;->MUSIC_AUTHORIZATION_AUTHORIZED:Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;->MUSIC_AUTHORIZATION_DENIED:Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;

    aput-object v1, v0, v3

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;->$VALUES:[Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 197
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 198
    iput p3, p0, Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;->value:I

    .line 199
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 189
    const-class v0, Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;

    return-object v0
.end method

.method public static values()[Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;
    .locals 1

    .prologue
    .line 189
    sget-object v0, Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;->$VALUES:[Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;

    invoke-virtual {v0}, [Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 203
    iget v0, p0, Lcom/navdy/service/library/events/audio/MusicCapability$MusicAuthorizationStatus;->value:I

    return v0
.end method

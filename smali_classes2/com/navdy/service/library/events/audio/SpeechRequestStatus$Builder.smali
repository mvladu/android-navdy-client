.class public final Lcom/navdy/service/library/events/audio/SpeechRequestStatus$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SpeechRequestStatus.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/audio/SpeechRequestStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/audio/SpeechRequestStatus;",
        ">;"
    }
.end annotation


# instance fields
.field public id:Ljava/lang/String;

.field public status:Lcom/navdy/service/library/events/audio/SpeechRequestStatus$SpeechRequestStatusType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 69
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/audio/SpeechRequestStatus;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/audio/SpeechRequestStatus;

    .prologue
    .line 72
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 73
    if-nez p1, :cond_0

    .line 76
    :goto_0
    return-void

    .line 74
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/SpeechRequestStatus;->id:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/SpeechRequestStatus$Builder;->id:Ljava/lang/String;

    .line 75
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/SpeechRequestStatus;->status:Lcom/navdy/service/library/events/audio/SpeechRequestStatus$SpeechRequestStatusType;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/SpeechRequestStatus$Builder;->status:Lcom/navdy/service/library/events/audio/SpeechRequestStatus$SpeechRequestStatusType;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/audio/SpeechRequestStatus;
    .locals 2

    .prologue
    .line 96
    new-instance v0, Lcom/navdy/service/library/events/audio/SpeechRequestStatus;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/audio/SpeechRequestStatus;-><init>(Lcom/navdy/service/library/events/audio/SpeechRequestStatus$Builder;Lcom/navdy/service/library/events/audio/SpeechRequestStatus$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 63
    invoke-virtual {p0}, Lcom/navdy/service/library/events/audio/SpeechRequestStatus$Builder;->build()Lcom/navdy/service/library/events/audio/SpeechRequestStatus;

    move-result-object v0

    return-object v0
.end method

.method public id(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/SpeechRequestStatus$Builder;
    .locals 0
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 82
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/SpeechRequestStatus$Builder;->id:Ljava/lang/String;

    .line 83
    return-object p0
.end method

.method public status(Lcom/navdy/service/library/events/audio/SpeechRequestStatus$SpeechRequestStatusType;)Lcom/navdy/service/library/events/audio/SpeechRequestStatus$Builder;
    .locals 0
    .param p1, "status"    # Lcom/navdy/service/library/events/audio/SpeechRequestStatus$SpeechRequestStatusType;

    .prologue
    .line 90
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/SpeechRequestStatus$Builder;->status:Lcom/navdy/service/library/events/audio/SpeechRequestStatus$SpeechRequestStatusType;

    .line 91
    return-object p0
.end method

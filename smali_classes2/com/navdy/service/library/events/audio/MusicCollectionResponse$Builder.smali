.class public final Lcom/navdy/service/library/events/audio/MusicCollectionResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "MusicCollectionResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/audio/MusicCollectionResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/audio/MusicCollectionResponse;",
        ">;"
    }
.end annotation


# instance fields
.field public characterMap:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicCharacterMap;",
            ">;"
        }
    .end annotation
.end field

.field public collectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

.field public musicCollections:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicCollectionInfo;",
            ">;"
        }
    .end annotation
.end field

.field public musicTracks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicTrackInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 89
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 90
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/audio/MusicCollectionResponse;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/audio/MusicCollectionResponse;

    .prologue
    .line 93
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 94
    if-nez p1, :cond_0

    .line 99
    :goto_0
    return-void

    .line 95
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->collectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicCollectionResponse$Builder;->collectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    .line 96
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->musicCollections:Ljava/util/List;

    invoke-static {v0}, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->access$000(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicCollectionResponse$Builder;->musicCollections:Ljava/util/List;

    .line 97
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->musicTracks:Ljava/util/List;

    invoke-static {v0}, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->access$100(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicCollectionResponse$Builder;->musicTracks:Ljava/util/List;

    .line 98
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->characterMap:Ljava/util/List;

    invoke-static {v0}, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;->access$200(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicCollectionResponse$Builder;->characterMap:Ljava/util/List;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/audio/MusicCollectionResponse;
    .locals 2

    .prologue
    .line 138
    new-instance v0, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/audio/MusicCollectionResponse;-><init>(Lcom/navdy/service/library/events/audio/MusicCollectionResponse$Builder;Lcom/navdy/service/library/events/audio/MusicCollectionResponse$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 82
    invoke-virtual {p0}, Lcom/navdy/service/library/events/audio/MusicCollectionResponse$Builder;->build()Lcom/navdy/service/library/events/audio/MusicCollectionResponse;

    move-result-object v0

    return-object v0
.end method

.method public characterMap(Ljava/util/List;)Lcom/navdy/service/library/events/audio/MusicCollectionResponse$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicCharacterMap;",
            ">;)",
            "Lcom/navdy/service/library/events/audio/MusicCollectionResponse$Builder;"
        }
    .end annotation

    .prologue
    .line 132
    .local p1, "characterMap":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCharacterMap;>;"
    invoke-static {p1}, Lcom/navdy/service/library/events/audio/MusicCollectionResponse$Builder;->checkForNulls(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicCollectionResponse$Builder;->characterMap:Ljava/util/List;

    .line 133
    return-object p0
.end method

.method public collectionInfo(Lcom/navdy/service/library/events/audio/MusicCollectionInfo;)Lcom/navdy/service/library/events/audio/MusicCollectionResponse$Builder;
    .locals 0
    .param p1, "collectionInfo"    # Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicCollectionResponse$Builder;->collectionInfo:Lcom/navdy/service/library/events/audio/MusicCollectionInfo;

    .line 106
    return-object p0
.end method

.method public musicCollections(Ljava/util/List;)Lcom/navdy/service/library/events/audio/MusicCollectionResponse$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicCollectionInfo;",
            ">;)",
            "Lcom/navdy/service/library/events/audio/MusicCollectionResponse$Builder;"
        }
    .end annotation

    .prologue
    .line 114
    .local p1, "musicCollections":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionInfo;>;"
    invoke-static {p1}, Lcom/navdy/service/library/events/audio/MusicCollectionResponse$Builder;->checkForNulls(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicCollectionResponse$Builder;->musicCollections:Ljava/util/List;

    .line 115
    return-object p0
.end method

.method public musicTracks(Ljava/util/List;)Lcom/navdy/service/library/events/audio/MusicCollectionResponse$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicTrackInfo;",
            ">;)",
            "Lcom/navdy/service/library/events/audio/MusicCollectionResponse$Builder;"
        }
    .end annotation

    .prologue
    .line 123
    .local p1, "musicTracks":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicTrackInfo;>;"
    invoke-static {p1}, Lcom/navdy/service/library/events/audio/MusicCollectionResponse$Builder;->checkForNulls(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicCollectionResponse$Builder;->musicTracks:Ljava/util/List;

    .line 124
    return-object p0
.end method

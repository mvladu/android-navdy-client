.class public final Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SpeechRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/audio/SpeechRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/audio/SpeechRequest;",
        ">;"
    }
.end annotation


# instance fields
.field public category:Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

.field public id:Ljava/lang/String;

.field public locale:Ljava/lang/String;

.field public sendStatus:Ljava/lang/Boolean;

.field public words:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 104
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 105
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/audio/SpeechRequest;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/audio/SpeechRequest;

    .prologue
    .line 108
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 109
    if-nez p1, :cond_0

    .line 115
    :goto_0
    return-void

    .line 110
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/SpeechRequest;->words:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;->words:Ljava/lang/String;

    .line 111
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/SpeechRequest;->category:Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;->category:Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

    .line 112
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/SpeechRequest;->id:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;->id:Ljava/lang/String;

    .line 113
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/SpeechRequest;->sendStatus:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;->sendStatus:Ljava/lang/Boolean;

    .line 114
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/SpeechRequest;->locale:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;->locale:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/audio/SpeechRequest;
    .locals 2

    .prologue
    .line 160
    invoke-virtual {p0}, Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;->checkRequiredFields()V

    .line 161
    new-instance v0, Lcom/navdy/service/library/events/audio/SpeechRequest;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/audio/SpeechRequest;-><init>(Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;Lcom/navdy/service/library/events/audio/SpeechRequest$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 96
    invoke-virtual {p0}, Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;->build()Lcom/navdy/service/library/events/audio/SpeechRequest;

    move-result-object v0

    return-object v0
.end method

.method public category(Lcom/navdy/service/library/events/audio/SpeechRequest$Category;)Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;
    .locals 0
    .param p1, "category"    # Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

    .prologue
    .line 129
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;->category:Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

    .line 130
    return-object p0
.end method

.method public id(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;
    .locals 0
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 137
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;->id:Ljava/lang/String;

    .line 138
    return-object p0
.end method

.method public locale(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;
    .locals 0
    .param p1, "locale"    # Ljava/lang/String;

    .prologue
    .line 154
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;->locale:Ljava/lang/String;

    .line 155
    return-object p0
.end method

.method public sendStatus(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;
    .locals 0
    .param p1, "sendStatus"    # Ljava/lang/Boolean;

    .prologue
    .line 145
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;->sendStatus:Ljava/lang/Boolean;

    .line 146
    return-object p0
.end method

.method public words(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;
    .locals 0
    .param p1, "words"    # Ljava/lang/String;

    .prologue
    .line 121
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/SpeechRequest$Builder;->words:Ljava/lang/String;

    .line 122
    return-object p0
.end method

.class public final Lcom/navdy/service/library/events/audio/MusicCollectionRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "MusicCollectionRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/audio/MusicCollectionRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/audio/MusicCollectionRequest;",
        ">;"
    }
.end annotation


# instance fields
.field public collectionId:Ljava/lang/String;

.field public collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

.field public collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

.field public filters:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicCollectionFilter;",
            ">;"
        }
    .end annotation
.end field

.field public groupBy:Lcom/navdy/service/library/events/audio/MusicCollectionType;

.field public includeCharacterMap:Ljava/lang/Boolean;

.field public limit:Ljava/lang/Integer;

.field public offset:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 131
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 132
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/audio/MusicCollectionRequest;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/audio/MusicCollectionRequest;

    .prologue
    .line 135
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 136
    if-nez p1, :cond_0

    .line 145
    :goto_0
    return-void

    .line 137
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest$Builder;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .line 138
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest$Builder;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    .line 139
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->filters:Ljava/util/List;

    invoke-static {v0}, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->access$000(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest$Builder;->filters:Ljava/util/List;

    .line 140
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->collectionId:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest$Builder;->collectionId:Ljava/lang/String;

    .line 141
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->limit:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest$Builder;->limit:Ljava/lang/Integer;

    .line 142
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->offset:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest$Builder;->offset:Ljava/lang/Integer;

    .line 143
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->includeCharacterMap:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest$Builder;->includeCharacterMap:Ljava/lang/Boolean;

    .line 144
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;->groupBy:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest$Builder;->groupBy:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/audio/MusicCollectionRequest;
    .locals 2

    .prologue
    .line 209
    new-instance v0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/audio/MusicCollectionRequest;-><init>(Lcom/navdy/service/library/events/audio/MusicCollectionRequest$Builder;Lcom/navdy/service/library/events/audio/MusicCollectionRequest$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 120
    invoke-virtual {p0}, Lcom/navdy/service/library/events/audio/MusicCollectionRequest$Builder;->build()Lcom/navdy/service/library/events/audio/MusicCollectionRequest;

    move-result-object v0

    return-object v0
.end method

.method public collectionId(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicCollectionRequest$Builder;
    .locals 0
    .param p1, "collectionId"    # Ljava/lang/String;

    .prologue
    .line 177
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest$Builder;->collectionId:Ljava/lang/String;

    .line 178
    return-object p0
.end method

.method public collectionSource(Lcom/navdy/service/library/events/audio/MusicCollectionSource;)Lcom/navdy/service/library/events/audio/MusicCollectionRequest$Builder;
    .locals 0
    .param p1, "collectionSource"    # Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .prologue
    .line 151
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest$Builder;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .line 152
    return-object p0
.end method

.method public collectionType(Lcom/navdy/service/library/events/audio/MusicCollectionType;)Lcom/navdy/service/library/events/audio/MusicCollectionRequest$Builder;
    .locals 0
    .param p1, "collectionType"    # Lcom/navdy/service/library/events/audio/MusicCollectionType;

    .prologue
    .line 160
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest$Builder;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    .line 161
    return-object p0
.end method

.method public filters(Ljava/util/List;)Lcom/navdy/service/library/events/audio/MusicCollectionRequest$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/audio/MusicCollectionFilter;",
            ">;)",
            "Lcom/navdy/service/library/events/audio/MusicCollectionRequest$Builder;"
        }
    .end annotation

    .prologue
    .line 169
    .local p1, "filters":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/audio/MusicCollectionFilter;>;"
    invoke-static {p1}, Lcom/navdy/service/library/events/audio/MusicCollectionRequest$Builder;->checkForNulls(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest$Builder;->filters:Ljava/util/List;

    .line 170
    return-object p0
.end method

.method public groupBy(Lcom/navdy/service/library/events/audio/MusicCollectionType;)Lcom/navdy/service/library/events/audio/MusicCollectionRequest$Builder;
    .locals 0
    .param p1, "groupBy"    # Lcom/navdy/service/library/events/audio/MusicCollectionType;

    .prologue
    .line 203
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest$Builder;->groupBy:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    .line 204
    return-object p0
.end method

.method public includeCharacterMap(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/audio/MusicCollectionRequest$Builder;
    .locals 0
    .param p1, "includeCharacterMap"    # Ljava/lang/Boolean;

    .prologue
    .line 198
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest$Builder;->includeCharacterMap:Ljava/lang/Boolean;

    .line 199
    return-object p0
.end method

.method public limit(Ljava/lang/Integer;)Lcom/navdy/service/library/events/audio/MusicCollectionRequest$Builder;
    .locals 0
    .param p1, "limit"    # Ljava/lang/Integer;

    .prologue
    .line 185
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest$Builder;->limit:Ljava/lang/Integer;

    .line 186
    return-object p0
.end method

.method public offset(Ljava/lang/Integer;)Lcom/navdy/service/library/events/audio/MusicCollectionRequest$Builder;
    .locals 0
    .param p1, "offset"    # Ljava/lang/Integer;

    .prologue
    .line 190
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicCollectionRequest$Builder;->offset:Ljava/lang/Integer;

    .line 191
    return-object p0
.end method

.class public final enum Lcom/navdy/service/library/events/LegacyCapability;
.super Ljava/lang/Enum;
.source "LegacyCapability.java"

# interfaces
.implements Lcom/squareup/wire/ProtoEnum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/service/library/events/LegacyCapability;",
        ">;",
        "Lcom/squareup/wire/ProtoEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/service/library/events/LegacyCapability;

.field public static final enum CAPABILITY_COMPACT_UI:Lcom/navdy/service/library/events/LegacyCapability;

.field public static final enum CAPABILITY_LOCAL_MUSIC_BROWSER:Lcom/navdy/service/library/events/LegacyCapability;

.field public static final enum CAPABILITY_PLACE_TYPE_SEARCH:Lcom/navdy/service/library/events/LegacyCapability;

.field public static final enum CAPABILITY_VOICE_SEARCH:Lcom/navdy/service/library/events/LegacyCapability;

.field public static final enum CAPABILITY_VOICE_SEARCH_BETA:Lcom/navdy/service/library/events/LegacyCapability;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 9
    new-instance v0, Lcom/navdy/service/library/events/LegacyCapability;

    const-string v1, "CAPABILITY_VOICE_SEARCH_BETA"

    invoke-direct {v0, v1, v7, v3}, Lcom/navdy/service/library/events/LegacyCapability;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/LegacyCapability;->CAPABILITY_VOICE_SEARCH_BETA:Lcom/navdy/service/library/events/LegacyCapability;

    .line 10
    new-instance v0, Lcom/navdy/service/library/events/LegacyCapability;

    const-string v1, "CAPABILITY_COMPACT_UI"

    invoke-direct {v0, v1, v3, v4}, Lcom/navdy/service/library/events/LegacyCapability;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/LegacyCapability;->CAPABILITY_COMPACT_UI:Lcom/navdy/service/library/events/LegacyCapability;

    .line 11
    new-instance v0, Lcom/navdy/service/library/events/LegacyCapability;

    const-string v1, "CAPABILITY_PLACE_TYPE_SEARCH"

    invoke-direct {v0, v1, v4, v5}, Lcom/navdy/service/library/events/LegacyCapability;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/LegacyCapability;->CAPABILITY_PLACE_TYPE_SEARCH:Lcom/navdy/service/library/events/LegacyCapability;

    .line 12
    new-instance v0, Lcom/navdy/service/library/events/LegacyCapability;

    const-string v1, "CAPABILITY_LOCAL_MUSIC_BROWSER"

    invoke-direct {v0, v1, v5, v6}, Lcom/navdy/service/library/events/LegacyCapability;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/LegacyCapability;->CAPABILITY_LOCAL_MUSIC_BROWSER:Lcom/navdy/service/library/events/LegacyCapability;

    .line 13
    new-instance v0, Lcom/navdy/service/library/events/LegacyCapability;

    const-string v1, "CAPABILITY_VOICE_SEARCH"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v6, v2}, Lcom/navdy/service/library/events/LegacyCapability;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/LegacyCapability;->CAPABILITY_VOICE_SEARCH:Lcom/navdy/service/library/events/LegacyCapability;

    .line 7
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/navdy/service/library/events/LegacyCapability;

    sget-object v1, Lcom/navdy/service/library/events/LegacyCapability;->CAPABILITY_VOICE_SEARCH_BETA:Lcom/navdy/service/library/events/LegacyCapability;

    aput-object v1, v0, v7

    sget-object v1, Lcom/navdy/service/library/events/LegacyCapability;->CAPABILITY_COMPACT_UI:Lcom/navdy/service/library/events/LegacyCapability;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/service/library/events/LegacyCapability;->CAPABILITY_PLACE_TYPE_SEARCH:Lcom/navdy/service/library/events/LegacyCapability;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/service/library/events/LegacyCapability;->CAPABILITY_LOCAL_MUSIC_BROWSER:Lcom/navdy/service/library/events/LegacyCapability;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/service/library/events/LegacyCapability;->CAPABILITY_VOICE_SEARCH:Lcom/navdy/service/library/events/LegacyCapability;

    aput-object v1, v0, v6

    sput-object v0, Lcom/navdy/service/library/events/LegacyCapability;->$VALUES:[Lcom/navdy/service/library/events/LegacyCapability;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 18
    iput p3, p0, Lcom/navdy/service/library/events/LegacyCapability;->value:I

    .line 19
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/LegacyCapability;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 7
    const-class v0, Lcom/navdy/service/library/events/LegacyCapability;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/LegacyCapability;

    return-object v0
.end method

.method public static values()[Lcom/navdy/service/library/events/LegacyCapability;
    .locals 1

    .prologue
    .line 7
    sget-object v0, Lcom/navdy/service/library/events/LegacyCapability;->$VALUES:[Lcom/navdy/service/library/events/LegacyCapability;

    invoke-virtual {v0}, [Lcom/navdy/service/library/events/LegacyCapability;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/events/LegacyCapability;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 23
    iget v0, p0, Lcom/navdy/service/library/events/LegacyCapability;->value:I

    return v0
.end method

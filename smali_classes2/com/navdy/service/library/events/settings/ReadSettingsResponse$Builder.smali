.class public final Lcom/navdy/service/library/events/settings/ReadSettingsResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ReadSettingsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/settings/ReadSettingsResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/settings/ReadSettingsResponse;",
        ">;"
    }
.end annotation


# instance fields
.field public screenConfiguration:Lcom/navdy/service/library/events/settings/ScreenConfiguration;

.field public settings:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/settings/Setting;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 65
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/settings/ReadSettingsResponse;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/settings/ReadSettingsResponse;

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 69
    if-nez p1, :cond_0

    .line 72
    :goto_0
    return-void

    .line 70
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/settings/ReadSettingsResponse;->screenConfiguration:Lcom/navdy/service/library/events/settings/ScreenConfiguration;

    iput-object v0, p0, Lcom/navdy/service/library/events/settings/ReadSettingsResponse$Builder;->screenConfiguration:Lcom/navdy/service/library/events/settings/ScreenConfiguration;

    .line 71
    iget-object v0, p1, Lcom/navdy/service/library/events/settings/ReadSettingsResponse;->settings:Ljava/util/List;

    invoke-static {v0}, Lcom/navdy/service/library/events/settings/ReadSettingsResponse;->access$000(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/settings/ReadSettingsResponse$Builder;->settings:Ljava/util/List;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/settings/ReadSettingsResponse;
    .locals 2

    .prologue
    .line 91
    invoke-virtual {p0}, Lcom/navdy/service/library/events/settings/ReadSettingsResponse$Builder;->checkRequiredFields()V

    .line 92
    new-instance v0, Lcom/navdy/service/library/events/settings/ReadSettingsResponse;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/settings/ReadSettingsResponse;-><init>(Lcom/navdy/service/library/events/settings/ReadSettingsResponse$Builder;Lcom/navdy/service/library/events/settings/ReadSettingsResponse$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 59
    invoke-virtual {p0}, Lcom/navdy/service/library/events/settings/ReadSettingsResponse$Builder;->build()Lcom/navdy/service/library/events/settings/ReadSettingsResponse;

    move-result-object v0

    return-object v0
.end method

.method public screenConfiguration(Lcom/navdy/service/library/events/settings/ScreenConfiguration;)Lcom/navdy/service/library/events/settings/ReadSettingsResponse$Builder;
    .locals 0
    .param p1, "screenConfiguration"    # Lcom/navdy/service/library/events/settings/ScreenConfiguration;

    .prologue
    .line 75
    iput-object p1, p0, Lcom/navdy/service/library/events/settings/ReadSettingsResponse$Builder;->screenConfiguration:Lcom/navdy/service/library/events/settings/ScreenConfiguration;

    .line 76
    return-object p0
.end method

.method public settings(Ljava/util/List;)Lcom/navdy/service/library/events/settings/ReadSettingsResponse$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/settings/Setting;",
            ">;)",
            "Lcom/navdy/service/library/events/settings/ReadSettingsResponse$Builder;"
        }
    .end annotation

    .prologue
    .line 85
    .local p1, "settings":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/settings/Setting;>;"
    invoke-static {p1}, Lcom/navdy/service/library/events/settings/ReadSettingsResponse$Builder;->checkForNulls(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/settings/ReadSettingsResponse$Builder;->settings:Ljava/util/List;

    .line 86
    return-object p0
.end method

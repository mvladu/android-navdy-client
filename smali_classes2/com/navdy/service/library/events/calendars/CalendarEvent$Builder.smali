.class public final Lcom/navdy/service/library/events/calendars/CalendarEvent$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CalendarEvent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/calendars/CalendarEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/calendars/CalendarEvent;",
        ">;"
    }
.end annotation


# instance fields
.field public all_day:Ljava/lang/Boolean;

.field public calendar_name:Ljava/lang/String;

.field public color:Ljava/lang/Integer;

.field public destination:Lcom/navdy/service/library/events/destination/Destination;

.field public display_name:Ljava/lang/String;

.field public end_time:Ljava/lang/Long;

.field public location:Ljava/lang/String;

.field public start_time:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 139
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 140
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/calendars/CalendarEvent;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/calendars/CalendarEvent;

    .prologue
    .line 143
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 144
    if-nez p1, :cond_0

    .line 153
    :goto_0
    return-void

    .line 145
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/calendars/CalendarEvent;->display_name:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/calendars/CalendarEvent$Builder;->display_name:Ljava/lang/String;

    .line 146
    iget-object v0, p1, Lcom/navdy/service/library/events/calendars/CalendarEvent;->start_time:Ljava/lang/Long;

    iput-object v0, p0, Lcom/navdy/service/library/events/calendars/CalendarEvent$Builder;->start_time:Ljava/lang/Long;

    .line 147
    iget-object v0, p1, Lcom/navdy/service/library/events/calendars/CalendarEvent;->end_time:Ljava/lang/Long;

    iput-object v0, p0, Lcom/navdy/service/library/events/calendars/CalendarEvent$Builder;->end_time:Ljava/lang/Long;

    .line 148
    iget-object v0, p1, Lcom/navdy/service/library/events/calendars/CalendarEvent;->all_day:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/calendars/CalendarEvent$Builder;->all_day:Ljava/lang/Boolean;

    .line 149
    iget-object v0, p1, Lcom/navdy/service/library/events/calendars/CalendarEvent;->color:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/navdy/service/library/events/calendars/CalendarEvent$Builder;->color:Ljava/lang/Integer;

    .line 150
    iget-object v0, p1, Lcom/navdy/service/library/events/calendars/CalendarEvent;->location:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/calendars/CalendarEvent$Builder;->location:Ljava/lang/String;

    .line 151
    iget-object v0, p1, Lcom/navdy/service/library/events/calendars/CalendarEvent;->calendar_name:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/calendars/CalendarEvent$Builder;->calendar_name:Ljava/lang/String;

    .line 152
    iget-object v0, p1, Lcom/navdy/service/library/events/calendars/CalendarEvent;->destination:Lcom/navdy/service/library/events/destination/Destination;

    iput-object v0, p0, Lcom/navdy/service/library/events/calendars/CalendarEvent$Builder;->destination:Lcom/navdy/service/library/events/destination/Destination;

    goto :goto_0
.end method


# virtual methods
.method public all_day(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/calendars/CalendarEvent$Builder;
    .locals 0
    .param p1, "all_day"    # Ljava/lang/Boolean;

    .prologue
    .line 183
    iput-object p1, p0, Lcom/navdy/service/library/events/calendars/CalendarEvent$Builder;->all_day:Ljava/lang/Boolean;

    .line 184
    return-object p0
.end method

.method public build()Lcom/navdy/service/library/events/calendars/CalendarEvent;
    .locals 2

    .prologue
    .line 224
    new-instance v0, Lcom/navdy/service/library/events/calendars/CalendarEvent;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/calendars/CalendarEvent;-><init>(Lcom/navdy/service/library/events/calendars/CalendarEvent$Builder;Lcom/navdy/service/library/events/calendars/CalendarEvent$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 128
    invoke-virtual {p0}, Lcom/navdy/service/library/events/calendars/CalendarEvent$Builder;->build()Lcom/navdy/service/library/events/calendars/CalendarEvent;

    move-result-object v0

    return-object v0
.end method

.method public calendar_name(Ljava/lang/String;)Lcom/navdy/service/library/events/calendars/CalendarEvent$Builder;
    .locals 0
    .param p1, "calendar_name"    # Ljava/lang/String;

    .prologue
    .line 209
    iput-object p1, p0, Lcom/navdy/service/library/events/calendars/CalendarEvent$Builder;->calendar_name:Ljava/lang/String;

    .line 210
    return-object p0
.end method

.method public color(Ljava/lang/Integer;)Lcom/navdy/service/library/events/calendars/CalendarEvent$Builder;
    .locals 0
    .param p1, "color"    # Ljava/lang/Integer;

    .prologue
    .line 192
    iput-object p1, p0, Lcom/navdy/service/library/events/calendars/CalendarEvent$Builder;->color:Ljava/lang/Integer;

    .line 193
    return-object p0
.end method

.method public destination(Lcom/navdy/service/library/events/destination/Destination;)Lcom/navdy/service/library/events/calendars/CalendarEvent$Builder;
    .locals 0
    .param p1, "destination"    # Lcom/navdy/service/library/events/destination/Destination;

    .prologue
    .line 218
    iput-object p1, p0, Lcom/navdy/service/library/events/calendars/CalendarEvent$Builder;->destination:Lcom/navdy/service/library/events/destination/Destination;

    .line 219
    return-object p0
.end method

.method public display_name(Ljava/lang/String;)Lcom/navdy/service/library/events/calendars/CalendarEvent$Builder;
    .locals 0
    .param p1, "display_name"    # Ljava/lang/String;

    .prologue
    .line 159
    iput-object p1, p0, Lcom/navdy/service/library/events/calendars/CalendarEvent$Builder;->display_name:Ljava/lang/String;

    .line 160
    return-object p0
.end method

.method public end_time(Ljava/lang/Long;)Lcom/navdy/service/library/events/calendars/CalendarEvent$Builder;
    .locals 0
    .param p1, "end_time"    # Ljava/lang/Long;

    .prologue
    .line 175
    iput-object p1, p0, Lcom/navdy/service/library/events/calendars/CalendarEvent$Builder;->end_time:Ljava/lang/Long;

    .line 176
    return-object p0
.end method

.method public location(Ljava/lang/String;)Lcom/navdy/service/library/events/calendars/CalendarEvent$Builder;
    .locals 0
    .param p1, "location"    # Ljava/lang/String;

    .prologue
    .line 200
    iput-object p1, p0, Lcom/navdy/service/library/events/calendars/CalendarEvent$Builder;->location:Ljava/lang/String;

    .line 201
    return-object p0
.end method

.method public start_time(Ljava/lang/Long;)Lcom/navdy/service/library/events/calendars/CalendarEvent$Builder;
    .locals 0
    .param p1, "start_time"    # Ljava/lang/Long;

    .prologue
    .line 167
    iput-object p1, p0, Lcom/navdy/service/library/events/calendars/CalendarEvent$Builder;->start_time:Ljava/lang/Long;

    .line 168
    return-object p0
.end method

.class public final enum Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;
.super Ljava/lang/Enum;
.source "AccelerateShutdown.java"

# interfaces
.implements Lcom/squareup/wire/ProtoEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AccelerateReason"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;",
        ">;",
        "Lcom/squareup/wire/ProtoEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;

.field public static final enum ACCELERATE_REASON_HFP_DISCONNECT:Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;

.field public static final enum ACCELERATE_REASON_UNKNOWN:Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;

.field public static final enum ACCELERATE_REASON_WALKING:Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 71
    new-instance v0, Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;

    const-string v1, "ACCELERATE_REASON_UNKNOWN"

    invoke-direct {v0, v1, v4, v2}, Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;->ACCELERATE_REASON_UNKNOWN:Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;

    .line 72
    new-instance v0, Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;

    const-string v1, "ACCELERATE_REASON_HFP_DISCONNECT"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;->ACCELERATE_REASON_HFP_DISCONNECT:Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;

    .line 73
    new-instance v0, Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;

    const-string v1, "ACCELERATE_REASON_WALKING"

    invoke-direct {v0, v1, v3, v5}, Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;->ACCELERATE_REASON_WALKING:Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;

    .line 69
    new-array v0, v5, [Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;

    sget-object v1, Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;->ACCELERATE_REASON_UNKNOWN:Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;->ACCELERATE_REASON_HFP_DISCONNECT:Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;->ACCELERATE_REASON_WALKING:Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;

    aput-object v1, v0, v3

    sput-object v0, Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;->$VALUES:[Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 77
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 78
    iput p3, p0, Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;->value:I

    .line 79
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 69
    const-class v0, Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;

    return-object v0
.end method

.method public static values()[Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;
    .locals 1

    .prologue
    .line 69
    sget-object v0, Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;->$VALUES:[Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;

    invoke-virtual {v0}, [Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 83
    iget v0, p0, Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;->value:I

    return v0
.end method

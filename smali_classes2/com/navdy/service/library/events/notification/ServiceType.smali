.class public final enum Lcom/navdy/service/library/events/notification/ServiceType;
.super Ljava/lang/Enum;
.source "ServiceType.java"

# interfaces
.implements Lcom/squareup/wire/ProtoEnum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/service/library/events/notification/ServiceType;",
        ">;",
        "Lcom/squareup/wire/ProtoEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/service/library/events/notification/ServiceType;

.field public static final enum SERVICE_ANCS:Lcom/navdy/service/library/events/notification/ServiceType;

.field public static final enum SERVICE_PANDORA:Lcom/navdy/service/library/events/notification/ServiceType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 9
    new-instance v0, Lcom/navdy/service/library/events/notification/ServiceType;

    const-string v1, "SERVICE_ANCS"

    invoke-direct {v0, v1, v3, v2}, Lcom/navdy/service/library/events/notification/ServiceType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/notification/ServiceType;->SERVICE_ANCS:Lcom/navdy/service/library/events/notification/ServiceType;

    .line 10
    new-instance v0, Lcom/navdy/service/library/events/notification/ServiceType;

    const-string v1, "SERVICE_PANDORA"

    invoke-direct {v0, v1, v2, v4}, Lcom/navdy/service/library/events/notification/ServiceType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/notification/ServiceType;->SERVICE_PANDORA:Lcom/navdy/service/library/events/notification/ServiceType;

    .line 7
    new-array v0, v4, [Lcom/navdy/service/library/events/notification/ServiceType;

    sget-object v1, Lcom/navdy/service/library/events/notification/ServiceType;->SERVICE_ANCS:Lcom/navdy/service/library/events/notification/ServiceType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/service/library/events/notification/ServiceType;->SERVICE_PANDORA:Lcom/navdy/service/library/events/notification/ServiceType;

    aput-object v1, v0, v2

    sput-object v0, Lcom/navdy/service/library/events/notification/ServiceType;->$VALUES:[Lcom/navdy/service/library/events/notification/ServiceType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 15
    iput p3, p0, Lcom/navdy/service/library/events/notification/ServiceType;->value:I

    .line 16
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/notification/ServiceType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 7
    const-class v0, Lcom/navdy/service/library/events/notification/ServiceType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/notification/ServiceType;

    return-object v0
.end method

.method public static values()[Lcom/navdy/service/library/events/notification/ServiceType;
    .locals 1

    .prologue
    .line 7
    sget-object v0, Lcom/navdy/service/library/events/notification/ServiceType;->$VALUES:[Lcom/navdy/service/library/events/notification/ServiceType;

    invoke-virtual {v0}, [Lcom/navdy/service/library/events/notification/ServiceType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/events/notification/ServiceType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 20
    iget v0, p0, Lcom/navdy/service/library/events/notification/ServiceType;->value:I

    return v0
.end method

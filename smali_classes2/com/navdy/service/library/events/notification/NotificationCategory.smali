.class public final enum Lcom/navdy/service/library/events/notification/NotificationCategory;
.super Ljava/lang/Enum;
.source "NotificationCategory.java"

# interfaces
.implements Lcom/squareup/wire/ProtoEnum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/service/library/events/notification/NotificationCategory;",
        ">;",
        "Lcom/squareup/wire/ProtoEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/service/library/events/notification/NotificationCategory;

.field public static final enum CATEGORY_BUSINESS_AND_FINANCE:Lcom/navdy/service/library/events/notification/NotificationCategory;

.field public static final enum CATEGORY_EMAIL:Lcom/navdy/service/library/events/notification/NotificationCategory;

.field public static final enum CATEGORY_ENTERTAINMENT:Lcom/navdy/service/library/events/notification/NotificationCategory;

.field public static final enum CATEGORY_HEALTH_AND_FITNESS:Lcom/navdy/service/library/events/notification/NotificationCategory;

.field public static final enum CATEGORY_INCOMING_CALL:Lcom/navdy/service/library/events/notification/NotificationCategory;

.field public static final enum CATEGORY_LOCATION:Lcom/navdy/service/library/events/notification/NotificationCategory;

.field public static final enum CATEGORY_MISSED_CALL:Lcom/navdy/service/library/events/notification/NotificationCategory;

.field public static final enum CATEGORY_NEWS:Lcom/navdy/service/library/events/notification/NotificationCategory;

.field public static final enum CATEGORY_OTHER:Lcom/navdy/service/library/events/notification/NotificationCategory;

.field public static final enum CATEGORY_SCHEDULE:Lcom/navdy/service/library/events/notification/NotificationCategory;

.field public static final enum CATEGORY_SOCIAL:Lcom/navdy/service/library/events/notification/NotificationCategory;

.field public static final enum CATEGORY_VOICE_MAIL:Lcom/navdy/service/library/events/notification/NotificationCategory;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 9
    new-instance v0, Lcom/navdy/service/library/events/notification/NotificationCategory;

    const-string v1, "CATEGORY_OTHER"

    invoke-direct {v0, v1, v4, v4}, Lcom/navdy/service/library/events/notification/NotificationCategory;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/notification/NotificationCategory;->CATEGORY_OTHER:Lcom/navdy/service/library/events/notification/NotificationCategory;

    .line 10
    new-instance v0, Lcom/navdy/service/library/events/notification/NotificationCategory;

    const-string v1, "CATEGORY_INCOMING_CALL"

    invoke-direct {v0, v1, v5, v5}, Lcom/navdy/service/library/events/notification/NotificationCategory;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/notification/NotificationCategory;->CATEGORY_INCOMING_CALL:Lcom/navdy/service/library/events/notification/NotificationCategory;

    .line 11
    new-instance v0, Lcom/navdy/service/library/events/notification/NotificationCategory;

    const-string v1, "CATEGORY_MISSED_CALL"

    invoke-direct {v0, v1, v6, v6}, Lcom/navdy/service/library/events/notification/NotificationCategory;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/notification/NotificationCategory;->CATEGORY_MISSED_CALL:Lcom/navdy/service/library/events/notification/NotificationCategory;

    .line 12
    new-instance v0, Lcom/navdy/service/library/events/notification/NotificationCategory;

    const-string v1, "CATEGORY_VOICE_MAIL"

    invoke-direct {v0, v1, v7, v7}, Lcom/navdy/service/library/events/notification/NotificationCategory;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/notification/NotificationCategory;->CATEGORY_VOICE_MAIL:Lcom/navdy/service/library/events/notification/NotificationCategory;

    .line 13
    new-instance v0, Lcom/navdy/service/library/events/notification/NotificationCategory;

    const-string v1, "CATEGORY_SOCIAL"

    invoke-direct {v0, v1, v8, v8}, Lcom/navdy/service/library/events/notification/NotificationCategory;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/notification/NotificationCategory;->CATEGORY_SOCIAL:Lcom/navdy/service/library/events/notification/NotificationCategory;

    .line 14
    new-instance v0, Lcom/navdy/service/library/events/notification/NotificationCategory;

    const-string v1, "CATEGORY_SCHEDULE"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/notification/NotificationCategory;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/notification/NotificationCategory;->CATEGORY_SCHEDULE:Lcom/navdy/service/library/events/notification/NotificationCategory;

    .line 15
    new-instance v0, Lcom/navdy/service/library/events/notification/NotificationCategory;

    const-string v1, "CATEGORY_EMAIL"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/notification/NotificationCategory;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/notification/NotificationCategory;->CATEGORY_EMAIL:Lcom/navdy/service/library/events/notification/NotificationCategory;

    .line 16
    new-instance v0, Lcom/navdy/service/library/events/notification/NotificationCategory;

    const-string v1, "CATEGORY_NEWS"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/notification/NotificationCategory;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/notification/NotificationCategory;->CATEGORY_NEWS:Lcom/navdy/service/library/events/notification/NotificationCategory;

    .line 17
    new-instance v0, Lcom/navdy/service/library/events/notification/NotificationCategory;

    const-string v1, "CATEGORY_HEALTH_AND_FITNESS"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/notification/NotificationCategory;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/notification/NotificationCategory;->CATEGORY_HEALTH_AND_FITNESS:Lcom/navdy/service/library/events/notification/NotificationCategory;

    .line 18
    new-instance v0, Lcom/navdy/service/library/events/notification/NotificationCategory;

    const-string v1, "CATEGORY_BUSINESS_AND_FINANCE"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/notification/NotificationCategory;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/notification/NotificationCategory;->CATEGORY_BUSINESS_AND_FINANCE:Lcom/navdy/service/library/events/notification/NotificationCategory;

    .line 19
    new-instance v0, Lcom/navdy/service/library/events/notification/NotificationCategory;

    const-string v1, "CATEGORY_LOCATION"

    const/16 v2, 0xa

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/notification/NotificationCategory;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/notification/NotificationCategory;->CATEGORY_LOCATION:Lcom/navdy/service/library/events/notification/NotificationCategory;

    .line 20
    new-instance v0, Lcom/navdy/service/library/events/notification/NotificationCategory;

    const-string v1, "CATEGORY_ENTERTAINMENT"

    const/16 v2, 0xb

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/notification/NotificationCategory;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/notification/NotificationCategory;->CATEGORY_ENTERTAINMENT:Lcom/navdy/service/library/events/notification/NotificationCategory;

    .line 7
    const/16 v0, 0xc

    new-array v0, v0, [Lcom/navdy/service/library/events/notification/NotificationCategory;

    sget-object v1, Lcom/navdy/service/library/events/notification/NotificationCategory;->CATEGORY_OTHER:Lcom/navdy/service/library/events/notification/NotificationCategory;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/service/library/events/notification/NotificationCategory;->CATEGORY_INCOMING_CALL:Lcom/navdy/service/library/events/notification/NotificationCategory;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/service/library/events/notification/NotificationCategory;->CATEGORY_MISSED_CALL:Lcom/navdy/service/library/events/notification/NotificationCategory;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/service/library/events/notification/NotificationCategory;->CATEGORY_VOICE_MAIL:Lcom/navdy/service/library/events/notification/NotificationCategory;

    aput-object v1, v0, v7

    sget-object v1, Lcom/navdy/service/library/events/notification/NotificationCategory;->CATEGORY_SOCIAL:Lcom/navdy/service/library/events/notification/NotificationCategory;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/navdy/service/library/events/notification/NotificationCategory;->CATEGORY_SCHEDULE:Lcom/navdy/service/library/events/notification/NotificationCategory;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/navdy/service/library/events/notification/NotificationCategory;->CATEGORY_EMAIL:Lcom/navdy/service/library/events/notification/NotificationCategory;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/navdy/service/library/events/notification/NotificationCategory;->CATEGORY_NEWS:Lcom/navdy/service/library/events/notification/NotificationCategory;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/navdy/service/library/events/notification/NotificationCategory;->CATEGORY_HEALTH_AND_FITNESS:Lcom/navdy/service/library/events/notification/NotificationCategory;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/navdy/service/library/events/notification/NotificationCategory;->CATEGORY_BUSINESS_AND_FINANCE:Lcom/navdy/service/library/events/notification/NotificationCategory;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/navdy/service/library/events/notification/NotificationCategory;->CATEGORY_LOCATION:Lcom/navdy/service/library/events/notification/NotificationCategory;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/navdy/service/library/events/notification/NotificationCategory;->CATEGORY_ENTERTAINMENT:Lcom/navdy/service/library/events/notification/NotificationCategory;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/service/library/events/notification/NotificationCategory;->$VALUES:[Lcom/navdy/service/library/events/notification/NotificationCategory;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 25
    iput p3, p0, Lcom/navdy/service/library/events/notification/NotificationCategory;->value:I

    .line 26
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/notification/NotificationCategory;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 7
    const-class v0, Lcom/navdy/service/library/events/notification/NotificationCategory;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/notification/NotificationCategory;

    return-object v0
.end method

.method public static values()[Lcom/navdy/service/library/events/notification/NotificationCategory;
    .locals 1

    .prologue
    .line 7
    sget-object v0, Lcom/navdy/service/library/events/notification/NotificationCategory;->$VALUES:[Lcom/navdy/service/library/events/notification/NotificationCategory;

    invoke-virtual {v0}, [Lcom/navdy/service/library/events/notification/NotificationCategory;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/events/notification/NotificationCategory;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 30
    iget v0, p0, Lcom/navdy/service/library/events/notification/NotificationCategory;->value:I

    return v0
.end method

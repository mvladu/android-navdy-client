.class public final enum Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;
.super Ljava/lang/Enum;
.source "ConnectionRequest.java"

# interfaces
.implements Lcom/squareup/wire/ProtoEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/connection/ConnectionRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Action"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;",
        ">;",
        "Lcom/squareup/wire/ProtoEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;

.field public static final enum CONNECTION_SELECT:Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;

.field public static final enum CONNECTION_START_SEARCH:Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;

.field public static final enum CONNECTION_STOP_SEARCH:Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 105
    new-instance v0, Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;

    const-string v1, "CONNECTION_START_SEARCH"

    invoke-direct {v0, v1, v4, v2}, Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;->CONNECTION_START_SEARCH:Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;

    .line 109
    new-instance v0, Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;

    const-string v1, "CONNECTION_STOP_SEARCH"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;->CONNECTION_STOP_SEARCH:Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;

    .line 115
    new-instance v0, Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;

    const-string v1, "CONNECTION_SELECT"

    invoke-direct {v0, v1, v3, v5}, Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;->CONNECTION_SELECT:Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;

    .line 100
    new-array v0, v5, [Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;

    sget-object v1, Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;->CONNECTION_START_SEARCH:Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;->CONNECTION_STOP_SEARCH:Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;->CONNECTION_SELECT:Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;

    aput-object v1, v0, v3

    sput-object v0, Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;->$VALUES:[Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 119
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 120
    iput p3, p0, Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;->value:I

    .line 121
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 100
    const-class v0, Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;

    return-object v0
.end method

.method public static values()[Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;
    .locals 1

    .prologue
    .line 100
    sget-object v0, Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;->$VALUES:[Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;

    invoke-virtual {v0}, [Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 125
    iget v0, p0, Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;->value:I

    return v0
.end method

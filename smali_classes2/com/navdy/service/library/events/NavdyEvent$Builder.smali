.class public final Lcom/navdy/service/library/events/NavdyEvent$Builder;
.super Lcom/squareup/wire/ExtendableMessage$ExtendableBuilder;
.source "NavdyEvent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/NavdyEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ExtendableMessage$ExtendableBuilder",
        "<",
        "Lcom/navdy/service/library/events/NavdyEvent;",
        ">;"
    }
.end annotation


# instance fields
.field public type:Lcom/navdy/service/library/events/NavdyEvent$MessageType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/squareup/wire/ExtendableMessage$ExtendableBuilder;-><init>()V

    .line 55
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/NavdyEvent;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/NavdyEvent;

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/squareup/wire/ExtendableMessage$ExtendableBuilder;-><init>(Lcom/squareup/wire/ExtendableMessage;)V

    .line 59
    if-nez p1, :cond_0

    .line 61
    :goto_0
    return-void

    .line 60
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/NavdyEvent;->type:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    iput-object v0, p0, Lcom/navdy/service/library/events/NavdyEvent$Builder;->type:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/NavdyEvent;
    .locals 2

    .prologue
    .line 76
    invoke-virtual {p0}, Lcom/navdy/service/library/events/NavdyEvent$Builder;->checkRequiredFields()V

    .line 77
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/NavdyEvent;-><init>(Lcom/navdy/service/library/events/NavdyEvent$Builder;Lcom/navdy/service/library/events/NavdyEvent$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 50
    invoke-virtual {p0}, Lcom/navdy/service/library/events/NavdyEvent$Builder;->build()Lcom/navdy/service/library/events/NavdyEvent;

    move-result-object v0

    return-object v0
.end method

.method public setExtension(Lcom/squareup/wire/Extension;Ljava/lang/Object;)Lcom/navdy/service/library/events/NavdyEvent$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/wire/Extension",
            "<",
            "Lcom/navdy/service/library/events/NavdyEvent;",
            "TE;>;TE;)",
            "Lcom/navdy/service/library/events/NavdyEvent$Builder;"
        }
    .end annotation

    .prologue
    .line 70
    .local p1, "extension":Lcom/squareup/wire/Extension;, "Lcom/squareup/wire/Extension<Lcom/navdy/service/library/events/NavdyEvent;TE;>;"
    .local p2, "value":Ljava/lang/Object;, "TE;"
    invoke-super {p0, p1, p2}, Lcom/squareup/wire/ExtendableMessage$ExtendableBuilder;->setExtension(Lcom/squareup/wire/Extension;Ljava/lang/Object;)Lcom/squareup/wire/ExtendableMessage$ExtendableBuilder;

    .line 71
    return-object p0
.end method

.method public bridge synthetic setExtension(Lcom/squareup/wire/Extension;Ljava/lang/Object;)Lcom/squareup/wire/ExtendableMessage$ExtendableBuilder;
    .locals 1

    .prologue
    .line 50
    invoke-virtual {p0, p1, p2}, Lcom/navdy/service/library/events/NavdyEvent$Builder;->setExtension(Lcom/squareup/wire/Extension;Ljava/lang/Object;)Lcom/navdy/service/library/events/NavdyEvent$Builder;

    move-result-object v0

    return-object v0
.end method

.method public type(Lcom/navdy/service/library/events/NavdyEvent$MessageType;)Lcom/navdy/service/library/events/NavdyEvent$Builder;
    .locals 0
    .param p1, "type"    # Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/navdy/service/library/events/NavdyEvent$Builder;->type:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 65
    return-object p0
.end method

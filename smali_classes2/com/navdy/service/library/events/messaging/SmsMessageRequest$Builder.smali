.class public final Lcom/navdy/service/library/events/messaging/SmsMessageRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SmsMessageRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/messaging/SmsMessageRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/messaging/SmsMessageRequest;",
        ">;"
    }
.end annotation


# instance fields
.field public id:Ljava/lang/String;

.field public message:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public number:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 78
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/messaging/SmsMessageRequest;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/messaging/SmsMessageRequest;

    .prologue
    .line 81
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 82
    if-nez p1, :cond_0

    .line 87
    :goto_0
    return-void

    .line 83
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/messaging/SmsMessageRequest;->number:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/messaging/SmsMessageRequest$Builder;->number:Ljava/lang/String;

    .line 84
    iget-object v0, p1, Lcom/navdy/service/library/events/messaging/SmsMessageRequest;->message:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/messaging/SmsMessageRequest$Builder;->message:Ljava/lang/String;

    .line 85
    iget-object v0, p1, Lcom/navdy/service/library/events/messaging/SmsMessageRequest;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/messaging/SmsMessageRequest$Builder;->name:Ljava/lang/String;

    .line 86
    iget-object v0, p1, Lcom/navdy/service/library/events/messaging/SmsMessageRequest;->id:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/messaging/SmsMessageRequest$Builder;->id:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/messaging/SmsMessageRequest;
    .locals 2

    .prologue
    .line 111
    invoke-virtual {p0}, Lcom/navdy/service/library/events/messaging/SmsMessageRequest$Builder;->checkRequiredFields()V

    .line 112
    new-instance v0, Lcom/navdy/service/library/events/messaging/SmsMessageRequest;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/messaging/SmsMessageRequest;-><init>(Lcom/navdy/service/library/events/messaging/SmsMessageRequest$Builder;Lcom/navdy/service/library/events/messaging/SmsMessageRequest$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 70
    invoke-virtual {p0}, Lcom/navdy/service/library/events/messaging/SmsMessageRequest$Builder;->build()Lcom/navdy/service/library/events/messaging/SmsMessageRequest;

    move-result-object v0

    return-object v0
.end method

.method public id(Ljava/lang/String;)Lcom/navdy/service/library/events/messaging/SmsMessageRequest$Builder;
    .locals 0
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/navdy/service/library/events/messaging/SmsMessageRequest$Builder;->id:Ljava/lang/String;

    .line 106
    return-object p0
.end method

.method public message(Ljava/lang/String;)Lcom/navdy/service/library/events/messaging/SmsMessageRequest$Builder;
    .locals 0
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 95
    iput-object p1, p0, Lcom/navdy/service/library/events/messaging/SmsMessageRequest$Builder;->message:Ljava/lang/String;

    .line 96
    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/navdy/service/library/events/messaging/SmsMessageRequest$Builder;
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 100
    iput-object p1, p0, Lcom/navdy/service/library/events/messaging/SmsMessageRequest$Builder;->name:Ljava/lang/String;

    .line 101
    return-object p0
.end method

.method public number(Ljava/lang/String;)Lcom/navdy/service/library/events/messaging/SmsMessageRequest$Builder;
    .locals 0
    .param p1, "number"    # Ljava/lang/String;

    .prologue
    .line 90
    iput-object p1, p0, Lcom/navdy/service/library/events/messaging/SmsMessageRequest$Builder;->number:Ljava/lang/String;

    .line 91
    return-object p0
.end method

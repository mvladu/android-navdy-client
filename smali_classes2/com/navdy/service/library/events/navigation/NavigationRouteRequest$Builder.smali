.class public final Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "NavigationRouteRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;",
        ">;"
    }
.end annotation


# instance fields
.field public autoNavigate:Ljava/lang/Boolean;

.field public cancelCurrent:Ljava/lang/Boolean;

.field public destination:Lcom/navdy/service/library/events/location/Coordinate;

.field public destinationDisplay:Lcom/navdy/service/library/events/location/Coordinate;

.field public destinationType:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

.field public destination_identifier:Ljava/lang/String;

.field public geoCodeStreetAddress:Ljava/lang/Boolean;

.field public label:Ljava/lang/String;

.field public originDisplay:Ljava/lang/Boolean;

.field public requestDestination:Lcom/navdy/service/library/events/destination/Destination;

.field public requestId:Ljava/lang/String;

.field public routeAttributes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$RouteAttribute;",
            ">;"
        }
    .end annotation
.end field

.field public streetAddress:Ljava/lang/String;

.field public waypoints:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/location/Coordinate;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 215
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 216
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    .prologue
    .line 219
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 220
    if-nez p1, :cond_0

    .line 235
    :goto_0
    return-void

    .line 221
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destination:Lcom/navdy/service/library/events/location/Coordinate;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->destination:Lcom/navdy/service/library/events/location/Coordinate;

    .line 222
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->label:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->label:Ljava/lang/String;

    .line 223
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->waypoints:Ljava/util/List;

    invoke-static {v0}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->access$000(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->waypoints:Ljava/util/List;

    .line 224
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->streetAddress:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->streetAddress:Ljava/lang/String;

    .line 225
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->geoCodeStreetAddress:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->geoCodeStreetAddress:Ljava/lang/Boolean;

    .line 226
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->autoNavigate:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->autoNavigate:Ljava/lang/Boolean;

    .line 227
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destination_identifier:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->destination_identifier:Ljava/lang/String;

    .line 228
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destinationType:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->destinationType:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    .line 229
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->originDisplay:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->originDisplay:Ljava/lang/Boolean;

    .line 230
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->cancelCurrent:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->cancelCurrent:Ljava/lang/Boolean;

    .line 231
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->requestId:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->requestId:Ljava/lang/String;

    .line 232
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->destinationDisplay:Lcom/navdy/service/library/events/location/Coordinate;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->destinationDisplay:Lcom/navdy/service/library/events/location/Coordinate;

    .line 233
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->routeAttributes:Ljava/util/List;

    invoke-static {v0}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->access$100(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->routeAttributes:Ljava/util/List;

    .line 234
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;->requestDestination:Lcom/navdy/service/library/events/destination/Destination;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->requestDestination:Lcom/navdy/service/library/events/destination/Destination;

    goto :goto_0
.end method


# virtual methods
.method public autoNavigate(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;
    .locals 0
    .param p1, "autoNavigate"    # Ljava/lang/Boolean;

    .prologue
    .line 282
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->autoNavigate:Ljava/lang/Boolean;

    .line 283
    return-object p0
.end method

.method public build()Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
    .locals 2

    .prologue
    .line 360
    invoke-virtual {p0}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->checkRequiredFields()V

    .line 361
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;-><init>(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 198
    invoke-virtual {p0}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->build()Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;

    move-result-object v0

    return-object v0
.end method

.method public cancelCurrent(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;
    .locals 0
    .param p1, "cancelCurrent"    # Ljava/lang/Boolean;

    .prologue
    .line 319
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->cancelCurrent:Ljava/lang/Boolean;

    .line 320
    return-object p0
.end method

.method public destination(Lcom/navdy/service/library/events/location/Coordinate;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;
    .locals 0
    .param p1, "destination"    # Lcom/navdy/service/library/events/location/Coordinate;

    .prologue
    .line 241
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->destination:Lcom/navdy/service/library/events/location/Coordinate;

    .line 242
    return-object p0
.end method

.method public destinationDisplay(Lcom/navdy/service/library/events/location/Coordinate;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;
    .locals 0
    .param p1, "destinationDisplay"    # Lcom/navdy/service/library/events/location/Coordinate;

    .prologue
    .line 336
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->destinationDisplay:Lcom/navdy/service/library/events/location/Coordinate;

    .line 337
    return-object p0
.end method

.method public destinationType(Lcom/navdy/service/library/events/destination/Destination$FavoriteType;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;
    .locals 0
    .param p1, "destinationType"    # Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    .prologue
    .line 298
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->destinationType:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    .line 299
    return-object p0
.end method

.method public destination_identifier(Ljava/lang/String;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;
    .locals 0
    .param p1, "destination_identifier"    # Ljava/lang/String;

    .prologue
    .line 290
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->destination_identifier:Ljava/lang/String;

    .line 291
    return-object p0
.end method

.method public geoCodeStreetAddress(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;
    .locals 0
    .param p1, "geoCodeStreetAddress"    # Ljava/lang/Boolean;

    .prologue
    .line 274
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->geoCodeStreetAddress:Ljava/lang/Boolean;

    .line 275
    return-object p0
.end method

.method public label(Ljava/lang/String;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;
    .locals 0
    .param p1, "label"    # Ljava/lang/String;

    .prologue
    .line 249
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->label:Ljava/lang/String;

    .line 250
    return-object p0
.end method

.method public originDisplay(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;
    .locals 0
    .param p1, "originDisplay"    # Ljava/lang/Boolean;

    .prologue
    .line 311
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->originDisplay:Ljava/lang/Boolean;

    .line 312
    return-object p0
.end method

.method public requestDestination(Lcom/navdy/service/library/events/destination/Destination;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;
    .locals 0
    .param p1, "requestDestination"    # Lcom/navdy/service/library/events/destination/Destination;

    .prologue
    .line 354
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->requestDestination:Lcom/navdy/service/library/events/destination/Destination;

    .line 355
    return-object p0
.end method

.method public requestId(Ljava/lang/String;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;
    .locals 0
    .param p1, "requestId"    # Ljava/lang/String;

    .prologue
    .line 328
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->requestId:Ljava/lang/String;

    .line 329
    return-object p0
.end method

.method public routeAttributes(Ljava/util/List;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$RouteAttribute;",
            ">;)",
            "Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;"
        }
    .end annotation

    .prologue
    .line 344
    .local p1, "routeAttributes":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$RouteAttribute;>;"
    invoke-static {p1}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->checkForNulls(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->routeAttributes:Ljava/util/List;

    .line 345
    return-object p0
.end method

.method public streetAddress(Ljava/lang/String;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;
    .locals 0
    .param p1, "streetAddress"    # Ljava/lang/String;

    .prologue
    .line 265
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->streetAddress:Ljava/lang/String;

    .line 266
    return-object p0
.end method

.method public waypoints(Ljava/util/List;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/location/Coordinate;",
            ">;)",
            "Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;"
        }
    .end annotation

    .prologue
    .line 257
    .local p1, "waypoints":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/location/Coordinate;>;"
    invoke-static {p1}, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->checkForNulls(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;->waypoints:Ljava/util/List;

    .line 258
    return-object p0
.end method

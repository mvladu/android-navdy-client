.class public final Lcom/navdy/service/library/events/contacts/Contact$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Contact.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/contacts/Contact;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/contacts/Contact;",
        ">;"
    }
.end annotation


# instance fields
.field public label:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public number:Ljava/lang/String;

.field public numberType:Lcom/navdy/service/library/events/contacts/PhoneNumberType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 89
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 90
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/contacts/Contact;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/contacts/Contact;

    .prologue
    .line 93
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 94
    if-nez p1, :cond_0

    .line 99
    :goto_0
    return-void

    .line 95
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/contacts/Contact;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/contacts/Contact$Builder;->name:Ljava/lang/String;

    .line 96
    iget-object v0, p1, Lcom/navdy/service/library/events/contacts/Contact;->number:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/contacts/Contact$Builder;->number:Ljava/lang/String;

    .line 97
    iget-object v0, p1, Lcom/navdy/service/library/events/contacts/Contact;->numberType:Lcom/navdy/service/library/events/contacts/PhoneNumberType;

    iput-object v0, p0, Lcom/navdy/service/library/events/contacts/Contact$Builder;->numberType:Lcom/navdy/service/library/events/contacts/PhoneNumberType;

    .line 98
    iget-object v0, p1, Lcom/navdy/service/library/events/contacts/Contact;->label:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/contacts/Contact$Builder;->label:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/contacts/Contact;
    .locals 2

    .prologue
    .line 135
    new-instance v0, Lcom/navdy/service/library/events/contacts/Contact;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/contacts/Contact;-><init>(Lcom/navdy/service/library/events/contacts/Contact$Builder;Lcom/navdy/service/library/events/contacts/Contact$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 82
    invoke-virtual {p0}, Lcom/navdy/service/library/events/contacts/Contact$Builder;->build()Lcom/navdy/service/library/events/contacts/Contact;

    move-result-object v0

    return-object v0
.end method

.method public label(Ljava/lang/String;)Lcom/navdy/service/library/events/contacts/Contact$Builder;
    .locals 0
    .param p1, "label"    # Ljava/lang/String;

    .prologue
    .line 129
    iput-object p1, p0, Lcom/navdy/service/library/events/contacts/Contact$Builder;->label:Ljava/lang/String;

    .line 130
    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/navdy/service/library/events/contacts/Contact$Builder;
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/navdy/service/library/events/contacts/Contact$Builder;->name:Ljava/lang/String;

    .line 106
    return-object p0
.end method

.method public number(Ljava/lang/String;)Lcom/navdy/service/library/events/contacts/Contact$Builder;
    .locals 0
    .param p1, "number"    # Ljava/lang/String;

    .prologue
    .line 113
    iput-object p1, p0, Lcom/navdy/service/library/events/contacts/Contact$Builder;->number:Ljava/lang/String;

    .line 114
    return-object p0
.end method

.method public numberType(Lcom/navdy/service/library/events/contacts/PhoneNumberType;)Lcom/navdy/service/library/events/contacts/Contact$Builder;
    .locals 0
    .param p1, "numberType"    # Lcom/navdy/service/library/events/contacts/PhoneNumberType;

    .prologue
    .line 121
    iput-object p1, p0, Lcom/navdy/service/library/events/contacts/Contact$Builder;->numberType:Lcom/navdy/service/library/events/contacts/PhoneNumberType;

    .line 122
    return-object p0
.end method

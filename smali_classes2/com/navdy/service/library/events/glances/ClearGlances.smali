.class public final Lcom/navdy/service/library/events/glances/ClearGlances;
.super Lcom/squareup/wire/Message;
.source "ClearGlances.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/glances/ClearGlances$Builder;
    }
.end annotation


# static fields
.field private static final serialVersionUID:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 14
    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/glances/ClearGlances$Builder;)V
    .locals 0
    .param p1, "builder"    # Lcom/navdy/service/library/events/glances/ClearGlances$Builder;

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 17
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/glances/ClearGlances;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 18
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/glances/ClearGlances$Builder;Lcom/navdy/service/library/events/glances/ClearGlances$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/glances/ClearGlances$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/glances/ClearGlances$1;

    .prologue
    .line 10
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/glances/ClearGlances;-><init>(Lcom/navdy/service/library/events/glances/ClearGlances$Builder;)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    .line 22
    instance-of v0, p1, Lcom/navdy/service/library/events/glances/ClearGlances;

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x0

    return v0
.end method

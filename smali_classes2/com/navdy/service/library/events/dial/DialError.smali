.class public final enum Lcom/navdy/service/library/events/dial/DialError;
.super Ljava/lang/Enum;
.source "DialError.java"

# interfaces
.implements Lcom/squareup/wire/ProtoEnum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/service/library/events/dial/DialError;",
        ">;",
        "Lcom/squareup/wire/ProtoEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/service/library/events/dial/DialError;

.field public static final enum DIAL_ALREADY_PAIRED:Lcom/navdy/service/library/events/dial/DialError;

.field public static final enum DIAL_ERROR:Lcom/navdy/service/library/events/dial/DialError;

.field public static final enum DIAL_NOT_FOUND:Lcom/navdy/service/library/events/dial/DialError;

.field public static final enum DIAL_NOT_PAIRED:Lcom/navdy/service/library/events/dial/DialError;

.field public static final enum DIAL_OPERATION_IN_PROGRESS:Lcom/navdy/service/library/events/dial/DialError;

.field public static final enum DIAL_PAIRED:Lcom/navdy/service/library/events/dial/DialError;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x5

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 12
    new-instance v0, Lcom/navdy/service/library/events/dial/DialError;

    const-string v1, "DIAL_ERROR"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/dial/DialError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/dial/DialError;->DIAL_ERROR:Lcom/navdy/service/library/events/dial/DialError;

    .line 16
    new-instance v0, Lcom/navdy/service/library/events/dial/DialError;

    const-string v1, "DIAL_ALREADY_PAIRED"

    invoke-direct {v0, v1, v3, v4}, Lcom/navdy/service/library/events/dial/DialError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/dial/DialError;->DIAL_ALREADY_PAIRED:Lcom/navdy/service/library/events/dial/DialError;

    .line 20
    new-instance v0, Lcom/navdy/service/library/events/dial/DialError;

    const-string v1, "DIAL_OPERATION_IN_PROGRESS"

    invoke-direct {v0, v1, v4, v5}, Lcom/navdy/service/library/events/dial/DialError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/dial/DialError;->DIAL_OPERATION_IN_PROGRESS:Lcom/navdy/service/library/events/dial/DialError;

    .line 24
    new-instance v0, Lcom/navdy/service/library/events/dial/DialError;

    const-string v1, "DIAL_PAIRED"

    invoke-direct {v0, v1, v5, v6}, Lcom/navdy/service/library/events/dial/DialError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/dial/DialError;->DIAL_PAIRED:Lcom/navdy/service/library/events/dial/DialError;

    .line 28
    new-instance v0, Lcom/navdy/service/library/events/dial/DialError;

    const-string v1, "DIAL_NOT_FOUND"

    invoke-direct {v0, v1, v6, v7}, Lcom/navdy/service/library/events/dial/DialError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/dial/DialError;->DIAL_NOT_FOUND:Lcom/navdy/service/library/events/dial/DialError;

    .line 32
    new-instance v0, Lcom/navdy/service/library/events/dial/DialError;

    const-string v1, "DIAL_NOT_PAIRED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v7, v2}, Lcom/navdy/service/library/events/dial/DialError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/dial/DialError;->DIAL_NOT_PAIRED:Lcom/navdy/service/library/events/dial/DialError;

    .line 7
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/navdy/service/library/events/dial/DialError;

    const/4 v1, 0x0

    sget-object v2, Lcom/navdy/service/library/events/dial/DialError;->DIAL_ERROR:Lcom/navdy/service/library/events/dial/DialError;

    aput-object v2, v0, v1

    sget-object v1, Lcom/navdy/service/library/events/dial/DialError;->DIAL_ALREADY_PAIRED:Lcom/navdy/service/library/events/dial/DialError;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/service/library/events/dial/DialError;->DIAL_OPERATION_IN_PROGRESS:Lcom/navdy/service/library/events/dial/DialError;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/service/library/events/dial/DialError;->DIAL_PAIRED:Lcom/navdy/service/library/events/dial/DialError;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/service/library/events/dial/DialError;->DIAL_NOT_FOUND:Lcom/navdy/service/library/events/dial/DialError;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/service/library/events/dial/DialError;->DIAL_NOT_PAIRED:Lcom/navdy/service/library/events/dial/DialError;

    aput-object v1, v0, v7

    sput-object v0, Lcom/navdy/service/library/events/dial/DialError;->$VALUES:[Lcom/navdy/service/library/events/dial/DialError;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 37
    iput p3, p0, Lcom/navdy/service/library/events/dial/DialError;->value:I

    .line 38
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/dial/DialError;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 7
    const-class v0, Lcom/navdy/service/library/events/dial/DialError;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/dial/DialError;

    return-object v0
.end method

.method public static values()[Lcom/navdy/service/library/events/dial/DialError;
    .locals 1

    .prologue
    .line 7
    sget-object v0, Lcom/navdy/service/library/events/dial/DialError;->$VALUES:[Lcom/navdy/service/library/events/dial/DialError;

    invoke-virtual {v0}, [Lcom/navdy/service/library/events/dial/DialError;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/events/dial/DialError;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lcom/navdy/service/library/events/dial/DialError;->value:I

    return v0
.end method

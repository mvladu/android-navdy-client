.class public final Lcom/navdy/service/library/events/dial/DialBondResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DialBondResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/dial/DialBondResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/dial/DialBondResponse;",
        ">;"
    }
.end annotation


# instance fields
.field public macAddress:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public status:Lcom/navdy/service/library/events/dial/DialError;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 79
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/dial/DialBondResponse;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/dial/DialBondResponse;

    .prologue
    .line 82
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 83
    if-nez p1, :cond_0

    .line 87
    :goto_0
    return-void

    .line 84
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/dial/DialBondResponse;->status:Lcom/navdy/service/library/events/dial/DialError;

    iput-object v0, p0, Lcom/navdy/service/library/events/dial/DialBondResponse$Builder;->status:Lcom/navdy/service/library/events/dial/DialError;

    .line 85
    iget-object v0, p1, Lcom/navdy/service/library/events/dial/DialBondResponse;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/dial/DialBondResponse$Builder;->name:Ljava/lang/String;

    .line 86
    iget-object v0, p1, Lcom/navdy/service/library/events/dial/DialBondResponse;->macAddress:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/dial/DialBondResponse$Builder;->macAddress:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/dial/DialBondResponse;
    .locals 2

    .prologue
    .line 115
    new-instance v0, Lcom/navdy/service/library/events/dial/DialBondResponse;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/dial/DialBondResponse;-><init>(Lcom/navdy/service/library/events/dial/DialBondResponse$Builder;Lcom/navdy/service/library/events/dial/DialBondResponse$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 72
    invoke-virtual {p0}, Lcom/navdy/service/library/events/dial/DialBondResponse$Builder;->build()Lcom/navdy/service/library/events/dial/DialBondResponse;

    move-result-object v0

    return-object v0
.end method

.method public macAddress(Ljava/lang/String;)Lcom/navdy/service/library/events/dial/DialBondResponse$Builder;
    .locals 0
    .param p1, "macAddress"    # Ljava/lang/String;

    .prologue
    .line 109
    iput-object p1, p0, Lcom/navdy/service/library/events/dial/DialBondResponse$Builder;->macAddress:Ljava/lang/String;

    .line 110
    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/navdy/service/library/events/dial/DialBondResponse$Builder;
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 101
    iput-object p1, p0, Lcom/navdy/service/library/events/dial/DialBondResponse$Builder;->name:Ljava/lang/String;

    .line 102
    return-object p0
.end method

.method public status(Lcom/navdy/service/library/events/dial/DialError;)Lcom/navdy/service/library/events/dial/DialBondResponse$Builder;
    .locals 0
    .param p1, "status"    # Lcom/navdy/service/library/events/dial/DialError;

    .prologue
    .line 93
    iput-object p1, p0, Lcom/navdy/service/library/events/dial/DialBondResponse$Builder;->status:Lcom/navdy/service/library/events/dial/DialError;

    .line 94
    return-object p0
.end method

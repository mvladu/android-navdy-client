.class public final Lcom/navdy/service/library/events/debug/StopDrivePlaybackEvent$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "StopDrivePlaybackEvent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/debug/StopDrivePlaybackEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/debug/StopDrivePlaybackEvent;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 34
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/debug/StopDrivePlaybackEvent;)V
    .locals 0
    .param p1, "message"    # Lcom/navdy/service/library/events/debug/StopDrivePlaybackEvent;

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 38
    return-void
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/debug/StopDrivePlaybackEvent;
    .locals 2

    .prologue
    .line 42
    new-instance v0, Lcom/navdy/service/library/events/debug/StopDrivePlaybackEvent;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/debug/StopDrivePlaybackEvent;-><init>(Lcom/navdy/service/library/events/debug/StopDrivePlaybackEvent$Builder;Lcom/navdy/service/library/events/debug/StopDrivePlaybackEvent$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/navdy/service/library/events/debug/StopDrivePlaybackEvent$Builder;->build()Lcom/navdy/service/library/events/debug/StopDrivePlaybackEvent;

    move-result-object v0

    return-object v0
.end method

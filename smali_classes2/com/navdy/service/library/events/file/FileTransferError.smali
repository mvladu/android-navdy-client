.class public final enum Lcom/navdy/service/library/events/file/FileTransferError;
.super Ljava/lang/Enum;
.source "FileTransferError.java"

# interfaces
.implements Lcom/squareup/wire/ProtoEnum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/service/library/events/file/FileTransferError;",
        ">;",
        "Lcom/squareup/wire/ProtoEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/service/library/events/file/FileTransferError;

.field public static final enum FILE_TRANSFER_ABORTED:Lcom/navdy/service/library/events/file/FileTransferError;

.field public static final enum FILE_TRANSFER_CHECKSUM_ERROR:Lcom/navdy/service/library/events/file/FileTransferError;

.field public static final enum FILE_TRANSFER_HOST_BUSY:Lcom/navdy/service/library/events/file/FileTransferError;

.field public static final enum FILE_TRANSFER_ILLEGAL_CHUNK:Lcom/navdy/service/library/events/file/FileTransferError;

.field public static final enum FILE_TRANSFER_INSUFFICIENT_SPACE:Lcom/navdy/service/library/events/file/FileTransferError;

.field public static final enum FILE_TRANSFER_IO_ERROR:Lcom/navdy/service/library/events/file/FileTransferError;

.field public static final enum FILE_TRANSFER_NOT_INITIATED:Lcom/navdy/service/library/events/file/FileTransferError;

.field public static final enum FILE_TRANSFER_NO_ERROR:Lcom/navdy/service/library/events/file/FileTransferError;

.field public static final enum FILE_TRANSFER_PERMISSION_DENIED:Lcom/navdy/service/library/events/file/FileTransferError;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 9
    new-instance v0, Lcom/navdy/service/library/events/file/FileTransferError;

    const-string v1, "FILE_TRANSFER_NO_ERROR"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v4}, Lcom/navdy/service/library/events/file/FileTransferError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/file/FileTransferError;->FILE_TRANSFER_NO_ERROR:Lcom/navdy/service/library/events/file/FileTransferError;

    .line 14
    new-instance v0, Lcom/navdy/service/library/events/file/FileTransferError;

    const-string v1, "FILE_TRANSFER_INSUFFICIENT_SPACE"

    invoke-direct {v0, v1, v4, v5}, Lcom/navdy/service/library/events/file/FileTransferError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/file/FileTransferError;->FILE_TRANSFER_INSUFFICIENT_SPACE:Lcom/navdy/service/library/events/file/FileTransferError;

    .line 18
    new-instance v0, Lcom/navdy/service/library/events/file/FileTransferError;

    const-string v1, "FILE_TRANSFER_ABORTED"

    invoke-direct {v0, v1, v5, v6}, Lcom/navdy/service/library/events/file/FileTransferError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/file/FileTransferError;->FILE_TRANSFER_ABORTED:Lcom/navdy/service/library/events/file/FileTransferError;

    .line 23
    new-instance v0, Lcom/navdy/service/library/events/file/FileTransferError;

    const-string v1, "FILE_TRANSFER_IO_ERROR"

    invoke-direct {v0, v1, v6, v7}, Lcom/navdy/service/library/events/file/FileTransferError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/file/FileTransferError;->FILE_TRANSFER_IO_ERROR:Lcom/navdy/service/library/events/file/FileTransferError;

    .line 27
    new-instance v0, Lcom/navdy/service/library/events/file/FileTransferError;

    const-string v1, "FILE_TRANSFER_ILLEGAL_CHUNK"

    invoke-direct {v0, v1, v7, v8}, Lcom/navdy/service/library/events/file/FileTransferError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/file/FileTransferError;->FILE_TRANSFER_ILLEGAL_CHUNK:Lcom/navdy/service/library/events/file/FileTransferError;

    .line 31
    new-instance v0, Lcom/navdy/service/library/events/file/FileTransferError;

    const-string v1, "FILE_TRANSFER_NOT_INITIATED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v8, v2}, Lcom/navdy/service/library/events/file/FileTransferError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/file/FileTransferError;->FILE_TRANSFER_NOT_INITIATED:Lcom/navdy/service/library/events/file/FileTransferError;

    .line 35
    new-instance v0, Lcom/navdy/service/library/events/file/FileTransferError;

    const-string v1, "FILE_TRANSFER_PERMISSION_DENIED"

    const/4 v2, 0x6

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/file/FileTransferError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/file/FileTransferError;->FILE_TRANSFER_PERMISSION_DENIED:Lcom/navdy/service/library/events/file/FileTransferError;

    .line 39
    new-instance v0, Lcom/navdy/service/library/events/file/FileTransferError;

    const-string v1, "FILE_TRANSFER_CHECKSUM_ERROR"

    const/4 v2, 0x7

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/file/FileTransferError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/file/FileTransferError;->FILE_TRANSFER_CHECKSUM_ERROR:Lcom/navdy/service/library/events/file/FileTransferError;

    .line 43
    new-instance v0, Lcom/navdy/service/library/events/file/FileTransferError;

    const-string v1, "FILE_TRANSFER_HOST_BUSY"

    const/16 v2, 0x8

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/file/FileTransferError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/file/FileTransferError;->FILE_TRANSFER_HOST_BUSY:Lcom/navdy/service/library/events/file/FileTransferError;

    .line 7
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/navdy/service/library/events/file/FileTransferError;

    const/4 v1, 0x0

    sget-object v2, Lcom/navdy/service/library/events/file/FileTransferError;->FILE_TRANSFER_NO_ERROR:Lcom/navdy/service/library/events/file/FileTransferError;

    aput-object v2, v0, v1

    sget-object v1, Lcom/navdy/service/library/events/file/FileTransferError;->FILE_TRANSFER_INSUFFICIENT_SPACE:Lcom/navdy/service/library/events/file/FileTransferError;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/service/library/events/file/FileTransferError;->FILE_TRANSFER_ABORTED:Lcom/navdy/service/library/events/file/FileTransferError;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/service/library/events/file/FileTransferError;->FILE_TRANSFER_IO_ERROR:Lcom/navdy/service/library/events/file/FileTransferError;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/service/library/events/file/FileTransferError;->FILE_TRANSFER_ILLEGAL_CHUNK:Lcom/navdy/service/library/events/file/FileTransferError;

    aput-object v1, v0, v7

    sget-object v1, Lcom/navdy/service/library/events/file/FileTransferError;->FILE_TRANSFER_NOT_INITIATED:Lcom/navdy/service/library/events/file/FileTransferError;

    aput-object v1, v0, v8

    const/4 v1, 0x6

    sget-object v2, Lcom/navdy/service/library/events/file/FileTransferError;->FILE_TRANSFER_PERMISSION_DENIED:Lcom/navdy/service/library/events/file/FileTransferError;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/navdy/service/library/events/file/FileTransferError;->FILE_TRANSFER_CHECKSUM_ERROR:Lcom/navdy/service/library/events/file/FileTransferError;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/navdy/service/library/events/file/FileTransferError;->FILE_TRANSFER_HOST_BUSY:Lcom/navdy/service/library/events/file/FileTransferError;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/service/library/events/file/FileTransferError;->$VALUES:[Lcom/navdy/service/library/events/file/FileTransferError;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 48
    iput p3, p0, Lcom/navdy/service/library/events/file/FileTransferError;->value:I

    .line 49
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/file/FileTransferError;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 7
    const-class v0, Lcom/navdy/service/library/events/file/FileTransferError;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/file/FileTransferError;

    return-object v0
.end method

.method public static values()[Lcom/navdy/service/library/events/file/FileTransferError;
    .locals 1

    .prologue
    .line 7
    sget-object v0, Lcom/navdy/service/library/events/file/FileTransferError;->$VALUES:[Lcom/navdy/service/library/events/file/FileTransferError;

    invoke-virtual {v0}, [Lcom/navdy/service/library/events/file/FileTransferError;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/events/file/FileTransferError;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lcom/navdy/service/library/events/file/FileTransferError;->value:I

    return v0
.end method

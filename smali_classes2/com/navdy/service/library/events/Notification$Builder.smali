.class public final Lcom/navdy/service/library/events/Notification$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Notification.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/Notification;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/Notification;",
        ">;"
    }
.end annotation


# instance fields
.field public message_data:Ljava/lang/String;

.field public origin:Ljava/lang/String;

.field public sender:Ljava/lang/String;

.field public sms_class:Ljava/lang/Integer;

.field public target:Ljava/lang/String;

.field public type:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 91
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 92
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/Notification;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/Notification;

    .prologue
    .line 95
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 96
    if-nez p1, :cond_0

    .line 103
    :goto_0
    return-void

    .line 97
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/Notification;->sms_class:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/navdy/service/library/events/Notification$Builder;->sms_class:Ljava/lang/Integer;

    .line 98
    iget-object v0, p1, Lcom/navdy/service/library/events/Notification;->origin:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/Notification$Builder;->origin:Ljava/lang/String;

    .line 99
    iget-object v0, p1, Lcom/navdy/service/library/events/Notification;->sender:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/Notification$Builder;->sender:Ljava/lang/String;

    .line 100
    iget-object v0, p1, Lcom/navdy/service/library/events/Notification;->target:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/Notification$Builder;->target:Ljava/lang/String;

    .line 101
    iget-object v0, p1, Lcom/navdy/service/library/events/Notification;->message_data:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/Notification$Builder;->message_data:Ljava/lang/String;

    .line 102
    iget-object v0, p1, Lcom/navdy/service/library/events/Notification;->type:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/Notification$Builder;->type:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/Notification;
    .locals 2

    .prologue
    .line 137
    invoke-virtual {p0}, Lcom/navdy/service/library/events/Notification$Builder;->checkRequiredFields()V

    .line 138
    new-instance v0, Lcom/navdy/service/library/events/Notification;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/Notification;-><init>(Lcom/navdy/service/library/events/Notification$Builder;Lcom/navdy/service/library/events/Notification$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 82
    invoke-virtual {p0}, Lcom/navdy/service/library/events/Notification$Builder;->build()Lcom/navdy/service/library/events/Notification;

    move-result-object v0

    return-object v0
.end method

.method public message_data(Ljava/lang/String;)Lcom/navdy/service/library/events/Notification$Builder;
    .locals 0
    .param p1, "message_data"    # Ljava/lang/String;

    .prologue
    .line 126
    iput-object p1, p0, Lcom/navdy/service/library/events/Notification$Builder;->message_data:Ljava/lang/String;

    .line 127
    return-object p0
.end method

.method public origin(Ljava/lang/String;)Lcom/navdy/service/library/events/Notification$Builder;
    .locals 0
    .param p1, "origin"    # Ljava/lang/String;

    .prologue
    .line 111
    iput-object p1, p0, Lcom/navdy/service/library/events/Notification$Builder;->origin:Ljava/lang/String;

    .line 112
    return-object p0
.end method

.method public sender(Ljava/lang/String;)Lcom/navdy/service/library/events/Notification$Builder;
    .locals 0
    .param p1, "sender"    # Ljava/lang/String;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/navdy/service/library/events/Notification$Builder;->sender:Ljava/lang/String;

    .line 117
    return-object p0
.end method

.method public sms_class(Ljava/lang/Integer;)Lcom/navdy/service/library/events/Notification$Builder;
    .locals 0
    .param p1, "sms_class"    # Ljava/lang/Integer;

    .prologue
    .line 106
    iput-object p1, p0, Lcom/navdy/service/library/events/Notification$Builder;->sms_class:Ljava/lang/Integer;

    .line 107
    return-object p0
.end method

.method public target(Ljava/lang/String;)Lcom/navdy/service/library/events/Notification$Builder;
    .locals 0
    .param p1, "target"    # Ljava/lang/String;

    .prologue
    .line 121
    iput-object p1, p0, Lcom/navdy/service/library/events/Notification$Builder;->target:Ljava/lang/String;

    .line 122
    return-object p0
.end method

.method public type(Ljava/lang/String;)Lcom/navdy/service/library/events/Notification$Builder;
    .locals 0
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 131
    iput-object p1, p0, Lcom/navdy/service/library/events/Notification$Builder;->type:Ljava/lang/String;

    .line 132
    return-object p0
.end method

.class public final Lcom/navdy/service/library/events/destination/Destination$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Destination.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/destination/Destination;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/destination/Destination;",
        ">;"
    }
.end annotation


# instance fields
.field public contacts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/contacts/Contact;",
            ">;"
        }
    .end annotation
.end field

.field public destinationDistance:Ljava/lang/String;

.field public destinationIcon:Ljava/lang/Integer;

.field public destinationIconBkColor:Ljava/lang/Integer;

.field public destination_subtitle:Ljava/lang/String;

.field public destination_title:Ljava/lang/String;

.field public display_position:Lcom/navdy/service/library/events/location/LatLong;

.field public favorite_type:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

.field public full_address:Ljava/lang/String;

.field public identifier:Ljava/lang/String;

.field public is_recommendation:Ljava/lang/Boolean;

.field public last_navigated_to:Ljava/lang/Long;

.field public navigation_position:Lcom/navdy/service/library/events/location/LatLong;

.field public phoneNumbers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/contacts/PhoneNumber;",
            ">;"
        }
    .end annotation
.end field

.field public place_id:Ljava/lang/String;

.field public place_type:Lcom/navdy/service/library/events/places/PlaceType;

.field public suggestion_type:Lcom/navdy/service/library/events/destination/Destination$SuggestionType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 266
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 267
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/destination/Destination;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/destination/Destination;

    .prologue
    .line 270
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 271
    if-nez p1, :cond_0

    .line 289
    :goto_0
    return-void

    .line 272
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/destination/Destination;->navigation_position:Lcom/navdy/service/library/events/location/LatLong;

    iput-object v0, p0, Lcom/navdy/service/library/events/destination/Destination$Builder;->navigation_position:Lcom/navdy/service/library/events/location/LatLong;

    .line 273
    iget-object v0, p1, Lcom/navdy/service/library/events/destination/Destination;->display_position:Lcom/navdy/service/library/events/location/LatLong;

    iput-object v0, p0, Lcom/navdy/service/library/events/destination/Destination$Builder;->display_position:Lcom/navdy/service/library/events/location/LatLong;

    .line 274
    iget-object v0, p1, Lcom/navdy/service/library/events/destination/Destination;->full_address:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/destination/Destination$Builder;->full_address:Ljava/lang/String;

    .line 275
    iget-object v0, p1, Lcom/navdy/service/library/events/destination/Destination;->destination_title:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/destination/Destination$Builder;->destination_title:Ljava/lang/String;

    .line 276
    iget-object v0, p1, Lcom/navdy/service/library/events/destination/Destination;->destination_subtitle:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/destination/Destination$Builder;->destination_subtitle:Ljava/lang/String;

    .line 277
    iget-object v0, p1, Lcom/navdy/service/library/events/destination/Destination;->favorite_type:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    iput-object v0, p0, Lcom/navdy/service/library/events/destination/Destination$Builder;->favorite_type:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    .line 278
    iget-object v0, p1, Lcom/navdy/service/library/events/destination/Destination;->identifier:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/destination/Destination$Builder;->identifier:Ljava/lang/String;

    .line 279
    iget-object v0, p1, Lcom/navdy/service/library/events/destination/Destination;->suggestion_type:Lcom/navdy/service/library/events/destination/Destination$SuggestionType;

    iput-object v0, p0, Lcom/navdy/service/library/events/destination/Destination$Builder;->suggestion_type:Lcom/navdy/service/library/events/destination/Destination$SuggestionType;

    .line 280
    iget-object v0, p1, Lcom/navdy/service/library/events/destination/Destination;->is_recommendation:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/destination/Destination$Builder;->is_recommendation:Ljava/lang/Boolean;

    .line 281
    iget-object v0, p1, Lcom/navdy/service/library/events/destination/Destination;->last_navigated_to:Ljava/lang/Long;

    iput-object v0, p0, Lcom/navdy/service/library/events/destination/Destination$Builder;->last_navigated_to:Ljava/lang/Long;

    .line 282
    iget-object v0, p1, Lcom/navdy/service/library/events/destination/Destination;->place_type:Lcom/navdy/service/library/events/places/PlaceType;

    iput-object v0, p0, Lcom/navdy/service/library/events/destination/Destination$Builder;->place_type:Lcom/navdy/service/library/events/places/PlaceType;

    .line 283
    iget-object v0, p1, Lcom/navdy/service/library/events/destination/Destination;->destinationIcon:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/navdy/service/library/events/destination/Destination$Builder;->destinationIcon:Ljava/lang/Integer;

    .line 284
    iget-object v0, p1, Lcom/navdy/service/library/events/destination/Destination;->destinationIconBkColor:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/navdy/service/library/events/destination/Destination$Builder;->destinationIconBkColor:Ljava/lang/Integer;

    .line 285
    iget-object v0, p1, Lcom/navdy/service/library/events/destination/Destination;->place_id:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/destination/Destination$Builder;->place_id:Ljava/lang/String;

    .line 286
    iget-object v0, p1, Lcom/navdy/service/library/events/destination/Destination;->destinationDistance:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/destination/Destination$Builder;->destinationDistance:Ljava/lang/String;

    .line 287
    iget-object v0, p1, Lcom/navdy/service/library/events/destination/Destination;->phoneNumbers:Ljava/util/List;

    invoke-static {v0}, Lcom/navdy/service/library/events/destination/Destination;->access$000(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/destination/Destination$Builder;->phoneNumbers:Ljava/util/List;

    .line 288
    iget-object v0, p1, Lcom/navdy/service/library/events/destination/Destination;->contacts:Ljava/util/List;

    invoke-static {v0}, Lcom/navdy/service/library/events/destination/Destination;->access$100(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/destination/Destination$Builder;->contacts:Ljava/util/List;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/destination/Destination;
    .locals 2

    .prologue
    .line 452
    invoke-virtual {p0}, Lcom/navdy/service/library/events/destination/Destination$Builder;->checkRequiredFields()V

    .line 453
    new-instance v0, Lcom/navdy/service/library/events/destination/Destination;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/destination/Destination;-><init>(Lcom/navdy/service/library/events/destination/Destination$Builder;Lcom/navdy/service/library/events/destination/Destination$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 246
    invoke-virtual {p0}, Lcom/navdy/service/library/events/destination/Destination$Builder;->build()Lcom/navdy/service/library/events/destination/Destination;

    move-result-object v0

    return-object v0
.end method

.method public contacts(Ljava/util/List;)Lcom/navdy/service/library/events/destination/Destination$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/contacts/Contact;",
            ">;)",
            "Lcom/navdy/service/library/events/destination/Destination$Builder;"
        }
    .end annotation

    .prologue
    .line 446
    .local p1, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/contacts/Contact;>;"
    invoke-static {p1}, Lcom/navdy/service/library/events/destination/Destination$Builder;->checkForNulls(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/destination/Destination$Builder;->contacts:Ljava/util/List;

    .line 447
    return-object p0
.end method

.method public destinationDistance(Ljava/lang/String;)Lcom/navdy/service/library/events/destination/Destination$Builder;
    .locals 0
    .param p1, "destinationDistance"    # Ljava/lang/String;

    .prologue
    .line 429
    iput-object p1, p0, Lcom/navdy/service/library/events/destination/Destination$Builder;->destinationDistance:Ljava/lang/String;

    .line 430
    return-object p0
.end method

.method public destinationIcon(Ljava/lang/Integer;)Lcom/navdy/service/library/events/destination/Destination$Builder;
    .locals 0
    .param p1, "destinationIcon"    # Ljava/lang/Integer;

    .prologue
    .line 402
    iput-object p1, p0, Lcom/navdy/service/library/events/destination/Destination$Builder;->destinationIcon:Ljava/lang/Integer;

    .line 403
    return-object p0
.end method

.method public destinationIconBkColor(Ljava/lang/Integer;)Lcom/navdy/service/library/events/destination/Destination$Builder;
    .locals 0
    .param p1, "destinationIconBkColor"    # Ljava/lang/Integer;

    .prologue
    .line 411
    iput-object p1, p0, Lcom/navdy/service/library/events/destination/Destination$Builder;->destinationIconBkColor:Ljava/lang/Integer;

    .line 412
    return-object p0
.end method

.method public destination_subtitle(Ljava/lang/String;)Lcom/navdy/service/library/events/destination/Destination$Builder;
    .locals 0
    .param p1, "destination_subtitle"    # Ljava/lang/String;

    .prologue
    .line 329
    iput-object p1, p0, Lcom/navdy/service/library/events/destination/Destination$Builder;->destination_subtitle:Ljava/lang/String;

    .line 330
    return-object p0
.end method

.method public destination_title(Ljava/lang/String;)Lcom/navdy/service/library/events/destination/Destination$Builder;
    .locals 0
    .param p1, "destination_title"    # Ljava/lang/String;

    .prologue
    .line 320
    iput-object p1, p0, Lcom/navdy/service/library/events/destination/Destination$Builder;->destination_title:Ljava/lang/String;

    .line 321
    return-object p0
.end method

.method public display_position(Lcom/navdy/service/library/events/location/LatLong;)Lcom/navdy/service/library/events/destination/Destination$Builder;
    .locals 0
    .param p1, "display_position"    # Lcom/navdy/service/library/events/location/LatLong;

    .prologue
    .line 303
    iput-object p1, p0, Lcom/navdy/service/library/events/destination/Destination$Builder;->display_position:Lcom/navdy/service/library/events/location/LatLong;

    .line 304
    return-object p0
.end method

.method public favorite_type(Lcom/navdy/service/library/events/destination/Destination$FavoriteType;)Lcom/navdy/service/library/events/destination/Destination$Builder;
    .locals 0
    .param p1, "favorite_type"    # Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    .prologue
    .line 334
    iput-object p1, p0, Lcom/navdy/service/library/events/destination/Destination$Builder;->favorite_type:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    .line 335
    return-object p0
.end method

.method public full_address(Ljava/lang/String;)Lcom/navdy/service/library/events/destination/Destination$Builder;
    .locals 0
    .param p1, "full_address"    # Ljava/lang/String;

    .prologue
    .line 311
    iput-object p1, p0, Lcom/navdy/service/library/events/destination/Destination$Builder;->full_address:Ljava/lang/String;

    .line 312
    return-object p0
.end method

.method public identifier(Ljava/lang/String;)Lcom/navdy/service/library/events/destination/Destination$Builder;
    .locals 0
    .param p1, "identifier"    # Ljava/lang/String;

    .prologue
    .line 344
    iput-object p1, p0, Lcom/navdy/service/library/events/destination/Destination$Builder;->identifier:Ljava/lang/String;

    .line 345
    return-object p0
.end method

.method public is_recommendation(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/destination/Destination$Builder;
    .locals 0
    .param p1, "is_recommendation"    # Ljava/lang/Boolean;

    .prologue
    .line 376
    iput-object p1, p0, Lcom/navdy/service/library/events/destination/Destination$Builder;->is_recommendation:Ljava/lang/Boolean;

    .line 377
    return-object p0
.end method

.method public last_navigated_to(Ljava/lang/Long;)Lcom/navdy/service/library/events/destination/Destination$Builder;
    .locals 0
    .param p1, "last_navigated_to"    # Ljava/lang/Long;

    .prologue
    .line 385
    iput-object p1, p0, Lcom/navdy/service/library/events/destination/Destination$Builder;->last_navigated_to:Ljava/lang/Long;

    .line 386
    return-object p0
.end method

.method public navigation_position(Lcom/navdy/service/library/events/location/LatLong;)Lcom/navdy/service/library/events/destination/Destination$Builder;
    .locals 0
    .param p1, "navigation_position"    # Lcom/navdy/service/library/events/location/LatLong;

    .prologue
    .line 295
    iput-object p1, p0, Lcom/navdy/service/library/events/destination/Destination$Builder;->navigation_position:Lcom/navdy/service/library/events/location/LatLong;

    .line 296
    return-object p0
.end method

.method public phoneNumbers(Ljava/util/List;)Lcom/navdy/service/library/events/destination/Destination$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/contacts/PhoneNumber;",
            ">;)",
            "Lcom/navdy/service/library/events/destination/Destination$Builder;"
        }
    .end annotation

    .prologue
    .line 437
    .local p1, "phoneNumbers":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/contacts/PhoneNumber;>;"
    invoke-static {p1}, Lcom/navdy/service/library/events/destination/Destination$Builder;->checkForNulls(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/destination/Destination$Builder;->phoneNumbers:Ljava/util/List;

    .line 438
    return-object p0
.end method

.method public place_id(Ljava/lang/String;)Lcom/navdy/service/library/events/destination/Destination$Builder;
    .locals 0
    .param p1, "place_id"    # Ljava/lang/String;

    .prologue
    .line 420
    iput-object p1, p0, Lcom/navdy/service/library/events/destination/Destination$Builder;->place_id:Ljava/lang/String;

    .line 421
    return-object p0
.end method

.method public place_type(Lcom/navdy/service/library/events/places/PlaceType;)Lcom/navdy/service/library/events/destination/Destination$Builder;
    .locals 0
    .param p1, "place_type"    # Lcom/navdy/service/library/events/places/PlaceType;

    .prologue
    .line 393
    iput-object p1, p0, Lcom/navdy/service/library/events/destination/Destination$Builder;->place_type:Lcom/navdy/service/library/events/places/PlaceType;

    .line 394
    return-object p0
.end method

.method public suggestion_type(Lcom/navdy/service/library/events/destination/Destination$SuggestionType;)Lcom/navdy/service/library/events/destination/Destination$Builder;
    .locals 0
    .param p1, "suggestion_type"    # Lcom/navdy/service/library/events/destination/Destination$SuggestionType;

    .prologue
    .line 349
    iput-object p1, p0, Lcom/navdy/service/library/events/destination/Destination$Builder;->suggestion_type:Lcom/navdy/service/library/events/destination/Destination$SuggestionType;

    .line 350
    return-object p0
.end method

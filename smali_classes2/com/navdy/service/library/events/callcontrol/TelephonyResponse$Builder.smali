.class public final Lcom/navdy/service/library/events/callcontrol/TelephonyResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "TelephonyResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/callcontrol/TelephonyResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/callcontrol/TelephonyResponse;",
        ">;"
    }
.end annotation


# instance fields
.field public action:Lcom/navdy/service/library/events/callcontrol/CallAction;

.field public status:Lcom/navdy/service/library/events/RequestStatus;

.field public statusDetail:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 78
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/callcontrol/TelephonyResponse;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/callcontrol/TelephonyResponse;

    .prologue
    .line 81
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 82
    if-nez p1, :cond_0

    .line 86
    :goto_0
    return-void

    .line 83
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/callcontrol/TelephonyResponse;->action:Lcom/navdy/service/library/events/callcontrol/CallAction;

    iput-object v0, p0, Lcom/navdy/service/library/events/callcontrol/TelephonyResponse$Builder;->action:Lcom/navdy/service/library/events/callcontrol/CallAction;

    .line 84
    iget-object v0, p1, Lcom/navdy/service/library/events/callcontrol/TelephonyResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    iput-object v0, p0, Lcom/navdy/service/library/events/callcontrol/TelephonyResponse$Builder;->status:Lcom/navdy/service/library/events/RequestStatus;

    .line 85
    iget-object v0, p1, Lcom/navdy/service/library/events/callcontrol/TelephonyResponse;->statusDetail:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/callcontrol/TelephonyResponse$Builder;->statusDetail:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public action(Lcom/navdy/service/library/events/callcontrol/CallAction;)Lcom/navdy/service/library/events/callcontrol/TelephonyResponse$Builder;
    .locals 0
    .param p1, "action"    # Lcom/navdy/service/library/events/callcontrol/CallAction;

    .prologue
    .line 92
    iput-object p1, p0, Lcom/navdy/service/library/events/callcontrol/TelephonyResponse$Builder;->action:Lcom/navdy/service/library/events/callcontrol/CallAction;

    .line 93
    return-object p0
.end method

.method public build()Lcom/navdy/service/library/events/callcontrol/TelephonyResponse;
    .locals 2

    .prologue
    .line 114
    invoke-virtual {p0}, Lcom/navdy/service/library/events/callcontrol/TelephonyResponse$Builder;->checkRequiredFields()V

    .line 115
    new-instance v0, Lcom/navdy/service/library/events/callcontrol/TelephonyResponse;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/callcontrol/TelephonyResponse;-><init>(Lcom/navdy/service/library/events/callcontrol/TelephonyResponse$Builder;Lcom/navdy/service/library/events/callcontrol/TelephonyResponse$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 71
    invoke-virtual {p0}, Lcom/navdy/service/library/events/callcontrol/TelephonyResponse$Builder;->build()Lcom/navdy/service/library/events/callcontrol/TelephonyResponse;

    move-result-object v0

    return-object v0
.end method

.method public status(Lcom/navdy/service/library/events/RequestStatus;)Lcom/navdy/service/library/events/callcontrol/TelephonyResponse$Builder;
    .locals 0
    .param p1, "status"    # Lcom/navdy/service/library/events/RequestStatus;

    .prologue
    .line 100
    iput-object p1, p0, Lcom/navdy/service/library/events/callcontrol/TelephonyResponse$Builder;->status:Lcom/navdy/service/library/events/RequestStatus;

    .line 101
    return-object p0
.end method

.method public statusDetail(Ljava/lang/String;)Lcom/navdy/service/library/events/callcontrol/TelephonyResponse$Builder;
    .locals 0
    .param p1, "statusDetail"    # Ljava/lang/String;

    .prologue
    .line 108
    iput-object p1, p0, Lcom/navdy/service/library/events/callcontrol/TelephonyResponse$Builder;->statusDetail:Ljava/lang/String;

    .line 109
    return-object p0
.end method

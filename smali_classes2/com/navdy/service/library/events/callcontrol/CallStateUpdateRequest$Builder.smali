.class public final Lcom/navdy/service/library/events/callcontrol/CallStateUpdateRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CallStateUpdateRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/callcontrol/CallStateUpdateRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/callcontrol/CallStateUpdateRequest;",
        ">;"
    }
.end annotation


# instance fields
.field public start:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 54
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/callcontrol/CallStateUpdateRequest;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/callcontrol/CallStateUpdateRequest;

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 58
    if-nez p1, :cond_0

    .line 60
    :goto_0
    return-void

    .line 59
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/callcontrol/CallStateUpdateRequest;->start:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/callcontrol/CallStateUpdateRequest$Builder;->start:Ljava/lang/Boolean;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/callcontrol/CallStateUpdateRequest;
    .locals 2

    .prologue
    .line 73
    invoke-virtual {p0}, Lcom/navdy/service/library/events/callcontrol/CallStateUpdateRequest$Builder;->checkRequiredFields()V

    .line 74
    new-instance v0, Lcom/navdy/service/library/events/callcontrol/CallStateUpdateRequest;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/callcontrol/CallStateUpdateRequest;-><init>(Lcom/navdy/service/library/events/callcontrol/CallStateUpdateRequest$Builder;Lcom/navdy/service/library/events/callcontrol/CallStateUpdateRequest$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 49
    invoke-virtual {p0}, Lcom/navdy/service/library/events/callcontrol/CallStateUpdateRequest$Builder;->build()Lcom/navdy/service/library/events/callcontrol/CallStateUpdateRequest;

    move-result-object v0

    return-object v0
.end method

.method public start(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/callcontrol/CallStateUpdateRequest$Builder;
    .locals 0
    .param p1, "start"    # Ljava/lang/Boolean;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/navdy/service/library/events/callcontrol/CallStateUpdateRequest$Builder;->start:Ljava/lang/Boolean;

    .line 68
    return-object p0
.end method

.class public final enum Lcom/navdy/service/library/events/NavdyEvent$MessageType;
.super Ljava/lang/Enum;
.source "NavdyEvent.java"

# interfaces
.implements Lcom/squareup/wire/ProtoEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/NavdyEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MessageType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/service/library/events/NavdyEvent$MessageType;",
        ">;",
        "Lcom/squareup/wire/ProtoEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum AccelerateShutdown:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum AudioStatus:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum AutoCompleteRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum AutoCompleteResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum CalendarEvent:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum CalendarEventUpdates:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum CallEvent:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum CallStateUpdateRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum CancelSpeechRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum CannedMessagesRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum CannedMessagesUpdate:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum ClearGlances:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum ConnectionRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum ConnectionStateChange:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum ConnectionStatus:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum ContactRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum ContactResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum Coordinate:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum DashboardPreferences:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum DateTimeConfiguration:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum DestinationSelectedRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum DestinationSelectedResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum DeviceInfo:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum DialBondRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum DialBondResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum DialSimulationEvent:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum DialStatusRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum DialStatusResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum DisconnectRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum DismissScreen:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum DisplaySpeakerPreferencesRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum DisplaySpeakerPreferencesUpdate:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum DriveRecordingsRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum DriveRecordingsResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum DriverProfilePreferencesRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum DriverProfilePreferencesUpdate:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum FavoriteContactsRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum FavoriteContactsResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum FavoriteDestinationsRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum FavoriteDestinationsUpdate:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum FileListRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum FileListResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum FileTransferData:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum FileTransferRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum FileTransferResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum FileTransferStatus:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum GestureEvent:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum GetNavigationSessionState:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum GlanceEvent:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum InputPreferencesRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum InputPreferencesUpdate:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum LaunchAppEvent:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum LinkPropertiesChanged:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum MediaRemoteKeyEvent:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum MusicArtworkRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum MusicArtworkResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum MusicCapabilitiesRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum MusicCapabilitiesResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum MusicCollectionRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum MusicCollectionResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum MusicCollectionSourceUpdate:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum MusicEvent:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum MusicTrackInfo:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum MusicTrackInfoRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum NavigationManeuverEvent:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum NavigationPreferencesRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum NavigationPreferencesUpdate:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum NavigationRouteCancelRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum NavigationRouteRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum NavigationRouteResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum NavigationRouteStatus:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum NavigationSessionDeferRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum NavigationSessionRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum NavigationSessionResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum NavigationSessionRouteChange:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum NavigationSessionStatusEvent:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum NetworkLinkReady:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum NetworkStateChange:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum Notification:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum NotificationEvent:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum NotificationListRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum NotificationListResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum NotificationPreferencesRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum NotificationPreferencesUpdate:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum NotificationsStateChange:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum NotificationsStateRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum NowPlayingUpdateRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum ObdStatusRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum ObdStatusResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum PhoneBatteryStatus:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum PhoneEvent:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum PhoneStatusRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum PhoneStatusResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum PhotoRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum PhotoResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum PhotoUpdate:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum PhotoUpdateQuery:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum PhotoUpdateQueryResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum PhotoUpdatesRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum Ping:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum PlaceTypeSearchRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum PlaceTypeSearchResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum PlacesSearchRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum PlacesSearchResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum PreviewFileRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum PreviewFileResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum ReadSettingsRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum ReadSettingsResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum RecommendedDestinationsRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum RecommendedDestinationsUpdate:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum ResumeMusicRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum RouteManeuverRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum RouteManeuverResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum ShowCustomNotification:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum ShowScreen:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum SmsMessageRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum SmsMessageResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum SpeechRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum SpeechRequestStatus:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum StartDrivePlaybackEvent:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum StartDrivePlaybackResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum StartDriveRecordingEvent:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum StartDriveRecordingResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum StopDrivePlaybackEvent:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum StopDriveRecordingEvent:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum StopDriveRecordingResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum SuggestedDestination:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum TelephonyRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum TelephonyResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum TransmitLocation:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum TripUpdate:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum TripUpdateAck:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum UpdateSettings:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum VoiceAssistRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum VoiceAssistResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum VoiceSearchRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

.field public static final enum VoiceSearchResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 83
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "Coordinate"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v4}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->Coordinate:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 84
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "Notification"

    invoke-direct {v0, v1, v4, v5}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->Notification:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 85
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "CallEvent"

    invoke-direct {v0, v1, v5, v6}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->CallEvent:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 86
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "FileListRequest"

    invoke-direct {v0, v1, v6, v7}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->FileListRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 87
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "FileListResponse"

    invoke-direct {v0, v1, v7, v8}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->FileListResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 88
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "PreviewFileRequest"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v8, v2}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->PreviewFileRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 89
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "PreviewFileResponse"

    const/4 v2, 0x6

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->PreviewFileResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 90
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "NavigationManeuverEvent"

    const/4 v2, 0x7

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->NavigationManeuverEvent:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 91
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "NavigationRouteRequest"

    const/16 v2, 0x8

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->NavigationRouteRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 92
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "NavigationRouteResponse"

    const/16 v2, 0x9

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->NavigationRouteResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 93
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "NavigationSessionRequest"

    const/16 v2, 0xa

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->NavigationSessionRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 94
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "NavigationSessionResponse"

    const/16 v2, 0xb

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->NavigationSessionResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 95
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "NavigationSessionStatusEvent"

    const/16 v2, 0xc

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->NavigationSessionStatusEvent:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 96
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "DismissScreen"

    const/16 v2, 0xd

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->DismissScreen:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 97
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "ShowScreen"

    const/16 v2, 0xe

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->ShowScreen:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 98
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "GestureEvent"

    const/16 v2, 0xf

    const/16 v3, 0x10

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->GestureEvent:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 99
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "PlacesSearchRequest"

    const/16 v2, 0x10

    const/16 v3, 0x11

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->PlacesSearchRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 100
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "PlacesSearchResponse"

    const/16 v2, 0x11

    const/16 v3, 0x12

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->PlacesSearchResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 101
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "ReadSettingsRequest"

    const/16 v2, 0x12

    const/16 v3, 0x13

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->ReadSettingsRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 102
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "ReadSettingsResponse"

    const/16 v2, 0x13

    const/16 v3, 0x14

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->ReadSettingsResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 103
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "UpdateSettings"

    const/16 v2, 0x14

    const/16 v3, 0x15

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->UpdateSettings:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 104
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "ConnectionStateChange"

    const/16 v2, 0x15

    const/16 v3, 0x16

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->ConnectionStateChange:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 105
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "SpeechRequest"

    const/16 v2, 0x16

    const/16 v3, 0x17

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->SpeechRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 106
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "TelephonyRequest"

    const/16 v2, 0x17

    const/16 v3, 0x18

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->TelephonyRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 107
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "TelephonyResponse"

    const/16 v2, 0x18

    const/16 v3, 0x19

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->TelephonyResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 108
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "NotificationEvent"

    const/16 v2, 0x19

    const/16 v3, 0x1a

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->NotificationEvent:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 109
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "DeviceInfo"

    const/16 v2, 0x1a

    const/16 v3, 0x1b

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->DeviceInfo:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 110
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "PhoneEvent"

    const/16 v2, 0x1b

    const/16 v3, 0x1c

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->PhoneEvent:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 111
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "VoiceAssistRequest"

    const/16 v2, 0x1c

    const/16 v3, 0x1d

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->VoiceAssistRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 112
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "PhotoRequest"

    const/16 v2, 0x1d

    const/16 v3, 0x1e

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->PhotoRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 113
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "PhotoResponse"

    const/16 v2, 0x1e

    const/16 v3, 0x1f

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->PhotoResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 114
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "NotificationListRequest"

    const/16 v2, 0x1f

    const/16 v3, 0x20

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->NotificationListRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 115
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "NotificationListResponse"

    const/16 v2, 0x20

    const/16 v3, 0x21

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->NotificationListResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 116
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "ShowCustomNotification"

    const/16 v2, 0x21

    const/16 v3, 0x22

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->ShowCustomNotification:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 117
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "DateTimeConfiguration"

    const/16 v2, 0x22

    const/16 v3, 0x23

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->DateTimeConfiguration:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 118
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "FileTransferResponse"

    const/16 v2, 0x23

    const/16 v3, 0x24

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->FileTransferResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 119
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "FileTransferRequest"

    const/16 v2, 0x24

    const/16 v3, 0x25

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->FileTransferRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 120
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "FileTransferData"

    const/16 v2, 0x25

    const/16 v3, 0x26

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->FileTransferData:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 121
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "FileTransferStatus"

    const/16 v2, 0x26

    const/16 v3, 0x27

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->FileTransferStatus:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 122
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "Ping"

    const/16 v2, 0x27

    const/16 v3, 0x28

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->Ping:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 123
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "NotificationsStateChange"

    const/16 v2, 0x28

    const/16 v3, 0x29

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->NotificationsStateChange:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 124
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "NotificationsStateRequest"

    const/16 v2, 0x29

    const/16 v3, 0x2a

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->NotificationsStateRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 125
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "MusicTrackInfoRequest"

    const/16 v2, 0x2a

    const/16 v3, 0x2b

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->MusicTrackInfoRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 126
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "MusicTrackInfo"

    const/16 v2, 0x2b

    const/16 v3, 0x2c

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->MusicTrackInfo:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 127
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "MusicEvent"

    const/16 v2, 0x2c

    const/16 v3, 0x2d

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->MusicEvent:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 128
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "DialStatusRequest"

    const/16 v2, 0x2d

    const/16 v3, 0x2e

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->DialStatusRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 129
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "DialStatusResponse"

    const/16 v2, 0x2e

    const/16 v3, 0x2f

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->DialStatusResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 130
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "DialBondRequest"

    const/16 v2, 0x2f

    const/16 v3, 0x30

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->DialBondRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 131
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "DialBondResponse"

    const/16 v2, 0x30

    const/16 v3, 0x31

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->DialBondResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 132
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "DriverProfilePreferencesRequest"

    const/16 v2, 0x31

    const/16 v3, 0x32

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->DriverProfilePreferencesRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 133
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "DriverProfilePreferencesUpdate"

    const/16 v2, 0x32

    const/16 v3, 0x33

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->DriverProfilePreferencesUpdate:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 134
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "TransmitLocation"

    const/16 v2, 0x33

    const/16 v3, 0x34

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->TransmitLocation:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 135
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "DisconnectRequest"

    const/16 v2, 0x34

    const/16 v3, 0x35

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->DisconnectRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 136
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "VoiceAssistResponse"

    const/16 v2, 0x35

    const/16 v3, 0x36

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->VoiceAssistResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 137
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "GetNavigationSessionState"

    const/16 v2, 0x36

    const/16 v3, 0x37

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->GetNavigationSessionState:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 138
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "RouteManeuverRequest"

    const/16 v2, 0x37

    const/16 v3, 0x38

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->RouteManeuverRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 139
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "RouteManeuverResponse"

    const/16 v2, 0x38

    const/16 v3, 0x39

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->RouteManeuverResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 140
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "NavigationPreferencesRequest"

    const/16 v2, 0x39

    const/16 v3, 0x3a

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->NavigationPreferencesRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 141
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "NavigationPreferencesUpdate"

    const/16 v2, 0x3a

    const/16 v3, 0x3b

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->NavigationPreferencesUpdate:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 142
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "DialSimulationEvent"

    const/16 v2, 0x3b

    const/16 v3, 0x3c

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->DialSimulationEvent:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 143
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "MediaRemoteKeyEvent"

    const/16 v2, 0x3c

    const/16 v3, 0x3d

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->MediaRemoteKeyEvent:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 144
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "ContactRequest"

    const/16 v2, 0x3d

    const/16 v3, 0x3e

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->ContactRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 145
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "ContactResponse"

    const/16 v2, 0x3e

    const/16 v3, 0x3f

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->ContactResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 146
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "FavoriteContactsRequest"

    const/16 v2, 0x3f

    const/16 v3, 0x40

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->FavoriteContactsRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 147
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "FavoriteContactsResponse"

    const/16 v2, 0x40

    const/16 v3, 0x41

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->FavoriteContactsResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 148
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "FavoriteDestinationsRequest"

    const/16 v2, 0x41

    const/16 v3, 0x42

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->FavoriteDestinationsRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 149
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "FavoriteDestinationsUpdate"

    const/16 v2, 0x42

    const/16 v3, 0x43

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->FavoriteDestinationsUpdate:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 150
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "RecommendedDestinationsRequest"

    const/16 v2, 0x43

    const/16 v3, 0x44

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->RecommendedDestinationsRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 151
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "RecommendedDestinationsUpdate"

    const/16 v2, 0x44

    const/16 v3, 0x45

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->RecommendedDestinationsUpdate:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 152
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "ConnectionRequest"

    const/16 v2, 0x45

    const/16 v3, 0x46

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->ConnectionRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 153
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "ConnectionStatus"

    const/16 v2, 0x46

    const/16 v3, 0x47

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->ConnectionStatus:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 154
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "PhoneStatusRequest"

    const/16 v2, 0x47

    const/16 v3, 0x48

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->PhoneStatusRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 155
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "PhoneStatusResponse"

    const/16 v2, 0x48

    const/16 v3, 0x49

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->PhoneStatusResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 156
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "LaunchAppEvent"

    const/16 v2, 0x49

    const/16 v3, 0x4a

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->LaunchAppEvent:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 157
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "InputPreferencesRequest"

    const/16 v2, 0x4a

    const/16 v3, 0x4b

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->InputPreferencesRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 158
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "InputPreferencesUpdate"

    const/16 v2, 0x4b

    const/16 v3, 0x4c

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->InputPreferencesUpdate:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 159
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "PhoneBatteryStatus"

    const/16 v2, 0x4c

    const/16 v3, 0x4d

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->PhoneBatteryStatus:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 160
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "SmsMessageRequest"

    const/16 v2, 0x4d

    const/16 v3, 0x4e

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->SmsMessageRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 161
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "SmsMessageResponse"

    const/16 v2, 0x4e

    const/16 v3, 0x4f

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->SmsMessageResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 162
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "ObdStatusRequest"

    const/16 v2, 0x4f

    const/16 v3, 0x50

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->ObdStatusRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 163
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "ObdStatusResponse"

    const/16 v2, 0x50

    const/16 v3, 0x51

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->ObdStatusResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 164
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "TripUpdate"

    const/16 v2, 0x51

    const/16 v3, 0x52

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->TripUpdate:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 165
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "TripUpdateAck"

    const/16 v2, 0x52

    const/16 v3, 0x53

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->TripUpdateAck:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 166
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "PhotoUpdatesRequest"

    const/16 v2, 0x53

    const/16 v3, 0x54

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->PhotoUpdatesRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 167
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "PhotoUpdate"

    const/16 v2, 0x54

    const/16 v3, 0x55

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->PhotoUpdate:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 168
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "DisplaySpeakerPreferencesRequest"

    const/16 v2, 0x55

    const/16 v3, 0x56

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->DisplaySpeakerPreferencesRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 169
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "DisplaySpeakerPreferencesUpdate"

    const/16 v2, 0x56

    const/16 v3, 0x57

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->DisplaySpeakerPreferencesUpdate:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 170
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "NavigationRouteStatus"

    const/16 v2, 0x57

    const/16 v3, 0x58

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->NavigationRouteStatus:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 171
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "NavigationRouteCancelRequest"

    const/16 v2, 0x58

    const/16 v3, 0x59

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->NavigationRouteCancelRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 172
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "NotificationPreferencesRequest"

    const/16 v2, 0x59

    const/16 v3, 0x5a

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->NotificationPreferencesRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 173
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "NotificationPreferencesUpdate"

    const/16 v2, 0x5a

    const/16 v3, 0x5b

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->NotificationPreferencesUpdate:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 174
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "StartDriveRecordingEvent"

    const/16 v2, 0x5b

    const/16 v3, 0x5c

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->StartDriveRecordingEvent:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 175
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "StopDriveRecordingEvent"

    const/16 v2, 0x5c

    const/16 v3, 0x5d

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->StopDriveRecordingEvent:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 176
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "DriveRecordingsRequest"

    const/16 v2, 0x5d

    const/16 v3, 0x5e

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->DriveRecordingsRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 177
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "DriveRecordingsResponse"

    const/16 v2, 0x5e

    const/16 v3, 0x5f

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->DriveRecordingsResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 178
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "StartDrivePlaybackEvent"

    const/16 v2, 0x5f

    const/16 v3, 0x60

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->StartDrivePlaybackEvent:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 179
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "StopDrivePlaybackEvent"

    const/16 v2, 0x60

    const/16 v3, 0x61

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->StopDrivePlaybackEvent:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 180
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "StartDriveRecordingResponse"

    const/16 v2, 0x61

    const/16 v3, 0x62

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->StartDriveRecordingResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 181
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "StartDrivePlaybackResponse"

    const/16 v2, 0x62

    const/16 v3, 0x63

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->StartDrivePlaybackResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 182
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "NavigationSessionRouteChange"

    const/16 v2, 0x63

    const/16 v3, 0x64

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->NavigationSessionRouteChange:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 183
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "NetworkStateChange"

    const/16 v2, 0x64

    const/16 v3, 0x65

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->NetworkStateChange:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 184
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "StopDriveRecordingResponse"

    const/16 v2, 0x65

    const/16 v3, 0x66

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->StopDriveRecordingResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 185
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "NavigationSessionDeferRequest"

    const/16 v2, 0x66

    const/16 v3, 0x67

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->NavigationSessionDeferRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 186
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "CallStateUpdateRequest"

    const/16 v2, 0x67

    const/16 v3, 0x68

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->CallStateUpdateRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 187
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "AutoCompleteRequest"

    const/16 v2, 0x68

    const/16 v3, 0x69

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->AutoCompleteRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 188
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "AutoCompleteResponse"

    const/16 v2, 0x69

    const/16 v3, 0x6a

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->AutoCompleteResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 189
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "GlanceEvent"

    const/16 v2, 0x6a

    const/16 v3, 0x6b

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->GlanceEvent:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 190
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "NowPlayingUpdateRequest"

    const/16 v2, 0x6b

    const/16 v3, 0x6c

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->NowPlayingUpdateRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 191
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "CancelSpeechRequest"

    const/16 v2, 0x6c

    const/16 v3, 0x6d

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->CancelSpeechRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 192
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "NetworkLinkReady"

    const/16 v2, 0x6d

    const/16 v3, 0x6e

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->NetworkLinkReady:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 193
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "SuggestedDestination"

    const/16 v2, 0x6e

    const/16 v3, 0x6f

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->SuggestedDestination:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 194
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "CalendarEvent"

    const/16 v2, 0x6f

    const/16 v3, 0x70

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->CalendarEvent:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 195
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "CalendarEventUpdates"

    const/16 v2, 0x70

    const/16 v3, 0x71

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->CalendarEventUpdates:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 196
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "SpeechRequestStatus"

    const/16 v2, 0x71

    const/16 v3, 0x72

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->SpeechRequestStatus:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 197
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "AudioStatus"

    const/16 v2, 0x72

    const/16 v3, 0x73

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->AudioStatus:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 198
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "LinkPropertiesChanged"

    const/16 v2, 0x73

    const/16 v3, 0x74

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->LinkPropertiesChanged:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 199
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "VoiceSearchRequest"

    const/16 v2, 0x74

    const/16 v3, 0x75

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->VoiceSearchRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 200
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "VoiceSearchResponse"

    const/16 v2, 0x75

    const/16 v3, 0x76

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->VoiceSearchResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 201
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "MusicCollectionRequest"

    const/16 v2, 0x76

    const/16 v3, 0x77

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->MusicCollectionRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 202
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "MusicCollectionResponse"

    const/16 v2, 0x77

    const/16 v3, 0x78

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->MusicCollectionResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 203
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "MusicCapabilitiesRequest"

    const/16 v2, 0x78

    const/16 v3, 0x79

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->MusicCapabilitiesRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 204
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "MusicCapabilitiesResponse"

    const/16 v2, 0x79

    const/16 v3, 0x7a

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->MusicCapabilitiesResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 205
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "MusicArtworkRequest"

    const/16 v2, 0x7a

    const/16 v3, 0x7b

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->MusicArtworkRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 206
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "MusicArtworkResponse"

    const/16 v2, 0x7b

    const/16 v3, 0x7c

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->MusicArtworkResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 207
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "DashboardPreferences"

    const/16 v2, 0x7c

    const/16 v3, 0x7d

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->DashboardPreferences:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 208
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "ClearGlances"

    const/16 v2, 0x7d

    const/16 v3, 0x7e

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->ClearGlances:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 209
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "PlaceTypeSearchRequest"

    const/16 v2, 0x7e

    const/16 v3, 0x7f

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->PlaceTypeSearchRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 210
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "PlaceTypeSearchResponse"

    const/16 v2, 0x7f

    const/16 v3, 0x80

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->PlaceTypeSearchResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 211
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "AccelerateShutdown"

    const/16 v2, 0x80

    const/16 v3, 0x81

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->AccelerateShutdown:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 212
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "DestinationSelectedRequest"

    const/16 v2, 0x81

    const/16 v3, 0x82

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->DestinationSelectedRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 213
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "DestinationSelectedResponse"

    const/16 v2, 0x82

    const/16 v3, 0x83

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->DestinationSelectedResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 214
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "PhotoUpdateQuery"

    const/16 v2, 0x83

    const/16 v3, 0x84

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->PhotoUpdateQuery:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 215
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "PhotoUpdateQueryResponse"

    const/16 v2, 0x84

    const/16 v3, 0x85

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->PhotoUpdateQueryResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 216
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "ResumeMusicRequest"

    const/16 v2, 0x85

    const/16 v3, 0x86

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->ResumeMusicRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 217
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "CannedMessagesRequest"

    const/16 v2, 0x86

    const/16 v3, 0x87

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->CannedMessagesRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 218
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "CannedMessagesUpdate"

    const/16 v2, 0x87

    const/16 v3, 0x88

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->CannedMessagesUpdate:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 219
    new-instance v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const-string v1, "MusicCollectionSourceUpdate"

    const/16 v2, 0x88

    const/16 v3, 0x89

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$MessageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->MusicCollectionSourceUpdate:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 81
    const/16 v0, 0x89

    new-array v0, v0, [Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    const/4 v1, 0x0

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->Coordinate:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    sget-object v1, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->Notification:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->CallEvent:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->FileListRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->FileListResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->PreviewFileRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v1, v0, v8

    const/4 v1, 0x6

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->PreviewFileResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->NavigationManeuverEvent:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->NavigationRouteRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->NavigationRouteResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->NavigationSessionRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->NavigationSessionResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->NavigationSessionStatusEvent:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->DismissScreen:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->ShowScreen:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->GestureEvent:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->PlacesSearchRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->PlacesSearchResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->ReadSettingsRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->ReadSettingsResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->UpdateSettings:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->ConnectionStateChange:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->SpeechRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->TelephonyRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->TelephonyResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->NotificationEvent:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->DeviceInfo:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->PhoneEvent:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->VoiceAssistRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->PhotoRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->PhotoResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->NotificationListRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->NotificationListResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->ShowCustomNotification:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->DateTimeConfiguration:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->FileTransferResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->FileTransferRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->FileTransferData:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->FileTransferStatus:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->Ping:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->NotificationsStateChange:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->NotificationsStateRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->MusicTrackInfoRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->MusicTrackInfo:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->MusicEvent:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->DialStatusRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->DialStatusResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->DialBondRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->DialBondResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->DriverProfilePreferencesRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->DriverProfilePreferencesUpdate:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->TransmitLocation:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->DisconnectRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->VoiceAssistResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->GetNavigationSessionState:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->RouteManeuverRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->RouteManeuverResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->NavigationPreferencesRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->NavigationPreferencesUpdate:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->DialSimulationEvent:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->MediaRemoteKeyEvent:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->ContactRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->ContactResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->FavoriteContactsRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x40

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->FavoriteContactsResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x41

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->FavoriteDestinationsRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x42

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->FavoriteDestinationsUpdate:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x43

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->RecommendedDestinationsRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x44

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->RecommendedDestinationsUpdate:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x45

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->ConnectionRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x46

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->ConnectionStatus:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x47

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->PhoneStatusRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x48

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->PhoneStatusResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x49

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->LaunchAppEvent:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->InputPreferencesRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->InputPreferencesUpdate:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->PhoneBatteryStatus:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->SmsMessageRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->SmsMessageResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->ObdStatusRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x50

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->ObdStatusResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x51

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->TripUpdate:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x52

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->TripUpdateAck:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x53

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->PhotoUpdatesRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x54

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->PhotoUpdate:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x55

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->DisplaySpeakerPreferencesRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x56

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->DisplaySpeakerPreferencesUpdate:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x57

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->NavigationRouteStatus:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x58

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->NavigationRouteCancelRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x59

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->NotificationPreferencesRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->NotificationPreferencesUpdate:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x5b

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->StartDriveRecordingEvent:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x5c

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->StopDriveRecordingEvent:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x5d

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->DriveRecordingsRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x5e

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->DriveRecordingsResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x5f

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->StartDrivePlaybackEvent:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x60

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->StopDrivePlaybackEvent:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x61

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->StartDriveRecordingResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x62

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->StartDrivePlaybackResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x63

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->NavigationSessionRouteChange:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x64

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->NetworkStateChange:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x65

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->StopDriveRecordingResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x66

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->NavigationSessionDeferRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x67

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->CallStateUpdateRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x68

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->AutoCompleteRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x69

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->AutoCompleteResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x6a

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->GlanceEvent:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x6b

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->NowPlayingUpdateRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x6c

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->CancelSpeechRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x6d

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->NetworkLinkReady:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x6e

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->SuggestedDestination:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x6f

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->CalendarEvent:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x70

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->CalendarEventUpdates:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x71

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->SpeechRequestStatus:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x72

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->AudioStatus:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x73

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->LinkPropertiesChanged:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x74

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->VoiceSearchRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x75

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->VoiceSearchResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x76

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->MusicCollectionRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x77

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->MusicCollectionResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x78

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->MusicCapabilitiesRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x79

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->MusicCapabilitiesResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x7a

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->MusicArtworkRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x7b

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->MusicArtworkResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x7c

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->DashboardPreferences:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x7d

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->ClearGlances:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x7e

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->PlaceTypeSearchRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x7f

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->PlaceTypeSearchResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x80

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->AccelerateShutdown:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x81

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->DestinationSelectedRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x82

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->DestinationSelectedResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x83

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->PhotoUpdateQuery:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x84

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->PhotoUpdateQueryResponse:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x85

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->ResumeMusicRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x86

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->CannedMessagesRequest:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x87

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->CannedMessagesUpdate:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    const/16 v1, 0x88

    sget-object v2, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->MusicCollectionSourceUpdate:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->$VALUES:[Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 223
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 224
    iput p3, p0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->value:I

    .line 225
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/NavdyEvent$MessageType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 81
    const-class v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    return-object v0
.end method

.method public static values()[Lcom/navdy/service/library/events/NavdyEvent$MessageType;
    .locals 1

    .prologue
    .line 81
    sget-object v0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->$VALUES:[Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    invoke-virtual {v0}, [Lcom/navdy/service/library/events/NavdyEvent$MessageType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 229
    iget v0, p0, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->value:I

    return v0
.end method

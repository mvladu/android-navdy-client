.class public Lcom/navdy/service/library/file/FileTransferSessionManager;
.super Ljava/lang/Object;
.source "FileTransferSessionManager.java"

# interfaces
.implements Lcom/navdy/service/library/file/IFileTransferManager;


# static fields
.field private static final MAX_SESSIONS:I = 0x5

.field private static final SESSION_TIMEOUT_INTERVAL:I = 0xea60

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mFileTransferAuthority:Lcom/navdy/service/library/file/IFileTransferAuthority;

.field private mFileTransferSessionsIdIndexed:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/navdy/service/library/file/FileTransferSession;",
            ">;"
        }
    .end annotation
.end field

.field private mFileTransferSessionsPathIndexed:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/navdy/service/library/file/FileTransferSession;",
            ">;"
        }
    .end annotation
.end field

.field private mTransferId:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 37
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/service/library/file/FileTransferSessionManager;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/service/library/file/FileTransferSessionManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/navdy/service/library/file/IFileTransferAuthority;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "authority"    # Lcom/navdy/service/library/file/IFileTransferAuthority;

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/navdy/service/library/file/FileTransferSessionManager;->mTransferId:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 40
    iput-object p1, p0, Lcom/navdy/service/library/file/FileTransferSessionManager;->mContext:Landroid/content/Context;

    .line 41
    iput-object p2, p0, Lcom/navdy/service/library/file/FileTransferSessionManager;->mFileTransferAuthority:Lcom/navdy/service/library/file/IFileTransferAuthority;

    .line 42
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/navdy/service/library/file/FileTransferSessionManager;->mFileTransferSessionsPathIndexed:Ljava/util/HashMap;

    .line 43
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/navdy/service/library/file/FileTransferSessionManager;->mFileTransferSessionsIdIndexed:Ljava/util/HashMap;

    .line 44
    return-void
.end method

.method public static final absolutePath(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "destinationFolder"    # Ljava/lang/String;
    .param p2, "destinationFileName"    # Ljava/lang/String;

    .prologue
    .line 232
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz p1, :cond_0

    .end local p1    # "destinationFolder":Ljava/lang/String;
    :goto_0
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .restart local p1    # "destinationFolder":Ljava/lang/String;
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object p1

    goto :goto_0
.end method

.method private endFileTransferSession(Lcom/navdy/service/library/file/FileTransferSession;Z)V
    .locals 5
    .param p1, "session"    # Lcom/navdy/service/library/file/FileTransferSession;
    .param p2, "deleteFile"    # Z

    .prologue
    .line 197
    if-eqz p1, :cond_0

    .line 199
    monitor-enter p1

    .line 200
    :try_start_0
    iget-object v2, p1, Lcom/navdy/service/library/file/FileTransferSession;->mFileName:Ljava/lang/String;

    .line 201
    .local v2, "fileName":Ljava/lang/String;
    iget-object v1, p1, Lcom/navdy/service/library/file/FileTransferSession;->mDestinationFolder:Ljava/lang/String;

    .line 202
    .local v1, "destFolder":Ljava/lang/String;
    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 203
    iget-object v3, p0, Lcom/navdy/service/library/file/FileTransferSessionManager;->mContext:Landroid/content/Context;

    invoke-static {v3, v1, v2}, Lcom/navdy/service/library/file/FileTransferSessionManager;->absolutePath(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 205
    .local v0, "absolutePath":Ljava/lang/String;
    iget-object v3, p0, Lcom/navdy/service/library/file/FileTransferSessionManager;->mFileTransferSessionsPathIndexed:Ljava/util/HashMap;

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 206
    iget-object v3, p0, Lcom/navdy/service/library/file/FileTransferSessionManager;->mFileTransferSessionsIdIndexed:Ljava/util/HashMap;

    iget v4, p1, Lcom/navdy/service/library/file/FileTransferSession;->mTransferId:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 207
    iget-object v3, p0, Lcom/navdy/service/library/file/FileTransferSessionManager;->mContext:Landroid/content/Context;

    invoke-virtual {p1, v3, p2}, Lcom/navdy/service/library/file/FileTransferSession;->endSession(Landroid/content/Context;Z)V

    .line 209
    .end local v0    # "absolutePath":Ljava/lang/String;
    .end local v1    # "destFolder":Ljava/lang/String;
    .end local v2    # "fileName":Ljava/lang/String;
    :cond_0
    return-void

    .line 202
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3
.end method

.method private endSessions(Z)I
    .locals 10
    .param p1, "onlyInactiveSession"    # Z

    .prologue
    .line 118
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 119
    .local v0, "currentTimeStamp":J
    const/4 v3, 0x0

    .line 120
    .local v3, "removed":I
    iget-object v5, p0, Lcom/navdy/service/library/file/FileTransferSessionManager;->mFileTransferSessionsIdIndexed:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 121
    .local v2, "keysIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 122
    iget-object v5, p0, Lcom/navdy/service/library/file/FileTransferSessionManager;->mFileTransferSessionsIdIndexed:Ljava/util/HashMap;

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/navdy/service/library/file/FileTransferSession;

    .line 123
    .local v4, "session":Lcom/navdy/service/library/file/FileTransferSession;
    if-eqz p1, :cond_1

    if-eqz v4, :cond_0

    .line 125
    invoke-virtual {v4}, Lcom/navdy/service/library/file/FileTransferSession;->getLastActivity()J

    move-result-wide v6

    sub-long v6, v0, v6

    const-wide/32 v8, 0xea60

    cmp-long v5, v6, v8

    if-lez v5, :cond_0

    .line 126
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    .line 127
    const/4 v5, 0x0

    invoke-direct {p0, v4, v5}, Lcom/navdy/service/library/file/FileTransferSessionManager;->endFileTransferSession(Lcom/navdy/service/library/file/FileTransferSession;Z)V

    .line 128
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 131
    .end local v4    # "session":Lcom/navdy/service/library/file/FileTransferSession;
    :cond_2
    return v3
.end method

.method public static isPullRequest(Lcom/navdy/service/library/events/file/FileType;)Z
    .locals 2
    .param p0, "fileType"    # Lcom/navdy/service/library/events/file/FileType;

    .prologue
    .line 278
    sget-object v0, Lcom/navdy/service/library/file/FileTransferSessionManager$1;->$SwitchMap$com$navdy$service$library$events$file$FileType:[I

    invoke-virtual {p0}, Lcom/navdy/service/library/events/file/FileType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 282
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 280
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 278
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public static prepareFileTransferData(ILcom/navdy/service/library/file/TransferDataSource;IIJJ)Lcom/navdy/service/library/events/file/FileTransferData;
    .locals 18
    .param p0, "transferId"    # I
    .param p1, "source"    # Lcom/navdy/service/library/file/TransferDataSource;
    .param p2, "chunkIndex"    # I
    .param p3, "chunkSize"    # I
    .param p4, "startOffset"    # J
    .param p6, "alreadyTransferred"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 246
    const/4 v6, 0x0

    .line 248
    .local v6, "fileReader":Ljava/io/RandomAccessFile;
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Lcom/navdy/service/library/file/TransferDataSource;->length()J

    move-result-wide v14

    .line 249
    .local v14, "totalFileSize":J
    sub-long v10, v14, p6

    .line 250
    .local v10, "remainingBytes":J
    move/from16 v9, p3

    .line 251
    .local v9, "nextChunkSize":I
    const/4 v8, 0x0

    .line 252
    .local v8, "lastChunk":Z
    move/from16 v0, p3

    int-to-long v0, v0

    move-wide/from16 v16, v0

    cmp-long v13, v10, v16

    if-gez v13, :cond_0

    .line 253
    long-to-int v9, v10

    .line 254
    const/4 v8, 0x1

    .line 256
    :cond_0
    new-array v4, v9, [B

    .line 258
    .local v4, "chunk":[B
    move-object/from16 v0, p1

    move-wide/from16 v1, p6

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/file/TransferDataSource;->seek(J)V

    .line 260
    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/navdy/service/library/file/TransferDataSource;->read([B)I

    .line 261
    new-instance v13, Lcom/navdy/service/library/events/file/FileTransferData$Builder;

    invoke-direct {v13}, Lcom/navdy/service/library/events/file/FileTransferData$Builder;-><init>()V

    .line 262
    invoke-static/range {p0 .. p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Lcom/navdy/service/library/events/file/FileTransferData$Builder;->transferId(Ljava/lang/Integer;)Lcom/navdy/service/library/events/file/FileTransferData$Builder;

    move-result-object v13

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Lcom/navdy/service/library/events/file/FileTransferData$Builder;->chunkIndex(Ljava/lang/Integer;)Lcom/navdy/service/library/events/file/FileTransferData$Builder;

    move-result-object v7

    .line 263
    .local v7, "fileTransferDataBuilder":Lcom/navdy/service/library/events/file/FileTransferData$Builder;
    if-eqz v8, :cond_1

    .line 264
    invoke-virtual/range {p1 .. p1}, Lcom/navdy/service/library/file/TransferDataSource;->checkSum()Ljava/lang/String;

    move-result-object v5

    .line 265
    .local v5, "fileChecksum":Ljava/lang/String;
    invoke-virtual {v7, v5}, Lcom/navdy/service/library/events/file/FileTransferData$Builder;->fileCheckSum(Ljava/lang/String;)Lcom/navdy/service/library/events/file/FileTransferData$Builder;

    .line 267
    .end local v5    # "fileChecksum":Ljava/lang/String;
    :cond_1
    invoke-static {v4}, Lokio/ByteString;->of([B)Lokio/ByteString;

    move-result-object v13

    invoke-virtual {v7, v13}, Lcom/navdy/service/library/events/file/FileTransferData$Builder;->dataBytes(Lokio/ByteString;)Lcom/navdy/service/library/events/file/FileTransferData$Builder;

    move-result-object v13

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Lcom/navdy/service/library/events/file/FileTransferData$Builder;->lastChunk(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/file/FileTransferData$Builder;

    move-result-object v13

    invoke-virtual {v13}, Lcom/navdy/service/library/events/file/FileTransferData$Builder;->build()Lcom/navdy/service/library/events/file/FileTransferData;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v13

    return-object v13

    .line 268
    .end local v4    # "chunk":[B
    .end local v7    # "fileTransferDataBuilder":Lcom/navdy/service/library/events/file/FileTransferData$Builder;
    .end local v8    # "lastChunk":Z
    .end local v9    # "nextChunkSize":I
    .end local v10    # "remainingBytes":J
    .end local v14    # "totalFileSize":J
    :catch_0
    move-exception v12

    .line 269
    .local v12, "t":Ljava/lang/Throwable;
    sget-object v13, Lcom/navdy/service/library/file/FileTransferSessionManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "Exception while preparing the chunk data "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 270
    throw v12
.end method


# virtual methods
.method public absolutePathForTransferId(I)Ljava/lang/String;
    .locals 5
    .param p1, "transferId"    # I

    .prologue
    .line 219
    iget-object v3, p0, Lcom/navdy/service/library/file/FileTransferSessionManager;->mFileTransferSessionsIdIndexed:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/service/library/file/FileTransferSession;

    .line 220
    .local v2, "transferSession":Lcom/navdy/service/library/file/FileTransferSession;
    if-eqz v2, :cond_0

    .line 222
    monitor-enter v2

    .line 223
    :try_start_0
    iget-object v1, v2, Lcom/navdy/service/library/file/FileTransferSession;->mFileName:Ljava/lang/String;

    .line 224
    .local v1, "fileName":Ljava/lang/String;
    iget-object v0, v2, Lcom/navdy/service/library/file/FileTransferSession;->mDestinationFolder:Ljava/lang/String;

    .line 225
    .local v0, "destFolder":Ljava/lang/String;
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 226
    iget-object v3, p0, Lcom/navdy/service/library/file/FileTransferSessionManager;->mContext:Landroid/content/Context;

    invoke-static {v3, v0, v1}, Lcom/navdy/service/library/file/FileTransferSessionManager;->absolutePath(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 228
    .end local v0    # "destFolder":Ljava/lang/String;
    .end local v1    # "fileName":Ljava/lang/String;
    :goto_0
    return-object v3

    .line 225
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3

    .line 228
    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public endFileTransferSession(IZ)V
    .locals 3
    .param p1, "transferId"    # I
    .param p2, "deleteFile"    # Z

    .prologue
    .line 212
    iget-object v1, p0, Lcom/navdy/service/library/file/FileTransferSessionManager;->mFileTransferSessionsIdIndexed:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/file/FileTransferSession;

    .line 213
    .local v0, "session":Lcom/navdy/service/library/file/FileTransferSession;
    if-eqz v0, :cond_0

    .line 214
    invoke-direct {p0, v0, p2}, Lcom/navdy/service/library/file/FileTransferSessionManager;->endFileTransferSession(Lcom/navdy/service/library/file/FileTransferSession;Z)V

    .line 216
    :cond_0
    return-void
.end method

.method public getFileType(I)Lcom/navdy/service/library/events/file/FileType;
    .locals 3
    .param p1, "transferId"    # I

    .prologue
    .line 187
    iget-object v1, p0, Lcom/navdy/service/library/file/FileTransferSessionManager;->mFileTransferSessionsIdIndexed:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/file/FileTransferSession;

    .line 188
    .local v0, "session":Lcom/navdy/service/library/file/FileTransferSession;
    if-eqz v0, :cond_0

    .line 189
    invoke-virtual {v0}, Lcom/navdy/service/library/file/FileTransferSession;->getFileType()Lcom/navdy/service/library/events/file/FileType;

    move-result-object v1

    .line 191
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getNextChunk(I)Lcom/navdy/service/library/events/file/FileTransferData;
    .locals 4
    .param p1, "transferId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 174
    iget-object v2, p0, Lcom/navdy/service/library/file/FileTransferSessionManager;->mFileTransferSessionsIdIndexed:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/service/library/file/FileTransferSession;

    .line 175
    .local v1, "session":Lcom/navdy/service/library/file/FileTransferSession;
    if-eqz v1, :cond_2

    .line 176
    invoke-virtual {v1}, Lcom/navdy/service/library/file/FileTransferSession;->getNextChunk()Lcom/navdy/service/library/events/file/FileTransferData;

    move-result-object v0

    .line 177
    .local v0, "data":Lcom/navdy/service/library/events/file/FileTransferData;
    if-eqz v0, :cond_0

    iget-object v2, v0, Lcom/navdy/service/library/events/file/FileTransferData;->lastChunk:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/navdy/service/library/file/FileTransferSession;->isFlowControlEnabled()Z

    move-result v2

    if-nez v2, :cond_1

    .line 178
    :cond_0
    const/4 v2, 0x1

    invoke-direct {p0, v1, v2}, Lcom/navdy/service/library/file/FileTransferSessionManager;->endFileTransferSession(Lcom/navdy/service/library/file/FileTransferSession;Z)V

    .line 182
    .end local v0    # "data":Lcom/navdy/service/library/events/file/FileTransferData;
    :cond_1
    :goto_0
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public handleFileTransferData(Lcom/navdy/service/library/events/file/FileTransferData;)Lcom/navdy/service/library/events/file/FileTransferStatus;
    .locals 5
    .param p1, "data"    # Lcom/navdy/service/library/events/file/FileTransferData;

    .prologue
    const/4 v4, 0x0

    .line 154
    iget-object v2, p0, Lcom/navdy/service/library/file/FileTransferSessionManager;->mFileTransferSessionsIdIndexed:Ljava/util/HashMap;

    iget-object v3, p1, Lcom/navdy/service/library/events/file/FileTransferData;->transferId:Ljava/lang/Integer;

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/file/FileTransferSession;

    .line 156
    .local v0, "session":Lcom/navdy/service/library/file/FileTransferSession;
    if-eqz v0, :cond_2

    .line 157
    iget-object v2, p0, Lcom/navdy/service/library/file/FileTransferSessionManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v2, p1}, Lcom/navdy/service/library/file/FileTransferSession;->appendChunk(Landroid/content/Context;Lcom/navdy/service/library/events/file/FileTransferData;)Lcom/navdy/service/library/events/file/FileTransferStatus;

    move-result-object v1

    .line 158
    .local v1, "status":Lcom/navdy/service/library/events/file/FileTransferStatus;
    iget-object v2, v1, Lcom/navdy/service/library/events/file/FileTransferStatus;->transferComplete:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, v1, Lcom/navdy/service/library/events/file/FileTransferStatus;->error:Lcom/navdy/service/library/events/file/FileTransferError;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/navdy/service/library/events/file/FileTransferStatus;->error:Lcom/navdy/service/library/events/file/FileTransferError;

    sget-object v3, Lcom/navdy/service/library/events/file/FileTransferError;->FILE_TRANSFER_NO_ERROR:Lcom/navdy/service/library/events/file/FileTransferError;

    if-eq v2, v3, :cond_1

    .line 159
    :cond_0
    invoke-direct {p0, v0, v4}, Lcom/navdy/service/library/file/FileTransferSessionManager;->endFileTransferSession(Lcom/navdy/service/library/file/FileTransferSession;Z)V

    .line 168
    .end local v1    # "status":Lcom/navdy/service/library/events/file/FileTransferStatus;
    :cond_1
    :goto_0
    return-object v1

    .line 164
    :cond_2
    new-instance v2, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;

    invoke-direct {v2}, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;-><init>()V

    .line 165
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->success(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;

    move-result-object v2

    .line 166
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->transferComplete(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;

    move-result-object v2

    sget-object v3, Lcom/navdy/service/library/events/file/FileTransferError;->FILE_TRANSFER_NOT_INITIATED:Lcom/navdy/service/library/events/file/FileTransferError;

    .line 167
    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->error(Lcom/navdy/service/library/events/file/FileTransferError;)Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;

    move-result-object v2

    .line 168
    invoke-virtual {v2}, Lcom/navdy/service/library/events/file/FileTransferStatus$Builder;->build()Lcom/navdy/service/library/events/file/FileTransferStatus;

    move-result-object v1

    goto :goto_0
.end method

.method public handleFileTransferRequest(Lcom/navdy/service/library/events/file/FileTransferRequest;)Lcom/navdy/service/library/events/file/FileTransferResponse;
    .locals 18
    .param p1, "request"    # Lcom/navdy/service/library/events/file/FileTransferRequest;

    .prologue
    .line 49
    if-nez p1, :cond_1

    .line 50
    const/4 v15, 0x0

    .line 109
    :cond_0
    :goto_0
    return-object v15

    .line 53
    :cond_1
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/service/library/file/FileTransferSessionManager;->mFileTransferAuthority:Lcom/navdy/service/library/file/IFileTransferAuthority;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/navdy/service/library/events/file/FileTransferRequest;->fileType:Lcom/navdy/service/library/events/file/FileType;

    invoke-interface {v2, v3}, Lcom/navdy/service/library/file/IFileTransferAuthority;->isFileTypeAllowed(Lcom/navdy/service/library/events/file/FileType;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 54
    new-instance v2, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    invoke-direct {v2}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;-><init>()V

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->success(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    move-result-object v2

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/navdy/service/library/events/file/FileTransferRequest;->destinationFileName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->destinationFileName(Ljava/lang/String;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    move-result-object v2

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/navdy/service/library/events/file/FileTransferRequest;->fileType:Lcom/navdy/service/library/events/file/FileType;

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->fileType(Lcom/navdy/service/library/events/file/FileType;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    move-result-object v2

    sget-object v3, Lcom/navdy/service/library/events/file/FileTransferError;->FILE_TRANSFER_PERMISSION_DENIED:Lcom/navdy/service/library/events/file/FileTransferError;

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->error(Lcom/navdy/service/library/events/file/FileTransferError;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->build()Lcom/navdy/service/library/events/file/FileTransferResponse;

    move-result-object v15

    goto :goto_0

    .line 56
    :cond_2
    const/4 v1, 0x0

    .line 57
    .local v1, "session":Lcom/navdy/service/library/file/FileTransferSession;
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/navdy/service/library/events/file/FileTransferRequest;->fileType:Lcom/navdy/service/library/events/file/FileType;

    sget-object v3, Lcom/navdy/service/library/events/file/FileType;->FILE_TYPE_PERF_TEST:Lcom/navdy/service/library/events/file/FileType;

    if-ne v2, v3, :cond_3

    .line 59
    new-instance v1, Lcom/navdy/service/library/file/FileTransferSession;

    .end local v1    # "session":Lcom/navdy/service/library/file/FileTransferSession;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/service/library/file/FileTransferSessionManager;->mTransferId:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v2

    invoke-direct {v1, v2}, Lcom/navdy/service/library/file/FileTransferSession;-><init>(I)V

    .line 60
    .restart local v1    # "session":Lcom/navdy/service/library/file/FileTransferSession;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/service/library/file/FileTransferSessionManager;->mContext:Landroid/content/Context;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/navdy/service/library/events/file/FileTransferRequest;->fileType:Lcom/navdy/service/library/events/file/FileType;

    const-string v4, "<TESTDATA>"

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/navdy/service/library/events/file/FileTransferRequest;->fileSize:Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-virtual/range {v1 .. v6}, Lcom/navdy/service/library/file/FileTransferSession;->initTestData(Landroid/content/Context;Lcom/navdy/service/library/events/file/FileType;Ljava/lang/String;J)Lcom/navdy/service/library/events/file/FileTransferResponse;

    move-result-object v15

    .line 61
    .local v15, "response":Lcom/navdy/service/library/events/file/FileTransferResponse;
    iget-object v2, v15, Lcom/navdy/service/library/events/file/FileTransferResponse;->success:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 62
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/service/library/file/FileTransferSessionManager;->mFileTransferSessionsIdIndexed:Ljava/util/HashMap;

    iget v3, v1, Lcom/navdy/service/library/file/FileTransferSession;->mTransferId:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 107
    .end local v1    # "session":Lcom/navdy/service/library/file/FileTransferSession;
    .end local v15    # "response":Lcom/navdy/service/library/events/file/FileTransferResponse;
    :catch_0
    move-exception v16

    .line 108
    .local v16, "t":Ljava/lang/Throwable;
    sget-object v2, Lcom/navdy/service/library/file/FileTransferSessionManager;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Exception in handle file request "

    move-object/from16 v0, v16

    invoke-virtual {v2, v3, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 109
    new-instance v2, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    invoke-direct {v2}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;-><init>()V

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->success(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    move-result-object v2

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/navdy/service/library/events/file/FileTransferRequest;->destinationFileName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->destinationFileName(Ljava/lang/String;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    move-result-object v2

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/navdy/service/library/events/file/FileTransferRequest;->fileType:Lcom/navdy/service/library/events/file/FileType;

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->fileType(Lcom/navdy/service/library/events/file/FileType;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    move-result-object v2

    sget-object v3, Lcom/navdy/service/library/events/file/FileTransferError;->FILE_TRANSFER_IO_ERROR:Lcom/navdy/service/library/events/file/FileTransferError;

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->error(Lcom/navdy/service/library/events/file/FileTransferError;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->build()Lcom/navdy/service/library/events/file/FileTransferResponse;

    move-result-object v15

    goto/16 :goto_0

    .line 65
    .end local v16    # "t":Ljava/lang/Throwable;
    .restart local v1    # "session":Lcom/navdy/service/library/file/FileTransferSession;
    :cond_3
    :try_start_1
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/navdy/service/library/events/file/FileTransferRequest;->fileType:Lcom/navdy/service/library/events/file/FileType;

    invoke-static {v2}, Lcom/navdy/service/library/file/FileTransferSessionManager;->isPullRequest(Lcom/navdy/service/library/events/file/FileType;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 66
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/service/library/file/FileTransferSessionManager;->mFileTransferAuthority:Lcom/navdy/service/library/file/IFileTransferAuthority;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/navdy/service/library/events/file/FileTransferRequest;->fileType:Lcom/navdy/service/library/events/file/FileType;

    invoke-interface {v2, v3}, Lcom/navdy/service/library/file/IFileTransferAuthority;->getDirectoryForFileType(Lcom/navdy/service/library/events/file/FileType;)Ljava/lang/String;

    move-result-object v4

    .line 67
    .local v4, "destinationFolder":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/service/library/file/FileTransferSessionManager;->mContext:Landroid/content/Context;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/navdy/service/library/events/file/FileTransferRequest;->destinationFileName:Ljava/lang/String;

    invoke-static {v2, v4, v3}, Lcom/navdy/service/library/file/FileTransferSessionManager;->absolutePath(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 68
    .local v13, "absolutePath":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/service/library/file/FileTransferSessionManager;->mFileTransferSessionsPathIndexed:Ljava/util/HashMap;

    invoke-virtual {v2, v13}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "session":Lcom/navdy/service/library/file/FileTransferSession;
    check-cast v1, Lcom/navdy/service/library/file/FileTransferSession;

    .line 69
    .restart local v1    # "session":Lcom/navdy/service/library/file/FileTransferSession;
    if-nez v1, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/service/library/file/FileTransferSessionManager;->mFileTransferSessionsPathIndexed:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    const/4 v3, 0x5

    if-ne v2, v3, :cond_4

    .line 71
    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/navdy/service/library/file/FileTransferSessionManager;->endSessions(Z)I

    move-result v2

    if-nez v2, :cond_4

    .line 73
    new-instance v2, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    invoke-direct {v2}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;-><init>()V

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->success(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    move-result-object v2

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/navdy/service/library/events/file/FileTransferRequest;->destinationFileName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->destinationFileName(Ljava/lang/String;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    move-result-object v2

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/navdy/service/library/events/file/FileTransferRequest;->fileType:Lcom/navdy/service/library/events/file/FileType;

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->fileType(Lcom/navdy/service/library/events/file/FileType;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    move-result-object v2

    sget-object v3, Lcom/navdy/service/library/events/file/FileTransferError;->FILE_TRANSFER_HOST_BUSY:Lcom/navdy/service/library/events/file/FileTransferError;

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->error(Lcom/navdy/service/library/events/file/FileTransferError;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->build()Lcom/navdy/service/library/events/file/FileTransferResponse;

    move-result-object v15

    goto/16 :goto_0

    .line 76
    :cond_4
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v2}, Lcom/navdy/service/library/file/FileTransferSessionManager;->endFileTransferSession(Lcom/navdy/service/library/file/FileTransferSession;Z)V

    .line 78
    new-instance v1, Lcom/navdy/service/library/file/FileTransferSession;

    .end local v1    # "session":Lcom/navdy/service/library/file/FileTransferSession;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/service/library/file/FileTransferSessionManager;->mTransferId:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v2

    invoke-direct {v1, v2}, Lcom/navdy/service/library/file/FileTransferSession;-><init>(I)V

    .line 79
    .restart local v1    # "session":Lcom/navdy/service/library/file/FileTransferSession;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/service/library/file/FileTransferSessionManager;->mContext:Landroid/content/Context;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/navdy/service/library/events/file/FileTransferRequest;->fileType:Lcom/navdy/service/library/events/file/FileType;

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/navdy/service/library/events/file/FileTransferRequest;->destinationFileName:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/navdy/service/library/events/file/FileTransferRequest;->fileSize:Ljava/lang/Long;

    .line 81
    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/navdy/service/library/events/file/FileTransferRequest;->offset:Ljava/lang/Long;

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    move-object/from16 v0, p1

    iget-object v10, v0, Lcom/navdy/service/library/events/file/FileTransferRequest;->fileDataChecksum:Ljava/lang/String;

    sget-object v11, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/service/library/events/file/FileTransferRequest;->override:Ljava/lang/Boolean;

    move-object/from16 v17, v0

    .line 82
    move-object/from16 v0, v17

    invoke-virtual {v11, v0}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v11

    .line 79
    invoke-virtual/range {v1 .. v11}, Lcom/navdy/service/library/file/FileTransferSession;->initFileTransfer(Landroid/content/Context;Lcom/navdy/service/library/events/file/FileType;Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;Z)Lcom/navdy/service/library/events/file/FileTransferResponse;

    move-result-object v15

    .line 83
    .restart local v15    # "response":Lcom/navdy/service/library/events/file/FileTransferResponse;
    iget-object v2, v15, Lcom/navdy/service/library/events/file/FileTransferResponse;->success:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 84
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/service/library/file/FileTransferSessionManager;->mFileTransferSessionsIdIndexed:Ljava/util/HashMap;

    iget v3, v1, Lcom/navdy/service/library/file/FileTransferSession;->mTransferId:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/service/library/file/FileTransferSessionManager;->mFileTransferSessionsPathIndexed:Ljava/util/HashMap;

    invoke-virtual {v2, v13, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 90
    .end local v4    # "destinationFolder":Ljava/lang/String;
    .end local v13    # "absolutePath":Ljava/lang/String;
    .end local v15    # "response":Lcom/navdy/service/library/events/file/FileTransferResponse;
    :cond_5
    new-instance v1, Lcom/navdy/service/library/file/FileTransferSession;

    .end local v1    # "session":Lcom/navdy/service/library/file/FileTransferSession;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/service/library/file/FileTransferSessionManager;->mTransferId:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v2

    invoke-direct {v1, v2}, Lcom/navdy/service/library/file/FileTransferSession;-><init>(I)V

    .line 91
    .restart local v1    # "session":Lcom/navdy/service/library/file/FileTransferSession;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/service/library/file/FileTransferSessionManager;->mFileTransferAuthority:Lcom/navdy/service/library/file/IFileTransferAuthority;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/navdy/service/library/events/file/FileTransferRequest;->fileType:Lcom/navdy/service/library/events/file/FileType;

    invoke-interface {v2, v3}, Lcom/navdy/service/library/file/IFileTransferAuthority;->getDirectoryForFileType(Lcom/navdy/service/library/events/file/FileType;)Ljava/lang/String;

    move-result-object v8

    .line 92
    .local v8, "tempLogFolder":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/service/library/file/FileTransferSessionManager;->mFileTransferAuthority:Lcom/navdy/service/library/file/IFileTransferAuthority;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/navdy/service/library/events/file/FileTransferRequest;->fileType:Lcom/navdy/service/library/events/file/FileType;

    invoke-interface {v2, v3}, Lcom/navdy/service/library/file/IFileTransferAuthority;->getFileToSend(Lcom/navdy/service/library/events/file/FileType;)Ljava/lang/String;

    move-result-object v12

    .line 93
    .local v12, "absoluteFilePath":Ljava/lang/String;
    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 94
    new-instance v14, Ljava/io/File;

    invoke-direct {v14, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 95
    .local v14, "fileToSend":Ljava/io/File;
    invoke-virtual {v14}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_6

    .line 96
    new-instance v2, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    invoke-direct {v2}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;-><init>()V

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->success(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    move-result-object v2

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/navdy/service/library/events/file/FileTransferRequest;->destinationFileName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->destinationFileName(Ljava/lang/String;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    move-result-object v2

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/navdy/service/library/events/file/FileTransferRequest;->fileType:Lcom/navdy/service/library/events/file/FileType;

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->fileType(Lcom/navdy/service/library/events/file/FileType;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    move-result-object v2

    sget-object v3, Lcom/navdy/service/library/events/file/FileTransferError;->FILE_TRANSFER_IO_ERROR:Lcom/navdy/service/library/events/file/FileTransferError;

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->error(Lcom/navdy/service/library/events/file/FileTransferError;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->build()Lcom/navdy/service/library/events/file/FileTransferResponse;

    move-result-object v15

    goto/16 :goto_0

    .line 98
    :cond_6
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/navdy/service/library/file/FileTransferSessionManager;->mContext:Landroid/content/Context;

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/navdy/service/library/events/file/FileTransferRequest;->fileType:Lcom/navdy/service/library/events/file/FileType;

    invoke-virtual {v14}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/navdy/service/library/events/file/FileTransferRequest;->supportsAcks:Ljava/lang/Boolean;

    sget-object v3, Lcom/navdy/service/library/events/file/FileTransferRequest;->DEFAULT_SUPPORTSACKS:Ljava/lang/Boolean;

    invoke-static {v2, v3}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    move-object v5, v1

    invoke-virtual/range {v5 .. v10}, Lcom/navdy/service/library/file/FileTransferSession;->initPull(Landroid/content/Context;Lcom/navdy/service/library/events/file/FileType;Ljava/lang/String;Ljava/lang/String;Z)Lcom/navdy/service/library/events/file/FileTransferResponse;

    move-result-object v15

    .line 99
    .restart local v15    # "response":Lcom/navdy/service/library/events/file/FileTransferResponse;
    iget-object v2, v15, Lcom/navdy/service/library/events/file/FileTransferResponse;->success:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 100
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/service/library/file/FileTransferSessionManager;->mFileTransferSessionsIdIndexed:Ljava/util/HashMap;

    iget v3, v1, Lcom/navdy/service/library/file/FileTransferSession;->mTransferId:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 104
    .end local v14    # "fileToSend":Ljava/io/File;
    .end local v15    # "response":Lcom/navdy/service/library/events/file/FileTransferResponse;
    :cond_7
    new-instance v2, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    invoke-direct {v2}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;-><init>()V

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->success(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    move-result-object v2

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/navdy/service/library/events/file/FileTransferRequest;->destinationFileName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->destinationFileName(Ljava/lang/String;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    move-result-object v2

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/navdy/service/library/events/file/FileTransferRequest;->fileType:Lcom/navdy/service/library/events/file/FileType;

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->fileType(Lcom/navdy/service/library/events/file/FileType;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    move-result-object v2

    sget-object v3, Lcom/navdy/service/library/events/file/FileTransferError;->FILE_TRANSFER_IO_ERROR:Lcom/navdy/service/library/events/file/FileTransferError;

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->error(Lcom/navdy/service/library/events/file/FileTransferError;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->build()Lcom/navdy/service/library/events/file/FileTransferResponse;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v15

    goto/16 :goto_0
.end method

.method public handleFileTransferStatus(Lcom/navdy/service/library/events/file/FileTransferStatus;)Z
    .locals 4
    .param p1, "fileTransferStatus"    # Lcom/navdy/service/library/events/file/FileTransferStatus;

    .prologue
    const/4 v1, 0x0

    .line 141
    if-nez p1, :cond_1

    .line 149
    :cond_0
    :goto_0
    return v1

    .line 144
    :cond_1
    iget-object v2, p0, Lcom/navdy/service/library/file/FileTransferSessionManager;->mFileTransferSessionsIdIndexed:Ljava/util/HashMap;

    iget-object v3, p1, Lcom/navdy/service/library/events/file/FileTransferStatus;->transferId:Ljava/lang/Integer;

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/file/FileTransferSession;

    .line 146
    .local v0, "session":Lcom/navdy/service/library/file/FileTransferSession;
    if-eqz v0, :cond_0

    .line 147
    invoke-virtual {v0, p1}, Lcom/navdy/service/library/file/FileTransferSession;->handleFileTransferStatus(Lcom/navdy/service/library/events/file/FileTransferStatus;)Z

    move-result v1

    goto :goto_0
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 136
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/navdy/service/library/file/FileTransferSessionManager;->endSessions(Z)I

    .line 137
    return-void
.end method

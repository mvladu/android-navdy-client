.class public Lcom/navdy/service/library/network/BTSocketAcceptor;
.super Ljava/lang/Object;
.source "BTSocketAcceptor.java"

# interfaces
.implements Lcom/navdy/service/library/network/SocketAcceptor;


# static fields
.field private static final needsPatch:Z

.field private static sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field btServerSocket:Landroid/bluetooth/BluetoothServerSocket;

.field private internalSocket:Lcom/navdy/service/library/network/BTSocketAdapter;

.field private final sdpName:Ljava/lang/String;

.field private final secure:Z

.field private final serviceUUID:Ljava/util/UUID;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 22
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/service/library/network/BTSocketAcceptor;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/service/library/network/BTSocketAcceptor;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 24
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x14

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/navdy/service/library/network/BTSocketAcceptor;->needsPatch:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/UUID;)V
    .locals 1
    .param p1, "sdpName"    # Ljava/lang/String;
    .param p2, "serviceUUID"    # Ljava/util/UUID;

    .prologue
    .line 41
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/service/library/network/BTSocketAcceptor;-><init>(Ljava/lang/String;Ljava/util/UUID;Z)V

    .line 42
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/UUID;Z)V
    .locals 0
    .param p1, "sdpName"    # Ljava/lang/String;
    .param p2, "serviceUUID"    # Ljava/util/UUID;
    .param p3, "secure"    # Z

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/navdy/service/library/network/BTSocketAcceptor;->sdpName:Ljava/lang/String;

    .line 36
    iput-object p2, p0, Lcom/navdy/service/library/network/BTSocketAcceptor;->serviceUUID:Ljava/util/UUID;

    .line 37
    iput-boolean p3, p0, Lcom/navdy/service/library/network/BTSocketAcceptor;->secure:Z

    .line 38
    return-void
.end method

.method private storeSocket()V
    .locals 5

    .prologue
    .line 86
    :try_start_0
    iget-object v3, p0, Lcom/navdy/service/library/network/BTSocketAcceptor;->btServerSocket:Landroid/bluetooth/BluetoothServerSocket;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "mSocket"

    invoke-virtual {v3, v4}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    .line 87
    .local v2, "mSocketField":Ljava/lang/reflect/Field;
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 88
    iget-object v3, p0, Lcom/navdy/service/library/network/BTSocketAcceptor;->btServerSocket:Landroid/bluetooth/BluetoothServerSocket;

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothSocket;

    .line 89
    .local v0, "bluetoothSocket":Landroid/bluetooth/BluetoothSocket;
    new-instance v3, Lcom/navdy/service/library/network/BTSocketAdapter;

    invoke-direct {v3, v0}, Lcom/navdy/service/library/network/BTSocketAdapter;-><init>(Landroid/bluetooth/BluetoothSocket;)V

    iput-object v3, p0, Lcom/navdy/service/library/network/BTSocketAcceptor;->internalSocket:Lcom/navdy/service/library/network/BTSocketAdapter;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 93
    .end local v0    # "bluetoothSocket":Landroid/bluetooth/BluetoothSocket;
    .end local v2    # "mSocketField":Ljava/lang/reflect/Field;
    :goto_0
    return-void

    .line 90
    :catch_0
    move-exception v1

    .line 91
    .local v1, "e":Ljava/lang/Exception;
    sget-object v3, Lcom/navdy/service/library/network/BTSocketAcceptor;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Failed to close server socket"

    invoke-virtual {v3, v4, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public accept()Lcom/navdy/service/library/network/SocketAdapter;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 46
    iget-object v1, p0, Lcom/navdy/service/library/network/BTSocketAcceptor;->btServerSocket:Landroid/bluetooth/BluetoothServerSocket;

    if-nez v1, :cond_1

    .line 47
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    .line 48
    .local v0, "adapter":Landroid/bluetooth/BluetoothAdapter;
    if-nez v0, :cond_0

    .line 49
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Bluetooth unavailable"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 51
    :cond_0
    iget-boolean v1, p0, Lcom/navdy/service/library/network/BTSocketAcceptor;->secure:Z

    if-eqz v1, :cond_2

    .line 52
    iget-object v1, p0, Lcom/navdy/service/library/network/BTSocketAcceptor;->sdpName:Ljava/lang/String;

    iget-object v2, p0, Lcom/navdy/service/library/network/BTSocketAcceptor;->serviceUUID:Ljava/util/UUID;

    invoke-virtual {v0, v1, v2}, Landroid/bluetooth/BluetoothAdapter;->listenUsingRfcommWithServiceRecord(Ljava/lang/String;Ljava/util/UUID;)Landroid/bluetooth/BluetoothServerSocket;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/service/library/network/BTSocketAcceptor;->btServerSocket:Landroid/bluetooth/BluetoothServerSocket;

    .line 56
    :goto_0
    sget-boolean v1, Lcom/navdy/service/library/network/BTSocketAcceptor;->needsPatch:Z

    if-eqz v1, :cond_1

    .line 57
    invoke-direct {p0}, Lcom/navdy/service/library/network/BTSocketAcceptor;->storeSocket()V

    .line 60
    .end local v0    # "adapter":Landroid/bluetooth/BluetoothAdapter;
    :cond_1
    new-instance v1, Lcom/navdy/service/library/network/BTSocketAdapter;

    iget-object v2, p0, Lcom/navdy/service/library/network/BTSocketAcceptor;->btServerSocket:Landroid/bluetooth/BluetoothServerSocket;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothServerSocket;->accept()Landroid/bluetooth/BluetoothSocket;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/navdy/service/library/network/BTSocketAdapter;-><init>(Landroid/bluetooth/BluetoothSocket;)V

    return-object v1

    .line 54
    .restart local v0    # "adapter":Landroid/bluetooth/BluetoothAdapter;
    :cond_2
    iget-object v1, p0, Lcom/navdy/service/library/network/BTSocketAcceptor;->sdpName:Ljava/lang/String;

    iget-object v2, p0, Lcom/navdy/service/library/network/BTSocketAcceptor;->serviceUUID:Ljava/util/UUID;

    invoke-virtual {v0, v1, v2}, Landroid/bluetooth/BluetoothAdapter;->listenUsingInsecureRfcommWithServiceRecord(Ljava/lang/String;Ljava/util/UUID;)Landroid/bluetooth/BluetoothServerSocket;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/service/library/network/BTSocketAcceptor;->btServerSocket:Landroid/bluetooth/BluetoothServerSocket;

    goto :goto_0
.end method

.method public close()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 65
    iget-object v0, p0, Lcom/navdy/service/library/network/BTSocketAcceptor;->btServerSocket:Landroid/bluetooth/BluetoothServerSocket;

    if-eqz v0, :cond_1

    .line 66
    iget-object v0, p0, Lcom/navdy/service/library/network/BTSocketAcceptor;->btServerSocket:Landroid/bluetooth/BluetoothServerSocket;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothServerSocket;->close()V

    .line 67
    iget-object v0, p0, Lcom/navdy/service/library/network/BTSocketAcceptor;->internalSocket:Lcom/navdy/service/library/network/BTSocketAdapter;

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p0, Lcom/navdy/service/library/network/BTSocketAcceptor;->internalSocket:Lcom/navdy/service/library/network/BTSocketAdapter;

    invoke-virtual {v0}, Lcom/navdy/service/library/network/BTSocketAdapter;->close()V

    .line 69
    iput-object v1, p0, Lcom/navdy/service/library/network/BTSocketAcceptor;->internalSocket:Lcom/navdy/service/library/network/BTSocketAdapter;

    .line 71
    :cond_0
    iput-object v1, p0, Lcom/navdy/service/library/network/BTSocketAcceptor;->btServerSocket:Landroid/bluetooth/BluetoothServerSocket;

    .line 73
    :cond_1
    return-void
.end method

.method public getRemoteConnectionInfo(Lcom/navdy/service/library/network/SocketAdapter;Lcom/navdy/service/library/device/connection/ConnectionType;)Lcom/navdy/service/library/device/connection/ConnectionInfo;
    .locals 3
    .param p1, "socket"    # Lcom/navdy/service/library/network/SocketAdapter;
    .param p2, "connectionType"    # Lcom/navdy/service/library/device/connection/ConnectionType;

    .prologue
    .line 77
    invoke-interface {p1}, Lcom/navdy/service/library/network/SocketAdapter;->getRemoteDevice()Lcom/navdy/service/library/device/NavdyDeviceId;

    move-result-object v0

    .line 78
    .local v0, "deviceId":Lcom/navdy/service/library/device/NavdyDeviceId;
    if-eqz v0, :cond_0

    .line 79
    new-instance v1, Lcom/navdy/service/library/device/connection/BTConnectionInfo;

    iget-object v2, p0, Lcom/navdy/service/library/network/BTSocketAcceptor;->serviceUUID:Ljava/util/UUID;

    invoke-direct {v1, v0, v2, p2}, Lcom/navdy/service/library/device/connection/BTConnectionInfo;-><init>(Lcom/navdy/service/library/device/NavdyDeviceId;Ljava/util/UUID;Lcom/navdy/service/library/device/connection/ConnectionType;)V

    .line 81
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

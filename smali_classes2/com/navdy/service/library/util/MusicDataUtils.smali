.class public final Lcom/navdy/service/library/util/MusicDataUtils;
.super Ljava/lang/Object;
.source "MusicDataUtils.java"


# static fields
.field public static final ALTERNATE_SEPARATOR:Ljava/lang/String; = "_"

.field private static final SEPARATOR:Ljava/lang/String; = " // "


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static photoIdentifierFromTrackInfo(Lcom/navdy/service/library/events/audio/MusicTrackInfo;)Ljava/lang/String;
    .locals 1
    .param p0, "trackInfo"    # Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    .prologue
    .line 25
    if-nez p0, :cond_0

    .line 26
    const/4 v0, 0x0

    .line 28
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->index:Ljava/lang/Long;

    if-nez v0, :cond_1

    invoke-static {p0}, Lcom/navdy/service/library/util/MusicDataUtils;->songIdentifierFromTrackInfo(Lcom/navdy/service/library/events/audio/MusicTrackInfo;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->index:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static songIdentifierFromArtworkResponse(Lcom/navdy/service/library/events/audio/MusicArtworkResponse;)Ljava/lang/String;
    .locals 3
    .param p0, "artworkResponse"    # Lcom/navdy/service/library/events/audio/MusicArtworkResponse;

    .prologue
    .line 51
    if-nez p0, :cond_0

    .line 52
    const/4 v1, 0x0

    .line 56
    :goto_0
    return-object v1

    .line 54
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->name:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " // "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->album:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " // "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->author:Ljava/lang/String;

    .line 55
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 56
    .local v0, "unformatted":Ljava/lang/String;
    const-string v1, "[^A-Za-z0-9_]"

    const-string v2, "_"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static songIdentifierFromTrackInfo(Lcom/navdy/service/library/events/audio/MusicTrackInfo;)Ljava/lang/String;
    .locals 1
    .param p0, "trackInfo"    # Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    .prologue
    .line 38
    const-string v0, " // "

    invoke-static {p0, v0}, Lcom/navdy/service/library/util/MusicDataUtils;->songIdentifierFromTrackInfo(Lcom/navdy/service/library/events/audio/MusicTrackInfo;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static songIdentifierFromTrackInfo(Lcom/navdy/service/library/events/audio/MusicTrackInfo;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "trackInfo"    # Lcom/navdy/service/library/events/audio/MusicTrackInfo;
    .param p1, "separator"    # Ljava/lang/String;

    .prologue
    .line 42
    if-nez p0, :cond_0

    .line 43
    const/4 v1, 0x0

    .line 47
    :goto_0
    return-object v1

    .line 45
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->name:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->album:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicTrackInfo;->author:Ljava/lang/String;

    .line 46
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 47
    .local v0, "unformatted":Ljava/lang/String;
    const-string v1, "[^A-Za-z0-9_]"

    const-string v2, "_"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.class final Lcom/navdy/service/library/util/IOUtils$1;
.super Ljava/lang/Object;
.source "IOUtils.java"

# interfaces
.implements Lcom/navdy/service/library/util/IOUtils$OnFileTraversal;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/service/library/util/IOUtils;->hashForPath(Ljava/lang/String;Z[Ljava/lang/String;)Ljava/lang/String;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$buffer:[B

.field final synthetic val$mDigest:Ljava/security/MessageDigest;


# direct methods
.method constructor <init>([BLjava/security/MessageDigest;)V
    .locals 0

    .prologue
    .line 484
    iput-object p1, p0, Lcom/navdy/service/library/util/IOUtils$1;->val$buffer:[B

    iput-object p2, p0, Lcom/navdy/service/library/util/IOUtils$1;->val$mDigest:Ljava/security/MessageDigest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFileTraversal(Ljava/io/File;)V
    .locals 14
    .param p1, "file"    # Ljava/io/File;

    .prologue
    .line 487
    const/4 v1, 0x0

    .line 489
    .local v1, "fileInputStream":Ljava/io/FileInputStream;
    :try_start_0
    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v4

    .line 490
    .local v4, "length":J
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 492
    .end local v1    # "fileInputStream":Ljava/io/FileInputStream;
    .local v2, "fileInputStream":Ljava/io/FileInputStream;
    const-wide/16 v6, 0x0

    .line 496
    .local v6, "totalRead":J
    :cond_0
    :try_start_1
    iget-object v8, p0, Lcom/navdy/service/library/util/IOUtils$1;->val$buffer:[B

    const/4 v9, 0x0

    sub-long v10, v4, v6

    const-wide/32 v12, 0x100000

    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v10

    long-to-int v10, v10

    invoke-virtual {v2, v8, v9, v10}, Ljava/io/FileInputStream;->read([BII)I

    move-result v0

    .line 497
    .local v0, "bytesRead":I
    iget-object v8, p0, Lcom/navdy/service/library/util/IOUtils$1;->val$mDigest:Ljava/security/MessageDigest;

    iget-object v9, p0, Lcom/navdy/service/library/util/IOUtils$1;->val$buffer:[B

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10, v0}, Ljava/security/MessageDigest;->update([BII)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 498
    int-to-long v8, v0

    add-long/2addr v6, v8

    .line 499
    cmp-long v8, v6, v4

    if-gez v8, :cond_1

    const/4 v8, -0x1

    if-ne v0, v8, :cond_0

    .line 503
    :cond_1
    invoke-static {v2}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    move-object v1, v2

    .line 505
    .end local v0    # "bytesRead":I
    .end local v2    # "fileInputStream":Ljava/io/FileInputStream;
    .end local v4    # "length":J
    .end local v6    # "totalRead":J
    .restart local v1    # "fileInputStream":Ljava/io/FileInputStream;
    :goto_0
    return-void

    .line 500
    :catch_0
    move-exception v3

    .line 501
    .local v3, "t":Ljava/lang/Throwable;
    :goto_1
    :try_start_2
    invoke-static {}, Lcom/navdy/service/library/util/IOUtils;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v8

    const-string v9, "Exception while calculating md5 checksum for a path "

    invoke-virtual {v8, v9, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 503
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_0

    .end local v3    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v8

    :goto_2
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v8

    .end local v1    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v2    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v4    # "length":J
    .restart local v6    # "totalRead":J
    :catchall_1
    move-exception v8

    move-object v1, v2

    .end local v2    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v1    # "fileInputStream":Ljava/io/FileInputStream;
    goto :goto_2

    .line 500
    .end local v1    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v2    # "fileInputStream":Ljava/io/FileInputStream;
    :catch_1
    move-exception v3

    move-object v1, v2

    .end local v2    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v1    # "fileInputStream":Ljava/io/FileInputStream;
    goto :goto_1
.end method

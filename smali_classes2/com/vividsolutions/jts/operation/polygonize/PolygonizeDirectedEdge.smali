.class Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;
.super Lcom/vividsolutions/jts/planargraph/DirectedEdge;
.source "PolygonizeDirectedEdge.java"


# instance fields
.field private edgeRing:Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;

.field private label:J

.field private next:Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/planargraph/Node;Lcom/vividsolutions/jts/planargraph/Node;Lcom/vividsolutions/jts/geom/Coordinate;Z)V
    .locals 2
    .param p1, "from"    # Lcom/vividsolutions/jts/planargraph/Node;
    .param p2, "to"    # Lcom/vividsolutions/jts/planargraph/Node;
    .param p3, "directionPt"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p4, "edgeDirection"    # Z

    .prologue
    const/4 v0, 0x0

    .line 69
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/vividsolutions/jts/planargraph/DirectedEdge;-><init>(Lcom/vividsolutions/jts/planargraph/Node;Lcom/vividsolutions/jts/planargraph/Node;Lcom/vividsolutions/jts/geom/Coordinate;Z)V

    .line 51
    iput-object v0, p0, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;->edgeRing:Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;

    .line 52
    iput-object v0, p0, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;->next:Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;

    .line 53
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;->label:J

    .line 70
    return-void
.end method


# virtual methods
.method public getLabel()J
    .locals 2

    .prologue
    .line 75
    iget-wide v0, p0, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;->label:J

    return-wide v0
.end method

.method public getNext()Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;->next:Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;

    return-object v0
.end method

.method public isInRing()Z
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;->edgeRing:Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setLabel(J)V
    .locals 1
    .param p1, "label"    # J

    .prologue
    .line 79
    iput-wide p1, p0, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;->label:J

    return-void
.end method

.method public setNext(Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;)V
    .locals 0
    .param p1, "next"    # Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;

    .prologue
    .line 89
    iput-object p1, p0, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;->next:Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;

    return-void
.end method

.method public setRing(Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;)V
    .locals 0
    .param p1, "edgeRing"    # Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;

    .prologue
    .line 102
    iput-object p1, p0, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;->edgeRing:Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;

    .line 103
    return-void
.end method

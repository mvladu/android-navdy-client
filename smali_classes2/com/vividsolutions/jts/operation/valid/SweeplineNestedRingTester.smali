.class public Lcom/vividsolutions/jts/operation/valid/SweeplineNestedRingTester;
.super Ljava/lang/Object;
.source "SweeplineNestedRingTester.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vividsolutions/jts/operation/valid/SweeplineNestedRingTester$OverlapAction;
    }
.end annotation


# instance fields
.field private graph:Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

.field private nestedPt:Lcom/vividsolutions/jts/geom/Coordinate;

.field private rings:Ljava/util/List;

.field private sweepLine:Lcom/vividsolutions/jts/index/sweepline/SweepLineIndex;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geomgraph/GeometryGraph;)V
    .locals 1
    .param p1, "graph"    # Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/valid/SweeplineNestedRingTester;->rings:Ljava/util/List;

    .line 57
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/valid/SweeplineNestedRingTester;->nestedPt:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 61
    iput-object p1, p0, Lcom/vividsolutions/jts/operation/valid/SweeplineNestedRingTester;->graph:Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    .line 62
    return-void
.end method

.method static synthetic access$000(Lcom/vividsolutions/jts/operation/valid/SweeplineNestedRingTester;Lcom/vividsolutions/jts/geom/LinearRing;Lcom/vividsolutions/jts/geom/LinearRing;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vividsolutions/jts/operation/valid/SweeplineNestedRingTester;
    .param p1, "x1"    # Lcom/vividsolutions/jts/geom/LinearRing;
    .param p2, "x2"    # Lcom/vividsolutions/jts/geom/LinearRing;

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Lcom/vividsolutions/jts/operation/valid/SweeplineNestedRingTester;->isInside(Lcom/vividsolutions/jts/geom/LinearRing;Lcom/vividsolutions/jts/geom/LinearRing;)Z

    move-result v0

    return v0
.end method

.method private buildIndex()V
    .locals 8

    .prologue
    .line 83
    new-instance v2, Lcom/vividsolutions/jts/index/sweepline/SweepLineIndex;

    invoke-direct {v2}, Lcom/vividsolutions/jts/index/sweepline/SweepLineIndex;-><init>()V

    iput-object v2, p0, Lcom/vividsolutions/jts/operation/valid/SweeplineNestedRingTester;->sweepLine:Lcom/vividsolutions/jts/index/sweepline/SweepLineIndex;

    .line 85
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/valid/SweeplineNestedRingTester;->rings:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v7, v2, :cond_0

    .line 86
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/valid/SweeplineNestedRingTester;->rings:Ljava/util/List;

    invoke-interface {v2, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vividsolutions/jts/geom/LinearRing;

    .line 87
    .local v6, "ring":Lcom/vividsolutions/jts/geom/LinearRing;
    invoke-virtual {v6}, Lcom/vividsolutions/jts/geom/LinearRing;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v0

    .line 88
    .local v0, "env":Lcom/vividsolutions/jts/geom/Envelope;
    new-instance v1, Lcom/vividsolutions/jts/index/sweepline/SweepLineInterval;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/Envelope;->getMinX()D

    move-result-wide v2

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/Envelope;->getMaxX()D

    move-result-wide v4

    invoke-direct/range {v1 .. v6}, Lcom/vividsolutions/jts/index/sweepline/SweepLineInterval;-><init>(DDLjava/lang/Object;)V

    .line 89
    .local v1, "sweepInt":Lcom/vividsolutions/jts/index/sweepline/SweepLineInterval;
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/valid/SweeplineNestedRingTester;->sweepLine:Lcom/vividsolutions/jts/index/sweepline/SweepLineIndex;

    invoke-virtual {v2, v1}, Lcom/vividsolutions/jts/index/sweepline/SweepLineIndex;->add(Lcom/vividsolutions/jts/index/sweepline/SweepLineInterval;)V

    .line 85
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 91
    .end local v0    # "env":Lcom/vividsolutions/jts/geom/Envelope;
    .end local v1    # "sweepInt":Lcom/vividsolutions/jts/index/sweepline/SweepLineInterval;
    .end local v6    # "ring":Lcom/vividsolutions/jts/geom/LinearRing;
    :cond_0
    return-void
.end method

.method private isInside(Lcom/vividsolutions/jts/geom/LinearRing;Lcom/vividsolutions/jts/geom/LinearRing;)Z
    .locals 8
    .param p1, "innerRing"    # Lcom/vividsolutions/jts/geom/LinearRing;
    .param p2, "searchRing"    # Lcom/vividsolutions/jts/geom/LinearRing;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 95
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/LinearRing;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    .line 96
    .local v1, "innerRingPts":[Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-virtual {p2}, Lcom/vividsolutions/jts/geom/LinearRing;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v3

    .line 98
    .local v3, "searchRingPts":[Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/LinearRing;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v4

    invoke-virtual {p2}, Lcom/vividsolutions/jts/geom/LinearRing;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/vividsolutions/jts/geom/Envelope;->intersects(Lcom/vividsolutions/jts/geom/Envelope;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 109
    :cond_0
    :goto_0
    return v6

    .line 101
    :cond_1
    iget-object v4, p0, Lcom/vividsolutions/jts/operation/valid/SweeplineNestedRingTester;->graph:Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    invoke-static {v1, p2, v4}, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->findPtNotNode([Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/LinearRing;Lcom/vividsolutions/jts/geomgraph/GeometryGraph;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    .line 102
    .local v0, "innerRingPt":Lcom/vividsolutions/jts/geom/Coordinate;
    if-eqz v0, :cond_2

    move v4, v5

    :goto_1
    const-string v7, "Unable to find a ring point not a node of the search ring"

    invoke-static {v4, v7}, Lcom/vividsolutions/jts/util/Assert;->isTrue(ZLjava/lang/String;)V

    .line 104
    invoke-static {v0, v3}, Lcom/vividsolutions/jts/algorithm/CGAlgorithms;->isPointInRing(Lcom/vividsolutions/jts/geom/Coordinate;[Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v2

    .line 105
    .local v2, "isInside":Z
    if-eqz v2, :cond_0

    .line 106
    iput-object v0, p0, Lcom/vividsolutions/jts/operation/valid/SweeplineNestedRingTester;->nestedPt:Lcom/vividsolutions/jts/geom/Coordinate;

    move v6, v5

    .line 107
    goto :goto_0

    .end local v2    # "isInside":Z
    :cond_2
    move v4, v6

    .line 102
    goto :goto_1
.end method


# virtual methods
.method public add(Lcom/vividsolutions/jts/geom/LinearRing;)V
    .locals 1
    .param p1, "ring"    # Lcom/vividsolutions/jts/geom/LinearRing;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/valid/SweeplineNestedRingTester;->rings:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 69
    return-void
.end method

.method public getNestedPoint()Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/valid/SweeplineNestedRingTester;->nestedPt:Lcom/vividsolutions/jts/geom/Coordinate;

    return-object v0
.end method

.method public isNonNested()Z
    .locals 2

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/vividsolutions/jts/operation/valid/SweeplineNestedRingTester;->buildIndex()V

    .line 75
    new-instance v0, Lcom/vividsolutions/jts/operation/valid/SweeplineNestedRingTester$OverlapAction;

    invoke-direct {v0, p0}, Lcom/vividsolutions/jts/operation/valid/SweeplineNestedRingTester$OverlapAction;-><init>(Lcom/vividsolutions/jts/operation/valid/SweeplineNestedRingTester;)V

    .line 77
    .local v0, "action":Lcom/vividsolutions/jts/operation/valid/SweeplineNestedRingTester$OverlapAction;
    iget-object v1, p0, Lcom/vividsolutions/jts/operation/valid/SweeplineNestedRingTester;->sweepLine:Lcom/vividsolutions/jts/index/sweepline/SweepLineIndex;

    invoke-virtual {v1, v0}, Lcom/vividsolutions/jts/index/sweepline/SweepLineIndex;->computeOverlaps(Lcom/vividsolutions/jts/index/sweepline/SweepLineOverlapAction;)V

    .line 78
    iget-boolean v1, v0, Lcom/vividsolutions/jts/operation/valid/SweeplineNestedRingTester$OverlapAction;->isNonNested:Z

    return v1
.end method

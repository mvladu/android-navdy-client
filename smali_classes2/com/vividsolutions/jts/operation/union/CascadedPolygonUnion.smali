.class public Lcom/vividsolutions/jts/operation/union/CascadedPolygonUnion;
.super Ljava/lang/Object;
.source "CascadedPolygonUnion.java"


# static fields
.field private static final STRTREE_NODE_CAPACITY:I = 0x4


# instance fields
.field private geomFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;

.field private inputPolys:Ljava/util/Collection;


# direct methods
.method public constructor <init>(Ljava/util/Collection;)V
    .locals 1
    .param p1, "polys"    # Ljava/util/Collection;

    .prologue
    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/union/CascadedPolygonUnion;->geomFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    .line 94
    iput-object p1, p0, Lcom/vividsolutions/jts/operation/union/CascadedPolygonUnion;->inputPolys:Ljava/util/Collection;

    .line 95
    return-void
.end method

.method private binaryUnion(Ljava/util/List;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 2
    .param p1, "geoms"    # Ljava/util/List;

    .prologue
    .line 200
    const/4 v0, 0x0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {p0, p1, v0, v1}, Lcom/vividsolutions/jts/operation/union/CascadedPolygonUnion;->binaryUnion(Ljava/util/List;II)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    return-object v0
.end method

.method private binaryUnion(Ljava/util/List;II)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 5
    .param p1, "geoms"    # Ljava/util/List;
    .param p2, "start"    # I
    .param p3, "end"    # I

    .prologue
    .line 214
    sub-int v3, p3, p2

    const/4 v4, 0x1

    if-gt v3, v4, :cond_0

    .line 215
    invoke-static {p1, p2}, Lcom/vividsolutions/jts/operation/union/CascadedPolygonUnion;->getGeometry(Ljava/util/List;I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    .line 216
    .local v0, "g0":Lcom/vividsolutions/jts/geom/Geometry;
    const/4 v3, 0x0

    invoke-direct {p0, v0, v3}, Lcom/vividsolutions/jts/operation/union/CascadedPolygonUnion;->unionSafe(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v3

    .line 226
    .end local v0    # "g0":Lcom/vividsolutions/jts/geom/Geometry;
    :goto_0
    return-object v3

    .line 218
    :cond_0
    sub-int v3, p3, p2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 219
    invoke-static {p1, p2}, Lcom/vividsolutions/jts/operation/union/CascadedPolygonUnion;->getGeometry(Ljava/util/List;I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v3

    add-int/lit8 v4, p2, 0x1

    invoke-static {p1, v4}, Lcom/vividsolutions/jts/operation/union/CascadedPolygonUnion;->getGeometry(Ljava/util/List;I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v4

    invoke-direct {p0, v3, v4}, Lcom/vividsolutions/jts/operation/union/CascadedPolygonUnion;->unionSafe(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v3

    goto :goto_0

    .line 223
    :cond_1
    add-int v3, p3, p2

    div-int/lit8 v2, v3, 0x2

    .line 224
    .local v2, "mid":I
    invoke-direct {p0, p1, p2, v2}, Lcom/vividsolutions/jts/operation/union/CascadedPolygonUnion;->binaryUnion(Ljava/util/List;II)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    .line 225
    .restart local v0    # "g0":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, p1, v2, p3}, Lcom/vividsolutions/jts/operation/union/CascadedPolygonUnion;->binaryUnion(Ljava/util/List;II)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    .line 226
    .local v1, "g1":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, v0, v1}, Lcom/vividsolutions/jts/operation/union/CascadedPolygonUnion;->unionSafe(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v3

    goto :goto_0
.end method

.method private bufferUnion(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 6
    .param p1, "g0"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "g1"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 185
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v0

    .line 186
    .local v0, "factory":Lcom/vividsolutions/jts/geom/GeometryFactory;
    const/4 v3, 0x2

    new-array v3, v3, [Lcom/vividsolutions/jts/geom/Geometry;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    aput-object p2, v3, v4

    invoke-virtual {v0, v3}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createGeometryCollection([Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/GeometryCollection;

    move-result-object v1

    .line 187
    .local v1, "gColl":Lcom/vividsolutions/jts/geom/Geometry;
    const-wide/16 v4, 0x0

    invoke-virtual {v1, v4, v5}, Lcom/vividsolutions/jts/geom/Geometry;->buffer(D)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v2

    .line 188
    .local v2, "unionAll":Lcom/vividsolutions/jts/geom/Geometry;
    return-object v2
.end method

.method private bufferUnion(Ljava/util/List;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 6
    .param p1, "geoms"    # Ljava/util/List;

    .prologue
    .line 177
    const/4 v3, 0x0

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v3}, Lcom/vividsolutions/jts/geom/Geometry;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v0

    .line 178
    .local v0, "factory":Lcom/vividsolutions/jts/geom/GeometryFactory;
    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/geom/GeometryFactory;->buildGeometry(Ljava/util/Collection;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    .line 179
    .local v1, "gColl":Lcom/vividsolutions/jts/geom/Geometry;
    const-wide/16 v4, 0x0

    invoke-virtual {v1, v4, v5}, Lcom/vividsolutions/jts/geom/Geometry;->buffer(D)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v2

    .line 180
    .local v2, "unionAll":Lcom/vividsolutions/jts/geom/Geometry;
    return-object v2
.end method

.method private extractByEnvelope(Lcom/vividsolutions/jts/geom/Envelope;Lcom/vividsolutions/jts/geom/Geometry;Ljava/util/List;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 4
    .param p1, "env"    # Lcom/vividsolutions/jts/geom/Envelope;
    .param p2, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p3, "disjointGeoms"    # Ljava/util/List;

    .prologue
    .line 354
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 355
    .local v2, "intersectingGeoms":Ljava/util/List;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p2}, Lcom/vividsolutions/jts/geom/Geometry;->getNumGeometries()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 356
    invoke-virtual {p2, v1}, Lcom/vividsolutions/jts/geom/Geometry;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    .line 357
    .local v0, "elem":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/Geometry;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/vividsolutions/jts/geom/Envelope;->intersects(Lcom/vividsolutions/jts/geom/Envelope;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 358
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 355
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 360
    :cond_0
    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 362
    .end local v0    # "elem":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_1
    iget-object v3, p0, Lcom/vividsolutions/jts/operation/union/CascadedPolygonUnion;->geomFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-virtual {v3, v2}, Lcom/vividsolutions/jts/geom/GeometryFactory;->buildGeometry(Ljava/util/Collection;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v3

    return-object v3
.end method

.method private static getGeometry(Ljava/util/List;I)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 1
    .param p0, "list"    # Ljava/util/List;
    .param p1, "index"    # I

    .prologue
    .line 241
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_0

    const/4 v0, 0x0

    .line 242
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geom/Geometry;

    goto :goto_0
.end method

.method private reduceToGeometries(Ljava/util/List;)Ljava/util/List;
    .locals 5
    .param p1, "geomTree"    # Ljava/util/List;

    .prologue
    .line 254
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 255
    .local v1, "geoms":Ljava/util/List;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 256
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 257
    .local v3, "o":Ljava/lang/Object;
    const/4 v0, 0x0

    .line 258
    .local v0, "geom":Lcom/vividsolutions/jts/geom/Geometry;
    instance-of v4, v3, Ljava/util/List;

    if-eqz v4, :cond_1

    .line 259
    check-cast v3, Ljava/util/List;

    .end local v3    # "o":Ljava/lang/Object;
    invoke-direct {p0, v3}, Lcom/vividsolutions/jts/operation/union/CascadedPolygonUnion;->unionTree(Ljava/util/List;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    .line 264
    :cond_0
    :goto_1
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 261
    .restart local v3    # "o":Ljava/lang/Object;
    :cond_1
    instance-of v4, v3, Lcom/vividsolutions/jts/geom/Geometry;

    if-eqz v4, :cond_0

    move-object v0, v3

    .line 262
    check-cast v0, Lcom/vividsolutions/jts/geom/Geometry;

    goto :goto_1

    .line 266
    .end local v0    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    .end local v3    # "o":Ljava/lang/Object;
    :cond_2
    return-object v1
.end method

.method private repeatedUnion(Ljava/util/List;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 4
    .param p1, "geoms"    # Ljava/util/List;

    .prologue
    .line 164
    const/4 v2, 0x0

    .line 165
    .local v2, "union":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 166
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geom/Geometry;

    .line 167
    .local v0, "g":Lcom/vividsolutions/jts/geom/Geometry;
    if-nez v2, :cond_0

    .line 168
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/Geometry;->clone()Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "union":Lcom/vividsolutions/jts/geom/Geometry;
    check-cast v2, Lcom/vividsolutions/jts/geom/Geometry;

    .restart local v2    # "union":Lcom/vividsolutions/jts/geom/Geometry;
    goto :goto_0

    .line 170
    :cond_0
    invoke-virtual {v2, v0}, Lcom/vividsolutions/jts/geom/Geometry;->union(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v2

    goto :goto_0

    .line 172
    .end local v0    # "g":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_1
    return-object v2
.end method

.method private static restrictToPolygons(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 3
    .param p0, "g"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 402
    instance-of v1, p0, Lcom/vividsolutions/jts/geom/Polygonal;

    if-eqz v1, :cond_0

    .line 408
    .end local p0    # "g":Lcom/vividsolutions/jts/geom/Geometry;
    .local v0, "polygons":Ljava/util/List;
    :goto_0
    return-object p0

    .line 405
    .end local v0    # "polygons":Ljava/util/List;
    .restart local p0    # "g":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_0
    invoke-static {p0}, Lcom/vividsolutions/jts/geom/util/PolygonExtracter;->getPolygons(Lcom/vividsolutions/jts/geom/Geometry;)Ljava/util/List;

    move-result-object v0

    .line 406
    .restart local v0    # "polygons":Ljava/util/List;
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 407
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/geom/Polygon;

    move-object p0, v1

    goto :goto_0

    .line 408
    :cond_1
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Geometry;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v1

    invoke-static {v0}, Lcom/vividsolutions/jts/geom/GeometryFactory;->toPolygonArray(Ljava/util/Collection;)[Lcom/vividsolutions/jts/geom/Polygon;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createMultiPolygon([Lcom/vividsolutions/jts/geom/Polygon;)Lcom/vividsolutions/jts/geom/MultiPolygon;

    move-result-object p0

    goto :goto_0
.end method

.method public static union(Ljava/util/Collection;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 2
    .param p0, "polys"    # Ljava/util/Collection;

    .prologue
    .line 79
    new-instance v0, Lcom/vividsolutions/jts/operation/union/CascadedPolygonUnion;

    invoke-direct {v0, p0}, Lcom/vividsolutions/jts/operation/union/CascadedPolygonUnion;-><init>(Ljava/util/Collection;)V

    .line 80
    .local v0, "op":Lcom/vividsolutions/jts/operation/union/CascadedPolygonUnion;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/union/CascadedPolygonUnion;->union()Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    return-object v1
.end method

.method private unionActual(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 1
    .param p1, "g0"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "g1"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 384
    invoke-virtual {p1, p2}, Lcom/vividsolutions/jts/geom/Geometry;->union(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    invoke-static {v0}, Lcom/vividsolutions/jts/operation/union/CascadedPolygonUnion;->restrictToPolygons(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    return-object v0
.end method

.method private unionOptimized(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 6
    .param p1, "g0"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "g1"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    const/4 v5, 0x1

    .line 293
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v2

    .line 294
    .local v2, "g0Env":Lcom/vividsolutions/jts/geom/Envelope;
    invoke-virtual {p2}, Lcom/vividsolutions/jts/geom/Geometry;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v3

    .line 296
    .local v3, "g1Env":Lcom/vividsolutions/jts/geom/Envelope;
    invoke-virtual {v2, v3}, Lcom/vividsolutions/jts/geom/Envelope;->intersects(Lcom/vividsolutions/jts/geom/Envelope;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 298
    invoke-static {p1, p2}, Lcom/vividsolutions/jts/geom/util/GeometryCombiner;->combine(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    .line 313
    :goto_0
    return-object v0

    .line 306
    :cond_0
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getNumGeometries()I

    move-result v4

    if-gt v4, v5, :cond_1

    invoke-virtual {p2}, Lcom/vividsolutions/jts/geom/Geometry;->getNumGeometries()I

    move-result v4

    if-gt v4, v5, :cond_1

    .line 307
    invoke-direct {p0, p1, p2}, Lcom/vividsolutions/jts/operation/union/CascadedPolygonUnion;->unionActual(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    goto :goto_0

    .line 312
    :cond_1
    invoke-virtual {v2, v3}, Lcom/vividsolutions/jts/geom/Envelope;->intersection(Lcom/vividsolutions/jts/geom/Envelope;)Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v1

    .line 313
    .local v1, "commonEnv":Lcom/vividsolutions/jts/geom/Envelope;
    invoke-direct {p0, p1, p2, v1}, Lcom/vividsolutions/jts/operation/union/CascadedPolygonUnion;->unionUsingEnvelopeIntersection(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Envelope;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    goto :goto_0
.end method

.method private unionSafe(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 1
    .param p1, "g0"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "g1"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 280
    if-nez p1, :cond_0

    if-nez p2, :cond_0

    .line 281
    const/4 v0, 0x0

    .line 288
    :goto_0
    return-object v0

    .line 283
    :cond_0
    if-nez p1, :cond_1

    .line 284
    invoke-virtual {p2}, Lcom/vividsolutions/jts/geom/Geometry;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geom/Geometry;

    goto :goto_0

    .line 285
    :cond_1
    if-nez p2, :cond_2

    .line 286
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geom/Geometry;

    goto :goto_0

    .line 288
    :cond_2
    invoke-direct {p0, p1, p2}, Lcom/vividsolutions/jts/operation/union/CascadedPolygonUnion;->unionOptimized(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    goto :goto_0
.end method

.method private unionTree(Ljava/util/List;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 2
    .param p1, "geomTree"    # Ljava/util/List;

    .prologue
    .line 144
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/operation/union/CascadedPolygonUnion;->reduceToGeometries(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 146
    .local v0, "geoms":Ljava/util/List;
    invoke-direct {p0, v0}, Lcom/vividsolutions/jts/operation/union/CascadedPolygonUnion;->binaryUnion(Ljava/util/List;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    .line 151
    .local v1, "union":Lcom/vividsolutions/jts/geom/Geometry;
    return-object v1
.end method

.method private unionUsingEnvelopeIntersection(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Envelope;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 5
    .param p1, "g0"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "g1"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p3, "common"    # Lcom/vividsolutions/jts/geom/Envelope;

    .prologue
    .line 337
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 339
    .local v0, "disjointPolys":Ljava/util/List;
    invoke-direct {p0, p3, p1, v0}, Lcom/vividsolutions/jts/operation/union/CascadedPolygonUnion;->extractByEnvelope(Lcom/vividsolutions/jts/geom/Envelope;Lcom/vividsolutions/jts/geom/Geometry;Ljava/util/List;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    .line 340
    .local v1, "g0Int":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, p3, p2, v0}, Lcom/vividsolutions/jts/operation/union/CascadedPolygonUnion;->extractByEnvelope(Lcom/vividsolutions/jts/geom/Envelope;Lcom/vividsolutions/jts/geom/Geometry;Ljava/util/List;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v2

    .line 343
    .local v2, "g1Int":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, v1, v2}, Lcom/vividsolutions/jts/operation/union/CascadedPolygonUnion;->unionActual(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v4

    .line 345
    .local v4, "union":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 346
    invoke-static {v0}, Lcom/vividsolutions/jts/geom/util/GeometryCombiner;->combine(Ljava/util/Collection;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v3

    .line 348
    .local v3, "overallUnion":Lcom/vividsolutions/jts/geom/Geometry;
    return-object v3
.end method


# virtual methods
.method public union()Lcom/vividsolutions/jts/geom/Geometry;
    .locals 6

    .prologue
    .line 114
    iget-object v5, p0, Lcom/vividsolutions/jts/operation/union/CascadedPolygonUnion;->inputPolys:Ljava/util/Collection;

    invoke-interface {v5}, Ljava/util/Collection;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 115
    const/4 v4, 0x0

    .line 135
    :goto_0
    return-object v4

    .line 116
    :cond_0
    iget-object v5, p0, Lcom/vividsolutions/jts/operation/union/CascadedPolygonUnion;->inputPolys:Ljava/util/Collection;

    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v5}, Lcom/vividsolutions/jts/geom/Geometry;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v5

    iput-object v5, p0, Lcom/vividsolutions/jts/operation/union/CascadedPolygonUnion;->geomFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    .line 125
    new-instance v1, Lcom/vividsolutions/jts/index/strtree/STRtree;

    const/4 v5, 0x4

    invoke-direct {v1, v5}, Lcom/vividsolutions/jts/index/strtree/STRtree;-><init>(I)V

    .line 126
    .local v1, "index":Lcom/vividsolutions/jts/index/strtree/STRtree;
    iget-object v5, p0, Lcom/vividsolutions/jts/operation/union/CascadedPolygonUnion;->inputPolys:Ljava/util/Collection;

    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 127
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vividsolutions/jts/geom/Geometry;

    .line 128
    .local v2, "item":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/Geometry;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v5

    invoke-virtual {v1, v5, v2}, Lcom/vividsolutions/jts/index/strtree/STRtree;->insert(Lcom/vividsolutions/jts/geom/Envelope;Ljava/lang/Object;)V

    goto :goto_1

    .line 130
    .end local v2    # "item":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_1
    invoke-virtual {v1}, Lcom/vividsolutions/jts/index/strtree/STRtree;->itemsTree()Ljava/util/List;

    move-result-object v3

    .line 134
    .local v3, "itemTree":Ljava/util/List;
    invoke-direct {p0, v3}, Lcom/vividsolutions/jts/operation/union/CascadedPolygonUnion;->unionTree(Ljava/util/List;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v4

    .line 135
    .local v4, "unionAll":Lcom/vividsolutions/jts/geom/Geometry;
    goto :goto_0
.end method

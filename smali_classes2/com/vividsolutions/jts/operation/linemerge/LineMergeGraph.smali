.class public Lcom/vividsolutions/jts/operation/linemerge/LineMergeGraph;
.super Lcom/vividsolutions/jts/planargraph/PlanarGraph;
.source "LineMergeGraph.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/vividsolutions/jts/planargraph/PlanarGraph;-><init>()V

    return-void
.end method

.method private getNode(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/planargraph/Node;
    .locals 1
    .param p1, "coordinate"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 83
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/operation/linemerge/LineMergeGraph;->findNode(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/planargraph/Node;

    move-result-object v0

    .line 84
    .local v0, "node":Lcom/vividsolutions/jts/planargraph/Node;
    if-nez v0, :cond_0

    .line 85
    new-instance v0, Lcom/vividsolutions/jts/planargraph/Node;

    .end local v0    # "node":Lcom/vividsolutions/jts/planargraph/Node;
    invoke-direct {v0, p1}, Lcom/vividsolutions/jts/planargraph/Node;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 86
    .restart local v0    # "node":Lcom/vividsolutions/jts/planargraph/Node;
    invoke-virtual {p0, v0}, Lcom/vividsolutions/jts/operation/linemerge/LineMergeGraph;->add(Lcom/vividsolutions/jts/planargraph/Node;)V

    .line 89
    :cond_0
    return-object v0
.end method


# virtual methods
.method public addEdge(Lcom/vividsolutions/jts/geom/LineString;)V
    .locals 11
    .param p1, "lineString"    # Lcom/vividsolutions/jts/geom/LineString;

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 62
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/LineString;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 80
    :cond_0
    :goto_0
    return-void

    .line 64
    :cond_1
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v8

    invoke-static {v8}, Lcom/vividsolutions/jts/geom/CoordinateArrays;->removeRepeatedPoints([Lcom/vividsolutions/jts/geom/Coordinate;)[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    .line 67
    .local v0, "coordinates":[Lcom/vividsolutions/jts/geom/Coordinate;
    array-length v8, v0

    if-le v8, v9, :cond_0

    .line 69
    aget-object v6, v0, v10

    .line 70
    .local v6, "startCoordinate":Lcom/vividsolutions/jts/geom/Coordinate;
    array-length v8, v0

    add-int/lit8 v8, v8, -0x1

    aget-object v4, v0, v8

    .line 71
    .local v4, "endCoordinate":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-direct {p0, v6}, Lcom/vividsolutions/jts/operation/linemerge/LineMergeGraph;->getNode(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/planargraph/Node;

    move-result-object v7

    .line 72
    .local v7, "startNode":Lcom/vividsolutions/jts/planargraph/Node;
    invoke-direct {p0, v4}, Lcom/vividsolutions/jts/operation/linemerge/LineMergeGraph;->getNode(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/planargraph/Node;

    move-result-object v5

    .line 73
    .local v5, "endNode":Lcom/vividsolutions/jts/planargraph/Node;
    new-instance v1, Lcom/vividsolutions/jts/operation/linemerge/LineMergeDirectedEdge;

    aget-object v8, v0, v9

    invoke-direct {v1, v7, v5, v8, v9}, Lcom/vividsolutions/jts/operation/linemerge/LineMergeDirectedEdge;-><init>(Lcom/vividsolutions/jts/planargraph/Node;Lcom/vividsolutions/jts/planargraph/Node;Lcom/vividsolutions/jts/geom/Coordinate;Z)V

    .line 75
    .local v1, "directedEdge0":Lcom/vividsolutions/jts/planargraph/DirectedEdge;
    new-instance v2, Lcom/vividsolutions/jts/operation/linemerge/LineMergeDirectedEdge;

    array-length v8, v0

    add-int/lit8 v8, v8, -0x2

    aget-object v8, v0, v8

    invoke-direct {v2, v5, v7, v8, v10}, Lcom/vividsolutions/jts/operation/linemerge/LineMergeDirectedEdge;-><init>(Lcom/vividsolutions/jts/planargraph/Node;Lcom/vividsolutions/jts/planargraph/Node;Lcom/vividsolutions/jts/geom/Coordinate;Z)V

    .line 77
    .local v2, "directedEdge1":Lcom/vividsolutions/jts/planargraph/DirectedEdge;
    new-instance v3, Lcom/vividsolutions/jts/operation/linemerge/LineMergeEdge;

    invoke-direct {v3, p1}, Lcom/vividsolutions/jts/operation/linemerge/LineMergeEdge;-><init>(Lcom/vividsolutions/jts/geom/LineString;)V

    .line 78
    .local v3, "edge":Lcom/vividsolutions/jts/planargraph/Edge;
    invoke-virtual {v3, v1, v2}, Lcom/vividsolutions/jts/planargraph/Edge;->setDirectedEdges(Lcom/vividsolutions/jts/planargraph/DirectedEdge;Lcom/vividsolutions/jts/planargraph/DirectedEdge;)V

    .line 79
    invoke-virtual {p0, v3}, Lcom/vividsolutions/jts/operation/linemerge/LineMergeGraph;->add(Lcom/vividsolutions/jts/planargraph/Edge;)V

    goto :goto_0
.end method

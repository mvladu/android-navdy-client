.class Lcom/vividsolutions/jts/operation/overlay/snap/SnapTransformer;
.super Lcom/vividsolutions/jts/geom/util/GeometryTransformer;
.source "GeometrySnapper.java"


# instance fields
.field private isSelfSnap:Z

.field private snapPts:[Lcom/vividsolutions/jts/geom/Coordinate;

.field private snapTolerance:D


# direct methods
.method constructor <init>(D[Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 1
    .param p1, "snapTolerance"    # D
    .param p3, "snapPts"    # [Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 231
    invoke-direct {p0}, Lcom/vividsolutions/jts/geom/util/GeometryTransformer;-><init>()V

    .line 228
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vividsolutions/jts/operation/overlay/snap/SnapTransformer;->isSelfSnap:Z

    .line 232
    iput-wide p1, p0, Lcom/vividsolutions/jts/operation/overlay/snap/SnapTransformer;->snapTolerance:D

    .line 233
    iput-object p3, p0, Lcom/vividsolutions/jts/operation/overlay/snap/SnapTransformer;->snapPts:[Lcom/vividsolutions/jts/geom/Coordinate;

    .line 234
    return-void
.end method

.method constructor <init>(D[Lcom/vividsolutions/jts/geom/Coordinate;Z)V
    .locals 1
    .param p1, "snapTolerance"    # D
    .param p3, "snapPts"    # [Lcom/vividsolutions/jts/geom/Coordinate;
    .param p4, "isSelfSnap"    # Z

    .prologue
    .line 237
    invoke-direct {p0}, Lcom/vividsolutions/jts/geom/util/GeometryTransformer;-><init>()V

    .line 228
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vividsolutions/jts/operation/overlay/snap/SnapTransformer;->isSelfSnap:Z

    .line 238
    iput-wide p1, p0, Lcom/vividsolutions/jts/operation/overlay/snap/SnapTransformer;->snapTolerance:D

    .line 239
    iput-object p3, p0, Lcom/vividsolutions/jts/operation/overlay/snap/SnapTransformer;->snapPts:[Lcom/vividsolutions/jts/geom/Coordinate;

    .line 240
    iput-boolean p4, p0, Lcom/vividsolutions/jts/operation/overlay/snap/SnapTransformer;->isSelfSnap:Z

    .line 241
    return-void
.end method

.method private snapLine([Lcom/vividsolutions/jts/geom/Coordinate;[Lcom/vividsolutions/jts/geom/Coordinate;)[Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 4
    .param p1, "srcPts"    # [Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "snapPts"    # [Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 252
    new-instance v0, Lcom/vividsolutions/jts/operation/overlay/snap/LineStringSnapper;

    iget-wide v2, p0, Lcom/vividsolutions/jts/operation/overlay/snap/SnapTransformer;->snapTolerance:D

    invoke-direct {v0, p1, v2, v3}, Lcom/vividsolutions/jts/operation/overlay/snap/LineStringSnapper;-><init>([Lcom/vividsolutions/jts/geom/Coordinate;D)V

    .line 253
    .local v0, "snapper":Lcom/vividsolutions/jts/operation/overlay/snap/LineStringSnapper;
    iget-boolean v1, p0, Lcom/vividsolutions/jts/operation/overlay/snap/SnapTransformer;->isSelfSnap:Z

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/operation/overlay/snap/LineStringSnapper;->setAllowSnappingToSourceVertices(Z)V

    .line 254
    invoke-virtual {v0, p2}, Lcom/vividsolutions/jts/operation/overlay/snap/LineStringSnapper;->snapTo([Lcom/vividsolutions/jts/geom/Coordinate;)[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method protected transformCoordinates(Lcom/vividsolutions/jts/geom/CoordinateSequence;Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/CoordinateSequence;
    .locals 3
    .param p1, "coords"    # Lcom/vividsolutions/jts/geom/CoordinateSequence;
    .param p2, "parent"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 245
    invoke-interface {p1}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->toCoordinateArray()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    .line 246
    .local v1, "srcPts":[Lcom/vividsolutions/jts/geom/Coordinate;
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/overlay/snap/SnapTransformer;->snapPts:[Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {p0, v1, v2}, Lcom/vividsolutions/jts/operation/overlay/snap/SnapTransformer;->snapLine([Lcom/vividsolutions/jts/geom/Coordinate;[Lcom/vividsolutions/jts/geom/Coordinate;)[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    .line 247
    .local v0, "newPts":[Lcom/vividsolutions/jts/geom/Coordinate;
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/overlay/snap/SnapTransformer;->factory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/GeometryFactory;->getCoordinateSequenceFactory()Lcom/vividsolutions/jts/geom/CoordinateSequenceFactory;

    move-result-object v2

    invoke-interface {v2, v0}, Lcom/vividsolutions/jts/geom/CoordinateSequenceFactory;->create([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/CoordinateSequence;

    move-result-object v2

    return-object v2
.end method

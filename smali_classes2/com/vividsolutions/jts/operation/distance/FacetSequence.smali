.class public Lcom/vividsolutions/jts/operation/distance/FacetSequence;
.super Ljava/lang/Object;
.source "FacetSequence.java"


# instance fields
.field private end:I

.field private p0:Lcom/vividsolutions/jts/geom/Coordinate;

.field private p1:Lcom/vividsolutions/jts/geom/Coordinate;

.field private pt:Lcom/vividsolutions/jts/geom/Coordinate;

.field private pts:Lcom/vividsolutions/jts/geom/CoordinateSequence;

.field private q0:Lcom/vividsolutions/jts/geom/Coordinate;

.field private q1:Lcom/vividsolutions/jts/geom/Coordinate;

.field private seqPt:Lcom/vividsolutions/jts/geom/Coordinate;

.field private start:I


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/CoordinateSequence;I)V
    .locals 1
    .param p1, "pts"    # Lcom/vividsolutions/jts/geom/CoordinateSequence;
    .param p2, "start"    # I

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    new-instance v0, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->pt:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 55
    new-instance v0, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->seqPt:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 132
    new-instance v0, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 133
    new-instance v0, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 134
    new-instance v0, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->q0:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 135
    new-instance v0, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->q1:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 79
    iput-object p1, p0, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->pts:Lcom/vividsolutions/jts/geom/CoordinateSequence;

    .line 80
    iput p2, p0, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->start:I

    .line 81
    add-int/lit8 v0, p2, 0x1

    iput v0, p0, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->end:I

    .line 82
    return-void
.end method

.method public constructor <init>(Lcom/vividsolutions/jts/geom/CoordinateSequence;II)V
    .locals 1
    .param p1, "pts"    # Lcom/vividsolutions/jts/geom/CoordinateSequence;
    .param p2, "start"    # I
    .param p3, "end"    # I

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    new-instance v0, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->pt:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 55
    new-instance v0, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->seqPt:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 132
    new-instance v0, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 133
    new-instance v0, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 134
    new-instance v0, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->q0:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 135
    new-instance v0, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->q1:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 66
    iput-object p1, p0, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->pts:Lcom/vividsolutions/jts/geom/CoordinateSequence;

    .line 67
    iput p2, p0, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->start:I

    .line 68
    iput p3, p0, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->end:I

    .line 69
    return-void
.end method

.method private computeLineLineDistance(Lcom/vividsolutions/jts/operation/distance/FacetSequence;)D
    .locals 12
    .param p1, "facetSeq"    # Lcom/vividsolutions/jts/operation/distance/FacetSequence;

    .prologue
    const-wide/16 v6, 0x0

    .line 140
    const-wide v4, 0x7fefffffffffffffL    # Double.MAX_VALUE

    .line 142
    .local v4, "minDistance":D
    iget v2, p0, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->start:I

    .local v2, "i":I
    :goto_0
    iget v8, p0, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->end:I

    add-int/lit8 v8, v8, -0x1

    if-ge v2, v8, :cond_3

    .line 143
    iget v3, p1, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->start:I

    .local v3, "j":I
    :goto_1
    iget v8, p1, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->end:I

    add-int/lit8 v8, v8, -0x1

    if-ge v3, v8, :cond_2

    .line 144
    iget-object v8, p0, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->pts:Lcom/vividsolutions/jts/geom/CoordinateSequence;

    iget-object v9, p0, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-interface {v8, v2, v9}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getCoordinate(ILcom/vividsolutions/jts/geom/Coordinate;)V

    .line 145
    iget-object v8, p0, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->pts:Lcom/vividsolutions/jts/geom/CoordinateSequence;

    add-int/lit8 v9, v2, 0x1

    iget-object v10, p0, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-interface {v8, v9, v10}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getCoordinate(ILcom/vividsolutions/jts/geom/Coordinate;)V

    .line 146
    iget-object v8, p1, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->pts:Lcom/vividsolutions/jts/geom/CoordinateSequence;

    iget-object v9, p0, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->q0:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-interface {v8, v3, v9}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getCoordinate(ILcom/vividsolutions/jts/geom/Coordinate;)V

    .line 147
    iget-object v8, p1, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->pts:Lcom/vividsolutions/jts/geom/CoordinateSequence;

    add-int/lit8 v9, v3, 0x1

    iget-object v10, p0, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->q1:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-interface {v8, v9, v10}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getCoordinate(ILcom/vividsolutions/jts/geom/Coordinate;)V

    .line 149
    iget-object v8, p0, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-object v9, p0, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-object v10, p0, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->q0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-object v11, p0, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->q1:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-static {v8, v9, v10, v11}, Lcom/vividsolutions/jts/algorithm/CGAlgorithms;->distanceLineLine(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v0

    .line 150
    .local v0, "dist":D
    cmpl-double v8, v0, v6

    if-nez v8, :cond_0

    .line 157
    .end local v0    # "dist":D
    .end local v3    # "j":I
    :goto_2
    return-wide v6

    .line 152
    .restart local v0    # "dist":D
    .restart local v3    # "j":I
    :cond_0
    cmpg-double v8, v0, v4

    if-gez v8, :cond_1

    .line 153
    move-wide v4, v0

    .line 143
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 142
    .end local v0    # "dist":D
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .end local v3    # "j":I
    :cond_3
    move-wide v6, v4

    .line 157
    goto :goto_2
.end method

.method private computePointLineDistance(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/operation/distance/FacetSequence;)D
    .locals 10
    .param p1, "pt"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "facetSeq"    # Lcom/vividsolutions/jts/operation/distance/FacetSequence;

    .prologue
    const-wide/16 v6, 0x0

    .line 162
    const-wide v4, 0x7fefffffffffffffL    # Double.MAX_VALUE

    .line 164
    .local v4, "minDistance":D
    iget v2, p2, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->start:I

    .local v2, "i":I
    :goto_0
    iget v3, p2, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->end:I

    add-int/lit8 v3, v3, -0x1

    if-ge v2, v3, :cond_0

    .line 165
    iget-object v3, p2, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->pts:Lcom/vividsolutions/jts/geom/CoordinateSequence;

    iget-object v8, p0, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->q0:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-interface {v3, v2, v8}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getCoordinate(ILcom/vividsolutions/jts/geom/Coordinate;)V

    .line 166
    iget-object v3, p2, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->pts:Lcom/vividsolutions/jts/geom/CoordinateSequence;

    add-int/lit8 v8, v2, 0x1

    iget-object v9, p0, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->q1:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-interface {v3, v8, v9}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getCoordinate(ILcom/vividsolutions/jts/geom/Coordinate;)V

    .line 167
    iget-object v3, p0, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->q0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-object v8, p0, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->q1:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-static {p1, v3, v8}, Lcom/vividsolutions/jts/algorithm/CGAlgorithms;->distancePointLine(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v0

    .line 168
    .local v0, "dist":D
    cmpl-double v3, v0, v6

    if-nez v3, :cond_1

    move-wide v4, v6

    .line 173
    .end local v0    # "dist":D
    .end local v4    # "minDistance":D
    :cond_0
    return-wide v4

    .line 169
    .restart local v0    # "dist":D
    .restart local v4    # "minDistance":D
    :cond_1
    cmpg-double v3, v0, v4

    if-gez v3, :cond_2

    .line 170
    move-wide v4, v0

    .line 164
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method


# virtual methods
.method public distance(Lcom/vividsolutions/jts/operation/distance/FacetSequence;)D
    .locals 5
    .param p1, "facetSeq"    # Lcom/vividsolutions/jts/operation/distance/FacetSequence;

    .prologue
    .line 111
    invoke-virtual {p0}, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->isPoint()Z

    move-result v0

    .line 112
    .local v0, "isPoint":Z
    invoke-virtual {p1}, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->isPoint()Z

    move-result v1

    .line 114
    .local v1, "isPointOther":Z
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 115
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->pts:Lcom/vividsolutions/jts/geom/CoordinateSequence;

    iget v3, p0, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->start:I

    iget-object v4, p0, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->pt:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-interface {v2, v3, v4}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getCoordinate(ILcom/vividsolutions/jts/geom/Coordinate;)V

    .line 116
    iget-object v2, p1, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->pts:Lcom/vividsolutions/jts/geom/CoordinateSequence;

    iget v3, p1, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->start:I

    iget-object v4, p0, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->seqPt:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-interface {v2, v3, v4}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getCoordinate(ILcom/vividsolutions/jts/geom/Coordinate;)V

    .line 117
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->pt:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-object v3, p0, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->seqPt:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {v2, v3}, Lcom/vividsolutions/jts/geom/Coordinate;->distance(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v2

    .line 127
    :goto_0
    return-wide v2

    .line 119
    :cond_0
    if-eqz v0, :cond_1

    .line 120
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->pts:Lcom/vividsolutions/jts/geom/CoordinateSequence;

    iget v3, p0, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->start:I

    iget-object v4, p0, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->pt:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-interface {v2, v3, v4}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getCoordinate(ILcom/vividsolutions/jts/geom/Coordinate;)V

    .line 121
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->pt:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {p0, v2, p1}, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->computePointLineDistance(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/operation/distance/FacetSequence;)D

    move-result-wide v2

    goto :goto_0

    .line 123
    :cond_1
    if-eqz v1, :cond_2

    .line 124
    iget-object v2, p1, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->pts:Lcom/vividsolutions/jts/geom/CoordinateSequence;

    iget v3, p1, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->start:I

    iget-object v4, p0, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->seqPt:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-interface {v2, v3, v4}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getCoordinate(ILcom/vividsolutions/jts/geom/Coordinate;)V

    .line 125
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->seqPt:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {p0, v2, p0}, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->computePointLineDistance(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/operation/distance/FacetSequence;)D

    move-result-wide v2

    goto :goto_0

    .line 127
    :cond_2
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->computeLineLineDistance(Lcom/vividsolutions/jts/operation/distance/FacetSequence;)D

    move-result-wide v2

    goto :goto_0
.end method

.method public getCoordinate(I)Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 100
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->pts:Lcom/vividsolutions/jts/geom/CoordinateSequence;

    iget v1, p0, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->start:I

    add-int/2addr v1, p1

    invoke-interface {v0, v1}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getCoordinate(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    return-object v0
.end method

.method public getEnvelope()Lcom/vividsolutions/jts/geom/Envelope;
    .locals 6

    .prologue
    .line 86
    new-instance v0, Lcom/vividsolutions/jts/geom/Envelope;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/Envelope;-><init>()V

    .line 87
    .local v0, "env":Lcom/vividsolutions/jts/geom/Envelope;
    iget v1, p0, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->start:I

    .local v1, "i":I
    :goto_0
    iget v2, p0, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->end:I

    if-ge v1, v2, :cond_0

    .line 88
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->pts:Lcom/vividsolutions/jts/geom/CoordinateSequence;

    invoke-interface {v2, v1}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getX(I)D

    move-result-wide v2

    iget-object v4, p0, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->pts:Lcom/vividsolutions/jts/geom/CoordinateSequence;

    invoke-interface {v4, v1}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getY(I)D

    move-result-wide v4

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/vividsolutions/jts/geom/Envelope;->expandToInclude(DD)V

    .line 87
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 90
    :cond_0
    return-object v0
.end method

.method public isPoint()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 105
    iget v1, p0, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->end:I

    iget v2, p0, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->start:I

    sub-int/2addr v1, v2

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public size()I
    .locals 2

    .prologue
    .line 95
    iget v0, p0, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->end:I

    iget v1, p0, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->start:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 178
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 179
    .local v0, "buf":Ljava/lang/StringBuffer;
    const-string v3, "LINESTRING ( "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 180
    new-instance v2, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v2}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>()V

    .line 181
    .local v2, "p":Lcom/vividsolutions/jts/geom/Coordinate;
    iget v1, p0, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->start:I

    .local v1, "i":I
    :goto_0
    iget v3, p0, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->end:I

    if-ge v1, v3, :cond_1

    .line 182
    iget v3, p0, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->start:I

    if-le v1, v3, :cond_0

    .line 183
    const-string v3, ", "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 184
    :cond_0
    iget-object v3, p0, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->pts:Lcom/vividsolutions/jts/geom/CoordinateSequence;

    invoke-interface {v3, v1, v2}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getCoordinate(ILcom/vividsolutions/jts/geom/Coordinate;)V

    .line 185
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v4, v2, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, v2, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 181
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 187
    :cond_1
    const-string v3, " )"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 188
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

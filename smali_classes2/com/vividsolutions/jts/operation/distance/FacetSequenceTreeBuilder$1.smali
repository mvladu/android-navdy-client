.class Lcom/vividsolutions/jts/operation/distance/FacetSequenceTreeBuilder$1;
.super Ljava/lang/Object;
.source "FacetSequenceTreeBuilder.java"

# interfaces
.implements Lcom/vividsolutions/jts/geom/GeometryComponentFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/vividsolutions/jts/operation/distance/FacetSequenceTreeBuilder;->computeFacetSequences(Lcom/vividsolutions/jts/geom/Geometry;)Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$sections:Ljava/util/List;


# direct methods
.method constructor <init>(Ljava/util/List;)V
    .locals 0

    .prologue
    .line 77
    iput-object p1, p0, Lcom/vividsolutions/jts/operation/distance/FacetSequenceTreeBuilder$1;->val$sections:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public filter(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 2
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 78
    const/4 v0, 0x0

    .line 79
    .local v0, "seq":Lcom/vividsolutions/jts/geom/CoordinateSequence;
    instance-of v1, p1, Lcom/vividsolutions/jts/geom/LineString;

    if-eqz v1, :cond_1

    .line 80
    check-cast p1, Lcom/vividsolutions/jts/geom/LineString;

    .end local p1    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinateSequence()Lcom/vividsolutions/jts/geom/CoordinateSequence;

    move-result-object v0

    .line 81
    iget-object v1, p0, Lcom/vividsolutions/jts/operation/distance/FacetSequenceTreeBuilder$1;->val$sections:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/vividsolutions/jts/operation/distance/FacetSequenceTreeBuilder;->access$000(Lcom/vividsolutions/jts/geom/CoordinateSequence;Ljava/util/List;)V

    .line 87
    :cond_0
    :goto_0
    return-void

    .line 83
    .restart local p1    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_1
    instance-of v1, p1, Lcom/vividsolutions/jts/geom/Point;

    if-eqz v1, :cond_0

    .line 84
    check-cast p1, Lcom/vividsolutions/jts/geom/Point;

    .end local p1    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Point;->getCoordinateSequence()Lcom/vividsolutions/jts/geom/CoordinateSequence;

    move-result-object v0

    .line 85
    iget-object v1, p0, Lcom/vividsolutions/jts/operation/distance/FacetSequenceTreeBuilder$1;->val$sections:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/vividsolutions/jts/operation/distance/FacetSequenceTreeBuilder;->access$000(Lcom/vividsolutions/jts/geom/CoordinateSequence;Ljava/util/List;)V

    goto :goto_0
.end method

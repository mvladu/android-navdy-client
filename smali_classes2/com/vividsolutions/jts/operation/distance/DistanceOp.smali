.class public Lcom/vividsolutions/jts/operation/distance/DistanceOp;
.super Ljava/lang/Object;
.source "DistanceOp.java"


# instance fields
.field private geom:[Lcom/vividsolutions/jts/geom/Geometry;

.field private minDistance:D

.field private minDistanceLocation:[Lcom/vividsolutions/jts/operation/distance/GeometryLocation;

.field private ptLocator:Lcom/vividsolutions/jts/algorithm/PointLocator;

.field private terminateDistance:D


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 2
    .param p1, "g0"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "g1"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 130
    const-wide/16 v0, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/vividsolutions/jts/operation/distance/DistanceOp;-><init>(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;D)V

    .line 131
    return-void
.end method

.method public constructor <init>(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;D)V
    .locals 3
    .param p1, "g0"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "g1"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p3, "terminateDistance"    # D

    .prologue
    .line 141
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 116
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->terminateDistance:D

    .line 118
    new-instance v0, Lcom/vividsolutions/jts/algorithm/PointLocator;

    invoke-direct {v0}, Lcom/vividsolutions/jts/algorithm/PointLocator;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->ptLocator:Lcom/vividsolutions/jts/algorithm/PointLocator;

    .line 120
    const-wide v0, 0x7fefffffffffffffL    # Double.MAX_VALUE

    iput-wide v0, p0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->minDistance:D

    .line 142
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/vividsolutions/jts/geom/Geometry;

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->geom:[Lcom/vividsolutions/jts/geom/Geometry;

    .line 143
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->geom:[Lcom/vividsolutions/jts/geom/Geometry;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 144
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->geom:[Lcom/vividsolutions/jts/geom/Geometry;

    const/4 v1, 0x1

    aput-object p2, v0, v1

    .line 145
    iput-wide p3, p0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->terminateDistance:D

    .line 146
    return-void
.end method

.method public static closestPoints(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)[Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 2
    .param p0, "g0"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p1, "g1"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 110
    new-instance v0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;

    invoke-direct {v0, p0, p1}, Lcom/vividsolutions/jts/operation/distance/DistanceOp;-><init>(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 111
    .local v0, "distOp":Lcom/vividsolutions/jts/operation/distance/DistanceOp;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->nearestPoints()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    return-object v1
.end method

.method private computeContainmentDistance()V
    .locals 6

    .prologue
    .line 242
    const/4 v1, 0x2

    new-array v0, v1, [Lcom/vividsolutions/jts/operation/distance/GeometryLocation;

    .line 244
    .local v0, "locPtPoly":[Lcom/vividsolutions/jts/operation/distance/GeometryLocation;
    const/4 v1, 0x0

    invoke-direct {p0, v1, v0}, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->computeContainmentDistance(I[Lcom/vividsolutions/jts/operation/distance/GeometryLocation;)V

    .line 245
    iget-wide v2, p0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->minDistance:D

    iget-wide v4, p0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->terminateDistance:D

    cmpg-double v1, v2, v4

    if-gtz v1, :cond_0

    .line 247
    :goto_0
    return-void

    .line 246
    :cond_0
    const/4 v1, 0x1

    invoke-direct {p0, v1, v0}, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->computeContainmentDistance(I[Lcom/vividsolutions/jts/operation/distance/GeometryLocation;)V

    goto :goto_0
.end method

.method private computeContainmentDistance(I[Lcom/vividsolutions/jts/operation/distance/GeometryLocation;)V
    .locals 8
    .param p1, "polyGeomIndex"    # I
    .param p2, "locPtPoly"    # [Lcom/vividsolutions/jts/operation/distance/GeometryLocation;

    .prologue
    .line 251
    rsub-int/lit8 v1, p1, 0x1

    .line 252
    .local v1, "locationsIndex":I
    iget-object v3, p0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->geom:[Lcom/vividsolutions/jts/geom/Geometry;

    aget-object v3, v3, p1

    invoke-static {v3}, Lcom/vividsolutions/jts/geom/util/PolygonExtracter;->getPolygons(Lcom/vividsolutions/jts/geom/Geometry;)Ljava/util/List;

    move-result-object v2

    .line 253
    .local v2, "polys":Ljava/util/List;
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 254
    iget-object v3, p0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->geom:[Lcom/vividsolutions/jts/geom/Geometry;

    aget-object v3, v3, v1

    invoke-static {v3}, Lcom/vividsolutions/jts/operation/distance/ConnectedElementLocationFilter;->getLocations(Lcom/vividsolutions/jts/geom/Geometry;)Ljava/util/List;

    move-result-object v0

    .line 255
    .local v0, "insideLocs":Ljava/util/List;
    invoke-direct {p0, v0, v2, p2}, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->computeContainmentDistance(Ljava/util/List;Ljava/util/List;[Lcom/vividsolutions/jts/operation/distance/GeometryLocation;)V

    .line 256
    iget-wide v4, p0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->minDistance:D

    iget-wide v6, p0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->terminateDistance:D

    cmpg-double v3, v4, v6

    if-gtz v3, :cond_0

    .line 258
    iget-object v3, p0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->minDistanceLocation:[Lcom/vividsolutions/jts/operation/distance/GeometryLocation;

    const/4 v4, 0x0

    aget-object v4, p2, v4

    aput-object v4, v3, v1

    .line 259
    iget-object v3, p0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->minDistanceLocation:[Lcom/vividsolutions/jts/operation/distance/GeometryLocation;

    const/4 v4, 0x1

    aget-object v4, p2, v4

    aput-object v4, v3, p1

    .line 263
    .end local v0    # "insideLocs":Ljava/util/List;
    :cond_0
    return-void
.end method

.method private computeContainmentDistance(Lcom/vividsolutions/jts/operation/distance/GeometryLocation;Lcom/vividsolutions/jts/geom/Polygon;[Lcom/vividsolutions/jts/operation/distance/GeometryLocation;)V
    .locals 4
    .param p1, "ptLoc"    # Lcom/vividsolutions/jts/operation/distance/GeometryLocation;
    .param p2, "poly"    # Lcom/vividsolutions/jts/geom/Polygon;
    .param p3, "locPtPoly"    # [Lcom/vividsolutions/jts/operation/distance/GeometryLocation;

    .prologue
    .line 280
    invoke-virtual {p1}, Lcom/vividsolutions/jts/operation/distance/GeometryLocation;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    .line 282
    .local v0, "pt":Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->ptLocator:Lcom/vividsolutions/jts/algorithm/PointLocator;

    invoke-virtual {v2, v0, p2}, Lcom/vividsolutions/jts/algorithm/PointLocator;->locate(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Geometry;)I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 283
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->minDistance:D

    .line 284
    const/4 v1, 0x0

    aput-object p1, p3, v1

    .line 285
    const/4 v1, 0x1

    new-instance v2, Lcom/vividsolutions/jts/operation/distance/GeometryLocation;

    invoke-direct {v2, p2, v0}, Lcom/vividsolutions/jts/operation/distance/GeometryLocation;-><init>(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Coordinate;)V

    aput-object v2, p3, v1

    .line 288
    :cond_0
    return-void
.end method

.method private computeContainmentDistance(Ljava/util/List;Ljava/util/List;[Lcom/vividsolutions/jts/operation/distance/GeometryLocation;)V
    .locals 8
    .param p1, "locs"    # Ljava/util/List;
    .param p2, "polys"    # Ljava/util/List;
    .param p3, "locPtPoly"    # [Lcom/vividsolutions/jts/operation/distance/GeometryLocation;

    .prologue
    .line 267
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 268
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vividsolutions/jts/operation/distance/GeometryLocation;

    .line 269
    .local v2, "loc":Lcom/vividsolutions/jts/operation/distance/GeometryLocation;
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_2

    .line 270
    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vividsolutions/jts/geom/Polygon;

    invoke-direct {p0, v2, v3, p3}, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->computeContainmentDistance(Lcom/vividsolutions/jts/operation/distance/GeometryLocation;Lcom/vividsolutions/jts/geom/Polygon;[Lcom/vividsolutions/jts/operation/distance/GeometryLocation;)V

    .line 271
    iget-wide v4, p0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->minDistance:D

    iget-wide v6, p0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->terminateDistance:D

    cmpg-double v3, v4, v6

    if-gtz v3, :cond_1

    .line 274
    .end local v1    # "j":I
    .end local v2    # "loc":Lcom/vividsolutions/jts/operation/distance/GeometryLocation;
    :cond_0
    return-void

    .line 269
    .restart local v1    # "j":I
    .restart local v2    # "loc":Lcom/vividsolutions/jts/operation/distance/GeometryLocation;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 267
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private computeFacetDistance()V
    .locals 13

    .prologue
    const/4 v12, 0x0

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 297
    const/4 v5, 0x2

    new-array v2, v5, [Lcom/vividsolutions/jts/operation/distance/GeometryLocation;

    .line 303
    .local v2, "locGeom":[Lcom/vividsolutions/jts/operation/distance/GeometryLocation;
    iget-object v5, p0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->geom:[Lcom/vividsolutions/jts/geom/Geometry;

    aget-object v5, v5, v10

    invoke-static {v5}, Lcom/vividsolutions/jts/geom/util/LinearComponentExtracter;->getLines(Lcom/vividsolutions/jts/geom/Geometry;)Ljava/util/List;

    move-result-object v0

    .line 304
    .local v0, "lines0":Ljava/util/List;
    iget-object v5, p0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->geom:[Lcom/vividsolutions/jts/geom/Geometry;

    aget-object v5, v5, v11

    invoke-static {v5}, Lcom/vividsolutions/jts/geom/util/LinearComponentExtracter;->getLines(Lcom/vividsolutions/jts/geom/Geometry;)Ljava/util/List;

    move-result-object v1

    .line 306
    .local v1, "lines1":Ljava/util/List;
    iget-object v5, p0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->geom:[Lcom/vividsolutions/jts/geom/Geometry;

    aget-object v5, v5, v10

    invoke-static {v5}, Lcom/vividsolutions/jts/geom/util/PointExtracter;->getPoints(Lcom/vividsolutions/jts/geom/Geometry;)Ljava/util/List;

    move-result-object v3

    .line 307
    .local v3, "pts0":Ljava/util/List;
    iget-object v5, p0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->geom:[Lcom/vividsolutions/jts/geom/Geometry;

    aget-object v5, v5, v11

    invoke-static {v5}, Lcom/vividsolutions/jts/geom/util/PointExtracter;->getPoints(Lcom/vividsolutions/jts/geom/Geometry;)Ljava/util/List;

    move-result-object v4

    .line 310
    .local v4, "pts1":Ljava/util/List;
    invoke-direct {p0, v0, v1, v2}, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->computeMinDistanceLines(Ljava/util/List;Ljava/util/List;[Lcom/vividsolutions/jts/operation/distance/GeometryLocation;)V

    .line 311
    invoke-direct {p0, v2, v10}, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->updateMinDistance([Lcom/vividsolutions/jts/operation/distance/GeometryLocation;Z)V

    .line 312
    iget-wide v6, p0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->minDistance:D

    iget-wide v8, p0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->terminateDistance:D

    cmpg-double v5, v6, v8

    if-gtz v5, :cond_1

    .line 330
    :cond_0
    :goto_0
    return-void

    .line 314
    :cond_1
    aput-object v12, v2, v10

    .line 315
    aput-object v12, v2, v11

    .line 316
    invoke-direct {p0, v0, v4, v2}, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->computeMinDistanceLinesPoints(Ljava/util/List;Ljava/util/List;[Lcom/vividsolutions/jts/operation/distance/GeometryLocation;)V

    .line 317
    invoke-direct {p0, v2, v10}, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->updateMinDistance([Lcom/vividsolutions/jts/operation/distance/GeometryLocation;Z)V

    .line 318
    iget-wide v6, p0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->minDistance:D

    iget-wide v8, p0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->terminateDistance:D

    cmpg-double v5, v6, v8

    if-lez v5, :cond_0

    .line 320
    aput-object v12, v2, v10

    .line 321
    aput-object v12, v2, v11

    .line 322
    invoke-direct {p0, v1, v3, v2}, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->computeMinDistanceLinesPoints(Ljava/util/List;Ljava/util/List;[Lcom/vividsolutions/jts/operation/distance/GeometryLocation;)V

    .line 323
    invoke-direct {p0, v2, v11}, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->updateMinDistance([Lcom/vividsolutions/jts/operation/distance/GeometryLocation;Z)V

    .line 324
    iget-wide v6, p0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->minDistance:D

    iget-wide v8, p0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->terminateDistance:D

    cmpg-double v5, v6, v8

    if-lez v5, :cond_0

    .line 326
    aput-object v12, v2, v10

    .line 327
    aput-object v12, v2, v11

    .line 328
    invoke-direct {p0, v3, v4, v2}, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->computeMinDistancePoints(Ljava/util/List;Ljava/util/List;[Lcom/vividsolutions/jts/operation/distance/GeometryLocation;)V

    .line 329
    invoke-direct {p0, v2, v10}, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->updateMinDistance([Lcom/vividsolutions/jts/operation/distance/GeometryLocation;Z)V

    goto :goto_0
.end method

.method private computeMinDistance()V
    .locals 4

    .prologue
    .line 232
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->minDistanceLocation:[Lcom/vividsolutions/jts/operation/distance/GeometryLocation;

    if-eqz v0, :cond_1

    .line 238
    :cond_0
    :goto_0
    return-void

    .line 234
    :cond_1
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/vividsolutions/jts/operation/distance/GeometryLocation;

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->minDistanceLocation:[Lcom/vividsolutions/jts/operation/distance/GeometryLocation;

    .line 235
    invoke-direct {p0}, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->computeContainmentDistance()V

    .line 236
    iget-wide v0, p0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->minDistance:D

    iget-wide v2, p0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->terminateDistance:D

    cmpg-double v0, v0, v2

    if-lez v0, :cond_0

    .line 237
    invoke-direct {p0}, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->computeFacetDistance()V

    goto :goto_0
.end method

.method private computeMinDistance(Lcom/vividsolutions/jts/geom/LineString;Lcom/vividsolutions/jts/geom/LineString;[Lcom/vividsolutions/jts/operation/distance/GeometryLocation;)V
    .locals 16
    .param p1, "line0"    # Lcom/vividsolutions/jts/geom/LineString;
    .param p2, "line1"    # Lcom/vividsolutions/jts/geom/LineString;
    .param p3, "locGeom"    # [Lcom/vividsolutions/jts/operation/distance/GeometryLocation;

    .prologue
    .line 377
    invoke-virtual/range {p1 .. p1}, Lcom/vividsolutions/jts/geom/LineString;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v11

    invoke-virtual/range {p2 .. p2}, Lcom/vividsolutions/jts/geom/LineString;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/vividsolutions/jts/geom/Envelope;->distance(Lcom/vividsolutions/jts/geom/Envelope;)D

    move-result-wide v12

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->minDistance:D

    cmpl-double v11, v12, v14

    if-lez v11, :cond_1

    .line 399
    :cond_0
    return-void

    .line 380
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v3

    .line 381
    .local v3, "coord0":[Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-virtual/range {p2 .. p2}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v4

    .line 383
    .local v4, "coord1":[Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    array-length v11, v3

    add-int/lit8 v11, v11, -0x1

    if-ge v5, v11, :cond_0

    .line 384
    const/4 v8, 0x0

    .local v8, "j":I
    :goto_1
    array-length v11, v4

    add-int/lit8 v11, v11, -0x1

    if-ge v8, v11, :cond_3

    .line 385
    aget-object v11, v3, v5

    add-int/lit8 v12, v5, 0x1

    aget-object v12, v3, v12

    aget-object v13, v4, v8

    add-int/lit8 v14, v8, 0x1

    aget-object v14, v4, v14

    invoke-static {v11, v12, v13, v14}, Lcom/vividsolutions/jts/algorithm/CGAlgorithms;->distanceLineLine(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v6

    .line 388
    .local v6, "dist":D
    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->minDistance:D

    cmpg-double v11, v6, v12

    if-gez v11, :cond_2

    .line 389
    move-object/from16 v0, p0

    iput-wide v6, v0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->minDistance:D

    .line 390
    new-instance v9, Lcom/vividsolutions/jts/geom/LineSegment;

    aget-object v11, v3, v5

    add-int/lit8 v12, v5, 0x1

    aget-object v12, v3, v12

    invoke-direct {v9, v11, v12}, Lcom/vividsolutions/jts/geom/LineSegment;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 391
    .local v9, "seg0":Lcom/vividsolutions/jts/geom/LineSegment;
    new-instance v10, Lcom/vividsolutions/jts/geom/LineSegment;

    aget-object v11, v4, v8

    add-int/lit8 v12, v8, 0x1

    aget-object v12, v4, v12

    invoke-direct {v10, v11, v12}, Lcom/vividsolutions/jts/geom/LineSegment;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 392
    .local v10, "seg1":Lcom/vividsolutions/jts/geom/LineSegment;
    invoke-virtual {v9, v10}, Lcom/vividsolutions/jts/geom/LineSegment;->closestPoints(Lcom/vividsolutions/jts/geom/LineSegment;)[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    .line 393
    .local v2, "closestPt":[Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v11, 0x0

    new-instance v12, Lcom/vividsolutions/jts/operation/distance/GeometryLocation;

    const/4 v13, 0x0

    aget-object v13, v2, v13

    move-object/from16 v0, p1

    invoke-direct {v12, v0, v5, v13}, Lcom/vividsolutions/jts/operation/distance/GeometryLocation;-><init>(Lcom/vividsolutions/jts/geom/Geometry;ILcom/vividsolutions/jts/geom/Coordinate;)V

    aput-object v12, p3, v11

    .line 394
    const/4 v11, 0x1

    new-instance v12, Lcom/vividsolutions/jts/operation/distance/GeometryLocation;

    const/4 v13, 0x1

    aget-object v13, v2, v13

    move-object/from16 v0, p2

    invoke-direct {v12, v0, v8, v13}, Lcom/vividsolutions/jts/operation/distance/GeometryLocation;-><init>(Lcom/vividsolutions/jts/geom/Geometry;ILcom/vividsolutions/jts/geom/Coordinate;)V

    aput-object v12, p3, v11

    .line 396
    .end local v2    # "closestPt":[Lcom/vividsolutions/jts/geom/Coordinate;
    .end local v9    # "seg0":Lcom/vividsolutions/jts/geom/LineSegment;
    .end local v10    # "seg1":Lcom/vividsolutions/jts/geom/LineSegment;
    :cond_2
    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->minDistance:D

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->terminateDistance:D

    cmpg-double v11, v12, v14

    if-lez v11, :cond_0

    .line 384
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 383
    .end local v6    # "dist":D
    :cond_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_0
.end method

.method private computeMinDistance(Lcom/vividsolutions/jts/geom/LineString;Lcom/vividsolutions/jts/geom/Point;[Lcom/vividsolutions/jts/operation/distance/GeometryLocation;)V
    .locals 12
    .param p1, "line"    # Lcom/vividsolutions/jts/geom/LineString;
    .param p2, "pt"    # Lcom/vividsolutions/jts/geom/Point;
    .param p3, "locGeom"    # [Lcom/vividsolutions/jts/operation/distance/GeometryLocation;

    .prologue
    .line 404
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/LineString;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v7

    invoke-virtual {p2}, Lcom/vividsolutions/jts/geom/Point;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/vividsolutions/jts/geom/Envelope;->distance(Lcom/vividsolutions/jts/geom/Envelope;)D

    move-result-wide v8

    iget-wide v10, p0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->minDistance:D

    cmpl-double v7, v8, v10

    if-lez v7, :cond_1

    .line 423
    :cond_0
    return-void

    .line 407
    :cond_1
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    .line 408
    .local v1, "coord0":[Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-virtual {p2}, Lcom/vividsolutions/jts/geom/Point;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    .line 410
    .local v0, "coord":Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    array-length v7, v1

    add-int/lit8 v7, v7, -0x1

    if-ge v4, v7, :cond_0

    .line 411
    aget-object v7, v1, v4

    add-int/lit8 v8, v4, 0x1

    aget-object v8, v1, v8

    invoke-static {v0, v7, v8}, Lcom/vividsolutions/jts/algorithm/CGAlgorithms;->distancePointLine(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v2

    .line 413
    .local v2, "dist":D
    iget-wide v8, p0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->minDistance:D

    cmpg-double v7, v2, v8

    if-gez v7, :cond_2

    .line 414
    iput-wide v2, p0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->minDistance:D

    .line 415
    new-instance v5, Lcom/vividsolutions/jts/geom/LineSegment;

    aget-object v7, v1, v4

    add-int/lit8 v8, v4, 0x1

    aget-object v8, v1, v8

    invoke-direct {v5, v7, v8}, Lcom/vividsolutions/jts/geom/LineSegment;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 416
    .local v5, "seg":Lcom/vividsolutions/jts/geom/LineSegment;
    invoke-virtual {v5, v0}, Lcom/vividsolutions/jts/geom/LineSegment;->closestPoint(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v6

    .line 417
    .local v6, "segClosestPoint":Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v7, 0x0

    new-instance v8, Lcom/vividsolutions/jts/operation/distance/GeometryLocation;

    invoke-direct {v8, p1, v4, v6}, Lcom/vividsolutions/jts/operation/distance/GeometryLocation;-><init>(Lcom/vividsolutions/jts/geom/Geometry;ILcom/vividsolutions/jts/geom/Coordinate;)V

    aput-object v8, p3, v7

    .line 418
    const/4 v7, 0x1

    new-instance v8, Lcom/vividsolutions/jts/operation/distance/GeometryLocation;

    const/4 v9, 0x0

    invoke-direct {v8, p2, v9, v0}, Lcom/vividsolutions/jts/operation/distance/GeometryLocation;-><init>(Lcom/vividsolutions/jts/geom/Geometry;ILcom/vividsolutions/jts/geom/Coordinate;)V

    aput-object v8, p3, v7

    .line 420
    .end local v5    # "seg":Lcom/vividsolutions/jts/geom/LineSegment;
    .end local v6    # "segClosestPoint":Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_2
    iget-wide v8, p0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->minDistance:D

    iget-wide v10, p0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->terminateDistance:D

    cmpg-double v7, v8, v10

    if-lez v7, :cond_0

    .line 410
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method private computeMinDistanceLines(Ljava/util/List;Ljava/util/List;[Lcom/vividsolutions/jts/operation/distance/GeometryLocation;)V
    .locals 8
    .param p1, "lines0"    # Ljava/util/List;
    .param p2, "lines1"    # Ljava/util/List;
    .param p3, "locGeom"    # [Lcom/vividsolutions/jts/operation/distance/GeometryLocation;

    .prologue
    .line 334
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    if-ge v0, v4, :cond_0

    .line 335
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vividsolutions/jts/geom/LineString;

    .line 336
    .local v2, "line0":Lcom/vividsolutions/jts/geom/LineString;
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v4

    if-ge v1, v4, :cond_2

    .line 337
    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vividsolutions/jts/geom/LineString;

    .line 338
    .local v3, "line1":Lcom/vividsolutions/jts/geom/LineString;
    invoke-direct {p0, v2, v3, p3}, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->computeMinDistance(Lcom/vividsolutions/jts/geom/LineString;Lcom/vividsolutions/jts/geom/LineString;[Lcom/vividsolutions/jts/operation/distance/GeometryLocation;)V

    .line 339
    iget-wide v4, p0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->minDistance:D

    iget-wide v6, p0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->terminateDistance:D

    cmpg-double v4, v4, v6

    if-gtz v4, :cond_1

    .line 342
    .end local v1    # "j":I
    .end local v2    # "line0":Lcom/vividsolutions/jts/geom/LineString;
    .end local v3    # "line1":Lcom/vividsolutions/jts/geom/LineString;
    :cond_0
    return-void

    .line 336
    .restart local v1    # "j":I
    .restart local v2    # "line0":Lcom/vividsolutions/jts/geom/LineString;
    .restart local v3    # "line1":Lcom/vividsolutions/jts/geom/LineString;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 334
    .end local v3    # "line1":Lcom/vividsolutions/jts/geom/LineString;
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private computeMinDistanceLinesPoints(Ljava/util/List;Ljava/util/List;[Lcom/vividsolutions/jts/operation/distance/GeometryLocation;)V
    .locals 8
    .param p1, "lines"    # Ljava/util/List;
    .param p2, "points"    # Ljava/util/List;
    .param p3, "locGeom"    # [Lcom/vividsolutions/jts/operation/distance/GeometryLocation;

    .prologue
    .line 364
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    if-ge v0, v4, :cond_0

    .line 365
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vividsolutions/jts/geom/LineString;

    .line 366
    .local v2, "line":Lcom/vividsolutions/jts/geom/LineString;
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v4

    if-ge v1, v4, :cond_2

    .line 367
    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vividsolutions/jts/geom/Point;

    .line 368
    .local v3, "pt":Lcom/vividsolutions/jts/geom/Point;
    invoke-direct {p0, v2, v3, p3}, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->computeMinDistance(Lcom/vividsolutions/jts/geom/LineString;Lcom/vividsolutions/jts/geom/Point;[Lcom/vividsolutions/jts/operation/distance/GeometryLocation;)V

    .line 369
    iget-wide v4, p0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->minDistance:D

    iget-wide v6, p0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->terminateDistance:D

    cmpg-double v4, v4, v6

    if-gtz v4, :cond_1

    .line 372
    .end local v1    # "j":I
    .end local v2    # "line":Lcom/vividsolutions/jts/geom/LineString;
    .end local v3    # "pt":Lcom/vividsolutions/jts/geom/Point;
    :cond_0
    return-void

    .line 366
    .restart local v1    # "j":I
    .restart local v2    # "line":Lcom/vividsolutions/jts/geom/LineString;
    .restart local v3    # "pt":Lcom/vividsolutions/jts/geom/Point;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 364
    .end local v3    # "pt":Lcom/vividsolutions/jts/geom/Point;
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private computeMinDistancePoints(Ljava/util/List;Ljava/util/List;[Lcom/vividsolutions/jts/operation/distance/GeometryLocation;)V
    .locals 11
    .param p1, "points0"    # Ljava/util/List;
    .param p2, "points1"    # Ljava/util/List;
    .param p3, "locGeom"    # [Lcom/vividsolutions/jts/operation/distance/GeometryLocation;

    .prologue
    const/4 v10, 0x0

    .line 346
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    if-ge v2, v6, :cond_1

    .line 347
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vividsolutions/jts/geom/Point;

    .line 348
    .local v4, "pt0":Lcom/vividsolutions/jts/geom/Point;
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_1
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v6

    if-ge v3, v6, :cond_3

    .line 349
    invoke-interface {p2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vividsolutions/jts/geom/Point;

    .line 350
    .local v5, "pt1":Lcom/vividsolutions/jts/geom/Point;
    invoke-virtual {v4}, Lcom/vividsolutions/jts/geom/Point;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v6

    invoke-virtual {v5}, Lcom/vividsolutions/jts/geom/Point;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/vividsolutions/jts/geom/Coordinate;->distance(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v0

    .line 351
    .local v0, "dist":D
    iget-wide v6, p0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->minDistance:D

    cmpg-double v6, v0, v6

    if-gez v6, :cond_0

    .line 352
    iput-wide v0, p0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->minDistance:D

    .line 353
    new-instance v6, Lcom/vividsolutions/jts/operation/distance/GeometryLocation;

    invoke-virtual {v4}, Lcom/vividsolutions/jts/geom/Point;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v7

    invoke-direct {v6, v4, v10, v7}, Lcom/vividsolutions/jts/operation/distance/GeometryLocation;-><init>(Lcom/vividsolutions/jts/geom/Geometry;ILcom/vividsolutions/jts/geom/Coordinate;)V

    aput-object v6, p3, v10

    .line 354
    const/4 v6, 0x1

    new-instance v7, Lcom/vividsolutions/jts/operation/distance/GeometryLocation;

    invoke-virtual {v5}, Lcom/vividsolutions/jts/geom/Point;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v8

    invoke-direct {v7, v5, v10, v8}, Lcom/vividsolutions/jts/operation/distance/GeometryLocation;-><init>(Lcom/vividsolutions/jts/geom/Geometry;ILcom/vividsolutions/jts/geom/Coordinate;)V

    aput-object v7, p3, v6

    .line 356
    :cond_0
    iget-wide v6, p0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->minDistance:D

    iget-wide v8, p0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->terminateDistance:D

    cmpg-double v6, v6, v8

    if-gtz v6, :cond_2

    .line 359
    .end local v0    # "dist":D
    .end local v3    # "j":I
    .end local v4    # "pt0":Lcom/vividsolutions/jts/geom/Point;
    .end local v5    # "pt1":Lcom/vividsolutions/jts/geom/Point;
    :cond_1
    return-void

    .line 348
    .restart local v0    # "dist":D
    .restart local v3    # "j":I
    .restart local v4    # "pt0":Lcom/vividsolutions/jts/geom/Point;
    .restart local v5    # "pt1":Lcom/vividsolutions/jts/geom/Point;
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 346
    .end local v0    # "dist":D
    .end local v5    # "pt1":Lcom/vividsolutions/jts/geom/Point;
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static distance(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)D
    .locals 4
    .param p0, "g0"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p1, "g1"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 68
    new-instance v0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;

    invoke-direct {v0, p0, p1}, Lcom/vividsolutions/jts/operation/distance/DistanceOp;-><init>(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 69
    .local v0, "distOp":Lcom/vividsolutions/jts/operation/distance/DistanceOp;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->distance()D

    move-result-wide v2

    return-wide v2
.end method

.method public static isWithinDistance(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;D)Z
    .locals 4
    .param p0, "g0"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p1, "g1"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "distance"    # D

    .prologue
    .line 81
    new-instance v0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/vividsolutions/jts/operation/distance/DistanceOp;-><init>(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;D)V

    .line 82
    .local v0, "distOp":Lcom/vividsolutions/jts/operation/distance/DistanceOp;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->distance()D

    move-result-wide v2

    cmpg-double v1, v2, p2

    if-gtz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static nearestPoints(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)[Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 2
    .param p0, "g0"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p1, "g1"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 95
    new-instance v0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;

    invoke-direct {v0, p0, p1}, Lcom/vividsolutions/jts/operation/distance/DistanceOp;-><init>(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 96
    .local v0, "distOp":Lcom/vividsolutions/jts/operation/distance/DistanceOp;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->nearestPoints()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    return-object v1
.end method

.method private updateMinDistance([Lcom/vividsolutions/jts/operation/distance/GeometryLocation;Z)V
    .locals 4
    .param p1, "locGeom"    # [Lcom/vividsolutions/jts/operation/distance/GeometryLocation;
    .param p2, "flip"    # Z

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 217
    aget-object v0, p1, v2

    if-nez v0, :cond_0

    .line 227
    :goto_0
    return-void

    .line 219
    :cond_0
    if-eqz p2, :cond_1

    .line 220
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->minDistanceLocation:[Lcom/vividsolutions/jts/operation/distance/GeometryLocation;

    aget-object v1, p1, v3

    aput-object v1, v0, v2

    .line 221
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->minDistanceLocation:[Lcom/vividsolutions/jts/operation/distance/GeometryLocation;

    aget-object v1, p1, v2

    aput-object v1, v0, v3

    goto :goto_0

    .line 224
    :cond_1
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->minDistanceLocation:[Lcom/vividsolutions/jts/operation/distance/GeometryLocation;

    aget-object v1, p1, v2

    aput-object v1, v0, v2

    .line 225
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->minDistanceLocation:[Lcom/vividsolutions/jts/operation/distance/GeometryLocation;

    aget-object v1, p1, v3

    aput-object v1, v0, v3

    goto :goto_0
.end method


# virtual methods
.method public closestLocations()[Lcom/vividsolutions/jts/operation/distance/GeometryLocation;
    .locals 1

    .prologue
    .line 211
    invoke-virtual {p0}, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->nearestLocations()[Lcom/vividsolutions/jts/operation/distance/GeometryLocation;

    move-result-object v0

    return-object v0
.end method

.method public closestPoints()[Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 1

    .prologue
    .line 189
    invoke-virtual {p0}, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->nearestPoints()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    return-object v0
.end method

.method public distance()D
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 157
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->geom:[Lcom/vividsolutions/jts/geom/Geometry;

    aget-object v0, v0, v1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->geom:[Lcom/vividsolutions/jts/geom/Geometry;

    aget-object v0, v0, v2

    if-nez v0, :cond_1

    .line 158
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "null geometries are not supported"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 159
    :cond_1
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->geom:[Lcom/vividsolutions/jts/geom/Geometry;

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/Geometry;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->geom:[Lcom/vividsolutions/jts/geom/Geometry;

    aget-object v0, v0, v2

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/Geometry;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 160
    :cond_2
    const-wide/16 v0, 0x0

    .line 163
    :goto_0
    return-wide v0

    .line 162
    :cond_3
    invoke-direct {p0}, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->computeMinDistance()V

    .line 163
    iget-wide v0, p0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->minDistance:D

    goto :goto_0
.end method

.method public nearestLocations()[Lcom/vividsolutions/jts/operation/distance/GeometryLocation;
    .locals 1

    .prologue
    .line 200
    invoke-direct {p0}, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->computeMinDistance()V

    .line 201
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->minDistanceLocation:[Lcom/vividsolutions/jts/operation/distance/GeometryLocation;

    return-object v0
.end method

.method public nearestPoints()[Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 174
    invoke-direct {p0}, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->computeMinDistance()V

    .line 175
    const/4 v1, 0x2

    new-array v0, v1, [Lcom/vividsolutions/jts/geom/Coordinate;

    iget-object v1, p0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->minDistanceLocation:[Lcom/vividsolutions/jts/operation/distance/GeometryLocation;

    aget-object v1, v1, v2

    invoke-virtual {v1}, Lcom/vividsolutions/jts/operation/distance/GeometryLocation;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->minDistanceLocation:[Lcom/vividsolutions/jts/operation/distance/GeometryLocation;

    aget-object v1, v1, v3

    invoke-virtual {v1}, Lcom/vividsolutions/jts/operation/distance/GeometryLocation;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    aput-object v1, v0, v3

    .line 179
    .local v0, "nearestPts":[Lcom/vividsolutions/jts/geom/Coordinate;
    return-object v0
.end method

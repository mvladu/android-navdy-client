.class public Lcom/vividsolutions/jts/operation/distance/ConnectedElementPointFilter;
.super Ljava/lang/Object;
.source "ConnectedElementPointFilter.java"

# interfaces
.implements Lcom/vividsolutions/jts/geom/GeometryFilter;


# instance fields
.field private pts:Ljava/util/List;


# direct methods
.method constructor <init>(Ljava/util/List;)V
    .locals 0
    .param p1, "pts"    # Ljava/util/List;

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    iput-object p1, p0, Lcom/vividsolutions/jts/operation/distance/ConnectedElementPointFilter;->pts:Ljava/util/List;

    .line 68
    return-void
.end method

.method public static getCoordinates(Lcom/vividsolutions/jts/geom/Geometry;)Ljava/util/List;
    .locals 2
    .param p0, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 58
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 59
    .local v0, "pts":Ljava/util/List;
    new-instance v1, Lcom/vividsolutions/jts/operation/distance/ConnectedElementPointFilter;

    invoke-direct {v1, v0}, Lcom/vividsolutions/jts/operation/distance/ConnectedElementPointFilter;-><init>(Ljava/util/List;)V

    invoke-virtual {p0, v1}, Lcom/vividsolutions/jts/geom/Geometry;->apply(Lcom/vividsolutions/jts/geom/GeometryFilter;)V

    .line 60
    return-object v0
.end method


# virtual methods
.method public filter(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 2
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 72
    instance-of v0, p1, Lcom/vividsolutions/jts/geom/Point;

    if-nez v0, :cond_0

    instance-of v0, p1, Lcom/vividsolutions/jts/geom/LineString;

    if-nez v0, :cond_0

    instance-of v0, p1, Lcom/vividsolutions/jts/geom/Polygon;

    if-eqz v0, :cond_1

    .line 75
    :cond_0
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/distance/ConnectedElementPointFilter;->pts:Ljava/util/List;

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 76
    :cond_1
    return-void
.end method

.class public Lcom/vividsolutions/jts/algorithm/PointLocator;
.super Ljava/lang/Object;
.source "PointLocator.java"


# instance fields
.field private boundaryRule:Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;

.field private isIn:Z

.field private numBoundaries:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    sget-object v0, Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;->OGC_SFS_BOUNDARY_RULE:Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;

    iput-object v0, p0, Lcom/vividsolutions/jts/algorithm/PointLocator;->boundaryRule:Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;

    .line 64
    return-void
.end method

.method public constructor <init>(Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;)V
    .locals 2
    .param p1, "boundaryRule"    # Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    sget-object v0, Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;->OGC_SFS_BOUNDARY_RULE:Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;

    iput-object v0, p0, Lcom/vividsolutions/jts/algorithm/PointLocator;->boundaryRule:Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;

    .line 68
    if-nez p1, :cond_0

    .line 69
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Rule must be non-null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 70
    :cond_0
    iput-object p1, p0, Lcom/vividsolutions/jts/algorithm/PointLocator;->boundaryRule:Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;

    .line 71
    return-void
.end method

.method private computeLocation(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 8
    .param p1, "p"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 119
    instance-of v7, p2, Lcom/vividsolutions/jts/geom/Point;

    if-eqz v7, :cond_0

    move-object v7, p2

    .line 120
    check-cast v7, Lcom/vividsolutions/jts/geom/Point;

    invoke-direct {p0, p1, v7}, Lcom/vividsolutions/jts/algorithm/PointLocator;->locate(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Point;)I

    move-result v7

    invoke-direct {p0, v7}, Lcom/vividsolutions/jts/algorithm/PointLocator;->updateLocationInfo(I)V

    .line 122
    :cond_0
    instance-of v7, p2, Lcom/vividsolutions/jts/geom/LineString;

    if-eqz v7, :cond_2

    .line 123
    check-cast p2, Lcom/vividsolutions/jts/geom/LineString;

    .end local p2    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, p1, p2}, Lcom/vividsolutions/jts/algorithm/PointLocator;->locate(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/LineString;)I

    move-result v7

    invoke-direct {p0, v7}, Lcom/vividsolutions/jts/algorithm/PointLocator;->updateLocationInfo(I)V

    .line 150
    :cond_1
    :goto_0
    return-void

    .line 125
    .restart local p2    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_2
    instance-of v7, p2, Lcom/vividsolutions/jts/geom/Polygon;

    if-eqz v7, :cond_3

    .line 126
    check-cast p2, Lcom/vividsolutions/jts/geom/Polygon;

    .end local p2    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, p1, p2}, Lcom/vividsolutions/jts/algorithm/PointLocator;->locate(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Polygon;)I

    move-result v7

    invoke-direct {p0, v7}, Lcom/vividsolutions/jts/algorithm/PointLocator;->updateLocationInfo(I)V

    goto :goto_0

    .line 128
    .restart local p2    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_3
    instance-of v7, p2, Lcom/vividsolutions/jts/geom/MultiLineString;

    if-eqz v7, :cond_4

    move-object v4, p2

    .line 129
    check-cast v4, Lcom/vividsolutions/jts/geom/MultiLineString;

    .line 130
    .local v4, "ml":Lcom/vividsolutions/jts/geom/MultiLineString;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-virtual {v4}, Lcom/vividsolutions/jts/geom/MultiLineString;->getNumGeometries()I

    move-result v7

    if-ge v2, v7, :cond_1

    .line 131
    invoke-virtual {v4, v2}, Lcom/vividsolutions/jts/geom/MultiLineString;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v3

    check-cast v3, Lcom/vividsolutions/jts/geom/LineString;

    .line 132
    .local v3, "l":Lcom/vividsolutions/jts/geom/LineString;
    invoke-direct {p0, p1, v3}, Lcom/vividsolutions/jts/algorithm/PointLocator;->locate(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/LineString;)I

    move-result v7

    invoke-direct {p0, v7}, Lcom/vividsolutions/jts/algorithm/PointLocator;->updateLocationInfo(I)V

    .line 130
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 135
    .end local v2    # "i":I
    .end local v3    # "l":Lcom/vividsolutions/jts/geom/LineString;
    .end local v4    # "ml":Lcom/vividsolutions/jts/geom/MultiLineString;
    :cond_4
    instance-of v7, p2, Lcom/vividsolutions/jts/geom/MultiPolygon;

    if-eqz v7, :cond_5

    move-object v5, p2

    .line 136
    check-cast v5, Lcom/vividsolutions/jts/geom/MultiPolygon;

    .line 137
    .local v5, "mpoly":Lcom/vividsolutions/jts/geom/MultiPolygon;
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_2
    invoke-virtual {v5}, Lcom/vividsolutions/jts/geom/MultiPolygon;->getNumGeometries()I

    move-result v7

    if-ge v2, v7, :cond_1

    .line 138
    invoke-virtual {v5, v2}, Lcom/vividsolutions/jts/geom/MultiPolygon;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v6

    check-cast v6, Lcom/vividsolutions/jts/geom/Polygon;

    .line 139
    .local v6, "poly":Lcom/vividsolutions/jts/geom/Polygon;
    invoke-direct {p0, p1, v6}, Lcom/vividsolutions/jts/algorithm/PointLocator;->locate(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Polygon;)I

    move-result v7

    invoke-direct {p0, v7}, Lcom/vividsolutions/jts/algorithm/PointLocator;->updateLocationInfo(I)V

    .line 137
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 142
    .end local v2    # "i":I
    .end local v5    # "mpoly":Lcom/vividsolutions/jts/geom/MultiPolygon;
    .end local v6    # "poly":Lcom/vividsolutions/jts/geom/Polygon;
    :cond_5
    instance-of v7, p2, Lcom/vividsolutions/jts/geom/GeometryCollection;

    if-eqz v7, :cond_1

    .line 143
    new-instance v1, Lcom/vividsolutions/jts/geom/GeometryCollectionIterator;

    move-object v7, p2

    check-cast v7, Lcom/vividsolutions/jts/geom/GeometryCollection;

    invoke-direct {v1, v7}, Lcom/vividsolutions/jts/geom/GeometryCollectionIterator;-><init>(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 144
    .local v1, "geomi":Ljava/util/Iterator;
    :cond_6
    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 145
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geom/Geometry;

    .line 146
    .local v0, "g2":Lcom/vividsolutions/jts/geom/Geometry;
    if-eq v0, p2, :cond_6

    .line 147
    invoke-direct {p0, p1, v0}, Lcom/vividsolutions/jts/algorithm/PointLocator;->computeLocation(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Geometry;)V

    goto :goto_3
.end method

.method private locate(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/LineString;)I
    .locals 4
    .param p1, "p"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "l"    # Lcom/vividsolutions/jts/geom/LineString;

    .prologue
    const/4 v1, 0x2

    const/4 v2, 0x0

    .line 171
    invoke-virtual {p2}, Lcom/vividsolutions/jts/geom/LineString;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/vividsolutions/jts/geom/Envelope;->intersects(Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 182
    :cond_0
    :goto_0
    return v1

    .line 173
    :cond_1
    invoke-virtual {p2}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    .line 174
    .local v0, "pt":[Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-virtual {p2}, Lcom/vividsolutions/jts/geom/LineString;->isClosed()Z

    move-result v3

    if-nez v3, :cond_3

    .line 175
    aget-object v3, v0, v2

    invoke-virtual {p1, v3}, Lcom/vividsolutions/jts/geom/Coordinate;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    array-length v3, v0

    add-int/lit8 v3, v3, -0x1

    aget-object v3, v0, v3

    invoke-virtual {p1, v3}, Lcom/vividsolutions/jts/geom/Coordinate;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 177
    :cond_2
    const/4 v1, 0x1

    goto :goto_0

    .line 180
    :cond_3
    invoke-static {p1, v0}, Lcom/vividsolutions/jts/algorithm/CGAlgorithms;->isOnLine(Lcom/vividsolutions/jts/geom/Coordinate;[Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v1, v2

    .line 181
    goto :goto_0
.end method

.method private locate(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Point;)I
    .locals 2
    .param p1, "p"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "pt"    # Lcom/vividsolutions/jts/geom/Point;

    .prologue
    .line 162
    invoke-virtual {p2}, Lcom/vividsolutions/jts/geom/Point;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    .line 163
    .local v0, "ptCoord":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/geom/Coordinate;->equals2D(Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 164
    const/4 v1, 0x0

    .line 165
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x2

    goto :goto_0
.end method

.method private locate(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Polygon;)I
    .locals 8
    .param p1, "p"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "poly"    # Lcom/vividsolutions/jts/geom/Polygon;

    .prologue
    const/4 v5, 0x2

    const/4 v6, 0x1

    .line 195
    invoke-virtual {p2}, Lcom/vividsolutions/jts/geom/Polygon;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 209
    :cond_0
    :goto_0
    return v5

    .line 197
    :cond_1
    invoke-virtual {p2}, Lcom/vividsolutions/jts/geom/Polygon;->getExteriorRing()Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v3

    check-cast v3, Lcom/vividsolutions/jts/geom/LinearRing;

    .line 199
    .local v3, "shell":Lcom/vividsolutions/jts/geom/LinearRing;
    invoke-direct {p0, p1, v3}, Lcom/vividsolutions/jts/algorithm/PointLocator;->locateInPolygonRing(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/LinearRing;)I

    move-result v4

    .line 200
    .local v4, "shellLoc":I
    if-eq v4, v5, :cond_0

    .line 201
    if-ne v4, v6, :cond_2

    move v5, v6

    goto :goto_0

    .line 203
    :cond_2
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-virtual {p2}, Lcom/vividsolutions/jts/geom/Polygon;->getNumInteriorRing()I

    move-result v7

    if-ge v2, v7, :cond_4

    .line 204
    invoke-virtual {p2, v2}, Lcom/vividsolutions/jts/geom/Polygon;->getInteriorRingN(I)Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geom/LinearRing;

    .line 205
    .local v0, "hole":Lcom/vividsolutions/jts/geom/LinearRing;
    invoke-direct {p0, p1, v0}, Lcom/vividsolutions/jts/algorithm/PointLocator;->locateInPolygonRing(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/LinearRing;)I

    move-result v1

    .line 206
    .local v1, "holeLoc":I
    if-eqz v1, :cond_0

    .line 207
    if-ne v1, v6, :cond_3

    move v5, v6

    goto :goto_0

    .line 203
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 209
    .end local v0    # "hole":Lcom/vividsolutions/jts/geom/LinearRing;
    .end local v1    # "holeLoc":I
    :cond_4
    const/4 v5, 0x0

    goto :goto_0
.end method

.method private locateInPolygonRing(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/LinearRing;)I
    .locals 1
    .param p1, "p"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "ring"    # Lcom/vividsolutions/jts/geom/LinearRing;

    .prologue
    .line 188
    invoke-virtual {p2}, Lcom/vividsolutions/jts/geom/LinearRing;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/geom/Envelope;->intersects(Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x2

    .line 190
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p2}, Lcom/vividsolutions/jts/geom/LinearRing;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/vividsolutions/jts/algorithm/CGAlgorithms;->locatePointInRing(Lcom/vividsolutions/jts/geom/Coordinate;[Lcom/vividsolutions/jts/geom/Coordinate;)I

    move-result v0

    goto :goto_0
.end method

.method private updateLocationInfo(I)V
    .locals 1
    .param p1, "loc"    # I

    .prologue
    const/4 v0, 0x1

    .line 154
    if-nez p1, :cond_0

    iput-boolean v0, p0, Lcom/vividsolutions/jts/algorithm/PointLocator;->isIn:Z

    .line 155
    :cond_0
    if-ne p1, v0, :cond_1

    iget v0, p0, Lcom/vividsolutions/jts/algorithm/PointLocator;->numBoundaries:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/vividsolutions/jts/algorithm/PointLocator;->numBoundaries:I

    .line 156
    :cond_1
    return-void
.end method


# virtual methods
.method public intersects(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 2
    .param p1, "p"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 82
    invoke-virtual {p0, p1, p2}, Lcom/vividsolutions/jts/algorithm/PointLocator;->locate(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Geometry;)I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public locate(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Geometry;)I
    .locals 4
    .param p1, "p"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x0

    .line 97
    invoke-virtual {p2}, Lcom/vividsolutions/jts/geom/Geometry;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 114
    .end local p2    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_0
    :goto_0
    return v0

    .line 99
    .restart local p2    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_1
    instance-of v2, p2, Lcom/vividsolutions/jts/geom/LineString;

    if-eqz v2, :cond_2

    .line 100
    check-cast p2, Lcom/vividsolutions/jts/geom/LineString;

    .end local p2    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, p1, p2}, Lcom/vividsolutions/jts/algorithm/PointLocator;->locate(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/LineString;)I

    move-result v0

    goto :goto_0

    .line 102
    .restart local p2    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_2
    instance-of v2, p2, Lcom/vividsolutions/jts/geom/Polygon;

    if-eqz v2, :cond_3

    .line 103
    check-cast p2, Lcom/vividsolutions/jts/geom/Polygon;

    .end local p2    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, p1, p2}, Lcom/vividsolutions/jts/algorithm/PointLocator;->locate(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Polygon;)I

    move-result v0

    goto :goto_0

    .line 106
    .restart local p2    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_3
    iput-boolean v1, p0, Lcom/vividsolutions/jts/algorithm/PointLocator;->isIn:Z

    .line 107
    iput v1, p0, Lcom/vividsolutions/jts/algorithm/PointLocator;->numBoundaries:I

    .line 108
    invoke-direct {p0, p1, p2}, Lcom/vividsolutions/jts/algorithm/PointLocator;->computeLocation(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 109
    iget-object v2, p0, Lcom/vividsolutions/jts/algorithm/PointLocator;->boundaryRule:Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;

    iget v3, p0, Lcom/vividsolutions/jts/algorithm/PointLocator;->numBoundaries:I

    invoke-interface {v2, v3}, Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;->isInBoundary(I)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 110
    const/4 v0, 0x1

    goto :goto_0

    .line 111
    :cond_4
    iget v2, p0, Lcom/vividsolutions/jts/algorithm/PointLocator;->numBoundaries:I

    if-gtz v2, :cond_5

    iget-boolean v2, p0, Lcom/vividsolutions/jts/algorithm/PointLocator;->isIn:Z

    if-eqz v2, :cond_0

    :cond_5
    move v0, v1

    .line 112
    goto :goto_0
.end method

.class public Lcom/vividsolutions/jts/algorithm/MinimumDiameter;
.super Ljava/lang/Object;
.source "MinimumDiameter.java"


# instance fields
.field private convexHullPts:[Lcom/vividsolutions/jts/geom/Coordinate;

.field private final inputGeom:Lcom/vividsolutions/jts/geom/Geometry;

.field private final isConvex:Z

.field private minBaseSeg:Lcom/vividsolutions/jts/geom/LineSegment;

.field private minPtIndex:I

.field private minWidth:D

.field private minWidthPt:Lcom/vividsolutions/jts/geom/Coordinate;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 1
    .param p1, "inputGeom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 81
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;-><init>(Lcom/vividsolutions/jts/geom/Geometry;Z)V

    .line 82
    return-void
.end method

.method public constructor <init>(Lcom/vividsolutions/jts/geom/Geometry;Z)V
    .locals 2
    .param p1, "inputGeom"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "isConvex"    # Z

    .prologue
    const/4 v1, 0x0

    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    iput-object v1, p0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->convexHullPts:[Lcom/vividsolutions/jts/geom/Coordinate;

    .line 69
    new-instance v0, Lcom/vividsolutions/jts/geom/LineSegment;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/LineSegment;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->minBaseSeg:Lcom/vividsolutions/jts/geom/LineSegment;

    .line 70
    iput-object v1, p0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->minWidthPt:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 72
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->minWidth:D

    .line 96
    iput-object p1, p0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->inputGeom:Lcom/vividsolutions/jts/geom/Geometry;

    .line 97
    iput-boolean p2, p0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->isConvex:Z

    .line 98
    return-void
.end method

.method private static computeC(DDLcom/vividsolutions/jts/geom/Coordinate;)D
    .locals 4
    .param p0, "a"    # D
    .param p2, "b"    # D
    .param p4, "p"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 317
    iget-wide v0, p4, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    mul-double/2addr v0, p0

    iget-wide v2, p4, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    mul-double/2addr v2, p2

    sub-double/2addr v0, v2

    return-wide v0
.end method

.method private computeConvexRingMinDiameter([Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 6
    .param p1, "pts"    # [Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 203
    const-wide v4, 0x7fefffffffffffffL    # Double.MAX_VALUE

    iput-wide v4, p0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->minWidth:D

    .line 204
    const/4 v0, 0x1

    .line 206
    .local v0, "currMaxIndex":I
    new-instance v2, Lcom/vividsolutions/jts/geom/LineSegment;

    invoke-direct {v2}, Lcom/vividsolutions/jts/geom/LineSegment;-><init>()V

    .line 208
    .local v2, "seg":Lcom/vividsolutions/jts/geom/LineSegment;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v3, p1

    add-int/lit8 v3, v3, -0x1

    if-ge v1, v3, :cond_0

    .line 209
    aget-object v3, p1, v1

    iput-object v3, v2, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 210
    add-int/lit8 v3, v1, 0x1

    aget-object v3, p1, v3

    iput-object v3, v2, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 211
    invoke-direct {p0, p1, v2, v0}, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->findMaxPerpDistance([Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/LineSegment;I)I

    move-result v0

    .line 208
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 213
    :cond_0
    return-void
.end method

.method private computeMinimumDiameter()V
    .locals 3

    .prologue
    .line 153
    iget-object v1, p0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->minWidthPt:Lcom/vividsolutions/jts/geom/Coordinate;

    if-eqz v1, :cond_0

    .line 162
    :goto_0
    return-void

    .line 156
    :cond_0
    iget-boolean v1, p0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->isConvex:Z

    if-eqz v1, :cond_1

    .line 157
    iget-object v1, p0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->inputGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-direct {p0, v1}, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->computeWidthConvex(Lcom/vividsolutions/jts/geom/Geometry;)V

    goto :goto_0

    .line 159
    :cond_1
    new-instance v1, Lcom/vividsolutions/jts/algorithm/ConvexHull;

    iget-object v2, p0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->inputGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-direct {v1, v2}, Lcom/vividsolutions/jts/algorithm/ConvexHull;-><init>(Lcom/vividsolutions/jts/geom/Geometry;)V

    invoke-virtual {v1}, Lcom/vividsolutions/jts/algorithm/ConvexHull;->getConvexHull()Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    .line 160
    .local v0, "convexGeom":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, v0}, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->computeWidthConvex(Lcom/vividsolutions/jts/geom/Geometry;)V

    goto :goto_0
.end method

.method private static computeSegmentForLine(DDD)Lcom/vividsolutions/jts/geom/LineSegment;
    .locals 10
    .param p0, "a"    # D
    .param p2, "b"    # D
    .param p4, "c"    # D

    .prologue
    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    .line 329
    invoke-static {p2, p3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    invoke-static {p0, p1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v4

    cmpl-double v2, v2, v4

    if-lez v2, :cond_0

    .line 330
    new-instance v0, Lcom/vividsolutions/jts/geom/Coordinate;

    div-double v2, p4, p2

    invoke-direct {v0, v6, v7, v2, v3}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DD)V

    .line 331
    .local v0, "p0":Lcom/vividsolutions/jts/geom/Coordinate;
    new-instance v1, Lcom/vividsolutions/jts/geom/Coordinate;

    div-double v2, p4, p2

    div-double v4, p0, p2

    sub-double/2addr v2, v4

    invoke-direct {v1, v8, v9, v2, v3}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DD)V

    .line 337
    .local v1, "p1":Lcom/vividsolutions/jts/geom/Coordinate;
    :goto_0
    new-instance v2, Lcom/vividsolutions/jts/geom/LineSegment;

    invoke-direct {v2, v0, v1}, Lcom/vividsolutions/jts/geom/LineSegment;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    return-object v2

    .line 334
    .end local v0    # "p0":Lcom/vividsolutions/jts/geom/Coordinate;
    .end local v1    # "p1":Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_0
    new-instance v0, Lcom/vividsolutions/jts/geom/Coordinate;

    div-double v2, p4, p0

    invoke-direct {v0, v2, v3, v6, v7}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DD)V

    .line 335
    .restart local v0    # "p0":Lcom/vividsolutions/jts/geom/Coordinate;
    new-instance v1, Lcom/vividsolutions/jts/geom/Coordinate;

    div-double v2, p4, p0

    div-double v4, p2, p0

    sub-double/2addr v2, v4

    invoke-direct {v1, v2, v3, v8, v9}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DD)V

    .restart local v1    # "p1":Lcom/vividsolutions/jts/geom/Coordinate;
    goto :goto_0
.end method

.method private computeWidthConvex(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 6
    .param p1, "convexGeom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x1

    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    .line 167
    instance-of v0, p1, Lcom/vividsolutions/jts/geom/Polygon;

    if-eqz v0, :cond_0

    .line 168
    check-cast p1, Lcom/vividsolutions/jts/geom/Polygon;

    .end local p1    # "convexGeom":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Polygon;->getExteriorRing()Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    iput-object v0, p0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->convexHullPts:[Lcom/vividsolutions/jts/geom/Coordinate;

    .line 173
    :goto_0
    iget-object v0, p0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->convexHullPts:[Lcom/vividsolutions/jts/geom/Coordinate;

    array-length v0, v0

    if-nez v0, :cond_1

    .line 174
    iput-wide v4, p0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->minWidth:D

    .line 175
    iput-object v1, p0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->minWidthPt:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 176
    iput-object v1, p0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->minBaseSeg:Lcom/vividsolutions/jts/geom/LineSegment;

    .line 192
    :goto_1
    return-void

    .line 170
    .restart local p1    # "convexGeom":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_0
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    iput-object v0, p0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->convexHullPts:[Lcom/vividsolutions/jts/geom/Coordinate;

    goto :goto_0

    .line 178
    .end local p1    # "convexGeom":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_1
    iget-object v0, p0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->convexHullPts:[Lcom/vividsolutions/jts/geom/Coordinate;

    array-length v0, v0

    if-ne v0, v3, :cond_2

    .line 179
    iput-wide v4, p0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->minWidth:D

    .line 180
    iget-object v0, p0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->convexHullPts:[Lcom/vividsolutions/jts/geom/Coordinate;

    aget-object v0, v0, v2

    iput-object v0, p0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->minWidthPt:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 181
    iget-object v0, p0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->minBaseSeg:Lcom/vividsolutions/jts/geom/LineSegment;

    iget-object v1, p0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->convexHullPts:[Lcom/vividsolutions/jts/geom/Coordinate;

    aget-object v1, v1, v2

    iput-object v1, v0, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 182
    iget-object v0, p0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->minBaseSeg:Lcom/vividsolutions/jts/geom/LineSegment;

    iget-object v1, p0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->convexHullPts:[Lcom/vividsolutions/jts/geom/Coordinate;

    aget-object v1, v1, v2

    iput-object v1, v0, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    goto :goto_1

    .line 184
    :cond_2
    iget-object v0, p0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->convexHullPts:[Lcom/vividsolutions/jts/geom/Coordinate;

    array-length v0, v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->convexHullPts:[Lcom/vividsolutions/jts/geom/Coordinate;

    array-length v0, v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_4

    .line 185
    :cond_3
    iput-wide v4, p0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->minWidth:D

    .line 186
    iget-object v0, p0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->convexHullPts:[Lcom/vividsolutions/jts/geom/Coordinate;

    aget-object v0, v0, v2

    iput-object v0, p0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->minWidthPt:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 187
    iget-object v0, p0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->minBaseSeg:Lcom/vividsolutions/jts/geom/LineSegment;

    iget-object v1, p0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->convexHullPts:[Lcom/vividsolutions/jts/geom/Coordinate;

    aget-object v1, v1, v2

    iput-object v1, v0, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 188
    iget-object v0, p0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->minBaseSeg:Lcom/vividsolutions/jts/geom/LineSegment;

    iget-object v1, p0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->convexHullPts:[Lcom/vividsolutions/jts/geom/Coordinate;

    aget-object v1, v1, v3

    iput-object v1, v0, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    goto :goto_1

    .line 191
    :cond_4
    iget-object v0, p0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->convexHullPts:[Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {p0, v0}, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->computeConvexRingMinDiameter([Lcom/vividsolutions/jts/geom/Coordinate;)V

    goto :goto_1
.end method

.method private findMaxPerpDistance([Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/LineSegment;I)I
    .locals 8
    .param p1, "pts"    # [Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "seg"    # Lcom/vividsolutions/jts/geom/LineSegment;
    .param p3, "startIndex"    # I

    .prologue
    .line 217
    aget-object v6, p1, p3

    invoke-virtual {p2, v6}, Lcom/vividsolutions/jts/geom/LineSegment;->distancePerpendicular(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v2

    .line 218
    .local v2, "maxPerpDistance":D
    move-wide v4, v2

    .line 219
    .local v4, "nextPerpDistance":D
    move v0, p3

    .line 220
    .local v0, "maxIndex":I
    move v1, v0

    .line 221
    .local v1, "nextIndex":I
    :goto_0
    cmpl-double v6, v4, v2

    if-ltz v6, :cond_0

    .line 222
    move-wide v2, v4

    .line 223
    move v0, v1

    .line 225
    invoke-static {p1, v0}, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->nextIndex([Lcom/vividsolutions/jts/geom/Coordinate;I)I

    move-result v1

    .line 226
    aget-object v6, p1, v1

    invoke-virtual {p2, v6}, Lcom/vividsolutions/jts/geom/LineSegment;->distancePerpendicular(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v4

    goto :goto_0

    .line 229
    :cond_0
    iget-wide v6, p0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->minWidth:D

    cmpg-double v6, v2, v6

    if-gez v6, :cond_1

    .line 230
    iput v0, p0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->minPtIndex:I

    .line 231
    iput-wide v2, p0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->minWidth:D

    .line 232
    iget v6, p0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->minPtIndex:I

    aget-object v6, p1, v6

    iput-object v6, p0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->minWidthPt:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 233
    new-instance v6, Lcom/vividsolutions/jts/geom/LineSegment;

    invoke-direct {v6, p2}, Lcom/vividsolutions/jts/geom/LineSegment;-><init>(Lcom/vividsolutions/jts/geom/LineSegment;)V

    iput-object v6, p0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->minBaseSeg:Lcom/vividsolutions/jts/geom/LineSegment;

    .line 237
    :cond_1
    return v0
.end method

.method private static nextIndex([Lcom/vividsolutions/jts/geom/Coordinate;I)I
    .locals 1
    .param p0, "pts"    # [Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "index"    # I

    .prologue
    .line 242
    add-int/lit8 p1, p1, 0x1

    .line 243
    array-length v0, p0

    if-lt p1, v0, :cond_0

    const/4 p1, 0x0

    .line 244
    :cond_0
    return p1
.end method


# virtual methods
.method public getDiameter()Lcom/vividsolutions/jts/geom/LineString;
    .locals 5

    .prologue
    .line 140
    invoke-direct {p0}, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->computeMinimumDiameter()V

    .line 143
    iget-object v1, p0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->minWidthPt:Lcom/vividsolutions/jts/geom/Coordinate;

    if-nez v1, :cond_0

    .line 144
    iget-object v1, p0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->inputGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/Geometry;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v2

    const/4 v1, 0x0

    check-cast v1, [Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {v2, v1}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createLineString([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v1

    .line 147
    :goto_0
    return-object v1

    .line 146
    :cond_0
    iget-object v1, p0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->minBaseSeg:Lcom/vividsolutions/jts/geom/LineSegment;

    iget-object v2, p0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->minWidthPt:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {v1, v2}, Lcom/vividsolutions/jts/geom/LineSegment;->project(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    .line 147
    .local v0, "basePt":Lcom/vividsolutions/jts/geom/Coordinate;
    iget-object v1, p0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->inputGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/Geometry;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Lcom/vividsolutions/jts/geom/Coordinate;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->minWidthPt:Lcom/vividsolutions/jts/geom/Coordinate;

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createLineString([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v1

    goto :goto_0
.end method

.method public getLength()D
    .locals 2

    .prologue
    .line 107
    invoke-direct {p0}, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->computeMinimumDiameter()V

    .line 108
    iget-wide v0, p0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->minWidth:D

    return-wide v0
.end method

.method public getMinimumRectangle()Lcom/vividsolutions/jts/geom/Geometry;
    .locals 44

    .prologue
    .line 261
    invoke-direct/range {p0 .. p0}, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->computeMinimumDiameter()V

    .line 264
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->minWidth:D

    const-wide/16 v6, 0x0

    cmpl-double v4, v4, v6

    if-nez v4, :cond_1

    .line 265
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->minBaseSeg:Lcom/vividsolutions/jts/geom/LineSegment;

    iget-object v4, v4, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->minBaseSeg:Lcom/vividsolutions/jts/geom/LineSegment;

    iget-object v5, v5, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {v4, v5}, Lcom/vividsolutions/jts/geom/Coordinate;->equals2D(Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 266
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->inputGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v4}, Lcom/vividsolutions/jts/geom/Geometry;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->minBaseSeg:Lcom/vividsolutions/jts/geom/LineSegment;

    iget-object v5, v5, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {v4, v5}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createPoint(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Point;

    move-result-object v4

    .line 311
    :goto_0
    return-object v4

    .line 268
    :cond_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->minBaseSeg:Lcom/vividsolutions/jts/geom/LineSegment;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->inputGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v5}, Lcom/vividsolutions/jts/geom/Geometry;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vividsolutions/jts/geom/LineSegment;->toGeometry(Lcom/vividsolutions/jts/geom/GeometryFactory;)Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v4

    goto :goto_0

    .line 272
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->minBaseSeg:Lcom/vividsolutions/jts/geom/LineSegment;

    iget-object v4, v4, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v4, v4, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->minBaseSeg:Lcom/vividsolutions/jts/geom/LineSegment;

    iget-object v6, v6, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v6, v6, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    sub-double v18, v4, v6

    .line 273
    .local v18, "dx":D
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->minBaseSeg:Lcom/vividsolutions/jts/geom/LineSegment;

    iget-object v4, v4, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v4, v4, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->minBaseSeg:Lcom/vividsolutions/jts/geom/LineSegment;

    iget-object v6, v6, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v6, v6, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    sub-double v28, v4, v6

    .line 280
    .local v28, "dy":D
    const-wide v26, 0x7fefffffffffffffL    # Double.MAX_VALUE

    .line 281
    .local v26, "minPara":D
    const-wide v20, -0x10000000000001L

    .line 282
    .local v20, "maxPara":D
    const-wide v14, 0x7fefffffffffffffL    # Double.MAX_VALUE

    .line 283
    .local v14, "minPerp":D
    const-wide v8, -0x10000000000001L

    .line 286
    .local v8, "maxPerp":D
    const/16 v30, 0x0

    .local v30, "i":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->convexHullPts:[Lcom/vividsolutions/jts/geom/Coordinate;

    array-length v4, v4

    move/from16 v0, v30

    if-ge v0, v4, :cond_6

    .line 288
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->convexHullPts:[Lcom/vividsolutions/jts/geom/Coordinate;

    aget-object v4, v4, v30

    move-wide/from16 v0, v18

    move-wide/from16 v2, v28

    invoke-static {v0, v1, v2, v3, v4}, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->computeC(DDLcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v40

    .line 289
    .local v40, "paraC":D
    cmpl-double v4, v40, v20

    if-lez v4, :cond_2

    move-wide/from16 v20, v40

    .line 290
    :cond_2
    cmpg-double v4, v40, v26

    if-gez v4, :cond_3

    move-wide/from16 v26, v40

    .line 292
    :cond_3
    move-wide/from16 v0, v28

    neg-double v4, v0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->convexHullPts:[Lcom/vividsolutions/jts/geom/Coordinate;

    aget-object v6, v6, v30

    move-wide/from16 v0, v18

    invoke-static {v4, v5, v0, v1, v6}, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->computeC(DDLcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v42

    .line 293
    .local v42, "perpC":D
    cmpl-double v4, v42, v8

    if-lez v4, :cond_4

    move-wide/from16 v8, v42

    .line 294
    :cond_4
    cmpg-double v4, v42, v14

    if-gez v4, :cond_5

    move-wide/from16 v14, v42

    .line 286
    :cond_5
    add-int/lit8 v30, v30, 0x1

    goto :goto_1

    .line 298
    .end local v40    # "paraC":D
    .end local v42    # "perpC":D
    :cond_6
    move-wide/from16 v0, v18

    neg-double v4, v0

    move-wide/from16 v0, v28

    neg-double v6, v0

    invoke-static/range {v4 .. v9}, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->computeSegmentForLine(DDD)Lcom/vividsolutions/jts/geom/LineSegment;

    move-result-object v32

    .line 299
    .local v32, "maxPerpLine":Lcom/vividsolutions/jts/geom/LineSegment;
    move-wide/from16 v0, v18

    neg-double v10, v0

    move-wide/from16 v0, v28

    neg-double v12, v0

    invoke-static/range {v10 .. v15}, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->computeSegmentForLine(DDD)Lcom/vividsolutions/jts/geom/LineSegment;

    move-result-object v34

    .line 300
    .local v34, "minPerpLine":Lcom/vividsolutions/jts/geom/LineSegment;
    move-wide/from16 v0, v28

    neg-double v0, v0

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v21}, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->computeSegmentForLine(DDD)Lcom/vividsolutions/jts/geom/LineSegment;

    move-result-object v31

    .line 301
    .local v31, "maxParaLine":Lcom/vividsolutions/jts/geom/LineSegment;
    move-wide/from16 v0, v28

    neg-double v0, v0

    move-wide/from16 v22, v0

    move-wide/from16 v24, v18

    invoke-static/range {v22 .. v27}, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->computeSegmentForLine(DDD)Lcom/vividsolutions/jts/geom/LineSegment;

    move-result-object v33

    .line 304
    .local v33, "minParaLine":Lcom/vividsolutions/jts/geom/LineSegment;
    invoke-virtual/range {v31 .. v32}, Lcom/vividsolutions/jts/geom/LineSegment;->lineIntersection(Lcom/vividsolutions/jts/geom/LineSegment;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v35

    .line 305
    .local v35, "p0":Lcom/vividsolutions/jts/geom/Coordinate;
    move-object/from16 v0, v33

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/LineSegment;->lineIntersection(Lcom/vividsolutions/jts/geom/LineSegment;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v36

    .line 306
    .local v36, "p1":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-virtual/range {v33 .. v34}, Lcom/vividsolutions/jts/geom/LineSegment;->lineIntersection(Lcom/vividsolutions/jts/geom/LineSegment;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v37

    .line 307
    .local v37, "p2":Lcom/vividsolutions/jts/geom/Coordinate;
    move-object/from16 v0, v31

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/LineSegment;->lineIntersection(Lcom/vividsolutions/jts/geom/LineSegment;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v38

    .line 309
    .local v38, "p3":Lcom/vividsolutions/jts/geom/Coordinate;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->inputGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v4}, Lcom/vividsolutions/jts/geom/Geometry;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v4

    const/4 v5, 0x5

    new-array v5, v5, [Lcom/vividsolutions/jts/geom/Coordinate;

    const/4 v6, 0x0

    aput-object v35, v5, v6

    const/4 v6, 0x1

    aput-object v36, v5, v6

    const/4 v6, 0x2

    aput-object v37, v5, v6

    const/4 v6, 0x3

    aput-object v38, v5, v6

    const/4 v6, 0x4

    aput-object v35, v5, v6

    invoke-virtual {v4, v5}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createLinearRing([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/LinearRing;

    move-result-object v39

    .line 311
    .local v39, "shell":Lcom/vividsolutions/jts/geom/LinearRing;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->inputGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v4}, Lcom/vividsolutions/jts/geom/Geometry;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v4

    const/4 v5, 0x0

    move-object/from16 v0, v39

    invoke-virtual {v4, v0, v5}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createPolygon(Lcom/vividsolutions/jts/geom/LinearRing;[Lcom/vividsolutions/jts/geom/LinearRing;)Lcom/vividsolutions/jts/geom/Polygon;

    move-result-object v4

    goto/16 :goto_0
.end method

.method public getSupportingSegment()Lcom/vividsolutions/jts/geom/LineString;
    .locals 4

    .prologue
    .line 129
    invoke-direct {p0}, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->computeMinimumDiameter()V

    .line 130
    iget-object v0, p0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->inputGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/Geometry;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Lcom/vividsolutions/jts/geom/Coordinate;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->minBaseSeg:Lcom/vividsolutions/jts/geom/LineSegment;

    iget-object v3, v3, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->minBaseSeg:Lcom/vividsolutions/jts/geom/LineSegment;

    iget-object v3, v3, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createLineString([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v0

    return-object v0
.end method

.method public getWidthCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 1

    .prologue
    .line 118
    invoke-direct {p0}, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->computeMinimumDiameter()V

    .line 119
    iget-object v0, p0, Lcom/vividsolutions/jts/algorithm/MinimumDiameter;->minWidthPt:Lcom/vividsolutions/jts/geom/Coordinate;

    return-object v0
.end method

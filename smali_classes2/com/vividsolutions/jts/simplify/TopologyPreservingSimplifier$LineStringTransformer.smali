.class Lcom/vividsolutions/jts/simplify/TopologyPreservingSimplifier$LineStringTransformer;
.super Lcom/vividsolutions/jts/geom/util/GeometryTransformer;
.source "TopologyPreservingSimplifier.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vividsolutions/jts/simplify/TopologyPreservingSimplifier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "LineStringTransformer"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vividsolutions/jts/simplify/TopologyPreservingSimplifier;


# direct methods
.method constructor <init>(Lcom/vividsolutions/jts/simplify/TopologyPreservingSimplifier;)V
    .locals 0

    .prologue
    .line 133
    iput-object p1, p0, Lcom/vividsolutions/jts/simplify/TopologyPreservingSimplifier$LineStringTransformer;->this$0:Lcom/vividsolutions/jts/simplify/TopologyPreservingSimplifier;

    invoke-direct {p0}, Lcom/vividsolutions/jts/geom/util/GeometryTransformer;-><init>()V

    return-void
.end method


# virtual methods
.method protected transformCoordinates(Lcom/vividsolutions/jts/geom/CoordinateSequence;Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/CoordinateSequence;
    .locals 2
    .param p1, "coords"    # Lcom/vividsolutions/jts/geom/CoordinateSequence;
    .param p2, "parent"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 139
    instance-of v1, p2, Lcom/vividsolutions/jts/geom/LineString;

    if-eqz v1, :cond_0

    .line 140
    iget-object v1, p0, Lcom/vividsolutions/jts/simplify/TopologyPreservingSimplifier$LineStringTransformer;->this$0:Lcom/vividsolutions/jts/simplify/TopologyPreservingSimplifier;

    invoke-static {v1}, Lcom/vividsolutions/jts/simplify/TopologyPreservingSimplifier;->access$000(Lcom/vividsolutions/jts/simplify/TopologyPreservingSimplifier;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/simplify/TaggedLineString;

    .line 141
    .local v0, "taggedLine":Lcom/vividsolutions/jts/simplify/TaggedLineString;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/simplify/TaggedLineString;->getResultCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/vividsolutions/jts/simplify/TopologyPreservingSimplifier$LineStringTransformer;->createCoordinateSequence([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/CoordinateSequence;

    move-result-object v1

    .line 144
    .end local v0    # "taggedLine":Lcom/vividsolutions/jts/simplify/TaggedLineString;
    :goto_0
    return-object v1

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/vividsolutions/jts/geom/util/GeometryTransformer;->transformCoordinates(Lcom/vividsolutions/jts/geom/CoordinateSequence;Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/CoordinateSequence;

    move-result-object v1

    goto :goto_0
.end method

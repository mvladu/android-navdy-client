.class public abstract Lcom/vividsolutions/jts/geom/util/GeometryEditor$CoordinateSequenceOperation;
.super Ljava/lang/Object;
.source "GeometryEditor.java"

# interfaces
.implements Lcom/vividsolutions/jts/geom/util/GeometryEditor$GeometryEditorOperation;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vividsolutions/jts/geom/util/GeometryEditor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "CoordinateSequenceOperation"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 300
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract edit(Lcom/vividsolutions/jts/geom/CoordinateSequence;Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/CoordinateSequence;
.end method

.method public final edit(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/GeometryFactory;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 1
    .param p1, "geometry"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "factory"    # Lcom/vividsolutions/jts/geom/GeometryFactory;

    .prologue
    .line 304
    instance-of v0, p1, Lcom/vividsolutions/jts/geom/LinearRing;

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 305
    check-cast v0, Lcom/vividsolutions/jts/geom/LinearRing;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/LinearRing;->getCoordinateSequence()Lcom/vividsolutions/jts/geom/CoordinateSequence;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/vividsolutions/jts/geom/util/GeometryEditor$CoordinateSequenceOperation;->edit(Lcom/vividsolutions/jts/geom/CoordinateSequence;Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/CoordinateSequence;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createLinearRing(Lcom/vividsolutions/jts/geom/CoordinateSequence;)Lcom/vividsolutions/jts/geom/LinearRing;

    move-result-object p1

    .line 322
    .end local p1    # "geometry":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_0
    :goto_0
    return-object p1

    .line 310
    .restart local p1    # "geometry":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_1
    instance-of v0, p1, Lcom/vividsolutions/jts/geom/LineString;

    if-eqz v0, :cond_2

    move-object v0, p1

    .line 311
    check-cast v0, Lcom/vividsolutions/jts/geom/LineString;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinateSequence()Lcom/vividsolutions/jts/geom/CoordinateSequence;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/vividsolutions/jts/geom/util/GeometryEditor$CoordinateSequenceOperation;->edit(Lcom/vividsolutions/jts/geom/CoordinateSequence;Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/CoordinateSequence;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createLineString(Lcom/vividsolutions/jts/geom/CoordinateSequence;)Lcom/vividsolutions/jts/geom/LineString;

    move-result-object p1

    goto :goto_0

    .line 316
    :cond_2
    instance-of v0, p1, Lcom/vividsolutions/jts/geom/Point;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 317
    check-cast v0, Lcom/vividsolutions/jts/geom/Point;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/Point;->getCoordinateSequence()Lcom/vividsolutions/jts/geom/CoordinateSequence;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/vividsolutions/jts/geom/util/GeometryEditor$CoordinateSequenceOperation;->edit(Lcom/vividsolutions/jts/geom/CoordinateSequence;Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/CoordinateSequence;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createPoint(Lcom/vividsolutions/jts/geom/CoordinateSequence;)Lcom/vividsolutions/jts/geom/Point;

    move-result-object p1

    goto :goto_0
.end method

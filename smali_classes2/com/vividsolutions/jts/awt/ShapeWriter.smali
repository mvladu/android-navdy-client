.class public Lcom/vividsolutions/jts/awt/ShapeWriter;
.super Ljava/lang/Object;
.source "ShapeWriter.java"


# static fields
.field public static final DEFAULT_POINT_FACTORY:Lcom/vividsolutions/jts/awt/PointShapeFactory;

.field public static final DEFAULT_POINT_TRANSFORMATION:Lcom/vividsolutions/jts/awt/PointTransformation;


# instance fields
.field private decimationDistance:D

.field private doRemoveDuplicatePoints:Z

.field private pointFactory:Lcom/vividsolutions/jts/awt/PointShapeFactory;

.field private pointTransformer:Lcom/vividsolutions/jts/awt/PointTransformation;

.field private transPoint:Ljava/awt/geom/Point2D;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 73
    new-instance v0, Lcom/vividsolutions/jts/awt/IdentityPointTransformation;

    invoke-direct {v0}, Lcom/vividsolutions/jts/awt/IdentityPointTransformation;-><init>()V

    sput-object v0, Lcom/vividsolutions/jts/awt/ShapeWriter;->DEFAULT_POINT_TRANSFORMATION:Lcom/vividsolutions/jts/awt/PointTransformation;

    .line 78
    new-instance v0, Lcom/vividsolutions/jts/awt/PointShapeFactory$Square;

    const-wide/high16 v2, 0x4008000000000000L    # 3.0

    invoke-direct {v0, v2, v3}, Lcom/vividsolutions/jts/awt/PointShapeFactory$Square;-><init>(D)V

    sput-object v0, Lcom/vividsolutions/jts/awt/ShapeWriter;->DEFAULT_POINT_FACTORY:Lcom/vividsolutions/jts/awt/PointShapeFactory;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    sget-object v0, Lcom/vividsolutions/jts/awt/ShapeWriter;->DEFAULT_POINT_TRANSFORMATION:Lcom/vividsolutions/jts/awt/PointTransformation;

    iput-object v0, p0, Lcom/vividsolutions/jts/awt/ShapeWriter;->pointTransformer:Lcom/vividsolutions/jts/awt/PointTransformation;

    .line 81
    sget-object v0, Lcom/vividsolutions/jts/awt/ShapeWriter;->DEFAULT_POINT_FACTORY:Lcom/vividsolutions/jts/awt/PointShapeFactory;

    iput-object v0, p0, Lcom/vividsolutions/jts/awt/ShapeWriter;->pointFactory:Lcom/vividsolutions/jts/awt/PointShapeFactory;

    .line 86
    new-instance v0, Ljava/awt/geom/Point2D$Double;

    invoke-direct {v0}, Ljava/awt/geom/Point2D$Double;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/awt/ShapeWriter;->transPoint:Ljava/awt/geom/Point2D;

    .line 93
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vividsolutions/jts/awt/ShapeWriter;->doRemoveDuplicatePoints:Z

    .line 95
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/vividsolutions/jts/awt/ShapeWriter;->decimationDistance:D

    .line 128
    return-void
.end method

.method public constructor <init>(Lcom/vividsolutions/jts/awt/PointTransformation;)V
    .locals 1
    .param p1, "pointTransformer"    # Lcom/vividsolutions/jts/awt/PointTransformation;

    .prologue
    .line 120
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/vividsolutions/jts/awt/ShapeWriter;-><init>(Lcom/vividsolutions/jts/awt/PointTransformation;Lcom/vividsolutions/jts/awt/PointShapeFactory;)V

    .line 121
    return-void
.end method

.method public constructor <init>(Lcom/vividsolutions/jts/awt/PointTransformation;Lcom/vividsolutions/jts/awt/PointShapeFactory;)V
    .locals 2
    .param p1, "pointTransformer"    # Lcom/vividsolutions/jts/awt/PointTransformation;
    .param p2, "pointFactory"    # Lcom/vividsolutions/jts/awt/PointShapeFactory;

    .prologue
    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    sget-object v0, Lcom/vividsolutions/jts/awt/ShapeWriter;->DEFAULT_POINT_TRANSFORMATION:Lcom/vividsolutions/jts/awt/PointTransformation;

    iput-object v0, p0, Lcom/vividsolutions/jts/awt/ShapeWriter;->pointTransformer:Lcom/vividsolutions/jts/awt/PointTransformation;

    .line 81
    sget-object v0, Lcom/vividsolutions/jts/awt/ShapeWriter;->DEFAULT_POINT_FACTORY:Lcom/vividsolutions/jts/awt/PointShapeFactory;

    iput-object v0, p0, Lcom/vividsolutions/jts/awt/ShapeWriter;->pointFactory:Lcom/vividsolutions/jts/awt/PointShapeFactory;

    .line 86
    new-instance v0, Ljava/awt/geom/Point2D$Double;

    invoke-direct {v0}, Ljava/awt/geom/Point2D$Double;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/awt/ShapeWriter;->transPoint:Ljava/awt/geom/Point2D;

    .line 93
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vividsolutions/jts/awt/ShapeWriter;->doRemoveDuplicatePoints:Z

    .line 95
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/vividsolutions/jts/awt/ShapeWriter;->decimationDistance:D

    .line 106
    if-eqz p1, :cond_0

    .line 107
    iput-object p1, p0, Lcom/vividsolutions/jts/awt/ShapeWriter;->pointTransformer:Lcom/vividsolutions/jts/awt/PointTransformation;

    .line 108
    :cond_0
    if-eqz p2, :cond_1

    .line 109
    iput-object p2, p0, Lcom/vividsolutions/jts/awt/ShapeWriter;->pointFactory:Lcom/vividsolutions/jts/awt/PointShapeFactory;

    .line 110
    :cond_1
    return-void
.end method

.method private appendRing(Lcom/vividsolutions/jts/awt/PolygonShape;[Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 16
    .param p1, "poly"    # Lcom/vividsolutions/jts/awt/PolygonShape;
    .param p2, "coords"    # [Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 210
    const-wide/high16 v8, 0x7ff8000000000000L    # NaN

    .line 211
    .local v8, "prevx":D
    const-wide/high16 v10, 0x7ff8000000000000L    # NaN

    .line 212
    .local v10, "prevy":D
    const/4 v6, 0x0

    .line 214
    .local v6, "prev":Lcom/vividsolutions/jts/geom/Coordinate;
    move-object/from16 v0, p2

    array-length v7, v0

    add-int/lit8 v5, v7, -0x1

    .line 220
    .local v5, "n":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v5, :cond_7

    .line 222
    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/vividsolutions/jts/awt/ShapeWriter;->decimationDistance:D

    const-wide/16 v14, 0x0

    cmpl-double v7, v12, v14

    if-lez v7, :cond_3

    .line 223
    if-eqz v6, :cond_1

    aget-object v7, p2, v2

    iget-wide v12, v7, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-wide v14, v6, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    sub-double/2addr v12, v14

    invoke-static {v12, v13}, Ljava/lang/Math;->abs(D)D

    move-result-wide v12

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/vividsolutions/jts/awt/ShapeWriter;->decimationDistance:D

    cmpg-double v7, v12, v14

    if-gez v7, :cond_1

    aget-object v7, p2, v2

    iget-wide v12, v7, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-wide v14, v6, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    sub-double/2addr v12, v14

    invoke-static {v12, v13}, Ljava/lang/Math;->abs(D)D

    move-result-wide v12

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/vividsolutions/jts/awt/ShapeWriter;->decimationDistance:D

    cmpg-double v7, v12, v14

    if-gez v7, :cond_1

    const/4 v3, 0x1

    .line 226
    .local v3, "isDecimated":Z
    :goto_1
    if-ge v2, v5, :cond_2

    if-eqz v3, :cond_2

    .line 220
    .end local v3    # "isDecimated":Z
    :cond_0
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 223
    :cond_1
    const/4 v3, 0x0

    goto :goto_1

    .line 228
    .restart local v3    # "isDecimated":Z
    :cond_2
    aget-object v6, p2, v2

    .line 231
    .end local v3    # "isDecimated":Z
    :cond_3
    aget-object v7, p2, v2

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/vividsolutions/jts/awt/ShapeWriter;->transPoint:Ljava/awt/geom/Point2D;

    move-object/from16 v0, p0

    invoke-direct {v0, v7, v12}, Lcom/vividsolutions/jts/awt/ShapeWriter;->transformPoint(Lcom/vividsolutions/jts/geom/Coordinate;Ljava/awt/geom/Point2D;)Ljava/awt/geom/Point2D;

    .line 233
    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/vividsolutions/jts/awt/ShapeWriter;->doRemoveDuplicatePoints:Z

    if-eqz v7, :cond_5

    .line 235
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/vividsolutions/jts/awt/ShapeWriter;->transPoint:Ljava/awt/geom/Point2D;

    invoke-virtual {v7}, Ljava/awt/geom/Point2D;->getX()D

    move-result-wide v12

    cmpl-double v7, v12, v8

    if-nez v7, :cond_6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/vividsolutions/jts/awt/ShapeWriter;->transPoint:Ljava/awt/geom/Point2D;

    invoke-virtual {v7}, Ljava/awt/geom/Point2D;->getY()D

    move-result-wide v12

    cmpl-double v7, v12, v10

    if-nez v7, :cond_6

    const/4 v4, 0x1

    .line 236
    .local v4, "isDup":Z
    :goto_3
    if-ge v2, v5, :cond_4

    if-nez v4, :cond_0

    .line 238
    :cond_4
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/vividsolutions/jts/awt/ShapeWriter;->transPoint:Ljava/awt/geom/Point2D;

    invoke-virtual {v7}, Ljava/awt/geom/Point2D;->getX()D

    move-result-wide v8

    .line 239
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/vividsolutions/jts/awt/ShapeWriter;->transPoint:Ljava/awt/geom/Point2D;

    invoke-virtual {v7}, Ljava/awt/geom/Point2D;->getY()D

    move-result-wide v10

    .line 241
    .end local v4    # "isDup":Z
    :cond_5
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/vividsolutions/jts/awt/ShapeWriter;->transPoint:Ljava/awt/geom/Point2D;

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Lcom/vividsolutions/jts/awt/PolygonShape;->addToRing(Ljava/awt/geom/Point2D;)V

    goto :goto_2

    .line 235
    :cond_6
    const/4 v4, 0x0

    goto :goto_3

    .line 244
    :cond_7
    invoke-virtual/range {p1 .. p1}, Lcom/vividsolutions/jts/awt/PolygonShape;->endRing()V

    .line 245
    return-void
.end method

.method private toShape(Lcom/vividsolutions/jts/geom/GeometryCollection;)Ljava/awt/Shape;
    .locals 4
    .param p1, "gc"    # Lcom/vividsolutions/jts/geom/GeometryCollection;

    .prologue
    .line 249
    new-instance v2, Lcom/vividsolutions/jts/awt/GeometryCollectionShape;

    invoke-direct {v2}, Lcom/vividsolutions/jts/awt/GeometryCollectionShape;-><init>()V

    .line 251
    .local v2, "shape":Lcom/vividsolutions/jts/awt/GeometryCollectionShape;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/GeometryCollection;->getNumGeometries()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 252
    invoke-virtual {p1, v1}, Lcom/vividsolutions/jts/geom/GeometryCollection;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    .line 253
    .local v0, "g":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-virtual {p0, v0}, Lcom/vividsolutions/jts/awt/ShapeWriter;->toShape(Lcom/vividsolutions/jts/geom/Geometry;)Ljava/awt/Shape;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/vividsolutions/jts/awt/GeometryCollectionShape;->add(Ljava/awt/Shape;)V

    .line 251
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 255
    .end local v0    # "g":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_0
    return-object v2
.end method

.method private toShape(Lcom/vividsolutions/jts/geom/Point;)Ljava/awt/Shape;
    .locals 2
    .param p1, "point"    # Lcom/vividsolutions/jts/geom/Point;

    .prologue
    .line 313
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Point;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/vividsolutions/jts/awt/ShapeWriter;->transformPoint(Lcom/vividsolutions/jts/geom/Coordinate;)Ljava/awt/geom/Point2D;

    move-result-object v0

    .line 314
    .local v0, "viewPoint":Ljava/awt/geom/Point2D;
    iget-object v1, p0, Lcom/vividsolutions/jts/awt/ShapeWriter;->pointFactory:Lcom/vividsolutions/jts/awt/PointShapeFactory;

    invoke-interface {v1, v0}, Lcom/vividsolutions/jts/awt/PointShapeFactory;->createPoint(Ljava/awt/geom/Point2D;)Ljava/awt/Shape;

    move-result-object v1

    return-object v1
.end method

.method private toShape(Lcom/vividsolutions/jts/geom/Polygon;)Ljava/awt/Shape;
    .locals 3
    .param p1, "p"    # Lcom/vividsolutions/jts/geom/Polygon;

    .prologue
    .line 198
    new-instance v1, Lcom/vividsolutions/jts/awt/PolygonShape;

    invoke-direct {v1}, Lcom/vividsolutions/jts/awt/PolygonShape;-><init>()V

    .line 200
    .local v1, "poly":Lcom/vividsolutions/jts/awt/PolygonShape;
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Polygon;->getExteriorRing()Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/vividsolutions/jts/awt/ShapeWriter;->appendRing(Lcom/vividsolutions/jts/awt/PolygonShape;[Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 201
    const/4 v0, 0x0

    .local v0, "j":I
    :goto_0
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Polygon;->getNumInteriorRing()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 202
    invoke-virtual {p1, v0}, Lcom/vividsolutions/jts/geom/Polygon;->getInteriorRingN(I)Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/vividsolutions/jts/awt/ShapeWriter;->appendRing(Lcom/vividsolutions/jts/awt/PolygonShape;[Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 201
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 205
    :cond_0
    return-object v1
.end method

.method private toShape(Lcom/vividsolutions/jts/geom/LineString;)Ljava/awt/geom/GeneralPath;
    .locals 18
    .param p1, "lineString"    # Lcom/vividsolutions/jts/geom/LineString;

    .prologue
    .line 271
    new-instance v12, Ljava/awt/geom/GeneralPath;

    invoke-direct {v12}, Ljava/awt/geom/GeneralPath;-><init>()V

    .line 273
    .local v12, "shape":Ljava/awt/geom/GeneralPath;
    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinateN(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v7

    .line 274
    .local v7, "prev":Lcom/vividsolutions/jts/geom/Coordinate;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vividsolutions/jts/awt/ShapeWriter;->transPoint:Ljava/awt/geom/Point2D;

    move-object/from16 v0, p0

    invoke-direct {v0, v7, v13}, Lcom/vividsolutions/jts/awt/ShapeWriter;->transformPoint(Lcom/vividsolutions/jts/geom/Coordinate;Ljava/awt/geom/Point2D;)Ljava/awt/geom/Point2D;

    .line 275
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vividsolutions/jts/awt/ShapeWriter;->transPoint:Ljava/awt/geom/Point2D;

    invoke-virtual {v13}, Ljava/awt/geom/Point2D;->getX()D

    move-result-wide v14

    double-to-float v13, v14

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vividsolutions/jts/awt/ShapeWriter;->transPoint:Ljava/awt/geom/Point2D;

    invoke-virtual {v14}, Ljava/awt/geom/Point2D;->getY()D

    move-result-wide v14

    double-to-float v14, v14

    invoke-virtual {v12, v13, v14}, Ljava/awt/geom/GeneralPath;->moveTo(FF)V

    .line 277
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vividsolutions/jts/awt/ShapeWriter;->transPoint:Ljava/awt/geom/Point2D;

    invoke-virtual {v13}, Ljava/awt/geom/Point2D;->getX()D

    move-result-wide v8

    .line 278
    .local v8, "prevx":D
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vividsolutions/jts/awt/ShapeWriter;->transPoint:Ljava/awt/geom/Point2D;

    invoke-virtual {v13}, Ljava/awt/geom/Point2D;->getY()D

    move-result-wide v10

    .line 280
    .local v10, "prevy":D
    invoke-virtual/range {p1 .. p1}, Lcom/vividsolutions/jts/geom/LineString;->getNumPoints()I

    move-result v13

    add-int/lit8 v6, v13, -0x1

    .line 282
    .local v6, "n":I
    const/4 v3, 0x1

    .local v3, "i":I
    :goto_0
    if-gt v3, v6, :cond_7

    .line 283
    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinateN(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    .line 284
    .local v2, "currentCoord":Lcom/vividsolutions/jts/geom/Coordinate;
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/vividsolutions/jts/awt/ShapeWriter;->decimationDistance:D

    const-wide/16 v16, 0x0

    cmpl-double v13, v14, v16

    if-lez v13, :cond_3

    .line 285
    if-eqz v7, :cond_1

    iget-wide v14, v2, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-wide v0, v7, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v16, v0

    sub-double v14, v14, v16

    invoke-static {v14, v15}, Ljava/lang/Math;->abs(D)D

    move-result-wide v14

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/awt/ShapeWriter;->decimationDistance:D

    move-wide/from16 v16, v0

    cmpg-double v13, v14, v16

    if-gez v13, :cond_1

    iget-wide v14, v2, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-wide v0, v7, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v16, v0

    sub-double v14, v14, v16

    invoke-static {v14, v15}, Ljava/lang/Math;->abs(D)D

    move-result-wide v14

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/awt/ShapeWriter;->decimationDistance:D

    move-wide/from16 v16, v0

    cmpg-double v13, v14, v16

    if-gez v13, :cond_1

    const/4 v4, 0x1

    .line 288
    .local v4, "isDecimated":Z
    :goto_1
    if-ge v3, v6, :cond_2

    if-eqz v4, :cond_2

    .line 282
    .end local v4    # "isDecimated":Z
    :cond_0
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 285
    :cond_1
    const/4 v4, 0x0

    goto :goto_1

    .line 291
    .restart local v4    # "isDecimated":Z
    :cond_2
    move-object v7, v2

    .line 294
    .end local v4    # "isDecimated":Z
    :cond_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vividsolutions/jts/awt/ShapeWriter;->transPoint:Ljava/awt/geom/Point2D;

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v13}, Lcom/vividsolutions/jts/awt/ShapeWriter;->transformPoint(Lcom/vividsolutions/jts/geom/Coordinate;Ljava/awt/geom/Point2D;)Ljava/awt/geom/Point2D;

    .line 296
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/vividsolutions/jts/awt/ShapeWriter;->doRemoveDuplicatePoints:Z

    if-eqz v13, :cond_5

    .line 298
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vividsolutions/jts/awt/ShapeWriter;->transPoint:Ljava/awt/geom/Point2D;

    invoke-virtual {v13}, Ljava/awt/geom/Point2D;->getX()D

    move-result-wide v14

    cmpl-double v13, v14, v8

    if-nez v13, :cond_6

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vividsolutions/jts/awt/ShapeWriter;->transPoint:Ljava/awt/geom/Point2D;

    invoke-virtual {v13}, Ljava/awt/geom/Point2D;->getY()D

    move-result-wide v14

    cmpl-double v13, v14, v10

    if-nez v13, :cond_6

    const/4 v5, 0x1

    .line 299
    .local v5, "isDup":Z
    :goto_3
    if-ge v3, v6, :cond_4

    if-nez v5, :cond_0

    .line 301
    :cond_4
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vividsolutions/jts/awt/ShapeWriter;->transPoint:Ljava/awt/geom/Point2D;

    invoke-virtual {v13}, Ljava/awt/geom/Point2D;->getX()D

    move-result-wide v8

    .line 302
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vividsolutions/jts/awt/ShapeWriter;->transPoint:Ljava/awt/geom/Point2D;

    invoke-virtual {v13}, Ljava/awt/geom/Point2D;->getY()D

    move-result-wide v10

    .line 305
    .end local v5    # "isDup":Z
    :cond_5
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/vividsolutions/jts/awt/ShapeWriter;->transPoint:Ljava/awt/geom/Point2D;

    invoke-virtual {v13}, Ljava/awt/geom/Point2D;->getX()D

    move-result-wide v14

    double-to-float v13, v14

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vividsolutions/jts/awt/ShapeWriter;->transPoint:Ljava/awt/geom/Point2D;

    invoke-virtual {v14}, Ljava/awt/geom/Point2D;->getY()D

    move-result-wide v14

    double-to-float v14, v14

    invoke-virtual {v12, v13, v14}, Ljava/awt/geom/GeneralPath;->lineTo(FF)V

    goto :goto_2

    .line 298
    :cond_6
    const/4 v5, 0x0

    goto :goto_3

    .line 308
    .end local v2    # "currentCoord":Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_7
    return-object v12
.end method

.method private toShape(Lcom/vividsolutions/jts/geom/MultiLineString;)Ljava/awt/geom/GeneralPath;
    .locals 5
    .param p1, "mls"    # Lcom/vividsolutions/jts/geom/MultiLineString;

    .prologue
    .line 260
    new-instance v2, Ljava/awt/geom/GeneralPath;

    invoke-direct {v2}, Ljava/awt/geom/GeneralPath;-><init>()V

    .line 262
    .local v2, "path":Ljava/awt/geom/GeneralPath;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/MultiLineString;->getNumGeometries()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 263
    invoke-virtual {p1, v0}, Lcom/vividsolutions/jts/geom/MultiLineString;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/geom/LineString;

    .line 264
    .local v1, "lineString":Lcom/vividsolutions/jts/geom/LineString;
    invoke-direct {p0, v1}, Lcom/vividsolutions/jts/awt/ShapeWriter;->toShape(Lcom/vividsolutions/jts/geom/LineString;)Ljava/awt/geom/GeneralPath;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Ljava/awt/geom/GeneralPath;->append(Ljava/awt/Shape;Z)V

    .line 262
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 266
    .end local v1    # "lineString":Lcom/vividsolutions/jts/geom/LineString;
    :cond_0
    return-object v2
.end method

.method private transformPoint(Lcom/vividsolutions/jts/geom/Coordinate;)Ljava/awt/geom/Point2D;
    .locals 1
    .param p1, "model"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 318
    new-instance v0, Ljava/awt/geom/Point2D$Double;

    invoke-direct {v0}, Ljava/awt/geom/Point2D$Double;-><init>()V

    invoke-direct {p0, p1, v0}, Lcom/vividsolutions/jts/awt/ShapeWriter;->transformPoint(Lcom/vividsolutions/jts/geom/Coordinate;Ljava/awt/geom/Point2D;)Ljava/awt/geom/Point2D;

    move-result-object v0

    return-object v0
.end method

.method private transformPoint(Lcom/vividsolutions/jts/geom/Coordinate;Ljava/awt/geom/Point2D;)Ljava/awt/geom/Point2D;
    .locals 1
    .param p1, "model"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "view"    # Ljava/awt/geom/Point2D;

    .prologue
    .line 322
    iget-object v0, p0, Lcom/vividsolutions/jts/awt/ShapeWriter;->pointTransformer:Lcom/vividsolutions/jts/awt/PointTransformation;

    invoke-interface {v0, p1, p2}, Lcom/vividsolutions/jts/awt/PointTransformation;->transform(Lcom/vividsolutions/jts/geom/Coordinate;Ljava/awt/geom/Point2D;)V

    .line 323
    return-object p2
.end method


# virtual methods
.method public setDecimation(D)V
    .locals 1
    .param p1, "decimationDistance"    # D

    .prologue
    .line 164
    iput-wide p1, p0, Lcom/vividsolutions/jts/awt/ShapeWriter;->decimationDistance:D

    .line 165
    return-void
.end method

.method public setRemoveDuplicatePoints(Z)V
    .locals 0
    .param p1, "doRemoveDuplicatePoints"    # Z

    .prologue
    .line 142
    iput-boolean p1, p0, Lcom/vividsolutions/jts/awt/ShapeWriter;->doRemoveDuplicatePoints:Z

    .line 143
    return-void
.end method

.method public toShape(Lcom/vividsolutions/jts/geom/Geometry;)Ljava/awt/Shape;
    .locals 3
    .param p1, "geometry"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 185
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/awt/geom/GeneralPath;

    invoke-direct {v0}, Ljava/awt/geom/GeneralPath;-><init>()V

    .line 190
    .end local p1    # "geometry":Lcom/vividsolutions/jts/geom/Geometry;
    :goto_0
    return-object v0

    .line 186
    .restart local p1    # "geometry":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_0
    instance-of v0, p1, Lcom/vividsolutions/jts/geom/Polygon;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/vividsolutions/jts/geom/Polygon;

    .end local p1    # "geometry":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/awt/ShapeWriter;->toShape(Lcom/vividsolutions/jts/geom/Polygon;)Ljava/awt/Shape;

    move-result-object v0

    goto :goto_0

    .line 187
    .restart local p1    # "geometry":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_1
    instance-of v0, p1, Lcom/vividsolutions/jts/geom/LineString;

    if-eqz v0, :cond_2

    check-cast p1, Lcom/vividsolutions/jts/geom/LineString;

    .end local p1    # "geometry":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/awt/ShapeWriter;->toShape(Lcom/vividsolutions/jts/geom/LineString;)Ljava/awt/geom/GeneralPath;

    move-result-object v0

    goto :goto_0

    .line 188
    .restart local p1    # "geometry":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_2
    instance-of v0, p1, Lcom/vividsolutions/jts/geom/MultiLineString;

    if-eqz v0, :cond_3

    check-cast p1, Lcom/vividsolutions/jts/geom/MultiLineString;

    .end local p1    # "geometry":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/awt/ShapeWriter;->toShape(Lcom/vividsolutions/jts/geom/MultiLineString;)Ljava/awt/geom/GeneralPath;

    move-result-object v0

    goto :goto_0

    .line 189
    .restart local p1    # "geometry":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_3
    instance-of v0, p1, Lcom/vividsolutions/jts/geom/Point;

    if-eqz v0, :cond_4

    check-cast p1, Lcom/vividsolutions/jts/geom/Point;

    .end local p1    # "geometry":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/awt/ShapeWriter;->toShape(Lcom/vividsolutions/jts/geom/Point;)Ljava/awt/Shape;

    move-result-object v0

    goto :goto_0

    .line 190
    .restart local p1    # "geometry":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_4
    instance-of v0, p1, Lcom/vividsolutions/jts/geom/GeometryCollection;

    if-eqz v0, :cond_5

    check-cast p1, Lcom/vividsolutions/jts/geom/GeometryCollection;

    .end local p1    # "geometry":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/awt/ShapeWriter;->toShape(Lcom/vividsolutions/jts/geom/GeometryCollection;)Ljava/awt/Shape;

    move-result-object v0

    goto :goto_0

    .line 192
    .restart local p1    # "geometry":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unrecognized Geometry class: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.class public Lcom/vividsolutions/jts/math/Vector2D;
.super Ljava/lang/Object;
.source "Vector2D.java"


# instance fields
.field private x:D

.field private y:D


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 102
    invoke-direct {p0, v0, v1, v0, v1}, Lcom/vividsolutions/jts/math/Vector2D;-><init>(DD)V

    .line 103
    return-void
.end method

.method public constructor <init>(DD)V
    .locals 1
    .param p1, "x"    # D
    .param p3, "y"    # D

    .prologue
    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 106
    iput-wide p1, p0, Lcom/vividsolutions/jts/math/Vector2D;->x:D

    .line 107
    iput-wide p3, p0, Lcom/vividsolutions/jts/math/Vector2D;->y:D

    .line 108
    return-void
.end method

.method public constructor <init>(Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 2
    .param p1, "v"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 121
    iget-wide v0, p1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iput-wide v0, p0, Lcom/vividsolutions/jts/math/Vector2D;->x:D

    .line 122
    iget-wide v0, p1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iput-wide v0, p0, Lcom/vividsolutions/jts/math/Vector2D;->y:D

    .line 123
    return-void
.end method

.method public constructor <init>(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 4
    .param p1, "from"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "to"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 116
    iget-wide v0, p2, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-wide v2, p1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Lcom/vividsolutions/jts/math/Vector2D;->x:D

    .line 117
    iget-wide v0, p2, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-wide v2, p1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Lcom/vividsolutions/jts/math/Vector2D;->y:D

    .line 118
    return-void
.end method

.method public constructor <init>(Lcom/vividsolutions/jts/math/Vector2D;)V
    .locals 2
    .param p1, "v"    # Lcom/vividsolutions/jts/math/Vector2D;

    .prologue
    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 111
    iget-wide v0, p1, Lcom/vividsolutions/jts/math/Vector2D;->x:D

    iput-wide v0, p0, Lcom/vividsolutions/jts/math/Vector2D;->x:D

    .line 112
    iget-wide v0, p1, Lcom/vividsolutions/jts/math/Vector2D;->y:D

    iput-wide v0, p0, Lcom/vividsolutions/jts/math/Vector2D;->y:D

    .line 113
    return-void
.end method

.method public static create(DD)Lcom/vividsolutions/jts/math/Vector2D;
    .locals 2
    .param p0, "x"    # D
    .param p2, "y"    # D

    .prologue
    .line 55
    new-instance v0, Lcom/vividsolutions/jts/math/Vector2D;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/vividsolutions/jts/math/Vector2D;-><init>(DD)V

    return-object v0
.end method

.method public static create(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/math/Vector2D;
    .locals 1
    .param p0, "coord"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 75
    new-instance v0, Lcom/vividsolutions/jts/math/Vector2D;

    invoke-direct {v0, p0}, Lcom/vividsolutions/jts/math/Vector2D;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;)V

    return-object v0
.end method

.method public static create(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/math/Vector2D;
    .locals 1
    .param p0, "from"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "to"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 88
    new-instance v0, Lcom/vividsolutions/jts/math/Vector2D;

    invoke-direct {v0, p0, p1}, Lcom/vividsolutions/jts/math/Vector2D;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    return-object v0
.end method

.method public static create(Lcom/vividsolutions/jts/math/Vector2D;)Lcom/vividsolutions/jts/math/Vector2D;
    .locals 1
    .param p0, "v"    # Lcom/vividsolutions/jts/math/Vector2D;

    .prologue
    .line 65
    new-instance v0, Lcom/vividsolutions/jts/math/Vector2D;

    invoke-direct {v0, p0}, Lcom/vividsolutions/jts/math/Vector2D;-><init>(Lcom/vividsolutions/jts/math/Vector2D;)V

    return-object v0
.end method


# virtual methods
.method public add(Lcom/vividsolutions/jts/math/Vector2D;)Lcom/vividsolutions/jts/math/Vector2D;
    .locals 6
    .param p1, "v"    # Lcom/vividsolutions/jts/math/Vector2D;

    .prologue
    .line 140
    iget-wide v0, p0, Lcom/vividsolutions/jts/math/Vector2D;->x:D

    iget-wide v2, p1, Lcom/vividsolutions/jts/math/Vector2D;->x:D

    add-double/2addr v0, v2

    iget-wide v2, p0, Lcom/vividsolutions/jts/math/Vector2D;->y:D

    iget-wide v4, p1, Lcom/vividsolutions/jts/math/Vector2D;->y:D

    add-double/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Lcom/vividsolutions/jts/math/Vector2D;->create(DD)Lcom/vividsolutions/jts/math/Vector2D;

    move-result-object v0

    return-object v0
.end method

.method public angle()D
    .locals 4

    .prologue
    .line 235
    iget-wide v0, p0, Lcom/vividsolutions/jts/math/Vector2D;->y:D

    iget-wide v2, p0, Lcom/vividsolutions/jts/math/Vector2D;->x:D

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v0

    return-wide v0
.end method

.method public angle(Lcom/vividsolutions/jts/math/Vector2D;)D
    .locals 4
    .param p1, "v"    # Lcom/vividsolutions/jts/math/Vector2D;

    .prologue
    .line 240
    invoke-virtual {p1}, Lcom/vividsolutions/jts/math/Vector2D;->angle()D

    move-result-wide v0

    invoke-virtual {p0}, Lcom/vividsolutions/jts/math/Vector2D;->angle()D

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/vividsolutions/jts/algorithm/Angle;->diff(DD)D

    move-result-wide v0

    return-wide v0
.end method

.method public angleTo(Lcom/vividsolutions/jts/math/Vector2D;)D
    .locals 10
    .param p1, "v"    # Lcom/vividsolutions/jts/math/Vector2D;

    .prologue
    const-wide v8, 0x401921fb54442d18L    # 6.283185307179586

    .line 245
    invoke-virtual {p0}, Lcom/vividsolutions/jts/math/Vector2D;->angle()D

    move-result-wide v0

    .line 246
    .local v0, "a1":D
    invoke-virtual {p1}, Lcom/vividsolutions/jts/math/Vector2D;->angle()D

    move-result-wide v2

    .line 247
    .local v2, "a2":D
    sub-double v4, v2, v0

    .line 250
    .local v4, "angDel":D
    const-wide v6, -0x3ff6de04abbbd2e8L    # -3.141592653589793

    cmpg-double v6, v4, v6

    if-gtz v6, :cond_1

    .line 251
    add-double/2addr v4, v8

    .line 254
    .end local v4    # "angDel":D
    :cond_0
    :goto_0
    return-wide v4

    .line 252
    .restart local v4    # "angDel":D
    :cond_1
    const-wide v6, 0x400921fb54442d18L    # Math.PI

    cmpl-double v6, v4, v6

    if-lez v6, :cond_0

    .line 253
    sub-double/2addr v4, v8

    goto :goto_0
.end method

.method public average(Lcom/vividsolutions/jts/math/Vector2D;)Lcom/vividsolutions/jts/math/Vector2D;
    .locals 2
    .param p1, "v"    # Lcom/vividsolutions/jts/math/Vector2D;

    .prologue
    .line 187
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p0, p1, v0, v1}, Lcom/vividsolutions/jts/math/Vector2D;->weightedSum(Lcom/vividsolutions/jts/math/Vector2D;D)Lcom/vividsolutions/jts/math/Vector2D;

    move-result-object v0

    return-object v0
.end method

.method public clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 317
    new-instance v0, Lcom/vividsolutions/jts/math/Vector2D;

    invoke-direct {v0, p0}, Lcom/vividsolutions/jts/math/Vector2D;-><init>(Lcom/vividsolutions/jts/math/Vector2D;)V

    return-object v0
.end method

.method public distance(Lcom/vividsolutions/jts/math/Vector2D;)D
    .locals 8
    .param p1, "v"    # Lcom/vividsolutions/jts/math/Vector2D;

    .prologue
    .line 218
    iget-wide v4, p1, Lcom/vividsolutions/jts/math/Vector2D;->x:D

    iget-wide v6, p0, Lcom/vividsolutions/jts/math/Vector2D;->x:D

    sub-double v0, v4, v6

    .line 219
    .local v0, "delx":D
    iget-wide v4, p1, Lcom/vividsolutions/jts/math/Vector2D;->y:D

    iget-wide v6, p0, Lcom/vividsolutions/jts/math/Vector2D;->y:D

    sub-double v2, v4, v6

    .line 220
    .local v2, "dely":D
    mul-double v4, v0, v0

    mul-double v6, v2, v2

    add-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    return-wide v4
.end method

.method public divide(D)Lcom/vividsolutions/jts/math/Vector2D;
    .locals 5
    .param p1, "d"    # D

    .prologue
    .line 164
    iget-wide v0, p0, Lcom/vividsolutions/jts/math/Vector2D;->x:D

    div-double/2addr v0, p1

    iget-wide v2, p0, Lcom/vividsolutions/jts/math/Vector2D;->y:D

    div-double/2addr v2, p1

    invoke-static {v0, v1, v2, v3}, Lcom/vividsolutions/jts/math/Vector2D;->create(DD)Lcom/vividsolutions/jts/math/Vector2D;

    move-result-object v0

    return-object v0
.end method

.method public dot(Lcom/vividsolutions/jts/math/Vector2D;)D
    .locals 6
    .param p1, "v"    # Lcom/vividsolutions/jts/math/Vector2D;

    .prologue
    .line 230
    iget-wide v0, p0, Lcom/vividsolutions/jts/math/Vector2D;->x:D

    iget-wide v2, p1, Lcom/vividsolutions/jts/math/Vector2D;->x:D

    mul-double/2addr v0, v2

    iget-wide v2, p0, Lcom/vividsolutions/jts/math/Vector2D;->y:D

    iget-wide v4, p1, Lcom/vividsolutions/jts/math/Vector2D;->y:D

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    return-wide v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 339
    instance-of v2, p1, Lcom/vividsolutions/jts/math/Vector2D;

    if-nez v2, :cond_1

    .line 343
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 342
    check-cast v0, Lcom/vividsolutions/jts/math/Vector2D;

    .line 343
    .local v0, "v":Lcom/vividsolutions/jts/math/Vector2D;
    iget-wide v2, p0, Lcom/vividsolutions/jts/math/Vector2D;->x:D

    iget-wide v4, v0, Lcom/vividsolutions/jts/math/Vector2D;->x:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    iget-wide v2, p0, Lcom/vividsolutions/jts/math/Vector2D;->y:D

    iget-wide v4, v0, Lcom/vividsolutions/jts/math/Vector2D;->y:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getComponent(I)D
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 134
    if-nez p1, :cond_0

    .line 135
    iget-wide v0, p0, Lcom/vividsolutions/jts/math/Vector2D;->x:D

    .line 136
    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/vividsolutions/jts/math/Vector2D;->y:D

    goto :goto_0
.end method

.method public getX()D
    .locals 2

    .prologue
    .line 126
    iget-wide v0, p0, Lcom/vividsolutions/jts/math/Vector2D;->x:D

    return-wide v0
.end method

.method public getY()D
    .locals 2

    .prologue
    .line 130
    iget-wide v0, p0, Lcom/vividsolutions/jts/math/Vector2D;->y:D

    return-wide v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 353
    const/16 v0, 0x11

    .line 354
    .local v0, "result":I
    iget-wide v2, p0, Lcom/vividsolutions/jts/math/Vector2D;->x:D

    invoke-static {v2, v3}, Lcom/vividsolutions/jts/geom/Coordinate;->hashCode(D)I

    move-result v1

    add-int/lit16 v0, v1, 0x275

    .line 355
    mul-int/lit8 v1, v0, 0x25

    iget-wide v2, p0, Lcom/vividsolutions/jts/math/Vector2D;->y:D

    invoke-static {v2, v3}, Lcom/vividsolutions/jts/geom/Coordinate;->hashCode(D)I

    move-result v2

    add-int v0, v1, v2

    .line 356
    return v0
.end method

.method public isParallel(Lcom/vividsolutions/jts/math/Vector2D;)Z
    .locals 10
    .param p1, "v"    # Lcom/vividsolutions/jts/math/Vector2D;

    .prologue
    .line 299
    const-wide/16 v8, 0x0

    iget-wide v0, p0, Lcom/vividsolutions/jts/math/Vector2D;->x:D

    iget-wide v2, p0, Lcom/vividsolutions/jts/math/Vector2D;->y:D

    iget-wide v4, p1, Lcom/vividsolutions/jts/math/Vector2D;->x:D

    iget-wide v6, p1, Lcom/vividsolutions/jts/math/Vector2D;->y:D

    invoke-static/range {v0 .. v7}, Lcom/vividsolutions/jts/algorithm/RobustDeterminant;->signOfDet2x2(DDDD)I

    move-result v0

    int-to-double v0, v0

    cmpl-double v0, v8, v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public length()D
    .locals 6

    .prologue
    .line 172
    iget-wide v0, p0, Lcom/vividsolutions/jts/math/Vector2D;->x:D

    iget-wide v2, p0, Lcom/vividsolutions/jts/math/Vector2D;->x:D

    mul-double/2addr v0, v2

    iget-wide v2, p0, Lcom/vividsolutions/jts/math/Vector2D;->y:D

    iget-wide v4, p0, Lcom/vividsolutions/jts/math/Vector2D;->y:D

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    return-wide v0
.end method

.method public lengthSquared()D
    .locals 6

    .prologue
    .line 176
    iget-wide v0, p0, Lcom/vividsolutions/jts/math/Vector2D;->x:D

    iget-wide v2, p0, Lcom/vividsolutions/jts/math/Vector2D;->x:D

    mul-double/2addr v0, v2

    iget-wide v2, p0, Lcom/vividsolutions/jts/math/Vector2D;->y:D

    iget-wide v4, p0, Lcom/vividsolutions/jts/math/Vector2D;->y:D

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    return-wide v0
.end method

.method public multiply(D)Lcom/vividsolutions/jts/math/Vector2D;
    .locals 5
    .param p1, "d"    # D

    .prologue
    .line 154
    iget-wide v0, p0, Lcom/vividsolutions/jts/math/Vector2D;->x:D

    mul-double/2addr v0, p1

    iget-wide v2, p0, Lcom/vividsolutions/jts/math/Vector2D;->y:D

    mul-double/2addr v2, p1

    invoke-static {v0, v1, v2, v3}, Lcom/vividsolutions/jts/math/Vector2D;->create(DD)Lcom/vividsolutions/jts/math/Vector2D;

    move-result-object v0

    return-object v0
.end method

.method public negate()Lcom/vividsolutions/jts/math/Vector2D;
    .locals 4

    .prologue
    .line 168
    iget-wide v0, p0, Lcom/vividsolutions/jts/math/Vector2D;->x:D

    neg-double v0, v0

    iget-wide v2, p0, Lcom/vividsolutions/jts/math/Vector2D;->y:D

    neg-double v2, v2

    invoke-static {v0, v1, v2, v3}, Lcom/vividsolutions/jts/math/Vector2D;->create(DD)Lcom/vividsolutions/jts/math/Vector2D;

    move-result-object v0

    return-object v0
.end method

.method public normalize()Lcom/vividsolutions/jts/math/Vector2D;
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 180
    invoke-virtual {p0}, Lcom/vividsolutions/jts/math/Vector2D;->length()D

    move-result-wide v0

    .line 181
    .local v0, "length":D
    cmpl-double v2, v0, v4

    if-lez v2, :cond_0

    .line 182
    invoke-virtual {p0, v0, v1}, Lcom/vividsolutions/jts/math/Vector2D;->divide(D)Lcom/vividsolutions/jts/math/Vector2D;

    move-result-object v2

    .line 183
    :goto_0
    return-object v2

    :cond_0
    invoke-static {v4, v5, v4, v5}, Lcom/vividsolutions/jts/math/Vector2D;->create(DD)Lcom/vividsolutions/jts/math/Vector2D;

    move-result-object v2

    goto :goto_0
.end method

.method public rotate(D)Lcom/vividsolutions/jts/math/Vector2D;
    .locals 11
    .param p1, "angle"    # D

    .prologue
    .line 259
    invoke-static {p1, p2}, Ljava/lang/Math;->cos(D)D

    move-result-wide v0

    .line 260
    .local v0, "cos":D
    invoke-static {p1, p2}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    .line 261
    .local v2, "sin":D
    iget-wide v4, p0, Lcom/vividsolutions/jts/math/Vector2D;->x:D

    mul-double/2addr v4, v0

    iget-wide v6, p0, Lcom/vividsolutions/jts/math/Vector2D;->y:D

    mul-double/2addr v6, v2

    sub-double/2addr v4, v6

    iget-wide v6, p0, Lcom/vividsolutions/jts/math/Vector2D;->x:D

    mul-double/2addr v6, v2

    iget-wide v8, p0, Lcom/vividsolutions/jts/math/Vector2D;->y:D

    mul-double/2addr v8, v0

    add-double/2addr v6, v8

    invoke-static {v4, v5, v6, v7}, Lcom/vividsolutions/jts/math/Vector2D;->create(DD)Lcom/vividsolutions/jts/math/Vector2D;

    move-result-object v4

    return-object v4
.end method

.method public rotateByQuarterCircle(I)Lcom/vividsolutions/jts/math/Vector2D;
    .locals 6
    .param p1, "numQuarters"    # I

    .prologue
    .line 279
    rem-int/lit8 v0, p1, 0x4

    .line 280
    .local v0, "nQuad":I
    if-gez p1, :cond_0

    if-eqz v0, :cond_0

    .line 281
    add-int/lit8 v0, v0, 0x4

    .line 283
    :cond_0
    packed-switch v0, :pswitch_data_0

    .line 293
    invoke-static {}, Lcom/vividsolutions/jts/util/Assert;->shouldNeverReachHere()V

    .line 294
    const/4 v1, 0x0

    :goto_0
    return-object v1

    .line 285
    :pswitch_0
    iget-wide v2, p0, Lcom/vividsolutions/jts/math/Vector2D;->x:D

    iget-wide v4, p0, Lcom/vividsolutions/jts/math/Vector2D;->y:D

    invoke-static {v2, v3, v4, v5}, Lcom/vividsolutions/jts/math/Vector2D;->create(DD)Lcom/vividsolutions/jts/math/Vector2D;

    move-result-object v1

    goto :goto_0

    .line 287
    :pswitch_1
    iget-wide v2, p0, Lcom/vividsolutions/jts/math/Vector2D;->y:D

    neg-double v2, v2

    iget-wide v4, p0, Lcom/vividsolutions/jts/math/Vector2D;->x:D

    invoke-static {v2, v3, v4, v5}, Lcom/vividsolutions/jts/math/Vector2D;->create(DD)Lcom/vividsolutions/jts/math/Vector2D;

    move-result-object v1

    goto :goto_0

    .line 289
    :pswitch_2
    iget-wide v2, p0, Lcom/vividsolutions/jts/math/Vector2D;->x:D

    neg-double v2, v2

    iget-wide v4, p0, Lcom/vividsolutions/jts/math/Vector2D;->y:D

    neg-double v4, v4

    invoke-static {v2, v3, v4, v5}, Lcom/vividsolutions/jts/math/Vector2D;->create(DD)Lcom/vividsolutions/jts/math/Vector2D;

    move-result-object v1

    goto :goto_0

    .line 291
    :pswitch_3
    iget-wide v2, p0, Lcom/vividsolutions/jts/math/Vector2D;->y:D

    iget-wide v4, p0, Lcom/vividsolutions/jts/math/Vector2D;->x:D

    neg-double v4, v4

    invoke-static {v2, v3, v4, v5}, Lcom/vividsolutions/jts/math/Vector2D;->create(DD)Lcom/vividsolutions/jts/math/Vector2D;

    move-result-object v1

    goto :goto_0

    .line 283
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public subtract(Lcom/vividsolutions/jts/math/Vector2D;)Lcom/vividsolutions/jts/math/Vector2D;
    .locals 6
    .param p1, "v"    # Lcom/vividsolutions/jts/math/Vector2D;

    .prologue
    .line 144
    iget-wide v0, p0, Lcom/vividsolutions/jts/math/Vector2D;->x:D

    iget-wide v2, p1, Lcom/vividsolutions/jts/math/Vector2D;->x:D

    sub-double/2addr v0, v2

    iget-wide v2, p0, Lcom/vividsolutions/jts/math/Vector2D;->y:D

    iget-wide v4, p1, Lcom/vividsolutions/jts/math/Vector2D;->y:D

    sub-double/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Lcom/vividsolutions/jts/math/Vector2D;->create(DD)Lcom/vividsolutions/jts/math/Vector2D;

    move-result-object v0

    return-object v0
.end method

.method public toCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 6

    .prologue
    .line 307
    new-instance v0, Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v2, p0, Lcom/vividsolutions/jts/math/Vector2D;->x:D

    iget-wide v4, p0, Lcom/vividsolutions/jts/math/Vector2D;->y:D

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DD)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 326
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/vividsolutions/jts/math/Vector2D;->x:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/vividsolutions/jts/math/Vector2D;->y:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public translate(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 8
    .param p1, "coord"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 303
    new-instance v0, Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v2, p0, Lcom/vividsolutions/jts/math/Vector2D;->x:D

    iget-wide v4, p1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    add-double/2addr v2, v4

    iget-wide v4, p0, Lcom/vividsolutions/jts/math/Vector2D;->y:D

    iget-wide v6, p1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    add-double/2addr v4, v6

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DD)V

    return-object v0
.end method

.method public weightedSum(Lcom/vividsolutions/jts/math/Vector2D;D)Lcom/vividsolutions/jts/math/Vector2D;
    .locals 8
    .param p1, "v"    # Lcom/vividsolutions/jts/math/Vector2D;
    .param p2, "frac"    # D

    .prologue
    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    .line 206
    iget-wide v0, p0, Lcom/vividsolutions/jts/math/Vector2D;->x:D

    mul-double/2addr v0, p2

    sub-double v2, v6, p2

    iget-wide v4, p1, Lcom/vividsolutions/jts/math/Vector2D;->x:D

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    iget-wide v2, p0, Lcom/vividsolutions/jts/math/Vector2D;->y:D

    mul-double/2addr v2, p2

    sub-double v4, v6, p2

    iget-wide v6, p1, Lcom/vividsolutions/jts/math/Vector2D;->y:D

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Lcom/vividsolutions/jts/math/Vector2D;->create(DD)Lcom/vividsolutions/jts/math/Vector2D;

    move-result-object v0

    return-object v0
.end method

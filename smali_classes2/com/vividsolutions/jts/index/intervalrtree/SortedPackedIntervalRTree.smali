.class public Lcom/vividsolutions/jts/index/intervalrtree/SortedPackedIntervalRTree;
.super Ljava/lang/Object;
.source "SortedPackedIntervalRTree.java"


# instance fields
.field private leaves:Ljava/util/List;

.field private level:I

.field private root:Lcom/vividsolutions/jts/index/intervalrtree/IntervalRTreeNode;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/index/intervalrtree/SortedPackedIntervalRTree;->leaves:Ljava/util/List;

    .line 59
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vividsolutions/jts/index/intervalrtree/SortedPackedIntervalRTree;->root:Lcom/vividsolutions/jts/index/intervalrtree/IntervalRTreeNode;

    .line 116
    const/4 v0, 0x0

    iput v0, p0, Lcom/vividsolutions/jts/index/intervalrtree/SortedPackedIntervalRTree;->level:I

    .line 64
    return-void
.end method

.method private buildLevel(Ljava/util/List;Ljava/util/List;)V
    .locals 6
    .param p1, "src"    # Ljava/util/List;
    .param p2, "dest"    # Ljava/util/List;

    .prologue
    .line 120
    iget v4, p0, Lcom/vividsolutions/jts/index/intervalrtree/SortedPackedIntervalRTree;->level:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/vividsolutions/jts/index/intervalrtree/SortedPackedIntervalRTree;->level:I

    .line 121
    invoke-interface {p2}, Ljava/util/List;->clear()V

    .line 122
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    if-ge v0, v4, :cond_2

    .line 123
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/index/intervalrtree/IntervalRTreeNode;

    .line 124
    .local v1, "n1":Lcom/vividsolutions/jts/index/intervalrtree/IntervalRTreeNode;
    add-int/lit8 v4, v0, 0x1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    if-ge v4, v5, :cond_0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vividsolutions/jts/index/intervalrtree/IntervalRTreeNode;

    move-object v2, v4

    .line 126
    .local v2, "n2":Lcom/vividsolutions/jts/index/intervalrtree/IntervalRTreeNode;
    :goto_1
    if-nez v2, :cond_1

    .line 127
    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 122
    :goto_2
    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    .line 124
    .end local v2    # "n2":Lcom/vividsolutions/jts/index/intervalrtree/IntervalRTreeNode;
    :cond_0
    const/4 v2, 0x0

    goto :goto_1

    .line 129
    .restart local v2    # "n2":Lcom/vividsolutions/jts/index/intervalrtree/IntervalRTreeNode;
    :cond_1
    new-instance v3, Lcom/vividsolutions/jts/index/intervalrtree/IntervalRTreeBranchNode;

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vividsolutions/jts/index/intervalrtree/IntervalRTreeNode;

    add-int/lit8 v5, v0, 0x1

    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/vividsolutions/jts/index/intervalrtree/IntervalRTreeNode;

    invoke-direct {v3, v4, v5}, Lcom/vividsolutions/jts/index/intervalrtree/IntervalRTreeBranchNode;-><init>(Lcom/vividsolutions/jts/index/intervalrtree/IntervalRTreeNode;Lcom/vividsolutions/jts/index/intervalrtree/IntervalRTreeNode;)V

    .line 134
    .local v3, "node":Lcom/vividsolutions/jts/index/intervalrtree/IntervalRTreeNode;
    invoke-interface {p2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 137
    .end local v1    # "n1":Lcom/vividsolutions/jts/index/intervalrtree/IntervalRTreeNode;
    .end local v2    # "n2":Lcom/vividsolutions/jts/index/intervalrtree/IntervalRTreeNode;
    .end local v3    # "node":Lcom/vividsolutions/jts/index/intervalrtree/IntervalRTreeNode;
    :cond_2
    return-void
.end method

.method private declared-synchronized buildRoot()V
    .locals 1

    .prologue
    .line 90
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/vividsolutions/jts/index/intervalrtree/SortedPackedIntervalRTree;->root:Lcom/vividsolutions/jts/index/intervalrtree/IntervalRTreeNode;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 92
    :goto_0
    monitor-exit p0

    return-void

    .line 91
    :cond_0
    :try_start_1
    invoke-direct {p0}, Lcom/vividsolutions/jts/index/intervalrtree/SortedPackedIntervalRTree;->buildTree()Lcom/vividsolutions/jts/index/intervalrtree/IntervalRTreeNode;

    move-result-object v0

    iput-object v0, p0, Lcom/vividsolutions/jts/index/intervalrtree/SortedPackedIntervalRTree;->root:Lcom/vividsolutions/jts/index/intervalrtree/IntervalRTreeNode;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 90
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private buildTree()Lcom/vividsolutions/jts/index/intervalrtree/IntervalRTreeNode;
    .locals 5

    .prologue
    .line 98
    iget-object v3, p0, Lcom/vividsolutions/jts/index/intervalrtree/SortedPackedIntervalRTree;->leaves:Ljava/util/List;

    new-instance v4, Lcom/vividsolutions/jts/index/intervalrtree/IntervalRTreeNode$NodeComparator;

    invoke-direct {v4}, Lcom/vividsolutions/jts/index/intervalrtree/IntervalRTreeNode$NodeComparator;-><init>()V

    invoke-static {v3, v4}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 101
    iget-object v1, p0, Lcom/vividsolutions/jts/index/intervalrtree/SortedPackedIntervalRTree;->leaves:Ljava/util/List;

    .line 102
    .local v1, "src":Ljava/util/List;
    const/4 v2, 0x0

    .line 103
    .local v2, "temp":Ljava/util/List;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 106
    .local v0, "dest":Ljava/util/List;
    :goto_0
    invoke-direct {p0, v1, v0}, Lcom/vividsolutions/jts/index/intervalrtree/SortedPackedIntervalRTree;->buildLevel(Ljava/util/List;Ljava/util/List;)V

    .line 107
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 108
    const/4 v3, 0x0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vividsolutions/jts/index/intervalrtree/IntervalRTreeNode;

    return-object v3

    .line 110
    :cond_0
    move-object v2, v1

    .line 111
    move-object v1, v0

    .line 112
    move-object v0, v2

    goto :goto_0
.end method

.method private init()V
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/vividsolutions/jts/index/intervalrtree/SortedPackedIntervalRTree;->root:Lcom/vividsolutions/jts/index/intervalrtree/IntervalRTreeNode;

    if-eqz v0, :cond_0

    .line 86
    :goto_0
    return-void

    .line 85
    :cond_0
    invoke-direct {p0}, Lcom/vividsolutions/jts/index/intervalrtree/SortedPackedIntervalRTree;->buildRoot()V

    goto :goto_0
.end method

.method private printNode(Lcom/vividsolutions/jts/index/intervalrtree/IntervalRTreeNode;)V
    .locals 8
    .param p1, "node"    # Lcom/vividsolutions/jts/index/intervalrtree/IntervalRTreeNode;

    .prologue
    .line 141
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v2, p1, Lcom/vividsolutions/jts/index/intervalrtree/IntervalRTreeNode;->min:D

    iget v4, p0, Lcom/vividsolutions/jts/index/intervalrtree/SortedPackedIntervalRTree;->level:I

    int-to-double v4, v4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DD)V

    new-instance v2, Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v4, p1, Lcom/vividsolutions/jts/index/intervalrtree/IntervalRTreeNode;->max:D

    iget v3, p0, Lcom/vividsolutions/jts/index/intervalrtree/SortedPackedIntervalRTree;->level:I

    int-to-double v6, v3

    invoke-direct {v2, v4, v5, v6, v7}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DD)V

    invoke-static {v1, v2}, Lcom/vividsolutions/jts/io/WKTWriter;->toLineString(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 142
    return-void
.end method


# virtual methods
.method public insert(DDLjava/lang/Object;)V
    .locals 7
    .param p1, "min"    # D
    .param p3, "max"    # D
    .param p5, "item"    # Ljava/lang/Object;

    .prologue
    .line 77
    iget-object v0, p0, Lcom/vividsolutions/jts/index/intervalrtree/SortedPackedIntervalRTree;->root:Lcom/vividsolutions/jts/index/intervalrtree/IntervalRTreeNode;

    if-eqz v0, :cond_0

    .line 78
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Index cannot be added to once it has been queried"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 79
    :cond_0
    iget-object v0, p0, Lcom/vividsolutions/jts/index/intervalrtree/SortedPackedIntervalRTree;->leaves:Ljava/util/List;

    new-instance v1, Lcom/vividsolutions/jts/index/intervalrtree/IntervalRTreeLeafNode;

    move-wide v2, p1

    move-wide v4, p3

    move-object v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/vividsolutions/jts/index/intervalrtree/IntervalRTreeLeafNode;-><init>(DDLjava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 80
    return-void
.end method

.method public query(DDLcom/vividsolutions/jts/index/ItemVisitor;)V
    .locals 7
    .param p1, "min"    # D
    .param p3, "max"    # D
    .param p5, "visitor"    # Lcom/vividsolutions/jts/index/ItemVisitor;

    .prologue
    .line 154
    invoke-direct {p0}, Lcom/vividsolutions/jts/index/intervalrtree/SortedPackedIntervalRTree;->init()V

    .line 156
    iget-object v1, p0, Lcom/vividsolutions/jts/index/intervalrtree/SortedPackedIntervalRTree;->root:Lcom/vividsolutions/jts/index/intervalrtree/IntervalRTreeNode;

    move-wide v2, p1

    move-wide v4, p3

    move-object v6, p5

    invoke-virtual/range {v1 .. v6}, Lcom/vividsolutions/jts/index/intervalrtree/IntervalRTreeNode;->query(DDLcom/vividsolutions/jts/index/ItemVisitor;)V

    .line 157
    return-void
.end method

.class public Lcom/vividsolutions/jts/triangulate/NonEncroachingSplitPointFinder;
.super Ljava/lang/Object;
.source "NonEncroachingSplitPointFinder.java"

# interfaces
.implements Lcom/vividsolutions/jts/triangulate/ConstraintSplitPointFinder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static projectedSplitPoint(Lcom/vividsolutions/jts/triangulate/Segment;Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 2
    .param p0, "seg"    # Lcom/vividsolutions/jts/triangulate/Segment;
    .param p1, "encroachPt"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 90
    invoke-virtual {p0}, Lcom/vividsolutions/jts/triangulate/Segment;->getLineSegment()Lcom/vividsolutions/jts/geom/LineSegment;

    move-result-object v0

    .line 91
    .local v0, "lineSeg":Lcom/vividsolutions/jts/geom/LineSegment;
    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/geom/LineSegment;->project(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    .line 92
    .local v1, "projPt":Lcom/vividsolutions/jts/geom/Coordinate;
    return-object v1
.end method


# virtual methods
.method public findSplitPoint(Lcom/vividsolutions/jts/triangulate/Segment;Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 18
    .param p1, "seg"    # Lcom/vividsolutions/jts/triangulate/Segment;
    .param p2, "encroachPt"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 59
    invoke-virtual/range {p1 .. p1}, Lcom/vividsolutions/jts/triangulate/Segment;->getLineSegment()Lcom/vividsolutions/jts/geom/LineSegment;

    move-result-object v2

    .line 60
    .local v2, "lineSeg":Lcom/vividsolutions/jts/geom/LineSegment;
    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/LineSegment;->getLength()D

    move-result-wide v10

    .line 61
    .local v10, "segLen":D
    const-wide/high16 v14, 0x4000000000000000L    # 2.0

    div-double v6, v10, v14

    .line 62
    .local v6, "midPtLen":D
    new-instance v12, Lcom/vividsolutions/jts/triangulate/SplitSegment;

    invoke-direct {v12, v2}, Lcom/vividsolutions/jts/triangulate/SplitSegment;-><init>(Lcom/vividsolutions/jts/geom/LineSegment;)V

    .line 64
    .local v12, "splitSeg":Lcom/vividsolutions/jts/triangulate/SplitSegment;
    invoke-static/range {p1 .. p2}, Lcom/vividsolutions/jts/triangulate/NonEncroachingSplitPointFinder;->projectedSplitPoint(Lcom/vividsolutions/jts/triangulate/Segment;Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v3

    .line 70
    .local v3, "projPt":Lcom/vividsolutions/jts/geom/Coordinate;
    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Lcom/vividsolutions/jts/geom/Coordinate;->distance(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v14

    const-wide/high16 v16, 0x4000000000000000L    # 2.0

    mul-double v14, v14, v16

    const-wide v16, 0x3fe999999999999aL    # 0.8

    mul-double v8, v14, v16

    .line 71
    .local v8, "nonEncroachDiam":D
    move-wide v4, v8

    .line 72
    .local v4, "maxSplitLen":D
    cmpl-double v13, v4, v6

    if-lez v13, :cond_0

    .line 73
    move-wide v4, v6

    .line 75
    :cond_0
    invoke-virtual {v12, v4, v5}, Lcom/vividsolutions/jts/triangulate/SplitSegment;->setMinimumLength(D)V

    .line 77
    invoke-virtual {v12, v3}, Lcom/vividsolutions/jts/triangulate/SplitSegment;->splitAt(Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 79
    invoke-virtual {v12}, Lcom/vividsolutions/jts/triangulate/SplitSegment;->getSplitPoint()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v13

    return-object v13
.end method

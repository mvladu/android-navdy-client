.class public Lcom/vividsolutions/jts/noding/SegmentStringDissolver;
.super Ljava/lang/Object;
.source "SegmentStringDissolver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vividsolutions/jts/noding/SegmentStringDissolver$SegmentStringMerger;
    }
.end annotation


# instance fields
.field private merger:Lcom/vividsolutions/jts/noding/SegmentStringDissolver$SegmentStringMerger;

.field private ocaMap:Ljava/util/Map;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 97
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/vividsolutions/jts/noding/SegmentStringDissolver;-><init>(Lcom/vividsolutions/jts/noding/SegmentStringDissolver$SegmentStringMerger;)V

    .line 98
    return-void
.end method

.method public constructor <init>(Lcom/vividsolutions/jts/noding/SegmentStringDissolver$SegmentStringMerger;)V
    .locals 1
    .param p1, "merger"    # Lcom/vividsolutions/jts/noding/SegmentStringDissolver$SegmentStringMerger;

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/noding/SegmentStringDissolver;->ocaMap:Ljava/util/Map;

    .line 90
    iput-object p1, p0, Lcom/vividsolutions/jts/noding/SegmentStringDissolver;->merger:Lcom/vividsolutions/jts/noding/SegmentStringDissolver$SegmentStringMerger;

    .line 91
    return-void
.end method

.method private add(Lcom/vividsolutions/jts/noding/OrientedCoordinateArray;Lcom/vividsolutions/jts/noding/SegmentString;)V
    .locals 1
    .param p1, "oca"    # Lcom/vividsolutions/jts/noding/OrientedCoordinateArray;
    .param p2, "segString"    # Lcom/vividsolutions/jts/noding/SegmentString;

    .prologue
    .line 113
    iget-object v0, p0, Lcom/vividsolutions/jts/noding/SegmentStringDissolver;->ocaMap:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    return-void
.end method

.method private findMatching(Lcom/vividsolutions/jts/noding/OrientedCoordinateArray;Lcom/vividsolutions/jts/noding/SegmentString;)Lcom/vividsolutions/jts/noding/SegmentString;
    .locals 2
    .param p1, "oca"    # Lcom/vividsolutions/jts/noding/OrientedCoordinateArray;
    .param p2, "segString"    # Lcom/vividsolutions/jts/noding/SegmentString;

    .prologue
    .line 141
    iget-object v1, p0, Lcom/vividsolutions/jts/noding/SegmentStringDissolver;->ocaMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/noding/SegmentString;

    .line 148
    .local v0, "matchSS":Lcom/vividsolutions/jts/noding/SegmentString;
    return-object v0
.end method


# virtual methods
.method public dissolve(Lcom/vividsolutions/jts/noding/SegmentString;)V
    .locals 5
    .param p1, "segString"    # Lcom/vividsolutions/jts/noding/SegmentString;

    .prologue
    .line 124
    new-instance v2, Lcom/vividsolutions/jts/noding/OrientedCoordinateArray;

    invoke-interface {p1}, Lcom/vividsolutions/jts/noding/SegmentString;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/vividsolutions/jts/noding/OrientedCoordinateArray;-><init>([Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 125
    .local v2, "oca":Lcom/vividsolutions/jts/noding/OrientedCoordinateArray;
    invoke-direct {p0, v2, p1}, Lcom/vividsolutions/jts/noding/SegmentStringDissolver;->findMatching(Lcom/vividsolutions/jts/noding/OrientedCoordinateArray;Lcom/vividsolutions/jts/noding/SegmentString;)Lcom/vividsolutions/jts/noding/SegmentString;

    move-result-object v0

    .line 126
    .local v0, "existing":Lcom/vividsolutions/jts/noding/SegmentString;
    if-nez v0, :cond_1

    .line 127
    invoke-direct {p0, v2, p1}, Lcom/vividsolutions/jts/noding/SegmentStringDissolver;->add(Lcom/vividsolutions/jts/noding/OrientedCoordinateArray;Lcom/vividsolutions/jts/noding/SegmentString;)V

    .line 136
    :cond_0
    :goto_0
    return-void

    .line 130
    :cond_1
    iget-object v3, p0, Lcom/vividsolutions/jts/noding/SegmentStringDissolver;->merger:Lcom/vividsolutions/jts/noding/SegmentStringDissolver$SegmentStringMerger;

    if-eqz v3, :cond_0

    .line 131
    invoke-interface {v0}, Lcom/vividsolutions/jts/noding/SegmentString;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v3

    invoke-interface {p1}, Lcom/vividsolutions/jts/noding/SegmentString;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/vividsolutions/jts/geom/CoordinateArrays;->equals([Lcom/vividsolutions/jts/geom/Coordinate;[Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v1

    .line 133
    .local v1, "isSameOrientation":Z
    iget-object v3, p0, Lcom/vividsolutions/jts/noding/SegmentStringDissolver;->merger:Lcom/vividsolutions/jts/noding/SegmentStringDissolver$SegmentStringMerger;

    invoke-interface {v3, v0, p1, v1}, Lcom/vividsolutions/jts/noding/SegmentStringDissolver$SegmentStringMerger;->merge(Lcom/vividsolutions/jts/noding/SegmentString;Lcom/vividsolutions/jts/noding/SegmentString;Z)V

    goto :goto_0
.end method

.method public dissolve(Ljava/util/Collection;)V
    .locals 2
    .param p1, "segStrings"    # Ljava/util/Collection;

    .prologue
    .line 106
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 107
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/noding/SegmentString;

    invoke-virtual {p0, v1}, Lcom/vividsolutions/jts/noding/SegmentStringDissolver;->dissolve(Lcom/vividsolutions/jts/noding/SegmentString;)V

    goto :goto_0

    .line 109
    :cond_0
    return-void
.end method

.method public getDissolved()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/vividsolutions/jts/noding/SegmentStringDissolver;->ocaMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

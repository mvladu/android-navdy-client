.class public Lcom/vividsolutions/jts/util/CoordinateArrayFilter;
.super Ljava/lang/Object;
.source "CoordinateArrayFilter.java"

# interfaces
.implements Lcom/vividsolutions/jts/geom/CoordinateFilter;


# instance fields
.field n:I

.field pts:[Lcom/vividsolutions/jts/geom/Coordinate;


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vividsolutions/jts/util/CoordinateArrayFilter;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    .line 47
    const/4 v0, 0x0

    iput v0, p0, Lcom/vividsolutions/jts/util/CoordinateArrayFilter;->n:I

    .line 56
    new-array v0, p1, [Lcom/vividsolutions/jts/geom/Coordinate;

    iput-object v0, p0, Lcom/vividsolutions/jts/util/CoordinateArrayFilter;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    .line 57
    return-void
.end method


# virtual methods
.method public filter(Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 3
    .param p1, "coord"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/vividsolutions/jts/util/CoordinateArrayFilter;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    iget v1, p0, Lcom/vividsolutions/jts/util/CoordinateArrayFilter;->n:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/vividsolutions/jts/util/CoordinateArrayFilter;->n:I

    aput-object p1, v0, v1

    .line 70
    return-void
.end method

.method public getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/vividsolutions/jts/util/CoordinateArrayFilter;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    return-object v0
.end method

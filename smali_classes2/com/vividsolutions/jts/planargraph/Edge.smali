.class public Lcom/vividsolutions/jts/planargraph/Edge;
.super Lcom/vividsolutions/jts/planargraph/GraphComponent;
.source "Edge.java"


# instance fields
.field protected dirEdge:[Lcom/vividsolutions/jts/planargraph/DirectedEdge;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/vividsolutions/jts/planargraph/GraphComponent;-><init>()V

    .line 62
    return-void
.end method

.method public constructor <init>(Lcom/vividsolutions/jts/planargraph/DirectedEdge;Lcom/vividsolutions/jts/planargraph/DirectedEdge;)V
    .locals 0
    .param p1, "de0"    # Lcom/vividsolutions/jts/planargraph/DirectedEdge;
    .param p2, "de1"    # Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/vividsolutions/jts/planargraph/GraphComponent;-><init>()V

    .line 71
    invoke-virtual {p0, p1, p2}, Lcom/vividsolutions/jts/planargraph/Edge;->setDirectedEdges(Lcom/vividsolutions/jts/planargraph/DirectedEdge;Lcom/vividsolutions/jts/planargraph/DirectedEdge;)V

    .line 72
    return-void
.end method


# virtual methods
.method public getDirEdge(I)Lcom/vividsolutions/jts/planargraph/DirectedEdge;
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 95
    iget-object v0, p0, Lcom/vividsolutions/jts/planargraph/Edge;->dirEdge:[Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getDirEdge(Lcom/vividsolutions/jts/planargraph/Node;)Lcom/vividsolutions/jts/planargraph/DirectedEdge;
    .locals 3
    .param p1, "fromNode"    # Lcom/vividsolutions/jts/planargraph/Node;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 104
    iget-object v0, p0, Lcom/vividsolutions/jts/planargraph/Edge;->dirEdge:[Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/vividsolutions/jts/planargraph/DirectedEdge;->getFromNode()Lcom/vividsolutions/jts/planargraph/Node;

    move-result-object v0

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/vividsolutions/jts/planargraph/Edge;->dirEdge:[Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    aget-object v0, v0, v1

    .line 108
    :goto_0
    return-object v0

    .line 105
    :cond_0
    iget-object v0, p0, Lcom/vividsolutions/jts/planargraph/Edge;->dirEdge:[Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    aget-object v0, v0, v2

    invoke-virtual {v0}, Lcom/vividsolutions/jts/planargraph/DirectedEdge;->getFromNode()Lcom/vividsolutions/jts/planargraph/Node;

    move-result-object v0

    if-ne v0, p1, :cond_1

    iget-object v0, p0, Lcom/vividsolutions/jts/planargraph/Edge;->dirEdge:[Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    aget-object v0, v0, v2

    goto :goto_0

    .line 108
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getOppositeNode(Lcom/vividsolutions/jts/planargraph/Node;)Lcom/vividsolutions/jts/planargraph/Node;
    .locals 3
    .param p1, "node"    # Lcom/vividsolutions/jts/planargraph/Node;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 117
    iget-object v0, p0, Lcom/vividsolutions/jts/planargraph/Edge;->dirEdge:[Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/vividsolutions/jts/planargraph/DirectedEdge;->getFromNode()Lcom/vividsolutions/jts/planargraph/Node;

    move-result-object v0

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/vividsolutions/jts/planargraph/Edge;->dirEdge:[Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/vividsolutions/jts/planargraph/DirectedEdge;->getToNode()Lcom/vividsolutions/jts/planargraph/Node;

    move-result-object v0

    .line 121
    :goto_0
    return-object v0

    .line 118
    :cond_0
    iget-object v0, p0, Lcom/vividsolutions/jts/planargraph/Edge;->dirEdge:[Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    aget-object v0, v0, v2

    invoke-virtual {v0}, Lcom/vividsolutions/jts/planargraph/DirectedEdge;->getFromNode()Lcom/vividsolutions/jts/planargraph/Node;

    move-result-object v0

    if-ne v0, p1, :cond_1

    iget-object v0, p0, Lcom/vividsolutions/jts/planargraph/Edge;->dirEdge:[Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    aget-object v0, v0, v2

    invoke-virtual {v0}, Lcom/vividsolutions/jts/planargraph/DirectedEdge;->getToNode()Lcom/vividsolutions/jts/planargraph/Node;

    move-result-object v0

    goto :goto_0

    .line 121
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isRemoved()Z
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/vividsolutions/jts/planargraph/Edge;->dirEdge:[Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method remove()V
    .locals 1

    .prologue
    .line 128
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vividsolutions/jts/planargraph/Edge;->dirEdge:[Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    .line 129
    return-void
.end method

.method public setDirectedEdges(Lcom/vividsolutions/jts/planargraph/DirectedEdge;Lcom/vividsolutions/jts/planargraph/DirectedEdge;)V
    .locals 2
    .param p1, "de0"    # Lcom/vividsolutions/jts/planargraph/DirectedEdge;
    .param p2, "de1"    # Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    .prologue
    .line 80
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    iput-object v0, p0, Lcom/vividsolutions/jts/planargraph/Edge;->dirEdge:[Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    .line 81
    invoke-virtual {p1, p0}, Lcom/vividsolutions/jts/planargraph/DirectedEdge;->setEdge(Lcom/vividsolutions/jts/planargraph/Edge;)V

    .line 82
    invoke-virtual {p2, p0}, Lcom/vividsolutions/jts/planargraph/DirectedEdge;->setEdge(Lcom/vividsolutions/jts/planargraph/Edge;)V

    .line 83
    invoke-virtual {p1, p2}, Lcom/vividsolutions/jts/planargraph/DirectedEdge;->setSym(Lcom/vividsolutions/jts/planargraph/DirectedEdge;)V

    .line 84
    invoke-virtual {p2, p1}, Lcom/vividsolutions/jts/planargraph/DirectedEdge;->setSym(Lcom/vividsolutions/jts/planargraph/DirectedEdge;)V

    .line 85
    invoke-virtual {p1}, Lcom/vividsolutions/jts/planargraph/DirectedEdge;->getFromNode()Lcom/vividsolutions/jts/planargraph/Node;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/planargraph/Node;->addOutEdge(Lcom/vividsolutions/jts/planargraph/DirectedEdge;)V

    .line 86
    invoke-virtual {p2}, Lcom/vividsolutions/jts/planargraph/DirectedEdge;->getFromNode()Lcom/vividsolutions/jts/planargraph/Node;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/vividsolutions/jts/planargraph/Node;->addOutEdge(Lcom/vividsolutions/jts/planargraph/DirectedEdge;)V

    .line 87
    return-void
.end method

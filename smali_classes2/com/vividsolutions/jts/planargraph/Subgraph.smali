.class public Lcom/vividsolutions/jts/planargraph/Subgraph;
.super Ljava/lang/Object;
.source "Subgraph.java"


# instance fields
.field protected dirEdges:Ljava/util/List;

.field protected edges:Ljava/util/Set;

.field protected nodeMap:Lcom/vividsolutions/jts/planargraph/NodeMap;

.field protected parentGraph:Lcom/vividsolutions/jts/planargraph/PlanarGraph;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/planargraph/PlanarGraph;)V
    .locals 1
    .param p1, "parentGraph"    # Lcom/vividsolutions/jts/planargraph/PlanarGraph;

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/planargraph/Subgraph;->edges:Ljava/util/Set;

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/planargraph/Subgraph;->dirEdges:Ljava/util/List;

    .line 52
    new-instance v0, Lcom/vividsolutions/jts/planargraph/NodeMap;

    invoke-direct {v0}, Lcom/vividsolutions/jts/planargraph/NodeMap;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/planargraph/Subgraph;->nodeMap:Lcom/vividsolutions/jts/planargraph/NodeMap;

    .line 60
    iput-object p1, p0, Lcom/vividsolutions/jts/planargraph/Subgraph;->parentGraph:Lcom/vividsolutions/jts/planargraph/PlanarGraph;

    .line 61
    return-void
.end method


# virtual methods
.method public add(Lcom/vividsolutions/jts/planargraph/Edge;)V
    .locals 4
    .param p1, "e"    # Lcom/vividsolutions/jts/planargraph/Edge;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 82
    iget-object v0, p0, Lcom/vividsolutions/jts/planargraph/Subgraph;->edges:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    :goto_0
    return-void

    .line 84
    :cond_0
    iget-object v0, p0, Lcom/vividsolutions/jts/planargraph/Subgraph;->edges:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 85
    iget-object v0, p0, Lcom/vividsolutions/jts/planargraph/Subgraph;->dirEdges:Ljava/util/List;

    invoke-virtual {p1, v2}, Lcom/vividsolutions/jts/planargraph/Edge;->getDirEdge(I)Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 86
    iget-object v0, p0, Lcom/vividsolutions/jts/planargraph/Subgraph;->dirEdges:Ljava/util/List;

    invoke-virtual {p1, v3}, Lcom/vividsolutions/jts/planargraph/Edge;->getDirEdge(I)Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 87
    iget-object v0, p0, Lcom/vividsolutions/jts/planargraph/Subgraph;->nodeMap:Lcom/vividsolutions/jts/planargraph/NodeMap;

    invoke-virtual {p1, v2}, Lcom/vividsolutions/jts/planargraph/Edge;->getDirEdge(I)Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vividsolutions/jts/planargraph/DirectedEdge;->getFromNode()Lcom/vividsolutions/jts/planargraph/Node;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/planargraph/NodeMap;->add(Lcom/vividsolutions/jts/planargraph/Node;)Lcom/vividsolutions/jts/planargraph/Node;

    .line 88
    iget-object v0, p0, Lcom/vividsolutions/jts/planargraph/Subgraph;->nodeMap:Lcom/vividsolutions/jts/planargraph/NodeMap;

    invoke-virtual {p1, v3}, Lcom/vividsolutions/jts/planargraph/Edge;->getDirEdge(I)Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vividsolutions/jts/planargraph/DirectedEdge;->getFromNode()Lcom/vividsolutions/jts/planargraph/Node;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/planargraph/NodeMap;->add(Lcom/vividsolutions/jts/planargraph/Node;)Lcom/vividsolutions/jts/planargraph/Node;

    goto :goto_0
.end method

.method public contains(Lcom/vividsolutions/jts/planargraph/Edge;)Z
    .locals 1
    .param p1, "e"    # Lcom/vividsolutions/jts/planargraph/Edge;

    .prologue
    .line 122
    iget-object v0, p0, Lcom/vividsolutions/jts/planargraph/Subgraph;->edges:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public dirEdgeIterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/vividsolutions/jts/planargraph/Subgraph;->dirEdges:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public edgeIterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/vividsolutions/jts/planargraph/Subgraph;->edges:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public getParent()Lcom/vividsolutions/jts/planargraph/PlanarGraph;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/vividsolutions/jts/planargraph/Subgraph;->parentGraph:Lcom/vividsolutions/jts/planargraph/PlanarGraph;

    return-object v0
.end method

.method public nodeIterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/vividsolutions/jts/planargraph/Subgraph;->nodeMap:Lcom/vividsolutions/jts/planargraph/NodeMap;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/planargraph/NodeMap;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

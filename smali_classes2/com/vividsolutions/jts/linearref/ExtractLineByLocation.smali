.class Lcom/vividsolutions/jts/linearref/ExtractLineByLocation;
.super Ljava/lang/Object;
.source "ExtractLineByLocation.java"


# instance fields
.field private line:Lcom/vividsolutions/jts/geom/Geometry;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 0
    .param p1, "line"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    iput-object p1, p0, Lcom/vividsolutions/jts/linearref/ExtractLineByLocation;->line:Lcom/vividsolutions/jts/geom/Geometry;

    .line 66
    return-void
.end method

.method private computeLine(Lcom/vividsolutions/jts/linearref/LinearLocation;Lcom/vividsolutions/jts/linearref/LinearLocation;)Lcom/vividsolutions/jts/geom/LineString;
    .locals 12
    .param p1, "start"    # Lcom/vividsolutions/jts/linearref/LinearLocation;
    .param p2, "end"    # Lcom/vividsolutions/jts/linearref/LinearLocation;

    .prologue
    .line 102
    iget-object v7, p0, Lcom/vividsolutions/jts/linearref/ExtractLineByLocation;->line:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v7}, Lcom/vividsolutions/jts/geom/Geometry;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    .line 103
    .local v0, "coordinates":[Lcom/vividsolutions/jts/geom/Coordinate;
    new-instance v5, Lcom/vividsolutions/jts/geom/CoordinateList;

    invoke-direct {v5}, Lcom/vividsolutions/jts/geom/CoordinateList;-><init>()V

    .line 105
    .local v5, "newCoordinates":Lcom/vividsolutions/jts/geom/CoordinateList;
    invoke-virtual {p1}, Lcom/vividsolutions/jts/linearref/LinearLocation;->getSegmentIndex()I

    move-result v6

    .line 106
    .local v6, "startSegmentIndex":I
    invoke-virtual {p1}, Lcom/vividsolutions/jts/linearref/LinearLocation;->getSegmentFraction()D

    move-result-wide v8

    const-wide/16 v10, 0x0

    cmpl-double v7, v8, v10

    if-lez v7, :cond_0

    .line 107
    add-int/lit8 v6, v6, 0x1

    .line 108
    :cond_0
    invoke-virtual {p2}, Lcom/vividsolutions/jts/linearref/LinearLocation;->getSegmentIndex()I

    move-result v2

    .line 109
    .local v2, "lastSegmentIndex":I
    invoke-virtual {p2}, Lcom/vividsolutions/jts/linearref/LinearLocation;->getSegmentFraction()D

    move-result-wide v8

    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    cmpl-double v7, v8, v10

    if-nez v7, :cond_1

    .line 110
    add-int/lit8 v2, v2, 0x1

    .line 111
    :cond_1
    array-length v7, v0

    if-lt v2, v7, :cond_2

    .line 112
    array-length v7, v0

    add-int/lit8 v2, v7, -0x1

    .line 116
    :cond_2
    invoke-virtual {p1}, Lcom/vividsolutions/jts/linearref/LinearLocation;->isVertex()Z

    move-result v7

    if-nez v7, :cond_3

    .line 117
    iget-object v7, p0, Lcom/vividsolutions/jts/linearref/ExtractLineByLocation;->line:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {p1, v7}, Lcom/vividsolutions/jts/linearref/LinearLocation;->getCoordinate(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/vividsolutions/jts/geom/CoordinateList;->add(Ljava/lang/Object;)Z

    .line 118
    :cond_3
    move v1, v6

    .local v1, "i":I
    :goto_0
    if-gt v1, v2, :cond_4

    .line 119
    aget-object v7, v0, v1

    invoke-virtual {v5, v7}, Lcom/vividsolutions/jts/geom/CoordinateList;->add(Ljava/lang/Object;)Z

    .line 118
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 121
    :cond_4
    invoke-virtual {p2}, Lcom/vividsolutions/jts/linearref/LinearLocation;->isVertex()Z

    move-result v7

    if-nez v7, :cond_5

    .line 122
    iget-object v7, p0, Lcom/vividsolutions/jts/linearref/ExtractLineByLocation;->line:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {p2, v7}, Lcom/vividsolutions/jts/linearref/LinearLocation;->getCoordinate(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/vividsolutions/jts/geom/CoordinateList;->add(Ljava/lang/Object;)Z

    .line 125
    :cond_5
    invoke-virtual {v5}, Lcom/vividsolutions/jts/geom/CoordinateList;->size()I

    move-result v7

    if-gtz v7, :cond_6

    .line 126
    iget-object v7, p0, Lcom/vividsolutions/jts/linearref/ExtractLineByLocation;->line:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {p1, v7}, Lcom/vividsolutions/jts/linearref/LinearLocation;->getCoordinate(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/vividsolutions/jts/geom/CoordinateList;->add(Ljava/lang/Object;)Z

    .line 128
    :cond_6
    invoke-virtual {v5}, Lcom/vividsolutions/jts/geom/CoordinateList;->toCoordinateArray()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v3

    .line 134
    .local v3, "newCoordinateArray":[Lcom/vividsolutions/jts/geom/Coordinate;
    array-length v7, v3

    const/4 v8, 0x1

    if-gt v7, v8, :cond_7

    .line 135
    const/4 v7, 0x2

    new-array v4, v7, [Lcom/vividsolutions/jts/geom/Coordinate;

    const/4 v7, 0x0

    const/4 v8, 0x0

    aget-object v8, v3, v8

    aput-object v8, v4, v7

    const/4 v7, 0x1

    const/4 v8, 0x0

    aget-object v8, v3, v8

    aput-object v8, v4, v7

    .end local v3    # "newCoordinateArray":[Lcom/vividsolutions/jts/geom/Coordinate;
    .local v4, "newCoordinateArray":[Lcom/vividsolutions/jts/geom/Coordinate;
    move-object v3, v4

    .line 137
    .end local v4    # "newCoordinateArray":[Lcom/vividsolutions/jts/geom/Coordinate;
    .restart local v3    # "newCoordinateArray":[Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_7
    iget-object v7, p0, Lcom/vividsolutions/jts/linearref/ExtractLineByLocation;->line:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v7}, Lcom/vividsolutions/jts/geom/Geometry;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v7

    invoke-virtual {v7, v3}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createLineString([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v7

    return-object v7
.end method

.method private computeLinear(Lcom/vividsolutions/jts/linearref/LinearLocation;Lcom/vividsolutions/jts/linearref/LinearLocation;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 8
    .param p1, "start"    # Lcom/vividsolutions/jts/linearref/LinearLocation;
    .param p2, "end"    # Lcom/vividsolutions/jts/linearref/LinearLocation;

    .prologue
    .line 149
    new-instance v0, Lcom/vividsolutions/jts/linearref/LinearGeometryBuilder;

    iget-object v3, p0, Lcom/vividsolutions/jts/linearref/ExtractLineByLocation;->line:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v3}, Lcom/vividsolutions/jts/geom/Geometry;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/vividsolutions/jts/linearref/LinearGeometryBuilder;-><init>(Lcom/vividsolutions/jts/geom/GeometryFactory;)V

    .line 150
    .local v0, "builder":Lcom/vividsolutions/jts/linearref/LinearGeometryBuilder;
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/vividsolutions/jts/linearref/LinearGeometryBuilder;->setFixInvalidLines(Z)V

    .line 152
    invoke-virtual {p1}, Lcom/vividsolutions/jts/linearref/LinearLocation;->isVertex()Z

    move-result v3

    if-nez v3, :cond_0

    .line 153
    iget-object v3, p0, Lcom/vividsolutions/jts/linearref/ExtractLineByLocation;->line:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {p1, v3}, Lcom/vividsolutions/jts/linearref/LinearLocation;->getCoordinate(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/vividsolutions/jts/linearref/LinearGeometryBuilder;->add(Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 155
    :cond_0
    new-instance v1, Lcom/vividsolutions/jts/linearref/LinearIterator;

    iget-object v3, p0, Lcom/vividsolutions/jts/linearref/ExtractLineByLocation;->line:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-direct {v1, v3, p1}, Lcom/vividsolutions/jts/linearref/LinearIterator;-><init>(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/linearref/LinearLocation;)V

    .local v1, "it":Lcom/vividsolutions/jts/linearref/LinearIterator;
    :goto_0
    invoke-virtual {v1}, Lcom/vividsolutions/jts/linearref/LinearIterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 156
    invoke-virtual {v1}, Lcom/vividsolutions/jts/linearref/LinearIterator;->getComponentIndex()I

    move-result v3

    invoke-virtual {v1}, Lcom/vividsolutions/jts/linearref/LinearIterator;->getVertexIndex()I

    move-result v4

    const-wide/16 v6, 0x0

    invoke-virtual {p2, v3, v4, v6, v7}, Lcom/vividsolutions/jts/linearref/LinearLocation;->compareLocationValues(IID)I

    move-result v3

    if-gez v3, :cond_3

    .line 165
    :cond_1
    invoke-virtual {p2}, Lcom/vividsolutions/jts/linearref/LinearLocation;->isVertex()Z

    move-result v3

    if-nez v3, :cond_2

    .line 166
    iget-object v3, p0, Lcom/vividsolutions/jts/linearref/ExtractLineByLocation;->line:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {p2, v3}, Lcom/vividsolutions/jts/linearref/LinearLocation;->getCoordinate(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/vividsolutions/jts/linearref/LinearGeometryBuilder;->add(Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 168
    :cond_2
    invoke-virtual {v0}, Lcom/vividsolutions/jts/linearref/LinearGeometryBuilder;->getGeometry()Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v3

    return-object v3

    .line 160
    :cond_3
    invoke-virtual {v1}, Lcom/vividsolutions/jts/linearref/LinearIterator;->getSegmentStart()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    .line 161
    .local v2, "pt":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-virtual {v0, v2}, Lcom/vividsolutions/jts/linearref/LinearGeometryBuilder;->add(Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 162
    invoke-virtual {v1}, Lcom/vividsolutions/jts/linearref/LinearIterator;->isEndOfLine()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 163
    invoke-virtual {v0}, Lcom/vividsolutions/jts/linearref/LinearGeometryBuilder;->endLine()V

    .line 155
    :cond_4
    invoke-virtual {v1}, Lcom/vividsolutions/jts/linearref/LinearIterator;->next()V

    goto :goto_0
.end method

.method public static extract(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/linearref/LinearLocation;Lcom/vividsolutions/jts/linearref/LinearLocation;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 2
    .param p0, "line"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p1, "start"    # Lcom/vividsolutions/jts/linearref/LinearLocation;
    .param p2, "end"    # Lcom/vividsolutions/jts/linearref/LinearLocation;

    .prologue
    .line 58
    new-instance v0, Lcom/vividsolutions/jts/linearref/ExtractLineByLocation;

    invoke-direct {v0, p0}, Lcom/vividsolutions/jts/linearref/ExtractLineByLocation;-><init>(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 59
    .local v0, "ls":Lcom/vividsolutions/jts/linearref/ExtractLineByLocation;
    invoke-virtual {v0, p1, p2}, Lcom/vividsolutions/jts/linearref/ExtractLineByLocation;->extract(Lcom/vividsolutions/jts/linearref/LinearLocation;Lcom/vividsolutions/jts/linearref/LinearLocation;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    return-object v1
.end method

.method private reverse(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 1
    .param p1, "linear"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 86
    instance-of v0, p1, Lcom/vividsolutions/jts/geom/LineString;

    if-eqz v0, :cond_0

    .line 87
    check-cast p1, Lcom/vividsolutions/jts/geom/LineString;

    .end local p1    # "linear":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/LineString;->reverse()Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    .line 91
    :goto_0
    return-object v0

    .line 88
    .restart local p1    # "linear":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_0
    instance-of v0, p1, Lcom/vividsolutions/jts/geom/MultiLineString;

    if-eqz v0, :cond_1

    .line 89
    check-cast p1, Lcom/vividsolutions/jts/geom/MultiLineString;

    .end local p1    # "linear":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/MultiLineString;->reverse()Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    goto :goto_0

    .line 90
    .restart local p1    # "linear":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_1
    const-string v0, "non-linear geometry encountered"

    invoke-static {v0}, Lcom/vividsolutions/jts/util/Assert;->shouldNeverReachHere(Ljava/lang/String;)V

    .line 91
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public extract(Lcom/vividsolutions/jts/linearref/LinearLocation;Lcom/vividsolutions/jts/linearref/LinearLocation;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 1
    .param p1, "start"    # Lcom/vividsolutions/jts/linearref/LinearLocation;
    .param p2, "end"    # Lcom/vividsolutions/jts/linearref/LinearLocation;

    .prologue
    .line 78
    invoke-virtual {p2, p1}, Lcom/vividsolutions/jts/linearref/LinearLocation;->compareTo(Ljava/lang/Object;)I

    move-result v0

    if-gez v0, :cond_0

    .line 79
    invoke-direct {p0, p2, p1}, Lcom/vividsolutions/jts/linearref/ExtractLineByLocation;->computeLinear(Lcom/vividsolutions/jts/linearref/LinearLocation;Lcom/vividsolutions/jts/linearref/LinearLocation;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/vividsolutions/jts/linearref/ExtractLineByLocation;->reverse(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    .line 81
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/vividsolutions/jts/linearref/ExtractLineByLocation;->computeLinear(Lcom/vividsolutions/jts/linearref/LinearLocation;Lcom/vividsolutions/jts/linearref/LinearLocation;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    goto :goto_0
.end method

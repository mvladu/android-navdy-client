.class public Lcom/vividsolutions/jts/linearref/LinearIterator;
.super Ljava/lang/Object;
.source "LinearIterator.java"


# instance fields
.field private componentIndex:I

.field private currentLine:Lcom/vividsolutions/jts/geom/LineString;

.field private linearGeom:Lcom/vividsolutions/jts/geom/Geometry;

.field private final numLines:I

.field private vertexIndex:I


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 1
    .param p1, "linear"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    const/4 v0, 0x0

    .line 81
    invoke-direct {p0, p1, v0, v0}, Lcom/vividsolutions/jts/linearref/LinearIterator;-><init>(Lcom/vividsolutions/jts/geom/Geometry;II)V

    .line 82
    return-void
.end method

.method public constructor <init>(Lcom/vividsolutions/jts/geom/Geometry;II)V
    .locals 2
    .param p1, "linearGeom"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "componentIndex"    # I
    .param p3, "vertexIndex"    # I

    .prologue
    const/4 v0, 0x0

    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    iput v0, p0, Lcom/vividsolutions/jts/linearref/LinearIterator;->componentIndex:I

    .line 72
    iput v0, p0, Lcom/vividsolutions/jts/linearref/LinearIterator;->vertexIndex:I

    .line 107
    instance-of v0, p1, Lcom/vividsolutions/jts/geom/Lineal;

    if-nez v0, :cond_0

    .line 108
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Lineal geometry is required"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 109
    :cond_0
    iput-object p1, p0, Lcom/vividsolutions/jts/linearref/LinearIterator;->linearGeom:Lcom/vividsolutions/jts/geom/Geometry;

    .line 110
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getNumGeometries()I

    move-result v0

    iput v0, p0, Lcom/vividsolutions/jts/linearref/LinearIterator;->numLines:I

    .line 111
    iput p2, p0, Lcom/vividsolutions/jts/linearref/LinearIterator;->componentIndex:I

    .line 112
    iput p3, p0, Lcom/vividsolutions/jts/linearref/LinearIterator;->vertexIndex:I

    .line 113
    invoke-direct {p0}, Lcom/vividsolutions/jts/linearref/LinearIterator;->loadCurrentLine()V

    .line 114
    return-void
.end method

.method public constructor <init>(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/linearref/LinearLocation;)V
    .locals 2
    .param p1, "linear"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "start"    # Lcom/vividsolutions/jts/linearref/LinearLocation;

    .prologue
    .line 93
    invoke-virtual {p2}, Lcom/vividsolutions/jts/linearref/LinearLocation;->getComponentIndex()I

    move-result v0

    invoke-static {p2}, Lcom/vividsolutions/jts/linearref/LinearIterator;->segmentEndVertexIndex(Lcom/vividsolutions/jts/linearref/LinearLocation;)I

    move-result v1

    invoke-direct {p0, p1, v0, v1}, Lcom/vividsolutions/jts/linearref/LinearIterator;-><init>(Lcom/vividsolutions/jts/geom/Geometry;II)V

    .line 94
    return-void
.end method

.method private loadCurrentLine()V
    .locals 2

    .prologue
    .line 118
    iget v0, p0, Lcom/vividsolutions/jts/linearref/LinearIterator;->componentIndex:I

    iget v1, p0, Lcom/vividsolutions/jts/linearref/LinearIterator;->numLines:I

    if-lt v0, v1, :cond_0

    .line 119
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vividsolutions/jts/linearref/LinearIterator;->currentLine:Lcom/vividsolutions/jts/geom/LineString;

    .line 123
    :goto_0
    return-void

    .line 122
    :cond_0
    iget-object v0, p0, Lcom/vividsolutions/jts/linearref/LinearIterator;->linearGeom:Lcom/vividsolutions/jts/geom/Geometry;

    iget v1, p0, Lcom/vividsolutions/jts/linearref/LinearIterator;->componentIndex:I

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/Geometry;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geom/LineString;

    iput-object v0, p0, Lcom/vividsolutions/jts/linearref/LinearIterator;->currentLine:Lcom/vividsolutions/jts/geom/LineString;

    goto :goto_0
.end method

.method private static segmentEndVertexIndex(Lcom/vividsolutions/jts/linearref/LinearLocation;)I
    .locals 4
    .param p0, "loc"    # Lcom/vividsolutions/jts/linearref/LinearLocation;

    .prologue
    .line 59
    invoke-virtual {p0}, Lcom/vividsolutions/jts/linearref/LinearLocation;->getSegmentFraction()D

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    .line 60
    invoke-virtual {p0}, Lcom/vividsolutions/jts/linearref/LinearLocation;->getSegmentIndex()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 61
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/vividsolutions/jts/linearref/LinearLocation;->getSegmentIndex()I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public getComponentIndex()I
    .locals 1

    .prologue
    .line 175
    iget v0, p0, Lcom/vividsolutions/jts/linearref/LinearIterator;->componentIndex:I

    return v0
.end method

.method public getLine()Lcom/vividsolutions/jts/geom/LineString;
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/vividsolutions/jts/linearref/LinearIterator;->currentLine:Lcom/vividsolutions/jts/geom/LineString;

    return-object v0
.end method

.method public getSegmentEnd()Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 2

    .prologue
    .line 205
    iget v0, p0, Lcom/vividsolutions/jts/linearref/LinearIterator;->vertexIndex:I

    invoke-virtual {p0}, Lcom/vividsolutions/jts/linearref/LinearIterator;->getLine()Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/LineString;->getNumPoints()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    .line 206
    iget-object v0, p0, Lcom/vividsolutions/jts/linearref/LinearIterator;->currentLine:Lcom/vividsolutions/jts/geom/LineString;

    iget v1, p0, Lcom/vividsolutions/jts/linearref/LinearIterator;->vertexIndex:I

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinateN(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    .line 207
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSegmentStart()Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 2

    .prologue
    .line 194
    iget-object v0, p0, Lcom/vividsolutions/jts/linearref/LinearIterator;->currentLine:Lcom/vividsolutions/jts/geom/LineString;

    iget v1, p0, Lcom/vividsolutions/jts/linearref/LinearIterator;->vertexIndex:I

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinateN(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    return-object v0
.end method

.method public getVertexIndex()I
    .locals 1

    .prologue
    .line 181
    iget v0, p0, Lcom/vividsolutions/jts/linearref/LinearIterator;->vertexIndex:I

    return v0
.end method

.method public hasNext()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 135
    iget v1, p0, Lcom/vividsolutions/jts/linearref/LinearIterator;->componentIndex:I

    iget v2, p0, Lcom/vividsolutions/jts/linearref/LinearIterator;->numLines:I

    if-lt v1, v2, :cond_1

    .line 139
    :cond_0
    :goto_0
    return v0

    .line 136
    :cond_1
    iget v1, p0, Lcom/vividsolutions/jts/linearref/LinearIterator;->componentIndex:I

    iget v2, p0, Lcom/vividsolutions/jts/linearref/LinearIterator;->numLines:I

    add-int/lit8 v2, v2, -0x1

    if-ne v1, v2, :cond_2

    iget v1, p0, Lcom/vividsolutions/jts/linearref/LinearIterator;->vertexIndex:I

    iget-object v2, p0, Lcom/vividsolutions/jts/linearref/LinearIterator;->currentLine:Lcom/vividsolutions/jts/geom/LineString;

    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/LineString;->getNumPoints()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 139
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isEndOfLine()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 164
    iget v1, p0, Lcom/vividsolutions/jts/linearref/LinearIterator;->componentIndex:I

    iget v2, p0, Lcom/vividsolutions/jts/linearref/LinearIterator;->numLines:I

    if-lt v1, v2, :cond_1

    .line 168
    :cond_0
    :goto_0
    return v0

    .line 166
    :cond_1
    iget v1, p0, Lcom/vividsolutions/jts/linearref/LinearIterator;->vertexIndex:I

    iget-object v2, p0, Lcom/vividsolutions/jts/linearref/LinearIterator;->currentLine:Lcom/vividsolutions/jts/geom/LineString;

    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/LineString;->getNumPoints()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-lt v1, v2, :cond_0

    .line 168
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public next()V
    .locals 2

    .prologue
    .line 147
    invoke-virtual {p0}, Lcom/vividsolutions/jts/linearref/LinearIterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 155
    :cond_0
    :goto_0
    return-void

    .line 149
    :cond_1
    iget v0, p0, Lcom/vividsolutions/jts/linearref/LinearIterator;->vertexIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/vividsolutions/jts/linearref/LinearIterator;->vertexIndex:I

    .line 150
    iget v0, p0, Lcom/vividsolutions/jts/linearref/LinearIterator;->vertexIndex:I

    iget-object v1, p0, Lcom/vividsolutions/jts/linearref/LinearIterator;->currentLine:Lcom/vividsolutions/jts/geom/LineString;

    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/LineString;->getNumPoints()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 151
    iget v0, p0, Lcom/vividsolutions/jts/linearref/LinearIterator;->componentIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/vividsolutions/jts/linearref/LinearIterator;->componentIndex:I

    .line 152
    invoke-direct {p0}, Lcom/vividsolutions/jts/linearref/LinearIterator;->loadCurrentLine()V

    .line 153
    const/4 v0, 0x0

    iput v0, p0, Lcom/vividsolutions/jts/linearref/LinearIterator;->vertexIndex:I

    goto :goto_0
.end method

.class Lcom/vividsolutions/jts/linearref/LengthIndexOfPoint;
.super Ljava/lang/Object;
.source "LengthIndexOfPoint.java"


# instance fields
.field private linearGeom:Lcom/vividsolutions/jts/geom/Geometry;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 0
    .param p1, "linearGeom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput-object p1, p0, Lcom/vividsolutions/jts/linearref/LengthIndexOfPoint;->linearGeom:Lcom/vividsolutions/jts/geom/Geometry;

    .line 64
    return-void
.end method

.method public static indexOf(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Coordinate;)D
    .locals 4
    .param p0, "linearGeom"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p1, "inputPt"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 50
    new-instance v0, Lcom/vividsolutions/jts/linearref/LengthIndexOfPoint;

    invoke-direct {v0, p0}, Lcom/vividsolutions/jts/linearref/LengthIndexOfPoint;-><init>(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 51
    .local v0, "locater":Lcom/vividsolutions/jts/linearref/LengthIndexOfPoint;
    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/linearref/LengthIndexOfPoint;->indexOf(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v2

    return-wide v2
.end method

.method public static indexOfAfter(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Coordinate;D)D
    .locals 4
    .param p0, "linearGeom"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p1, "inputPt"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "minIndex"    # D

    .prologue
    .line 56
    new-instance v0, Lcom/vividsolutions/jts/linearref/LengthIndexOfPoint;

    invoke-direct {v0, p0}, Lcom/vividsolutions/jts/linearref/LengthIndexOfPoint;-><init>(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 57
    .local v0, "locater":Lcom/vividsolutions/jts/linearref/LengthIndexOfPoint;
    invoke-virtual {v0, p1, p2, p3}, Lcom/vividsolutions/jts/linearref/LengthIndexOfPoint;->indexOfAfter(Lcom/vividsolutions/jts/geom/Coordinate;D)D

    move-result-wide v2

    return-wide v2
.end method

.method private indexOfFromStart(Lcom/vividsolutions/jts/geom/Coordinate;D)D
    .locals 14
    .param p1, "inputPt"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "minIndex"    # D

    .prologue
    .line 112
    const-wide v2, 0x7fefffffffffffffL    # Double.MAX_VALUE

    .line 114
    .local v2, "minDistance":D
    move-wide/from16 v4, p2

    .line 115
    .local v4, "ptMeasure":D
    const-wide/16 v10, 0x0

    .line 116
    .local v10, "segmentStartMeasure":D
    new-instance v1, Lcom/vividsolutions/jts/geom/LineSegment;

    invoke-direct {v1}, Lcom/vividsolutions/jts/geom/LineSegment;-><init>()V

    .line 117
    .local v1, "seg":Lcom/vividsolutions/jts/geom/LineSegment;
    new-instance v0, Lcom/vividsolutions/jts/linearref/LinearIterator;

    iget-object v12, p0, Lcom/vividsolutions/jts/linearref/LengthIndexOfPoint;->linearGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-direct {v0, v12}, Lcom/vividsolutions/jts/linearref/LinearIterator;-><init>(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 118
    .local v0, "it":Lcom/vividsolutions/jts/linearref/LinearIterator;
    :goto_0
    invoke-virtual {v0}, Lcom/vividsolutions/jts/linearref/LinearIterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_2

    .line 119
    invoke-virtual {v0}, Lcom/vividsolutions/jts/linearref/LinearIterator;->isEndOfLine()Z

    move-result v12

    if-nez v12, :cond_1

    .line 120
    invoke-virtual {v0}, Lcom/vividsolutions/jts/linearref/LinearIterator;->getSegmentStart()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v12

    iput-object v12, v1, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 121
    invoke-virtual {v0}, Lcom/vividsolutions/jts/linearref/LinearIterator;->getSegmentEnd()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v12

    iput-object v12, v1, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 122
    invoke-virtual {v1, p1}, Lcom/vividsolutions/jts/geom/LineSegment;->distance(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v6

    .line 123
    .local v6, "segDistance":D
    invoke-direct {p0, v1, p1, v10, v11}, Lcom/vividsolutions/jts/linearref/LengthIndexOfPoint;->segmentNearestMeasure(Lcom/vividsolutions/jts/geom/LineSegment;Lcom/vividsolutions/jts/geom/Coordinate;D)D

    move-result-wide v8

    .line 124
    .local v8, "segMeasureToPt":D
    cmpg-double v12, v6, v2

    if-gez v12, :cond_0

    cmpl-double v12, v8, p2

    if-lez v12, :cond_0

    .line 126
    move-wide v4, v8

    .line 127
    move-wide v2, v6

    .line 129
    :cond_0
    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/LineSegment;->getLength()D

    move-result-wide v12

    add-double/2addr v10, v12

    .line 131
    .end local v6    # "segDistance":D
    .end local v8    # "segMeasureToPt":D
    :cond_1
    invoke-virtual {v0}, Lcom/vividsolutions/jts/linearref/LinearIterator;->next()V

    goto :goto_0

    .line 133
    :cond_2
    return-wide v4
.end method

.method private segmentNearestMeasure(Lcom/vividsolutions/jts/geom/LineSegment;Lcom/vividsolutions/jts/geom/Coordinate;D)D
    .locals 5
    .param p1, "seg"    # Lcom/vividsolutions/jts/geom/LineSegment;
    .param p2, "inputPt"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p3, "segmentStartMeasure"    # D

    .prologue
    .line 140
    invoke-virtual {p1, p2}, Lcom/vividsolutions/jts/geom/LineSegment;->projectionFactor(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v0

    .line 141
    .local v0, "projFactor":D
    const-wide/16 v2, 0x0

    cmpg-double v2, v0, v2

    if-gtz v2, :cond_0

    .line 146
    .end local p3    # "segmentStartMeasure":D
    :goto_0
    return-wide p3

    .line 143
    .restart local p3    # "segmentStartMeasure":D
    :cond_0
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    cmpg-double v2, v0, v2

    if-gtz v2, :cond_1

    .line 144
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/LineSegment;->getLength()D

    move-result-wide v2

    mul-double/2addr v2, v0

    add-double/2addr p3, v2

    goto :goto_0

    .line 146
    :cond_1
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/LineSegment;->getLength()D

    move-result-wide v2

    add-double/2addr p3, v2

    goto :goto_0
.end method


# virtual methods
.method public indexOf(Lcom/vividsolutions/jts/geom/Coordinate;)D
    .locals 2
    .param p1, "inputPt"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 74
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    invoke-direct {p0, p1, v0, v1}, Lcom/vividsolutions/jts/linearref/LengthIndexOfPoint;->indexOfFromStart(Lcom/vividsolutions/jts/geom/Coordinate;D)D

    move-result-wide v0

    return-wide v0
.end method

.method public indexOfAfter(Lcom/vividsolutions/jts/geom/Coordinate;D)D
    .locals 6
    .param p1, "inputPt"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "minIndex"    # D

    .prologue
    .line 94
    const-wide/16 v4, 0x0

    cmpg-double v4, p2, v4

    if-gez v4, :cond_1

    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/linearref/LengthIndexOfPoint;->indexOf(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v2

    .line 107
    :cond_0
    :goto_0
    return-wide v2

    .line 97
    :cond_1
    iget-object v4, p0, Lcom/vividsolutions/jts/linearref/LengthIndexOfPoint;->linearGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v4}, Lcom/vividsolutions/jts/geom/Geometry;->getLength()D

    move-result-wide v2

    .line 98
    .local v2, "endIndex":D
    cmpg-double v4, v2, p2

    if-ltz v4, :cond_0

    .line 101
    invoke-direct {p0, p1, p2, p3}, Lcom/vividsolutions/jts/linearref/LengthIndexOfPoint;->indexOfFromStart(Lcom/vividsolutions/jts/geom/Coordinate;D)D

    move-result-wide v0

    .line 105
    .local v0, "closestAfter":D
    cmpl-double v4, v0, p2

    if-ltz v4, :cond_2

    const/4 v4, 0x1

    :goto_1
    const-string v5, "computed index is before specified minimum index"

    invoke-static {v4, v5}, Lcom/vividsolutions/jts/util/Assert;->isTrue(ZLjava/lang/String;)V

    move-wide v2, v0

    .line 107
    goto :goto_0

    .line 105
    :cond_2
    const/4 v4, 0x0

    goto :goto_1
.end method

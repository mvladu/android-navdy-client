.class public Lcom/vividsolutions/jts/geomgraph/Edge;
.super Lcom/vividsolutions/jts/geomgraph/GraphComponent;
.source "Edge.java"


# instance fields
.field private depth:Lcom/vividsolutions/jts/geomgraph/Depth;

.field private depthDelta:I

.field eiList:Lcom/vividsolutions/jts/geomgraph/EdgeIntersectionList;

.field private env:Lcom/vividsolutions/jts/geom/Envelope;

.field private isIsolated:Z

.field private mce:Lcom/vividsolutions/jts/geomgraph/index/MonotoneChainEdge;

.field private name:Ljava/lang/String;

.field pts:[Lcom/vividsolutions/jts/geom/Coordinate;


# direct methods
.method public constructor <init>([Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 1
    .param p1, "pts"    # [Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 83
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/vividsolutions/jts/geomgraph/Edge;-><init>([Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geomgraph/Label;)V

    .line 84
    return-void
.end method

.method public constructor <init>([Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geomgraph/Label;)V
    .locals 1
    .param p1, "pts"    # [Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "label"    # Lcom/vividsolutions/jts/geomgraph/Label;

    .prologue
    .line 77
    invoke-direct {p0}, Lcom/vividsolutions/jts/geomgraph/GraphComponent;-><init>()V

    .line 69
    new-instance v0, Lcom/vividsolutions/jts/geomgraph/EdgeIntersectionList;

    invoke-direct {v0, p0}, Lcom/vividsolutions/jts/geomgraph/EdgeIntersectionList;-><init>(Lcom/vividsolutions/jts/geomgraph/Edge;)V

    iput-object v0, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->eiList:Lcom/vividsolutions/jts/geomgraph/EdgeIntersectionList;

    .line 72
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->isIsolated:Z

    .line 73
    new-instance v0, Lcom/vividsolutions/jts/geomgraph/Depth;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geomgraph/Depth;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->depth:Lcom/vividsolutions/jts/geomgraph/Depth;

    .line 74
    const/4 v0, 0x0

    iput v0, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->depthDelta:I

    .line 78
    iput-object p1, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    .line 79
    iput-object p2, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->label:Lcom/vividsolutions/jts/geomgraph/Label;

    .line 80
    return-void
.end method

.method public static updateIM(Lcom/vividsolutions/jts/geomgraph/Label;Lcom/vividsolutions/jts/geom/IntersectionMatrix;)V
    .locals 5
    .param p0, "label"    # Lcom/vividsolutions/jts/geomgraph/Label;
    .param p1, "im"    # Lcom/vividsolutions/jts/geom/IntersectionMatrix;

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 60
    invoke-virtual {p0, v3, v3}, Lcom/vividsolutions/jts/geomgraph/Label;->getLocation(II)I

    move-result v0

    invoke-virtual {p0, v2, v3}, Lcom/vividsolutions/jts/geomgraph/Label;->getLocation(II)I

    move-result v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/vividsolutions/jts/geom/IntersectionMatrix;->setAtLeastIfValid(III)V

    .line 61
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geomgraph/Label;->isArea()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    invoke-virtual {p0, v3, v2}, Lcom/vividsolutions/jts/geomgraph/Label;->getLocation(II)I

    move-result v0

    invoke-virtual {p0, v2, v2}, Lcom/vividsolutions/jts/geomgraph/Label;->getLocation(II)I

    move-result v1

    invoke-virtual {p1, v0, v1, v4}, Lcom/vividsolutions/jts/geom/IntersectionMatrix;->setAtLeastIfValid(III)V

    .line 63
    invoke-virtual {p0, v3, v4}, Lcom/vividsolutions/jts/geomgraph/Label;->getLocation(II)I

    move-result v0

    invoke-virtual {p0, v2, v4}, Lcom/vividsolutions/jts/geomgraph/Label;->getLocation(II)I

    move-result v1

    invoke-virtual {p1, v0, v1, v4}, Lcom/vividsolutions/jts/geom/IntersectionMatrix;->setAtLeastIfValid(III)V

    .line 65
    :cond_0
    return-void
.end method


# virtual methods
.method public addIntersection(Lcom/vividsolutions/jts/algorithm/LineIntersector;III)V
    .locals 8
    .param p1, "li"    # Lcom/vividsolutions/jts/algorithm/LineIntersector;
    .param p2, "segmentIndex"    # I
    .param p3, "geomIndex"    # I
    .param p4, "intIndex"    # I

    .prologue
    .line 181
    new-instance v3, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {p1, p4}, Lcom/vividsolutions/jts/algorithm/LineIntersector;->getIntersection(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v7

    invoke-direct {v3, v7}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 182
    .local v3, "intPt":Lcom/vividsolutions/jts/geom/Coordinate;
    move v6, p2

    .line 183
    .local v6, "normalizedSegmentIndex":I
    invoke-virtual {p1, p3, p4}, Lcom/vividsolutions/jts/algorithm/LineIntersector;->getEdgeDistance(II)D

    move-result-wide v0

    .line 186
    .local v0, "dist":D
    add-int/lit8 v5, v6, 0x1

    .line 187
    .local v5, "nextSegIndex":I
    iget-object v7, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    array-length v7, v7

    if-ge v5, v7, :cond_0

    .line 188
    iget-object v7, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    aget-object v4, v7, v5

    .line 193
    .local v4, "nextPt":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-virtual {v3, v4}, Lcom/vividsolutions/jts/geom/Coordinate;->equals2D(Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 195
    move v6, v5

    .line 196
    const-wide/16 v0, 0x0

    .line 202
    .end local v4    # "nextPt":Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_0
    iget-object v7, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->eiList:Lcom/vividsolutions/jts/geomgraph/EdgeIntersectionList;

    invoke-virtual {v7, v3, v6, v0, v1}, Lcom/vividsolutions/jts/geomgraph/EdgeIntersectionList;->add(Lcom/vividsolutions/jts/geom/Coordinate;ID)Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;

    move-result-object v2

    .line 205
    .local v2, "ei":Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;
    return-void
.end method

.method public addIntersections(Lcom/vividsolutions/jts/algorithm/LineIntersector;II)V
    .locals 2
    .param p1, "li"    # Lcom/vividsolutions/jts/algorithm/LineIntersector;
    .param p2, "segmentIndex"    # I
    .param p3, "geomIndex"    # I

    .prologue
    .line 170
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/vividsolutions/jts/algorithm/LineIntersector;->getIntersectionNum()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 171
    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/vividsolutions/jts/geomgraph/Edge;->addIntersection(Lcom/vividsolutions/jts/algorithm/LineIntersector;III)V

    .line 170
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 173
    :cond_0
    return-void
.end method

.method public computeIM(Lcom/vividsolutions/jts/geom/IntersectionMatrix;)V
    .locals 1
    .param p1, "im"    # Lcom/vividsolutions/jts/geom/IntersectionMatrix;

    .prologue
    .line 213
    iget-object v0, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->label:Lcom/vividsolutions/jts/geomgraph/Label;

    invoke-static {v0, p1}, Lcom/vividsolutions/jts/geomgraph/Edge;->updateIM(Lcom/vividsolutions/jts/geomgraph/Label;Lcom/vividsolutions/jts/geom/IntersectionMatrix;)V

    .line 214
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v5, 0x0

    .line 225
    instance-of v6, p1, Lcom/vividsolutions/jts/geomgraph/Edge;

    if-nez v6, :cond_1

    .line 242
    :cond_0
    :goto_0
    return v5

    :cond_1
    move-object v0, p1

    .line 226
    check-cast v0, Lcom/vividsolutions/jts/geomgraph/Edge;

    .line 228
    .local v0, "e":Lcom/vividsolutions/jts/geomgraph/Edge;
    iget-object v6, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    array-length v6, v6

    iget-object v7, v0, Lcom/vividsolutions/jts/geomgraph/Edge;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    array-length v7, v7

    if-ne v6, v7, :cond_0

    .line 230
    const/4 v3, 0x1

    .line 231
    .local v3, "isEqualForward":Z
    const/4 v4, 0x1

    .line 232
    .local v4, "isEqualReverse":Z
    iget-object v6, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    array-length v2, v6

    .line 233
    .local v2, "iRev":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v6, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    array-length v6, v6

    if-ge v1, v6, :cond_5

    .line 234
    iget-object v6, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    aget-object v6, v6, v1

    iget-object v7, v0, Lcom/vividsolutions/jts/geomgraph/Edge;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    aget-object v7, v7, v1

    invoke-virtual {v6, v7}, Lcom/vividsolutions/jts/geom/Coordinate;->equals2D(Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 235
    const/4 v3, 0x0

    .line 237
    :cond_2
    iget-object v6, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    aget-object v6, v6, v1

    iget-object v7, v0, Lcom/vividsolutions/jts/geomgraph/Edge;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    add-int/lit8 v2, v2, -0x1

    aget-object v7, v7, v2

    invoke-virtual {v6, v7}, Lcom/vividsolutions/jts/geom/Coordinate;->equals2D(Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 238
    const/4 v4, 0x0

    .line 240
    :cond_3
    if-nez v3, :cond_4

    if-eqz v4, :cond_0

    .line 233
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 242
    :cond_5
    const/4 v5, 0x1

    goto :goto_0
.end method

.method public getCollapsedEdge()Lcom/vividsolutions/jts/geomgraph/Edge;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 148
    const/4 v2, 0x2

    new-array v0, v2, [Lcom/vividsolutions/jts/geom/Coordinate;

    .line 149
    .local v0, "newPts":[Lcom/vividsolutions/jts/geom/Coordinate;
    iget-object v2, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    aget-object v2, v2, v3

    aput-object v2, v0, v3

    .line 150
    iget-object v2, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    aget-object v2, v2, v4

    aput-object v2, v0, v4

    .line 151
    new-instance v1, Lcom/vividsolutions/jts/geomgraph/Edge;

    iget-object v2, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->label:Lcom/vividsolutions/jts/geomgraph/Label;

    invoke-static {v2}, Lcom/vividsolutions/jts/geomgraph/Label;->toLineLabel(Lcom/vividsolutions/jts/geomgraph/Label;)Lcom/vividsolutions/jts/geomgraph/Label;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/vividsolutions/jts/geomgraph/Edge;-><init>([Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geomgraph/Label;)V

    .line 152
    .local v1, "newe":Lcom/vividsolutions/jts/geomgraph/Edge;
    return-object v1
.end method

.method public getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 2

    .prologue
    .line 95
    iget-object v0, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    array-length v0, v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    .line 96
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCoordinate(I)Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 91
    iget-object v0, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    return-object v0
.end method

.method public getDepth()Lcom/vividsolutions/jts/geomgraph/Depth;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->depth:Lcom/vividsolutions/jts/geomgraph/Depth;

    return-object v0
.end method

.method public getDepthDelta()I
    .locals 1

    .prologue
    .line 116
    iget v0, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->depthDelta:I

    return v0
.end method

.method public getEdgeIntersectionList()Lcom/vividsolutions/jts/geomgraph/EdgeIntersectionList;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->eiList:Lcom/vividsolutions/jts/geomgraph/EdgeIntersectionList;

    return-object v0
.end method

.method public getEnvelope()Lcom/vividsolutions/jts/geom/Envelope;
    .locals 3

    .prologue
    .line 101
    iget-object v1, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->env:Lcom/vividsolutions/jts/geom/Envelope;

    if-nez v1, :cond_0

    .line 102
    new-instance v1, Lcom/vividsolutions/jts/geom/Envelope;

    invoke-direct {v1}, Lcom/vividsolutions/jts/geom/Envelope;-><init>()V

    iput-object v1, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->env:Lcom/vividsolutions/jts/geom/Envelope;

    .line 103
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 104
    iget-object v1, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->env:Lcom/vividsolutions/jts/geom/Envelope;

    iget-object v2, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Lcom/vividsolutions/jts/geom/Envelope;->expandToInclude(Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 103
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 107
    .end local v0    # "i":I
    :cond_0
    iget-object v1, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->env:Lcom/vividsolutions/jts/geom/Envelope;

    return-object v1
.end method

.method public getMaximumSegmentIndex()I
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public getMonotoneChainEdge()Lcom/vividsolutions/jts/geomgraph/index/MonotoneChainEdge;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->mce:Lcom/vividsolutions/jts/geomgraph/index/MonotoneChainEdge;

    if-nez v0, :cond_0

    new-instance v0, Lcom/vividsolutions/jts/geomgraph/index/MonotoneChainEdge;

    invoke-direct {v0, p0}, Lcom/vividsolutions/jts/geomgraph/index/MonotoneChainEdge;-><init>(Lcom/vividsolutions/jts/geomgraph/Edge;)V

    iput-object v0, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->mce:Lcom/vividsolutions/jts/geomgraph/index/MonotoneChainEdge;

    .line 128
    :cond_0
    iget-object v0, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->mce:Lcom/vividsolutions/jts/geomgraph/index/MonotoneChainEdge;

    return-object v0
.end method

.method public getNumPoints()I
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    array-length v0, v0

    return v0
.end method

.method public isClosed()Z
    .locals 3

    .prologue
    .line 133
    iget-object v0, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    iget-object v2, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/Coordinate;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isCollapsed()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 141
    iget-object v1, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->label:Lcom/vividsolutions/jts/geomgraph/Label;

    invoke-virtual {v1}, Lcom/vividsolutions/jts/geomgraph/Label;->isArea()Z

    move-result v1

    if-nez v1, :cond_1

    .line 144
    :cond_0
    :goto_0
    return v0

    .line 142
    :cond_1
    iget-object v1, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    array-length v1, v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    .line 143
    iget-object v1, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    aget-object v1, v1, v0

    iget-object v2, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    const/4 v3, 0x2

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Lcom/vividsolutions/jts/geom/Coordinate;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isIsolated()Z
    .locals 1

    .prologue
    .line 161
    iget-boolean v0, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->isIsolated:Z

    return v0
.end method

.method public isPointwiseEqual(Lcom/vividsolutions/jts/geomgraph/Edge;)Z
    .locals 4
    .param p1, "e"    # Lcom/vividsolutions/jts/geomgraph/Edge;

    .prologue
    const/4 v1, 0x0

    .line 250
    iget-object v2, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    array-length v2, v2

    iget-object v3, p1, Lcom/vividsolutions/jts/geomgraph/Edge;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    array-length v3, v3

    if-eq v2, v3, :cond_1

    .line 257
    :cond_0
    :goto_0
    return v1

    .line 252
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v2, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 253
    iget-object v2, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    aget-object v2, v2, v0

    iget-object v3, p1, Lcom/vividsolutions/jts/geomgraph/Edge;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    aget-object v3, v3, v0

    invoke-virtual {v2, v3}, Lcom/vividsolutions/jts/geom/Coordinate;->equals2D(Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 252
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 257
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public print(Ljava/io/PrintStream;)V
    .locals 4
    .param p1, "out"    # Ljava/io/PrintStream;

    .prologue
    .line 274
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "edge "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 275
    const-string v1, "LINESTRING ("

    invoke-virtual {p1, v1}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 276
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 277
    if-lez v0, :cond_0

    const-string v1, ","

    invoke-virtual {p1, v1}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 278
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    aget-object v2, v2, v0

    iget-wide v2, v2, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    aget-object v2, v2, v0

    iget-wide v2, v2, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 276
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 280
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ")  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->label:Lcom/vividsolutions/jts/geomgraph/Label;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->depthDelta:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 281
    return-void
.end method

.method public printReverse(Ljava/io/PrintStream;)V
    .locals 3
    .param p1, "out"    # Ljava/io/PrintStream;

    .prologue
    .line 284
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "edge "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 285
    iget-object v1, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    array-length v1, v1

    add-int/lit8 v0, v1, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_0

    .line 286
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 285
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 288
    :cond_0
    const-string v1, ""

    invoke-virtual {p1, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 289
    return-void
.end method

.method public setDepthDelta(I)V
    .locals 0
    .param p1, "depthDelta"    # I

    .prologue
    .line 117
    iput p1, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->depthDelta:I

    return-void
.end method

.method public setIsolated(Z)V
    .locals 0
    .param p1, "isIsolated"    # Z

    .prologue
    .line 157
    iput-boolean p1, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->isIsolated:Z

    .line 158
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 87
    iput-object p1, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->name:Ljava/lang/String;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 262
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 263
    .local v0, "buf":Ljava/lang/StringBuffer;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "edge "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 264
    const-string v2, "LINESTRING ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 265
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    array-length v2, v2

    if-ge v1, v2, :cond_1

    .line 266
    if-lez v1, :cond_0

    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 267
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    aget-object v3, v3, v1

    iget-wide v4, v3, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    aget-object v3, v3, v1

    iget-wide v4, v3, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 265
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 269
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ")  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->label:Lcom/vividsolutions/jts/geomgraph/Label;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/vividsolutions/jts/geomgraph/Edge;->depthDelta:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 270
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

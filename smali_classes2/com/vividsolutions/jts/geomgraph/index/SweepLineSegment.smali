.class public Lcom/vividsolutions/jts/geomgraph/index/SweepLineSegment;
.super Ljava/lang/Object;
.source "SweepLineSegment.java"


# instance fields
.field edge:Lcom/vividsolutions/jts/geomgraph/Edge;

.field ptIndex:I

.field pts:[Lcom/vividsolutions/jts/geom/Coordinate;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geomgraph/Edge;I)V
    .locals 1
    .param p1, "edge"    # Lcom/vividsolutions/jts/geomgraph/Edge;
    .param p2, "ptIndex"    # I

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p1, p0, Lcom/vividsolutions/jts/geomgraph/index/SweepLineSegment;->edge:Lcom/vividsolutions/jts/geomgraph/Edge;

    .line 53
    iput p2, p0, Lcom/vividsolutions/jts/geomgraph/index/SweepLineSegment;->ptIndex:I

    .line 54
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geomgraph/Edge;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    iput-object v0, p0, Lcom/vividsolutions/jts/geomgraph/index/SweepLineSegment;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    .line 55
    return-void
.end method


# virtual methods
.method public computeIntersections(Lcom/vividsolutions/jts/geomgraph/index/SweepLineSegment;Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;)V
    .locals 4
    .param p1, "ss"    # Lcom/vividsolutions/jts/geomgraph/index/SweepLineSegment;
    .param p2, "si"    # Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/vividsolutions/jts/geomgraph/index/SweepLineSegment;->edge:Lcom/vividsolutions/jts/geomgraph/Edge;

    iget v1, p0, Lcom/vividsolutions/jts/geomgraph/index/SweepLineSegment;->ptIndex:I

    iget-object v2, p1, Lcom/vividsolutions/jts/geomgraph/index/SweepLineSegment;->edge:Lcom/vividsolutions/jts/geomgraph/Edge;

    iget v3, p1, Lcom/vividsolutions/jts/geomgraph/index/SweepLineSegment;->ptIndex:I

    invoke-virtual {p2, v0, v1, v2, v3}, Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;->addIntersections(Lcom/vividsolutions/jts/geomgraph/Edge;ILcom/vividsolutions/jts/geomgraph/Edge;I)V

    .line 72
    return-void
.end method

.method public getMaxX()D
    .locals 6

    .prologue
    .line 65
    iget-object v4, p0, Lcom/vividsolutions/jts/geomgraph/index/SweepLineSegment;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    iget v5, p0, Lcom/vividsolutions/jts/geomgraph/index/SweepLineSegment;->ptIndex:I

    aget-object v4, v4, v5

    iget-wide v0, v4, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    .line 66
    .local v0, "x1":D
    iget-object v4, p0, Lcom/vividsolutions/jts/geomgraph/index/SweepLineSegment;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    iget v5, p0, Lcom/vividsolutions/jts/geomgraph/index/SweepLineSegment;->ptIndex:I

    add-int/lit8 v5, v5, 0x1

    aget-object v4, v4, v5

    iget-wide v2, v4, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    .line 67
    .local v2, "x2":D
    cmpl-double v4, v0, v2

    if-lez v4, :cond_0

    .end local v0    # "x1":D
    :goto_0
    return-wide v0

    .restart local v0    # "x1":D
    :cond_0
    move-wide v0, v2

    goto :goto_0
.end method

.method public getMinX()D
    .locals 6

    .prologue
    .line 59
    iget-object v4, p0, Lcom/vividsolutions/jts/geomgraph/index/SweepLineSegment;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    iget v5, p0, Lcom/vividsolutions/jts/geomgraph/index/SweepLineSegment;->ptIndex:I

    aget-object v4, v4, v5

    iget-wide v0, v4, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    .line 60
    .local v0, "x1":D
    iget-object v4, p0, Lcom/vividsolutions/jts/geomgraph/index/SweepLineSegment;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    iget v5, p0, Lcom/vividsolutions/jts/geomgraph/index/SweepLineSegment;->ptIndex:I

    add-int/lit8 v5, v5, 0x1

    aget-object v4, v4, v5

    iget-wide v2, v4, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    .line 61
    .local v2, "x2":D
    cmpg-double v4, v0, v2

    if-gez v4, :cond_0

    .end local v0    # "x1":D
    :goto_0
    return-wide v0

    .restart local v0    # "x1":D
    :cond_0
    move-wide v0, v2

    goto :goto_0
.end method
